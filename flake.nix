{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, utils, naersk, rust-overlay }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ rust-overlay.overlays.default ];
        };

        # Rust toolchain
        rustToolchain =
          pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;

        naersk-lib = pkgs.callPackage naersk {
          cargo = rustToolchain;
          rustc = rustToolchain;
        };

        # Native dependencies
        buildInputs = [
          pkgs.pkg-config
          pkgs.glib
          pkgs.gtk4
          pkgs.libadwaita
          pkgs.libepoxy
        ];
      in {
        defaultPackage = naersk-lib.buildPackage {
          src = ./.;
          inherit buildInputs;
        };
        devShell = pkgs.mkShell {
          buildInputs = [
            (rustToolchain.override
              (e: { extensions = e.extensions ++ [ "rust-analyzer" ]; }))
            pkgs.cargo-insta
          ] ++ buildInputs;

          GDK_PIXBUF_MODULE_FILE = "${pkgs.librsvg.out}/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache";
        };
      });
}
