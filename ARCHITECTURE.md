# Architecture

TODO: Describe the high-level architecture and invariants of this project


## How-tos

### Add a New Device

If the new device can be implemented as a combination of existing ones, you can define it in the standard library (`std.simac`).

Otherwise, you need to do these:

1. In [`simac_elements`](crates/simac_elements), define a new low-level circuit element (a type implementing `ElemTrait`) and add it to `tt_all_elem_tys!`

    - You can alternatively define one by [Verilog-A][8].
    See `simac_elements`'s [`README.md`](./crates/simac_elements/README.md) file for instructions.

2. In [`std.simac`](crates/simac_session/std.simac), define a new circuit wrapping this new circuit element.


## Other Files in Repository

This section describes the roles of the non-source files in this repository.

- This document ([`ARCHITECTURE.md`](./ARCHITECTURE.md)) describes the high-level architecture and architectural invariants.

- [`Cargo.toml`](./Cargo.toml) defines a [Cargo workspace][1]. `Cargo.lock` is a "[lock file][2]" to record all Cargo dependencies in the workspace to ensure build reproducibility.

- [`clippy.toml`](./clippy.toml) is a configuration file for [Clippy][7], a linting tool for Rust.

- [`flake.nix`](./flake.nix) defines a [Nix flake][3]. With this file, you can use the [Nix][4] package manager to bring every dependency into the search paths without installing them. `flake.lock` is a lock file for the Nix flake.

- [`README.md`](./README.md) is a document for general audience.

- [`rust-toolchain.toml`](./rust-toolchain.toml) [instructs][5] rustup, a Rust toolchain manager, to use a specific version of the Rust toolchain.

- [`rustfmt.toml`](./rustfmt.toml) is a [configuration file][6] for rustfmt, a source code formatter for Rust.


## Coding Conventions

### Naming

- `num_xxx`: The number of items

- `xxx_i`, `XxxI`: An index of XXX
    - `xxx_i_list`: A list of indices of XXX

- Bevy-related: We use Hungarian notation because we often need access to multiple representations of a single conceptual object in the same scope.
    - `comp_xxx`: A field or variable referencing a Bevy component instance
    - `qi_xxx`: A field or variable containing a Bevy `QueryItem`, which contains two or more `comp_xxx`s
    - `q_xxx`: A field or variable containing a Bevy `Query`
    - `ent_xxx`: A field or variable containing a Bevy `Entity`
    - `virt_xxx`: A field or variable containing a `simac_operator::stack::VirtEntity`

### Abbreviations

- `Cx`: Context
- `St`: State
- `i`: Index


[1]: https://doc.rust-lang.org/cargo/reference/workspaces.html
[2]: https://doc.rust-lang.org/cargo/appendix/glossary.html#lock-file
[3]: https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-flake
[4]: https://nixos.org/
[5]: https://rust-lang.github.io/rustup/overrides.html#the-toolchain-file
[6]: https://rust-lang.github.io/rustfmt/
[7]: https://github.com/rust-lang/rust-clippy/
[8]: https://en.wikipedia.org/wiki/Verilog-A
