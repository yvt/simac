//! `Vec<Option<T>>` with panicking accessors and auto-expansion
//!
//! This crate provides a `slab::Slab` counterpart of `slotmap::SecondaryMap`.

use std::{fmt, ops};

/// `Vec<Option<T>>` with panicking accessors and auto-expansion
///
/// # Examples
///
/// The index operator provides direct access to `Element`:
///
/// ```rust
/// use secondary_slab::SecondarySlab;
///
/// let mut map = SecondarySlab::new();
/// map.insert(42, "foo");
/// assert_eq!(map[42], "foo");
/// ```
///
/// Panics when an attempt is made to access a vacant slot:
///
/// ```rust,should_panic
/// # let map = secondary_slab::SecondarySlab::<()>::new();
/// dbg!(map[10]);
/// ```
///
/// Warning: The index operator can not be used to insert a new entry.
/// (This caveat was inherited from `slotmap::SecondaryMap`.)
///
/// ```rust,should_panic
/// # let mut map = secondary_slab::SecondarySlab::new();
/// map[10] = "bar";
/// ```
#[derive(Clone)]
pub struct SecondarySlab<Element> {
    inner: Vec<Option<Element>>,
}

impl<Element> SecondarySlab<Element> {
    /// Construct a new, empty `SecondarySlab`.
    pub const fn new() -> Self {
        Self { inner: Vec::new() }
    }

    /// Clear all slots by iterating over all slots.
    pub fn clear(&mut self) {
        self.inner.fill_with(|| None);
    }

    /// Get a flag indicating if it contains `index`.
    #[inline]
    pub fn contains_key(&self, index: usize) -> bool {
        self.get(index).is_some()
    }

    /// Insert a value at the given `index`.
    #[inline]
    pub fn insert(&mut self, index: usize, value: Element) -> Option<Element> {
        if let Some(slot) = self.inner.get_mut(index) {
            std::mem::replace(slot, Some(value))
        } else {
            self.insert_slow(index, value);
            None
        }
    }

    #[cold]
    fn insert_slow(&mut self, index: usize, value: Element) {
        self.inner.resize_with(index + 1, || None);
        self.inner[index] = Some(value);
    }

    /// Remove the value at the given `index`, returning it if it was not
    /// previously removed.
    #[inline]
    pub fn remove(&mut self, index: usize) -> Option<Element> {
        self.inner.get_mut(index)?.take()
    }

    /// Get a reference to the value corresponding to the index.
    #[inline]
    pub fn get(&self, index: usize) -> Option<&Element> {
        self.inner.get(index)?.as_ref()
    }

    /// Get a mutable reference to the value corresponding to the index.
    #[inline]
    pub fn get_mut(&mut self, index: usize) -> Option<&mut Element> {
        self.inner.get_mut(index)?.as_mut()
    }

    /// Get an iterator producing references to elements along with their
    /// indices.
    #[inline]
    pub fn iter(&self) -> Iter<'_, Element> {
        self.into_iter()
    }

    /// Get an iterator producing mutable references to elements along with
    /// their indices.
    #[inline]
    pub fn iter_mut(&mut self) -> IterMut<'_, Element> {
        self.into_iter()
    }
}

impl<Element> Default for SecondarySlab<Element> {
    fn default() -> Self {
        Self::new()
    }
}

impl<Element> Extend<(usize, Element)> for SecondarySlab<Element> {
    fn extend<Iter: IntoIterator<Item = (usize, Element)>>(&mut self, iter: Iter) {
        for (index, value) in iter {
            self.insert(index, value);
        }
    }
}

impl<Element> ops::Index<usize> for SecondarySlab<Element> {
    type Output = Element;

    #[inline]
    #[track_caller]
    fn index(&self, index: usize) -> &Self::Output {
        self.get(index).expect("empty slot")
    }
}

impl<Element> ops::IndexMut<usize> for SecondarySlab<Element> {
    #[inline]
    #[track_caller]
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.get_mut(index).expect("empty slot")
    }
}

impl<Element: fmt::Debug> fmt::Debug for SecondarySlab<Element> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map().entries(self.iter()).finish()
    }
}

impl<'a, Element> IntoIterator for &'a SecondarySlab<Element> {
    type Item = (usize, &'a Element);
    type IntoIter = Iter<'a, Element>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        Iter {
            inner: self.inner.iter().enumerate(),
        }
    }
}

impl<'a, Element> IntoIterator for &'a mut SecondarySlab<Element> {
    type Item = (usize, &'a mut Element);
    type IntoIter = IterMut<'a, Element>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        IterMut {
            inner: self.inner.iter_mut().enumerate(),
        }
    }
}

impl<Element> IntoIterator for SecondarySlab<Element> {
    type Item = (usize, Element);
    type IntoIter = IntoIter<Element>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            inner: self.inner.into_iter().enumerate(),
        }
    }
}

pub struct Iter<'a, Element> {
    inner: std::iter::Enumerate<std::slice::Iter<'a, Option<Element>>>,
}

impl<'a, Element> Iterator for Iter<'a, Element> {
    type Item = (usize, &'a Element);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        for (i, slot) in self.inner.by_ref() {
            if let Some(value) = slot {
                return Some((i, value));
            }
        }
        None
    }
}

pub struct IterMut<'a, Element> {
    inner: std::iter::Enumerate<std::slice::IterMut<'a, Option<Element>>>,
}

impl<'a, Element> Iterator for IterMut<'a, Element> {
    type Item = (usize, &'a mut Element);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        for (i, slot) in self.inner.by_ref() {
            if let Some(value) = slot {
                return Some((i, value));
            }
        }
        None
    }
}

pub struct IntoIter<Element> {
    inner: std::iter::Enumerate<std::vec::IntoIter<Option<Element>>>,
}

impl<Element> Iterator for IntoIter<Element> {
    type Item = (usize, Element);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        for (i, slot) in self.inner.by_ref() {
            if let Some(value) = slot {
                return Some((i, value));
            }
        }
        None
    }
}
