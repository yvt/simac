use base64::prelude::{BASE64_STANDARD, Engine as _};
use std::{fmt::Write as _, path::PathBuf};

use anyhow::{Context, Result};
use itertools::Itertools;
use rgb::RGB8;
use serde::de;

fn main() {
    let mut out = String::with_capacity(1 << 12);

    translate_jscm(&mut out, "src/potential_light.jscm", "POTENTIAL_LIGHT")
        .expect("translate_jscm");

    let out_dir = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());
    let out_file = out_dir.join("gen.rs");
    std::fs::write(out_file, out).expect("failed to write to `gen.rs`");
}

fn translate_jscm(out: &mut String, jscm_path: &str, name: &str) -> Result<()> {
    (|| -> Result<()> {
        // Read and parse `jscm_path`
        println!("cargo::rerun-if-changed={jscm_path}");
        let jscm = std::fs::read(jscm_path)?;
        let Jscm {
            colors: JscmColors(colors),
            usage_hints,
        } = serde_json::from_slice(&jscm)?;

        // Document the constant
        writeln!(out, "/// `{jscm_path}`").unwrap();
        writeln!(out, "///").unwrap();
        writeln!(out, "/// Usage hints: `{usage_hints:?}`").unwrap();

        writeln!(
            out,
            "#[cfg_attr(doc, doc = r##\"\n\n{}\"##)]",
            colors_to_image(&colors)
        )
        .unwrap();

        // Output a constant named `name`
        writeln!(out, "pub const {name}: &[RGB8; {}] = &[", colors.len()).unwrap();
        for RGB8 { r, g, b } in colors {
            write!(out, "RGB8::new({r}, {g}, {b}), ").unwrap();
        }
        out.push_str("\n];\n\n");
        Ok(())
    })()
    .with_context(|| format!("failed to translate {jscm_path:?}"))
}

fn colors_to_image(colors: &[RGB8]) -> String {
    let mut png: Vec<u8> = Vec::with_capacity(1 << 12);
    let mut encoder = png::Encoder::new(&mut png, colors.len() as _, 1);
    encoder.set_color(png::ColorType::Rgb);
    let mut writer = encoder.write_header().unwrap();
    writer
        .write_image_data(rgb::bytemuck::cast_slice(colors))
        .unwrap();
    writer.finish().unwrap();

    let png = BASE64_STANDARD.encode(png);

    let width = colors.len();
    let height = 20;
    format!(r#"<img src="data:image/svg+xml;base64,{png}" width={width} height={height}>"#)
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
struct Jscm {
    colors: JscmColors,
    usage_hints: Vec<String>,
}

#[derive(Debug)]
struct JscmColors([RGB8; 256]);

impl<'de> de::Deserialize<'de> for JscmColors {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let colors_str = String::deserialize(deserializer)?;

        if !colors_str.bytes().all(|b| b.is_ascii_hexdigit()) {
            return Err(de::Error::invalid_value(
                de::Unexpected::Str(&colors_str),
                &"string matching /[0-9a-fA-F]/",
            ));
        }

        let mut colors_str = &colors_str[..];
        let colors = std::iter::from_fn(|| {
            // Break `colors_str` into chunks of 6 bytes
            let color_str;
            (color_str, colors_str) = colors_str.split_at_checked(6)?;

            // `rrggbb` -> `Rgb { r: 0xrr, g: 0xgg, b: 0xbb }`
            let rgb = u32::from_str_radix(color_str, 16).unwrap();
            let [b, g, r, _] = rgb.to_le_bytes();
            Some(RGB8 { r, g, b })
        })
        .collect_array()
        .ok_or_else(|| {
            de::Error::invalid_value(de::Unexpected::Str(colors_str), &"string of correct length")
        })?;

        Ok(Self(colors))
    }
}
