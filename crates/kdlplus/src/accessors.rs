use super::*;

#[macropol::macropol]
macro_rules! define_accessor {
    (
        $Ty:ident::$children:ident =>
        $ETy:ident::$siblings:ident in Arena::$map:ident
    ) => {
        paste::paste! {
            impl $Ty {
                /// Create a linked list accessor for [`Self::$&children`].
                ///
                /// # Example
                ///
                /// ```rust
                /// use kdlplus::{Arena, $&Ty};
                ///
                /// fn $&{$children}_count(parent: &$&Ty, arena: &Arena) -> usize {
                ///     parent.$&children(&arena.$&map).iter().count()
                /// }
                /// ```
                pub fn $children<'a>(
                    &'a self,
                    $map: &'a [< $ETy Arena >],
                ) -> intrusive_list::accessor::ListAccessor<
                    'a,
                    [< $ETy I >],
                    [< $ETy Arena >],
                    impl Fn(&$ETy) -> &Option<intrusive_list::Link<[< $ETy I >]>> + use<>,
                > {
                    self.$children.accessor($map, |x| &x.$siblings)
                }

                /// Create a mutable linked list accessor for
                /// [`Self::$&children`].
                pub fn [< $children _mut >]<'a>(
                    &'a mut self,
                    $map: &'a mut [< $ETy Arena >],
                ) -> intrusive_list::accessor_mut::ListAccessorMut<
                    'a,
                    [< $ETy I >],
                    [< $ETy Arena >],
                    impl FnMut(&mut $ETy) -> &mut Option<intrusive_list::Link<[< $ETy I >]>> + use<>,
                > {
                    self.$children.accessor_mut($map, |x| &mut x.$siblings)
                }
            }
        }
    };
}

define_accessor!(Document::nodes => Node::sibling_nodes in Arena::nodes);
define_accessor!(Node::entries => Entry::sibling_entries in Arena::entries);
