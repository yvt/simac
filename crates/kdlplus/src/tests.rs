use super::*;
use anyhow::{Context as _, Result};
use proptest::prelude::*;
use simac_test_utils::init;
use std::{fmt, path::PathBuf, sync::OnceLock};

#[derive(Clone)]
struct Case {
    kdl_path: PathBuf,
    kdl_text: &'static str,
}

impl Case {
    fn all() -> &'static [Self] {
        static VALUE: OnceLock<Vec<Case>> = OnceLock::new();
        VALUE.get_or_init(|| {
            (|| {
                let path = "../../vendor/kdl-rs/tests/test_cases/input";
                std::fs::read_dir(path)
                    .with_context(|| format!("failed to read directory {path:?}"))?
                    .map(|entry| {
                        let entry = entry.with_context(|| {
                            format!("failed to read directory entry in {path:?}")
                        })?;

                        let kdl_path = entry.path();
                        let kdl_text = std::fs::read_to_string(&kdl_path)
                            .with_context(|| format!("failed to read {kdl_path:?}"))?;

                        Ok(Case {
                            kdl_path,
                            kdl_text: kdl_text.leak(),
                        })
                    })
                    .filter(|x| x.as_ref().map_or(true, |case| case.try_parse().is_some()))
                    .collect::<Result<Vec<Case>>>()
            })()
            .expect("failed to read test cases")
        })
    }

    fn try_parse(&self) -> Option<KdlDocument> {
        match self.kdl_text.parse() {
            Ok(x) => Some(x),
            Err(e) => {
                eprintln!("{:?}: parsing failed, skipping: {e}", self.kdl_path);
                None
            }
        }
    }

    fn parse(&self) -> KdlDocument {
        self.try_parse().unwrap()
    }
}

impl fmt::Debug for Case {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.kdl_path.fmt(f)
    }
}

impl Arbitrary for Case {
    type Parameters = ();

    type Strategy = prop::sample::Select<Self>;

    fn arbitrary_with((): Self::Parameters) -> Self::Strategy {
        prop::sample::select(Self::all())
    }
}

fn for_each_test_case(f: &mut dyn FnMut(KdlDocument) -> Result<()>) -> Result<()> {
    for case in Case::all() {
        f(case.parse()).with_context(|| format!("test case {case:?}"))?;
    }
    Ok(())
}

#[test]
fn roundtrip() -> Result<()> {
    for_each_test_case(&mut |kdl_doc| {
        // Perform roundtrip conversion
        let mut arena = Arena::default();
        let doc_i = arena.import_kdl_document(&kdl_doc);
        arena
            .validate()
            .context("validation failed for imported document")?;
        let kdl_doc2 = arena.export_kdl_document(doc_i);

        anyhow::ensure!(
            kdl_doc == kdl_doc2,
            "roundtrip conversion is lossy:\n\n\
            before = {kdl_doc:?}\n\n\
            after = {kdl_doc2:?}\n\n\
            arena = {arena:?}"
        );

        Ok(())
    })
}

proptest! {
    #[test]
    fn pt_remove_node(case: Case, index: prop::sample::Index) {
        init();

        let kdl_doc = case.parse();
        let mut arena = Arena::default();
        let root_document_i = arena.import_kdl_document(&kdl_doc);

        tracing::debug!(?arena);

        // Choose the node to remove
        prop_assume!(!arena.nodes.is_empty());
        let all_nodes: Vec<_> = arena.nodes.keys().collect();
        let node_i = *index.get(&all_nodes);
        tracing::debug!(?node_i, "Will remove this node");

        // Remove it in `arena`
        arena.remove_node_and_subtree(node_i);

        tracing::debug!(?arena, "Post-removal");
        arena
            .validate()
            .context("validation failed for modified document")
            .map_err(|e| TestCaseError::reject(format!("{e:?}")))?;

        // The node should be gone
        prop_assert!(!arena.nodes.contains_key(node_i));

        // Should not create orphan documents
        prop_assert!(arena.documents
            .iter()
            .all(|(i, document)| document.parent_node_i.is_some() || i == root_document_i));
    }
}

proptest! {
    #[test]
    fn pt_remove_document(case: Case, index: prop::sample::Index) {
        init();

        let kdl_doc = case.parse();
        let mut arena = Arena::default();
        let root_document_i = arena.import_kdl_document(&kdl_doc);

        tracing::debug!(?arena);

        // Choose the document to remove
        let all_documents: Vec<_> = arena.documents.keys().collect();
        let document_i = *index.get(&all_documents);
        tracing::debug!(?document_i, "Will remove this document");

        // Remove it in `arena`
        arena.remove_document_and_subtree(document_i);

        tracing::debug!(?arena, "Post-removal");
        arena
            .validate()
            .context("validation failed for modified document")
            .map_err(|e| TestCaseError::reject(format!("{e:?}")))?;

        // The document should be gone
        prop_assert!(!arena.documents.contains_key(document_i));

        // Should not create orphan node
        prop_assert!(arena.nodes
            .values()
            .all(|node| node.parent_document_i.is_some()));

        // If the root document is removed, `arena` should be empty now
        if root_document_i == document_i {
            prop_assert!(arena.nodes.is_empty());
            prop_assert!(arena.documents.is_empty());
            prop_assert!(arena.entries.is_empty());
        }
    }
}
