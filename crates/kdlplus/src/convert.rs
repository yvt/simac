use super::*;

impl Arena {
    pub fn import_kdl_document(&mut self, k_document: &KdlDocument) -> DocumentI {
        let format = k_document.format().cloned();
        let span = k_document.span();

        let mut nodes = intrusive_list::Head::default();
        for node in k_document.nodes() {
            let node_i = self.import_kdl_node(node);
            nodes
                .accessor_mut(&mut self.nodes, |x| &mut x.sibling_nodes)
                .push_back(node_i);
        }

        let document_i = self.documents.insert(Document {
            nodes,
            format,
            span,
            parent_node_i: None,
        });

        // Set the children's `Entry::parent_document_i`
        nodes
            .accessor_mut(&mut self.nodes, |x| &mut x.sibling_nodes)
            .for_each_mut(|_, x| {
                x.parent_document_i = Some(document_i);
                std::ops::ControlFlow::<()>::Continue(())
            });

        document_i
    }

    pub fn import_kdl_node(&mut self, k_node: &KdlNode) -> NodeI {
        let ty = k_node.ty().cloned();
        let name = k_node.name().clone();
        let format = k_node.format().cloned();
        let span = k_node.span();

        let mut entries = intrusive_list::Head::default();
        for entry in k_node.entries() {
            let entry_i = self.import_kdl_entry(entry);
            entries
                .accessor_mut(&mut self.entries, |x| &mut x.sibling_entries)
                .push_back(entry_i);
        }

        let children = k_node
            .children()
            .map(|document| self.import_kdl_document(document));

        let node_i = self.nodes.insert(Node {
            ty,
            name,
            entries,
            children,
            format,
            span,
            parent_document_i: None,
            sibling_nodes: None,
        });

        // Set the children's `Entry::parent_node_i`
        entries
            .accessor_mut(&mut self.entries, |x| &mut x.sibling_entries)
            .for_each_mut(|_, x| {
                x.parent_node_i = Some(node_i);
                std::ops::ControlFlow::<()>::Continue(())
            });

        if let Some(child_document_i) = children {
            self.documents[child_document_i].parent_node_i = Some(node_i);
        }

        node_i
    }

    pub fn import_kdl_entry(&mut self, k_entry: &KdlEntry) -> EntryI {
        let ty = k_entry.ty().cloned();
        let value = k_entry.value().clone();
        let name = k_entry.name().map(<_>::to_owned);
        let format = k_entry.format().cloned();
        let span = k_entry.span();

        self.entries.insert(Entry {
            ty,
            value,
            name,
            format,
            span,
            parent_node_i: None,
            sibling_entries: None,
        })
    }

    pub fn export_kdl_document(&self, document_i: DocumentI) -> KdlDocument {
        let Document {
            nodes,
            format,
            span,
            parent_node_i: _,
        } = &self.documents[document_i];

        let mut k_document = KdlDocument::new();
        k_document.nodes_mut().extend(
            nodes
                .accessor(&self.nodes, |x| &x.sibling_nodes)
                .iter()
                .map(|(i, _)| self.export_kdl_node(i)),
        );
        if let Some(x) = format {
            k_document.set_format(x.clone());
        }
        k_document.set_span(*span);

        k_document
    }

    pub fn export_kdl_node(&self, node_i: NodeI) -> KdlNode {
        let Node {
            ty,
            name,
            entries,
            children,
            format,
            span,
            parent_document_i: _,
            sibling_nodes: _,
        } = &self.nodes[node_i];

        let mut k_node = KdlNode::new(name.clone());
        if let Some(x) = ty {
            k_node.set_ty(x.clone());
        }
        k_node.entries_mut().extend(
            entries
                .accessor(&self.entries, |x| &x.sibling_entries)
                .iter()
                .map(|(i, _)| self.export_kdl_entry(i)),
        );
        if let Some(children) = *children {
            k_node.set_children(self.export_kdl_document(children));
        }
        if let Some(x) = format {
            k_node.set_format(x.clone());
        }
        k_node.set_span(*span);

        k_node
    }

    pub fn export_kdl_entry(&self, entry_i: EntryI) -> KdlEntry {
        let Entry {
            ty,
            value,
            name,
            format,
            span,
            parent_node_i: _,
            sibling_entries: _,
        } = &self.entries[entry_i];

        let mut k_entry = if let Some(name) = name {
            KdlEntry::new_prop(name.clone(), value.clone())
        } else {
            KdlEntry::new(value.clone())
        };

        if let Some(x) = ty {
            k_entry.set_ty(x.clone());
        }
        if let Some(x) = format {
            k_entry.set_format(x.clone());
        }
        k_entry.set_span(*span);

        k_entry
    }
}
