use super::*;

impl Arena {
    /// Remove the specified [`Node`] and all child objects from `self`.
    pub fn remove_node_and_subtree(&mut self, node_i: NodeI) {
        if let Some(document_i) = self.nodes[node_i].parent_document_i {
            self.documents[document_i]
                .nodes_mut(&mut self.nodes)
                .remove(node_i);
        }

        self.remove_orphan_node_and_subtree(node_i);
    }

    /// Remove the specified [`Document`] and all child objects from `self`.
    pub fn remove_document_and_subtree(&mut self, document_i: DocumentI) {
        if let Some(node_i) = self.documents[document_i].parent_node_i {
            self.nodes[node_i].children = None;
        }

        self.remove_orphan_document_and_subtree(document_i);
    }

    fn remove_orphan_node_and_subtree(&mut self, node_i: NodeI) {
        let mut node = self.nodes.remove(node_i).expect("invalid `NodeI`");

        while let Some(entry_i) = node.entries_mut(&mut self.entries).pop_front() {
            self.entries.remove(entry_i);
        }

        if let Some(document_i) = node.children {
            self.remove_orphan_document_and_subtree(document_i);
        }
    }

    fn remove_orphan_document_and_subtree(&mut self, document_i: DocumentI) {
        let mut document = self
            .documents
            .remove(document_i)
            .expect("invalid `DocumentI`");

        while let Some(node_i) = document.nodes_mut(&mut self.nodes).pop_front() {
            self.remove_orphan_node_and_subtree(node_i);
        }
    }
}

impl Document {
    /// Get the first child node with a matching name.
    pub fn get<'a>(&'a self, nodes: &'a NodeArena, name: &str) -> Option<(NodeI, &'a Node)> {
        self.nodes(nodes)
            .iter()
            .find(|(_, n)| n.name.value() == name)
    }
}

impl Entry {
    /// Reset this entry's value to its default representation.
    ///
    /// Call this method after modifying [`Self::value`].
    pub fn reset_value_repr(&mut self) {
        if let Some(format) = &mut self.format {
            format.value_repr = self.value.to_string();
        }
    }
}
