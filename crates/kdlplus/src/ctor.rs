//! DOM node construction
use super::*;

impl Document {
    pub fn new() -> Self {
        Self {
            nodes: <_>::default(),
            format: None,
            span: SourceSpan::from(0..0),
            parent_node_i: None,
        }
    }
}

impl Default for Document {
    fn default() -> Self {
        Self::new()
    }
}

impl Node {
    /// Construct an orphan `Self` with a given name.
    pub fn new(name: impl Into<KdlIdentifier>) -> Self {
        Self {
            name: name.into(),
            ty: None,
            entries: <_>::default(),
            children: None,
            format: None,
            span: SourceSpan::from(0..0),
            parent_document_i: None,
            sibling_nodes: None,
        }
    }
}

impl Entry {
    /// Construct an orphan, argument `Self`.
    pub fn new(value: impl Into<KdlValue>) -> Self {
        Self {
            ty: None,
            value: value.into(),
            name: None,
            format: None,
            span: SourceSpan::from(0..0),
            parent_node_i: None,
            sibling_entries: None,
        }
    }
}
