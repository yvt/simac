#![doc = include_str!("../README.md")]
use kdl::{
    KdlDocument, KdlDocumentFormat, KdlEntry, KdlEntryFormat, KdlIdentifier, KdlNode,
    KdlNodeFormat, KdlValue,
};
use miette::SourceSpan;
use slotmap::{SlotMap, new_key_type};

mod accessors;
mod convert;
mod ctor;
mod ops;
mod validate;

#[cfg(test)]
mod tests;

new_key_type! {
    /// The index type for documents in [`Arena`].
    pub struct DocumentI;

    /// The index type for nodes in [`Arena`].
    pub struct NodeI;

    /// The index type for entries in [`Arena`].
    pub struct EntryI;
}

#[derive(Default, Debug, Clone)]
pub struct Arena {
    pub documents: DocumentArena,
    pub nodes: NodeArena,
    pub entries: EntryArena,
}

/// [`SlotMap`]`<`[`DocumentI`]`, `[`Document`]`>`
pub type DocumentArena = SlotMap<DocumentI, Document>;

/// [`SlotMap`]`<`[`NodeI`]`, `[`Node`]`>`
pub type NodeArena = SlotMap<NodeI, Node>;

/// [`SlotMap`]`<`[`EntryI`]`, `[`Entry`]`>`
pub type EntryArena = SlotMap<EntryI, Entry>;

/// The `kdlplus` counterpart of [`KdlDocument`], stored in [`Arena`].
#[derive(Debug, Clone)]
pub struct Document {
    /// The linked list containing child [`Node`]s, linked by
    /// [`Node::sibling_nodes`].
    /// [`Node::parent_document_i`] is the reverse.
    pub nodes: intrusive_list::Head<NodeI>,
    pub format: Option<KdlDocumentFormat>,
    pub span: SourceSpan,

    /// The [`Node`] that `self` represents the children of.
    /// See [`Node::children`].
    pub parent_node_i: Option<NodeI>,
}

/// The `kdlplus` counterpart of [`KdlNode`], stored in [`Arena`].
#[derive(Debug, Clone)]
pub struct Node {
    pub ty: Option<KdlIdentifier>,
    pub name: KdlIdentifier,
    /// The linked list containing child [`Entry`]s, linked by
    /// [`Entry::sibling_entries`].
    /// [`Entry::parent_node_i`] is the reverse.
    pub entries: intrusive_list::Head<EntryI>,
    /// The [`Document`] representing the children of `self`.
    /// [`Document::parent_node_i`] is the reverse.
    pub children: Option<DocumentI>,
    pub format: Option<KdlNodeFormat>,
    pub span: SourceSpan,

    /// The index of the [`Document`] containing `self`.
    pub parent_document_i: Option<DocumentI>,
    /// The sibling items in [`Document::nodes`] of the [`Document`]
    /// pointed to by [`Self::parent_document_i`].
    ///
    /// Must be `Some(_)` if and only if [`Self::parent_document_i`] is.
    pub sibling_nodes: Option<intrusive_list::Link<NodeI>>,
}

/// The `kdlplus` counterpart of [`KdlEntry`], stored in [`Arena`].
#[derive(Debug, Clone)]
pub struct Entry {
    pub ty: Option<KdlIdentifier>,
    pub value: KdlValue,
    pub name: Option<KdlIdentifier>,
    pub format: Option<KdlEntryFormat>,
    pub span: SourceSpan,

    /// The index of the [`Node`] containing `self`.
    pub parent_node_i: Option<NodeI>,
    /// The sibling items in [`Node::entries`] of the [`Node`]
    /// pointed to by [`Self::parent_node_i`].
    ///
    /// Must be `Some(_)` if and only if [`Self::parent_node_i`] is.
    pub sibling_entries: Option<intrusive_list::Link<EntryI>>,
}
