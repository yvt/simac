use super::*;
use anyhow::{Context, Result};
use slotmap::SecondaryMap;

struct ValidationCx<'a> {
    arena: &'a Arena,
    documents: SecondaryMap<DocumentI, ()>,
    nodes: SecondaryMap<NodeI, NodeInfo>,
    entries: SecondaryMap<EntryI, ()>,
}

#[derive(Default)]
struct NodeInfo {
    visiting: bool,
}

impl Arena {
    /// Check the structural integrity of all objects in `self`.
    pub fn validate(&self) -> Result<()> {
        let mut cx = ValidationCx {
            arena: self,
            documents: SecondaryMap::with_capacity(self.documents.capacity()),
            nodes: SecondaryMap::with_capacity(self.nodes.capacity()),
            entries: SecondaryMap::with_capacity(self.entries.capacity()),
        };

        for (document_i, document) in self.documents.iter() {
            if document.parent_node_i.is_none() {
                cx.visit_document(document_i)
                    .with_context(|| format!("visiting document {document_i:?}"))?;
            }
        }

        for (node_i, node) in self.nodes.iter() {
            if node.parent_document_i.is_none() {
                cx.visit_node(node_i)
                    .with_context(|| format!("visiting node {node_i:?}"))?;
            }
        }

        for document_i in self.documents.keys() {
            anyhow::ensure!(
                cx.documents.contains_key(document_i),
                "non-root document {document_i:?} is unreachable from root items"
            );
        }

        for node_i in self.nodes.keys() {
            anyhow::ensure!(
                cx.nodes.contains_key(node_i),
                "non-root node {node_i:?} is unreachable from root items"
            );
        }

        for (entry_i, entry) in self.entries.iter() {
            (|| {
                anyhow::ensure!(
                    entry.parent_node_i.is_none() || cx.entries.contains_key(entry_i),
                    "entry has parent but is unreachable from root items"
                );

                Ok(())
            })()
            .with_context(|| format!("visiting entry {entry_i:?}"))?;
        }

        Ok(())
    }
}

impl ValidationCx<'_> {
    fn visit_document(&mut self, document_i: DocumentI) -> Result<()> {
        match self.documents.entry(document_i).unwrap() {
            slotmap::secondary::Entry::Occupied(_) => return Ok(()),
            slotmap::secondary::Entry::Vacant(entry) => {
                entry.insert(());
            }
        }

        let document = self
            .arena
            .documents
            .get(document_i)
            .context("invalid `DocumentI`")?;

        let nodes = document.nodes(&self.arena.nodes);
        nodes
            .validate()
            .context("failed to validate `Document::nodes`")?;
        for (i, (node_i, node)) in nodes.iter().enumerate() {
            let name = &node.name;

            (|| {
                anyhow::ensure!(node.parent_document_i == Some(document_i));

                self.visit_node(node_i)
            })()
            .with_context(|| format!("visiting child node {i} ({node_i:?}, name {name:?})"))?;
        }

        Ok(())
    }

    fn visit_node(&mut self, node_i: NodeI) -> Result<()> {
        let node = self.arena.nodes.get(node_i).context("invalid `NodeI`")?;

        match self.nodes.entry(node_i).unwrap() {
            slotmap::secondary::Entry::Occupied(entry) => {
                anyhow::ensure!(
                    !entry.get().visiting,
                    "circular parent-child relationship found",
                );

                return Ok(());
            }
            slotmap::secondary::Entry::Vacant(entry) => {
                entry.insert(NodeInfo { visiting: true });
            }
        }

        let entries = node.entries(&self.arena.entries);
        entries
            .validate()
            .context("failed to validate `Node::entries`")?;
        for (i, (entry_i, entry)) in entries.iter().enumerate() {
            let name = &entry.name;

            (|| {
                anyhow::ensure!(entry.parent_node_i == Some(node_i));

                self.entries.insert(entry_i, ());

                Ok(())
            })()
            .with_context(|| format!("visiting child entry {i} ({entry_i:?}, name {name:?})"))?;
        }

        if let Some(document_i) = node.children {
            (|| {
                if let Some(document) = self.arena.documents.get(document_i) {
                    anyhow::ensure!(document.parent_node_i == Some(node_i));
                }

                self.visit_document(document_i)
            })()
            .with_context(|| format!("visiting child document ({document_i:?})"))?;
        }

        self.nodes[node_i].visiting = false;

        Ok(())
    }
}
