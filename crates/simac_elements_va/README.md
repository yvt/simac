# `simac_elements_va`

Verilog-A to `simac_elements::ElemTy` compiler

Used by [`simac_elements`](../simac_elements)'s build script to compile Verilog-A models.


## Standard Compliance

This compiler uses [OpenVAF-Reloaded][1], a community fork of OpenVAF as its frontend to compile Verilog-A code down to intermediate code.
OpenVAF [implements][2] all features of the [Verilog-AMS Language Reference Manual 2.4.0][3] language standard required by standard compact models.
However, the actual support is limited by the compiler backend (this crate), the device model interface (`simac_elements`), and the simulator itself (`simac_simulator`).

Notable missing features of this compiler include (but not limited to):

- `[ref:va_todo_print]` Printing
- `[ref:va_todo_nontrivial_default_param_vals]` Module default parameter values dependent on others
- `[ref:va_todo_abstime]` Absolute time access
- [OpenVAF][4]: `$discontinuity(x)` where `x != -1`
- `[ref:va_todo_node_collapse]` Node collapse


## Command-Line Tool

The `va2elem` example program compiles a provided Verilog-A source file.
Example:

```console
$ cargo run --example va2elem --features clap -- crates/simac_elements/va/diode.va | rustfmt --config format_strings=true
       ⋮
pub struct Diode;
impl<Cx: crate::SimCx> crate::ElemTrait<Cx> for Diode {
    fn nodes(&self) -> &[crate::Node] {
       ⋮
```

> Note: A large model may cause `rustfmt` to overflow the stack and crash.
> Try increasing the default stack size (e.g., `ulimit -s 20000` on Linux) if this happens.


## References

`[tag:ramsey22]` N. Ramsey, “Beyond Relooper: Recursive Translation Of Unstructured Control Flow to Structured Control Flow (Functional Pearl),” Proceedings of the ACM on Programming Languages 6.ICFP (2022): 1-22.

`[tag:hecht72]` M. S. Hecht, J. D. Ullman. 1972. “Flow Graph Beducibility,” Proceedings of the Fourth Annual ACM Symposium on Theory of Computing (STOC '72). Association for Computing Machinery, New York, NY, USA, 238–250. doi:[10.1145/800152.804919](https://doi.org/10.1145/800152.804919)

`[tag:janssen97]` J. Janssen, H. Corporaal. 1997. “Making Graphs Reducible with Controlled Node Splitting,” ACM Transactions on Programming Languages and Systems, 19(6):1031–1052.

[1]: https://github.com/OpenVAF/OpenVAF-Reloaded
[2]: https://openvaf.semimod.de/docs/details/verilog-a-standard/
[3]: https://www.accellera.org/images/downloads/standards/v-ams/VAMS-LRM-2-4.pdf
[4]: https://github.com/OpenVAF/OpenVAF-Reloaded/blob/802bce21ac44833b52c010cb90d37e0d6fddd4ac/openvaf/hir_lower/src/expr.rs#L667
