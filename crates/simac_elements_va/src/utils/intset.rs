use std::{fmt, mem::replace};

/// A set of `usize` backed by a singly-linked list built atop `Vec`.
///
/// The memory requirement is `O(max_key)`.
pub(crate) struct SlUsizeSet {
    entries: Vec<SlUsizeSetEntry>,
    first_entry_i: usize,
}

#[derive(Debug, Clone, Copy)]
struct SlUsizeSetEntry {
    next: usize,
}

const NEXT_UNLINKED: usize = usize::MAX;
const NEXT_LAST: usize = usize::MAX - 1;

impl Default for SlUsizeSet {
    fn default() -> Self {
        Self::with_capacity(0)
    }
}

impl SlUsizeSet {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            entries: (0..capacity)
                .map(|_| SlUsizeSetEntry {
                    next: NEXT_UNLINKED,
                })
                .collect(),
            first_entry_i: NEXT_LAST,
        }
    }

    /// Insert a value to the set.
    pub fn push(&mut self, key: usize) -> bool {
        self.ensure_entry_present(key);
        if self.entries[key].next == NEXT_UNLINKED {
            self.entries[key].next = self.first_entry_i;
            self.first_entry_i = key;
            true
        } else {
            false
        }
    }

    #[expect(dead_code)]
    pub fn contains(&self, key: usize) -> bool {
        self.entries
            .get(key)
            .is_some_and(|e| e.next != NEXT_UNLINKED)
    }

    /// Remove the last inserted value from the set.
    pub fn pop(&mut self) -> Option<usize> {
        let entry_i = self.first_entry_i;
        let entry = self.entries.get_mut(entry_i)?;
        self.first_entry_i = entry.next;
        entry.next = NEXT_UNLINKED;
        Some(entry_i)
    }

    /// Empty the set.
    #[expect(dead_code)]
    pub fn clear(&mut self) {
        while self.pop().is_some() {}
    }

    pub fn iter(&self) -> impl Iterator<Item = usize> {
        let mut cur = self.first_entry_i;
        std::iter::from_fn(move || {
            let next = self.entries.get(cur)?.next;
            Some(replace(&mut cur, next))
        })
    }

    fn ensure_entry_present(&mut self, key: usize) {
        self.entries
            .resize_with((key + 1).max(self.entries.len()), || SlUsizeSetEntry {
                next: NEXT_UNLINKED,
            });
    }
}

impl Extend<usize> for SlUsizeSet {
    fn extend<T: IntoIterator<Item = usize>>(&mut self, iter: T) {
        for e in iter {
            self.push(e);
        }
    }
}

impl fmt::Debug for SlUsizeSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_set().entries(self.iter()).finish()
    }
}
