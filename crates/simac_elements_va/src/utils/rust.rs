//! Rust code generation
use std::fmt::Display;

use fn_formats::DisplayFmt;
use proc_macro2::Literal;

pub const TY_REAL: &str = "Cx::Real";

/// Construct an expression to cast `x`, an expression of a numeric type, to
/// `Cx::Real`.
pub(crate) fn real(x: impl Display) -> impl Display {
    DisplayFmt(move |f| write!(f, "<Cx::Real as num_traits::NumCast>::from({x}).unwrap()"))
}

/// Construct an expression evaluating to the zero value of `Cx::Real`.
pub const REAL_ZERO: &str = "<Cx::Real as num_traits::Zero>::zero()";

/// Construct an expression evaluating to a given `&str` value.
pub(crate) fn lit_str(x: &str) -> impl Display + use<> {
    Literal::string(x)
}

/// Construct an expression evaluating to a given `f64` value.
pub(crate) fn lit_f64(x: f64) -> String {
    match x.classify() {
        std::num::FpCategory::Nan => format!("f64::from_bits({:#x})", x.to_bits()),
        std::num::FpCategory::Infinite => {
            ["f64::INFINITY", "f64::NEG_INFINITY"][x.is_sign_negative() as usize].to_owned()
        }
        std::num::FpCategory::Zero
        | std::num::FpCategory::Subnormal
        | std::num::FpCategory::Normal => Literal::f64_suffixed(x).to_string(),
    }
}
