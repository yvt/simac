//! Utilities for Cargo build scripts
use std::{borrow::Cow, io::ErrorKind, path::Path, time::SystemTime};

use anyhow::{Context, Result};

/// Run the Verilog-A compiler and write the output to the specified file.
///
/// To re-run the build script upon input change, this function automatically
/// outputs [`cargo::rerun-if-changed`][1] Cargo directives.
///
/// This function will exit the process with a non-zero exit status if the
/// compiler produces error diagnostics.
///
/// This function will skip compilation if the output is up-to-date.
///
/// [1]: https://doc.rust-lang.org/cargo/reference/build-scripts.html#rerun-if-changed
#[track_caller]
pub fn compile_va(out_path: &Path, opts: crate::CompileOpts) {
    // Skip if the input did not change
    let deps_path = out_path.with_extension("deps");
    match deps_is_fresh(out_path, &deps_path) {
        Ok(true) => {
            tracing::debug!(?out_path, "The output is fresh; skipping compilation");
            return;
        }
        Ok(false) => {
            tracing::debug!(
                ?out_path,
                "The output is stale; proceeding with compilation"
            )
        }
        Err(error) => {
            tracing::warn!(
                ?error,
                "Failed to determine if the output is fresh; \
                assuming it is stale and proceeding with compilation"
            )
        }
    }

    tracing::info!(?out_path, ?opts, "Compiling Verilog-A file");
    let mut sink = termcolor::Buffer::ansi();
    let out = crate::compile(opts, Some(&mut sink));

    // Output compiler diagnostics with `cargo::warning=` Cargo instruction.
    // The stdout/err of a build script are captured by Cargo, so they are not
    // presented to the user unless the build script fails.
    for line in String::from_utf8_lossy(sink.as_slice()).lines() {
        println!("cargo::warning={line}");
    }

    // Exit on error
    let Ok(out) = out else { std::process::exit(1) };

    // Rerun if the input changes
    for path in &out.input_files {
        println!("cargo::rerun-if-changed={}", path.display());
    }

    // Write the generated code
    std::fs::write(out_path, out.rust_source)
        .unwrap_or_else(|e| panic!("failed to wrote to '{}': {e}", out_path.display()));

    // Record the input file list
    let deps: Deps = out
        .input_files
        .iter()
        .map(|path| {
            path.as_os_str()
                .to_str()
                .expect("non-Unicode char in input file name")
                .into()
        })
        .collect();
    let deps = serde_json::to_string(&deps).expect("failed to serialize deps file");
    std::fs::write(&deps_path, deps)
        .unwrap_or_else(|e| panic!("failed to write to '{}': {e}", deps_path.display()));
}

/// The deserialized contents of a "deps" file.
type Deps<'a> = Vec<Cow<'a, str>>;

/// Determine if the output file is fresh using the information from the
/// specified "deps" file.
#[tracing::instrument(skip_all)]
fn deps_is_fresh(out_path: &Path, deps_path: &Path) -> Result<bool> {
    tracing::debug!(?out_path, ?deps_path);

    // Check the output file's existence and mtime
    if !out_path.is_file() {
        tracing::debug!("The output file does not exist yet");
        return Ok(false);
    }

    let out_mtime = mtime(out_path)?;
    tracing::debug!(?out_mtime, "The mtime of the output file");

    // Read the "deps" file
    let data = match std::fs::read(deps_path) {
        Err(error) if error.kind() == ErrorKind::NotFound => {
            tracing::debug!("The deps file does not exist yet");
            return Ok(false);
        }
        result => result.context("failed to read deps file")?,
    };
    let deps: Deps = serde_json::from_slice(&data).context("failed to parse deps file")?;

    // Compare to the current executable's mtime
    let exe_path = std::env::current_exe().context("failed to get current executable path")?;
    tracing::debug!(?exe_path);

    let exe_mtime = mtime(&exe_path)?;
    tracing::debug!(?exe_path, ?exe_mtime, "The mtime of the current executable");

    if exe_mtime > out_mtime {
        tracing::debug!("The current executable is newer than the output file");
        return Ok(false);
    }

    // Compare to the input files' mtimes
    for in_path in &deps {
        let in_path = Path::new(&**in_path);

        let in_mtime = mtime(in_path)?;
        tracing::debug!(?in_path, ?in_mtime, "The mtime of the input file");

        if in_mtime > out_mtime {
            tracing::debug!(
                "The input file is newer than the output file; \
                the output is stale"
            );
            return Ok(false);
        }
    }

    Ok(true)
}

fn mtime(path: &Path) -> Result<SystemTime> {
    path.metadata().and_then(|m| m.modified()).with_context(|| {
        format!(
            "failed to get modification time of file '{}'",
            path.display()
        )
    })
}
