//! Converts CFG to a Rust-like structured control flow
use slotmap::{SlotMap, new_key_type};
use std::fmt;

pub mod cfg_to_scfg;
mod rcfg_to_scfg;

pub mod eliminate_return;

#[cfg(test)]
mod tests;

/// The maximum number of successors per label.
pub const MAX_SUCC: usize = 2;

/// A structured control flow graph (tree of constructs).
#[derive(Clone)]
pub struct Scfg<L> {
    pub controls: SlotMap<ControlI, Control<L>>,
    pub root_control_i: ControlI,
}

/// A structured control-flow construct.
#[derive(Debug, Clone)]
pub enum Control<L> {
    /// A labelled block.
    Block(ControlI),
    /// A labelled loop.
    Loop(ControlI),
    If {
        /// The input basic block defining the branch condition.
        cond: L,
        arms: [ControlI; MAX_SUCC],
    },
    /// Exit a parent [`Self::Block`] or [`Self::Loop`].
    Break {
        /// Specifies the relative depth of the [`Self::Block`] to exit from.
        /// `0` signifies the closest ancestor of [`Self::Block`] or
        /// [`Self::Loop`].
        depth: usize,
    },
    /// Jump to the beginning of a parent [`Self::Loop`].
    Continue {
        /// Specifies the relative depth of the [`Self::Loop`] to jump to.
        /// `0` signifies the closest ancestor of [`Self::Block`] or
        /// [`Self::Loop`].
        depth: usize,
    },
    /// Exit the provided CFG.
    Return,
    /// Straight-line code
    ///
    /// If this `L` has two successors, this `Control` will be followed by
    /// [`Control::If`] with the matching `L`.
    /// If the two successors are not distinct, this is not guaranteed.
    ///
    /// If this `L` has zero successors, this `Control` will be followed by
    /// [`Control::Return`].
    Actions(L),
    Seq([ControlI; 2]),
}

new_key_type! {
    /// An index into [`ControlFlow::controls`].
    pub struct ControlI;
}

impl<L> Scfg<L> {
    fn for_each_label<'a>(&'a mut self, mut f: impl FnMut(&'a mut L)) {
        for control in self.controls.values_mut() {
            control.for_each_label(&mut f);
        }
    }
}

impl<L> Control<L> {
    fn for_each_label<'a>(&'a mut self, mut f: impl FnMut(&'a mut L)) {
        match self {
            Control::If { cond, arms: _ } => f(cond),
            Control::Actions(label) => f(label),
            Control::Block { .. }
            | Control::Loop { .. }
            | Control::Break { .. }
            | Control::Continue { .. }
            | Control::Return
            | Control::Seq { .. } => {}
        }
    }
}

impl<L: fmt::Debug> fmt::Debug for Scfg<L> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        struct DebugControl<'a, L> {
            scfg: &'a Scfg<L>,
            control_i: ControlI,
        }

        impl<L: fmt::Debug> fmt::Debug for DebugControl<'_, L> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                let child = |control_i| Self {
                    scfg: self.scfg,
                    control_i,
                };

                match &self.scfg.controls[self.control_i] {
                    Control::Block(control_i) => f
                        .debug_struct("Block")
                        .field("index", &self.control_i)
                        .field("child", &child(*control_i))
                        .finish(),
                    Control::Loop(control_i) => f
                        .debug_struct("Loop")
                        .field("index", &self.control_i)
                        .field("child", &child(*control_i))
                        .finish(),
                    Control::If { cond, arms } => f
                        .debug_struct("If")
                        .field("index", &self.control_i)
                        .field("cond", cond)
                        .field("arms[0]", &child(arms[0]))
                        .field("arms[1]", &child(arms[1]))
                        .finish(),
                    Control::Break { depth } => f
                        .debug_struct("Break")
                        .field("index", &self.control_i)
                        .field("depth", depth)
                        .finish(),
                    Control::Continue { depth } => f
                        .debug_struct("Continue")
                        .field("index", &self.control_i)
                        .field("depth", depth)
                        .finish(),
                    Control::Return => f
                        .debug_struct("Return")
                        .field("index", &self.control_i)
                        .finish(),
                    Control::Actions(label) => f
                        .debug_struct("Actions")
                        .field("index", &self.control_i)
                        .field("label", label)
                        .finish(),
                    Control::Seq([child0, child1]) => f
                        .debug_struct("Seq")
                        .field("index", &self.control_i)
                        .field("child0", &child(*child0))
                        .field("child1", &child(*child1))
                        .finish(),
                }
            }
        }

        DebugControl {
            scfg: self,
            control_i: self.root_control_i,
        }
        .fmt(f)
    }
}
