use arrayvec::ArrayVec;
use itertools::{chain, enumerate};
use proptest::prelude::*;
use slotmap::SecondaryMap;

use super::{Control, ControlI, MAX_SUCC, cfg_to_scfg::cfg_to_scfg};

type LabelI = usize;

/// An instruction pointer position within [`Control`].
#[derive(Debug, Clone, Copy)]
enum SubIp {
    Start,
    End,
    /// [`Control::Seq`]: Between the two children.
    Seq1,
}

/// The goal of an execution of [`super::Scfg`].
#[derive(Debug)]
enum ExecGoal {
    /// Reach [`Control::Return`].
    Return,
    /// Reach [`Control::Actions`]`(label_i)`.
    Actions(LabelI),
}

fn any_cfg() -> impl Strategy<Value = Vec<ArrayVec<usize, MAX_SUCC>>> {
    const MAX_NUM_LABELS: usize = 6;
    (1..MAX_NUM_LABELS)
        .prop_flat_map(|num_blocks| {
            prop::collection::vec(prop::bits::u32::between(0, num_blocks), num_blocks)
        })
        .prop_flat_map(|blocks| (Just(blocks), prop::bool::ANY))
        .prop_map(|(blocks, force_connect)| {
            let num_blocks = blocks.len();
            blocks
                .into_iter()
                .enumerate()
                .map(|(i, next_blocks)| {
                    chain!(
                        Some(i + 1).filter(|&i| force_connect && i < num_blocks),
                        (0..num_blocks).filter(|i| (next_blocks & (1 << i)) != 0)
                    )
                    .take(MAX_SUCC)
                    .collect()
                })
                .collect()
        })
        .prop_map(|mut cfg: Vec<ArrayVec<usize, MAX_SUCC>>| {
            // Scan for reachable labels
            let mut visited = 1_u32;
            let mut stack = Vec::with_capacity(MAX_NUM_LABELS);
            stack.push(0);
            while let Some(i) = stack.pop() {
                for &j in &cfg[i] {
                    if (visited & (1 << j)) == 0 {
                        visited |= 1 << j;
                        stack.push(j);
                    }
                }
            }

            // Remove unreachable labels
            for i in (1..cfg.len()).rev() {
                if (visited & (1 << i)) != 0 {
                    continue;
                }

                // Label `i` is unreachable
                cfg.remove(i);

                for successors in &mut cfg {
                    for j in successors {
                        *j -= (*j > i) as usize;
                    }
                }
            }

            cfg
        })
}

proptest! {
    #[test]
    fn pt_cfg_to_scfg(
        cfg in any_cfg(),
        eliminate_return: bool,
    ) {
        crate::tests::init();
        pt_cfg_to_scfg_inner(cfg, eliminate_return);
    }
}

fn pt_cfg_to_scfg_inner(labels: Vec<ArrayVec<usize, MAX_SUCC>>, eliminate_return: bool) {
    tracing::info!(?labels);
    let mut scfg = cfg_to_scfg(&labels, 0);
    tracing::info!(?scfg);

    if eliminate_return {
        super::eliminate_return::eliminate_return(&mut scfg);
        tracing::info!(?scfg, "After `eliminate_return`");

        assert!(
            scfg.controls
                .values()
                .all(|c| !matches!(c, Control::Return))
        );
    }

    // Calculate the parents of `Control`s in `scfg`
    let mut control_parent: SecondaryMap<ControlI, ControlI> = <_>::default();
    for (parent_control_i, control) in scfg.controls.iter() {
        match control {
            &Control::Block(control_i) => {
                control_parent.insert(control_i, parent_control_i);
            }
            &Control::Loop(control_i) => {
                control_parent.insert(control_i, parent_control_i);
            }
            Control::If { arms, .. } => {
                for &control_i in arms {
                    control_parent.insert(control_i, parent_control_i);
                }
            }
            Control::Seq(children) => {
                for &control_i in children {
                    control_parent.insert(control_i, parent_control_i);
                }
            }
            Control::Break { .. }
            | Control::Continue { .. }
            | Control::Return
            | Control::Actions { .. } => {}
        }
    }

    // For every `Control::Actions`...
    for (control_i, control) in scfg.controls.iter() {
        let &Control::Actions(label_i) = control else { continue };

        // Expected successors
        let successors = &labels[label_i];

        // Test patterns (branch decisions and execution goals)
        let mut cases: ArrayVec<(Option<usize>, ExecGoal), 2> = <_>::default();
        match successors[..] {
            [] => cases.push((None, ExecGoal::Return)),
            [succ_node_i] => cases.push((None, ExecGoal::Actions(succ_node_i))),
            _ => cases.extend(
                enumerate(successors)
                    .map(|(i, &succ_node_i)| (Some(i), ExecGoal::Actions(succ_node_i))),
            ),
        }

        for (mut branch_i, goal) in cases {
            tracing::info!(?control_i, label_i, branch_i, ?goal, "Starting execution");

            // Instruction pointer
            let mut ip = control_i;
            let mut subip = SubIp::End;

            loop {
                let control = &scfg.controls[ip];
                tracing::debug!(ip = ?(ip, subip), ?control);
                match (control, subip) {
                    (_, SubIp::End) => {
                        let old_ip = ip;
                        ip = if let Some(ip) = control_parent.get(ip) {
                            *ip
                        } else if eliminate_return {
                            match goal {
                                ExecGoal::Return => {
                                    tracing::debug!("Reached the end as expected");
                                    break;
                                }
                                ExecGoal::Actions(_) => panic!("unexpectedly reached end"),
                            }
                        } else {
                            panic!("program counter fell through the end of `scfg`")
                        };

                        // Find the specific return site within `ip`
                        subip = match scfg.controls[ip] {
                            Control::Loop { .. } => SubIp::Start,
                            Control::Block { .. }
                            | Control::If { .. }
                            | Control::Break { .. }
                            | Control::Continue { .. }
                            | Control::Return
                            | Control::Actions(_) => SubIp::End,
                            Control::Seq([c0, _]) => {
                                if old_ip == c0 {
                                    SubIp::Seq1
                                } else {
                                    SubIp::End
                                }
                            }
                        };
                    }
                    (&Control::If { cond, arms }, SubIp::Start) => {
                        let branch_i = branch_i.take().expect("unexpected `If`");
                        assert_eq!(
                            cond, label_i,
                            "encountered `If` that does not use the result of `label_i`"
                        );
                        tracing::debug!(branch_i, "Encountered `If` as expected");
                        ip = arms[branch_i];
                        subip = SubIp::Start;
                    }
                    (
                        &Control::Break { mut depth } | &Control::Continue { mut depth },
                        SubIp::Start,
                    ) => loop {
                        ip = *control_parent
                            .get(ip)
                            .expect("no block found at given depth");

                        if !matches!(scfg.controls[ip], Control::Block(_) | Control::Loop(_)) {
                            continue;
                        }

                        if depth != 0 {
                            depth -= 1;
                            continue;
                        }

                        match (control, &scfg.controls[ip]) {
                            (Control::Break { .. }, _) => subip = SubIp::End,
                            (Control::Continue { .. }, Control::Loop(_)) => subip = SubIp::Start,
                            _ => {
                                panic!("`Continue` is not valid for `Block`")
                            }
                        }
                        break;
                    },
                    (Control::Return, SubIp::Start) => match goal {
                        ExecGoal::Return => {
                            tracing::debug!("Encountered `Return` as expected");
                            break;
                        }
                        ExecGoal::Actions(_) => panic!("unexpected `Return`"),
                    },
                    (&Control::Actions(succ_label_i), SubIp::Start) => match goal {
                        ExecGoal::Return => panic!("unexpected `Actions`"),
                        ExecGoal::Actions(expected_succ_label_i) => {
                            assert_eq!(
                                expected_succ_label_i, succ_label_i,
                                "encountered `Actions` with incorrect label"
                            );
                            tracing::debug!(
                                "Encountered `Actions{expected_succ_label_i}` as expected"
                            );
                            break;
                        }
                    },
                    (
                        &Control::Block(control_i)
                        | &Control::Loop(control_i)
                        | &Control::Seq([control_i, _]),
                        SubIp::Start,
                    )
                    | (&Control::Seq([_, control_i]), SubIp::Seq1) => {
                        ip = control_i;
                        subip = SubIp::Start;
                    }
                    (_, SubIp::Seq1) => {
                        unreachable!("`Controlip::Seq1` is only valid for `Control::Seq`")
                    }
                }
            }
        }
    }
}
