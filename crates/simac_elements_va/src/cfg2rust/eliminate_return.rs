use super::{Control, ControlI, Scfg};

/// Wrap all code with a block and replace every [`Control::Return`] with a
/// [`Control::Break`] targetting this block.
pub fn eliminate_return<L>(scfg: &mut Scfg<L>) {
    let block_control_i = scfg.controls.insert(Control::Block(scfg.root_control_i));

    rewrite_control(scfg, scfg.root_control_i, 0);

    scfg.root_control_i = block_control_i;
}

fn rewrite_control<L>(scfg: &mut Scfg<L>, control_i: ControlI, wrapper_depth: usize) {
    match scfg.controls[control_i] {
        Control::Block(child_control_i) | Control::Loop(child_control_i) => {
            rewrite_control(scfg, child_control_i, wrapper_depth + 1)
        }
        Control::If { cond: _, arms } => {
            for child_control_i in arms {
                rewrite_control(scfg, child_control_i, wrapper_depth);
            }
        }
        Control::Break { .. } | Control::Continue { .. } | Control::Actions(_) => {}
        Control::Return => {
            scfg.controls[control_i] = Control::Break {
                depth: wrapper_depth,
            }
        }
        Control::Seq(children) => {
            for child_control_i in children {
                rewrite_control(scfg, child_control_i, wrapper_depth);
            }
        }
    }
}
