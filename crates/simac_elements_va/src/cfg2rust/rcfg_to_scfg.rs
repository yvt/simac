//! Converts a reducible CFG to a structured CFG.
use arrayvec::ArrayVec;
use itertools::{Itertools, enumerate};
use petgraph::{Direction, algo::dominators, visit::DfsPostOrder};
use slotmap::{Key, SlotMap};

use super::{Control, ControlI, MAX_SUCC, Scfg};

/// An index into `labels`.
pub type LabelI = usize;

/// An instance of [`petgraph::Graph`] used to represent a control flow graph.
type Cfg = petgraph::Graph<(), ()>;

/// The nodex index type of [`Cfg`].
///
/// This can be converted to an index within `labels` and back forth by
/// [`label_to_cfg_node`] and [`cfg_node_to_label`].
type CfgNodeI = petgraph::graph::NodeIndex<u32>;

struct LabelInfo {
    /// The label's position within `postorder_list`.
    postorder: usize,
}

struct State<'a> {
    scfg: Scfg<LabelI>,
    labels: &'a [ArrayVec<LabelI, MAX_SUCC>],
    cfg: &'a Cfg,
    /// The list of `[dominator, dominatee]`, sorted by `(dominator,
    /// dominatee.postorder)`.
    idominatees: &'a [[CfgNodeI; 2]],
    label_info_list: &'a [LabelInfo],
    cx: Vec<ContainingSyntax>,
}

#[derive(Debug, PartialEq)]
enum ContainingSyntax {
    LoopHeadedBy(CfgNodeI),
    BlockFollowedBy(CfgNodeI),
}

/// Convert a reducible CFG to a structured CFG using the algorithm described
/// by `[ref:ramsey22]`.
///
/// `labels[i]` specifies the sucessors of basic block `i`. The `LabelI` values
/// in the return value represent indices into `labels`.
#[tracing::instrument(skip_all)]
pub fn rcfg_to_scfg(labels: &[ArrayVec<LabelI, MAX_SUCC>], entry_label_i: LabelI) -> Scfg<LabelI> {
    let mut label_info_list: Vec<LabelInfo> = (0..labels.len())
        .map(|_| LabelInfo {
            postorder: 0, // set later
        })
        .collect();

    // Convert `labels` to `petgraph::Graph`
    // ------------------------------------------------------------------
    let mut cfg = Cfg::with_capacity(labels.len(), labels.len() * MAX_SUCC);

    for _ in 0..labels.len() {
        cfg.add_node(());
    }

    for (from_label_i, successors) in enumerate(labels) {
        for &to_label_i in successors {
            cfg.add_edge(
                label_to_cfg_node(from_label_i),
                label_to_cfg_node(to_label_i),
                (),
            );
        }
    }

    tracing::debug!(
        entry_label_i,
        "rCFG to convert to sCFG:\n{:?}",
        petgraph::dot::Dot::with_config(
            &cfg,
            &[
                petgraph::dot::Config::NodeIndexLabel,
                petgraph::dot::Config::EdgeNoLabel,
            ],
        ),
    );

    // Calculate some properties of the graph
    // ------------------------------------------------------------------

    // Postorder numbering
    let mut dfs = DfsPostOrder::new(&cfg, label_to_cfg_node(entry_label_i));
    let mut postorder_list = Vec::with_capacity(labels.len());

    while let Some(node_i) = dfs.next(&cfg) {
        let label_i = cfg_node_to_label(node_i);
        label_info_list[label_i].postorder = postorder_list.len();
        postorder_list.push(label_i);
    }

    tracing::debug!(?postorder_list);

    // Node -> immediate dominator
    let dominators = dominators::simple_fast(&cfg, label_to_cfg_node(entry_label_i));

    // Node -> immediate dominatees
    let idominatees: Vec<[CfgNodeI; 2]> = cfg
        .node_indices()
        .filter_map(|dominatee| {
            let idominator = dominators.immediate_dominator(dominatee)?;
            Some([idominator, dominatee])
        })
        .sorted_unstable_by_key(|&[idominator, dominatee]| {
            (
                idominator,
                label_info_list[cfg_node_to_label(dominatee)].postorder,
            )
        })
        .collect();

    let mut graph;
    tracing::debug!("Dominator tree:\n{:?}", {
        graph = cfg.clone();
        graph.clear_edges();
        for &[x, y] in &idominatees {
            graph.add_edge(x, y, ());
        }
        petgraph::dot::Dot::with_config(
            &graph,
            &[
                petgraph::dot::Config::NodeIndexLabel,
                petgraph::dot::Config::EdgeNoLabel,
            ],
        )
    },);

    // Run the main algorithm
    // ------------------------------------------------------------------
    let mut state = State {
        scfg: Scfg {
            controls: SlotMap::with_capacity_and_key(labels.len()),
            root_control_i: ControlI::null(), // set later
        },
        labels,
        idominatees: &idominatees,
        cfg: &cfg,
        label_info_list: &label_info_list,
        cx: Vec::new(),
    };

    state.scfg.root_control_i = state.translate_tree(label_to_cfg_node(entry_label_i));

    state.scfg
}

fn label_to_cfg_node(label_i: LabelI) -> CfgNodeI {
    CfgNodeI::new(label_i)
}

fn cfg_node_to_label(node: CfgNodeI) -> LabelI {
    node.index()
}

fn is_merge_node(cfg: &Cfg, node_i: CfgNodeI) -> bool {
    cfg.edges_directed(node_i, Direction::Incoming)
        // Avoid iterating over all elements of the `Edges` iterator
        .nth(1)
        .is_some()
}

fn is_loop_header(cfg: &Cfg, label_info_list: &[LabelInfo], node_i: CfgNodeI) -> bool {
    let postorder = label_info_list[cfg_node_to_label(node_i)].postorder;
    cfg.neighbors_directed(node_i, Direction::Incoming)
        .any(|other_node_i| {
            let other_postorder = label_info_list[cfg_node_to_label(other_node_i)].postorder;
            // `other_reverse_postorder >= reverse_postorder`
            other_postorder <= postorder
        })
}

impl State<'_> {
    /// Translate the subtree of the dominator tree.
    fn translate_tree(&mut self, node_i: CfgNodeI) -> ControlI {
        let label_i = cfg_node_to_label(node_i);

        tracing::trace!(?node_i, ?self.cx, "Translating dominator tree node");

        let is_loop_header = is_loop_header(self.cfg, self.label_info_list, node_i);

        if is_loop_header {
            // We are going to wrap the translation with `Control::Loop`.
            // Add the corresponding item to `self.cx` first.
            // [ref:rcfg_to_scfg_loop]
            self.cx.push(ContainingSyntax::LoopHeadedBy(node_i));
        }

        // Find the immediate dominatees that are merge nodes
        let i = self.idominatees.partition_point(|x| x[0] < node_i);
        let merge_node_idominatees = self.idominatees[i..]
            .iter()
            .take_while(|x| x[0] == node_i)
            .filter(|x| is_merge_node(self.cfg, x[1]))
            .map(|x| x[1]);

        tracing::trace!(
            is_loop_header,
            merge_node_idominatees = ?merge_node_idominatees.clone().collect_vec(),
        );

        // We are going to nest the translation of `node_i` inside blocks.
        // Add every nesting to `self.cx` first.
        // [ref:rcfg_to_scfg_node_within]
        let cx_len_without_blocks = self.cx.len();
        self.cx.extend(
            merge_node_idominatees
                .clone()
                .map(ContainingSyntax::BlockFollowedBy),
        );

        // Translate `node_i`
        let action_control_i = self.scfg.controls.insert(Control::Actions(label_i));

        let successors = &self.labels[label_i];
        let outflow_control_i = match successors[..] {
            [] => self.scfg.controls.insert(Control::Return),
            [next_label_i] => self.translate_branch(node_i, label_to_cfg_node(next_label_i)),
            [label1_i, label2_i] => {
                let arms = [label1_i, label2_i]
                    .map(|i| self.translate_branch(node_i, label_to_cfg_node(i)));
                self.scfg.controls.insert(Control::If {
                    cond: label_i,
                    arms,
                })
            }
            _ => unreachable!(),
        };

        let mut control_i = self
            .scfg
            .controls
            .insert(Control::Seq([action_control_i, outflow_control_i]));

        // [tag:rcfg_to_scfg_node_within]
        // Nest `control_i` within a block for every node in `merge_idominatees`
        //
        //     'idominatees2: {
        //         'idominatees1: {
        //              translation of node_i
        //         }
        //         idominatees1
        //     }
        //     idominatees2
        //
        // Child nodes with larger reverse postorder indices go outside.
        while self.cx.len() > cx_len_without_blocks {
            let Some(ContainingSyntax::BlockFollowedBy(child_node_i)) = self.cx.pop() else {
                unreachable!()
            };

            let block_control_i = self.scfg.controls.insert(Control::Block(control_i));

            let child_control_i = self.translate_tree(child_node_i);

            control_i = self
                .scfg
                .controls
                .insert(Control::Seq([block_control_i, child_control_i]));
        }

        // [tag:rcfg_to_scfg_loop]
        // If `node_i` is a loop header, wrap `control_i` with a `loop` block.
        if is_loop_header {
            self.cx.pop();
            control_i = self.scfg.controls.insert(Control::Loop(control_i));
        }

        control_i
    }

    /// Generate code that, when placed after the translation of `from_node_i`,
    /// transfers the control to the translation of `to_node_i`.
    fn translate_branch(&mut self, from_node_i: CfgNodeI, to_node_i: CfgNodeI) -> ControlI {
        let [from_postorder, to_postorder] = [from_node_i, to_node_i]
            .map(|node_i| self.label_info_list[cfg_node_to_label(node_i)].postorder);
        let is_backward = to_postorder >= from_postorder;

        let cx = &self.cx;
        let mut cx_rev = cx.iter().rev();

        if is_backward {
            let depth = cx_rev
                .position(|cs| *cs == ContainingSyntax::LoopHeadedBy(to_node_i))
                .unwrap_or_else(|| {
                    panic!("target loop header {to_node_i:?} not found in context {cx:?}")
                });
            self.scfg.controls.insert(Control::Continue { depth })
        } else if is_merge_node(self.cfg, to_node_i) {
            let depth = cx_rev
                .position(|cs| *cs == ContainingSyntax::BlockFollowedBy(to_node_i))
                .unwrap_or_else(|| {
                    panic!("target block {to_node_i:?} not found in context {cx:?}")
                });
            self.scfg.controls.insert(Control::Break { depth })
        } else {
            self.translate_tree(to_node_i)
        }
    }
}
