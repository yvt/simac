//! Converts a generic CFG to a structured CFG.
use arrayvec::ArrayVec;
use bitvec::vec::BitVec;
use fn_formats::DebugFmt;
use itertools::enumerate;
use petgraph::{Direction, visit::EdgeRef};

use crate::utils::intset::SlUsizeSet;

use super::{MAX_SUCC, Scfg};

/// An index into `labels`.
type LabelI = usize;

/// [`State::cfg`]
type Cfg = petgraph::stable_graph::StableGraph<CfgNode, ()>;

/// The node data of [`State::cfg`].
#[derive(Debug)]
struct CfgNode {
    /// The original label.
    label_i: LabelI,
    /// The node of [`State::reduced_cfg`] containing this node.
    rnode_i: ReducedCfgNodeI,
    /// The index of the next [`CfgNode`] contained by the same
    /// [`Self::rnode_i`]. [`CfgNodeI::end()`] if this is the last one.
    /// This singly-linked list is headed by [`ReducedCfgNode::first_node_i`].
    next_node_i_in_reduced_node: CfgNodeI,
    /// A temporary variable.
    temp_node_i: CfgNodeI,
    inner_label_i: LabelI,
}

type CfgNodeI = petgraph::stable_graph::NodeIndex<u32>;

/// [`State::reduced_cfg`]
type ReducedCfg = petgraph::stable_graph::StableGraph<ReducedCfgNode, ()>;

/// The node data of [`State::reduced_cfg`].
#[derive(Debug)]
struct ReducedCfgNode {
    /// The first [`CfgNode`] contained by this node.
    first_node_i: CfgNodeI,
}

type ReducedCfgNodeI = petgraph::stable_graph::NodeIndex<u32>;

struct State {
    cfg: Cfg,
    /// The reduced form of [`Self::cfg`]. (A *supergraph* in `[ref:ramsey22]`)
    reduced_cfg: ReducedCfg,
    /// The nodes of [`Self::cfg`] and [`Self::reduced_cfg`] containing the
    /// entry label.
    entry_node_i: CfgNodeI,
    /// The set of nodes in [`Self::reduced_cfg`] that might be eligible for
    /// T2 transformation (`[ref:hecht72]`).
    t2_dirty: SlUsizeSet,
    /// Temporary flags associated with nodes.
    node_flags: BitVec,
}

/// Convert a CFG to a structured CFG.
///
/// `labels[i]` specifies the sucessors of basic block `i`. The `usize` values
/// in the return value represent indices into `labels`.
pub fn cfg_to_scfg(labels: &[ArrayVec<LabelI, MAX_SUCC>], entry_label_i: LabelI) -> Scfg<LabelI> {
    // Convert `labels` to `petgraph::StableGraph`
    // ------------------------------------------------------------------
    let mut cfg = Cfg::with_capacity(labels.len(), labels.len() * MAX_SUCC);

    for label_i in 0..labels.len() {
        let node_i = cfg.add_node(CfgNode {
            label_i,
            rnode_i: CfgNodeI::new(label_i),
            next_node_i_in_reduced_node: CfgNodeI::end(),
            temp_node_i: CfgNodeI::end(),
            inner_label_i: 0,
        });

        // This allows us to convert `LabelI` to `NodeIndex` with no storage
        // cost
        assert_eq!(node_i.index(), label_i);
    }

    for (from_label_i, successors) in enumerate(labels) {
        // Remove duplicate successors
        let mut successors = &successors[..];
        match successors {
            [x, y] if x == y => {
                successors = &successors[..1];
            }
            [] | [_] | [_, _] => {}
            _ => unreachable!(),
        }

        for &to_label_i in successors {
            // The successor order is not preserved during insertion to `cfg`.
            // It is restored later by [ref:cfg_to_scfg_successor_order].
            cfg.add_edge(CfgNodeI::new(from_label_i), CfgNodeI::new(to_label_i), ());
        }
    }

    tracing::debug!(
        entry_label_i,
        "CFG to convert to sCFG:\n{:?}",
        petgraph::dot::Dot::with_config(
            &cfg,
            &[
                petgraph::dot::Config::NodeIndexLabel,
                petgraph::dot::Config::EdgeNoLabel
            ]
        ),
    );

    // Reduce the original CFG [ref:hecht72]
    // ------------------------------------------------------------------

    // If the original CFG is reducible, this procedure will turn it into
    // a single-node graph.

    let reduced_cfg = cfg.map(
        |node_i, node| {
            // Initially, `reduced_cfg[node_i]` contains single node
            // `cfg[node_i]`
            debug_assert_eq!(node.rnode_i, node_i);
            debug_assert_eq!(node.next_node_i_in_reduced_node, CfgNodeI::end());
            ReducedCfgNode {
                first_node_i: node_i,
            }
        },
        |_, ()| (),
    );

    let mut state = State {
        entry_node_i: CfgNodeI::new(entry_label_i),
        t2_dirty: SlUsizeSet::with_capacity(cfg.node_count()),
        node_flags: BitVec::new(),
        cfg,
        reduced_cfg,
    };

    // Remove self-loops (T1)
    //
    //                 T1(u)
    //      ╭─▶ u ──╮   ══▶   u
    //      ╰───────╯
    state.reduced_cfg.retain_edges(|reduced_cfg, edge| {
        let (x, y) = reduced_cfg.edge_endpoints(edge).unwrap();
        x != y
    });

    // Merge single-predecessor nodes (T2)
    state
        .t2_dirty
        .extend(state.reduced_cfg.node_indices().map(ReducedCfgNodeI::index));
    state.reduce_reduced_cfg();

    tracing::debug!("Reduced CFG:\n{}", state.pretty_print_reduced_cfg());

    #[cfg(debug_assertions)]
    for node_i in state.reduced_cfg.node_indices() {
        if node_i.index() == entry_label_i {
            continue;
        }
        let predecessors: Vec<_> = state
            .reduced_cfg
            .neighbors_directed(node_i, Direction::Incoming)
            .collect();
        assert_ne!(
            predecessors.len(),
            1,
            "{node_i:?} still have one predecessor"
        );
    }

    // Split nodes until the CFG becomes reducible
    // ------------------------------------------------------------------

    // TODO: Implement controlled node splitting [ref:janssen97] to
    // optimize the number of splits

    // Pick any non-entry reduced-CFG node
    while let Some(rnode_i) = state
        .reduced_cfg
        .node_indices()
        .find(|&i| i != state.entry_node_i)
    {
        tracing::trace!(?rnode_i, "Splitting reduced CFG node");
        state.split_rnode(rnode_i);
        state.reduce_reduced_cfg();

        tracing::trace!("CFG after node splitting:\n{}", state.pretty_print_cfg());
        tracing::trace!(
            "Reduced CFG after node splitting:\n{}",
            state.pretty_print_reduced_cfg()
        );
    }

    tracing::debug!(
        "Final CFG after node splitting:\n{}",
        state.pretty_print_cfg()
    );

    // Execute `rcfg_to_scfg`
    // ------------------------------------------------------------------

    let mut cfg = state.cfg;

    // Assign `CfgNode::inner_label_i` and calculate the mapping from
    // `inner_label_i` to `label_i`
    let inner_label_to_label: Vec<LabelI> = cfg
        .node_weights_mut()
        .enumerate()
        .map(|(inner_label_i, node)| {
            node.inner_label_i = inner_label_i;
            node.label_i
        })
        .collect();

    tracing::debug!(
        "Mapping from inner labels to outer labels: {:?}",
        DebugFmt(|f| f
            .debug_map()
            .entries(inner_label_to_label.iter().enumerate())
            .finish()),
    );

    // Create an input to `rcfg_to_scfg`
    let inner_labels: Vec<ArrayVec<LabelI, MAX_SUCC>> = cfg
        .node_indices()
        .enumerate()
        .map(|(inner_label_i, node_i)| {
            let label_i = inner_label_to_label[inner_label_i];

            let mut successors: ArrayVec<LabelI, MAX_SUCC> = cfg
                .neighbors_directed(node_i, Direction::Outgoing)
                .map(|succ_node_i| cfg[succ_node_i].inner_label_i)
                .collect();

            if let [i0, i1] = &mut successors[..] {
                // [tag:cfg_to_scfg_successor_order]
                // Make sure the successors are in the correct order
                let orig_successors = &labels[label_i];
                if inner_label_to_label[*i0] != orig_successors[0] {
                    (*i0, *i1) = (*i1, *i0);
                }
            }

            successors
        })
        .collect();
    let inner_entry_label_i = cfg[state.entry_node_i].inner_label_i;

    // Call `rcfg_to_scfg`
    let mut scfg = super::rcfg_to_scfg::rcfg_to_scfg(&inner_labels, inner_entry_label_i);

    // Map `inner_label_i` -> `label_i`
    scfg.for_each_label(|p_label| *p_label = inner_label_to_label[*p_label]);

    scfg
}

impl State {
    /// Reduce [`Self::reduced_cfg`] until no further transformations are
    /// possible.
    ///
    /// It is assumed that `reduced_cfg` is free of self-loops (i.e., T1
    /// transformation from `[ref:hecht72]` has already been applied).
    ///
    /// This method merges single-predecessor nodes (T2 transformation).
    /// This method will only remove self-loops (T1 transformation) that are
    /// resultant of this transformation.
    fn reduce_reduced_cfg(&mut self) {
        let Self {
            cfg,
            reduced_cfg,
            t2_dirty,
            node_flags,
            entry_node_i,
            ..
        } = self;

        // Reserve space in `node_flags`
        node_flags.resize(node_flags.len().max(reduced_cfg.capacity().0), false);

        // Merge single-predecessor nodes (T2)
        while let Some(rnode_i) = t2_dirty.pop().map(ReducedCfgNodeI::new) {
            tracing::trace!(?rnode_i, "Checking node for T2");

            if rnode_i == *entry_node_i {
                tracing::trace!("Ignoring entry node");
                continue;
            }

            let _span = tracing::trace_span!("T2(rnode_i)", ?rnode_i).entered();

            let mut predecessors = reduced_cfg.neighbors_directed(rnode_i, Direction::Incoming);
            let Some(pred_rnode_i) = predecessors.next() else { continue };
            if predecessors.next().is_some() {
                // Unable to merge - `rnode_i` has two or more predecessors
                continue;
            }

            // We are going to merge `rnode_i` into `pred_rnode_i`, moving
            // all successors of the former to the latter.
            //
            //                     ╭─▶ ◻
            //     ──▶ pred_rnode ─┴─▶ rnode ─┬─▶ △
            //                                ╰─▶ ○
            //                   ║
            //                   ║ T2(rnode_i)
            //                   ▼
            //                         ╭─▶ ◻
            //         ──▶ pred_rnode ─┼─▶ △
            //                         ╰─▶ ○
            tracing::trace!(?rnode_i, ?pred_rnode_i, "Will apply T2(rnode_i)");

            // Check the existing successors of `pred_rnode_i`
            for succ_rnode_i in reduced_cfg.neighbors_directed(pred_rnode_i, Direction::Outgoing) {
                // `node_flags[i]` is set iff edge `[pred_rnode_i, i]` exists
                node_flags.set(succ_rnode_i.index(), true);
            }

            // Move successors of `rnode_i` to `pred_rnode_i`
            while let Some(edge_ref) = reduced_cfg
                .edges_directed(rnode_i, Direction::Outgoing)
                .next()
            {
                let succ_rnode_i = edge_ref.target();

                // Remove edge `[rnode_i, succ_rnode_i]`
                reduced_cfg.remove_edge(edge_ref.id());

                if succ_rnode_i == pred_rnode_i {
                    // Exclude self-loops (T1)
                    tracing::trace!(
                        old_edge = ?[rnode_i, succ_rnode_i],
                        "Ignoring this edge, which would be deleted by T1",
                    );

                    // This means that `pred_rnode_i` will have one less predecessor.
                    // Mark it to check later.
                    t2_dirty.push(pred_rnode_i.index());
                    continue;
                }

                if node_flags.replace(succ_rnode_i.index(), true) {
                    // Exclude duplicate edge
                    tracing::trace!(
                        old_edge = ?[rnode_i, succ_rnode_i],
                        new_edge = ?[pred_rnode_i, succ_rnode_i],
                        "Not moving duplicate edge",
                    );

                    // This means that `succ_rnode_i` will have one less predecessor.
                    // Mark it to check later.
                    t2_dirty.push(succ_rnode_i.index());
                    continue;
                }

                // Add edge `[pred_rnode_i, succ_rnode_i]`
                tracing::trace!(
                    old_edge = ?[rnode_i, succ_rnode_i],
                    new_edge = ?[pred_rnode_i, succ_rnode_i],
                    "Moving edge from `rnode_i` to `pred_rnode_i`",
                );
                reduced_cfg.add_edge(pred_rnode_i, succ_rnode_i, ());
            }

            // Clear `node_flags`
            for succ_rnode_i in reduced_cfg.neighbors_directed(pred_rnode_i, Direction::Outgoing) {
                node_flags.set(succ_rnode_i.index(), false);
            }

            // Remove `rnode_i` from `reduced_cfg`
            let mut reduced_node = reduced_cfg.remove_node(rnode_i).unwrap();

            // Merge the `CfgNode` list of `rnode_i` into that of `pred_rnode_i`
            let reduced_pred_node = &mut reduced_cfg[pred_rnode_i];
            while reduced_node.first_node_i != CfgNodeI::end() {
                let node_i: CfgNodeI = reduced_node.first_node_i;
                let cfg_node = &mut cfg[node_i];

                // Remove this `CfgNode` from `rnode_i`
                reduced_node.first_node_i = cfg_node.next_node_i_in_reduced_node;

                // Insert this `CfgNode` into `pred_rnode_i`
                cfg_node.next_node_i_in_reduced_node = reduced_pred_node.first_node_i;
                cfg_node.rnode_i = pred_rnode_i;
                reduced_pred_node.first_node_i = node_i;
            }
        }
    }

    /// *Split* the specified [`ReducedCfgNode`].
    ///
    /// The node-splitting transformation duplicates the [`ReducedCfgNode`] and
    /// all contained [`CfgNode`]s for every predecessor, and replaces every
    /// reference in the predecessors to an original [`CfgNode`] with a
    /// reference to the corresponding copy of the [`CfgNode`].
    /// `[ref:ramsey22]`
    #[tracing::instrument(level = "trace", skip(self))]
    fn split_rnode(&mut self, rnode_i: ReducedCfgNodeI) {
        let first_node_i = self.reduced_cfg[rnode_i].first_node_i;

        let num_predecessors = self
            .reduced_cfg
            .neighbors_directed(rnode_i, Direction::Incoming)
            .count();

        if num_predecessors <= 1 {
            return;
        }

        for _ in 0..num_predecessors - 1 {
            // Get the first predecessor of `rnode_i` and remove the edge
            let pred_redge = self
                .reduced_cfg
                .edges_directed(rnode_i, Direction::Incoming)
                .next()
                .unwrap();
            let pred_redge_i = pred_redge.id();
            let pred_rnode_i = pred_redge.source();

            // Create a new `ReducedCfgNode`
            let new_rnode_i = self.reduced_cfg.add_node(ReducedCfgNode {
                first_node_i: CfgNodeI::end(), // set later
            });
            let mut new_first_node_i = CfgNodeI::end();

            tracing::trace!(
                ?pred_rnode_i,
                ?pred_redge_i,
                ?new_rnode_i,
                "Duplicating `rnode_i`",
            );

            // Mark `new_rnode_i` for T2 transformation
            self.t2_dirty.push(new_rnode_i.index());

            // Duplicate all `CfgNode`s from `rnode_i` and add them to
            // `new_rnode_i`
            {
                let mut node_i = first_node_i;
                while node_i != CfgNodeI::end() {
                    let label_i = self.cfg[node_i].label_i;
                    let new_node_i = self.cfg.add_node(CfgNode {
                        label_i,
                        rnode_i: new_rnode_i,
                        next_node_i_in_reduced_node: new_first_node_i,
                        temp_node_i: CfgNodeI::end(),
                        inner_label_i: 0,
                    });

                    // Add this new `CfgNode` to the new `new_rnode_i`
                    new_first_node_i = new_node_i;

                    // Store a reference to the new `CfgNode` in the original
                    // node's `CfgNode::temp_node_i`
                    self.cfg[node_i].temp_node_i = new_node_i;

                    node_i = self.cfg[node_i].next_node_i_in_reduced_node;
                }
            }

            self.reduced_cfg[new_rnode_i].first_node_i = new_first_node_i;

            // Duplicate all reduced CFG edge originating from `rnode_i`
            {
                let mut successors = self
                    .reduced_cfg
                    .neighbors_directed(rnode_i, Direction::Outgoing)
                    .detach();

                while let Some(succ_rnode_i) = successors.next_node(&self.reduced_cfg) {
                    self.reduced_cfg.add_edge(new_rnode_i, succ_rnode_i, ());
                }
            }

            // Duplicate all CFG edges originating from `rnode_i`
            {
                let mut node_i = first_node_i;
                while node_i != CfgNodeI::end() {
                    let new_node_i = self.cfg[node_i].temp_node_i;

                    let mut successors = self
                        .cfg
                        .neighbors_directed(node_i, Direction::Outgoing)
                        .detach();

                    while let Some(mut succ_node_i) = successors.next_node(&self.cfg) {
                        let succ_node = &self.cfg[succ_node_i];

                        if succ_node.rnode_i == rnode_i {
                            // Replace the edge target with its counterpart in
                            // `new_rnode_i`
                            succ_node_i = succ_node.temp_node_i;
                        }

                        self.cfg.add_edge(new_node_i, succ_node_i, ());
                    }

                    node_i = self.cfg[node_i].next_node_i_in_reduced_node;
                }
            }

            // Replace reduced CFG edge `[pred_rnode_i, rnode_i]` with
            // `[pred_rnode_i, new_rnode_i]`
            self.reduced_cfg.remove_edge(pred_redge_i).unwrap();
            self.reduced_cfg.add_edge(pred_rnode_i, new_rnode_i, ());

            // Replace every CFG edge going from `pred_rnode_i` to `rnode_i` to
            // target the corresponding CFG node in `new_rnode_i`
            {
                let mut node_i = self.reduced_cfg[pred_rnode_i].first_node_i;
                while node_i != CfgNodeI::end() {
                    let mut successors = self
                        .cfg
                        .neighbors_directed(node_i, Direction::Outgoing)
                        .detach();

                    while let Some((succ_edge_i, succ_node_i)) = successors.next(&self.cfg) {
                        let succ_node = &self.cfg[succ_node_i];

                        if self.cfg[succ_node_i].rnode_i != rnode_i {
                            continue;
                        }

                        let new_succ_node_i = succ_node.temp_node_i;

                        // Replace the edge target with its counterpart in
                        // `new_rnode_i`
                        self.cfg.remove_edge(succ_edge_i);
                        self.cfg.add_edge(node_i, new_succ_node_i, ());
                    }

                    node_i = self.cfg[node_i].next_node_i_in_reduced_node;
                }
            }
        }

        // Since we have removed `num_predecessors - 1` predecessors,
        // original `rnode_i` should have only one predecessor remaining
        debug_assert_eq!(
            self.reduced_cfg
                .neighbors_directed(rnode_i, Direction::Incoming)
                .count(),
            1,
            "`rnode_i` has unexpected number of predecessors:\n{}",
            self.pretty_print_reduced_cfg(),
        );

        // Mark `rnode_i` for T2 transformation since it is eligible now
        self.t2_dirty.push(rnode_i.index());
    }

    fn pretty_print_cfg(&self) -> String {
        petgraph::dot::Dot::with_config(
            &self.cfg.map(
                |node_i, node| format!("{}: L{}", node_i.index(), node.label_i),
                |_, _| "",
            ),
            &[petgraph::dot::Config::EdgeIndexLabel],
        )
        .to_string()
    }

    fn pretty_print_reduced_cfg(&self) -> String {
        petgraph::dot::Dot::with_config(
            &self.reduced_cfg.map(
                |rnode_i, rnode| {
                    // Display the list of `CfgNode`s that belong to this
                    // `ReducedCfgNode`
                    let filter = |i| (i != CfgNodeI::end()).then_some(i);
                    let node_i_list: Vec<_> =
                        std::iter::successors(filter(rnode.first_node_i), |&i| {
                            filter(self.cfg[i].next_node_i_in_reduced_node)
                        })
                        .map(CfgNodeI::index)
                        .collect();

                    format!("{} {node_i_list:?}", rnode_i.index())
                },
                |_, _| "",
            ),
            &[petgraph::dot::Config::EdgeIndexLabel],
        )
        .to_string()
    }
}
