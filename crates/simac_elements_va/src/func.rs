//! Lowering [`Function`] to Rust statements
use std::fmt::{Display, Write as _};

use anyhow::Result;
use arrayvec::ArrayVec;
use fn_formats::DisplayFmt;
use fxhash::FxHashSet;
use itertools::enumerate;
use openvaf::mir::{Block, FuncRef, Function, InstructionData, Opcode, Param, Value, ValueDef};
use typed_index_collections::TiVec;

use crate::{
    cfg2rust,
    utils::rust::{TY_REAL, lit_f64, lit_str, real},
};

/// An evaluation strategy of an input parameter.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EvalStrategy {
    /// Evaluate the given expression before executing the function body.
    Eager,
    /// Evaluate the given expression whenever the [`Value`] is evaluated.
    OnDemand,
}

/// Translate a given [`Function`] to Rust code and append the result to `*out`.
///
/// # Callbacks
///
/// `callbacks` specifies the mapping from [`FuncRef`]s to function names.
///
/// If the function name for a [`FuncRef`] is `None`, the function calls to that
/// [`FuncRef`] are dropped. This is not allowed if the function returns one or
/// more values.
///
/// If the function has exactly one result, it should return the result value
/// as-is. Otherwise, it should wrap all results in a tuple.
///
/// A function call is lowered to an expression like this: `$name($args)`
/// `$name` can refer to a function, local variable (holding a closure), or a
/// method.
///
/// # Output Values
///
/// The generated code introduces into the scope local variables holding the
/// values of all [`Value`]s. The output values can be found in them.
/// All [`Value`]s containing output values must be marked with `output_values`.
///
/// # Types
///
/// - Reals are represented by type `Cx::Real`.
/// - Integers are represented by type [`i32`].
/// - Strings are represented by type `&'static str`.
pub(crate) fn func_to_rust(
    out: &mut String,
    literals: &lasso::Rodeo,
    function: &Function,
    params: &TiVec<Param, (String, EvalStrategy)>,
    callbacks: &TiVec<FuncRef, Option<String>>,
    output_values: &FxHashSet<Value>,
) -> Result<()> {
    Emitter {
        out,
        literals,
        function,
        params,
        callbacks,
        output_values,
    }
    .emit_function()
}

macro_rules! w {
    ($emitter:expr, $($tt:tt)*) => {
        write!($emitter.out, $($tt)*).unwrap()
    };
}

struct Emitter<'a> {
    out: &'a mut String,
    literals: &'a lasso::Rodeo,
    function: &'a Function,
    params: &'a TiVec<Param, (String, EvalStrategy)>,
    callbacks: &'a TiVec<FuncRef, Option<String>>,
    output_values: &'a FxHashSet<Value>,
}

struct ScfgCx {
    scfg: cfg2rust::Scfg<usize>,
    label_i_to_block: Vec<Block>,
    next_block_id: usize,
}

impl<'a> Emitter<'a> {
    fn emit_function(&mut self) -> Result<()> {
        let Self {
            function,
            params,
            output_values,
            ..
        } = self;

        // Build input to `cfg_to_scfg`
        let mut block_to_label_i: TiVec<Block, usize> =
            vec![usize::MAX; function.layout.num_blocks()].into();
        for (label_i, block) in function.layout.blocks().enumerate() {
            block_to_label_i[block] = label_i;
        }
        tracing::debug!(?block_to_label_i);

        let labels: Vec<_> = function
            .layout
            .blocks()
            .map(|block| {
                let Some(terminator) = function.layout.block_terminator(block) else {
                    // Empty exit block
                    return ArrayVec::from_iter([]);
                };

                let inst = &function.dfg.insts[terminator];
                match *inst {
                    InstructionData::Branch {
                        then_dst, else_dst, ..
                    } => {
                        // `Control::If` is not guaranteed to be generated if
                        // `then_dst == else_dst`, which would complicate our
                        // algorithm
                        assert_ne!(then_dst, else_dst);

                        ArrayVec::from([block_to_label_i[then_dst], block_to_label_i[else_dst]])
                    }
                    InstructionData::Jump { destination } => {
                        ArrayVec::from_iter([block_to_label_i[destination]])
                    }

                    // Exit block
                    InstructionData::Unary { .. }
                    | InstructionData::Binary { .. }
                    | InstructionData::PhiNode { .. }
                    | InstructionData::Call { .. } => ArrayVec::from_iter([]),
                }
            })
            .collect();

        let entry_label_i = block_to_label_i[function
            .layout
            .entry_block()
            .expect("entry block is missing")];

        // Build structured control flow
        let mut scfg = cfg2rust::cfg_to_scfg::cfg_to_scfg(&labels, entry_label_i);

        // [tag:va_func_no_return] Eliminate `Control::Return`
        cfg2rust::eliminate_return::eliminate_return(&mut scfg);

        // TODO: Eliminate redundant `Break`s

        tracing::debug!(?scfg, "Structured control flow");

        // This closure determines if `Value` is defined by an actually
        // executed instruction.
        let value_def_by_code = |def: &ValueDef| {
            let &ValueDef::Result(inst, _) = def else { return false };
            function.layout.inst_block(inst).is_some()
        };

        let inst_result_ty = |inst| {
            let inst_data = &function.dfg.insts[inst];
            let (InstructionData::Unary { opcode, .. } | InstructionData::Binary { opcode, .. }) =
                *inst_data
            else {
                return None;
            };
            opcode_result_ty(opcode)
        };

        // Emit local variables
        for value in function.dfg.values() {
            let def = function.dfg.value_def(value);
            if function.dfg.value_dead(value)
                && !output_values.contains(&value)
                && !value_def_by_code(&def)
            {
                continue;
            }

            match def {
                ValueDef::Result(inst, _) => {
                    if let Some(ty) = inst_result_ty(inst) {
                        // Try to explicitly set type to aid type inference
                        // (it would be ideal if we could unconditionally
                        // do that)
                        w!(self, "let mut {value}: {ty}")
                    } else {
                        w!(self, "let mut {value}")
                    }

                    // Unconditionally initialize the variable with a
                    // placeholder on declaration time. This is because some
                    // output values are only conditionally assigned, and Rust
                    // does not like that.
                    //
                    // Conditional assignment of output values can occur in
                    // cached values. This can be legally done if the reads of
                    // the cached values are also done under the same
                    // conditions.
                    w!(self, " = <_>::default();");
                }
                ValueDef::Invalid => {}
                ValueDef::Param(param) => {
                    let (expr, strategy) = &params[param];
                    if *strategy == EvalStrategy::Eager {
                        w!(self, "let {value} = {expr};");
                    }
                }
                ValueDef::Const(x) => {
                    w!(self, "let {value} = ");
                    match x {
                        openvaf::mir::Const::Float(x) => {
                            w!(self, "{}", real(lit_f64(f64::from_bits(x.bits()))))
                        }
                        openvaf::mir::Const::Int(x) => w!(self, "{x}_i32"),
                        openvaf::mir::Const::Str(x) => {
                            w!(self, "{}", lit_str(self.literals.resolve(&x)))
                        }
                        openvaf::mir::Const::Bool(x) => w!(self, "{x}"),
                    }
                    w!(self, ";");
                }
            }
        }

        // Emit code
        let root_control_i = scfg.root_control_i;
        let label_i_to_block = function.layout.blocks().collect();
        self.emit_scfg_control(
            &mut ScfgCx {
                scfg,
                label_i_to_block,
                next_block_id: 0,
            },
            root_control_i,
        );

        // Make sure all output variables are evaluated
        for &value in self.output_values {
            if let ValueDef::Param(param) = self.function.dfg.value_def(value) {
                let (expr, strategy) = &self.params[param];
                if *strategy == EvalStrategy::OnDemand {
                    w!(self, "let {} = {expr};", value_var(value));
                }
            }
        }

        Ok(())
    }

    fn emit_scfg_control(&mut self, scx: &mut ScfgCx, control_i: cfg2rust::ControlI) {
        match scx.scfg.controls[control_i] {
            cfg2rust::Control::Block(child_control_i) => {
                w!(self, "{}: {{", block_label(scx.next_block_id));
                scx.next_block_id += 1;
                self.emit_scfg_control(scx, child_control_i);
                w!(self, "}}");
                scx.next_block_id -= 1;
            }
            cfg2rust::Control::Loop(child_control_i) => {
                w!(self, "{}: loop {{", block_label(scx.next_block_id));
                scx.next_block_id += 1;
                self.emit_scfg_control(scx, child_control_i);
                w!(self, "}}");
                scx.next_block_id -= 1;
            }
            cfg2rust::Control::If {
                cond,
                arms: [then_control_i, else_control_i],
            } => {
                // [tag:va_func_branch]
                // Find the terminator that generated this `If`
                let layout = &self.function.layout;
                let dfg = &self.function.dfg;

                let block = scx.label_i_to_block[cond];
                let terminator = layout.block_terminator(block).expect("no terminator");
                let inst_data = &dfg.insts[terminator];
                let &InstructionData::Branch {
                    cond,
                    then_dst,
                    else_dst,
                    ..
                } = inst_data
                else {
                    unreachable!(
                        "`If` uses result of block {block:?} with \
                        non-`Branch` terminator: {inst_data:?}"
                    )
                };

                w!(self, "if {} {{", self.load_value(cond));
                self.emit_phi(then_dst, block);
                self.emit_scfg_control(scx, then_control_i);
                w!(self, "}} else {{");
                self.emit_phi(else_dst, block);
                self.emit_scfg_control(scx, else_control_i);
                w!(self, "}}");
            }
            cfg2rust::Control::Break { depth } => {
                w!(
                    self,
                    "break {};",
                    block_label(scx.next_block_id - 1 - depth)
                );
            }
            cfg2rust::Control::Continue { depth } => {
                w!(
                    self,
                    "continue {};",
                    block_label(scx.next_block_id - 1 - depth)
                );
            }
            cfg2rust::Control::Return => {
                // [ref:va_func_no_return]
                unreachable!();
            }
            cfg2rust::Control::Actions(label) => {
                self.emit_block(scx.label_i_to_block[label]);
            }
            cfg2rust::Control::Seq([c0, c1]) => {
                self.emit_scfg_control(scx, c0);
                self.emit_scfg_control(scx, c1);
            }
        }
    }

    fn emit_block(&mut self, block: Block) {
        let layout = &self.function.layout;
        let dfg = &self.function.dfg;

        w!(self, "\n/* {block} */\n");

        for inst in layout.block_insts(block) {
            let inst_data = &dfg.insts[inst];
            w!(self, "/* {inst} */");
            match inst_data {
                InstructionData::Unary { opcode, arg } => self.emit_arithmetic_op(
                    dfg.first_result(inst),
                    *opcode,
                    std::array::from_ref(arg),
                ),
                InstructionData::Binary { opcode, args } => {
                    self.emit_arithmetic_op(dfg.first_result(inst), *opcode, args)
                }
                InstructionData::Branch { .. } => {
                    // Condition branch is handled by [ref:va_func_branch]
                }
                InstructionData::PhiNode { .. } => {
                    // Phi nodes are handled in the precedessors
                }
                InstructionData::Jump { destination } => {
                    self.emit_phi(*destination, block);
                }
                InstructionData::Call { func_ref, args } => {
                    // `name` can contain a closure expression. Evaluate
                    // arguments first so that `name` can mutably borrow
                    // the variables used for  argument evaluation.
                    let args = args.as_slice(&dfg.insts.value_lists);
                    for (i, &arg) in enumerate(args) {
                        w!(self, "let arg{i} = {};", self.load_value(arg));
                    }

                    let results = dfg.inst_results(inst);

                    match results {
                        [] => {}
                        [v] => w!(self, "{} = ", value_var(*v)),
                        [v0, rest @ ..] => {
                            w!(self, "({}", value_var(*v0));
                            for v in rest {
                                w!(self, ", {}", value_var(*v));
                            }
                            w!(self, ") = ");
                        }
                    }

                    let Some(name) = &self.callbacks[*func_ref] else {
                        assert!(
                            results.is_empty(),
                            "unable to drop function call with result"
                        );
                        continue;
                    };

                    w!(self, "{name}(");

                    for i in 0..args.len() {
                        w!(self, "arg{i}, ");
                    }

                    w!(self, ");");
                }
            }
        }
    }

    /// Emit the code for phi instructions.
    ///
    /// This should be called while emitting the code of a branch source.
    fn emit_phi(&mut self, block: Block, predecessor_block: Block) {
        let layout = &self.function.layout;
        let dfg = &self.function.dfg;

        for inst in layout.block_insts(block) {
            let inst_data = &dfg.insts[inst];
            let InstructionData::PhiNode(phi_node) = inst_data else { break };

            let result = dfg.first_result(inst);

            let value = phi_node
                .edge_val(predecessor_block, &dfg.insts.value_lists, &dfg.phi_forest)
                .unwrap_or_else(|| {
                    panic!("{inst_data:?} does not handle source block {predecessor_block:?}")
                });

            w!(self, "{} = {};", value_var(result), self.load_value(value))
        }
    }

    fn emit_arithmetic_op(&mut self, result: Value, opcode: Opcode, args: &[Value]) {
        w!(self, "{} = ", value_var(result));

        let arg = |i: usize| self.load_value(args[i]);

        match opcode {
            Opcode::FIcast => w!(
                self,
                "num_traits::ToPrimitive::to_i32(&{}).unwrap()",
                arg(0)
            ),
            Opcode::IFcast => w!(self, "num_traits::NumCast::from({}).unwrap()", arg(0)),
            Opcode::BIcast => w!(self, "{} as {TY_INT}", arg(0)),
            Opcode::IBcast => w!(self, "{} != 0", arg(0)),
            Opcode::FBcast => w!(self, "!num_traits::Zero::is_zero(&{})", arg(0)),
            Opcode::BFcast => w!(self, "num_traits::NumCast::from({} as u8).unwrap()", arg(0)),

            Opcode::Sqrt => w!(self, "num_traits::Float::sqrt({})", arg(0)),
            Opcode::Exp => w!(self, "num_traits::Float::exp({})", arg(0)),
            Opcode::Ln => w!(self, "num_traits::Float::ln({})", arg(0)),
            Opcode::Log => w!(self, "num_traits::Float::log10({})", arg(0)),
            Opcode::Clog2 => w!(
                self,
                "({TY_INT}::BITS - {}.leading_zeros()) as {TY_INT}",
                arg(0)
            ),
            Opcode::Floor => w!(self, "num_traits::Float::floor({})", arg(0)),
            Opcode::Ceil => w!(self, "num_traits::Float::ceil({})", arg(0)),
            Opcode::Sin => w!(self, "num_traits::Float::sin({})", arg(0)),
            Opcode::Cos => w!(self, "num_traits::Float::cos({})", arg(0)),
            Opcode::Tan => w!(self, "num_traits::Float::tan({})", arg(0)),
            Opcode::Asin => w!(self, "num_traits::Float::asin({})", arg(0)),
            Opcode::Acos => w!(self, "num_traits::Float::acos({})", arg(0)),
            Opcode::Atan => w!(self, "num_traits::Float::atan({})", arg(0)),
            Opcode::Sinh => w!(self, "num_traits::Float::sinh({})", arg(0)),
            Opcode::Cosh => w!(self, "num_traits::Float::cosh({})", arg(0)),
            Opcode::Tanh => w!(self, "num_traits::Float::tanh({})", arg(0)),
            Opcode::Asinh => w!(self, "num_traits::Float::asinh({})", arg(0)),
            Opcode::Acosh => w!(self, "num_traits::Float::acosh({})", arg(0)),
            Opcode::Atanh => w!(self, "num_traits::Float::atanh({})", arg(0)),

            // TODO: `Iadd` etc might need to wrap-around
            Opcode::Iadd | Opcode::Fadd => w!(self, "{} + {}", arg(0), arg(1)),
            Opcode::Isub | Opcode::Fsub => w!(self, "{} - {}", arg(0), arg(1)),
            Opcode::Imul | Opcode::Fmul => w!(self, "{} * {}", arg(0), arg(1)),
            Opcode::Idiv | Opcode::Fdiv => w!(self, "{} / {}", arg(0), arg(1)),
            Opcode::Irem | Opcode::Frem => w!(self, "{} % {}", arg(0), arg(1)),
            Opcode::Ishl => w!(self, "{} << {}", arg(0), arg(1)),
            Opcode::Ishr => w!(self, "{} > {}", arg(0), arg(1)),
            Opcode::Ixor => w!(self, "{} ^ {}", arg(0), arg(1)),
            Opcode::Iand => w!(self, "{} & {}", arg(0), arg(1)),
            Opcode::Ior => w!(self, "{} | {}", arg(0), arg(1)),

            Opcode::Inot | Opcode::Bnot => w!(self, "!{}", arg(0)),
            Opcode::Fneg | Opcode::Ineg => w!(self, "-{}", arg(0)),

            Opcode::Ilt | Opcode::Flt => w!(self, "{} < {}", arg(0), arg(1)),
            Opcode::Igt | Opcode::Fgt => w!(self, "{} > {}", arg(0), arg(1)),
            Opcode::Ige | Opcode::Fge => w!(self, "{} >= {}", arg(0), arg(1)),
            Opcode::Ile | Opcode::Fle => w!(self, "{} <= {}", arg(0), arg(1)),
            Opcode::Ieq | Opcode::Feq | Opcode::Seq | Opcode::Beq => {
                w!(self, "{} == {}", arg(0), arg(1))
            }
            Opcode::Ine | Opcode::Fne | Opcode::Sne | Opcode::Bne => {
                w!(self, "{} != {}", arg(0), arg(1))
            }

            Opcode::Hypot => w!(self, "num_traits::Float::hypot({}, {})", arg(0), arg(1)),
            Opcode::Atan2 => w!(self, "num_traits::Float::atan2({}, {})", arg(0), arg(1)),
            Opcode::Pow => w!(self, "num_traits::Float::powf({}, {})", arg(0), arg(1)),
            Opcode::OptBarrier => w!(self, "{}", arg(0)),
            Opcode::Br | Opcode::Jmp | Opcode::Call | Opcode::Phi => {
                unreachable!("not arithmetic op: {opcode:?}")
            }
        }

        w!(self, ";");
    }

    /// Get the code to evaluate a given [`Value`].
    fn load_value(&self, value: Value) -> impl Display + use<'a> {
        let &Self {
            function, params, ..
        } = self;
        DisplayFmt(move |f| {
            if let ValueDef::Param(param) = function.dfg.value_def(value) {
                let (expr, strategy) = &params[param];
                if *strategy == EvalStrategy::OnDemand {
                    return write!(f, "({expr})");
                }
            }

            // In most cases, we just output the variable named `value.to_string()`
            write!(f, "{}", value_var(value))
        })
    }
}

#[inline]
fn value_var(value: Value) -> impl Display {
    value
}

/// Get the block label for `block_id` in source code form.
fn block_label(block_id: usize) -> impl Display {
    DisplayFmt(move |f| write!(f, "'block_{block_id}"))
}

/// Try to get the Rust result type of a given [`Opcode`].
fn opcode_result_ty(op: Opcode) -> Option<&'static str> {
    match op {
        Opcode::Bnot => Some("bool"),
        Opcode::Fadd | Opcode::Fdiv | Opcode::Fmul | Opcode::Fneg | Opcode::Frem | Opcode::Fsub => {
            Some(TY_REAL)
        }
        Opcode::Iadd
        | Opcode::Iand
        | Opcode::Idiv
        | Opcode::Imul
        | Opcode::Ineg
        | Opcode::Inot
        | Opcode::Ior
        | Opcode::Irem
        | Opcode::Ishl
        | Opcode::Ishr
        | Opcode::Isub
        | Opcode::Ixor => Some(TY_INT),

        Opcode::Ilt
        | Opcode::Igt
        | Opcode::Ige
        | Opcode::Ile
        | Opcode::Flt
        | Opcode::Fgt
        | Opcode::Fge
        | Opcode::Fle
        | Opcode::Ieq
        | Opcode::Feq
        | Opcode::Seq
        | Opcode::Beq
        | Opcode::Ine
        | Opcode::Fne
        | Opcode::Sne
        | Opcode::Bne => Some("bool"),

        Opcode::FIcast | Opcode::BIcast => Some(TY_INT),
        Opcode::IBcast | Opcode::FBcast => Some("bool"),
        Opcode::BFcast | Opcode::IFcast => Some(TY_REAL),

        Opcode::Sqrt
        | Opcode::Exp
        | Opcode::Ln
        | Opcode::Log
        | Opcode::Floor
        | Opcode::Ceil
        | Opcode::Sin
        | Opcode::Cos
        | Opcode::Tan
        | Opcode::Asin
        | Opcode::Acos
        | Opcode::Atan
        | Opcode::Sinh
        | Opcode::Cosh
        | Opcode::Tanh
        | Opcode::Asinh
        | Opcode::Acosh
        | Opcode::Atanh
        | Opcode::Hypot
        | Opcode::Pow
        | Opcode::Atan2 => Some(TY_REAL),
        Opcode::Clog2 => Some(TY_INT),

        Opcode::OptBarrier | Opcode::Br | Opcode::Jmp | Opcode::Call | Opcode::Phi => None,
    }
}

const TY_INT: &str = "i32";
