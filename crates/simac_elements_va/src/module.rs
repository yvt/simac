//! Lowering [`CompiledModule`] to Rust
use anyhow::Result;
use fn_formats::DisplayFmt;
use fxhash::{FxHashMap, FxHashSet};
use itertools::enumerate;
use openvaf::{
    hir::{CompilationDB, Parameter, Type},
    hir_lower::{CallBackKind, LimitState, ParamKind, PlaceKind},
    mir::{F_ZERO, FuncRef, Param, Value},
    mir_interpret::{self, Data},
    sim_back::{CompiledModule, SimUnknownKind, dae::SimUnknown},
};
use std::fmt::Write as _;
use try_match::match_or_default;
use typed_index_collections::TiVec;

use crate::{
    func::EvalStrategy,
    utils::rust::{REAL_ZERO, lit_f64, lit_str, real},
};

pub(crate) fn module_to_rust(
    out: &mut String,
    db: &CompilationDB,
    literals: &lasso::Rodeo,
    module: &CompiledModule,
) -> Result<()> {
    Emitter {
        out,
        db,
        literals,
        module,
    }
    .emit()
}

macro_rules! w {
    ($emitter:expr, $($tt:tt)*) => {
        write!($emitter.out, $($tt)*).unwrap()
    };
}

macro_rules! wln {
    ($emitter:expr, $($tt:tt)*) => {
        writeln!($emitter.out, $($tt)*).unwrap()
    };
}

const ELEM_TRAIT: &str = "crate::ElemTrait";
const ELEM_PARAM_I: &str = "crate::ElemParamI";
const EVAL_CX: &str = "crate::EvalCx";
const JNZ: &str = "crate::Jnz";
const NODE: &str = "crate::Node";
const PARAM_META: &str = "crate::ParamMeta";
const SIM_CX: &str = "crate::SimCx";

struct Emitter<'out, 'db> {
    out: &'out mut String,
    db: &'db CompilationDB,
    literals: &'db lasso::Rodeo,
    module: &'db CompiledModule<'db>,
}

impl Emitter<'_, '_> {
    fn emit(&mut self) -> Result<()> {
        let Self { db, module, .. } = *self;
        let name = module.info.module.name(db);

        let doc = format!(
            "An element translated from Verilog-A code.\n\n\
        Defined in `{}`",
            db.compilation_unit().name(db)
        );
        wln!(self, "#[doc = {}]", lit_str(&doc));
        wln!(self, "#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]");
        wln!(self, "pub struct {name};");

        wln!(self, "impl<Cx: {SIM_CX}> {ELEM_TRAIT}<Cx> for {name} {{");

        // `ElemTrait::nodes`
        // ----------------------------------------------------------------
        wln!(self, "fn nodes(&self) -> &[{NODE}] {{ const {{ &[");
        for (unknown, kind) in module.dae_system.unknowns.iter_enumerated() {
            let residual = &module.dae_system.residual[unknown];

            w!(self, "{NODE}::new()");

            if let SimUnknownKind::KirchoffLaw(node) = kind {
                if node.is_port(db) {
                    w!(self, ".as_terminal()");
                }
            }

            // [tag:va_residual_react_nonzero]
            if residual.react != F_ZERO || residual.react_lim_rhs != F_ZERO {
                w!(self, ".with_react()");
            }

            wln!(self, ",");
        }
        wln!(self, "] }} }}");

        let unknown_to_node_i = |u: SimUnknown| usize::from(u);

        // `ElemTrait::jnzs`
        // ----------------------------------------------------------------
        wln!(self, "fn jnzs(&self) -> &[{JNZ}] {{ const {{ &[");
        for entry in module.dae_system.jacobian.iter() {
            w!(
                self,
                "{JNZ}::new({}, {})",
                unknown_to_node_i(entry.row),
                unknown_to_node_i(entry.col)
            );

            // [tag:va_jnz_resist]
            if entry.resist != F_ZERO {
                w!(self, ".with_resist()");
            }

            // [tag:va_jnz_react]
            if entry.react != F_ZERO {
                w!(self, ".with_react()");
            }

            wln!(self, ",");
        }
        wln!(self, "] }} }}");

        // `ElemTrait::num_params`
        // ----------------------------------------------------------------
        let params = &module.info.params;
        wln!(self, "fn num_params(&self) -> usize {{ {} }}", params.len());

        // `ElemTrait::param_meta`
        // ----------------------------------------------------------------

        // Get the default parameter values by evaluating `module.
        // model_param_setup` with no parameters given
        // TODO: Correctly calculate the default values that are dependent on
        // other parameters [tag:va_todo_nontrivial_default_param_vals]
        let default_param_vals = default_param_vals(db, module)?;

        wln!(
            self,
            "fn param_meta(&self, param_i: {ELEM_PARAM_I}) -> Option<{PARAM_META}<'_>> {{"
        );
        wln!(self, "match param_i {{");
        for (i, (param, param_info)) in params.iter().enumerate() {
            wln!(self, "{i} => Some({PARAM_META} {{");
            wln!(self, "name: {},", lit_str(&param_info.name));
            wln!(self, "description: {},", lit_str(&param_info.description));
            wln!(self, "default_val: {},", lit_f64(default_param_vals[param]));
            wln!(self, "}}),");
        }
        wln!(self, "_ => None,");
        wln!(self, "}}");
        wln!(self, "}}");

        // `ElemTrait::num_states`
        // ----------------------------------------------------------------

        let num_states = module.intern.lim_state.len();
        wln!(self, "fn num_states(&self) -> usize {{ {num_states} }}");

        let limit_state_to_state_i = |i: LimitState| usize::from(i);

        // Function evaluation helpers
        // ----------------------------------------------------------------

        let param_expr_and_eval_strategy = |kind: &ParamKind| {
            Ok(match kind {
                ParamKind::Param(parameter) => {
                    let param_i = module
                        .info
                        .params
                        .get_index_of(parameter)
                        .unwrap_or_else(|| panic!("parameter not found: {parameter:?}"));
                    let ty = parameter.ty(db);
                    let expr = match ty {
                        Type::Real => format!("cx.param({param_i})"),
                        Type::Integer => {
                            format!(
                                "num_traits::ToPrimitive::to_i32(&cx.param({param_i})).unwrap()"
                            )
                        }
                        // [ref:va_string_param_use]
                        Type::String => anyhow::bail!("string param is not supported"),
                        // `openvaf/osdi/src/metadata.rs` says the rest is
                        // unreachable
                        _ => unreachable!(),
                    };
                    // Evaluate it eagerly to avoid repeated bounds checking
                    // mixed with computation code.
                    (expr, EvalStrategy::Eager)
                }
                ParamKind::Voltage { .. }
                | ParamKind::Current(_)
                | ParamKind::ImplicitUnknown(_) => {
                    unreachable!("operating point is unavailable in `init`")
                }
                ParamKind::ParamSysFun(openvaf::hir::ParamSysFun::mfactor) => {
                    (real(1).to_string(), EvalStrategy::OnDemand)
                }
                // TODO: Temperature parameter
                ParamKind::Temperature => (real(298).to_string(), EvalStrategy::OnDemand),
                // TODO: `ParamKind::ParamGiven`
                ParamKind::ParamGiven { .. } => ("true".to_string(), EvalStrategy::OnDemand),
                // TODO: `ParamKind::PortConnected`
                ParamKind::PortConnected { .. } => ("true".to_string(), EvalStrategy::OnDemand),
                ParamKind::EnableLim => ("true".to_string(), EvalStrategy::OnDemand),
                // [tag:va_todo_abstime]
                ParamKind::ParamSysFun(_)
                | ParamKind::Abstime
                | ParamKind::HiddenState(_)
                | ParamKind::EnableIntegration
                | ParamKind::PrevState(_)
                | ParamKind::NewState(_) => {
                    anyhow::bail!("unsupported module param kind: {kind:?}")
                }
            })
        };

        let callback_name = |kind: &CallBackKind| match kind {
            CallBackKind::SimParam | CallBackKind::SimParamOpt | CallBackKind::SimParamStr => {
                Some("(/* TODO: {kind:?} */)".to_owned())
            }
            // "If these derivative were non zero they would have been
            // removed" -- openvaf/osdi/src/compilation_unit.rs
            CallBackKind::Derivative(_) | CallBackKind::NodeDerivative(_) => {
                Some(format!("(|_| {REAL_ZERO})"))
            }
            CallBackKind::Print { .. } => None, // TODO [tag:va_todo_print]
            CallBackKind::StoreLimit(limit_state) => {
                let state_i = limit_state_to_state_i(*limit_state);
                Some(format!("(|x| {{ cx.set_state({state_i}, x); x }})"))
            }
            CallBackKind::LimDiscontinuity => Some("cx.prevent_convergence".to_owned()),
            CallBackKind::BuiltinLimit { name, num_args } => {
                // Call a builtin limiting function [ref:elements_limit_fns]
                let name = self.literals.resolve(name);
                let args = DisplayFmt(|f| {
                    for i in 0..*num_args {
                        write!(f, "arg{i}, ")?;
                    }
                    Ok(())
                });
                Some(format!(
                    "(|{args}| {{ \
                        let (x, changed) = crate::limit::{name}({args}); \
                        if changed {{ cx.prevent_convergence(); }} \
                        x \
                    }})"
                ))
            }
            CallBackKind::ParamInfo { .. }
            // TODO: Node collapse [tag:va_todo_node_collapse]
            | CallBackKind::CollapseHint { .. }
            | CallBackKind::Analysis { .. }
            | CallBackKind::TimeDerivative { .. }
            | CallBackKind::WhiteNoise { .. }
            | CallBackKind::FlickerNoise { .. }
            | CallBackKind::NoiseTable { .. } => None,
        };

        // `ElemTrait::eval`
        // ----------------------------------------------------------------
        wln!(self, "fn eval(&self, cx: &mut impl {EVAL_CX}<Cx>) {{");

        // ### Evaluate `module.init` ###

        // TODO: Actually cache the result values and do not evaluate them
        // every time `eval` is called

        for slot in module.init.cache_slots.keys() {
            wln!(self, "let cache_slot_{};", slot.0);
        }

        wln!(self, "{{");

        // Fetch parameters
        let params: TiVec<Param, (String, EvalStrategy)> = module
            .init
            .intern
            .params
            .iter()
            .map(|(kind, &value)| {
                if module.init.func.dfg.value_dead(value) {
                    return Ok(("(/* dead */)".to_owned(), EvalStrategy::OnDemand));
                }
                param_expr_and_eval_strategy(kind)
            })
            .collect::<Result<_>>()?;

        let callbacks: TiVec<FuncRef, Option<String>> = module
            .init
            .intern
            .callbacks
            .iter()
            .map(callback_name)
            .collect();

        let output_values = module.init.cached_vals.keys().copied().collect();

        // Run `module.init` to evaluate the "cached" values
        crate::func::func_to_rust(
            self.out,
            self.literals,
            &module.init.func,
            &params,
            &callbacks,
            &output_values,
        )?;

        // "Cache" the values
        for (&value, &slot) in module.init.cached_vals.iter() {
            wln!(self, "cache_slot_{} = {value};", slot.0);
        }

        wln!(self, "}}");

        // ### Evaluate `module.eval` ###

        wln!(self, "{{");

        // Fetch parameters
        let ref_val = |unknown: SimUnknownKind| {
            let node_i: usize = module
                .dae_system
                .unknowns
                .index(&unknown)
                .unwrap_or_else(|| panic!("unknown not found: {unknown:?}"))
                .into();
            DisplayFmt(move |f| write!(f, "cx.node({node_i})"))
        };

        let mut params: TiVec<Param, (String, EvalStrategy)> = module
            .intern
            .params
            .iter()
            .map(|(kind, &value)| {
                if module.eval.dfg.value_dead(value) {
                    return Ok(("(/* dead */)".to_owned(), EvalStrategy::OnDemand));
                }

                Ok(match kind {
                    ParamKind::Voltage { hi, lo } => {
                        let expr = if let Some(lo) = lo {
                            format!(
                                "{} - {}",
                                ref_val(SimUnknownKind::KirchoffLaw(*hi)),
                                ref_val(SimUnknownKind::KirchoffLaw(*lo))
                            )
                        } else {
                            ref_val(SimUnknownKind::KirchoffLaw(*hi)).to_string()
                        };

                        // Evaluate it eagerly to avoid repeated bounds
                        // checking mixed with computation code.
                        //
                        // This `ParamKind` is going to be unconditionally
                        // evaluated anyway, so there is no point in evaluating
                        // them on demand.
                        (expr, EvalStrategy::Eager)
                    }
                    ParamKind::Current(current_kind) => (
                        ref_val(SimUnknownKind::Current(*current_kind)).to_string(),
                        // Ditto
                        EvalStrategy::Eager,
                    ),
                    ParamKind::ImplicitUnknown(eq) => (
                        ref_val(SimUnknownKind::Implicit(*eq)).to_string(),
                        // Ditto
                        EvalStrategy::Eager,
                    ),
                    ParamKind::PrevState(limit_state) | ParamKind::NewState(limit_state) => {
                        let state_i = limit_state_to_state_i(*limit_state);
                        // This must be evaluated on demand because
                        // `ParamKind::PrevState` can be used to read out a
                        // value written during current invocation.
                        (format!("cx.state({state_i})"), EvalStrategy::OnDemand)
                    }
                    _ => param_expr_and_eval_strategy(kind)?,
                })
            })
            .collect::<Result<_>>()?;

        // Cached values calculated by `module.init` go to `params` too
        for (slot, _) in module.init.cache_slots.iter_enumerated() {
            params.push((format!("cache_slot_{}", slot.0), EvalStrategy::OnDemand));
        }

        let callbacks: TiVec<FuncRef, Option<String>> =
            module.intern.callbacks.iter().map(callback_name).collect();

        // All output values
        let mut output_values: FxHashSet<Value> = <_>::default();

        for residual in &module.dae_system.residual {
            output_values.insert(residual.resist);
            output_values.insert(residual.resist_lim_rhs);

            // [ref:va_residual_react_nonzero]
            if residual.react != F_ZERO || residual.react_lim_rhs != F_ZERO {
                output_values.insert(residual.react);
                output_values.insert(residual.react_lim_rhs);
            }
        }

        for entry in &module.dae_system.jacobian {
            // [ref:va_jnz_resist]
            if entry.resist != F_ZERO {
                output_values.insert(entry.resist);
            }

            // [ref:va_jnz_react]
            if entry.react != F_ZERO {
                output_values.insert(entry.react);
            }
        }

        // Evaluate the output values
        crate::func::func_to_rust(
            self.out,
            self.literals,
            &module.eval,
            &params,
            &callbacks,
            &output_values,
        )?;

        // Store the output values
        for (node_i, (unknown, residual)) in enumerate(module.dae_system.residual.iter_enumerated())
        {
            let neg = if matches!(
                module.dae_system.unknowns[unknown],
                SimUnknownKind::KirchoffLaw(_)
            ) {
                "-"
            } else {
                ""
            };

            let apply_lim = |rhs: Value, lim_rhs: Value| {
                DisplayFmt(move |f| {
                    if lim_rhs != F_ZERO {
                        write!(f, "({rhs} - {lim_rhs})")
                    } else {
                        write!(f, "{rhs}")
                    }
                })
            };

            wln!(
                self,
                "cx.set_residue_resist({node_i}, {neg}{});",
                apply_lim(residual.resist, residual.resist_lim_rhs),
            );

            // [ref:va_residual_react_nonzero]
            if residual.react != F_ZERO || residual.react_lim_rhs != F_ZERO {
                wln!(
                    self,
                    "cx.set_residue_react({node_i}, {neg}{});",
                    apply_lim(residual.react, residual.react_lim_rhs)
                );
            }
        }

        for (jnz_i, entry) in enumerate(&module.dae_system.jacobian) {
            let neg = if matches!(
                module.dae_system.unknowns[entry.row],
                SimUnknownKind::KirchoffLaw(_)
            ) {
                "-"
            } else {
                ""
            };

            // [ref:va_jnz_resist]
            if entry.resist != F_ZERO {
                wln!(self, "cx.set_jnz_resist({jnz_i}, {neg}{});", entry.resist);
            }

            // [ref:va_jnz_react]
            if entry.react != F_ZERO {
                wln!(self, "cx.set_jnz_react({jnz_i}, {neg}{});", entry.react);
            }
        }

        wln!(self, "}}");

        wln!(self, "}}");

        // ----------------------------------------------------------------

        wln!(self, "}}");

        Ok(())
    }
}

/// Evaluate a `model_param_setup` function to try to determine the default
/// values of modle parameters.
///
/// Fails if `model_param_setup` depends on parameters that we do not consider
/// constant.
fn default_param_vals(
    db: &CompilationDB,
    module: &CompiledModule,
) -> Result<FxHashMap<Parameter, f64>> {
    let func = &module.model_param_setup;
    let intern = &module.model_param_intern;

    let output_values: FxHashSet<Value> = intern
        .outputs
        .iter()
        .filter_map(match_or_default!(, (PlaceKind::Param(_), val) => val.expand()))
        .collect();

    let eval_params: TiVec<Param, Data> = intern
        .params
        .iter()
        .map(|(kind, value)| {
            if func.dfg.value_dead(*value) && !output_values.contains(value) {
                return Ok(Data::UNDEF);
            }
            match kind {
                // TODO: Temperature parameter
                ParamKind::Temperature => Ok(298_f64.into()),
                ParamKind::ParamGiven { .. } => Ok(false.into()),
                ParamKind::ParamSysFun(openvaf::hir::ParamSysFun::mfactor) => Ok(1_f64.into()),
                ParamKind::Param(_) => Ok(Data::UNDEF),
                ParamKind::Abstime
                | ParamKind::EnableIntegration
                | ParamKind::EnableLim
                | ParamKind::PrevState(_)
                | ParamKind::NewState(_)
                | ParamKind::Voltage { .. }
                | ParamKind::Current(_)
                | ParamKind::PortConnected { .. }
                | ParamKind::ParamSysFun(_)
                | ParamKind::HiddenState(_)
                | ParamKind::ImplicitUnknown(_) => anyhow::bail!(
                    "model parameter setup function takes \
                    unsupported param kind: {kind:?}"
                ),
            }
        })
        .collect::<Result<_>>()?;

    anyhow::ensure!(
        intern.callback_uses.iter().all(|x| x.is_empty()),
        "model parameter setup function makes use of callback function, which \
        is not supported",
    );

    let mut interpret =
        mir_interpret::Interpreter::new(func, <_>::default(), eval_params.as_slice());
    interpret.run();

    let param_vals = intern
        .outputs
        .iter()
        .filter_map(|(kind, val)| {
            let &PlaceKind::Param(param) = kind else { return None };
            let Some(val) = val.expand() else {
                tracing::info!(
                    ?param,
                    "This parameter has no associated output value; assuming NaN"
                );
                return Some((param, f64::NAN));
            };
            let data: Data = interpret.state.read(val);
            let value = match param.ty(db) {
                Type::Real => data.f64(),
                Type::Integer => data.i32() as f64,
                // String params will fail on use [tag:va_string_param_use]
                Type::String => 0.,
                // `openvaf/osdi/src/metadata.rs` says the rest is
                // unreachable
                _ => unreachable!(),
            };
            Some((param, value))
        })
        .collect();

    Ok(param_vals)
}
