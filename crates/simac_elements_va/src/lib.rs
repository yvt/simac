#![doc = include_str!("../README.md")]
use anyhow::{Context, Result};
use fxhash::FxHashSet;
use itertools::izip;
use openvaf::{
    basedb::lints::LintLevel, hir::diagnostics::ConsoleSink, paths::AbsPathBuf,
    sim_back::collect_modules,
};
use std::path::PathBuf;
use termcolor::{ColorChoice, StandardStream};

pub mod build;
mod cfg2rust;
mod func;
mod module;
mod utils;

#[cfg(test)]
mod tests;

// TODO: Disable `derive(Parser)` unless needed
#[derive(Default, Debug, Clone)]
#[cfg_attr(feature = "clap", derive(clap::Parser))]
pub struct CompileOpts {
    /// Root source file to compile
    pub root_file: PathBuf,
    /// Add an include directory
    #[cfg_attr(feature = "clap", clap(short = 'I'))]
    pub include_dirs: Vec<PathBuf>,
    /// Predefine a macro
    #[cfg_attr(feature = "clap", clap(short = 'D', value_name = "MACRO[=VALUE]"))]
    pub macro_flags: Vec<String>,
    /// Root modules to compile (all if unspecified)
    #[cfg_attr(feature = "clap", clap(short = 'm'))]
    pub modules: Vec<String>,
    /// Lints to ignore
    #[cfg_attr(feature = "clap", clap(short = 'A', long = "allow"))]
    pub lints_allow: Vec<String>,
    /// Make lints warnings
    #[cfg_attr(feature = "clap", clap(short = 'W', long = "warn"))]
    pub lints_warn: Vec<String>,
    /// Make lints errors
    #[cfg_attr(feature = "clap", clap(short = 'E', long = "deny"))]
    pub lints_deny: Vec<String>,
}

impl CompileOpts {
    pub fn new(root_file: PathBuf) -> Self {
        Self {
            root_file,
            ..<_>::default()
        }
    }

    #[inline]
    pub fn with_macro(mut self, m: impl Into<String>) -> Self {
        self.macro_flags.push(m.into());
        self
    }
}

#[derive(Debug, Clone, Copy, thiserror::Error)]
#[error("compilation failed")]
pub struct FatalDiagnosticError;

#[derive(Debug, Clone)]
pub struct CompileOutput {
    pub rust_source: String,
    /// The list of source files used for compilation.
    pub input_files: Vec<AbsPathBuf>,
}

/// Compile a Verilog-A source file to Rust code implementing the interface
/// defined by `simac_elements` crate.
pub fn compile(
    opts: CompileOpts,
    sink: Option<&mut dyn termcolor::WriteColor>,
) -> Result<CompileOutput, FatalDiagnosticError> {
    let mut stderr = None;
    let sink = sink.unwrap_or_else(|| stderr.insert(StandardStream::stderr(ColorChoice::Auto)));

    match compile_inner(opts, Box::new(&mut *sink)) {
        Ok(out) => Ok(out),
        Err(error) if error.is::<FatalDiagnosticError>() => Err(FatalDiagnosticError),
        Err(error) => {
            // Create a dummy `CompilationDB`
            let db = openvaf::hir::CompilationDB::new_virtual("").unwrap();

            // Use it to create a `ConsoleSink` and print the error in the
            // same format
            let mut sink = ConsoleSink::new_with(&db, Box::new(sink));
            sink.print_simple_message(
                openvaf::hir::diagnostics::Severity::Error,
                format!("{error:#}"),
            );

            Err(FatalDiagnosticError)
        }
    }
}

fn compile_inner(
    opts: CompileOpts,
    sink: Box<dyn termcolor::WriteColor + '_>,
) -> Result<CompileOutput> {
    let root_file = opts
        .root_file
        .canonicalize()
        .with_context(|| format!("failed to resovle path '{}'", opts.root_file.display()))?;
    let root_file = AbsPathBuf::assert(root_file);

    let include_dirs: Vec<_> = opts
        .include_dirs
        .iter()
        .map(|path| {
            path.canonicalize()
                .map(AbsPathBuf::assert)
                .with_context(|| format!("failed to resolve path '{}'", path.display()))
        })
        .collect::<Result<_>>()?;

    let lints: Vec<_> = izip!(
        [&opts.lints_allow, &opts.lints_warn, &opts.lints_deny],
        [LintLevel::Allow, LintLevel::Warn, LintLevel::Deny],
    )
    .flat_map(|(lints, level)| lints.iter().map(move |lint| (lint.to_owned(), level)))
    .collect();

    let db =
        openvaf::hir::CompilationDB::new_fs(root_file, &include_dirs, &opts.macro_flags, &lints)?;

    let Some(mut modules) = collect_modules(&db, false, &mut ConsoleSink::new_with(&db, sink))
    else {
        return Err(FatalDiagnosticError.into());
    };

    // List the modules to compile
    if !opts.modules.is_empty() {
        let mut modules_to_include: FxHashSet<String> = opts.modules.into_iter().collect();

        // Keep the elements of `modules` specified by `modules_to_include`.
        // Keep the elements of `modules_to_include` not found in `modules`.
        modules.retain(|m| modules_to_include.remove(&*m.module.name(&db)));

        if !modules_to_include.is_empty() {
            anyhow::bail!("module not found: {modules_to_include:?}");
        }
    }

    let mut literals = lasso::Rodeo::new();

    // Lower the modules to MIR
    let modules: Vec<_> = modules
        .iter()
        .map(|m| openvaf::sim_back::CompiledModule::new(&db, m, &mut literals))
        .collect();

    let mut out = String::with_capacity(1 << 12);

    // Lower the modules to Rust
    for module in &modules {
        tracing::debug!(
            target = concat!(module_path!(), "::dump::compiled_module"),
            ?module.dae_system,
            ?module.eval,
            ?module.intern,
            ?module.init,
            ?module.model_param_setup,
            ?module.model_param_intern,
            module.node_collapse = ?module.node_collapse.pairs().collect::<Vec<_>>(),
        );

        module::module_to_rust(&mut out, &db, &literals, module).with_context(|| {
            format!(
                "failed to compile module `{}`",
                module.info.module.name(&db)
            )
        })?;
    }

    // List all input files
    let vfs = openvaf::basedb::VfsStorage::vfs(&db);
    let input_files = vfs
        .read()
        .iter()
        .filter_map(|(_, path)| Some(path.as_path()?.to_path_buf()))
        .collect();

    Ok(CompileOutput {
        rust_source: out,
        input_files,
    })
}
