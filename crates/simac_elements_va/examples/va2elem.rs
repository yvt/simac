use clap::Parser;

#[derive(Parser)]
struct Args {
    #[clap(flatten)]
    compile: simac_elements_va::CompileOpts,
}

fn main() {
    let args = Args::parse();

    tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(tracing_subscriber::filter::LevelFilter::WARN.into())
                .from_env_lossy(),
        )
        .with_writer(std::io::stderr)
        .with_ansi(is_terminal::is_terminal(std::io::stderr()))
        .init();

    let Ok(out) = simac_elements_va::compile(args.compile, None) else {
        // The details are already printed to stderr
        std::process::exit(1);
    };
    print!("{}", out.rust_source);
}
