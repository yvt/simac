//! Testing utilities

/// Install a `tracing` subscriber for tests.
pub fn init() {
    _ = tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(tracing_subscriber::filter::LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_test_writer()
        .try_init();
}
