/// Implement [`TypedIndex`] for the combination of a newtype index type
/// `$index_ty` and a specific container type `$container_ty`, enabling
/// type-safe indexing by [`IndexExt`][] and [`SlabExt`][].
#[macro_export]
macro_rules! impl_typed_index {
    (impl $(<$Gen:ident>)? TypedIndex<$container_ty:ty> for $index_ty:ty) => {
        impl $(<$Gen>)? $crate::TypedIndex<$container_ty> for $index_ty {
            type Raw = usize;

            #[inline]
            fn from_raw(x: Self::Raw) -> Self {
                Self(x)
            }

            #[inline]
            fn raw(self) -> Self::Raw {
                self.0
            }
        }
    };
}

/// A trait for newtype index types that are meant to be used with a specific
/// container type.
pub trait TypedIndex<Container: ?Sized>: Copy {
    type Raw;

    #[must_use = "this method has no side effects"]
    fn from_raw(x: Self::Raw) -> Self;

    #[must_use = "this method has no side effects"]
    fn raw(self) -> Self::Raw;
}

/// An extension trait for `impl `[`std::ops::Index`].
pub trait IndexExt<Index> {
    type Output: ?Sized;

    #[must_use = "this method has no side effects"]
    fn index_typed(&self, index: Index) -> &Self::Output;
}

/// An extension trait for `impl `[`std::ops::IndexMut`].
pub trait IndexMutExt<Index>: IndexExt<Index> {
    #[must_use = "this method has no side effects"]
    fn index_mut_typed(&mut self, index: Index) -> &mut Self::Output;
}

impl<Index, T> IndexExt<Index> for T
where
    Index: TypedIndex<Self>,
    T: ?Sized + std::ops::Index<Index::Raw>,
{
    type Output = <T as std::ops::Index<Index::Raw>>::Output;

    #[inline]
    fn index_typed(&self, index: Index) -> &Self::Output {
        &self[index.raw()]
    }
}

impl<Index, T> IndexMutExt<Index> for T
where
    Index: TypedIndex<Self>,
    T: ?Sized + std::ops::IndexMut<Index::Raw>,
{
    #[inline]
    fn index_mut_typed(&mut self, index: Index) -> &mut Self::Output
    where
        Self: std::ops::IndexMut<Index::Raw>,
    {
        &mut self[index.raw()]
    }
}

/// An extension trait for [`Vec`].
pub trait VecExt<Index>: Sized + IndexExt<Index>
where
    Index: TypedIndex<Self, Raw = usize>,
{
    type Element;

    /// Get a typed index pointing to the first element of `self`.
    /// Returns `None` if `self` is empty.
    #[must_use = "this method has no side effects"]
    fn first_typed(&self) -> Option<Index>;

    /// Get a typed index pointing to the last element of `self`.
    /// Returns `None` if `self` is empty.
    #[must_use = "this method has no side effects"]
    fn last_typed(&self) -> Option<Index>;

    #[must_use = "this method has no side effects"]
    fn get_typed(&self, i: Index) -> Option<&Self::Element>;

    #[must_use = "this method has no side effects"]
    fn get_mut_typed(&mut self, i: Index) -> Option<&mut Self::Element>;

    #[must_use = "consider using `push` if you don't need the resultant index"]
    fn push_typed(&mut self, x: Self::Element) -> Index;

    fn swap_remove_typed(&mut self, i: Index) -> Self::Element;

    fn iter_typed<'a>(&'a self) -> impl Iterator<Item = (Index, &'a Self::Element)>
    where
        Self::Element: 'a;

    fn iter_mut_typed<'a>(&'a mut self) -> impl Iterator<Item = (Index, &'a mut Self::Element)>
    where
        Self::Element: 'a;

    fn keys_typed<'a>(&self) -> impl Iterator<Item = Index> + 'a
    where
        Index: 'a;
}

impl<Index, T> VecExt<Index> for Vec<T>
where
    Index: TypedIndex<Self, Raw = usize>,
{
    type Element = T;

    #[must_use = "this method has no side effects"]
    fn first_typed(&self) -> Option<Index> {
        (!self.is_empty()).then(|| Index::from_raw(0))
    }

    #[must_use = "this method has no side effects"]
    fn last_typed(&self) -> Option<Index> {
        self.len().checked_sub(1).map(Index::from_raw)
    }

    #[inline]
    fn get_typed(&self, i: Index) -> Option<&Self::Element> {
        self.get(i.raw())
    }

    #[inline]
    fn get_mut_typed(&mut self, i: Index) -> Option<&mut Self::Element> {
        self.get_mut(i.raw())
    }

    #[inline]
    fn push_typed(&mut self, x: Self::Element) -> Index {
        let i = Index::from_raw(self.len());
        self.push(x);
        i
    }

    #[inline]
    fn swap_remove_typed(&mut self, i: Index) -> Self::Element {
        self.swap_remove(i.raw())
    }

    #[inline]
    fn iter_typed<'a>(&'a self) -> impl Iterator<Item = (Index, &'a Self::Element)>
    where
        Self::Element: 'a,
    {
        self.iter()
            .enumerate()
            .map(|(i, x)| (Index::from_raw(i), x))
    }

    #[inline]
    fn iter_mut_typed<'a>(&'a mut self) -> impl Iterator<Item = (Index, &'a mut Self::Element)>
    where
        Self::Element: 'a,
    {
        self.iter_mut()
            .enumerate()
            .map(|(i, x)| (Index::from_raw(i), x))
    }

    #[inline]
    fn keys_typed<'a>(&self) -> impl Iterator<Item = Index> + 'a
    where
        Index: 'a,
    {
        (0..self.len()).map(|i| Index::from_raw(i))
    }
}

/// An extension trait for [`slab::Slab`].
pub trait SlabExt<Index>: Sized + IndexExt<Index>
where
    Index: TypedIndex<Self, Raw = usize>,
{
    type Element;

    #[must_use = "consider using `inesrt` if you don't need the resultant index"]
    fn insert_typed(&mut self, x: Self::Element) -> Index;

    fn remove_typed(&mut self, i: Index) -> Self::Element;

    fn get_typed(&self, i: Index) -> Option<&Self::Element>;

    fn contains_typed(&self, i: Index) -> bool;

    fn iter_typed<'a>(&'a self) -> impl Iterator<Item = (Index, &'a Self::Element)>
    where
        Self::Element: 'a;
}

impl<Index, T> SlabExt<Index> for slab::Slab<T>
where
    Index: TypedIndex<Self, Raw = usize>,
{
    type Element = T;

    #[inline]
    fn insert_typed(&mut self, x: Self::Element) -> Index {
        Index::from_raw(self.insert(x))
    }

    #[inline]
    fn remove_typed(&mut self, i: Index) -> Self::Element {
        self.remove(i.raw())
    }

    #[inline]
    fn get_typed(&self, i: Index) -> Option<&T> {
        self.get(i.raw())
    }

    #[inline]
    fn contains_typed(&self, i: Index) -> bool {
        self.contains(i.raw())
    }

    #[inline]
    fn iter_typed<'a>(&'a self) -> impl Iterator<Item = (Index, &'a Self::Element)>
    where
        Self::Element: 'a,
    {
        self.iter().map(|(i, x)| (Index::from_raw(i), x))
    }
}

/// An extension trait for [`secondary_slab::SecondarySlab`].
pub trait SecondarySlabExt<Index>: Sized + IndexExt<Index>
where
    Index: TypedIndex<Self, Raw = usize>,
{
    type Element;

    fn insert_typed(&mut self, index: Index, x: Self::Element);

    fn remove_typed(&mut self, index: Index) -> Option<Self::Element>;
}

impl<Index, T> SecondarySlabExt<Index> for secondary_slab::SecondarySlab<T>
where
    Index: TypedIndex<Self, Raw = usize>,
{
    type Element = T;

    fn insert_typed(&mut self, index: Index, x: Self::Element) {
        self.insert(index.raw(), x);
    }

    fn remove_typed(&mut self, index: Index) -> Option<Self::Element> {
        self.remove(index.raw())
    }
}

pub mod prelude {
    pub use crate::{IndexExt as _, IndexMutExt as _, SlabExt as _, VecExt as _};
}
