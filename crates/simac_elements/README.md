# Low-level circuit element models

The element models are defined in this crate so that we can generate every model's code in a GPU kernel once we figure out how.


## Verilog-A

<!-- `[tag:elements_va_instructions]` -->
It is possible to define models by [Verilog-A][1] code instead of manually implementing `ElemTrait` trait.
To do this:

1. Add a Verilog-A source file to [`va`](./va) directory.

2. Add it to the list of source files to build. This list can be found in [`build.rs`](./build.rs) file. (`[ref:elements_va_list]`)
   This will generate a type implementing `ElemTrait` trait for every compiled module.

3. Add the generated types to `tt_all_elem_tys!` macro, which can be found in [`src/elem_ty.rs`](./src/elem_ty.rs) file. (`[ref:elements_ty_list]`)

See the [`README.md`](../simac_elements_va/README.md) file of `simac_elements_va` crate (Verilog-A compiler) for information about the language support.


## References

`[tag:nagel75]` L. W. Nagel, “[SPICE2: A Computer Program to Simulate Semiconductor Circuits](https://www2.eecs.berkeley.edu/Pubs/TechRpts/1975/9602.html).” PhD thesis,, EECS Department, University of California, Berkeley, 1975.

[1]: https://en.wikipedia.org/wiki/Verilog-A
