use simac_elements_va::{CompileOpts, build::compile_va};
use std::{env, fmt::Write as _, path::PathBuf};

fn main() {
    let out_dir = PathBuf::from(env::var_os("OUT_DIR").expect("$OUT_DIR not set"));

    _ = tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(tracing_subscriber::filter::LevelFilter::WARN.into())
                .with_env_var("VA_LOG")
                .from_env_lossy(),
        )
        .with_writer(std::io::stderr)
        .try_init();

    // The `mod` statements to import all generated Rust modules
    let mut imports = String::new();
    let mut add_mod = |name: &str| -> PathBuf {
        let path = out_dir.join(format!("{name}.rs"));
        writeln!(imports, "#[path = r##\"{}\"##]", path.display()).unwrap();
        writeln!(imports, "pub mod {name};").unwrap();
        path
    };

    // Common compiler options
    let va_opts = |root_file: &str| CompileOpts::new(root_file.into());

    // Compile Verilog-A modules to Rust source files
    // [tag:elements_va_list]
    compile_va(&add_mod("diode"), va_opts("va/diode.va"));
    compile_va(&add_mod("bjt_npn"), va_opts("va/bjt.va"));
    compile_va(&add_mod("bjt_pnp"), va_opts("va/bjt.va").with_macro("PNP"));

    // Import all generated Rust modules
    // [tag:elements_va_modules]
    let imports_path = out_dir.join("va_modules.rs");
    std::fs::write(imports_path, imports).expect("fs::write");
}
