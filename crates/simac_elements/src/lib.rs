#![doc = include_str!("../README.md")]
#![no_std]
use core::hash::Hash;
use num_traits::{Float, FloatConst, NumCast, One as _, Zero as _};

/// Used by macros
#[doc(hidden)]
pub extern crate tt_call;

#[cfg(any(test, feature = "proptest"))]
extern crate std;

#[cfg(test)]
mod tests;

/// Simulation context
pub trait SimCx: 'static {
    /// The floating point type to represent real values.
    type Real: Float + FloatConst + core::fmt::Debug + Default;
}

/// Evaluation context
pub trait EvalCx<Cx: SimCx> {
    /// Get the specified parameter's value.
    fn param(&self, param_i: ElemParamI) -> Cx::Real;

    /// Get the last assigned value of the specified state variable.
    ///
    /// This method accesses the same storage as [`Self::set_state`]. A written
    /// value becomes immediately visible through this method.
    fn state(&self, state_i: ElemStateI) -> Cx::Real;

    /// Update the specified state variable's value.
    ///
    /// The stored values carry over to subsequent NR iterations.
    /// This is intended to be used to limit changes between iterations to aid
    /// convergence.
    fn set_state(&mut self, state_i: ElemStateI, value: Cx::Real);

    /// Get the specified node's potential from the previous NR iteration
    /// (`v[t+1][i][node_i]`).
    fn node(&self, node_i: ElemNodeI) -> Cx::Real;

    /// Set the specified Jacobian entry of the resistive residual
    /// (`∂I(v[t+1])[i][row_i]/∂v[t+1][i][column_i]`).
    ///
    /// This method must be called exactly once for every [`ElemJnzI`] with a
    /// resistive component.
    fn set_jnz_resist(&mut self, jnz_i: ElemJnzI, value: Cx::Real);

    /// Set the specified Jacobian entry of the reactive residual
    /// (`∂Q(v[t+1])[i][row_i]/∂v[t+1][i][column_i]`).
    ///
    /// This method must be called exactly once for every [`ElemJnzI`] with a
    /// reactive component.
    fn set_jnz_react(&mut self, jnz_i: ElemJnzI, value: Cx::Real);

    /// Set the specified element of the resistive residual
    /// (`I(v[t+1])[i][node_i]`).
    ///
    /// This method can be called up to once for each [`ElemNodeI`].
    fn set_residue_resist(&mut self, node_i: ElemNodeI, value: Cx::Real);

    /// Set the specified element of the reactive residual
    /// (`Q(v[t+1])[i][node_i]`).
    ///
    /// This method can only be called for nodes with
    /// [`Node::has_react_residue`] set.
    /// This method must be called exactly once for every such [`ElemNodeI`].
    fn set_residue_react(&mut self, node_i: ElemNodeI, value: Cx::Real);

    /// Prevent NR iteration from converging.
    fn prevent_convergence(&mut self);
}

/// Stepping context
pub trait StepCx<Cx: SimCx> {
    /// Get the time step.
    fn time_step(&self) -> Cx::Real;

    /// Get the specified parameter's value.
    fn param(&self, param_i: ElemParamI) -> Cx::Real;

    fn node(&self, node_i: ElemNodeI) -> Cx::Real;

    /// Get the last assigned value of the specified state variable.
    ///
    /// This method accesses the same storage as [`Self::set_state`].
    fn state(&self, var_i: ElemStateI) -> Cx::Real;

    /// Update the specified state variable's value.
    fn set_state(&mut self, var_i: ElemStateI, value: Cx::Real);
}

/// A trait for types each of whose instance describes the structure of a
/// logical grouping of atomic circuit elements.
///
/// # DAE
///
/// The relationship between circuit variables is described by a
/// differential-algebraic system of equations (DAE) in the following form:
///
/// ```text
/// I(v) + ddt(Q(v)) = 0
/// ```
///
/// The left hand side is called the *residue*.
/// `I` and `Q` represent the *resistive* and *reactive* parts of the residue,
/// respectively.
///
/// This system is solved using a Newton-Raphson iteration and finite
/// difference.
/// This requires the first-order approximation of `I` and `Q` nearby a given
/// solution candidate `v[t+1][i]` (`t` = time step number, `i` = NR iteration
/// count):
///
/// ```text
/// I(v) ≈ jacobian_resistive(v[t+1][i]) * (v-v[t+1][i]) + residue_resistive(v[t+1][i])
/// Q(v) ≈ jacobian_reactive(v[t+1][i])  * (v-v[t+1][i]) + residue_reactive(v[t+1][i])
/// ```
///
/// [`ElemTrait::eval`] is called to calculate an element's contribution to
/// these coefficients.
///
/// - `residue_resistive` (`I(v[t+1][i])`): [`EvalCx::set_residue_resist`]
///
/// - `residue_reactive` (`Q(v[t+1][i])`): [`EvalCx::set_residue_react`]
///     - Nodes with a non-zero reactive residue must be marked beforehand with
///       [`Node::with_react`].
///
/// - `jacobian_resistive` (`J_I(v[t+1][i])`): [`EvalCx::set_jnz_resist`]
///     - The sparse structure of the Jacobian matrix must be statically
///       defined with [`Self::jnzs`].
///     - Non-zero resistive entries must be marked with [`Jnz::with_resist`].
///
/// - `jacobian_reactive` (`J_Q(v[t+1][i])`): [`EvalCx::set_jnz_react`]
///     - The sparse structure of the Jacobian matrix must be statically
///       defined with [`Self::jnzs`].
///     - Non-zero reactive entries must be marked with [`Jnz::with_react`].
pub trait ElemTrait<Cx: SimCx>:
    'static + Send + Sync + PartialEq + Eq + Hash + core::fmt::Debug
{
    /// Get the number of nodes (including terminals and other simulator
    /// unknowns).
    ///
    /// Do not override this method.
    fn num_nodes(&self) -> usize {
        self.nodes().len()
    }

    /// Get the list of nodes (including terminals and other simulator
    /// unknowns);
    fn nodes(&self) -> &[Node];

    // TODO: "Meta" stuff might be unfit for low-level models
    // FIXME: `node_meta` is not used anywhere, consider removing it
    /// Get metadata about the specified node.
    fn node_meta(&self, node_i: ElemNodeI) -> Option<NodeMeta<'_>> {
        _ = node_i;
        None
    }

    /// Get information about internal connectivity of terminal nodes.
    ///
    /// This information is used by `simac_simulate::layer_autoground` to detect
    /// floating circuit nodes.
    ///
    /// The returned list contains `ElemNodeI::MAX`-delimited lists expressing
    /// a partition of all terminal nodes (nodes having [`Node::is_terminal`]
    /// set) in the element.
    /// A node in a set is considered internally connected to every other node
    /// in the same set.
    ///
    /// The default value `[]` means all terminal nodes are in a single set
    /// (i.e., all nodes are connected to each other).
    fn connected_node_sets(&self) -> &[ElemNodeI] {
        &[]
    }

    /// Get the list of the non-zero Jacobian entries of the DAE describing
    /// this element.
    ///
    /// Duplicates are allowed.
    fn jnzs(&self) -> &[Jnz];

    /// Get the number of parameter slots.
    fn num_params(&self) -> usize {
        0
    }

    /// Get metadata about the specified parameter.
    fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        _ = param_i;
        None
    }

    /// Get the number of state variables.
    fn num_states(&self) -> usize {
        0
    }

    /// Get metadata about the specified state variable.
    fn state_meta(&self, var_i: ElemStateI) -> Option<StateVarMeta<'_>> {
        _ = var_i;
        None
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>);

    fn step(&self, cx: &mut impl StepCx<Cx>) {
        _ = cx
    }
}

/// A node index within an [`ElemTrait`]. Must be in range
/// `0..`[`ElemTrait::num_nodes`].
pub type ElemNodeI = usize;

/// An entry index within [`ElemTrait::jnzs`].
pub type ElemJnzI = usize;

pub type ElemParamI = usize;

pub type ElemStateI = usize;

/// Describes a node in [`ElemTrait`].
///
/// A node represents any simulator unknown.
#[derive(Debug, Clone, Copy)]
pub struct Node {
    pub is_terminal: bool,
    pub has_react_residue: bool,
}

impl Default for Node {
    fn default() -> Self {
        Self::new()
    }
}

impl Node {
    #[inline]
    pub const fn new() -> Self {
        Self {
            is_terminal: false,
            has_react_residue: false,
        }
    }

    /// Mark this node as a terminal node representing the potential of a
    /// circuit node to which it is attached to.
    #[inline]
    #[must_use = "this method does not mutate `self`"]
    pub const fn as_terminal(self) -> Self {
        Self {
            is_terminal: true,
            ..self
        }
    }

    /// Indicate that the reactive residual for this node (`Q(v[t+1])[node_i]`)
    /// may contain a non-zero value.
    #[inline]
    #[must_use = "this method does not mutate `self`"]
    pub const fn with_react(self) -> Self {
        Self {
            has_react_residue: true,
            ..self
        }
    }
}

/// Abstract information about a non-zero Jacobian entry of a matrix stamp
/// produced by [`ElemTrait`].
#[derive(Debug, Clone, Copy)]
pub struct Jnz {
    /// The row position of this entry.
    ///
    /// If this refers to a terminal node ([`Node::is_terminal`]), this entry
    /// contributes to the Kirchhoff's current law equation of that node.
    /// An inflow current should make a positive contribution (and vice versa)
    /// to the residue.
    pub row_i: ElemNodeI,
    /// The column position of this entry.
    pub column_i: ElemNodeI,
    pub has_resist: bool,
    pub has_react: bool,
}

impl Jnz {
    #[inline]
    pub const fn new(row_i: ElemNodeI, column_i: ElemNodeI) -> Self {
        Self {
            row_i,
            column_i,
            has_resist: false,
            has_react: false,
        }
    }

    /// Mark this entry as constant.
    #[inline]
    #[must_use = "this method does not mutate `self`"]
    pub const fn as_const(self) -> Self {
        self // TODO
    }

    /// Mark that the resistive component of this entry is non-zero.
    #[inline]
    #[must_use = "this method does not mutate `self`"]
    pub const fn with_resist(self) -> Self {
        Self {
            has_resist: true,
            ..self
        }
    }

    /// Mark that the reactive component of this entry is non-zero.
    #[inline]
    #[must_use = "this method does not mutate `self`"]
    pub const fn with_react(self) -> Self {
        Self {
            has_react: true,
            ..self
        }
    }
}

/// Node metadata
#[derive(Debug, Clone, Copy)]
pub struct NodeMeta<'a> {
    pub description: &'a str,
}

/// Parameter metadata
#[derive(Debug, Clone, Copy)]
pub struct ParamMeta<'a> {
    pub name: &'a str,
    pub description: &'a str,
    pub default_val: f64,
}

/// State variable metadata
#[derive(Debug, Clone, Copy)]
pub struct StateVarMeta<'a> {
    pub name: &'a str,
    pub description: &'a str,
}

mod elem_ty;
pub use elem_ty::*;

mod limit;

#[macro_use]
mod macros;

pub mod passive;
pub mod source;

mod va_modules {
    #![allow(unused_variables)]
    #![allow(unused_mut)]
    #![allow(unused_parens)]
    #![allow(unused_assignments)]
    #![allow(clippy::eq_op)]
    #![allow(clippy::approx_constant)]
    #![allow(clippy::redundant_closure_call)]

    // [ref:elements_va_modules]
    include!(concat!(env!("OUT_DIR"), "/va_modules.rs"));
}
pub use va_modules::*;
