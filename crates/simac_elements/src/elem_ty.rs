//! The list of all elements provided by this crate
use crate::{ElemNodeI, ElemParamI, ElemTrait, Jnz, Node, NodeMeta, ParamMeta};

/// Enumerate all [`ElemTrait`] instances provided by this crate.
/// <sup>**[tt-call]**</sup>
///
/// # Example
///
/// ```rust
/// use simac_elements::{tt_all_elem_tys, ElemTrait, ElemTy, SimCx};
///
/// fn elem_num_nodes<Cx: SimCx>(ty: ElemTy) -> usize {
///     let mut i = 0;
///     macro_rules! callback {
///         {
///             tys = [{ $( ( $Name:ident, $($path:tt)* ) )* }]
///         } => {
///             match ty {
///                 $( ElemTy::$Name => ElemTrait::<Cx>::num_nodes(&simac_elements::$($path)*), )*
///             }
///         }
///     }
///     tt_call::tt_call! {
///         macro = [{ tt_all_elem_tys }]
///         ~~> callback! {}
///     }
/// }
/// ```
#[macro_export]
macro_rules! tt_all_elem_tys {
    {
        $caller:tt
    } => {
        $crate::tt_call::tt_return! {
            $caller
            tys = [{
                // [tag:elements_ty_list]
                (Diode, diode::Diode)
                (Wire, passive::Wire)
                (Resistor, passive::Resistor)
                (Capacitor, passive::Capacitor)
                (Inductor, passive::Inductor)
                (VoltageSource, source::VoltageSource)
                (CurrentSource, source::CurrentSource)
                (BjtNpn, bjt_npn::Bjt)
                (BjtPnp, bjt_pnp::Bjt)
            }]
        }
    }
}

/// An internal macro to define [`ElemTy`].
macro_rules! define_enum_elem_ty {
    {
        tys = [{ $( ( $Name:ident, $($path:tt)* ) )* }]
    } => {
        /// Specifies one of the (low-level) circuit element types defined by
        /// `simac_elements` (this crate).
        ///
        /// The variants are automatically generated from the output of
        /// [`tt_all_elem_tys!`].
        #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, enum_map::Enum)]
        #[cfg_attr(
            any(test, feature = "proptest"),
            derive(proptest_derive::Arbitrary),
        )]
        pub enum ElemTy {
            $( $Name, )*
        }

        macro_rules! match_each_ty {
            // Call macro in each match arm
            ($self:expr => $cb:ident!()) => {
                match $self {
                    $( Self::$Name => $cb!(crate::$($path)*), )*
                }
            };
            // Call closure/function in each match arm
            ($self:expr, $cb:expr) => {
                match $self {
                    $( Self::$Name => $cb(&crate::$($path)*), )*
                }
            };
        }

        impl core::str::FromStr for ElemTy {
            type Err = ElemTyFromStrError;

            fn from_str(s: &str) -> Result<Self, Self::Err> {
                match s {
                    $( elem_name!($($path)*) => Ok(Self::$Name), )*
                    _ => Err(ElemTyFromStrError),
                }
            }
        }

        impl ElemTy {
            /// Get the element type path, excluding the crate name
            /// (`simac_elements`).
            ///
            /// [`str::parse`] can be used to convert it back to [`ElemTy`].
            pub fn name(self) -> &'static str {
                match self {
                    $( Self::$Name => elem_name!($($path)*), )*
                }
            }
        }
    }
}

macro_rules! elem_name {
    ($first:ident $(:: $rest:ident)*) => {
        concat!(stringify!($first) $(, "::", stringify!($rest))*)
    };
}

tt_call::tt_call! {
    macro = [{ crate::tt_all_elem_tys }]
    ~~> define_enum_elem_ty! {}
}

impl ElemTy {
    pub fn num_nodes(self) -> usize {
        match_each_ty!(self, ElemTrait::<SimCxImpl>::num_nodes)
    }

    pub fn nodes(self) -> &'static [Node] {
        match_each_ty!(self, ElemTrait::<SimCxImpl>::nodes)
    }

    pub fn node_meta(&self, node_i: ElemNodeI) -> Option<NodeMeta<'_>> {
        match_each_ty!(self, |ty| ElemTrait::<SimCxImpl>::node_meta(ty, node_i))
    }

    pub fn connected_node_sets(&self) -> &'static [ElemNodeI] {
        match_each_ty!(self, ElemTrait::<SimCxImpl>::connected_node_sets)
    }

    pub fn jnzs(&self) -> &[Jnz] {
        match_each_ty!(self, ElemTrait::<SimCxImpl>::jnzs)
    }

    pub fn num_params(self) -> usize {
        match_each_ty!(self, ElemTrait::<SimCxImpl>::num_params)
    }

    pub fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        debug_assert!(
            param_i < self.num_params(),
            "{param_i} >= {}",
            self.num_params()
        );

        match_each_ty!(self, |ty| ElemTrait::<SimCxImpl>::param_meta(ty, param_i))
    }

    pub fn param_name(self, param_i: usize) -> Option<&'static str> {
        debug_assert!(
            param_i < self.num_params(),
            "{param_i} >= {}",
            self.num_params()
        );

        match_each_ty!(self, |ty| {
            ElemTrait::<SimCxImpl>::param_meta(ty, param_i).map(|x| x.name)
        })
    }

    /// Get the default value of the specified parameter.
    ///
    /// The semantics of a return value `None` is yet to be defined.
    pub fn param_default_val(self, param_i: usize) -> Option<f64> {
        debug_assert!(
            param_i < self.num_params(),
            "{param_i} >= {}",
            self.num_params()
        );

        match_each_ty!(self, |ty| {
            ElemTrait::<SimCxImpl>::param_meta(ty, param_i).map(|x| x.default_val)
        })
    }

    pub fn num_states(self) -> usize {
        match_each_ty!(self, ElemTrait::<SimCxImpl>::num_states)
    }

    /// Call a closure, passing `&'static impl ElemTrait<Cx>`.
    pub fn apply<F: FnOnceElemTrait<Cx>, Cx: crate::SimCx>(self, f: F) -> F::Output {
        match_each_ty!(self, |ty| f.call(ty))
    }
}

/// The error type for the [`ElemTy`]'s [`core::str::FromStr`] implementation
/// indicating that the provided string does not match any known element type
/// names.
#[derive(Debug, thiserror::Error, PartialEq, Eq, Clone)]
#[error("unknown element type")]
pub struct ElemTyFromStrError;

/// `impl `[`FnOnce`]`(&'static impl `[`ElemTrait`]`<Cx>) -> Output`
pub trait FnOnceElemTrait<Cx: crate::SimCx> {
    type Output;

    fn call<T: ElemTrait<Cx>>(self, elem: &'static T) -> Self::Output;
}

/// A dummy implementation of [`simac_elements::SimCx`] to get access to the
/// methods provided by [`simac_elements::EvalCx`].
struct SimCxImpl;

impl crate::SimCx for SimCxImpl {
    type Real = f64;
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    proptest! {
        /// Check that [`ElemTy::name`] returns a parsable string.
        #[test]
        fn name_roundtrip(ty: ElemTy) {
            crate::tests::init();

            let name = ty.name();
            tracing::info!(?ty, name);

            let ty_new: ElemTy = name.parse()?;
            assert_eq!(ty, ty_new);
        }
    }
}
