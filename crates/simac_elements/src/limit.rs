//! Builtin limiting functions `[tag:elements_limit_fns]`
//!
//! # Function Signatures
//!
//! The functions provided by this module follow this pattern:
//!
//! - Arguments
//!     - `vnew: T` - Value to limit
//!     - `vold: T` - Previous limited value
//!     - Function-specific extra arguments
//!
//! - Return type
//!     - `0: T` - Limited value
//!     - `1: bool` - `true` if limiting was applied
#![allow(dead_code)]
use num_traits::{Float, NumCast};

/// Limit the change of a p-n junction voltage.
///
/// `vte` specifies a step size, which is usually the product of the thermal
/// voltage and the emission coefficient of the junction.
///
/// `vcrit` specifies a critical voltage, which is generally set to the point
/// of minimum radius of curvature: `vte * ln(vte / (Is * sqrt(2)))` (where
/// `Is` is the saturation current).
pub(crate) fn pnjlim<T: Float>(vnew: T, vold: T, vte: T, vcrit: T) -> (T, bool) {
    let one: T = NumCast::from(1).unwrap();
    let two: T = NumCast::from(2).unwrap();

    // Ngspice v13 style limiting
    let vdelta = vnew - vold;
    let vout = if vnew > vcrit && vdelta.abs() > vte + vte {
        if vold.is_sign_positive() {
            // This part was changed from the original to avoid creating
            // discontinuity around `abs(vdelta) ≈ 2*vte`
            let ratio = vdelta.abs() / vte;
            let ratio_new = two + (ratio - one).ln();
            let vdelta_new = (ratio_new * vte).copysign(vdelta);
            vold + vdelta_new
        } else {
            vte * (vnew / vte).ln()
        }
    } else if vnew.is_sign_negative() {
        let vmin = if vold.is_sign_positive() { -vold } else { vold + vold } - one;
        if vnew >= vmin {
            // Limiting is not required
            return (vnew, false);
        }
        vmin
    } else {
        // Limiting is not required
        return (vnew, false);
    };

    (vout, true)
}
