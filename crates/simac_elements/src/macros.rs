/// Construct a const array from pairs of indices and values.
macro_rules! const_array_map {
    (
        $T:ty;
        $([$i:expr] = $value:expr),* $(,)?
    ) => {preinterpret::preinterpret!{{
        struct ValueImpl;

        $(
            impl crate::macros::Value<{ $i }> for ValueImpl {
                type Type = $T;
                const VALUE: Self::Type = $value;
            }
        )*

        [!set! #i = 0]
        const { [
            $(
                [!ignore! $value]
                <ValueImpl as crate::macros::Value<{ #i }>>::VALUE
                [!set! #i = #i + 1]
            ),*
        ] }
    }}};
}

/// Internal mcaro of [`const_array_map!`]. Provides a constant value.
#[diagnostic::on_unimplemented(message = "`const_array_map!` input is missing value at index {I}")]
pub(crate) trait Value<const I: usize> {
    type Type;
    const VALUE: Self::Type;
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_const_array_map() {
        let array = const_array_map! {
            &'static str;
            [2] = "2",
            [0] = "0",
            [1] = "1",
        };
        assert_eq!(array, ["0", "1", "2"]);
    }
}
