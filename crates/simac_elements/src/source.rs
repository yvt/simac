//! Power sources.
use super::*;

/// An independent constant voltage source.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct VoltageSource;

impl VoltageSource {
    /// The node storing the branch current.
    pub const N_I: ElemNodeI = 2;
}

impl<Cx: SimCx> ElemTrait<Cx> for VoltageSource {
    fn nodes(&self) -> &[Node] {
        &const_array_map! {
            Node;
            [0] = Node::new().as_terminal(),
            [1] = Node::new().as_terminal(),
            [VoltageSource::N_I] = Node::new(),
        }
    }

    fn num_params(&self) -> usize {
        1 + Mod::PARAMS.len()
    }

    fn jnzs(&self) -> &[Jnz] {
        const {
            &[
                Jnz::new(0, Self::N_I).with_resist(),
                Jnz::new(1, Self::N_I).with_resist(),
                Jnz::new(Self::N_I, 0).with_resist(),
                Jnz::new(Self::N_I, 1).with_resist(),
            ]
        }
    }

    fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        match param_i {
            0 => Some(ParamMeta {
                name: "V",
                description: "output voltage",
                default_val: 5.0,
            }),
            i => Some(Mod::PARAMS[i - 1]),
        }
    }

    fn num_states(&self) -> usize {
        Mod::NUM_STATES
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>) {
        let v = cx.param(0)
            + Mod {
                param_start_i: 1,
                state_start_i: 0,
            }
            .eval(cx);

        // I(n0) = -i
        cx.set_residue_resist(0, -cx.node(Self::N_I));
        cx.set_jnz_resist(0, -Cx::Real::one());

        // I(n1) = i
        cx.set_residue_resist(1, cx.node(Self::N_I));
        cx.set_jnz_resist(1, Cx::Real::one());

        // V(n0, n1) - v = 0
        cx.set_residue_resist(Self::N_I, cx.node(0) - cx.node(1) - v);
        cx.set_jnz_resist(2, Cx::Real::one());
        cx.set_jnz_resist(3, -Cx::Real::one());
    }

    fn step(&self, cx: &mut impl StepCx<Cx>) {
        Mod {
            param_start_i: 1,
            state_start_i: 0,
        }
        .step(cx);
    }
}

/// An independent constant current source.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct CurrentSource;

impl<Cx: SimCx> ElemTrait<Cx> for CurrentSource {
    fn nodes(&self) -> &[Node] {
        &const_array_map! {
            Node;
            [0] = Node::new().as_terminal(),
            [1] = Node::new().as_terminal(),
        }
    }

    fn num_params(&self) -> usize {
        1 + Mod::PARAMS.len()
    }

    fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        match param_i {
            0 => Some(ParamMeta {
                name: "I",
                description: "output current",
                default_val: 1.0,
            }),
            i => Some(Mod::PARAMS[i - 1]),
        }
    }

    fn num_states(&self) -> usize {
        Mod::NUM_STATES
    }

    fn jnzs(&self) -> &[Jnz] {
        &[]
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>) {
        let i = cx.param(0)
            + Mod {
                param_start_i: 1,
                state_start_i: 0,
            }
            .eval(cx);

        // I(n0) = -i
        cx.set_residue_resist(0, -i);

        // I(n1) = i
        cx.set_residue_resist(1, i);
    }

    fn step(&self, cx: &mut impl StepCx<Cx>) {
        Mod {
            param_start_i: 1,
            state_start_i: 0,
        }
        .step(cx);
    }
}

struct Mod {
    param_start_i: ElemParamI,
    state_start_i: ElemStateI,
}

impl Mod {
    /// Common parameters for voltage/current source modulation.
    const PARAMS: &'static [ParamMeta<'static>] = &[
        // `Self::param_i_wave`
        ParamMeta {
            name: "wave",
            description: "modulation waveform; \
                0 = dc, 1 = sine, 2 = square",
            default_val: 0.,
        },
        // `Self::param_i_amp`
        ParamMeta {
            name: "amp",
            description: "modulation amplitude",
            default_val: 5.,
        },
        // `Self::param_i_freq`
        ParamMeta {
            name: "freq",
            description: "modulation frequency",
            default_val: 1000.,
        },
    ];

    /// The number of common state variables for voltage/current source
    /// modulation.
    const NUM_STATES: usize = 1;

    /// Evaluate the current modulation offset. Should be called from
    /// [`ElemTy::eval`].
    #[inline]
    fn eval<Cx: SimCx>(&self, cx: &mut impl EvalCx<Cx>) -> Cx::Real {
        let wave = cx.param(self.param_i_wave());
        if wave == NumCast::from(0).unwrap() {
            // DC
            Cx::Real::zero()
        } else if wave == NumCast::from(1).unwrap() {
            // Sine
            cx.param(self.param_i_amp()) * (cx.state(self.state_i_phase()) * Cx::Real::TAU()).sin()
        } else if wave == NumCast::from(2).unwrap() {
            // Square
            let half = Cx::Real::one() / NumCast::from(2).unwrap();
            cx.param(self.param_i_amp()) * (half - cx.state(self.state_i_phase())).signum()
        } else {
            if cfg!(debug_assertions) {
                panic!("Invalid source modulation type: {wave:?}");
            }
            Cx::Real::zero()
        }
    }

    /// Update the modulation state. Should be called from [`ElemTy::step`].
    #[inline]
    fn step<Cx: SimCx>(&self, cx: &mut impl StepCx<Cx>) {
        // Advance phase if the waveform is not DC
        if cx.param(self.param_i_freq()) != NumCast::from(0).unwrap() {
            let phase = (cx.state(self.state_i_phase())
                + cx.time_step() * cx.param(self.param_i_freq()))
            .fract();
            cx.set_state(self.state_i_phase(), phase);
        }
    }

    #[inline]
    fn param_i_wave(&self) -> ElemParamI {
        self.param_start_i
    }

    #[inline]
    fn param_i_amp(&self) -> ElemParamI {
        self.param_start_i + 1
    }

    #[inline]
    fn param_i_freq(&self) -> ElemParamI {
        self.param_start_i + 2
    }

    #[inline]
    fn state_i_phase(&self) -> ElemStateI {
        self.state_start_i
    }
}
