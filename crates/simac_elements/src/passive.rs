//! Passive elements.
use super::*;

/// A wire with zero resistance.
///
/// Due to MNA formulation, this requires a third node representing the branch
/// current ([`Self::N_I`]).
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Wire;

impl Wire {
    /// The node storing the branch current.
    pub const N_I: ElemNodeI = 2;
}

impl<Cx: SimCx> ElemTrait<Cx> for Wire {
    fn nodes(&self) -> &[Node] {
        &const_array_map! {
            Node;
            [0] = Node::new().as_terminal(),
            [1] = Node::new().as_terminal(),
            [Wire::N_I] = Node::new(),
        }
    }

    fn jnzs(&self) -> &[Jnz] {
        const {
            &[
                Jnz::new(0, Self::N_I).with_resist(),
                Jnz::new(1, Self::N_I).with_resist(),
                Jnz::new(Self::N_I, 0).with_resist(),
                Jnz::new(Self::N_I, 1).with_resist(),
            ]
        }
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>) {
        // I(n0) = -i
        cx.set_residue_resist(0, -cx.node(Self::N_I));
        cx.set_jnz_resist(0, -Cx::Real::one());

        // I(n1) = i
        cx.set_residue_resist(1, cx.node(Self::N_I));
        cx.set_jnz_resist(1, Cx::Real::one());

        // V(n0, n1) = 0
        cx.set_residue_resist(Self::N_I, cx.node(0) - cx.node(1));
        cx.set_jnz_resist(2, Cx::Real::one());
        cx.set_jnz_resist(3, -Cx::Real::one());
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Resistor;

impl<Cx: SimCx> ElemTrait<Cx> for Resistor {
    fn nodes(&self) -> &[Node] {
        &const_array_map! {
            Node;
            [0] = Node::new().as_terminal(),
            [1] = Node::new().as_terminal(),
        }
    }

    fn jnzs(&self) -> &[Jnz] {
        const {
            &[
                Jnz::new(0, 0).with_resist(),
                Jnz::new(0, 1).with_resist(),
                Jnz::new(1, 0).with_resist(),
                Jnz::new(1, 1).with_resist(),
            ]
        }
    }

    fn num_params(&self) -> usize {
        1
    }

    fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        match param_i {
            0 => Some(ParamMeta {
                name: "R",
                description: "resistance",
                default_val: 1e3,
            }),
            _ => unreachable!(),
        }
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>) {
        let g = cx.param(0).recip();

        let i_ref = g * (cx.node(0) - cx.node(1));

        // I(n0) = -g * V(n0, n1)
        cx.set_residue_resist(0, -i_ref);
        cx.set_jnz_resist(0, -g);
        cx.set_jnz_resist(1, g);

        // I(n1) = g * V(n0, n1)
        cx.set_residue_resist(1, i_ref);
        cx.set_jnz_resist(2, g);
        cx.set_jnz_resist(3, -g);
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Capacitor;

impl<Cx: SimCx> ElemTrait<Cx> for Capacitor {
    fn nodes(&self) -> &[Node] {
        &const_array_map! {
            Node;
            [0] = Node::new().as_terminal().with_react(),
            [1] = Node::new().as_terminal().with_react(),
        }
    }

    fn jnzs(&self) -> &[Jnz] {
        const {
            &[
                Jnz::new(0, 0).with_react(),
                Jnz::new(0, 1).with_react(),
                Jnz::new(1, 0).with_react(),
                Jnz::new(1, 1).with_react(),
            ]
        }
    }

    fn num_params(&self) -> usize {
        1
    }

    fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        match param_i {
            0 => Some(ParamMeta {
                name: "C",
                description: "capacitance",
                default_val: 1e-6,
            }),
            _ => unreachable!(),
        }
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>) {
        // i = C * v̇
        let c = cx.param(0);

        let q = c * (cx.node(0) - cx.node(1));

        // I(n0) = -C * ddt(V(n0, n1))
        cx.set_residue_react(0, -q);
        cx.set_jnz_react(0, -c);
        cx.set_jnz_react(1, c);

        // I(n1) = C * ddt(V(n0, n1))
        cx.set_residue_react(1, q);
        cx.set_jnz_react(2, c);
        cx.set_jnz_react(3, -c);
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Inductor;

impl Inductor {
    /// The node storing the branch current.
    const N_I: ElemNodeI = 2;
}

impl<Cx: SimCx> ElemTrait<Cx> for Inductor {
    fn nodes(&self) -> &[Node] {
        &const_array_map! {
            Node;
            [0] = Node::new().as_terminal(),
            [1] = Node::new().as_terminal(),
            [Inductor::N_I] = Node::new().with_react(),
        }
    }

    fn jnzs(&self) -> &[Jnz] {
        const {
            &[
                Jnz::new(0, Self::N_I).with_resist(),
                Jnz::new(1, Self::N_I).with_resist(),
                Jnz::new(Self::N_I, 0).with_resist(),
                Jnz::new(Self::N_I, 1).with_resist(),
                Jnz::new(Self::N_I, Self::N_I).with_react(),
            ]
        }
    }

    fn num_params(&self) -> usize {
        1
    }

    fn param_meta(&self, param_i: ElemParamI) -> Option<ParamMeta<'_>> {
        match param_i {
            0 => Some(ParamMeta {
                name: "L",
                description: "inductance",
                default_val: 1e-6,
            }),
            _ => unreachable!(),
        }
    }

    fn eval(&self, cx: &mut impl EvalCx<Cx>) {
        let l = cx.param(0);

        // I(n0) = -i
        cx.set_residue_resist(0, -cx.node(Self::N_I));
        cx.set_jnz_resist(0, -Cx::Real::one());

        // I(n1) = i
        cx.set_residue_resist(1, cx.node(Self::N_I));
        cx.set_jnz_resist(1, Cx::Real::one());

        // V(n0, n1) - L * ddt(i) = 0
        cx.set_residue_resist(Self::N_I, cx.node(0) - cx.node(1));
        cx.set_jnz_resist(2, Cx::Real::one());
        cx.set_jnz_resist(3, -Cx::Real::one());

        cx.set_residue_react(Self::N_I, -l * cx.node(Self::N_I));
        cx.set_jnz_react(4, -l);
    }
}
