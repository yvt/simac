# `simac_simulate`

Circuit transient simulation core

## References

`[tag:kao72]` W. H. Kao, “Comparison of quasi-Newton methods for the DC analysis of electronic circuits,” Doctoral dissertation, University of Illinois at Urbana-Champaign, 1972.
