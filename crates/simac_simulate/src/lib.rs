#![doc = include_str!("../README.md")]
use simac_elements::{ElemParamI, ElemTy};

mod utils;

pub mod core_host;
pub mod layer_autoground;

#[cfg(test)]
mod tests;

/// Global simulation parameters
#[derive(Debug, Clone)]
pub struct SimParams {
    /// The maximum number of Newton-Raphson iterations.
    pub iterations: usize,
    pub integration_method: IntegrationMethod,
    /// The iteration count upper threshold for adaptive time step control.
    /// If the NR iterations did not converge when this count is reached, the
    /// time size decreases. `[ref:nagel75]`
    pub step_control_hard_iterations: usize,
    /// The iteration count lower threshold for adaptive time step control.
    /// If the NR iterations converge when this count is reached, the time size
    /// increases. `[ref:nagel75]`
    pub step_control_easy_iterations: usize,
    /// The time step used to calculate a finite difference of `ddt(Q(v))` when
    /// [`WorldCore::step`] is called with zero time step.
    pub initial_condition_time_step: f64,
}

impl Default for SimParams {
    fn default() -> Self {
        Self {
            iterations: 5000,
            integration_method: IntegrationMethod::Trapezoid,
            step_control_hard_iterations: 10, // `[ref:nagel75]`
            step_control_easy_iterations: 5,  // `[ref:nagel75]`
            initial_condition_time_step: 1e-14,
        }
    }
}

/// Specifies a numerical integration method to use in transient simulation.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum IntegrationMethod {
    /// The [trapezoidal rule][1].
    ///
    /// [1]: https://en.wikipedia.org/wiki/Trapezoidal_rule_(differential_equations)
    Trapezoid,
    /// The [backward Euler method][1].
    ///
    /// [1]: https://en.wikipedia.org/wiki/Backward_Euler_method
    Euler,
}

/// The error type for [`WorldCore::step`].
#[derive(Debug, Clone, thiserror::Error)]
pub enum StepError {
    #[error("unable to find solution")]
    NoSolution,
}

/// Encapsulates the state of a circuit simulation.
///
/// The state includes:
///
/// - A flattened netlist
/// - Element states
pub trait WorldCore: Send + Sync + std::fmt::Debug {
    /// Advance the time and calculate the new state.
    ///
    /// This method recalculates variables, such as [`Self::node`].
    /// You can call this method with a time step of zero to calculate them
    /// without advancing the time.
    ///
    /// `time_step` specifies the time step size.
    /// If deemed necessary for convergence, the time step may be lowered down
    /// to `min_time_step`.
    ///
    /// If you modify the circuit, you should call this method with a time step
    /// of zero at least once to determine the new operating point. Once it's
    /// done, you can start calling it with non-zero time steps.
    ///
    /// If solution succeeds, this method will return the actual time step as
    /// a value betwen `min_time_step` and `time_step` (inclusive).
    ///
    /// If solution fails, this method will return [`StepError`] and leave the
    /// state unmodified.
    fn step(
        &mut self,
        time_step: f64,
        min_time_step: f64,
        sim_params: &SimParams,
    ) -> Result<f64, StepError>;

    /// Insert a node.
    fn insert_node(&mut self) -> NodeId;

    /// Remove the specified node.
    ///
    /// The behavior is unspecified if there are remaining references to the
    /// node to remove, or if the node to remove doesn't exist.
    fn remove_node(&mut self, node_id: NodeId);

    /// Get the specified node's last calculated value (potential or any
    /// simulator unknown).
    ///
    /// The behavior is unspecified if the node doesn't exist.
    fn node(&self, node_id: NodeId) -> f64;

    /// Insert an element.
    ///
    /// The parameters are initialized with zeros. (They are not initialized
    /// with [`simac_elements::ParamMeta::default_val`].)
    fn insert_elem(&mut self, elem_ty: ElemTy, states: &[f64], node_mappings: &[NodeId]) -> ElemId;

    /// Remove the specified element.
    ///
    /// The behavior is unspecified if the element doesn't exist.
    fn remove_elem(&mut self, elem_id: ElemId);

    /// Modify the specified element parameter.
    ///
    /// The behavior is unspecified if the element doesn't exist or the
    /// parameter index is out of range for the element type.
    fn set_elem_param(&mut self, elem_id: ElemId, param_i: ElemParamI, value: f64);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NodeId(usize);

impl NodeId {
    /// The ground node.
    pub const GROUND: Self = Self(0);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ElemId(usize);
