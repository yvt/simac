//! Host (CPU) implementation of [`WorldCore`]
use std::iter::repeat;

use enum_map::EnumMap;
use num_traits::{NumCast, ToPrimitive as _, Zero as _};
use simac_elements::{ElemNodeI, ElemParamI, ElemTy};
use slab::Slab;
use typed_index::{impl_typed_index, prelude::*};

use crate::{
    ElemId, NodeId, SimParams, StepError, WorldCore,
    utils::{FetchAddExt, VecExt},
};

mod update;

#[derive(Debug)]
pub struct WorldCoreImpl {
    circuit: Circuit,
    /// The last used time step.
    current_time_step: Real,
    /// The maximum integration order for the next iteration.
    /// It is either `1` or `2` for now.
    integration_order: u32,
    /// The cached solver variable mapping.
    vars: Option<update::Vars>,
    /// The cached circuit matrix layout.
    mat_layout: Option<update::MatLayout>,
}

#[derive(Debug)]
struct Circuit {
    nodes: Slab<Node>,
    elems: Slab<Elem>,
    elem_sets: EnumMap<ElemTy, ElemSet>,
}

impl_typed_index!(impl TypedIndex<Slab<Node>> for NodeId);
impl_typed_index!(impl TypedIndex<Slab<Elem>> for ElemId);

/// A node.
#[derive(Debug)]
struct Node {
    /// The last calculated voltage of this node.
    voltage: Real,
    /// The number of references to this node in [`ElemSet::node_id_list`].
    elem_ref_count: usize,
}

/// An elemeent.
#[derive(Debug)]
struct Elem {
    elem_ty: ElemTy,
    ty_elem_i: TyElemI,
}

/// All data specific to a particular [`ElemTy`].
#[derive(Debug)]
struct ElemSet {
    /// The list of [`ElemNodeI`]
    /// with [`simac_elements::Node::has_react_residue`] set.
    node_i_list_react: Vec<ElemNodeI>,
    /// The reverse mapping of [`Self::node_i_list_react`]
    node_i_list_react_rev: Vec<usize>,
    /// All elements of this type.
    ty_elems: Vec<TyElem>,
    /// `node_id_list[ty_elem_i * elem_ty.num_nodes() + node_i]`
    node_id_list: Vec<NodeId>,
    /// `param_list[ty_elem_i * elem_ty.num_params() + param_i]`
    param_list: Vec<Real>,
    /// `state_list[ty_elem_i * elem_ty.num_states() + state_i]`
    state_list: Vec<Real>,
    /// `residue_react_history[ty_elem_i * node_i_list_react.len() + i]`
    /// Each element holds `[Q(v)[t], dQ(v)[t]]`.
    ///
    /// We store each element's contribution to a node separately to support
    /// dynamic element insertion/removal.
    residue_react_history: Vec<[Real; 2]>,
}

/// An elemeent in [`ElemSet::ty_elems`].
#[derive(Debug)]
struct TyElem {
    elem_id: ElemId,
}

/// [`ElemSet::ty_elems`]: a per-type element index.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct TyElemI(usize);

impl_typed_index!(impl TypedIndex<Vec<TyElem>> for TyElemI);

impl Default for WorldCoreImpl {
    fn default() -> Self {
        let circuit = Circuit {
            nodes: Slab::new(),
            elems: Slab::new(),
            elem_sets: EnumMap::from_fn(|ty: ElemTy| ElemSet {
                node_i_list_react: ty
                    .nodes()
                    .iter()
                    .enumerate()
                    .filter_map(|(i, node)| node.has_react_residue.then_some(i))
                    .collect(),
                node_i_list_react_rev: ty
                    .nodes()
                    .iter()
                    .scan(0, |react_node_i, node| {
                        Some(if node.has_react_residue {
                            react_node_i.fetch_add(1)
                        } else {
                            usize::MAX
                        })
                    })
                    .collect(),
                ty_elems: Vec::new(),
                node_id_list: Vec::new(),
                param_list: Vec::new(),
                state_list: Vec::new(),
                residue_react_history: Vec::new(),
            }),
        };

        let mut this = Self {
            circuit,
            current_time_step: Real::zero(),
            integration_order: 1,
            vars: None,
            mat_layout: None,
        };
        assert_eq!(this.insert_node(), NodeId::GROUND);
        this
    }
}

impl WorldCore for WorldCoreImpl {
    fn step(
        &mut self,
        time_step: f64,
        min_time_step: f64,
        sim_params: &SimParams,
    ) -> Result<f64, StepError> {
        self.step_inner(time_step, min_time_step, sim_params)
    }

    fn insert_node(&mut self) -> NodeId {
        self.invalidate_solver_cache();

        self.circuit.nodes.insert_typed(Node {
            voltage: Real::zero(),
            elem_ref_count: 0,
        })
    }

    fn remove_node(&mut self, node_id: NodeId) {
        assert_ne!(node_id, NodeId::GROUND, "unable to remove ground node");
        let node = self.circuit.nodes.remove_typed(node_id);

        // Removing a node with outstanding references is a precondition
        // violation.
        debug_assert_eq!(node.elem_ref_count, 0, "removed node still has references");

        self.invalidate_solver_cache();
    }

    fn node(&self, node_id: NodeId) -> f64 {
        self.circuit
            .nodes
            .index_typed(node_id)
            .voltage
            .to_f64()
            .unwrap()
    }

    fn insert_elem(&mut self, elem_ty: ElemTy, states: &[f64], node_mappings: &[NodeId]) -> ElemId {
        let Circuit {
            elems,
            elem_sets,
            nodes,
        } = &mut self.circuit;

        let elem_set = &mut elem_sets[elem_ty];
        assert_eq!(states.len(), elem_ty.num_states());
        assert_eq!(node_mappings.len(), elem_ty.num_nodes());

        // Add the element to `Self::elems`
        let elem_id = elems.insert_typed(Elem {
            elem_ty,
            ty_elem_i: TyElemI(0), // will set later
        });

        // Add the element to `ElemSet`
        let ty_elem_i = elem_set.ty_elems.push_typed(TyElem { elem_id });

        elems.index_mut_typed(elem_id).ty_elem_i = ty_elem_i;

        for &node_id in node_mappings {
            nodes.index_mut_typed(node_id).elem_ref_count += 1;
        }

        elem_set.node_id_list.extend_from_slice(node_mappings);

        elem_set
            .param_list
            .extend(repeat(Real::zero()).take(elem_ty.num_params()));

        elem_set.state_list.extend(states.iter().copied().map(real));

        elem_set
            .residue_react_history
            .extend(repeat([Real::zero(); 2]).take(elem_set.node_i_list_react.len()));

        self.invalidate_solver_cache();

        elem_id
    }

    fn remove_elem(&mut self, elem_id: ElemId) {
        let Circuit {
            elems,
            elem_sets,
            nodes,
        } = &mut self.circuit;

        // Remove it from `Self::elems`
        let elem = elems.remove_typed(elem_id);

        let num_nodes = elem.elem_ty.num_nodes();
        let num_params = elem.elem_ty.num_params();
        let num_states = elem.elem_ty.num_states();

        // Get the `ElemSet` containing it
        let elem_set = &mut elem_sets[elem.elem_ty];

        // Decrement `Node::elem_ref_count`
        for &node_id in &elem_set.node_id_list[elem.ty_elem_i.0 * num_nodes..][..num_nodes] {
            nodes.index_mut_typed(node_id).elem_ref_count -= 1;
        }

        // Remove it from the `ElemSet`
        let was_not_last_ty_elem = elem.ty_elem_i != elem_set.ty_elems.last_typed().unwrap();

        elem_set.ty_elems.swap_remove_typed(elem.ty_elem_i);
        elem_set
            .node_id_list
            .swap_remove_range(elem.ty_elem_i.0 * num_nodes..(elem.ty_elem_i.0 + 1) * num_nodes);
        elem_set
            .param_list
            .swap_remove_range(elem.ty_elem_i.0 * num_params..(elem.ty_elem_i.0 + 1) * num_params);
        elem_set
            .state_list
            .swap_remove_range(elem.ty_elem_i.0 * num_states..(elem.ty_elem_i.0 + 1) * num_states);
        let num_react_nodes = elem_set.node_i_list_react.len();
        elem_set.residue_react_history.swap_remove_range(
            elem.ty_elem_i.0 * num_react_nodes..(elem.ty_elem_i.0 + 1) * num_react_nodes,
        );

        if was_not_last_ty_elem {
            // `swap_remove` has moved `last_ty_elem_i` into `elem.ty_elem_i`.
            // Adjust its pointers accordingly.
            let last_ty_elem = elem_set.ty_elems.index_typed(elem.ty_elem_i);
            elems.index_mut_typed(last_ty_elem.elem_id).ty_elem_i = elem.ty_elem_i;
        }

        self.invalidate_solver_cache();
    }

    fn set_elem_param(&mut self, elem_id: ElemId, param_i: ElemParamI, value: f64) {
        let Circuit {
            elems, elem_sets, ..
        } = &mut self.circuit;

        let elem = elems.index_typed(elem_id);
        let elem_set = &mut elem_sets[elem.elem_ty];
        debug_assert!(param_i < elem.elem_ty.num_params());
        elem_set.param_list[elem.ty_elem_i.0 * elem.elem_ty.num_params() + param_i] = real(value);
    }
}

impl WorldCoreImpl {
    /// Invalidate the solver cache.
    ///
    /// This method should be called whenever an element or node is inserted
    /// or removed.
    fn invalidate_solver_cache(&mut self) {
        self.vars = None;
        self.mat_layout = None;
    }
}

/// The internal operating precision of this module.
///
/// Change this type alias to experiment with other numeric precisions.
type Real = f64;

struct SimCxImpl;

impl simac_elements::SimCx for SimCxImpl {
    type Real = Real;
}

/// Construct [`Real`] from [`f64`].
#[inline]
fn real(x: f64) -> Real {
    NumCast::from(x).unwrap()
}
