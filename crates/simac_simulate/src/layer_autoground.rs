//! A [`WorldCore`] layer to ground floating subcircuits.
//!
//! # Notes
//!
//! `[tag:autoground_nonterminal_node]`
//! Non-terminal nodes ([`simac_elements::Node::is_terminal`]` == false`) are
//! assumed already grounded (grounding wires will not be created).
use enum_map::EnumMap;
use itertools::Either::{Left, Right};
use secondary_slab::SecondarySlab;
use simac_elements::ElemTy;
use slab::Slab;
use std::collections::VecDeque;
use typed_index::{
    IndexExt, IndexMutExt, SecondarySlabExt as _, SlabExt, VecExt, impl_typed_index,
};

use crate::{ElemId, NodeId, WorldCore, utils::VecExt as _};

#[derive(Debug)]
pub struct AutoGround<Inner> {
    inner: Inner,
    nodes: SecondarySlab<Node>,
    elems: SecondarySlab<Elem>,
    elem_sets: EnumMap<ElemTy, ElemSet>,
    elem_nodes: Slab<ElemNode>,
    islands: Slab<Island>,
    /// The list of [`Node`]s with no island membership ([`Node::island_i`]`
    /// == `[`IslandI::NONE`]).
    airborne_nodes: intrusive_list::Head<NodeId>,
    /// The list of [`Node`]s marked as dirty.
    dirty_nodes: intrusive_list::Head<NodeId>,
    /// The list of [`Node`]s with [`Node::num_nonterminal_elem_nodes`]` != 0`.
    nonterminal_nodes: intrusive_list::Head<NodeId>,
}

impl_typed_index!(impl TypedIndex<SecondarySlab<Node>> for NodeId);
impl_typed_index!(impl TypedIndex<SecondarySlab<Elem>> for ElemId);
impl_typed_index!(impl TypedIndex<Slab<ElemNode>> for ElemNodeI);
impl_typed_index!(impl TypedIndex<Slab<Island>> for IslandI);

/// A node created in [`AutoGround`].
#[derive(Debug)]
struct Node {
    /// All [`ElemNode`]s attached to this outer node.
    /// Does not take `[ref:autoground_nonterminal_node]` into account.
    elem_node_list: intrusive_list::Head<ElemNodeI>,

    /// The number of [`ElemNode`]s with non-terminal nodes.
    ///
    /// As per `[ref:autoground_nonterminal_node]`, if this value is non-zero,
    /// the node is assumed already grounded.
    num_nonterminal_elem_nodes: usize,
    /// If [`Self::num_nonterminal_elem_nodes`]` != 0`, links to other [`Node`]s
    /// in the linked list headed by [`AutoGround::nonterminal_nodes`].
    siblings_nonterminal: Option<intrusive_list::Link<NodeId>>,

    /// The island that this node is a part of.
    ///
    /// If it is [`IslandI::NONE`], the node does not belong to any island.
    island_i: IslandI,

    /// The parent node in the island spanning tree (rooted by
    /// [`Island::spanning_tree_root_node`]).
    ///
    /// Must be `None` if [`Self::island_i`]` == `[`IslandI::NONE`].
    parent_in_island_spanning_tree: Option<NodeId>,
    /// The child nodes in the island spanning tree (rooted by
    /// [`Island::spanning_tree_root_node`]).
    children_in_island_spanning_tree: intrusive_list::Head<NodeId>,
    /// Links to other [`Node`]s in the linked list headed by
    /// [`Node::children_in_island_spanning_tree`].
    ///
    /// Must be set if and only if [`Self::parent_in_island_spanning_tree`] is.
    siblings_in_island_spanning_tree: Option<intrusive_list::Link<NodeId>>,

    /// If [`Self::island_i`]` != `[`IslandI::NONE`], and the node is marked as
    /// dirty, links to other [`Node`]s in the linked list headed by
    /// [`AutoGround::dirty_nodes`].
    ///
    /// If [`Self::island_i`]` == `[`IslandI::NONE`],
    /// links to other [`Node`]s in the linked list headed by
    /// [`AutoGround::airborne_nodes`].
    ///
    /// # Dirty Flag Invariants
    ///
    /// Nodes with no island membership ([`Self::island_i`]` ==
    /// `[`IslandI::NONE`]) can never be marked as dirty.
    ///
    /// For every pair of nodes adjacent in the circuit, if they belong to
    /// different islands, at least one of them should be marked as dirty.
    siblings_dirty: Option<intrusive_list::Link<NodeId>>,

    /// A temporary variable used by [`AutoGround::flush`].
    uf_node_i: UfNodeI,
}

/// An element created in [`AutoGround`].
#[derive(Debug)]
struct Elem {
    elem_ty: ElemTy,
    /// An index into the [`ElemSet`].
    ty_elem_i: TyElemI,
}

/// All data specific to a particular [`ElemTy`].
#[derive(Debug)]
struct ElemSet {
    /// All elements of this type.
    ty_elems: Vec<TyElem>,
    /// `elem_node_list[ty_elem_i + elem_ty.num_nodes() + node_i]`
    elem_node_list: Vec<ElemNodeI>,
}

/// An elemeent in [`ElemSet::ty_elems`].
#[derive(Debug)]
struct TyElem {
    elem_id: ElemId,
}

/// [`ElemSet::ty_elems`]: a per-type element index.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct TyElemI(usize);

impl_typed_index!(impl TypedIndex<Vec<TyElem>> for TyElemI);

/// A node attached to an [`Elem`]. An element terminal.
#[derive(Debug)]
struct ElemNode {
    elem_id: ElemId,
    /// The attached node.
    node_id: NodeId,
    /// Links to other [`ElemNode`]s in the linked list headed by
    /// [`Node::elem_node_list`].
    siblings_in_node: Option<intrusive_list::Link<ElemNodeI>>,
}

/// [`AutoGround::elem_nodes`]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ElemNodeI(usize);

/// An island - a set of nodes connected by element branches.
#[derive(Debug)]
struct Island {
    num_nodes: usize,
    /// The root node of the spanning tree containing all nodes in the island.
    spanning_tree_root_node: NodeId,
    /// The wire connecting this island to [`NodeId::GROUND`].
    ground_wire: Option<GroundWire>,

    /// A temporary variable used by [`AutoGround::flush`].
    uf_node_i: UfNodeI,
}

/// [`AutoGround::islands`]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct IslandI(usize);

impl IslandI {
    /// The island containing [`NodeId::GROUND`].
    const GROUND: Self = Self(0);

    const NONE: Self = Self(usize::MAX);
}

/// A wire connecting an [`Island`] to [`NodeId::GROUND`].
#[derive(Debug, Clone, Copy)]
struct GroundWire {
    elem_id: ElemId,
    /// The node representing the branch current.
    current_node_id: NodeId,
}

/// A node in the union-find structure used by [`AutoGround::flush`].
#[derive(Debug)]
struct UfNode {
    /// The upper bound for tree height.
    rank: usize,
    /// The parent node.
    parent: UfNodeI,
    /// The [`IslandI`] if this UF node represents an [`Island`].
    /// [`IslandI::NONE`] otherwise.
    island_i: IslandI,
    /// The [`NodeId`] if this UF node represents a [`Node`].
    /// [`NodeId::GROUND`] otherwise.
    node_id: NodeId,
}

/// An index of [`UfNode`].
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct UfNodeI(usize);

impl_typed_index!(impl TypedIndex<Vec<UfNode>> for UfNodeI);

impl UfNodeI {
    const NONE: Self = UfNodeI(usize::MAX);
}

impl<Inner: WorldCore> AutoGround<Inner> {
    pub fn new(inner: Inner) -> Self {
        let mut this = Self {
            inner,
            nodes: SecondarySlab::new(),
            elems: SecondarySlab::new(),
            elem_sets: EnumMap::from_fn(|_| ElemSet {
                ty_elems: Vec::new(),
                elem_node_list: Vec::new(),
            }),
            elem_nodes: Slab::new(),
            islands: Slab::new(),
            airborne_nodes: intrusive_list::Head::default(),
            dirty_nodes: intrusive_list::Head::default(),
            nonterminal_nodes: intrusive_list::Head::default(),
        };
        this.insert_node_inner(NodeId::GROUND);
        assert!(this.islands.contains_typed(IslandI::GROUND));
        this
    }

    /// Create structures for a newly inserted node.
    fn insert_node_inner(&mut self, node_id: NodeId) {
        // Ground this node
        let ground_wire = (node_id != NodeId::GROUND).then(|| {
            let current_node_id = self.inner.insert_node();
            let elem_id = self.inner.insert_elem(
                ElemTy::Wire,
                &[],
                &[NodeId::GROUND, node_id, current_node_id],
            );
            GroundWire {
                elem_id,
                current_node_id,
            }
        });

        // Create a new island containing this node only
        let island_i = self.islands.insert_typed(Island {
            num_nodes: 1,
            spanning_tree_root_node: node_id,
            ground_wire,
            uf_node_i: UfNodeI::NONE,
        });

        tracing::debug!(
            ?node_id,
            ?island_i,
            ?ground_wire,
            "Created a single-node island for a new node"
        );

        // Set `self.nodes[node_id]`
        self.nodes.insert_typed(
            node_id,
            Node {
                elem_node_list: intrusive_list::Head::default(),
                num_nonterminal_elem_nodes: 0,
                siblings_nonterminal: None,
                island_i,
                parent_in_island_spanning_tree: None,
                children_in_island_spanning_tree: intrusive_list::Head::default(),
                siblings_in_island_spanning_tree: None,
                siblings_dirty: None,
                uf_node_i: UfNodeI::NONE,
            },
        );
    }

    /// Ensure that each connected component of the circuit has exactly one
    /// island.
    #[tracing::instrument(skip_all)]
    fn flush(&mut self) {
        // Calculate node/island connectivity
        // -------------------------------------------------------------------

        // We use a union-find structure to track connectivity. Each node's
        // information is stored in `UfNode`.
        //
        // Each island (along with all contained nodes) is treated as a single
        // node as far as this structure is concerned. This allows us to build
        // a full picture of node/island connectivity without iterating over
        // every node in islands.

        // The nodes included in the union-find structure
        let mut uf_nodes: Vec<UfNode> = Vec::new();

        // For every node marked as dirty, clear the dirty flag, and...
        while let Some(node_id1) = self
            .dirty_nodes
            .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_dirty)
            .pop_front()
        {
            // For every circuit edge connected to this node...
            let node1 = self.nodes.index_typed(node_id1);
            let island_i1 = node1.island_i;

            let uf_node_i1 = uf_ensure_init(
                &mut uf_nodes,
                &mut self.islands.index_mut_typed(island_i1).uf_node_i,
                |n| n.island_i = island_i1,
            );

            for_each_neighbor_node(
                node_id1,
                &mut self.nodes,
                &self.elems,
                &self.elem_sets,
                &self.elem_nodes,
                self.nonterminal_nodes,
                |node_id2, nodes| {
                    let node2 = nodes.index_typed(node_id2);
                    let island_i2 = node2.island_i;

                    // If the edge links two distinct islands...
                    if island_i2 == IslandI::NONE || island_i1 == island_i2 {
                        return;
                    }

                    tracing::trace!(
                        node1.id = ?node_id1,
                        node2.id = ?node_id2,
                        node1.island_i = ?island_i1,
                        node2.island_i = ?island_i2,
                        "Found an inter-island edge",
                    );

                    let uf_node_i2 = uf_ensure_init(
                        &mut uf_nodes,
                        &mut self.islands.index_mut_typed(island_i2).uf_node_i,
                        |n| n.island_i = island_i2,
                    );

                    // Merge the UF nodes associated with the nodes
                    uf_union(&mut uf_nodes, uf_node_i1, uf_node_i2);
                },
            );
        }

        // For every island-less node...
        if let Some(mut node_id1) = self.airborne_nodes.first {
            loop {
                let node1 = self.nodes.index_mut_typed(node_id1);
                let next = node1.siblings_dirty.unwrap().next;

                assert_eq!(node1.island_i, IslandI::NONE);

                let uf_node_i1 = uf_ensure_init(&mut uf_nodes, &mut node1.uf_node_i, |n| {
                    n.node_id = node_id1
                });

                // For every circuit edge connected to this node...
                for_each_neighbor_node(
                    node_id1,
                    &mut self.nodes,
                    &self.elems,
                    &self.elem_sets,
                    &self.elem_nodes,
                    self.nonterminal_nodes,
                    |node_id2, nodes| {
                        let node2 = nodes.index_mut_typed(node_id2);
                        let island_i2 = node2.island_i;

                        let uf_node_i2 = if island_i2 == IslandI::NONE {
                            uf_ensure_init(&mut uf_nodes, &mut node2.uf_node_i, |n| {
                                n.node_id = node_id2
                            })
                        } else {
                            uf_ensure_init(
                                &mut uf_nodes,
                                &mut self.islands.index_mut_typed(island_i2).uf_node_i,
                                |n| n.island_i = island_i2,
                            )
                        };

                        tracing::trace!(
                            node1.id = ?node_id1,
                            node2.id = ?node_id2,
                            node2.island_i = ?island_i2,
                            "Found an edge between a node and an island-less node",
                        );

                        // Merge the UF nodes associated with the nodse
                        uf_union(&mut uf_nodes, uf_node_i1, uf_node_i2);
                    },
                );

                if Some(next) == self.airborne_nodes.first {
                    break;
                } else {
                    node_id1 = next;
                }
            }
        }

        // All islands with `UfNode`s
        let island_i_list: Vec<IslandI> = uf_nodes
            .iter()
            .map(|n| n.island_i)
            .filter(|&i| i != IslandI::NONE)
            .collect();

        // Create a root `UfNode` for each connected component of the circuit
        // (excluding the ones occupied by exactly one island, which do not
        // need fixing.)
        let component_start_i = uf_nodes.len();

        for i in 0..uf_nodes.len() {
            let uf_node_i = UfNodeI(i);
            let mut root_i = uf_find(&mut uf_nodes, uf_node_i);
            tracing::trace!(?uf_node_i, ?root_i);

            if root_i.0 < component_start_i {
                // Create a new root `UfNode`
                let comp_i = uf_ensure_init(&mut uf_nodes, &mut { UfNodeI::NONE }, |_| {});
                assert!(comp_i.0 >= component_start_i);

                // And attach `root_i` to it
                uf_nodes.index_mut_typed(root_i).parent = comp_i;

                root_i = comp_i;
            }

            tracing::trace!(
                ?uf_node_i,
                new_root_i = ?root_i,
                comp_i = root_i.0 - component_start_i,
                "The UF node belongs to this connected component",
            );
        }

        struct Component {
            best_island_i: IslandI,
            best_island_score: usize,
        }

        let mut components: Vec<Component> = (component_start_i..uf_nodes.len())
            .map(|_| Component {
                best_island_i: IslandI::NONE,
                best_island_score: 0,
            })
            .collect();

        // Remove all but one island in each connected component
        // -------------------------------------------------------------------

        // Decide which island should remain in each component
        for &island_i in &island_i_list {
            let island = self.islands.index_typed(island_i);
            let island_score = if island_i == IslandI::GROUND {
                // Keep `IslandI::GROUND` no matter what
                usize::MAX
            } else {
                // Keep the largest one (because we have to iterate over all
                // contained nodes to remove an island)
                island.num_nodes
            };
            let root_i = uf_find(&mut uf_nodes, island.uf_node_i);
            let component = &mut components[root_i.0 - component_start_i];
            if island_score > component.best_island_score {
                component.best_island_i = island_i;
                component.best_island_score = island_score;
            }
        }

        // Remove islands
        for &island_i in &island_i_list {
            let island = self.islands.index_typed(island_i);
            let comp_i = uf_find(&mut uf_nodes, island.uf_node_i).0 - component_start_i;
            let best_island_i = components[comp_i].best_island_i;
            if island_i == best_island_i {
                // Keep this one
                tracing::debug!(?island_i, comp_i, "Preserving this island");
                continue;
            }

            tracing::debug!(
                ?island_i,
                ?best_island_i,
                comp_i,
                "Removing this island because there is a better one in the \
                same connected component",
            );

            // Remove the island spanning tree, leaving the nodes island-less
            remove_island_spanning_subtree(
                island.spanning_tree_root_node,
                &mut self.nodes,
                &mut self.islands,
                &mut self.airborne_nodes,
                &mut self.dirty_nodes,
            );

            assert_eq!(self.islands.index_typed(island_i).num_nodes, 0);
        }

        // Expand the remaining islands or create new ones to occupy all nodes
        // -------------------------------------------------------------------

        let new_island_placeholder = || Island {
            num_nodes: 0,
            spanning_tree_root_node: NodeId::GROUND,
            ground_wire: None,
            uf_node_i: UfNodeI::NONE,
        };
        let mut new_island_i: IslandI = self.islands.insert_typed(new_island_placeholder());

        let mut queue = VecDeque::new();
        let mut stack = Vec::new();

        // For every island-less node...
        while let Some(node_id) = self.airborne_nodes.first {
            let mut island_contact: Option<(NodeId, NodeId, IslandI)> = None;

            // Start breadth-first search at `node_id`
            let mut num_nodes = 1;
            queue.push_back(node_id);

            {
                // `node` is the (provisional) root of `new_island_i`
                let node = self.nodes.index_mut_typed(node_id);
                node.island_i = new_island_i;

                self.airborne_nodes
                    .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_dirty)
                    .remove(node_id);
            }

            tracing::debug!(
                ?node_id,
                "Building a spanning tree from this island-less node"
            );

            while let Some(node_id1) = queue.pop_front() {
                let mut children = intrusive_list::Head::new();

                // For each node `node_id2` connected to `node_id1`...
                for_each_neighbor_node(
                    node_id1,
                    &mut self.nodes,
                    &self.elems,
                    &self.elem_sets,
                    &self.elem_nodes,
                    self.nonterminal_nodes,
                    |node_id2, nodes| {
                        let node2 = nodes.index_mut_typed(node_id2);

                        if node2.island_i == new_island_i {
                            // Already visited
                            return;
                        } else if node2.island_i != IslandI::NONE {
                            // Made a contact with an existing island - we'll
                            // deal with this later
                            tracing::trace!(
                                node1.id = ?node_id1,
                                node2.id = ?node_id2,
                                ?node2.island_i,
                                "Encountered a node with island membership",
                            );
                            island_contact = Some((node_id1, node_id2, node2.island_i));
                            return;
                        }

                        tracing::trace!(?node_id1, ?node_id2, "Visiting a node");

                        num_nodes += 1;
                        queue.push_back(node_id2);

                        // Add `node2` as `node1`'s child
                        node2.parent_in_island_spanning_tree = Some(node_id1);
                        children
                            .accessor_typed_mut(nodes, |x| &mut x.siblings_in_island_spanning_tree)
                            .push_back(node_id2);

                        // `node2` is no longer island-less
                        let node2 = nodes.index_mut_typed(node_id2);
                        node2.island_i = new_island_i;

                        self.airborne_nodes
                            .accessor_typed_mut(nodes, |x| &mut x.siblings_dirty)
                            .remove(node_id2);
                    },
                );

                self.nodes
                    .index_mut_typed(node_id1)
                    .children_in_island_spanning_tree = children;
            }

            let Some((node_id1, node_id2, island_i)) = island_contact else {
                // We did not encounter any island - we establish a new island
                // here, which we actually already did by assigning
                // `new_island_i` to the visited nodes. We still need to fill
                // the fields of `new_island_i`.
                let current_node_id = self.inner.insert_node();
                let elem_id = self.inner.insert_elem(
                    ElemTy::Wire,
                    &[],
                    &[NodeId::GROUND, node_id, current_node_id],
                );

                let ground_wire = GroundWire {
                    elem_id,
                    current_node_id,
                };

                tracing::debug!(
                    ?new_island_i,
                    spanning_tree_root_node = ?node_id,
                    num_nodes,
                    ?ground_wire,
                    "Creating a new island"
                );

                *self.islands.index_mut_typed(new_island_i) = Island {
                    num_nodes,
                    spanning_tree_root_node: node_id,
                    ground_wire: Some(ground_wire),
                    uf_node_i: UfNodeI::NONE,
                };

                // Reserve a new `IslandI` to use in the next iteration
                new_island_i = self.islands.insert_typed(new_island_placeholder());

                continue;
            };

            // One of the visited node `node_id1` connects to `node_id2`, which
            // belongs to `island_i`.
            tracing::debug!(
                node1.id = ?node_id1,
                node2.id = ?node_id2,
                node2.island_i = ?island_i,
                num_nodes,
                "Found a path to a node with island membership; \
                attaching the constructed spanning tree to it",
            );

            // Restructure the tree to make `node_id1` the root of
            // `new_island_i`.
            reroot_spanning_tree(node_id1, &mut self.nodes);

            // Reparent `node_id1` to `node_id2`.
            self.nodes
                .index_mut_typed(node_id1)
                .parent_in_island_spanning_tree = Some(node_id2);
            {
                let mut children = self
                    .nodes
                    .index_typed(node_id2)
                    .children_in_island_spanning_tree;
                children
                    .accessor_typed_mut(&mut self.nodes, |x| {
                        &mut x.siblings_in_island_spanning_tree
                    })
                    .push_back(node_id1);
                self.nodes
                    .index_mut_typed(node_id2)
                    .children_in_island_spanning_tree = children;
            }

            // Relabel all descendants of `node_id1` as `island_i`.
            stack.push(node_id1);
            while let Some(node_id) = stack.pop() {
                let node = self.nodes.index_mut_typed(node_id);

                debug_assert_eq!(node.island_i, new_island_i);
                node.island_i = island_i;

                // Visit children
                for (node_id, _) in { node.children_in_island_spanning_tree }
                    .accessor_typed(&self.nodes, |n| &n.siblings_in_island_spanning_tree)
                    .iter()
                {
                    stack.push(node_id);
                }
            }

            self.islands.index_mut_typed(island_i).num_nodes += num_nodes;
        }

        self.islands.remove_typed(new_island_i);

        // Clean up
        // -------------------------------------------------------------------

        // Clear `UfNodeI`
        for uf_node in uf_nodes {
            if uf_node.island_i != IslandI::NONE {
                self.islands.index_mut_typed(uf_node.island_i).uf_node_i = UfNodeI::NONE;
            }

            if uf_node.node_id != NodeId::GROUND {
                self.nodes.index_mut_typed(uf_node.node_id).uf_node_i = UfNodeI::NONE;
            }
        }

        // Remove islands that are now empty
        for &island_i in &island_i_list {
            let island = self.islands.index_mut_typed(island_i);
            if island.num_nodes == 0 {
                let ground_wire = island.ground_wire.expect("should be non-grounded island");

                tracing::debug!(
                    ?island_i,
                    ?ground_wire,
                    "Completely removing this now-empty island",
                );
                self.inner.remove_elem(ground_wire.elem_id);
                self.inner.remove_node(ground_wire.current_node_id);
            } else {
                tracing::trace!(
                    ?island_i,
                    island.num_nodes,
                    "This island is not empty; not removing it"
                );
            }
        }
    }
}

impl<Inner: Default + WorldCore> Default for AutoGround<Inner> {
    fn default() -> Self {
        Self::new(Inner::default())
    }
}

impl<Inner: WorldCore> WorldCore for AutoGround<Inner> {
    #[tracing::instrument(level = "debug", skip(self))]
    fn insert_node(&mut self) -> NodeId {
        let node_id = self.inner.insert_node();
        self.insert_node_inner(node_id);
        node_id
    }

    #[tracing::instrument(level = "debug", skip(self))]
    fn remove_node(&mut self, node_id: NodeId) {
        let &mut Node {
            ref elem_node_list,
            num_nonterminal_elem_nodes,
            siblings_nonterminal,
            island_i,
            parent_in_island_spanning_tree,
            children_in_island_spanning_tree,
            siblings_in_island_spanning_tree,
            siblings_dirty,
            uf_node_i: _,
        } = self.nodes.index_mut_typed(node_id);

        // The node must not have any attached devices
        assert!(elem_node_list.is_empty(), "node is still in use");

        assert_eq!(num_nonterminal_elem_nodes, 0);
        assert!(siblings_nonterminal.is_none());

        // The node must not have a parent in the island spanning tree
        assert!(parent_in_island_spanning_tree.is_none());
        assert!(siblings_in_island_spanning_tree.is_none());

        // The node must not have children in the island spanning tree
        assert!(children_in_island_spanning_tree.is_empty());

        if island_i == IslandI::NONE {
            // Remove the node from `AutoGround::airborne_nodes`
            self.airborne_nodes
                .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_dirty)
                .remove(node_id);
        } else if siblings_dirty.is_some() {
            // Remove the node from `AutoGround::dirty_nodes`
            self.dirty_nodes
                .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_dirty)
                .remove(node_id);
        }

        self.nodes.remove_typed(node_id).unwrap();

        if island_i != IslandI::NONE {
            let island = self.islands.remove_typed(island_i);

            // These assertions should not panic if `node_id` is not `GROUND` and
            // not attached to any elements
            assert_eq!(island.spanning_tree_root_node, node_id);
            let ground_wire = island.ground_wire.expect("should be non-ground island");

            tracing::debug!(?island_i, ?ground_wire, "Removing the grounding wire");
            self.inner.remove_elem(ground_wire.elem_id);
            self.inner.remove_node(ground_wire.current_node_id);
        }

        self.inner.remove_node(node_id);
    }

    #[tracing::instrument(level = "debug", skip(self))]
    fn insert_elem(&mut self, elem_ty: ElemTy, states: &[f64], node_mappings: &[NodeId]) -> ElemId {
        // Insert the element to `Inner`
        let elem_id = self.inner.insert_elem(elem_ty, states, node_mappings);

        // Insert `ElemNode`s
        let elem_set = &mut self.elem_sets[elem_ty];
        elem_set
            .elem_node_list
            .extend(node_mappings.iter().map(|&node_id| {
                let elem_node_i = self.elem_nodes.insert_typed(ElemNode {
                    elem_id,
                    node_id,
                    siblings_in_node: None, // set later
                });

                // Insert the `ElemNode` into `Node::elem_node_list`
                self.nodes
                    .index_mut_typed(node_id)
                    .elem_node_list
                    .accessor_typed_mut(&mut self.elem_nodes, |x| &mut x.siblings_in_node)
                    .push_back(elem_node_i);

                elem_node_i
            }));

        // Insert `TyElem`
        let ty_elem_i: TyElemI = elem_set.ty_elems.push_typed(TyElem { elem_id });

        // Insert `Elem`
        self.elems
            .insert_typed(elem_id, Elem { elem_ty, ty_elem_i });

        // Check if it's possible to merge islands
        for branch in elem_internal_branches(elem_ty) {
            // Find the branch endpoints
            let node_ids = branch.map(|i| node_mappings[i]);
            let nodes = node_ids.map(|node_id| self.nodes.index_typed(node_id));
            let island_i_list = nodes.map(|node| node.island_i);

            // Skip if:
            // - both endpoints belong to the same island; or
            // - one of them does not belong to an island
            if island_i_list[0] == island_i_list[1]
                || island_i_list[0] == IslandI::NONE
                || island_i_list[1] == IslandI::NONE
            {
                continue;
            }

            // Add `node_ids[0]` to `AutoGround::dirty_nodes`
            if nodes[0].siblings_dirty.is_none() {
                tracing::debug!(
                    ?node_ids,
                    ?island_i_list,
                    "Marking a node as dirty because it now connects to a node \
                    belonging to a different island"
                );

                self.dirty_nodes
                    .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_dirty)
                    .push_back(node_ids[0]);
            }
        }

        for (node_i, elem_node) in elem_ty.nodes().iter().enumerate() {
            // Do the same for implicit branches created by
            // `[ref:autoground_nonterminal_node]`
            if elem_node.is_terminal {
                continue;
            }

            let node_id = node_mappings[node_i];
            let node = self.nodes.index_mut_typed(node_id);

            node.num_nonterminal_elem_nodes += 1;
            if node.num_nonterminal_elem_nodes == 1 {
                self.nonterminal_nodes
                    .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_nonterminal)
                    .push_back(node_id);
            }

            let node = self.nodes.index_typed(node_id);

            if node.island_i == IslandI::GROUND {
                tracing::trace!(?node_id, "This non-terminal node is already grounded");
                continue;
            }

            // Add `node_id` to `AutoGround::dirty_nodes`
            if node.siblings_dirty.is_none() {
                tracing::debug!(
                    ?node_id,
                    island_i = ?node.island_i,
                    "Marking a node as dirty because it is not grounded, and \
                    non-terminal node in element are assumed already grounded"
                );

                self.dirty_nodes
                    .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_dirty)
                    .push_back(node_id);
            }
        }

        elem_id
    }

    #[tracing::instrument(level = "debug", skip(self))]
    fn remove_elem(&mut self, elem_id: ElemId) {
        // Remove the element from `Inner`
        self.inner.remove_elem(elem_id);

        // Remove `Elem`
        let Elem { elem_ty, ty_elem_i } = self
            .elems
            .remove_typed(elem_id)
            .expect("invalid element ID");

        let elem_set = &mut self.elem_sets[elem_ty];
        let num_nodes = elem_ty.num_nodes();
        let elem_node_list = &elem_set.elem_node_list[ty_elem_i.0 * num_nodes..][..num_nodes];

        for branch in elem_internal_branches(elem_ty) {
            // Find the branch endpoints
            let mut node_ids =
                branch.map(|i| self.elem_nodes.index_typed(elem_node_list[i]).node_id);
            let mut nodes = node_ids.map(|node_id| self.nodes.index_typed(node_id));

            // If a spanning tree edge exists here, let `nodes[0]` be the one
            // closer to the root.
            if nodes[0].parent_in_island_spanning_tree == Some(node_ids[1]) {
                node_ids.reverse();
                nodes.reverse();
            } else if nodes[1].parent_in_island_spanning_tree == Some(node_ids[0]) {
                // already in order
            } else {
                // No such edge exists here
                continue;
            }

            assert_eq!(nodes[0].island_i, nodes[1].island_i);
            assert_ne!(nodes[0].island_i, IslandI::NONE);

            tracing::debug!(
                node_id = ?node_ids[1],
                island_i = ?nodes[1].island_i,
                "Removing this spanning subtree from the island",
            );

            // Remove the spanning subtree rooted at `nodes[1]`.
            remove_island_spanning_subtree(
                node_ids[1],
                &mut self.nodes,
                &mut self.islands,
                &mut self.airborne_nodes,
                &mut self.dirty_nodes,
            );
        }

        for (node_i, elem_node) in elem_ty.nodes().iter().enumerate() {
            // Do the same for implicit branches created by
            // `[ref:autoground_nonterminal_node]`
            if elem_node.is_terminal {
                continue;
            }

            let node_id = self.elem_nodes.index_typed(elem_node_list[node_i]).node_id;
            let node = self.nodes.index_mut_typed(node_id);

            node.num_nonterminal_elem_nodes -= 1;
            if node.num_nonterminal_elem_nodes == 0 {
                self.nonterminal_nodes
                    .accessor_typed_mut(&mut self.nodes, |x| &mut x.siblings_nonterminal)
                    .remove(node_id);
            }

            let node = self.nodes.index_typed(node_id);

            // If the node is attached to `NodeId::GROUND` in the spanning tree
            if node.parent_in_island_spanning_tree != Some(NodeId::GROUND) {
                continue;
            }

            assert_eq!(node.island_i, IslandI::GROUND);

            // Remove the spanning subtree rooted at `node`.
            remove_island_spanning_subtree(
                node_id,
                &mut self.nodes,
                &mut self.islands,
                &mut self.airborne_nodes,
                &mut self.dirty_nodes,
            );
        }

        // Remove its `ElemNode`s
        for &elem_node_i in elem_node_list {
            let &ElemNode { node_id, .. } = self.elem_nodes.index_typed(elem_node_i);

            // Remove this `ElemNode` from `Node::elem_node_list`
            self.nodes
                .index_mut_typed(node_id)
                .elem_node_list
                .accessor_typed_mut(&mut self.elem_nodes, |x| &mut x.siblings_in_node)
                .remove(elem_node_i);

            // Remove `ElemNode`
            self.elem_nodes.remove_typed(elem_node_i);
        }

        // Remove the element from `ElemSet`
        let was_not_last_ty_elem = ty_elem_i != elem_set.ty_elems.last_typed().unwrap();

        elem_set.ty_elems.swap_remove_typed(ty_elem_i);
        elem_set
            .elem_node_list
            .swap_remove_range(ty_elem_i.0 * num_nodes..(ty_elem_i.0 + 1) * num_nodes);

        if was_not_last_ty_elem {
            // `swap_remove` has moved `last_ty_elem_i` into `ty_elem_i`.
            // Adjust its pointers accordingly.
            let last_ty_elem = elem_set.ty_elems.index_typed(ty_elem_i);
            self.elems.index_mut_typed(last_ty_elem.elem_id).ty_elem_i = ty_elem_i;
        }
    }

    // Forwarded methods
    fn step(
        &mut self,
        time_step: f64,
        min_time_step: f64,
        sim_params: &crate::SimParams,
    ) -> Result<f64, crate::StepError> {
        self.flush();
        self.inner.step(time_step, min_time_step, sim_params)
    }

    fn node(&self, node_id: NodeId) -> f64 {
        self.inner.node(node_id)
    }

    fn set_elem_param(&mut self, elem_id: ElemId, param_i: simac_elements::ElemParamI, value: f64) {
        self.inner.set_elem_param(elem_id, param_i, value)
    }
}

/// Get an iterator producing imaginary branches representing the
/// specified element type's internal node connectivity.
fn elem_internal_branches(elem_ty: ElemTy) -> impl Iterator<Item = [simac_elements::ElemNodeI; 2]> {
    let sets = elem_ty.connected_node_sets();
    if sets.is_empty() {
        let mut root = None;
        Left(
            elem_ty
                .nodes()
                .iter()
                .enumerate()
                .flat_map(move |(i, node)| {
                    if !node.is_terminal {
                        None
                    } else if let Some(root) = root {
                        Some([root, i])
                    } else {
                        // The first terminal node found
                        root = Some(i);
                        None
                    }
                }),
        )
    } else {
        let mut root = None;
        Right(sets.iter().flat_map(move |&i| {
            if i == simac_elements::ElemNodeI::MAX {
                // Moving on to the next set
                root = None;
                None
            } else if let Some(root) = root {
                Some([root, i])
            } else {
                // The first node in the current set
                root = Some(i);
                None
            }
        }))
    }
}

/// Call `f`, passing to it every [`NodeId`] connected to `node_id1`.
fn for_each_neighbor_node(
    node_id1: NodeId,
    nodes: &mut SecondarySlab<Node>,
    elems: &SecondarySlab<Elem>,
    elem_sets: &EnumMap<ElemTy, ElemSet>,
    elem_nodes: &Slab<ElemNode>,
    nonterminal_nodes: intrusive_list::Head<NodeId>,
    mut f: impl FnMut(NodeId, &mut SecondarySlab<Node>),
) {
    for_each_neighbor_elem_node(
        nodes.index_typed(node_id1).elem_node_list,
        elems,
        elem_sets,
        elem_nodes,
        |elem_node_i2| {
            let node_id2 = elem_nodes.index_typed(elem_node_i2).node_id;
            f(node_id2, nodes);
        },
    );

    // `[ref:autoground_nonterminal_node]`
    if nodes.index_typed(node_id1).num_nonterminal_elem_nodes != 0 {
        f(NodeId::GROUND, nodes);
    }

    if node_id1 == NodeId::GROUND {
        if let Some(first) = nonterminal_nodes.first {
            let mut node_id2 = first;
            while {
                f(node_id2, nodes);
                node_id2 = nodes
                    .index_typed(node_id2)
                    .siblings_nonterminal
                    .unwrap()
                    .next;
                node_id2 != first
            } {}
        }
    }
}

/// Call `f`, passing to it every [`ElemNodeI`] connected to [`ElemNodeI`]s in
/// `elem_node_list`.
///
/// Note: This function not take `[ref:autoground_nonterminal_node]` into
/// account.
fn for_each_neighbor_elem_node(
    elem_node_list: intrusive_list::Head<ElemNodeI>,
    elems: &SecondarySlab<Elem>,
    elem_sets: &EnumMap<ElemTy, ElemSet>,
    elem_nodes: &Slab<ElemNode>,
    mut f: impl FnMut(ElemNodeI),
) {
    for (elem_node_i, &ElemNode { elem_id, .. }) in elem_node_list
        .accessor_typed(elem_nodes, |x| &x.siblings_in_node)
        .iter()
    {
        let &Elem {
            elem_ty, ty_elem_i, ..
        } = elems.index_typed(elem_id);

        let num_nodes = elem_ty.num_nodes();

        let elem_set = &elem_sets[elem_ty];

        // The `ElemNodeI`s for this element
        let elem_elem_node_i_list =
            &elem_set.elem_node_list[ty_elem_i.0 * num_nodes..][..num_nodes];

        for branch in elem_internal_branches(elem_ty) {
            // The `ElemNodeI`s for the branch endpoints
            let branch_elem_node_i_list = branch.map(|i| elem_elem_node_i_list[i]);

            // If this branch is attached to `elem_node_i`, find the opposite one
            let neighbor_elem_node_i = if branch_elem_node_i_list[0] == elem_node_i {
                branch_elem_node_i_list[1]
            } else if branch_elem_node_i_list[1] == elem_node_i {
                branch_elem_node_i_list[0]
            } else {
                continue;
            };

            f(neighbor_elem_node_i);
        }
    }
}

/// Remove `node_id` and all descendants from an island spanning tree.
///
/// Note: If `node_id` is a root node, this function does not update
/// [`Island::spanning_tree_root_node`].
fn remove_island_spanning_subtree(
    node_id: NodeId,
    nodes: &mut SecondarySlab<Node>,
    islands: &mut Slab<Island>,
    airborne_nodes: &mut intrusive_list::Head<NodeId>,
    dirty_nodes: &mut intrusive_list::Head<NodeId>,
) {
    // Clear `nodes[node_id].children_in_island_spanning_tree`
    while let Some(child_node_id) = nodes
        .index_typed(node_id)
        .children_in_island_spanning_tree
        .first
    {
        remove_island_spanning_subtree(child_node_id, nodes, islands, airborne_nodes, dirty_nodes);
    }

    let &mut Node {
        ref mut island_i,
        ref mut parent_in_island_spanning_tree,
        siblings_dirty,
        ..
    } = nodes.index_mut_typed(node_id);

    if *island_i == IslandI::NONE {
        // It didn't belong to any island actually
        return;
    }

    let island = islands.index_mut_typed(*island_i);

    *island_i = IslandI::NONE;

    // Remove it from parent
    if let Some(parent_node_id) = parent_in_island_spanning_tree.take() {
        // Unlink `node_id` from the parent's
        // `Node::children_in_island_spanning_tree`.
        //
        // This is a bit complicated due to `intrusive_list` not supporting
        // the pattern where a `Head` is contained by the same collection the
        // `Head` points to.
        let mut children = nodes
            .index_typed(parent_node_id)
            .children_in_island_spanning_tree;
        children
            .accessor_typed_mut(nodes, |x| &mut x.siblings_in_island_spanning_tree)
            .remove(node_id);
        nodes
            .index_mut_typed(parent_node_id)
            .children_in_island_spanning_tree = children;
    } else {
        assert_eq!(island.spanning_tree_root_node, node_id);
        // `node_id` is the root. leave `spanning_tree_root_node` as it is.
    }

    island.num_nodes -= 1;

    if siblings_dirty.is_some() {
        // Remove it from `dirty_nodes`
        dirty_nodes
            .accessor_typed_mut(nodes, |x| &mut x.siblings_dirty)
            .remove(node_id);
    }

    // Add it to `airborne_nodes` because `node.island_i == NONE`
    airborne_nodes
        .accessor_typed_mut(nodes, |x| &mut x.siblings_dirty)
        .push_back(node_id);
}

/// Restructure a spanning tree to designate `node_id` as the new root node.
///
/// Note: This function does not update [`Island::spanning_tree_root_node`].
fn reroot_spanning_tree(node_id: NodeId, nodes: &mut SecondarySlab<Node>) {
    let Some(parent_node_id) = nodes.index_typed(node_id).parent_in_island_spanning_tree else {
        // `node_id` is already a root node
        return;
    };

    // Make `parent_node_id` the new root node. This doesn't change the
    // relationship between `node_id` and `parent_node_id`
    //
    //   parent_node
    //      │   │
    //    ╭─╯   ╰───╮
    //    │         │
    //   ...       node
    //              │
    //           children
    reroot_spanning_tree(parent_node_id, nodes);

    // Remove `node` from `parent_node`
    {
        let mut children = nodes
            .index_typed(parent_node_id)
            .children_in_island_spanning_tree;
        children
            .accessor_typed_mut(nodes, |x| &mut x.siblings_in_island_spanning_tree)
            .remove(node_id);
        nodes
            .index_mut_typed(parent_node_id)
            .children_in_island_spanning_tree = children;
    }

    nodes
        .index_mut_typed(node_id)
        .parent_in_island_spanning_tree = None;

    // Add `parent_node` to `node`
    //
    //         node
    //          ││
    //      ╭───╯╰────╮
    //      │         │
    //  children  parent_node
    //                │
    //               ...
    {
        let mut children = nodes.index_typed(node_id).children_in_island_spanning_tree;
        children
            .accessor_typed_mut(nodes, |x| &mut x.siblings_in_island_spanning_tree)
            .push_back(parent_node_id);
        nodes
            .index_mut_typed(node_id)
            .children_in_island_spanning_tree = children;
    }

    nodes
        .index_mut_typed(parent_node_id)
        .parent_in_island_spanning_tree = Some(node_id);
}

/// If `*p_node_i == `[`UfNodeI::NONE`], insert a [`UfNode`] and assign the new
/// [`UfNodeI`] to it.
#[inline]
fn uf_ensure_init(
    nodes: &mut Vec<UfNode>,
    p_node_i: &mut UfNodeI,
    init: impl FnOnce(&mut UfNode),
) -> UfNodeI {
    if *p_node_i == UfNodeI::NONE {
        let mut node = UfNode {
            rank: 0,
            parent: UfNodeI(nodes.len()),
            island_i: IslandI::NONE,
            node_id: NodeId::GROUND,
        };
        init(&mut node);
        *p_node_i = nodes.push_typed(node);
    }

    *p_node_i
}

/// Find the representative node of the set containing the node `self`
/// points to.
#[inline]
fn uf_find(nodes: &mut Vec<UfNode>, mut n_i: UfNodeI) -> UfNodeI {
    loop {
        let parent_i = nodes.index_typed(n_i).parent;
        if parent_i == n_i {
            return n_i;
        }
        let grandparent_i = nodes.index_typed(parent_i).parent;
        nodes.index_mut_typed(n_i).parent = grandparent_i;
        n_i = parent_i;
    }
}

/// Merge two sets in a union-find structure.
#[inline]
fn uf_union(nodes: &mut Vec<UfNode>, mut n0_i: UfNodeI, mut n1_i: UfNodeI) {
    n0_i = uf_find(nodes, n0_i);
    n1_i = uf_find(nodes, n1_i);

    if n0_i == n1_i {
        return;
    }

    let n0 = nodes.index_typed(n0_i);
    let n1 = nodes.index_typed(n1_i);

    // Ensure that `n0.rank >= n1.rank`
    if n0.rank < n1.rank {
        (n0_i, n1_i) = (n1_i, n0_i);
    }

    // Make `n0` the new root
    if n0.rank == n1.rank {
        nodes.index_mut_typed(n0_i).rank += 1;
    }

    nodes.index_mut_typed(n1_i).parent = n0_i;
}
