use std::{mem::take, ops::Range};

pub(crate) trait SliceExt {
    type Element;

    /// Like `slice::chunks_exact` but returns an infinite iterator if
    /// `n == 0`.
    fn chunks_exact2(&self, n: usize) -> impl Iterator<Item = &Self>;

    /// Like `slice::chunks_exact_mut` but returns an infinite iterator if
    /// `n == 0`.
    fn chunks_exact_mut2(&mut self, n: usize) -> impl Iterator<Item = &mut Self>;
}

impl<T> SliceExt for [T] {
    type Element = T;

    #[inline]
    fn chunks_exact2(mut self: &Self, n: usize) -> impl Iterator<Item = &Self> {
        std::iter::from_fn(move || {
            if self.len() >= n {
                let head;
                (head, self) = self.split_at(n);
                Some(head)
            } else {
                None
            }
        })
    }

    #[inline]
    fn chunks_exact_mut2(mut self: &mut Self, n: usize) -> impl Iterator<Item = &mut Self> {
        std::iter::from_fn(move || {
            if self.len() >= n {
                let head;
                (head, self) = take(&mut self).split_at_mut(n);
                Some(head)
            } else {
                None
            }
        })
    }
}

pub(crate) trait VecExt {
    type Element;

    /// Remove `n` elements from the vector and move the last `n` elements into
    /// the now-vacant place.
    ///
    /// The resulting order is unspecified if the last `n` elements and the
    /// specified range partially overlap with each other.
    fn swap_remove_range(&mut self, index: Range<usize>);
}

impl<T> VecExt for Vec<T> {
    type Element = T;

    fn swap_remove_range(&mut self, index: Range<usize>) {
        for i in index.rev() {
            self.swap_remove(i);
        }
    }
}

pub(crate) trait FetchAddExt<Rhs> {
    fn fetch_add(&mut self, value: Rhs) -> Self;
}

impl<T, Rhs> FetchAddExt<Rhs> for T
where
    T: std::ops::AddAssign<Rhs> + Clone,
{
    #[inline]
    fn fetch_add(&mut self, value: Rhs) -> Self {
        let old_value = self.clone();
        *self += value;
        old_value
    }
}
