//! Solver intermediate state logging
//!
//! Each function uses a different tracing target so that they can
//! be individually filtered in or out.
use itertools::enumerate;

use super::*;

macro_rules! dump {
    ($target:literal, $($tt:tt)*) => {
        tracing::debug!(
            target: concat!(module_path!(), "::", $target),
            $($tt)*
        );
    };
}

macro_rules! dump_fmt {
    ($target:literal, $fmt:expr $(,)?) => {
        dump!($target, "{}", fn_formats::DisplayFmt($fmt),);
    };
}

/// Dump circuit topology in the DOT format. The output can be visualized by
/// Graphviz.
pub(super) fn circuit_dot(circuit: &Circuit) {
    dump_fmt!("circuit_dot", |f| {
        writeln!(f, "Circuit topology:\n")?;

        let nname = |NodeId(i)| format!("n{i}");
        writeln!(f, "  digraph circuit {{")?;
        for (node_id, _) in circuit.nodes.iter_typed() {
            writeln!(
                f,
                "    {} [shape=circle, label={}];",
                nname(node_id),
                node_id.0
            )?;
        }
        for (elem_ty, elem_set) in circuit.elem_sets.iter() {
            for ((ty_elem_i, _ty_elem), node_id_list) in izip!(
                elem_set.ty_elems.iter_typed(),
                elem_set.node_id_list.chunks_exact(elem_ty.num_nodes()),
            ) {
                let TyElemI(i) = ty_elem_i;
                let ename = format!("{elem_ty:?}{i}");
                writeln!(f, "    {ename} [shape=box];")?;
                for (i, &node_id) in enumerate(node_id_list) {
                    let nname = nname(node_id);
                    writeln!(f, "    {ename} -> {nname} [label={i}];")?;
                }
            }
        }
        writeln!(f, "  }}")?;
        Ok(())
    },);
}

/// Dump a system of linear equations to be solved during NR iteration.
pub(super) fn nr_eq(
    circuit: &Circuit,
    vars: &Vars,
    mat: &simac_compute::utils_csc::MatCsc<'_, Real>,
    rhs: &[Real],
) {
    dump_fmt!("nr_eq", |f| {
        writeln!(f, "NR equation:\n")?;

        #[inline]
        fn range(indices: &[usize]) -> std::ops::Range<usize> {
            indices[0]..indices[1]
        }

        let num_vars = mat.m;
        let mut rows: Vec<Vec<(VarIdx, Real)>> = (0..num_vars).map(|_| Vec::new()).collect();
        for (col_i, col_range) in mat.col_start.windows(2).enumerate() {
            let col_row_i = &mat.row_i[range(col_range)];
            let col_val = &mat.val[range(col_range)];
            for (&row_i, &val) in izip!(col_row_i, col_val) {
                rows[row_i].push((col_i, val));
            }
        }

        writeln!(f, "  [{} rows, {} nonzeros]", num_vars, mat.val.len())?;

        for (entries, rhs) in izip!(rows, rhs) {
            write!(f, "  {rhs:+.6e} = ")?;

            let mut first = true;
            for (col_i, val) in entries {
                if first {
                    first = false;
                    write!(f, "{val:+.6e}*")?;
                } else {
                    write!(f, " {val:+.6e}*")?;
                }

                vars.fmt_var_name(col_i, circuit, f)?;
            }

            if first {
                f.write_str("0")?;
            }
            f.write_str("\n")?;
        }

        Ok(())
    });
}

/// Dump the result of a single NR iteration.
pub(super) fn nr_out(
    circuit: &Circuit,
    vars: &Vars,
    ref_var_vals: &[Real],
    delta_var_vals: &[Real],
) {
    dump_fmt!("nr_out", |f| {
        writeln!(f, "NR intermediate solution:\n")?;

        for (i, &ref_val, &delta) in izip!(0.., ref_var_vals, delta_var_vals) {
            let name = fn_formats::DisplayFmt(|f| vars.fmt_var_name(i, circuit, f));
            let new_val = ref_val - delta;
            writeln!(
                f,
                "  {name}[i+1] = {new_val:.6e} \
                ([i] = {ref_val:.6e}, Δ = {delta:.6e})"
            )?;
        }

        Ok(())
    });
}

/// Dump the predicted RHS (residue) in the next NR iteration.
pub(super) fn nr_next_rhs(
    circuit: &Circuit,
    vars: &Vars,
    mat: &simac_compute::utils_csc::MatCsc<'_, Real>,
    rhs: &[Real],
    delta_var_vals: &[Real],
) {
    dump_fmt!("nr_next_rhs", |f| {
        writeln!(f, "Next RHS prediction:\n")?;

        let mut vars2: Vec<Real> = vec![Real::default(); delta_var_vals.len()];
        simac_compute::sgemv_csc::gemv_accumulate(
            mat.col_start,
            mat.row_i,
            mat.val,
            delta_var_vals,
            &mut vars2,
        );

        for (i, &delta, &rhs) in izip!(0.., &vars2, rhs) {
            let name = fn_formats::DisplayFmt(|f| vars.fmt_var_name(i, circuit, f));
            let new_rhs = rhs - delta;
            writeln!(
                f,
                "  Res({name})[i+1] = {new_rhs:.6e} \
                ([i] = {rhs:.6e}, Δ = {delta:.6e})"
            )?;
        }

        Ok(())
    });
}

/// Dump the constituents of reactive residue.
pub(super) fn residue_react(
    residue_react: &[[Real; 2]],
    residue_react_prev: &[[Real; 2]],
    elem_ty: ElemTy,
    elem_set: &super::super::ElemSet,
) {
    let node_i_list_react = elem_set.node_i_list_react.as_slice();

    if node_i_list_react.is_empty() || elem_set.ty_elems.is_empty() {
        return;
    }

    dump_fmt!("residue_react", |f| {
        writeln!(f, "Reactive residue of {elem_ty:?}:\n")?;

        let mut react_node_list = Vec::new();
        let node_i_list_react = elem_set.node_i_list_react.as_slice();

        for ((ty_elem_i, ty_elem), node_id_list, residue_react) in izip!(
            elem_set.ty_elems.iter_typed(),
            elem_set.node_id_list.chunks_exact(elem_ty.num_nodes()),
            residue_react.chunks_exact(node_i_list_react.len()),
        ) {
            let ty_elem_i: TyElemI = ty_elem_i;

            react_node_list.clear();
            react_node_list.extend(
                elem_set
                    .node_i_list_react
                    .iter()
                    .map(|&node_i| node_id_list[node_i]),
            );
            writeln!(f, "  {ty_elem_i:?} {:?}", ty_elem.elem_id)?;

            for (&node_i, &[q, dq], &[q0, dq0]) in
                izip!(node_i_list_react, residue_react, residue_react_prev)
            {
                let NodeId(i) = node_id_list[node_i];
                writeln!(f, "     Q(e[{i}])[i+1] <+ {q:.6e} ([i] = {q0:.6e})")?;
                writeln!(f, "    dQ(e[{i}])[i+1] <+ {dq:.6e} ([i] = {dq0:.6e})")?;
            }
        }

        Ok(())
    });
}
