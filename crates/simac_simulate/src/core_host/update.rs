use std::{fmt, mem::replace};

use enum_map::EnumMap;
use itertools::izip;
use num_traits::{Float, ToPrimitive as _, Zero as _};
use simac_elements::{ElemJnzI, ElemNodeI, ElemTy};
use slab::Slab;
use typed_index::{IndexExt, prelude::*};

use super::{Circuit, Real, TyElemI, WorldCoreImpl, real};
use crate::{
    IntegrationMethod, NodeId, SimParams, StepError,
    utils::{FetchAddExt as _, SliceExt as _},
};

mod dump;

/// The precalculated structure of a circuit matrix (CSC storage format).
#[derive(Debug)]
pub(super) struct MatLayout {
    col_start: Vec<usize>,
    row_i: Vec<usize>,
    /// Contributions to the circuit matrix elements.
    ///
    /// Contains subsequences terminated by [`CONTRIB_I_END`].
    /// `val[i]` is found by summing up the values from the `i`-th subsequence.
    val_source: Vec<ContribI>,
}

/// An index into [`LinearSolver::contribs`].
type ContribI = usize;

/// The terminator for subsequences in [`MatLayout::val_source`].
const CONTRIB_I_END: ContribI = usize::MAX;

impl WorldCoreImpl {
    #[tracing::instrument(skip_all)]
    pub(super) fn step_inner(
        &mut self,
        time_step: f64,
        min_time_step: f64,
        sim_params: &SimParams,
    ) -> Result<f64, StepError> {
        assert!(time_step >= 0.0);
        assert!(
            time_step == 0.0 || min_time_step > 0.0,
            "if `time_step` is `> 0`, `min_time_step` must be `> 0` too"
        );

        let advance = time_step != 0.0;

        let time_step = if advance {
            time_step
        } else {
            // If `time_step` is zero, use an extremely small time step to find
            // the state at `t = 0`.
            sim_params.initial_condition_time_step
        };

        let time_step = real(time_step);

        let mut cx = Cx {
            time_step,
            time_step_recip: time_step.recip(),
            integration_method: sim_params.integration_method,
            advance,
        };

        tracing::debug!(?cx);

        let min_time_step = real(min_time_step);

        // Allocate circuit variables
        let vars: &Vars = self
            .vars
            .get_or_insert_with(|| Vars::new(&self.circuit.nodes));

        // Calculate the circuit matrix layout
        let mat_layout: &MatLayout = self
            .mat_layout
            .get_or_insert_with(|| MatLayout::new(&self.circuit.elem_sets, vars));
        tracing::trace!(?mat_layout);

        dump::circuit_dot(&self.circuit);

        // Solver state
        // ------------------------------------------------------------------

        // Storage for current solution candidate
        let mut var_vals = vec![Real::zero(); vars.num_vars];

        // A closure to initialize `var_vals` with the last circuit state
        let load_var_vals = |var_vals: &mut [Real]| {
            for &(node_id, i) in &vars.var_node_v_sorted {
                var_vals[i] = self.circuit.nodes.index_typed(node_id).voltage;
            }
        };

        load_var_vals(&mut var_vals);

        // Storage for current element states
        let iter_state_vals = || {
            self.circuit
                .elem_sets
                .values()
                .flat_map(|elem_set| &elem_set.state_list)
                .copied()
        };
        let mut state_vals: Vec<Real> = iter_state_vals().collect();

        // A closure to re-initialize `state_vals` with the last circuit state
        let load_state_vals = |state_vals: &mut [Real]| {
            for (p_out_val, val) in izip!(state_vals, iter_state_vals()) {
                *p_out_val = val;
            }
        };

        // Storage for next solution candidate
        let mut next_var_vals = vec![Real::zero(); vars.num_vars];

        // Storage for reactive residue
        let residue_react_len = self
            .circuit
            .elem_sets
            .values()
            .map(|elem_set| elem_set.residue_react_history.len())
            .sum();
        let mut residue_react = Vec::with_capacity(residue_react_len);

        let mut linear_solver = LinearSolver {
            vars,
            mat_layout,

            rhs: Vec::new(),
            contribs: Vec::new(),
            mat_val: Vec::new(),

            mat2_row_i: Vec::new(),
            mat2_val: Vec::new(),
            mat2_col_start: vec![0; vars.num_vars + 1],
            lu_perm: vec![0; vars.num_vars],
        };

        // Find the new circuit state
        // ------------------------------------------------------------------

        if cx.advance {
            // Adaptive time step control based on iteration count [ref:nagel75]
            loop {
                // Update the time step
                cx.time_step = self.current_time_step.max(min_time_step).min(time_step);

                // The new time step to switch to in case of convergence failure
                let lower_time_step = (cx.time_step / real(8.)).max(min_time_step);
                let can_lower_time_step = lower_time_step < cx.time_step;

                let iterations = if can_lower_time_step {
                    sim_params.step_control_hard_iterations
                } else {
                    sim_params.iterations
                };

                if self.integration_order == 1 {
                    cx.integration_method = IntegrationMethod::Euler;
                }

                tracing::debug!(
                    iterations,
                    time_step = %cx.time_step,
                    "Solving the next circuit state"
                );

                match linear_solver.nr_solve(
                    &mut var_vals,
                    &mut next_var_vals,
                    &mut state_vals,
                    &mut residue_react,
                    &cx,
                    &self.circuit,
                    iterations,
                ) {
                    Ok(actual_iterations) => {
                        if actual_iterations <= sim_params.step_control_easy_iterations {
                            // Increase the time step for the next time
                            self.current_time_step = cx.time_step * 2.;
                            tracing::debug!(
                                "The final iteration count was below \
                                `step_control_easy_iterations`; increasing the \
                                time step"
                            );
                        }

                        // The solution is accepted, which means more previous
                        // solutions are available in the next iteration
                        self.integration_order = 2;
                        break;
                    }
                    Err(StepError::NoSolution) => {
                        if can_lower_time_step {
                            // Reduce the time step and try again
                            cx.time_step = lower_time_step;
                            self.current_time_step = cx.time_step;
                            self.integration_order = 1;
                            load_var_vals(&mut var_vals);
                            load_state_vals(&mut state_vals);
                            tracing::debug!("Reducing the time step and try again");
                        } else {
                            // Can not reduce the time step further
                            return Err(StepError::NoSolution);
                        }
                    }
                }
            }
        } else {
            // Use the lowest order scheme to calculate `ddt(Q(v[t+1]))` without
            // depending on earlier solutions
            cx.integration_method = IntegrationMethod::Euler;

            // Use the lowest order scheme for the next transient time step
            // because discontinuities such as changing circuit topology create
            // a spike in `ddt(Q)`, which would destabilize simulation
            self.integration_order = 1;

            linear_solver.nr_solve(
                &mut var_vals,
                &mut next_var_vals,
                &mut state_vals,
                &mut residue_react,
                &cx,
                &self.circuit,
                sim_params.iterations,
            )?;
        }

        // Store the new circuit state
        // ------------------------------------------------------------------
        for &(node_id, i) in &vars.var_node_v_sorted {
            self.circuit.nodes.index_mut_typed(node_id).voltage = next_var_vals[i];
        }

        let mut residue_react = &residue_react[..];
        let mut state_vals = &state_vals[..];

        for elem_set in self.circuit.elem_sets.values_mut() {
            let elem_ty_residue_react;
            (elem_ty_residue_react, residue_react) =
                residue_react.split_at(elem_set.residue_react_history.len());
            if cx.advance {
                // Store the solved `Q(v)[t+1]` and `ddt(Q(v))[t+1]`
                elem_set
                    .residue_react_history
                    .copy_from_slice(elem_ty_residue_react);
            } else {
                // Store the solved `ddt(Q(v))[t] ≈ ddt(Q(v))[t+1]`
                for ([_, p_dq], &[_, dq_next]) in izip!(
                    &mut elem_set.residue_react_history[..],
                    elem_ty_residue_react
                ) {
                    *p_dq = dq_next;
                }
            }

            // Write `state_vals` back to `ElemSet::state_list`
            let elem_ty_state_vals;
            (elem_ty_state_vals, state_vals) = state_vals.split_at(elem_set.state_list.len());
            elem_set.state_list.copy_from_slice(elem_ty_state_vals);
        }

        // Call `ElemTrait::step`
        // ------------------------------------------------------------------
        if cx.advance {
            for (elem_ty, elem_set) in &mut self.circuit.elem_sets {
                let num_states = elem_ty.num_states();
                let num_params = elem_ty.num_params();

                if num_states == 0 {
                    // No state variables to update
                    continue;
                }

                for (params, states) in izip!(
                    elem_set.param_list.chunks_exact2(num_params),
                    elem_set.state_list.chunks_exact_mut(num_states),
                ) {
                    // Call `ElemTrait::step`
                    elem_ty.apply(&mut StepCxImpl {
                        time_step: cx.time_step,
                        params,
                        states,
                    });
                }
            }
        }

        Ok(cx.time_step.to_f64().unwrap())
    }
}

type VarIdx = usize;

impl MatLayout {
    fn new(elem_sets: &EnumMap<ElemTy, super::ElemSet>, vars: &Vars) -> Self {
        let mut loll = LollMatLayout::new(vars.num_vars);

        let mut contrib_i = 0;

        for (elem_ty, elem_set) in elem_sets {
            let jnzs = elem_ty.jnzs();
            let num_nodes = elem_ty.num_nodes();

            // For every element of this type...
            for node_id_list in elem_set.node_id_list.chunks_exact(num_nodes) {
                // For every Jacobian entry in this element...
                for entry in jnzs {
                    let elem_node_to_mat_var = |i: ElemNodeI| vars.var_for_node_v(node_id_list[i]);

                    if let (Some(row_var_i), Some(column_var_i)) = (
                        elem_node_to_mat_var(entry.row_i),
                        elem_node_to_mat_var(entry.column_i),
                    ) {
                        if entry.has_resist {
                            // [tag:simulate_host_contrib_resist]
                            loll.add(row_var_i, column_var_i, contrib_i);
                        }
                        if entry.has_react {
                            // [tag:simulate_host_contrib_react]
                            loll.add(row_var_i, column_var_i, contrib_i + 1);
                        }
                    }

                    contrib_i += 2;
                }
            }
        }

        loll.to_csc()
    }
}

/// [`MatLayout`] in the column-major list-of-unsorted-linked-lists format
///
/// Used by [`MatLayout::new`].
struct LollMatLayout {
    col_first_i_list: Vec<usize>,
    entry_list: Vec<LollMatLayoutEntry>,
}

struct LollMatLayoutEntry {
    next_i: usize,
    row_i: usize,
    source: ContribI,
}

impl LollMatLayout {
    fn new(num_cols: usize) -> Self {
        Self {
            col_first_i_list: vec![usize::MAX; num_cols],
            entry_list: Vec::with_capacity(num_cols * 3),
        }
    }

    /// Add the specified value to element `(row_i, col_i)`.
    fn add(&mut self, row_i: usize, col_i: usize, source: ContribI) {
        let p_first_i = &mut self.col_first_i_list[col_i];
        let i = self.entry_list.len();
        let next_i = replace(p_first_i, i);
        self.entry_list.push(LollMatLayoutEntry {
            next_i,
            row_i,
            source,
        });
    }

    /// Convert `self` to [`MatLayout`].
    fn to_csc(&self) -> MatLayout {
        let num_cols = self.col_first_i_list.len();

        let mut col_start: Vec<usize> = Vec::with_capacity(num_cols + 1);
        let mut row_i: Vec<usize> = Vec::with_capacity(num_cols * 3);
        let mut val_source: Vec<ContribI> = Vec::with_capacity(self.entry_list.len());

        let mut col_entry_list: Vec<&LollMatLayoutEntry> =
            Vec::with_capacity(self.entry_list.len() / num_cols.max(1));

        col_start.push(0);

        // For every `col_i`...
        for &(mut i) in &self.col_first_i_list {
            // Collect entries in this column
            while let Some(entry) = self.entry_list.get(i) {
                col_entry_list.push(entry);
                i = entry.next_i;
            }

            // For every `(col_i, row_i)` in ascending order of `row_i`...
            col_entry_list.sort_unstable_by_key(|e| e.row_i);

            for row_entry_list in col_entry_list.chunk_by(|e1, e2| e1.row_i == e2.row_i) {
                // Add all `MatEntrySource`s for this `(col_i, row_i)`
                val_source.extend(row_entry_list.iter().map(|e| e.source));
                val_source.push(CONTRIB_I_END);

                row_i.push(row_entry_list[0].row_i);
            }

            col_entry_list.clear();
            col_start.push(row_i.len());
        }

        MatLayout {
            col_start,
            row_i,
            val_source,
        }
    }
}

/// The temporary variables used by the inner functions of
/// [`WorldCoreImpl::step_inner`].
#[derive(Debug)]
struct Cx {
    time_step: Real,
    time_step_recip: Real,
    integration_method: IntegrationMethod,
    advance: bool,
}

/// The storage for linear circuit solver state and temporary data.
struct LinearSolver<'a> {
    /// A variable mapping.
    vars: &'a Vars,
    mat_layout: &'a MatLayout,

    // The storage for circuit matrices
    // ------------------------------------------------------------
    /// The right hand side of a circuit system.
    rhs: Vec<Real>,

    /// All contributions (stamps) to the circuit matrix.
    contribs: Vec<Real>,

    /// The `val` component of the circuit matrix in the CSC storage
    /// format.
    mat_val: Vec<Real>,

    // Temporary variables
    // ------------------------------------------------------------
    mat2_row_i: Vec<usize>,
    mat2_val: Vec<Real>,
    mat2_col_start: Vec<usize>,
    lu_perm: Vec<usize>,
}

impl LinearSolver<'_> {
    /// Perform Newton-Raphson iteration to solve the next circuit state,
    /// storing the result in `*next_var_vals`.
    ///
    /// `var_vals` provides the previous circuit state. It is overwritten
    /// during the process.
    ///
    /// `state_vals` contains the values of element state variables. It is
    /// overwritten during the process.
    ///
    /// This method also updates `residue_react` with the solved reactive
    /// residue (`Q(v[t+1])`, `dQ(v[t+1])`) for use in subsequent time steps.
    ///
    /// Returns the final iteration count on success.
    fn nr_solve(
        &mut self,
        var_vals: &mut [Real],
        next_var_vals: &mut [Real],
        state_vals: &mut [Real],
        residue_react: &mut Vec<[Real; 2]>,
        cx: &Cx,
        circuit: &Circuit,
        iterations: usize,
    ) -> Result<usize, StepError> {
        let mut i = 0;
        loop {
            let _span = tracing::info_span!("NR iteration", i).entered();

            let mut prevent_convergence = false;

            // Linearize the circuit at `var_vals` and solve the next
            // circuit state
            self.linear_solve(
                var_vals,
                next_var_vals,
                state_vals,
                residue_react,
                &mut prevent_convergence,
                cx,
                circuit,
            );

            // Convergence check
            let max_change = izip!(&var_vals[..], &next_var_vals[..])
                .map(|v| (v.0 - v.1).abs())
                .fold(Real::zero(), Real::max);

            if prevent_convergence {
                tracing::debug!(
                    "`EvalCx::prevent_convergence` was called; NR iteration \
                    will not converge this time"
                );
            }

            // TODO: Implement a better, adjustable convergence check
            if next_var_vals.iter().any(|&x| !Float::is_finite(x)) {
                tracing::debug!(%max_change, i, "NR iteration diverged");
                return Err(StepError::NoSolution);
            } else if max_change < 1.0e-3 && !prevent_convergence {
                tracing::debug!(%max_change, i, "NR iteration converged");
                return Ok(i + 1);
            } else if i == iterations {
                tracing::debug!(%max_change, i, "NR iteration did not converge");
                return Err(StepError::NoSolution);
            } else {
                tracing::debug!(%max_change, "Continuing iteration");
                i += 1;
            }

            // Use the new solution as the linearization point for the next
            // iteration.
            // FIXME: Use ping-pong buffer technique to avoid copying
            var_vals.copy_from_slice(next_var_vals);
        }
    }

    /// Linearize the circuit at `var_vals` and solve the next circuit
    /// state, storing the result in `next_var_vals`.
    fn linear_solve(
        &mut self,
        var_vals: &[Real],
        next_var_vals: &mut [Real],
        mut state_vals: &mut [Real],
        residue_react: &mut Vec<[Real; 2]>,
        prevent_convergence: &mut bool,
        cx: &Cx,
        circuit: &Circuit,
    ) {
        // Create a circuit matrix
        // ------------------------------------------------------------------

        // The relationship between circuit variables is described by a
        // differential-algebraic system of equations (DAE) in the following
        // form:
        //
        //   I(v,t) + ddt(Q(v,t)) = 0
        //
        // We solve this system using a Newton-Raphson iteration and finite
        // difference. The following linear system is solved in each step:
        //
        //   (J_I(v[t+1])[i] + J_dQ(v[t+1])[i]) * Δv
        //   ────────────┬──
        //               ╰─── jacobian_resistive
        //
        //       = I(v[t+1])[i] + dQ(v[t+1])[i]
        //         ─────────┬──
        //                  ╰─── residue_resistive
        //
        // where Δv = v[t+1][i] - v[t+1][i+1]  (NR iteration delta)
        //       dQ(v) = ddt(Q(v))             (time derivative of Q(v))
        //
        // `dQ(v[t+1])[i]` is replaced with a finite difference based on the
        // selected integration scheme:
        //
        //   BE:   dQ(v[t+1])[i] = (1/h) * Q(v[t+1])[i] - (1/h) * Q(v[t])[i]
        //                                 ────────┬───
        //                                         ╰──── residue_reactive
        //
        //       J_dQ(v[t+1])[i] = (1/h) * J_Q(v[t+1])[i]
        //                                 ───────────┬──
        //                                            ╰─── jacobian_reactive
        //
        //   TR:   dQ(v[t+1])[i] = (2/h) * Q(v[t+1])[i] - (2/h) * Q(v[t])[i]
        //                                 ────────┬───
        //                                         ╰──── residue_reactive
        //                         - dQ(v[t])[i]
        //
        //       J_dQ(v[t+1])[i] = (2/h) * J_Q(v[t+1])[i]
        //                                 ───────────┬──
        //                                            ╰─── jacobian_reactive

        self.contribs.clear();
        self.contribs.reserve(self.mat_layout.val_source.len());

        self.rhs.clear();
        self.rhs.resize(circuit.nodes.len() - 1, Real::zero());

        // J_dQ(v[t+1])[i] / J_Q(v[t+1])[i]
        let contrib_react_factor = match cx.integration_method {
            IntegrationMethod::Trapezoid => 2. * cx.time_step_recip,
            IntegrationMethod::Euler => cx.time_step_recip,
        };

        residue_react.clear();

        for (elem_ty, elem_set) in &circuit.elem_sets {
            let elem_ty_state_vals;
            (elem_ty_state_vals, state_vals) = state_vals.split_at_mut(elem_set.state_list.len());

            process_elem_ty(
                &mut self.contribs,
                contrib_react_factor,
                &mut self.rhs,
                elem_ty_state_vals,
                residue_react,
                prevent_convergence,
                elem_ty,
                elem_set,
                self.vars,
                var_vals,
                cx,
            );
        }

        assert_eq!(self.rhs.len(), self.vars.num_vars);

        // Finalize the circuit matrix
        tracing::trace!(contribs = ?self.contribs);
        self.mat_val.clear();
        self.mat_val.reserve(self.mat_layout.row_i.len());
        let mut val_source_iter = self.mat_layout.val_source.iter().peekable();
        while val_source_iter.peek().is_some() {
            self.mat_val.push(
                val_source_iter
                    .by_ref()
                    // Take elements until we hit `CONTRIB_I_END`
                    .map_while(|&source| self.contribs.get(source))
                    .fold(Real::zero(), |x, y| x + y),
            );
        }

        let mat = simac_compute::utils_csc::MatCsc {
            m: self.vars.num_vars,
            n: self.vars.num_vars,
            col_start: &self.mat_layout.col_start,
            row_i: &self.mat_layout.row_i,
            val: &self.mat_val,
        };
        #[cfg(debug_assertions)]
        mat.validate().expect("invalid matrix format");

        dump::nr_eq(circuit, self.vars, &mat, &self.rhs);

        // Solve it
        // ------------------------------------------------------------------

        // TODO: Minimize fill-ins

        self.mat2_row_i.clear();
        self.mat2_val.clear();
        self.mat2_row_i.reserve(self.mat_layout.row_i.len() * 2);
        self.mat2_val.reserve(self.mat_val.len() * 2);

        simac_compute::slu_csc::decompose(
            &self.mat_layout.col_start,
            &self.mat_layout.row_i,
            &self.mat_val,
            &mut self.mat2_col_start,
            &mut self.mat2_row_i,
            &mut self.mat2_val,
            &mut self.lu_perm,
        );

        tracing::debug!(
            lu_num_vals = self.mat2_val.len(),
            ?self.lu_perm,
            "PLU decomposition complete",
        );

        for (&lu_perm, &x) in izip!(&self.lu_perm, &self.rhs) {
            next_var_vals[lu_perm] = x;
        }

        simac_compute::slu_csc::solve(
            &self.mat2_col_start,
            &self.mat2_row_i,
            &self.mat2_val,
            next_var_vals,
        );

        dump::nr_out(circuit, self.vars, var_vals, next_var_vals);

        dump::nr_next_rhs(circuit, self.vars, &mat, &self.rhs, next_var_vals);

        for (&val, next_val) in izip!(var_vals, &mut *next_var_vals) {
            *next_val = val - *next_val;
        }
    }
}

/// A variable mapping.
#[derive(Debug)]
pub(super) struct Vars {
    num_vars: VarIdx,
    var_node_v_sorted: Vec<(NodeId, VarIdx)>,
}

impl Vars {
    fn new(nodes: &Slab<super::Node>) -> Self {
        let mut num_vars: VarIdx = 0;

        // Node voltages
        let var_node_v_sorted: Vec<_> = nodes
            .iter_typed()
            // Omit `NodeId::GROUND`
            .skip(1)
            .map(|(node_id, _)| (node_id, num_vars.fetch_add(1)))
            .collect();

        tracing::debug!(
            num_vars,
            node_v = ?0..var_node_v_sorted.len(),
            "Variables",
        );

        Self {
            num_vars,
            var_node_v_sorted,
        }
    }

    /// Get the [`VarIdx`] representing the specified node's electric
    /// potential.
    fn var_for_node_v(&self, node_id: NodeId) -> Option<VarIdx> {
        if node_id == NodeId::GROUND {
            None
        } else {
            let i = self
                .var_node_v_sorted
                .binary_search_by_key(&node_id, |x| x.0)
                .expect("unkown node id");
            Some(self.var_node_v_sorted[i].1)
        }
    }

    fn fmt_var_name(
        &self,
        i: VarIdx,
        _circuit: &Circuit,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        if let Some(&(NodeId(i), _)) = self.var_node_v_sorted.get(i) {
            return write!(f, "e[{i}]");
        }
        write!(f, "?[{i}]")
    }
}

/// Calculate the matrix contributions (stamps) and the reactive residue
/// (`Q(v[t+1])[i]`, `dQ(v[t+1])[i]`) for the specified element type using
/// the operating point provided by `var_vals`.
#[expect(clippy::too_many_arguments)]
fn process_elem_ty(
    contribs: &mut Vec<Real>,
    contrib_react_factor: Real,
    rhs: &mut [Real],
    state_vals: &mut [Real],
    residue_react: &mut Vec<[Real; 2]>,
    prevent_convergence: &mut bool,
    elem_ty: ElemTy,
    elem_set: &super::ElemSet,
    vars: &Vars,
    var_vals: &[Real],
    cx: &Cx,
) {
    let _span = tracing::trace_span!("process_elem_ty", ?elem_ty).entered();

    let num_params = elem_ty.num_params();
    let num_states = elem_ty.num_states();
    let jnzs = elem_ty.jnzs();
    let node_i_list_react = elem_set.node_i_list_react.as_slice();

    // Add `contrib` entries for this `ElemTy`
    let contrib_start_i = contribs.len();
    contribs.resize(
        contrib_start_i + jnzs.len() * 2 * elem_set.ty_elems.len(),
        Real::zero(),
    );

    // Add `residue_react` entires for this `ElemTy`
    let residue_react_start_i = residue_react.len();
    residue_react.resize(
        residue_react_start_i + node_i_list_react.len() * elem_set.ty_elems.len(),
        [Real::zero(); 2],
    );

    for (
        (ty_elem_i, ty_elem),
        node_id_list,
        params,
        states,
        residue_react_history,
        contribs,
        residue_react,
    ) in izip!(
        elem_set.ty_elems.iter_typed(),
        elem_set.node_id_list.chunks_exact(elem_ty.num_nodes()),
        elem_set.param_list.chunks_exact2(num_params),
        state_vals.chunks_exact_mut2(num_states),
        elem_set
            .residue_react_history
            .chunks_exact2(node_i_list_react.len()),
        contribs[contrib_start_i..].chunks_exact_mut2(jnzs.len() * 2),
        residue_react[residue_react_start_i..].chunks_exact_mut2(node_i_list_react.len()),
    ) {
        let ty_elem_i: TyElemI = ty_elem_i;

        let _span = tracing::trace_span!(
            "Element",
            ?ty_elem_i,
            elem_id = ?ty_elem.elem_id,
        )
        .entered();

        // Call `ElemTrait::eval` to:
        //
        // - Set the resistive components of `contribs`: `J_I(v[t+1])[i]`
        // - Set the reactive components of `contribs`: `J_dQ(v[t+1])[i]`
        // - Add the resistive residue to `rhs_nodes[..]`: `I(v[t+1])[i]`
        // - Calculate the reactive residue `residue_react[..][0]`:
        //   `Q(v[t+1])[i]`
        elem_ty.apply(&mut EvalCxImpl {
            params,
            states,
            contribs,
            contrib_react_factor,
            rhs,
            residue_react,
            node_i_list_react_rev: &elem_set.node_i_list_react_rev,
            vars,
            var_vals,
            node_id_list,
            prevent_convergence,
        });

        // Calculate `residue_react[..][1]`: `dQ(v[t+1])[i]`
        for (&[q0, dq0], &mut [q_next, ref mut dq_next]) in
            izip!(residue_react_history, &mut residue_react[..])
        {
            *dq_next = match cx.integration_method {
                // dQ[t+1] = (2/h) * Q[t+1] - (2/h) * Q[t] - dQ[t]
                // FIXME: `dq0` is not available on first transient sim step
                IntegrationMethod::Trapezoid => (q_next - q0) * (2. * cx.time_step_recip) - dq0,
                // dQ[t+1] = (1/h) * Q[t+1] - (1/h) * Q[t]
                IntegrationMethod::Euler => (q_next - q0) * cx.time_step_recip,
            };
        }

        // Add `dQ(v[t+1])[i]` to RHS
        for (&node_i, &[_, dq_next]) in izip!(&elem_set.node_i_list_react, &residue_react[..]) {
            if let Some(i) = vars.var_for_node_v(node_id_list[node_i]) {
                rhs[i] += dq_next;
            }
        }
    }

    dump::residue_react(
        &residue_react[residue_react_start_i..],
        &elem_set.residue_react_history,
        elem_ty,
        elem_set,
    );
}

pub(super) struct EvalCxImpl<'a> {
    params: &'a [Real],
    states: &'a mut [Real],
    /// A subslice of [`LinearSolver::contribs`] allocated for the current
    /// element.
    contribs: &'a mut [Real],
    contrib_react_factor: Real,
    /// [`LinearSolver::rhs`]
    rhs: &'a mut [Real],
    residue_react: &'a mut [[Real; 2]],
    /// [`super::ElemSet::node_i_list_react_rev`]
    node_i_list_react_rev: &'a [usize],
    vars: &'a Vars,
    /// The circuit variable values used as the linearization point.
    var_vals: &'a [Real],
    /// The subset of [`super::ElemSet::node_id_list`] for the current
    /// element.
    node_id_list: &'a [NodeId],
    prevent_convergence: &'a mut bool,
}

impl simac_elements::FnOnceElemTrait<super::SimCxImpl> for &mut EvalCxImpl<'_> {
    type Output = ();

    fn call<T: simac_elements::ElemTrait<super::SimCxImpl>>(
        self,
        elem: &'static T,
    ) -> Self::Output {
        elem.eval(self)
    }
}

impl simac_elements::EvalCx<super::SimCxImpl> for EvalCxImpl<'_> {
    #[inline]
    fn param(&self, param_i: simac_elements::ElemParamI) -> Real {
        self.params[param_i]
    }

    #[inline]
    fn state(&self, state_i: simac_elements::ElemStateI) -> Real {
        self.states[state_i]
    }

    #[inline]
    fn set_state(&mut self, state_i: simac_elements::ElemStateI, value: Real) {
        self.states[state_i] = value;
    }

    #[inline]
    fn node(&self, node_i: ElemNodeI) -> Real {
        self.vars
            .var_for_node_v(self.node_id_list[node_i])
            .map(|i| self.var_vals[i])
            .unwrap_or(Real::zero())
    }

    #[inline]
    fn set_jnz_resist(&mut self, jnz_i: ElemJnzI, value: Real) {
        // Store an element of `J_I(v)`
        // [ref:simulate_host_contrib_resist]
        self.contribs[jnz_i * 2] = value;
    }

    #[inline]
    fn set_jnz_react(&mut self, jnz_i: ElemJnzI, value: Real) {
        // Store an element of `contrib_react_factor*J_Q(v)`
        // [ref:simulate_host_contrib_react]
        self.contribs[jnz_i * 2 + 1] = value * self.contrib_react_factor;
    }

    #[inline]
    fn set_residue_resist(&mut self, node_i: ElemNodeI, value: Real) {
        // (J_I(v[t+1])[i] + contrib_react_factor*J_Q(v[t+1])[i]) * Δv
        //    = I(v[t+1])[i] + ddt(Q(v[t+1]))[i]
        //      ^^^^^^^^^^^^
        if let Some(i) = self.vars.var_for_node_v(self.node_id_list[node_i]) {
            self.rhs[i] += value;
        }
    }

    #[inline]
    fn set_residue_react(&mut self, node_i: ElemNodeI, value: Real) {
        // Store an element of `Q(v[t+1])[i]`
        self.residue_react[self.node_i_list_react_rev[node_i]][0] = value;
    }

    #[inline]
    fn prevent_convergence(&mut self) {
        tracing::trace!("`EvalCx::prevent_convergence` was called");
        *self.prevent_convergence = true;
    }
}

pub(super) struct StepCxImpl<'a> {
    time_step: Real,
    params: &'a [Real],
    states: &'a mut [Real],
}

impl simac_elements::FnOnceElemTrait<super::SimCxImpl> for &mut StepCxImpl<'_> {
    type Output = ();

    fn call<T: simac_elements::ElemTrait<super::SimCxImpl>>(
        self,
        elem: &'static T,
    ) -> Self::Output {
        elem.step(self)
    }
}

impl simac_elements::StepCx<super::SimCxImpl> for StepCxImpl<'_> {
    #[inline]
    fn time_step(&self) -> Real {
        self.time_step
    }

    #[inline]
    fn param(&self, param_i: simac_elements::ElemParamI) -> Real {
        self.params[param_i]
    }

    fn node(&self, _node_i: ElemNodeI) -> Real {
        todo!()
    }

    #[inline]
    fn state(&self, var_i: simac_elements::ElemStateI) -> Real {
        self.states[var_i]
    }

    #[inline]
    fn set_state(&mut self, var_i: simac_elements::ElemStateI, value: Real) {
        self.states[var_i] = value;
    }
}
