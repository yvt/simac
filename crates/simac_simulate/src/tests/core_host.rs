use crate::core_host;

core_common_tests!(Opts);

struct Opts;

impl super::core_common::Opts for Opts {
    fn new_world() -> Box<dyn crate::WorldCore> {
        Box::<core_host::WorldCoreImpl>::default()
    }
}
