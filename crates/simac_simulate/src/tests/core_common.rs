//! Tests for all [`WorldCore`] instances
use approx::assert_abs_diff_eq;
use fxhash::FxHashMap;
use itertools::iproduct;
use simac_elements::ElemTy;

use crate::{IntegrationMethod, NodeId, SimParams, WorldCore};

pub(super) mod world_utils;
use world_utils::WorldExt as _;

/// Instantiate the common [`WorldCore`] tests.
macro_rules! core_common_tests {
    ($Opts:ty) => {
        core_common_tests! {
            $Opts;
            test_capacitor_charge,
            test_diode,
            test_resistor_grid,
            test_voltage_source_short_circuit,
        }
    };

    // Internals
    ($Opts:ty; $($test:ident,)*) => {
        // Define a test function for each `$test`
        $(
            #[test]
            fn $test() {
                $crate::tests::init();
                $crate::tests::core_common::$test::<$Opts>();
            }
        )*
    }
}

pub(super) trait Opts {
    fn new_world() -> Box<dyn WorldCore>;
}

pub(super) fn test_capacitor_charge<Opts: self::Opts>() {
    test_capacitor_charge_commond::<Opts>(IntegrationMethod::Trapezoid, 1.0e-3);
    test_capacitor_charge_commond::<Opts>(IntegrationMethod::Euler, 1.0e-3);
    test_capacitor_charge_commond::<Opts>(IntegrationMethod::Euler, 1.0);
}

#[tracing::instrument]
fn test_capacitor_charge_commond<Opts: self::Opts>(
    integration_method: IntegrationMethod,
    time_step: f64,
) {
    let mut world = Opts::new_world();

    // Define nodes
    let n_vcc = world.insert_node();
    let n_mid = world.insert_node();

    // Define elements
    let el_source = world.insert_elem2(ElemTy::VoltageSource, &[n_vcc, NodeId::GROUND]);
    world.elem(&el_source).set_param("V", 10.0);

    let el_resistor = world.insert_elem2(ElemTy::Resistor, &[n_vcc, n_mid]);
    world.elem(&el_resistor).set_param("R", 15.0);

    let el_capacitor = world.insert_elem2(ElemTy::Capacitor, &[n_mid, NodeId::GROUND]);
    world.elem(&el_capacitor).set_param("C", 1.0e-3);

    tracing::info!(?el_source, ?el_resistor, ?el_capacitor);

    // Run simulation
    tracing::info!("Updating the circuit");
    let sim_params = SimParams {
        integration_method,
        ..SimParams::default()
    };
    world.step(0.0, 0.0, &sim_params).expect("step");

    let v_capacitor = world.node(n_mid);
    let mut v_capacitors = vec![v_capacitor];

    for i in 0..30 {
        tracing::info!(i, "Advancing time");
        world.step(time_step, time_step, &sim_params).expect("step");

        let v_capacitor = world.node(n_mid);
        v_capacitors.push(v_capacitor);
    }

    tracing::debug!(?v_capacitors);

    // Validate the result
    let v_capacitors_expected: Vec<_> = std::iter::successors(Some(0.0), |v_capacitor| {
        match integration_method {
            IntegrationMethod::Trapezoid => {
                // v[t+1] = v[t] + (h/2)(v'[t] + v'[t+1])
                //  v'[t] = (10 - v[t]) / 15 / 1.0e-3
                // v[t+1] = v[t] + (h/2/15/1.0e-3)(20 - v[t] - v[t+1])
                let k = time_step / 2.0 / 15.0 / 1.0e-3;
                Some((v_capacitor + k * (20.0 - v_capacitor)) / (1.0 + k))
            }
            IntegrationMethod::Euler => {
                // v[t+1] = v[t] + h * v'[t+1]
                // v[t+1] = v[t] + (h/15/1.0e-3)(10 - v[t+1])
                let k = time_step / 15.0 / 1.0e-3;
                Some((v_capacitor + k * 10.0) / (1.0 + k))
            }
        }
    })
    .take(v_capacitors.len())
    .collect();

    assert_abs_diff_eq!(*v_capacitors, *v_capacitors_expected, epsilon = 1.0e-1);
}

pub(super) fn test_diode<Opts: self::Opts>() {
    let mut world = Opts::new_world();

    // Define nodes
    let n_vcc = world.insert_node();
    let n_mid = world.insert_node();

    // Define elements
    let el_source = world.insert_elem2(ElemTy::VoltageSource, &[n_vcc, NodeId::GROUND]);
    world.elem(&el_source).set_param("V", 10.0);

    let el_resistor = world.insert_elem2(ElemTy::Resistor, &[n_vcc, n_mid]);
    world.elem(&el_resistor).set_param("R", 1.0);

    let el_diode = world.insert_elem2(ElemTy::Diode, &[n_mid, NodeId::GROUND]);
    world
        .elem(&el_diode)
        .set_param("Is", 315e-9)
        .set_param("rs", 2.8)
        .set_param("n", 2.03)
        .set_param("tt", 1.44e-9)
        .set_param("Cj0", 2e-12)
        .set_param("m", 0.333);

    tracing::info!(?el_source, ?el_resistor, ?el_diode);

    // Simulation: Apply AC voltage
    tracing::info!("Updating the circuit");
    let sim_params = SimParams {
        // The default moethod (`Trapezoid`) causes ringing, which causes
        // the current assertions to fail
        integration_method: IntegrationMethod::Euler,
        ..SimParams::default()
    };

    for i in 0..200 {
        let v_source = (i as f64 * 0.05).sin();
        let v_source = (v_source.abs() * 1.2).min(1.0).copysign(v_source);
        tracing::info!(v_source);
        world.elem(&el_source).set_param("V", v_source);

        if i == 0 {
            world.step(0.0, 0.0, &sim_params).expect("step");
        } else {
            world.step(1.0e-6, 1.0e-12, &sim_params).expect("step");
        }

        let v_diode = world.node(n_mid);
        let i_diode = world.node(el_diode.node_mappings[3]); // `N_I1`
        tracing::info!(v_source, v_diode, i_diode);

        if i > 50 {
            if v_source == 1.0 {
                assert!(
                    i_diode > 0.05,
                    "too low current for a forward-biased diode: i_diode = {i_diode}"
                );
            } else if v_source == -1.0 {
                assert!(
                    i_diode < 0.0,
                    "current flows forward despite we're trying to make a diode reverse-biased: \
                    i_diode = {i_diode}"
                );
                assert!(
                    i_diode > -1.0e-4,
                    "too high current for a reverse-biased diode: i_diode = {i_diode}"
                );
            }
        }
    }
}

pub(super) fn test_resistor_grid<Opts: self::Opts>() {
    for (num_rows, num_cols) in iproduct!(2..4, 1..4) {
        let _span = tracing::info_span!("Simulating a resistor grid", num_rows, num_cols).entered();
        let mut world = Opts::new_world();

        tracing::info!("Creating nodes and elements");

        // Add nodes
        let node_ids: Vec<NodeId> = (0..num_rows * num_cols)
            .map(|_| world.insert_node())
            .collect();
        let node_id_at = |row_i: usize, col_i: usize| node_ids[row_i * num_cols + col_i];

        // A hash map to track elements connected to each node
        #[derive(Debug, Clone, Copy)]
        enum Kind {
            VoltageSource(NodeId),
            Wire(NodeId),
            Resistor(NodeId, NodeId, f64),
        }
        let mut node_els: FxHashMap<NodeId, Vec<(Kind, bool)>> = node_ids
            .iter()
            .copied()
            .chain([NodeId::GROUND])
            .map(|node_id| (node_id, Vec::new()))
            .collect();
        let mut push_node_el =
            |node_id, kind, dir| node_els.get_mut(&node_id).unwrap().push((kind, dir));

        // Add voltage sources
        let el_ground = world.insert_elem2(ElemTy::Wire, &[NodeId::GROUND, node_id_at(0, 0)]);
        tracing::debug!(?el_ground);
        push_node_el(
            NodeId::GROUND,
            Kind::Wire(el_ground.node_mappings[simac_elements::passive::Wire::N_I]),
            false,
        );
        push_node_el(
            node_id_at(0, 0),
            Kind::Wire(el_ground.node_mappings[simac_elements::passive::Wire::N_I]),
            true,
        );

        let el_source = world.insert_elem2(
            ElemTy::VoltageSource,
            &[node_id_at(num_rows - 1, num_cols - 1), NodeId::GROUND],
        );
        tracing::debug!(?el_source);
        push_node_el(
            node_id_at(num_rows - 1, num_cols - 1),
            Kind::VoltageSource(
                el_source.node_mappings[simac_elements::source::VoltageSource::N_I],
            ),
            false,
        );
        push_node_el(
            NodeId::GROUND,
            Kind::VoltageSource(
                el_source.node_mappings[simac_elements::source::VoltageSource::N_I],
            ),
            true,
        );

        world.elem(&el_source).set_param("V", 100.0);

        // Add resistors
        for (row_i, col_i) in iproduct!(1..num_rows, 0..num_cols) {
            let n1 = node_id_at(row_i, col_i);
            let n2 = node_id_at(row_i - 1, col_i);
            let el = world.insert_elem2(ElemTy::Resistor, &[n1, n2]);
            push_node_el(n1, Kind::Resistor(n1, n2, 20.0), false);
            push_node_el(n2, Kind::Resistor(n1, n2, 20.0), true);
            world.elem(&el).set_param("R", 20.0);
        }

        for (row_i, col_i) in iproduct!(0..num_rows, 1..num_cols) {
            let n1 = node_id_at(row_i, col_i);
            let n2 = node_id_at(row_i, col_i - 1);
            let el = world.insert_elem2(ElemTy::Resistor, &[n1, n2]);
            push_node_el(n1, Kind::Resistor(n1, n2, 10.0), false);
            push_node_el(n2, Kind::Resistor(n1, n2, 10.0), true);
            world.elem(&el).set_param("R", 10.0);
        }

        tracing::info!("Updating the circuit");
        world.step(0.0, 0.0, &SimParams::default()).expect("step");

        // Entering/exiting current
        let enter_curr =
            world.node(el_source.node_mappings[simac_elements::source::VoltageSource::N_I]);
        let exit_curr = world.node(el_ground.node_mappings[simac_elements::passive::Wire::N_I]);
        assert_abs_diff_eq!(enter_curr, exit_curr, epsilon = 1.0e-3);
        tracing::info!(current = -enter_curr, "Total current");

        if num_cols == 1 {
            assert_abs_diff_eq!(
                -enter_curr,
                // I = V / (R_vert * (num_rows - 1))
                100.0 / (20.0 * (num_rows - 1) as f64),
                epsilon = 1.0e-3
            );
        }

        // Check that KCL is observred
        for els in node_els.values() {
            let mut current_total = 0.0;
            for &(kind, dir) in els {
                let current = match kind {
                    Kind::VoltageSource(n_i) | Kind::Wire(n_i) => world.node(n_i),
                    Kind::Resistor(n1, n2, r) => (world.node(n1) - world.node(n2)) / r,
                };
                current_total += current * [1.0, -1.0][dir as usize];
            }

            assert_abs_diff_eq!(current_total, 0.0, epsilon = 1.0e-3);
        }

        // Check the voltage source nodes' voltages
        assert_abs_diff_eq!(world.node(NodeId::GROUND), 0.0, epsilon = 1.0e-3);
        assert_abs_diff_eq!(
            world.node(node_id_at(num_rows - 1, num_cols - 1)),
            100.0,
            epsilon = 1.0e-3
        );
    }
}

pub(super) fn test_voltage_source_short_circuit<Opts: self::Opts>() {
    let mut world = Opts::new_world();

    // Define nodes
    let n_vcc = world.insert_node();

    // Define elements
    let el_source = world.insert_elem2(ElemTy::VoltageSource, &[n_vcc, NodeId::GROUND]);
    world.elem(&el_source).set_param("V", 1.0);

    world.insert_elem2(ElemTy::Wire, &[n_vcc, NodeId::GROUND]);

    tracing::info!("Updating the circuit");
    world.step(0.0, 0.0, &SimParams::default()).expect_err(
        "expected simulation to fail because of voltage source loop \
        with no resistance",
    );
}
