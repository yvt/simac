use proptest::prelude::*;
use simac_elements::ElemTy;

use crate::{
    ElemId, NodeId, SimParams, WorldCore, core_host, layer_autoground,
    tests::core_common::world_utils::WorldExt as _,
};

core_common_tests!(Opts);

struct Opts;

impl super::core_common::Opts for Opts {
    fn new_world() -> Box<dyn WorldCore> {
        Box::<layer_autoground::AutoGround<core_host::WorldCoreImpl>>::default()
    }
}

#[derive(Debug, proptest_derive::Arbitrary)]
enum PtResistorsCmd {
    Update,
    NewNode,
    RemoveNode {
        node: prop::sample::Index,
    },
    NewEdge {
        nodes: [prop::sample::Index; 2],
        powered: bool,
    },
    RemoveEdge {
        edge: prop::sample::Index,
    },
}

proptest! {
    /// Create random circuits containing resistors and voltage sources and
    /// check that the solutions can be always found.
    #[test]
    fn pt_resistors(cmds: Vec<PtResistorsCmd>) {
        crate::tests::init();
        pt_resistors_inner(cmds);
    }
}

fn pt_resistors_inner(cmds: Vec<PtResistorsCmd>) {
    let mut world: Box<dyn WorldCore> =
        Box::<layer_autoground::AutoGround<core_host::WorldCoreImpl>>::default();
    let mut all_nodes = Vec::new();

    #[derive(Debug)]
    struct Edge {
        nodes: [NodeId; 2],
        el_resistor: ElemId,
        power: Option<(ElemId, NodeId)>,
    }

    let mut all_edges: Vec<Edge> = Vec::new();

    let remove_edge = |world: &mut dyn WorldCore, edge: &Edge| {
        tracing::info!(?edge, "Removing an edge");

        world.remove_elem(edge.el_resistor);
        if let Some((e, n)) = edge.power {
            world.remove_elem(e);
            world.remove_node(n);
        }
    };

    tracing::info!("Executing commands");

    for op in cmds {
        match op {
            PtResistorsCmd::NewNode => {
                let node_id = world.insert_node();
                tracing::info!(?node_id, "Created a node");
                all_nodes.push(node_id);
            }
            PtResistorsCmd::RemoveNode { node } => {
                if all_nodes.is_empty() {
                    tracing::debug!(
                        "Ignoring `RemoveNode` because no nodes are availble for removal"
                    );
                    continue;
                }

                let i = node.index(all_nodes.len());
                let node_id = all_nodes.remove(i);

                all_edges.retain(|edge| {
                    if !edge.nodes.contains(&node_id) {
                        return true;
                    }

                    tracing::info!(
                        ?edge,
                        "Removing an edge because we are about to \
                        remove a node attached to it",
                    );

                    remove_edge(&mut *world, edge);
                    false
                });

                world.remove_node(node_id);

                tracing::info!(?node_id, "Removed a node");
            }
            PtResistorsCmd::NewEdge { nodes, powered } => {
                let nodes = nodes.map(|i| {
                    let i = i.index(nodes.len() + 1);
                    all_nodes.get(i).copied().unwrap_or(NodeId::GROUND)
                });

                if nodes[0] == nodes[1] {
                    tracing::debug!(
                        ?nodes,
                        "Not connecting both ends of an edge to the same node"
                    );
                    continue;
                }

                let mut node2 = nodes[0];

                let power = powered.then(|| {
                    let n_mid = world.insert_node();
                    node2 = n_mid;

                    let el_source = world.insert_elem2(ElemTy::VoltageSource, &[nodes[0], n_mid]);
                    world.elem(&el_source).set_param("V", 10.0);

                    (el_source.elem_id(), n_mid)
                });

                let el_resistor = world.insert_elem2(ElemTy::Resistor, &[node2, nodes[1]]);
                world.elem(&el_resistor).set_param("R", 1.0);

                let edge = Edge {
                    nodes,
                    el_resistor: el_resistor.elem_id(),
                    power,
                };
                tracing::info!(?edge, "Inserted an edge");
                all_edges.push(edge);
            }
            PtResistorsCmd::RemoveEdge { edge } => {
                if all_edges.is_empty() {
                    tracing::debug!(
                        "Ignoring `RemoveEdge` because no edges are availble for removal"
                    );
                    continue;
                }

                let i = edge.index(all_edges.len());
                let edge = all_edges.remove(i);
                remove_edge(&mut *world, &edge);
            }
            PtResistorsCmd::Update => {
                tracing::info!("Updating the simulation");
                world.step(0.0, 0.0, &SimParams::default()).expect("step");
            }
        }
    }
}
