//! Utilities to manipulate [`WorldCore`]
use simac_elements::ElemTy;
use std::fmt;

use crate::{ElemId, NodeId, WorldCore};

/// An extension trait for `dyn `[`WorldCore`].
pub trait WorldExt {
    /// Call [`WorldCore::insert_elem`] and insert non-terminal (internal)
    /// nodes.
    ///
    /// Returns [`ElemHandle`], a convenient wrapper of [`ElemId`] and
    /// [`simac_elements::ElemTrait`] that you can pass to [`Self::elem`] to
    /// perform operations on the created element.
    fn insert_elem2(&mut self, elem_ty: ElemTy, terminal_node_mappings: &[NodeId]) -> ElemHandle;

    /// Construct an [`Elem`], which you can use to perform operations on
    /// the element specified by `elem_handle`.
    fn elem<'a>(&'a mut self, elem_handle: &'a ElemHandle) -> Elem<'a>;
}

/// See [`WorldExt::insert_elem2`].
#[derive(Clone)]
pub struct ElemHandle {
    elem_id: ElemId,
    elem_ty: ElemTy,
    pub node_mappings: Vec<NodeId>,
}

/// See [`WorldExt::elem`].
pub struct Elem<'a> {
    world: &'a mut (dyn WorldCore + 'a),
    elem_handle: &'a ElemHandle,
}

impl WorldExt for dyn WorldCore {
    fn insert_elem2(&mut self, elem_ty: ElemTy, terminal_node_mappings: &[NodeId]) -> ElemHandle {
        let mut terminal_node_mappings = terminal_node_mappings.iter().copied();
        let node_mappings: Vec<_> = elem_ty
            .nodes()
            .iter()
            .map(|node| {
                if node.is_terminal {
                    terminal_node_mappings
                        .next()
                        .expect("too few nodes provided")
                } else {
                    self.insert_node()
                }
            })
            .collect();

        let elem_id = self.insert_elem(elem_ty, &vec![0.0; elem_ty.num_states()], &node_mappings);

        tracing::trace!(?elem_id, ?elem_ty, ?node_mappings, "Inserted an element",);

        // Set paramter default values
        for i in 0..elem_ty.num_params() {
            let param_meta = elem_ty.param_meta(i).expect("param meta unavailable");
            self.set_elem_param(elem_id, i, param_meta.default_val);
        }

        ElemHandle {
            elem_id,
            elem_ty,
            node_mappings,
        }
    }

    #[inline]
    fn elem<'a>(&'a mut self, elem_handle: &'a ElemHandle) -> Elem<'a> {
        Elem {
            world: self,
            elem_handle,
        }
    }
}

impl fmt::Debug for ElemHandle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.elem_id.fmt(f)
    }
}

impl ElemHandle {
    pub fn elem_id(&self) -> ElemId {
        self.elem_id
    }
}

impl Elem<'_> {
    /// Set the value of the parameter specified by name.
    pub fn set_param(self, name: &str, value: f64) -> Self {
        let param_i = (0..self.elem_handle.elem_ty.num_params())
            .find(|&i| self.elem_handle.elem_ty.param_meta(i).unwrap().name == name)
            .unwrap_or_else(|| panic!("unable to find parameter {name:?}"));
        tracing::trace!(
            elem_id = ?self.elem_handle.elem_id,
            param_i,
            name,
            value,
            "Setting element parameter",
        );
        self.world
            .set_elem_param(self.elem_handle.elem_id, param_i, value);
        self
    }
}
