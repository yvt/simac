#[macro_use]
mod core_common;

mod core_host;
mod layer_autoground;

pub(crate) use simac_test_utils::init;
