/// An inverse function of [`ruler_exp`].
///
/// # Examples
///
/// ```rust
/// use simac_numfmt::ruler_log::ruler_log_round;
///
/// assert_eq!(ruler_log_round(0.0), 1 - 308 * 3);
/// assert_eq!(ruler_log_round(f64::MIN_POSITIVE), 1 - 308 * 3);
/// assert_eq!(ruler_log_round(2.0e-308), 1 - 308 * 3);
///
/// assert_eq!(ruler_log_round(5.0e-24), 2 - 24 * 3);
///
/// assert_eq!(ruler_log_round(1.0), 0);
/// assert_eq!(ruler_log_round(2.0), 1);
/// assert_eq!(ruler_log_round(5.0), 2);
/// assert_eq!(ruler_log_round(7.0), 2);
///
/// assert_eq!(ruler_log_round(5.0e42), 2 + 42 * 3);
///
/// assert_eq!(ruler_log_round(f64::MAX), 1 + 308 * 3);
/// assert_eq!(ruler_log_round(f64::INFINITY), 1 + 308 * 3);
/// ```
pub fn ruler_log_round(x: f64) -> i32 {
    let x = x.clamp(f64::MIN_POSITIVE, f64::MAX);
    let exp = x.log10().floor() as i32;
    let fac = x / 10_f64.powi(exp);
    exp * 3
        + if fac < 1.5 {
            0
        } else if fac < 3.5 {
            1
        } else if fac < 7.5 {
            2
        } else {
            3
        }
}

/// A [`f64::exp2`]-like function that returns
/// `[1, 2, 5][i % 3] * 10.pow(i / 3)`.
///
/// # Examples
///
/// ```rust
/// use simac_numfmt::ruler_log::ruler_exp;
///
/// assert_eq!(ruler_exp(1 - 308 * 3), 0.0);
/// assert_eq!(ruler_exp(2 - 308 * 3), 5.0e-308);
///
/// assert_eq!(ruler_exp(0), 1.0);
/// assert_eq!(ruler_exp(1), 2.0);
/// assert_eq!(ruler_exp(2), 5.0);
///
/// assert_eq!(ruler_exp(2 + 42 * 3), 5.0e42);
///
/// assert_eq!(ruler_exp(0 + 308 * 3), 1.0e+308);
/// assert_eq!(ruler_exp(1 + 308 * 3), f64::INFINITY);
/// ```
pub fn ruler_exp(x: i32) -> f64 {
    let frac = match x.rem_euclid(3) {
        0 => 1,
        1 => 2,
        2 => 5,
        _ => unreachable!(),
    };
    let exp = x.div_euclid(3);
    let y: f64 = format!("{frac}e{exp}").parse().unwrap();
    if y.is_subnormal() {
        return 0.0;
    }
    y
}
#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn ruler_exp_log_round(i in (-308 * 3 + 2)..=(308 * 3)) {
            let f = ruler_exp(i);
            let i2 = ruler_log_round(f);
            prop_assert_eq!(i, i2);
        }
    }
}
