//! Formatting with SI (metric) prefixes (e.g., `G`, `Meg`, `µ`)
use std::{
    cmp::Ordering,
    fmt::{self, Write},
};

/// All known SI prefixes (as of 2024) that are powers of one thousand, which
/// include milli and mega but exclude deci and hecto.
const PREFIXES: &[(i32, &str)] = &[
    (-30, "q"), // quecto
    (-27, "r"), // ronto
    (-24, "y"), // yocto
    (-21, "z"), // zepto
    (-18, "a"), // atto
    (-15, "f"), // femto
    (-12, "p"), // pico
    (-9, "n"),  // nano
    (-6, "μ"),  // micro
    (-3, "m"),  // milli
    // (-2, "c"),  // centi
    // (-1, "d"),  // deci
    (0, ""), // (none)
    // (1, "da"),  // deca
    // (2, "h"),   // hecto
    (3, "k"),  // kilo
    (6, "M"),  // mega
    (9, "G"),  // giga
    (12, "T"), // tera
    (15, "P"), // peta
    (18, "E"), // exa
    (21, "Z"), // zetta
    (24, "Y"), // yotta
    (27, "R"), // ronna
    (30, "Q"), // quetta
];

const _: () = {
    // `PREFIXES[i].0` must be `3 * (i + k)` for some `k`
    let mut exp = PREFIXES[0].0;
    assert!(exp % 3 == 0);
    let mut i = 0;
    while i < PREFIXES.len() {
        assert!(PREFIXES[i].0 == exp);
        exp += 3;
        i += 1;
    }
};

/// A [`fmt::Display`] wrapper to format a [`f64`] with SI prefixes
/// (e.g., `G`, `Meg`, `µ`).
///
/// # Examples
///
/// ```rust
/// use simac_numfmt::si_prefix::WithSiPrefix;
/// assert_eq!(format!("{}A", WithSiPrefix(1.2343e-2)), "12.343 mA");
/// ```
#[derive(Clone, Copy, PartialEq, PartialOrd)]
pub struct WithSiPrefix(pub f64);

impl fmt::Display for WithSiPrefix {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if !self.0.is_finite() || self.0 == 0.0 {
            self.0.fmt(f)?;
            return f.write_str(" ");
        }

        // Format the number in scientific notation (e.g., `1.0e-10`)
        // with a provided precision
        let st = if let Some(precision) = f.precision() {
            format!("{:.*e}", precision.max(3), self.0)
        } else {
            format!("{:e}", self.0)
        };

        // Is `+` sign required?
        if f.sign_plus() && self.0.is_sign_positive() {
            f.write_char('+')?;
        }

        // There must be exactly one integral digit
        let i_point = st.find('.');
        assert!(
            if let Some(i) = i_point {
                matches!(i, 1 | 2)
                    && matches!(st.as_bytes()[i - 1], b'1'..=b'9')
                    && (i == 1 || st.as_bytes()[i - 2] == b'-')
            } else {
                matches!(
                    st.as_bytes(),
                    [b'1'..=b'9', b'e', ..] | [b'-', b'1'..=b'9', b'e', ..]
                )
            },
            "unexpected formatted number: {st:?}"
        );

        // Parse the exponent
        let i_exp = st.find('e').unwrap();
        let exp: i32 = st[i_exp + 1..].parse().unwrap();

        //     i_point
        //        ↓
        //    + 1 . 2 3 4 5 6 7 e + 1 0
        //         ╰─────┬─────╯ ↑
        //            num_frac  i_exp

        let num_frac = i_point.map_or(0, |i| i_exp - i - 1);

        // Choose the prefix to use
        let prefixes = PREFIXES;
        let [first, .., last] = prefixes else { unreachable!() };
        let (prefix_exp, prefix) = if exp < first.0 {
            *first
        } else if exp >= last.0 + 3 {
            // Too large; just use scientific notation, but make it pretty
            f.write_str(&st[..i_exp])?;
            f.write_str("\u{202f}×\u{202f}10")?;
            let superscripts = ["⁰", "¹", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹", "⁻"];
            for &b in &st.as_bytes()[i_exp + 1..] {
                f.write_str(superscripts[if b == b'-' { 10 } else { (b - b'0').into() }])?;
            }
            return f.write_str(" ");
        } else {
            prefixes[((exp - first.0) / 3) as usize]
        };

        // Shift the decimal point
        match prefix_exp.cmp(&exp) {
            Ordering::Equal => f.write_str(&st[..i_exp])?,
            Ordering::Less => {
                // Move the point rightward, inserting zeros as necessary
                let offset = usize::try_from(exp - prefix_exp).unwrap();
                if let Some(i_point) = i_point {
                    if let Some(zeros) = offset.checked_sub(num_frac) {
                        //     1.2 e + 1 1
                        //      │
                        //      ╰───╮
                        //          ▼
                        //     1 2 0 G
                        f.write_str(&st[..i_point])?;
                        f.write_str(&st[i_point + 1..i_exp])?;
                        for _ in 0..zeros {
                            f.write_char('0')?;
                        }
                    } else {
                        //     1.2 3 4 e + 1 1
                        //      │
                        //      ╰───╮
                        //          ▼
                        //     1 2 3.4 G
                        f.write_str(&st[..i_point])?;
                        f.write_str(&st[i_point + 1..i_point + 1 + offset])?;
                        f.write_char('.')?;
                        f.write_str(&st[i_point + 1 + offset..i_exp])?;
                    }
                } else {
                    //     1 e + 1 1
                    //      │
                    //      ╰───╮
                    //          ▼
                    //     1 0 0 G
                    f.write_str(&st[..i_exp])?;
                    for _ in 0..offset {
                        f.write_char('0')?;
                    }
                }
            }
            Ordering::Greater => {
                // Move the point leftward, inserting zeros as necessary,
                // shifting out the existing digits to maintain the width
                let offset = usize::try_from(prefix_exp - exp).unwrap();
                let Some(new_num_frac) = num_frac.checked_sub(offset).map(|x| x + 1) else {
                    //     1 e - 3 1        1.0 0 e - 3 3
                    //     │                │
                    //     ╰─╮              ╰─────╮
                    //       ▼                    ▼
                    //     0                0.0 0
                    if self.0.is_sign_negative() {
                        f.write_char('-')?;
                    }
                    f.write_char('0')?;
                    if i_point.is_some() {
                        f.write_char('.')?;
                    }
                    for _ in 0..num_frac {
                        f.write_char('0')?;
                    }
                    return f.write_char(' ');
                };

                let i_point = i_point.unwrap();
                //     1.2 3 4 e - 3 2
                //     │
                //     ╰───╮
                //         ▼
                //     0.0 1 2 q
                //        ╰─┬─╯
                //     new_num_frac
                f.write_str(&st[..i_point - 1])?;
                f.write_str("0.")?;
                for _ in 1..offset {
                    f.write_char('0')?;
                }
                f.write_str(&st[i_point - 1..i_point])?;
                f.write_str(&st[i_point + 1..i_point + new_num_frac])?;
            }
        }

        f.write_str(" ")?;
        f.write_str(prefix)
    }
}

/// Parse a string possibly including an SI prefix.
///
/// # Examples
///
/// ```rust
/// use simac_numfmt::si_prefix::parse_with_si_prefix;
/// assert_eq!(parse_with_si_prefix("42").unwrap(), 42.);
/// assert_eq!(parse_with_si_prefix("42.0u").unwrap(), 42e-6);
/// assert_eq!(parse_with_si_prefix("5 G").unwrap(), 5e+9);
/// assert_eq!(parse_with_si_prefix("3e-3 M").unwrap(), 3e+3);
/// ```
pub fn parse_with_si_prefix(s: &str) -> Option<f64> {
    let s = s.trim();

    // Identify and strip the prefix
    let (s, prefix_exp) = 'a: {
        if let Some((body, tail)) = s.len().checked_sub(3).and_then(|i| s.split_at_checked(i)) {
            if tail.eq_ignore_ascii_case("meg") {
                // Alternative notation of 'M'
                break 'a (body, 6);
            }
        }

        let Some((i, _)) = s.char_indices().next_back() else {
            break 'a (s, 0);
        };

        let (body, tail) = s.split_at(i);

        if matches!(tail, "u" | "µ") {
            // Alternative notation of 'μ'
            break 'a (body, -6);
        }

        for &(exp, prefix) in PREFIXES {
            if prefix == tail {
                // Exact match
                break 'a (body, exp);
            }
        }

        for &(exp, prefix) in PREFIXES {
            if prefix.eq_ignore_ascii_case(tail) {
                // Case insensitive match
                break 'a (body, exp);
            }
        }

        (s, 0)
    };
    let s = s.trim_end();

    // Parse the number
    // TODO: Support the "pretty" scientific notation used by `WithSiPrefix`
    let num: f64 = s.parse().ok()?;

    // Format the number in scientific notation so that we can
    // manipulate the exponent
    let mut num_s = format!("{num:e}");

    // Parse the exponent
    let Some(i_exp) = num_s.find('e') else {
        // The number (before applying the SI prefix) is out of range, or
        // the number is non-finite from the beginning
        return None;
    };
    let exp: i32 = num_s[i_exp + 1..].parse().unwrap();

    // Update the exponent
    //
    // Note that the result is not necessarily equal to multiplying `num` by
    // `10_f64.powi(prefix_exp)`.
    num_s.truncate(i_exp + 1);
    write!(num_s, "{}", exp + prefix_exp).unwrap();

    // Parse the number again
    Some(num_s.parse().unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    macro_rules! fmt_si_pfx {
        ($fmt:literal, $val:expr) => {
            format!($fmt, WithSiPrefix($val))
        };
    }

    #[test]
    fn format_with_si_prefix_finite() {
        assert_eq!(fmt_si_pfx!("{:+}", 1e-31_f64).to_string(), "+0 ");
        assert_eq!(fmt_si_pfx!("{}", -1e-31_f64).to_string(), "-0 ");
        assert_eq!(fmt_si_pfx!("{}", 1e-31_f64).to_string(), "0 ");
        assert_eq!(fmt_si_pfx!("{}", 1e-30_f64).to_string(), "1 q");
        assert_eq!(fmt_si_pfx!("{}", 1e-29_f64).to_string(), "10 q");

        assert_eq!(fmt_si_pfx!("{}", 1e-4_f64).to_string(), "100 μ");
        assert_eq!(fmt_si_pfx!("{}", 1e-3_f64).to_string(), "1 m");
        assert_eq!(fmt_si_pfx!("{}", 1e-2_f64).to_string(), "10 m");
        assert_eq!(fmt_si_pfx!("{}", 1e-1_f64).to_string(), "100 m");
        assert_eq!(fmt_si_pfx!("{}", 1e+0_f64).to_string(), "1 ");
        assert_eq!(fmt_si_pfx!("{}", 1e+1_f64).to_string(), "10 ");
        assert_eq!(fmt_si_pfx!("{}", 1e+2_f64).to_string(), "100 ");
        assert_eq!(fmt_si_pfx!("{}", 1e+3_f64).to_string(), "1 k");

        assert_eq!(fmt_si_pfx!("{}", 1e+30_f64).to_string(), "1 Q");
        assert_eq!(fmt_si_pfx!("{}", 1e+31_f64).to_string(), "10 Q");
        assert_eq!(fmt_si_pfx!("{}", 1e+32_f64).to_string(), "100 Q");
        assert_eq!(
            fmt_si_pfx!("{}", 1e+33_f64).to_string(),
            "1\u{202f}×\u{202f}10³³ "
        );

        // If the precision is omitted, it should output as many digits as
        // necessary to enable roundtrip conversion
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-35_f64).to_string(), "0.0000 ");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-34_f64).to_string(), "0.0001 q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-33_f64).to_string(), "0.0012 q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-32_f64).to_string(), "0.0123 q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-31_f64).to_string(), "0.1234 q");
        assert_eq!(fmt_si_pfx!("{}", -1.2343e-31_f64).to_string(), "-0.1234 q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-31_f64).to_string(), "0.1234 q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-30_f64).to_string(), "1.2343 q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-29_f64).to_string(), "12.343 q");

        assert_eq!(fmt_si_pfx!("{}", 1.2343e-4_f64).to_string(), "123.43 μ");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-3_f64).to_string(), "1.2343 m");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-2_f64).to_string(), "12.343 m");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e-1_f64).to_string(), "123.43 m");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e+0_f64).to_string(), "1.2343 ");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e+1_f64).to_string(), "12.343 ");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e+2_f64).to_string(), "123.43 ");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e+3_f64).to_string(), "1.2343 k");

        assert_eq!(fmt_si_pfx!("{}", 1.2343e+30_f64).to_string(), "1.2343 Q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e+31_f64).to_string(), "12.343 Q");
        assert_eq!(fmt_si_pfx!("{}", 1.2343e+32_f64).to_string(), "123.43 Q");
        assert_eq!(
            fmt_si_pfx!("{}", 1.2343e+33_f64).to_string(),
            "1.2343\u{202f}×\u{202f}10³³ "
        );

        // If the precision is specified, it should output that number of digits
        assert_eq!(
            fmt_si_pfx!("{:+.4}", 1.2343e-35_f64).to_string(),
            "+0.0000 "
        );
        assert_eq!(
            fmt_si_pfx!("{:.4}", -1.2343e-35_f64).to_string(),
            "-0.0000 "
        );
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-35_f64).to_string(), "0.0000 ");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-34_f64).to_string(), "0.0001 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-33_f64).to_string(), "0.0012 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-32_f64).to_string(), "0.0123 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-31_f64).to_string(), "0.1234 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-30_f64).to_string(), "1.2343 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-29_f64).to_string(), "12.343 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-28_f64).to_string(), "123.43 q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-27_f64).to_string(), "1.2343 r");

        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-4_f64).to_string(), "123.43 μ");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-3_f64).to_string(), "1.2343 m");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-2_f64).to_string(), "12.343 m");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e-1_f64).to_string(), "123.43 m");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+0_f64).to_string(), "1.2343 ");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+1_f64).to_string(), "12.343 ");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+2_f64).to_string(), "123.43 ");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+3_f64).to_string(), "1.2343 k");

        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+30_f64).to_string(), "1.2343 Q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+31_f64).to_string(), "12.343 Q");
        assert_eq!(fmt_si_pfx!("{:.4}", 1.2343e+32_f64).to_string(), "123.43 Q");
        assert_eq!(
            fmt_si_pfx!("{:.4}", 1.2343e+33_f64).to_string(),
            "1.2343\u{202f}×\u{202f}10³³ "
        );
    }
    #[test]
    fn format_with_si_prefix_nan() {
        assert_eq!(fmt_si_pfx!("{}", f64::NAN), "NaN ");
    }

    #[test]
    fn format_with_si_prefix_infinity() {
        assert_eq!(fmt_si_pfx!("{}", f64::INFINITY), "inf ");
        assert_eq!(fmt_si_pfx!("{:+}", f64::INFINITY), "+inf ");
    }

    #[test]
    fn format_with_si_prefix_neg_infinity() {
        assert_eq!(fmt_si_pfx!("{}", f64::NEG_INFINITY).to_string(), "-inf ");
    }

    proptest! {
        #[test]
        fn pt_format_with_si_prefix(x: f64) {
            let _s = fmt_si_pfx!("{}", x).to_string();

            // TODO: Test `parse_with_si_prefix`
            // if !s.contains("×") {
            //     let y = parse_with_si_prefix(&s)
            //         .unwrap_or_else(|| panic!("failed to parse {s:?}"));
            //     prop_assert_eq!(x, y, "{:?} != {:?} (parsed {:?})", x, y, s);
            // }
        }
    }
}
