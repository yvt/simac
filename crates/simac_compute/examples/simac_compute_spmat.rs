use anyhow::{Context, Result};
use clap::Parser;
use itertools::izip;
use num_traits::Num;
use std::{
    fs::File,
    io::Read,
    path::{Path, PathBuf},
    time::{Duration, Instant},
};

/// Run `simac_compute`'s algorithms on a given sparse matrix
#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    /// Input matrix in Matrix Market Exchange format
    input: PathBuf,
}

fn main() -> Result<()> {
    let args = Args::parse();

    tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(tracing_subscriber::filter::LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .init();

    // Load the input matrix
    let mat = load_matrix_from_file::<f64>(&args.input)
        .with_context(|| format!("failed to read input matrix from file {:?}", args.input))?;
    tracing::trace!(?mat);

    // Dump information about the input matrix with log level `Info` or higher
    tracing::info!(mat.m, mat.n, "Matrix dimensions");
    tracing::info!(mat.nnz = mat.val.len());

    // LU decomposition
    if mat.m == mat.n {
        let _entered = tracing::info_span!("LU decomposition");

        struct Timings {
            decompose: Duration,
            sort: Duration,
        }

        let mut out = simac_compute::utils_csc::MatCscOwned {
            m: mat.m,
            n: mat.n,
            col_start: vec![usize::MAX; mat.n + 1],
            row_i: Vec::with_capacity(mat.row_i.len()),
            val: Vec::with_capacity(mat.row_i.len()),
        };
        let mut out_perm = vec![0; mat.n];

        // Perform decomposition a few times to get accurate timings
        let mut decompose = || {
            out.row_i.clear();
            out.val.clear();

            let t = Instant::now();
            simac_compute::slu_csc::decompose(
                &mat.col_start,
                &mat.row_i,
                &mat.val,
                &mut out.col_start,
                &mut out.row_i,
                &mut out.val,
                &mut out_perm,
            );
            let decompose = t.elapsed();

            let t = Instant::now();
            simac_compute::utils_csc::sort_row_i(&out.col_start, &mut out.row_i, &mut out.val);
            let sort = t.elapsed();

            Timings { decompose, sort }
        };

        for i in 0..2 {
            tracing::debug!(i, "Warming up");
            decompose();
        }

        tracing::debug!("Real run starting");
        let timings = decompose();

        let error = simac_compute::slu_csc::evaluate_mse(
            &mat.col_start,
            &mat.row_i,
            &mat.val,
            &out.col_start,
            &out.row_i,
            &out.val,
            &out_perm,
        );

        // Print the result
        tracing::info!(nnz_LUmI = out.val.len(), error, "LU decomposition complete");
        tracing::info!(?timings.decompose, ?timings.sort, "Timings");
    } else {
        tracing::info!("Not performing LU decomposition because the matrix is not square");
    }

    Ok(())
}

fn load_matrix_from_file<T: Num + Send + Clone>(
    path: &Path,
) -> Result<simac_compute::utils_csc::MatCscOwned<T>> {
    if file_starts_with(path, b"%%MatrixMarket")? {
        tracing::debug!(?path, "Reading the file as a Matrix Market Exchange file");
        return matrix_market_rs::MtxData::from_file(path)
            .context("failed to read the file as a Matrix Market Exchange file")
            .map(|x| csc_from_matrix_market(&x));
    }

    // Unknown format, perhaps it's a `.tar.gz` archive containing the matrix
    match () {
        #[cfg(unix)]
        () => {
            tracing::debug!(
                ?path,
                "The file is not a Matrix Market Exchange file; attempting to \
                read it as an archive file"
            );

            // `matrix_market_rs::MtxData::from_file` only accepts a file path.
            // Use a named pipe to feed decompressed data to it without
            // creating a temporary file.
            let file = File::open(path).context("failed to open the file for reading")?;
            input_extractor::detect_format_and_pipe_in(&file, |path| {
                tracing::debug!(?path, "Reading the pipe as a Matrix Market Exchange file");
                Ok(matrix_market_rs::MtxData::from_file(path)?)
            })
            .context(
                "failed to read the file as an archive file containing \
                a Matrix Market Exchange file",
            )
            .map(|x| csc_from_matrix_market(&x))
        }

        #[cfg(not(unix))]
        () => anyhow::bail!("unsupported file format"),
    }
}

fn csc_from_matrix_market<T: Num + Clone>(
    data: &matrix_market_rs::MtxData<T>,
) -> simac_compute::utils_csc::MatCscOwned<T> {
    let mat = match data {
        matrix_market_rs::MtxData::Dense(..) => todo!(),
        matrix_market_rs::MtxData::Sparse(dims, coords, vals, syminfo) => {
            let mut elements: Vec<_> = izip!(coords, vals)
                .map(|(&[i, j], val)| (i, j, val.clone()))
                .collect();
            if *syminfo == matrix_market_rs::SymInfo::Symmetric {
                elements.extend(
                    izip!(coords, vals)
                        .filter(|&(&[i, j], _)| i != j)
                        .map(|(&[i, j], val)| (j, i, val.clone())),
                );
            }
            elements.retain(|x| !x.2.is_zero());
            simac_compute::utils_csc::MatCscOwned::from_coords_aos(dims[0], dims[1], elements)
        }
    };
    mat.as_ref().validate().expect("matrix validation failed");
    mat
}

fn file_starts_with(path: &Path, needle: &[u8]) -> std::io::Result<bool> {
    let mut buf = vec![0u8; needle.len()];
    let mut file = File::open(path)?;
    match file.read_exact(&mut buf) {
        Ok(()) => Ok(&buf[..] == needle),
        Err(error) if error.kind() == std::io::ErrorKind::UnexpectedEof => Ok(false),
        Err(error) => Err(error),
    }
}

#[cfg(unix)]
mod input_extractor {
    use super::*;

    pub fn detect_format_and_pipe_in<T: Send>(
        file: &File,
        f: impl FnOnce(&Path) -> Result<T> + Send,
    ) -> Result<T> {
        let tar_stream =
            libflate::gzip::Decoder::new(file).context("failed to create a gzip decoder")?;
        let mut tar_archive = tar::Archive::new(tar_stream);
        let mut paths = Vec::new();
        for entry in tar_archive
            .entries()
            .context("failed to read the input stream as a tar archive")?
        {
            let mut entry = entry.context("failed to read a tar entry")?;
            let path = entry.path().context("failed to read a tar entry name")?;
            tracing::debug!(?path, "Found a tar entry");
            if paths.len() < 5 {
                paths.push((*path).to_owned());
            }

            if path.extension().is_none_or(|x| x != "mtx") {
                tracing::trace!("The entry name does not match the pattern");
                continue;
            }

            // Found the Matrix Market Exchange file; create a pipe
            return pipe_in(&mut entry, f);
        }

        anyhow::bail!(
            "unable to locate a tar entry matching the pattern. \
            discovered entries (up to 5): {paths:?}"
        );
    }

    /// Make the specified stream available through a named pipe.
    fn pipe_in<T: Send>(
        stream: &mut dyn Read,
        f: impl FnOnce(&Path) -> Result<T> + Send,
    ) -> Result<T> {
        let tempdir = tempfile::tempdir().context("failed to create a temporary directory")?;
        tracing::debug!(path = ?tempdir.path(), "Created a temporary directory");

        let fifo_path = tempdir.path().join("pipe");
        nix::unistd::mkfifo(&fifo_path, nix::sys::stat::Mode::S_IRWXU)
            .with_context(|| format!("failed to create a named pipe at {fifo_path:?}"))?;
        let _guard = scopeguard::guard((), |()| {
            if let Err(error) = std::fs::remove_file(&fifo_path) {
                tracing::warn!(?fifo_path, ?error, "Failed to remove a named pipe");
            }
        });
        tracing::debug!(?fifo_path, "Created a named pipe");

        std::thread::scope(|scope| {
            let handle = scope.spawn(|| f(&fifo_path));

            let fifo_send = File::options()
                .read(false)
                .write(true)
                .open(&fifo_path)
                .context("Failed to open a named pipe for writing")?;

            tracing::trace!("Copying bytes from the input stream into the named pipe");
            let result = std::io::copy(stream, &mut &fifo_send);
            tracing::trace!(?result, "Completd a write to the named pipe");

            handle.join().unwrap()
        })
    }
}
