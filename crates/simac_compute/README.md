# `simac_compute`

Compute kernels for SimAC

## Basics

### Matrix Storage Formats

#### CSC Storage Format

The Compressed Sparse Column (CSC) storage format represents a `m * n` matrix by the following fields:

- `m: usize`: The number of rows.
- `n: usize`: The number of columns.
- `nnz: usize`: The number of non-zero elements.
- `csc_col_start: [Index; n + 1]`: The start index of every column within `csc_row_i` and `csc_val`.
- `csc_row_i: [Index; nnz]`: The row index of every non-zero element in `csc_val`.
  Should be sorted within each column and in range `0..m`.
- `csc_val: [T]`: The non-zero elements.


## References

`[tag:gilbert88]` J. R. Gilbert and T. Peierls, “Sparse partial pivoting in time proportional to arithmetic operations,” SIAM Journal on Scientific and Statistical Computing, 1988.
