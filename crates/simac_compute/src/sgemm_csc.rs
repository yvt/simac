//! Sparse generic matrix-matrix multiplication, CSC storage format
use std::ops::Mul;

pub(crate) trait GemmGenericInput<Element> {
    /// The numbers of rows and columns.
    fn dims(&self) -> [usize; 2];

    /// Iterate over the specified column's elements in any order.
    fn iter_col<'a>(&'a self, col_i: usize) -> impl Iterator<Item = (usize, &'a Element)>
    where
        Element: 'a;
}

pub(crate) trait GemmGenericOutput<Element> {
    fn accumulate(&mut self, row_i: usize, value: Element);
    fn finish_col(&mut self);
}

pub(crate) fn gemm_generic<Element>(
    lhs: impl GemmGenericInput<Element>,
    rhs: impl GemmGenericInput<Element>,
    mut out: impl GemmGenericOutput<Element>,
) where
    Element: Copy + Mul<Output = Element>,
{
    assert_eq!(lhs.dims()[1], rhs.dims()[0]);

    for j in 0..rhs.dims()[1] {
        for (i, &val_rhs) in rhs.iter_col(j) {
            debug_assert!(i < rhs.dims()[0]);
            debug_assert!(i < lhs.dims()[1]);

            for (k, &val_lhs) in lhs.iter_col(i) {
                // `out[k, j] += lhs[k, i] * rhs[i, j]`
                out.accumulate(k, val_lhs * val_rhs);
            }
        }

        out.finish_col();
    }
}
