//! Sparse generic matrix-vector multiplication, CSC storage format
use itertools::izip;
use std::ops::{Add, Mul};

use crate::utils_csc::range;

/// Multiply the specified sparse matrix by the specified dense vector and
/// accumulate the result.
///
/// # Input Matrix Format
///
/// The input matrix should be provided in the CSC storage format, except that
/// `lhs_row_i` does not need to be sorted within each column.
///
/// # Complexity
///
/// `O(lhs_val.len())`
pub fn gemv_accumulate<Element>(
    lhs_col_start: &[usize],
    lhs_row_i: &[usize],
    lhs_val: &[Element],
    rhs_val: &[Element],
    out_val: &mut [Element],
) where
    Element: Copy + Add<Output = Element> + Mul<Output = Element>,
{
    assert_eq!(rhs_val.len() + 1, lhs_col_start.len());

    for (&val_rhs, col_range) in izip!(rhs_val.iter(), lhs_col_start.windows(2)) {
        for (&k, &val_lhs) in izip!(&lhs_row_i[range(col_range)], &lhs_val[range(col_range)]) {
            // `out[k, j] += lhs[k, i] * rhs[i, j]`
            out_val[k] = out_val[k] + val_lhs * val_rhs;
        }
    }
}
