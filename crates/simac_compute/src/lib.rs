#![doc = include_str!("../README.md")]

pub mod utils_csc;

pub mod sgemm_csc;
pub mod sgemv_csc;
pub mod slu_csc;

#[cfg(test)]
mod tests;
