//! Sparse LU decomposition, CSC storage format `[ref:gilbert88]`
use itertools::{izip, rev};
use num_traits::Float;

use crate::{sgemm_csc, utils_csc::range};

/// Perform LU decomposition with partial pivoting (`P*A = L*U`) and write
/// `L + U - I` to `*out_col_start`, `*out_row_i`, and `*out_val` and
/// `P` to `*out_perm`.
///
/// `*out_row_i` and `*out_val` must be empty before the call.
///
/// # Input/Output Format
///
/// The input should be provided in the CSC storage format.
///
/// The output matrix is laid out in the CSC storage format except that
/// `csc_row_i` is partitioned into `L-I`, `U∘I`, and `U-U∘I` (in this order),
/// and each partition is unsorted.
/// Use [`crate::utils_csc::sort_row_i`] to sort if necessary.
///
/// The permutation matrix `P` is represented as an array of `n` indices. If
/// `out_perm[r] = s`, then row `r` of `A` is in row `s` of `P*A`.
#[tracing::instrument(level = "trace", skip_all)]
pub fn decompose<T>(
    in_col_start: &[usize],
    in_row_i: &[usize],
    in_val: &[T],
    out_col_start: &mut [usize],
    out_row_i: &mut Vec<usize>,
    out_val: &mut Vec<T>,
    out_perm: &mut [usize],
) where
    T: Float + std::fmt::Debug,
{
    let size = in_col_start.len() - 1; // n, m

    assert_eq!(out_col_start.len(), in_col_start.len());

    assert!(out_row_i.is_empty());
    assert!(out_val.is_empty());
    out_row_i.reserve(in_row_i.len());
    out_val.reserve(in_row_i.len());

    assert_eq!(out_perm.len(), size);
    izip!(0.., &mut out_perm[..]).for_each(|(i, p)| *p = i);

    // `transpose(P)`
    let mut rev_perm: Vec<usize> = out_perm.to_owned();

    // The column index of the rightmost nonzero element of every row.
    // `usize::MAX` if it doesn't exist yet.
    let mut out_row_max_col_i: Vec<usize> = vec![usize::MAX; size];

    // An output colum under construction
    let mut out_col_val: Vec<T> = (0..size).map(|_| T::zero()).collect();

    // A stack for depth-first search
    struct Visit {
        /// `U[row_i,j]` is being visited
        row_i: usize,
        /// The element index of the next successor.
        cursor: usize,
    }
    let mut visit_stack: Vec<Visit> = Vec::with_capacity(size);

    out_col_start[0] = 0;

    for (j, in_col_range) in izip!(0.., in_col_start.windows(2)) {
        // We'll compute column `j` of `U` and `L`
        // (`U[0..j + 1, j]`, `L[j + 1..size, j]`).
        //
        //         ┌───────────┬─┬──────────┐
        //  row 0 →│'.         │ │          │
        //         │  '.   U   │ │          │
        //         │    '.     │ │          │
        //         │      '.   │ │          │
        //         │        '. │ │    A     │
        //         ├┄┄┄┄┄┄┄┄┄┄┄┼─┤          │
        //  row j →│           │ │          │
        //         │     L     │ │          │
        //         │           │ │          │
        //         │           │ │          │
        //         └───────────┴─┴──────────┘
        //          ↑           ↑
        //      column 0     column j
        //
        // If no pivoing takes place:
        //
        //     L[.., 0..=j] * U[0..=j, j] = A[.., j]
        //
        // Equivalently:
        //
        //     for i in 0..j:
        //         A[i, j] = dot(transpose(L[i, 0..=i]), U[0..=i, j])
        //     for i in j..size:
        //         A[i, j] = dot(transpose(L[i, 0..=j]), U[0..=j, j])
        //                 = dot(transpose(L[i, 0..j]), U[0..j, j])
        //                   + L[i, j] * U[j, j]
        //
        // A possible algorithm:
        //
        //     1.  Solve `L[0..j, 0..j] * U[0..j, j] = A[0..j, j]`
        //         for `U[0..j, j]`. Specifically:
        //
        //         U[0..j, j] := A[0..j, j]
        //         for i in 0..j:
        //             U[i+1..j, j] -= L[i+1..j, i] * U[i, j]
        //
        //     2.  `B[j..size] := A[j..size, j] -
        //          L[j..size, 0..j] * U[0..j, j]`. Specifically:
        //
        //         B[j..size] := A[j..size, j]
        //         for i in 0..j:
        //             B[j..size] -= L[j..size, i] * U[i, j]
        //
        //     3.  `U[j, j] := B[j]`
        //
        //     4.  `L[j..size, j] := B[j..size] / B[j]`
        //
        // n.b. It's implicit that `L[j, j] == 1`.
        //
        // Step 1 involves solving a lower triangular system. Normally, we
        // would solve a lower triangular linear system in increasing order of
        // row number, but actually we can solve it in any topological order
        // of graph G comprised of non-zero positions of `L[0..j, 0..j]`.
        // (Increasing row order is just one possible topological order, but
        // it's costly to achieve because of row reordering.)
        //
        //        ╭─────────────╮
        //        │             ▼
        //     A[0,j] =        U[0,j]
        //                      │
        //        ╭─────────────│───────────────╮
        //        │             │               ▼
        //        │        ╭┄┄┄┄│┄╭─────────────╮
        //        │        ┊    ▼ │             ▼
        //     A[1,j] = L[1,0]*U[0,j] +        U[1,j]
        //                      │               │
        //        ╭─────────────│───────────────│────────────────╮
        //        │             │               │                ▼
        //        │        ╭┄┄┄┄│┄╭─────────────│────────────────╮
        //        │        ┊    │ │             │                ▼
        //        │        ┊    │ │        ╭┄┄┄┄│┄╭──────────────╮
        //        │        ┊    ▼ │        ┊    ▼ │              ▼
        //     A[2,j] = L[2,0]*U[0,j] + L[2,1]*U[1,j] +        U[2,j]
        //                      │               │                │
        //                      ▼               ▼                ▼
        //     ...
        //
        // As for step 2, if we substitute `B[j..size]` with `U[j..size, j]`,
        // step 2 is actually just a continuation of step 1, except that
        // `B[j..size]` is the new unknowns and that no more elements of
        // `U[.., j]` are introduced.
        //
        //       ...
        //                       │                       │
        //        ╭──────────────│───────────────────────│─────────╮
        //        │              │                       │         ▼
        //        │        ╭┄┄┄┄┄│┄╭─────────────────────│─────────╮
        //        │        ┊     │ │                     │         ▼
        //        │        ┊     │ │             ╭┄┄┄┄┄┄┄│┄╭───────╮
        //        │        ┊     ▼ │             ┊       ▼ │       ▼
        //     A[i,j] = L[i,0]*U[0,j] + ... + L[i,j-1]*U[j-1,j] + B[i]
        //                       │                       │
        //      ...              │                       │
        //                       ▼                       ▼
        //     A[size-1,j] = ...

        debug_assert_eq!(out_col_start[j], out_row_i.len());
        let col_start_i = out_row_i.len();

        // ------------------------------------------------------------------
        // Step 1: Symbolic step (determine nonzero structure)

        // Start depth-first search from nonzero elements in `A[.., j]`
        for &row_i in &in_row_i[range(in_col_range)] {
            if out_row_max_col_i[row_i] == j {
                // The node already has a permanent mark
                continue;
            }

            visit_stack.push(Visit {
                row_i,
                cursor: out_col_start[out_perm[row_i]],
            });

            'dfs: while let Some(current) = visit_stack.last_mut() {
                let current_row_i_post_perm = out_perm[current.row_i];
                if current_row_i_post_perm < j {
                    // Find the next successor of the current node by scanning
                    // `L[current.row_i+1..j, current.row_i]`
                    //
                    // (successors = a subset of `U[0..j, j]` that may become
                    // solvable when `U[current.row_i, j]` is solved)
                    let col_end = out_col_start[current_row_i_post_perm + 1];
                    while current.cursor != col_end {
                        let other_row_i = out_row_i[current.cursor];
                        current.cursor += 1;

                        if out_perm[other_row_i] <= current_row_i_post_perm {
                            // [ref:slu_csc_row_i_partition_diag]
                            // `current.cursor` has entered the `U` partition;
                            // we'll find no more elements of `L`.
                            break;
                        }

                        if out_row_max_col_i[other_row_i] == j {
                            // This successor already has a permanent mark
                            continue;
                        }

                        // Visit this successor
                        visit_stack.push(Visit {
                            row_i: other_row_i,
                            cursor: out_col_start[out_perm[other_row_i]],
                        });

                        continue 'dfs;
                    }
                }

                // No more successors to visit. Create an entry for
                // `U[current.row_i, j]`, marking the node with a permanent
                // mark, and leave.
                debug_assert_ne!(out_row_max_col_i[current.row_i], j);
                out_row_max_col_i[current.row_i] = j;
                out_row_i.push(current.row_i);
                visit_stack.pop();
            } // 'dfs
        }

        tracing::trace!(
            j,
            num_vals = out_row_i.len() - col_start_i,
            "Symbolic step complete"
        );

        // [tag:slu_csc_row_i_partition]
        // Partition `out_row_i[col_start_i..]` into `B` (`row_i >= j`) and
        // `U` (`row_i < j`) while preserving the order of `U`, thus preserving
        // topological order
        //
        // Note that the diagonal element (`row_i == j`) is a part of `B` for
        // now, until we relabel it as `U` ([ref:slu_csc_row_i_partition_diag]).
        //
        //          ┌─┐
        //  row 0 → │ │ ╮
        //          │ │ │ out_row_i[u_start_i..]
        //          │ │ │ (U partition)
        //          │ │ ╯
        //          ├─┤
        //  row j → │ │ ╮
        //          ├─┤ │ out_row_i[col_start_i..u_start_i]
        //          │ │ │ (B partition)
        //          │ │ ╯
        //          └─┘
        //           ↑
        //        column j
        //
        let mut u_start_i = {
            let mut slice = &mut out_row_i[col_start_i..];
            let mut i = slice.len().wrapping_sub(1);
            while let Some(&row_i) = slice.get(i) {
                if out_perm[row_i] < j {
                    // `slice[i] == row_i` belongs to `U`. Swap it with
                    // `slice[len - 1]` and shorten the `slice` window to
                    // exclude it
                    slice.swap(i, slice.len() - 1);
                    slice = slice.split_last_mut().unwrap().1;
                }
                i = i.wrapping_sub(1);
            }
            col_start_i + slice.len()
        };

        tracing::trace!(
            j,
            num_b = u_start_i - col_start_i,
            num_u = out_row_i.len() - u_start_i,
            "Partitioned `out_row_i` into `U` and `B`"
        );

        // ------------------------------------------------------------------
        // Step 2: Numeric step (calculate nonzero elements)

        // Initialize `concat(U[0..j, j], B[j..size])` with `A[.., j]`
        debug_assert!(
            out_row_i[col_start_i..]
                .iter()
                .all(|&i| out_col_val[i].is_zero())
        );
        for i in range(in_col_range) {
            let (row_i, val_a) = (in_row_i[i], in_val[i]);
            out_col_val[row_i] = val_a;
        }

        // We have pushed the entries `out_{row_i,val}[col_start_i..]` in
        // postorder, so we can now visit `U[0..j, j]` in topological order by
        // scanning those entries in reverse.
        for i in rev(u_start_i..out_row_i.len()) {
            let row_i = out_row_i[i];
            let row_i_post_perm = out_perm[row_i];
            let val_u = out_col_val[row_i];

            // Check `L[row_i + 1.., row_i] != 0`
            let other_col_range = &out_col_start[out_perm[row_i]..][..2];
            for i in range(other_col_range) {
                let other_row_i = out_row_i[i];
                if out_perm[other_row_i] <= row_i_post_perm {
                    // [ref:slu_csc_row_i_partition_diag]
                    // `i` represents an element of `U`, not `L`.
                    break;
                }
                let val_l = out_val[i];

                // if other_row_i < j:
                //     U[other_row_i, j] -= L[other_row_i, row_i] * U[row_i, j]
                // else:
                //     B[other_row_i] -= B[other_row_i] * U[row_i, j]
                out_col_val[other_row_i] = out_col_val[other_row_i] - val_u * val_l;
            }
        }

        tracing::trace!(j, "U/B numeric step complete");

        // ------------------------------------------------------------------
        // Step 3: Partial pivoting

        // Find the element with the largest magnitude in `B[j..size]`.
        let (i, max_row_i, _) = izip!(col_start_i.., &out_row_i[col_start_i..u_start_i])
            .map(|(i, &row_i)| (i, row_i, out_col_val[row_i].abs()))
            .fold((usize::MAX, usize::MAX, T::zero()), |max, elem| {
                let val: &T = &elem.2;
                if *val > max.2 { elem } else { max }
            });

        if max_row_i == usize::MAX {
            // U[j, j]           = 0
            // L[j, j]           = 1
            // L[j + 1..size, j] = [0, ...]
            tracing::trace!(j, "No valid pivot found");
        } else {
            // Swap the element's row and row `j` by relabeling rows
            let max_row_i_post_perm = out_perm[max_row_i];
            out_perm.swap(max_row_i, rev_perm[j]);
            rev_perm.swap(j, max_row_i_post_perm);

            debug_assert_eq!(out_perm[max_row_i], j);
            debug_assert_eq!(rev_perm[j], max_row_i);

            // [ref:slu_csc_row_i_partition]
            // [tag:slu_csc_row_i_partition_diag]
            // We're going to relabel this `B[j]` as `U[j, j]`. Move it into
            // the `U` partition of `out_row_i[out_col_range]`
            //
            //          ┌─┐
            //  row 0 → │ │ ╮
            //          │ │ │
            //          │ │ │ out_row_i[u_start_i - 1..]
            //          │ │ │ (U partition)
            //          ├─┤ │
            //  row j → │ │ ╯
            //          ├─┤
            //          │ │ ╮
            //          │ │ │ out_row_i[col_start_i..u_start_i - 1]
            //          │ │ ╯ (B/L partition)
            //          └─┘
            //           ↑
            //        column j
            //
            u_start_i -= 1;
            out_row_i.swap(u_start_i, i);
        }

        // ------------------------------------------------------------------
        // Step 4: `U[j, j] := B[j]; L[j..size, j] := B[j..size] / B[j]`
        let max_val = out_col_val
            .get(max_row_i)
            .copied()
            .unwrap_or_else(T::infinity);
        let factor = max_val.recip();

        if max_row_i != usize::MAX {
            tracing::trace!(j, i = max_row_i, ?max_val, "Pivot found");
        }

        for &row_i in &out_row_i[col_start_i..u_start_i] {
            let val = &mut out_col_val[row_i];
            *val = *val * factor;
        }

        // ------------------------------------------------------------------
        // Step 5: Copy the nonzero elements of `out_col_val` into `out_val`
        out_val.reserve(out_col_val.len());

        for i in col_start_i..out_row_i.len() {
            let row_i = out_row_i[i];
            if !out_col_val[row_i].is_zero() {
                out_row_i[out_val.len()] = out_row_i[i];
                out_val.push(out_col_val[row_i]);
                out_col_val[row_i] = T::zero();
            }
        }

        if out_val.len() < out_row_i.len() {
            tracing::trace!(
                j,
                num = out_row_i.len() - out_val.len(),
                "Pruning exact zeros created in the current column"
            );

            out_row_i.truncate(out_val.len());
        }

        out_col_start[j + 1] = out_row_i.len();
    }

    // Apply row relabeling to `*out_row_i`
    for p_row_i in out_row_i {
        *p_row_i = out_perm[*p_row_i];
    }
}

/// Multiply the LU factors calculated by [`decompose`] to evaluate the mean
/// squared error.
///
/// Returns [`Float::nan`] if the matrix has no elements.
///
/// # Input Format
///
/// The input should be provided in the CSC storage format.
pub fn evaluate_mse<T>(
    in_col_start: &[usize],
    in_row_i: &[usize],
    in_val: &[T],
    lu_col_start: &[usize],
    lu_row_i: &[usize],
    lu_val: &[T],
    lu_perm: &[usize],
) -> T
where
    T: Float,
{
    let size = in_col_start.len() - 1; // n, m

    assert_eq!(lu_col_start.len(), in_col_start.len());

    // The inputs to `gemm_generic`
    struct Input<'a, T, Part> {
        lu_col_start: &'a [usize],
        lu_row_i: &'a [usize],
        lu_val: &'a [T],
        part: Part,
    }
    struct FactorL<T> {
        one: T,
    }
    struct FactorU;

    let lhs = Input {
        lu_col_start,
        lu_row_i,
        lu_val,
        part: FactorL { one: T::one() },
    };

    impl<T: Float> sgemm_csc::GemmGenericInput<T> for Input<'_, T, FactorL<T>> {
        #[inline]
        fn dims(&self) -> [usize; 2] {
            [self.lu_col_start.len() - 1; 2]
        }

        #[inline]
        fn iter_col<'a>(&'a self, col_i: usize) -> impl Iterator<Item = (usize, &'a T)>
        where
            T: 'a,
        {
            let col_range = &self.lu_col_start[col_i..][..2];
            range(col_range)
                .rev()
                .map(|i| (self.lu_row_i[i], &self.lu_val[i]))
                .take_while(move |&(row_i, _)| row_i > col_i)
                .chain([(col_i, &self.part.one)])
        }
    }

    let rhs = Input {
        lu_col_start,
        lu_row_i,
        lu_val,
        part: FactorU,
    };

    impl<T: Float> sgemm_csc::GemmGenericInput<T> for Input<'_, T, FactorU> {
        #[inline]
        fn dims(&self) -> [usize; 2] {
            [self.lu_col_start.len() - 1; 2]
        }

        #[inline]
        fn iter_col<'a>(&'a self, col_i: usize) -> impl Iterator<Item = (usize, &'a T)>
        where
            T: 'a,
        {
            let col_range = &self.lu_col_start[col_i..][..2];
            range(col_range)
                .map(|i| (self.lu_row_i[i], &self.lu_val[i]))
                .take_while(move |&(row_i, _)| row_i <= col_i)
        }
    }

    // The output sink of `gemm_generic`
    struct Output<'a, T> {
        in_col_start: &'a [usize],
        in_row_i: &'a [usize],
        in_val: &'a [T],
        lu_perm: &'a [usize],

        /// The column index of the rightmost nonzero element of every row.
        /// `usize::MAX` if it doesn't exist yet
        out_row_max_col_i: Vec<usize>,

        current_col_i: usize,

        /// Every element of the current column. `out_col[row_i]` contains a
        /// stale value if `out_row_max_col_i[row_i] != current_col_i`.
        out_col: Vec<T>,

        /// `[i for i, j for enumerate(out_row_max_col_i) if j == current_col_i]`
        out_row_i_list: Vec<usize>,

        /// The error sum.
        error: T,
    }

    let mut output = Output {
        in_col_start,
        in_row_i,
        in_val,
        lu_perm,
        out_row_max_col_i: vec![usize::MAX; size],
        current_col_i: 0,
        out_col: (0..size).map(|_| T::zero()).collect(),
        out_row_i_list: Vec::with_capacity(size),
        error: T::zero(),
    };

    impl<T: Float> sgemm_csc::GemmGenericOutput<T> for &mut Output<'_, T> {
        #[inline]
        fn accumulate(&mut self, row_i: usize, value: T) {
            let max_col_i = &mut self.out_row_max_col_i[row_i];
            let out_val = &mut self.out_col[row_i];
            if *max_col_i == self.current_col_i {
                *out_val = *out_val + value;
            } else {
                *out_val = value;
                *max_col_i = self.current_col_i;
                self.out_row_i_list.push(row_i);
            }
        }

        #[inline]
        fn finish_col(&mut self) {
            // Subtract the input (pre-decomposition) column
            let col_range = &self.in_col_start[self.current_col_i..][..2];
            for i in range(col_range) {
                let (row_i, val) = (self.in_row_i[i], self.in_val[i]);
                self.accumulate(self.lu_perm[row_i], -val);
            }

            // Accumulate the error
            for &row_i in &self.out_row_i_list {
                self.error = self.error + self.out_col[row_i].powi(2);
            }

            // Move on to the next column
            self.current_col_i += 1;
            self.out_row_i_list.clear();
        }
    }

    sgemm_csc::gemm_generic(lhs, rhs, &mut output);

    output.error / T::from(size).unwrap().powi(2)
}

/// Solve a linear system `L * U * out = in` using the LU factors calculated by
/// [`decompose`].
///
/// # Input Matrix Format
///
/// The input matrix should be provided in [`decompose`]'s output format.
pub fn solve<T>(lu_col_start: &[usize], lu_row_i: &[usize], lu_val: &[T], inout_val: &mut [T])
where
    T: Float + std::fmt::Debug,
{
    assert_eq!(lu_col_start.len(), inout_val.len() + 1);

    // Solve `L * out' = in` for `out'` (forward elimination)
    let diag: Vec<T> = lu_col_start
        .windows(2)
        .enumerate()
        .map(|(i, col_start)| {
            // `in_out_val[0..=i]` holds `out'[0..=i]`
            let x = inout_val[i];

            let mut diag = T::zero();
            for (&row_i, &val_l) in izip!(&lu_row_i[range(col_start)], &lu_val[range(col_start)]) {
                if row_i <= i {
                    if row_i == i {
                        // Remember `lu[i, i]`
                        diag = val_l;
                    }
                    break;
                }

                inout_val[row_i] = inout_val[row_i] - x * val_l;
            }

            diag
        })
        .collect();

    // Solve `U * out = out'` for `out` (backward elimination)
    for ((i, col_start), &diag) in rev(izip!(lu_col_start.windows(2).enumerate(), &diag)) {
        // `out'[0..=i]`
        let x = inout_val[i] / diag;
        inout_val[i] = x;

        for (&row_i, &val_u) in rev(izip!(
            &lu_row_i[range(col_start)],
            &lu_val[range(col_start)]
        )) {
            if row_i >= i {
                if !diag.is_zero() {
                    debug_assert!(val_u == diag || (val_u.is_nan() && diag.is_nan()));
                }
                break;
            }

            inout_val[row_i] = inout_val[row_i] - x * val_u;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils_csc::{MatCsc, MatCscOwned, proptest::any_mat_square};
    use proptest::prelude::*;

    fn decompose_owned<T>(mat: MatCsc<'_, T>) -> MatCscOwned<T>
    where
        T: Float + std::fmt::Debug,
    {
        let mut out = MatCscOwned {
            m: mat.m,
            n: mat.n,
            col_start: vec![usize::MAX; mat.n + 1],
            row_i: Vec::new(),
            val: Vec::new(),
        };
        let mut out_perm = vec![0; mat.n];

        decompose(
            mat.col_start,
            mat.row_i,
            mat.val,
            &mut out.col_start,
            &mut out.row_i,
            &mut out.val,
            &mut out_perm,
        );

        tracing::trace!(?out, "Before sorting `row_i`");

        // `decompose` output must be sorted in a specific way
        for (i, col_start) in out.col_start.windows(2).enumerate() {
            let row_i_list = &out.row_i[range(col_start)];
            assert!(
                row_i_list.iter().map(|row_i| i.cmp(row_i)).is_sorted(),
                "output column {i} is not partitioned as expected.\n\
                row_i_list = {row_i_list:?}"
            );
        }

        // `out2`: `out` rearranged as per the standard CSC storage format
        let mut out_csc = out.clone();
        crate::utils_csc::sort_row_i(&out_csc.col_start, &mut out_csc.row_i, &mut out_csc.val);

        let error = evaluate_mse(
            mat.col_start,
            mat.row_i,
            mat.val,
            &out_csc.col_start,
            &out_csc.row_i,
            &out_csc.val,
            &out_perm,
        );

        tracing::debug!(?error, ?out_csc, ?out_perm, "LU decomposition is complete");
        out_csc.as_ref().validate().unwrap();

        if mat.n == 0 {
            assert!(error.is_nan(), "MSE is not NaN: {error:?}");
        } else {
            assert!(error.to_f64().unwrap() < 1.0e-3, "MSE too large: {error:?}");
        }

        // Is the matrix (close to) singular?
        let is_singular = !diag_indices(&out.col_start, &out.row_i)
            .all(|i| i.is_some_and(|i| out.val[i].abs().to_f64().unwrap() > 1.0e-3));
        if is_singular {
            tracing::debug!(
                "Skipping the `solve` test because the diagnonal components of \
                U are close to zero"
            );
        } else {
            // `a`
            let vec_a: Vec<_> = (0..mat.m).map(|i| T::from(i).unwrap()).collect();
            tracing::trace!(?vec_a, "a (test vector)");

            // `M * a`
            let mut vec_ma: Vec<_> = (0..mat.m).map(|_| T::zero()).collect();
            crate::sgemv_csc::gemv_accumulate(
                mat.col_start,
                mat.row_i,
                mat.val,
                &vec_a,
                &mut vec_ma,
            );
            tracing::trace!(?vec_ma, "M * a");

            // `P * M * a`
            let mut vec_pma: Vec<_> = (0..mat.m).map(|_| T::zero()).collect();
            for (&x, &i) in izip!(&vec_ma, &out_perm) {
                vec_pma[i] = x;
            }
            tracing::trace!(?vec_pma, "P * M * a");

            // Solve for `a`
            let mut vec_a_got = vec_pma.clone();
            solve(&out.col_start, &out.row_i, &out.val, &mut vec_a_got);

            assert!(
                izip!(&vec_a, &vec_a_got)
                    .all(|(&a, &a_got)| (a - a_got).abs().to_f64().unwrap() < 1.0e-3),
                "the result of `solve` does not match the original vector.\
                 \n        a = {vec_a:?}\
                 \n      PMa = {vec_pma:?}\
                 \n solved a = {vec_a_got:?}"
            )
        }

        out
    }

    fn diag_indices(
        lu_col_start: &[usize],
        lu_row_i: &[usize],
    ) -> impl Iterator<Item = Option<usize>> {
        lu_col_start
            .windows(2)
            .enumerate()
            .map(|(col_i, col_range)| {
                lu_row_i[range(col_range)]
                    .binary_search_by(|&row_i| col_i.cmp(&row_i))
                    .ok()
                    .map(|i| i + col_range[0])
            })
    }

    proptest! {
        #[test]
        fn pt_decompose_identity(n in 0..10_usize) {
            crate::tests::init();
            let mat: MatCscOwned<f32> = MatCscOwned::identity(n);
            tracing::debug!(?mat);

            let out = decompose_owned(mat.as_ref());
            tracing::debug!(?out);

            prop_assert_eq!(out, mat);
        }

        #[test]
        fn pt_any_frac(mat in any_mat_square::<f64>(0..10, 0..100, -1.0..1.0)) {
            crate::tests::init();

            tracing::debug!(?mat);
            decompose_owned(mat.as_ref());
        }

        #[test]
        fn pt_any_int(
            mat in any_mat_square::<f64>(
                0..10, 0..100,
                (-2..2).prop_map(|i: i32| i as f64),
            ),
        ) {
            crate::tests::init();

            tracing::debug!(?mat);
            decompose_owned(mat.as_ref());
        }
    }
}
