//! Utilities for the CSC storage format
use anyhow::{Context, Result};
use fn_formats::DebugFmt;
use itertools::{EitherOrBoth, izip, merge_join_by};
use num_traits::{One, Zero};
use std::{
    cmp::{max, min},
    fmt,
};

/// An owned matrix in the CSC storage format.
#[derive(Clone)]
pub struct MatCscOwned<T> {
    /// The number of rows.
    pub m: usize,
    /// The number of columns.
    pub n: usize,
    pub col_start: Vec<usize>,
    pub row_i: Vec<usize>,
    pub val: Vec<T>,
}

/// A borrowed matrix in the CSC storage format.
#[derive(Clone)]
pub struct MatCsc<'a, T> {
    /// The number of rows.
    pub m: usize,
    /// The number of columns.
    pub n: usize,
    pub col_start: &'a [usize],
    pub row_i: &'a [usize],
    pub val: &'a [T],
}

impl<T> MatCscOwned<T> {
    pub fn as_ref(&self) -> MatCsc<'_, T> {
        MatCsc {
            m: self.m,
            n: self.n,
            col_start: &self.col_start,
            row_i: &self.row_i,
            val: &self.val,
        }
    }

    pub fn identity(n: usize) -> Self
    where
        T: One,
    {
        Self {
            m: n,
            n,
            col_start: (0..n + 1).collect(),
            row_i: (0..n).collect(),
            val: (0..n).map(|_| T::one()).collect(),
        }
    }

    /// Convert a matrix from the array-of-structs coordinate format to the CSC
    /// format.
    pub fn from_coords_aos(m: usize, n: usize, mut elements: Vec<(usize, usize, T)>) -> Self {
        elements.sort_unstable_by_key(|&(row, col, _)| (col, row));

        let col_start = {
            let mut i = 0;
            let mut element_cols = elements.iter().map(|&(_, col, _)| col).peekable();
            (0..n + 1)
                .map(|col| {
                    while element_cols
                        .peek()
                        .is_some_and(|&element_col| element_col < col)
                    {
                        element_cols.next();
                        i += 1;
                    }

                    i
                })
                .collect()
        };

        let row_i = elements.iter().map(|&(row, _, _)| row).collect();

        let val = elements.into_iter().map(|(_, _, val)| val).collect();

        Self {
            m,
            n,
            col_start,
            row_i,
            val,
        }
    }
}

impl<'a, T> MatCsc<'a, T> {
    /// Validate the structure of a CSC matrix.
    pub fn validate(&self) -> Result<()> {
        let Self {
            m: in_m,
            n: in_n,
            col_start: in_col_start,
            row_i: in_row_i,
            val: in_val,
        } = *self;

        let in_col_start_len_expected = in_n.checked_add(1).context("`in_n` is too large")?;
        anyhow::ensure!(
            in_col_start.len() == in_col_start_len_expected,
            "`col_start` length mismatch: {} (got) != {} (expected)",
            in_col_start.len(),
            in_col_start_len_expected
        );

        anyhow::ensure!(in_col_start.is_sorted(), "`col_start` is not monotonic");

        let max_val_i = *in_col_start.last().unwrap();
        let num_vals = min(in_row_i.len(), in_val.len());
        anyhow::ensure!(
            *in_col_start.last().unwrap() <= num_vals,
            "`col_start` contains out-of-bound indices: {max_val_i} > {num_vals}",
        );

        for (i, col_range) in in_col_start.windows(2).enumerate() {
            anyhow::ensure!(
                col_range[1] <= in_row_i.len(),
                "`col_start` contains out-of-bounds indices: \
                col_start[{}] = {} > {}",
                i + 1,
                col_range[1],
                in_row_i.len(),
            );

            let col_row_i = &in_row_i[range(col_range)];

            anyhow::ensure!(
                col_row_i.is_sorted(),
                "`row_i[{:?}]` (column {i}) is not monotonic",
                range(col_range),
            );

            anyhow::ensure!(
                col_row_i.windows(2).all(|w| w[0] != w[1]),
                "`row_i[{:?}]` (column {i}) contains duplicate elements",
                range(col_range),
            );

            if let Some(&row_i) = col_row_i.last() {
                anyhow::ensure!(
                    row_i < in_m,
                    "`row_i[{:?}]` (column {i}) contains out-of-bounds \
                    row indices: row_i[{}] = {} ≥ {}",
                    range(col_range),
                    col_range[1] - 1,
                    row_i,
                    in_m,
                );
            }
        }

        Ok(())
    }

    fn merge_join_and_try_fold<RhsT, B, E>(
        &self,
        rhs: &MatCsc<'a, RhsT>,
        init: B,
        mut f: impl FnMut(B, usize, usize, EitherOrBoth<&'a T, &'a RhsT>) -> Result<B, E>,
    ) -> Result<B, E> {
        let max_n = max(self.n, rhs.n);
        (0..max_n).try_fold(init, |st, col_i| {
            let lhs_col_range = (col_i < self.n)
                .then(|| range(&self.col_start[col_i..]))
                .unwrap_or(0..0);
            let rhs_col_range = (col_i < rhs.n)
                .then(|| range(&rhs.col_start[col_i..]))
                .unwrap_or(0..0);

            merge_join_by(lhs_col_range, rhs_col_range, |&lhs_i, &rhs_i| {
                Ord::cmp(&self.row_i[lhs_i], &rhs.row_i[rhs_i])
            })
            .try_fold(st, |st, merged| match merged {
                EitherOrBoth::Both(lhs_i, rhs_i) => f(
                    st,
                    self.row_i[lhs_i],
                    col_i,
                    EitherOrBoth::Both(&self.val[lhs_i], &rhs.val[rhs_i]),
                ),
                EitherOrBoth::Left(lhs_i) => f(
                    st,
                    self.row_i[lhs_i],
                    col_i,
                    EitherOrBoth::Left(&self.val[lhs_i]),
                ),
                EitherOrBoth::Right(rhs_i) => f(
                    st,
                    rhs.row_i[rhs_i],
                    col_i,
                    EitherOrBoth::Right(&rhs.val[rhs_i]),
                ),
            })
        })
    }
}

impl<T: fmt::Debug> fmt::Debug for MatCscOwned<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl<T: fmt::Debug> fmt::Debug for MatCsc<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Err(e) = self.validate() {
            return f
                .debug_struct("MatCsc <invalid>")
                .field("m", &self.m)
                .field("n", &self.n)
                .field("col_start", &self.col_start)
                .field("row_i", &self.row_i)
                .field("val", &self.val)
                .field("<error>", &e)
                .finish();
        }
        f.debug_map()
            .entry(&"dim", &(self.m, self.n))
            .entries(izip!(0usize.., self.col_start.windows(2).map(range)).map(
                |(col_i, col_range)| {
                    (
                        DebugFmt(move |f| write!(f, "[_, {col_i}]")),
                        DebugFmt(move |f| {
                            f.debug_map()
                                .entries(izip!(
                                    &self.row_i[col_range.clone()],
                                    &self.val[col_range.clone()]
                                ))
                                .finish()
                        }),
                    )
                },
            ))
            .finish()
    }
}

impl<T> PartialEq for MatCsc<'_, T>
where
    T: Zero + PartialEq,
{
    fn eq(&self, rhs: &Self) -> bool {
        if (self.m, self.n) != (rhs.m, rhs.n) {
            return false;
        }

        let zero = T::zero();
        self.merge_join_and_try_fold(rhs, (), |(), _row_i, _column_i, merged| {
            let (lhs, rhs) = merged.or(&zero, &zero);
            if lhs == rhs { Ok(()) } else { Err(()) }
        })
        .is_ok()
    }
}
impl<T> PartialEq for MatCscOwned<T>
where
    T: Zero + PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.as_ref().eq(&other.as_ref())
    }
}

/// Sort the elements of `row_i` of a matrix in the CSC storage format.
pub fn sort_row_i<T: Copy>(mat_col_start: &[usize], mat_row_i: &mut [usize], mat_val: &mut [T]) {
    let Some(max_rows_per_col) = mat_col_start
        .windows(2)
        .map(|col_range| col_range[1] - col_range[0])
        .max()
    else {
        // Zero columns
        return;
    };
    let mut rows = Vec::with_capacity(max_rows_per_col);

    for col_range in mat_col_start.windows(2) {
        let col_row_i = &mut mat_row_i[range(col_range)];
        let col_val = &mut mat_val[range(col_range)];

        // TODO: Optimize the case where `size_of_val(out_val) == 0`

        rows.extend(izip!(col_row_i.iter().copied(), col_val.iter().copied()));

        rows.sort_unstable_by_key(|row| row.0);

        for ((new_row_i, new_val), p_row_i, p_val) in izip!(rows.drain(..), col_row_i, col_val) {
            *p_row_i = new_row_i;
            *p_val = new_val;
        }
    }
}

#[inline]
pub(crate) fn range(indices: &[usize]) -> std::ops::Range<usize> {
    indices[0]..indices[1]
}

#[cfg(any(feature = "proptest", test))]
pub mod proptest {
    use super::*;
    use ::proptest::prelude::*;

    pub fn any_mat_square<T: fmt::Debug>(
        size: impl Into<prop::sample::SizeRange>,
        num_nonzeros: impl Into<prop::sample::SizeRange>,
        val: impl Strategy<Value = T>,
    ) -> impl Strategy<Value = MatCscOwned<T>> {
        let (min_nnz, max_nnz) = num_nonzeros.into().start_end_incl();
        let val = std::rc::Rc::new(val);
        prop::collection::vec(Just(()), size).prop_flat_map(move |vec| {
            let size = vec.len();
            let num = size.pow(2);
            prop::collection::btree_map(
                (0..size, 0..size),
                val.clone(),
                min_nnz.min(num)..=max_nnz.min(num),
            )
            .prop_map(move |elements| {
                MatCscOwned::from_coords_aos(
                    size,
                    size,
                    elements
                        .into_iter()
                        .map(|((row, col), val)| (row, col, val))
                        .collect(),
                )
            })
        })
    }
}

#[cfg(test)]
mod tests {
    use super::proptest::any_mat_square;
    use super::*;
    use ::proptest::prelude::*;

    proptest! {
        #[test]
        fn pt_identity(n in 0..10_usize) {
            crate::tests::init();
            let mat: MatCscOwned<f32> = MatCscOwned::identity(n);
            tracing::debug!(?mat);
            mat.as_ref().validate().expect("validation failed");
            prop_assert_eq!(mat.m, n);
            prop_assert_eq!(mat.n, n);
            prop_assert_eq!(mat.val, vec![1.0_f32; n]);
        }
    }

    proptest! {
        #[test]
        fn pt_from_coords_aos_identity(n in 0..10_usize) {
            crate::tests::init();
            let mat = MatCscOwned::from_coords_aos(
                n,
                n,
                (0..n).map(|i| (i, i, 1.0_f32)).collect(),
            );
            tracing::debug!(?mat);
            mat.as_ref().validate().expect("validation failed");

            prop_assert_eq!(mat, MatCscOwned::identity(n));
        }
    }

    proptest! {
        #[test]
        fn pt_any_mat_square(mat in any_mat_square(0..10, 0..100, -1.0..1.0)) {
            let MatCscOwned { m, n, col_start, row_i, .. } = &mat;
            tracing::debug!(m, n, ?col_start, ?row_i);
            mat.as_ref().validate().expect("validation failed");
        }
    }
}
