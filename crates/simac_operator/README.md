# Undo operation support in Bevy ECS

## Stashing

**Stashes** are a mechanism to take a backup of components before despawning an entity without the caller having knowledge of any specific components.

An operator can request to stash and despawn an entity by calling [`OperatorForwardCx::despawn_and_stash`].
This method iterates over all components of the entity to find the matching registered stashers ([`StashableComponent`]) and utilizes them to move the component data to a separate storage.
When the operator is rolled back, it calls [`OperatorBackwardCx::respawn_from_stash`] to restore the entity along with all components that have been stashed.

### Stashers

Stashers must be registered manually by implementing [`StashableComponent`] and calling [`OperatorStack::init_component`] for components to be stashed.

As a rule of thumb, you only need to register components serving as a source of truth of entity-specific data.
You do not need to register components that are automatically derived from other components by systems.


### Outbound Entity References

When an operator spawning an entity is rolled back and applied again, the result is mostly identical to that of the first application except that this "reincarnation" of entity is now associated with a different `Entity`.
This is because in most cases it is not possible to reuse the same `Entity`.
To establish stable identities of entities, this library provides [`VirtEntity`] type.

Since any contained `Entity` values may become invalid anytime during a stashed period, it is important that stasher implementations use the provided [`StashCx`] to convert `Entity` values to `VirtEntity` before stashing (and back to `Entity` when unstashing).

### Inbound Entity References

This library does not make any provisions to detect or handle outstanding references to an entity being despawned.
This unfortunately means operator implementations need to be careful not to invalidate such references by despawning entities prematurely.

Generally, you need to despawn-and-stash a group of entities in the topological order of the references held by stashable components.
If the entities form circular references, you must either break all cycles first or despawn-and-stash them all at once by passing the entity list to `OperatorForwardCx::despawn_and_stash`.


[`OperatorBackwardCx::respawn_from_stash`]: crate::operator::OperatorBackwardCx::respawn_from_stash
[`OperatorForwardCx::despawn_and_stash`]: crate::operator::OperatorForwardCx::despawn_and_stash
[`VirtEntity`]: crate::stack::VirtEntity
[`StashableComponent`]: crate::stash::StashableComponent
[`StashCx`]: crate::stash::StashCx
[`OperatorStack::init_component`]: crate::stack::OperatorStack::init_component
