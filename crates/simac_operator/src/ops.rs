//! Basic operators
//!
//! The operators provided by this module does not convert between [`Entity`]
//! and [`VirtEntity`]. Do not use them to manipulate components containing
//! [`Entity`].
use bevy_ecs::prelude::*;
use itertools::izip;
use std::{fmt, marker::PhantomData};

use crate::{VirtEntity, operator};

/// Create an [`operator::Operator`] to do nothing.
pub fn noop() -> Box<dyn operator::Operator> {
    #[derive(Debug)]
    struct Noop;

    impl operator::Operator for Noop {
        fn execute(
            self: Box<Self>,
            _cx: &mut dyn operator::OperatorExecuteCx,
            prev_operator: Option<&mut dyn operator::OperatorUndo>,
        ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
            if prev_operator.is_some_and(|x| x.as_any().is::<Self>()) {
                Err(operator::OperatorExecuteError::MergedIntoPrevOperator)
            } else {
                Ok(self)
            }
        }
    }

    impl operator::OperatorRedo for Noop {
        fn redo(
            self: Box<Self>,
            _cx: &mut dyn operator::OperatorForwardCx,
        ) -> Box<dyn operator::OperatorUndo> {
            self
        }
    }

    impl operator::OperatorUndo for Noop {
        fn undo(
            self: Box<Self>,
            _cx: &mut dyn operator::OperatorBackwardCx,
        ) -> Box<dyn operator::OperatorRedo> {
            self
        }
    }

    Box::new(Noop)
}

/// Create an [`operator::Operator`] to spawn entities containing bundles
/// created by a given iterator-producing closure.
pub fn spawn_entities_with<MB, B>(make_bundles: MB) -> Box<dyn operator::Operator>
where
    MB: FnMut() -> B + Send + Sync + 'static,
    B: IntoIterator<Item: Bundle>,
{
    struct SpawnEntityWith<MB> {
        make_bundles: MB,
    }

    struct SpawnEntityWithUr<MB> {
        virt_entities: Vec<VirtEntity>,
        make_bundles: MB,
    }

    impl<MB> fmt::Debug for SpawnEntityWith<MB> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            f.debug_tuple("SpawnEntityWith").finish_non_exhaustive()
        }
    }

    impl<MB> fmt::Debug for SpawnEntityWithUr<MB> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            f.debug_tuple("SpawnEntityWithUr")
                .field(&self.virt_entities)
                .finish()
        }
    }

    impl<MB, B> operator::Operator for SpawnEntityWith<MB>
    where
        MB: FnMut() -> B + Send + Sync + 'static,
        B: IntoIterator<Item: Bundle>,
    {
        fn execute(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorExecuteCx,
            _: Option<&mut dyn operator::OperatorUndo>,
        ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
            let mut make_bundles = self.make_bundles;
            let virt_entities: Vec<_> = (make_bundles)()
                .into_iter()
                .map(|bundle| {
                    let virt_entity = cx.new_virt_entity();
                    let entity = cx.world_mut().spawn(bundle).id();
                    cx.spawned(entity, virt_entity);
                    virt_entity
                })
                .collect();
            Ok(Box::new(SpawnEntityWithUr {
                virt_entities,
                make_bundles,
            }))
        }
    }

    impl<MB, B> operator::OperatorRedo for SpawnEntityWithUr<MB>
    where
        MB: FnMut() -> B + Send + Sync + 'static,
        B: IntoIterator<Item: Bundle>,
    {
        fn redo(
            mut self: Box<Self>,
            cx: &mut dyn operator::OperatorForwardCx,
        ) -> Box<dyn operator::OperatorUndo> {
            for (&virt_entity, bundle) in izip!(&self.virt_entities, (self.make_bundles)()) {
                let entity = cx.world_mut().spawn(bundle).id();
                cx.spawned(entity, virt_entity);
            }
            self
        }
    }

    impl<MB, B> operator::OperatorUndo for SpawnEntityWithUr<MB>
    where
        MB: FnMut() -> B + Send + Sync + 'static,
        B: IntoIterator<Item: Bundle>,
    {
        fn undo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorBackwardCx,
        ) -> Box<dyn operator::OperatorRedo> {
            for &virt_entity in &self.virt_entities {
                cx.despawn(virt_entity);
            }
            self
        }
    }

    Box::new(SpawnEntityWith { make_bundles })
}

/// Create an [`operator::Operator`] to despawn the specified entities.
///
/// See [`crate::operator::OperatorForwardCx::despawn_and_stash`] for
/// remarks about despawning entities with outstanding references.
pub fn despawn_entities(entities: Vec<Entity>) -> Box<dyn operator::Operator> {
    #[derive(Debug)]
    struct DespawnEntities {
        entities: Vec<Entity>,
    }

    #[derive(Debug)]
    struct DespawnEntitiesUr {
        virt_entities: Vec<VirtEntity>,
    }

    impl operator::Operator for DespawnEntities {
        fn execute(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorExecuteCx,
            _: Option<&mut dyn operator::OperatorUndo>,
        ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
            let virt_entities = self
                .entities
                .into_iter()
                .map(|x| cx.entity_to_virt(x))
                .collect();
            let op_redo = Box::new(DespawnEntitiesUr { virt_entities });
            Ok(operator::OperatorRedo::redo(
                op_redo,
                cx.as_forward_cx_mut(),
            ))
        }
    }

    impl operator::OperatorRedo for DespawnEntitiesUr {
        fn redo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorForwardCx,
        ) -> Box<dyn operator::OperatorUndo> {
            cx.despawn_and_stash(&self.virt_entities);
            self
        }
    }

    impl operator::OperatorUndo for DespawnEntitiesUr {
        fn undo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorBackwardCx,
        ) -> Box<dyn operator::OperatorRedo> {
            cx.respawn_from_stash(&self.virt_entities);
            self
        }
    }

    Box::new(DespawnEntities { entities })
}

/// Create an [`operator::Operator`] to insert, remove, or replace the specified
/// component of the specified entity.
///
/// Operators created by this function with the same `C` and `merge_key` can
/// be merged.
pub fn replace_component<C, MergeKey>(
    entity: Entity,
    merge_key: MergeKey,
    new: Option<C>,
) -> Box<dyn operator::Operator>
where
    C: Component + fmt::Debug,
    MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
{
    #[derive(Debug)]
    struct ReplaceComponent<C, MergeKey> {
        entity: Entity,
        merge_key: MergeKey,
        new: Option<C>,
    }

    #[derive(Debug)]
    struct ReplaceComponentUr<C, MergeKey> {
        virt_entity: VirtEntity,
        merge_key: MergeKey,
        new_or_old: Option<C>,
    }

    impl<C, MergeKey> operator::Operator for ReplaceComponent<C, MergeKey>
    where
        C: Component + fmt::Debug,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        fn execute(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorExecuteCx,
            prev_operator: Option<&mut dyn operator::OperatorUndo>,
        ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
            let Self {
                entity,
                merge_key,
                new,
            } = *self;
            let virt_entity = cx.entity_to_virt(entity);

            // Try to merge into `prev_operator`
            if prev_operator
                .and_then(|op| {
                    op.as_any_mut()
                        .downcast_mut::<ReplaceComponentUr<C, MergeKey>>()
                })
                .is_some_and(|op| op.virt_entity == virt_entity && op.merge_key == merge_key)
            {
                let mut entity_mut = cx.world_mut().entity_mut(self.entity);

                if let Some(new) = new {
                    entity_mut.insert(new);
                } else {
                    entity_mut.remove::<C>();
                }

                return Err(operator::OperatorExecuteError::MergedIntoPrevOperator);
            }

            let op_redo = Box::new(ReplaceComponentUr {
                virt_entity,
                merge_key,
                new_or_old: new,
            });
            Ok(op_redo.redo_or_undo(cx.as_forward_cx_mut()))
        }
    }

    impl<C, MergeKey> operator::OperatorRedo for ReplaceComponentUr<C, MergeKey>
    where
        C: Component + fmt::Debug,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        fn redo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorForwardCx,
        ) -> Box<dyn operator::OperatorUndo> {
            self.redo_or_undo(cx)
        }
    }

    impl<C, MergeKey> operator::OperatorUndo for ReplaceComponentUr<C, MergeKey>
    where
        C: Component + fmt::Debug,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        fn undo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorBackwardCx,
        ) -> Box<dyn operator::OperatorRedo> {
            self.redo_or_undo(cx)
        }
    }

    impl<C, MergeKey> ReplaceComponentUr<C, MergeKey>
    where
        C: Component + fmt::Debug,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        // FIXME: Take `dyn OperatorCx` when dyn upcasting is stabilized
        fn redo_or_undo(
            mut self: Box<Self>,
            cx: &mut (impl operator::OperatorCx + ?Sized),
        ) -> Box<Self> {
            let entity = cx.entity_to_phys(self.virt_entity);
            let mut entity_mut = cx.world_mut().entity_mut(entity);
            match (entity_mut.get_mut(), self.new_or_old.take()) {
                (None, None) => {}
                (None, Some(c)) => {
                    entity_mut.insert(c);
                }
                (Some(_), None) => self.new_or_old = entity_mut.take(),
                (Some(mut c1), Some(c2)) => self.new_or_old = Some(std::mem::replace(&mut c1, c2)),
            }
            self
        }
    }

    Box::new(ReplaceComponent {
        entity,
        merge_key,
        new,
    })
}

/// Create an [`operator::Operator`] to set a field of a component of the
/// specified entity.
///
/// Operators created by this function with the same `C` and `merge_key` can
/// be merged. If `project` conditionally changes its behavior, be sure to
/// include the condition in `merge_key`.
pub fn set_component_field<C, Project, T, MergeKey>(
    entity: Entity,
    merge_key: MergeKey,
    project: Project,
    value: T,
) -> Box<dyn operator::Operator>
where
    C: Component + fmt::Debug,
    Project: FnMut(&mut C) -> &mut T + Send + Sync + 'static,
    T: fmt::Debug + Send + Sync + 'static,
    MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
{
    struct SetComponentField<C, Project, T, MergeKey> {
        entity: Entity,
        merge_key: MergeKey,
        project: Project,
        value: T,
        _component: PhantomData<C>,
    }

    struct SetComponentFieldUr<C, Project, T, MergeKey> {
        virt_entity: VirtEntity,
        merge_key: MergeKey,
        project: Project,
        value: T,
        _component: PhantomData<C>,
    }

    impl<C: fmt::Debug, Project, T: fmt::Debug, MergeKey: fmt::Debug> fmt::Debug
        for SetComponentField<C, Project, T, MergeKey>
    {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            f.debug_struct("SetComponentField")
                .field("entity", &self.entity)
                .field("merge_key", &self.merge_key)
                .field("value", &self.value)
                .field("C", &std::any::type_name::<C>())
                .finish()
        }
    }

    impl<C: fmt::Debug, Project, T: fmt::Debug, MergeKey: fmt::Debug> fmt::Debug
        for SetComponentFieldUr<C, Project, T, MergeKey>
    {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            f.debug_struct("SetComponentFieldUr")
                .field("virt_entity", &self.virt_entity)
                .field("merge_key", &self.merge_key)
                .field("value", &self.value)
                .field("C", &std::any::type_name::<C>())
                .finish()
        }
    }

    impl<C, Project, T, MergeKey> operator::Operator for SetComponentField<C, Project, T, MergeKey>
    where
        C: Component + fmt::Debug,
        Project: FnMut(&mut C) -> &mut T + Send + Sync + 'static,
        T: fmt::Debug + Send + Sync + 'static,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        fn execute(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorExecuteCx,
            prev_operator: Option<&mut dyn operator::OperatorUndo>,
        ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
            let Self {
                entity,
                merge_key,
                mut project,
                value,
                _component,
            } = *self;
            let virt_entity = cx.entity_to_virt(entity);

            // Try to merge into `prev_operator`
            if prev_operator
                .and_then(|op| {
                    op.as_any_mut()
                        .downcast_mut::<SetComponentFieldUr<C, Project, T, MergeKey>>()
                })
                .is_some_and(|op| op.virt_entity == virt_entity && op.merge_key == merge_key)
            {
                let mut comp = cx.world_mut().get_mut::<C>(entity).unwrap_or_else(|| {
                    panic!(
                        "entity {entity} does not have component of type {}",
                        std::any::type_name::<C>()
                    )
                });

                *project(&mut comp) = value;

                return Err(operator::OperatorExecuteError::MergedIntoPrevOperator);
            }

            let op_redo = Box::new(SetComponentFieldUr {
                virt_entity,
                merge_key,
                project,
                value,
                _component,
            });
            Ok(op_redo.redo_or_undo(cx.as_forward_cx_mut()))
        }
    }

    impl<C, Project, T, MergeKey> operator::OperatorRedo
        for SetComponentFieldUr<C, Project, T, MergeKey>
    where
        C: Component + fmt::Debug,
        Project: FnMut(&mut C) -> &mut T + Send + Sync + 'static,
        T: fmt::Debug + Send + Sync + 'static,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        fn redo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorForwardCx,
        ) -> Box<dyn operator::OperatorUndo> {
            self.redo_or_undo(cx)
        }
    }

    impl<C, Project, T, MergeKey> operator::OperatorUndo
        for SetComponentFieldUr<C, Project, T, MergeKey>
    where
        C: Component + fmt::Debug,
        Project: FnMut(&mut C) -> &mut T + Send + Sync + 'static,
        T: fmt::Debug + Send + Sync + 'static,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        fn undo(
            self: Box<Self>,
            cx: &mut dyn operator::OperatorBackwardCx,
        ) -> Box<dyn operator::OperatorRedo> {
            self.redo_or_undo(cx)
        }
    }

    impl<C, Project, T, MergeKey> SetComponentFieldUr<C, Project, T, MergeKey>
    where
        C: Component + fmt::Debug,
        Project: FnMut(&mut C) -> &mut T + Send + Sync + 'static,
        T: fmt::Debug + Send + Sync + 'static,
        MergeKey: PartialEq + fmt::Debug + Send + Sync + 'static,
    {
        // FIXME: Take `dyn OperatorCx` when dyn upcasting is stabilized
        fn redo_or_undo(
            mut self: Box<Self>,
            cx: &mut (impl operator::OperatorCx + ?Sized),
        ) -> Box<Self> {
            let entity = cx.entity_to_phys(self.virt_entity);
            let mut comp = cx.world_mut().get_mut::<C>(entity).unwrap_or_else(|| {
                panic!(
                    "entity {entity} does not have component of type {}",
                    std::any::type_name::<C>()
                )
            });
            let field = (self.project)(&mut comp);
            std::mem::swap(field, &mut self.value);
            self
        }
    }

    Box::new(SetComponentField {
        entity,
        merge_key,
        project,
        value,
        _component: PhantomData::<C>,
    })
}
