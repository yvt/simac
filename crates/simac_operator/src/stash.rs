//! Saving (stashing) and restoring components
use bevy_ecs::{
    component::{ComponentId, Components},
    prelude::*,
};
use slotmap::{Key as _, SlotMap};
use std::{fmt, marker::PhantomData};

use crate::stack::VirtEntity;

/// The trait for [`Component`] types that can be stashed.
///
/// This trait only supports component types corresponding to Rust types
/// (i.e., having their identities established by Rust types implementing
/// [`Component`]).
pub trait StashableComponent: Send + Sync + 'static {
    type Stashed: fmt::Debug + Send + Sync;

    /// Save a component state.
    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed;

    /// Restore a component state.
    fn unstash(stashed: Self::Stashed, cx: &mut dyn StashCx) -> Self;
}

pub trait StashCx {
    /// Get te current manifestation of the provided [`VirtEntity`].
    #[track_caller]
    fn entity_to_phys(&self, entity: VirtEntity) -> Entity;

    /// Get the [`VirtEntity`] representing the provided [`Entity`].
    #[track_caller]
    fn entity_to_virt(&self, entity: Entity) -> VirtEntity;
}

/// Use with `#[macro_rules_attribute::derive]`.
/// Generates a [`StashableComponent`] implementation that simply clones `$Ty`.
///
/// Do not use if `$Ty` contains [`Entity`]s.
///
/// # Example
///
/// ```rust
/// use bevy_ecs::{component::Components, prelude::*};
/// use macro_rules_attribute::derive;
/// use simac_operator::{StashableComponent, stack::OperatorStack};
///
/// #[derive(Debug, Clone, Component, StashableComponent!)]
/// struct Foo<T> {
///     x: T,
/// }
///
/// fn register_foo(mut operator_stack: ResMut<OperatorStack>, components: &Components) {
///     operator_stack.init_component::<Foo<String>>(components);
/// }
/// ```
#[macro_export]
macro_rules! StashableComponent {
    (
        $( #[ $meta:meta] )*
        $_vis:vis
        struct $Ty:ident $( $rest:tt )*
    ) => {
        $crate::generics::parse! {
            $crate::StashableComponent {
                @ $Ty
            }
            $( $rest )*
        }
    };

    (
        // Internal rule
        @ $Ty:ident
        // `generic_parse!` return tokens
        [$($g:tt)*] [$($r:tt)*] [ $(where)? $($w:tt)*] $( $_rest:tt )*
    ) => {
        impl $($g)* $crate::stash::StashableComponent for $Ty $($r)* $($w)*
        where
            Self: Clone + Send + Sync + ::std::fmt::Debug + 'static,
            $($w)*
        {
            type Stashed = Self;

            fn stash(&mut self, _: &mut dyn $crate::stash::StashCx) -> Self::Stashed {
                self.clone()
            }

            fn unstash(stashed: Self::Stashed, _: &mut dyn $crate::stash::StashCx) -> Self {
                stashed
            }
        }
    };
}

/// Stashed objects of `Comp: `[`StashableComponent`].
struct ComponentStash<Stashed, Comp> {
    stash: SlotMap<ComponentStashKey, Stashed>,
    _phantom: PhantomData<fn(Comp) -> Comp>,
}

slotmap::new_key_type! { struct ComponentStashKey; }

/// A stash for a component.
trait ComponentStashAny: fmt::Debug + Send + Sync + 'static {
    fn stash(&mut self, entity: &mut EntityWorldMut<'_>, cx: &mut dyn StashCx)
    -> ComponentStashKey;
    fn unstash(
        &mut self,
        stash_key: ComponentStashKey,
        entity: &mut EntityWorldMut<'_>,
        cx: &mut dyn StashCx,
    );
    fn remove(&mut self, stash_key: ComponentStashKey);
}

impl<Stashed, Comp> fmt::Debug for ComponentStash<Stashed, Comp>
where
    Stashed: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ComponentStash")
            .field("component", &std::any::type_name::<Comp>())
            .field("stash", &self.stash)
            .finish()
    }
}

impl<Comp> ComponentStashAny for ComponentStash<Comp::Stashed, Comp>
where
    Comp: Component + StashableComponent,
{
    fn stash(
        &mut self,
        entity: &mut EntityWorldMut<'_>,
        cx: &mut dyn StashCx,
    ) -> ComponentStashKey {
        let mut comp = entity.get_mut::<Comp>().expect("component missing");
        let stashed = comp.stash(cx);
        self.stash.insert(stashed)
    }

    fn unstash(
        &mut self,
        stash_key: ComponentStashKey,
        entity: &mut EntityWorldMut<'_>,
        cx: &mut dyn StashCx,
    ) {
        let stashed = self.stash.remove(stash_key).expect("invalid stash key");
        let comp = Comp::unstash(stashed, cx);
        entity.insert(comp);
    }

    fn remove(&mut self, stash_key: ComponentStashKey) {
        self.stash.remove(stash_key);
    }
}

#[derive(Debug, Default, Resource)]
pub(crate) struct Stashes {
    /// Indexed by [`ComponentId::index`]
    stashes: Vec<Option<Box<dyn ComponentStashAny>>>,
    entity_stashes: SlotMap<EntityStashKey, EntityStash>,
}

slotmap::new_key_type! { pub(crate) struct EntityStashKey; }

/// A reference to a set of stashed components.
#[derive(Debug)]
struct EntityStash {
    component_stash_keys: Vec<(ComponentId, ComponentStashKey)>,
}

impl Stashes {
    /// Register a type implementing [`StashableComponent`].
    pub(crate) fn init_component<T: Component + StashableComponent>(
        &mut self,
        components: &Components,
    ) {
        let component_index = components
            .component_id::<T>()
            .expect("register component to `Commands` first")
            .index();
        self.stashes
            .resize_with((component_index + 1).max(self.stashes.len()), || None);
        self.stashes[component_index].get_or_insert_with(|| {
            Box::new(ComponentStash {
                stash: <_>::default(),
                _phantom: PhantomData::<fn(T) -> T>,
            }) as _
        });
    }

    /// Save the state of a given `entity`'s components.
    pub(crate) fn stash(
        &mut self,
        entity: &mut EntityWorldMut<'_>,
        cx: &mut dyn StashCx,
    ) -> EntityStashKey {
        // Enumerate `entity`'s components that we can stash. Store the result
        // in the preallocated storage of `Stash::stash_keys`.
        let mut component_stash_keys: Vec<(ComponentId, ComponentStashKey)> = Vec::new();

        for component_id in entity.archetype().components() {
            let component_index = component_id.index();
            if matches!(self.stashes.get(component_index), Some(Some(_))) {
                component_stash_keys.push((component_id, ComponentStashKey::null()));
            }
        }

        // Stash every supported component
        for &mut (component_id, ref mut p_component_stash_key) in &mut component_stash_keys {
            let component_stash = self.stashes[component_id.index()].as_mut().unwrap();
            *p_component_stash_key = component_stash.stash(entity, cx);
        }

        self.entity_stashes.insert(EntityStash {
            component_stash_keys,
        })
    }

    /// Restore the components of a given `entity`.
    ///
    /// The stash slot is cleared after the operation.
    pub(crate) fn unstash(
        &mut self,
        entity_stash_key: EntityStashKey,
        entity: &mut EntityWorldMut<'_>,
        cx: &mut dyn StashCx,
    ) {
        let entity_stash = self
            .entity_stashes
            .remove(entity_stash_key)
            .expect("invalid entity stash key");

        for (component_id, component_stash_key) in entity_stash.component_stash_keys {
            let component_stash = self.stashes[component_id.index()].as_mut().unwrap();
            component_stash.unstash(component_stash_key, entity, cx);
        }
    }

    /// Discard the saved state of an entity.
    pub(crate) fn remove(&mut self, entity_stash_key: EntityStashKey) {
        let entity_stash = self
            .entity_stashes
            .remove(entity_stash_key)
            .expect("invalid entity stash key");

        for (component_id, component_stash_key) in entity_stash.component_stash_keys {
            let component_stash = self.stashes[component_id.index()].as_mut().unwrap();
            component_stash.remove(component_stash_key);
        }
    }
}
