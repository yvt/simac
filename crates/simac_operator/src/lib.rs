#![doc = include_str!("../README.md")]

/// Used by [`StashableComponent!`]
#[doc(hidden)]
pub extern crate generics;

#[doc(no_inline)]
pub use stack::VirtEntity;
#[doc(no_inline)]
pub use stash::{StashCx, StashableComponent};

pub mod operator;
pub mod ops;
pub mod stack;
pub mod stash;

#[cfg(test)]
mod tests;
