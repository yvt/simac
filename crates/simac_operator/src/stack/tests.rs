use super::*;
use itertools::{enumerate, rev};
use proptest::prelude::*;
use proptest_derive::Arbitrary;
use std::mem::replace;
use try_match::match_ok;

impl OperatorStack {
    fn validate(&self) -> Result<()> {
        let VirtEntityDataMap {
            data_map,
            phys_to_virt,
        } = &self.virt_entities;

        // Validate `phys_to_virt`
        anyhow::ensure!(
            phys_to_virt.len()
                == data_map
                    .values()
                    .filter(|x| matches!(x, VirtEntityData::Spawned { .. }))
                    .count(),
            "`phys_to_virt` count mismatch"
        );

        for (&virt_entity, data) in data_map.iter() {
            let VirtEntityData::Spawned { entity } = data else { continue };
            anyhow::ensure!(phys_to_virt.get(entity) == Some(&virt_entity));
        }

        Ok(())
    }
}

type Name = u32;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Component)]
struct Comp<Link = Entity> {
    name: Name,
    link: Option<Link>,
}

#[derive(Debug, Arbitrary)]
enum Cmd {
    /// Spawn a [`Comp`].
    Spawn {
        preview: bool,
    },
    /// Despawn a [`Comp`].
    Despawn {
        names: [prop::sample::Index; 3],
        preview: bool,
    },
    /// Update [`Comp::link`].
    Update {
        name: prop::sample::Index,
        link: prop::sample::Index,
        preview: bool,
    },
    Undo,
    Redo,
}

proptest! {
    #[test]
    fn pt_cmds(
        init in prop::collection::vec(any::<Option<prop::sample::Index>>(), 0..3),
        cmds in prop::collection::vec(any::<Cmd>(), 0..50),
        max_undo_stack_len: usize,
    ) {
        crate::tests::init();
        pt_cmds_inner(init, cmds, max_undo_stack_len);
    }
}

fn pt_cmds_inner(
    init: Vec<Option<prop::sample::Index>>,
    cmds: Vec<Cmd>,
    max_undo_stack_len: usize,
) {
    // World state
    let mut world = World::new();
    world.register_component::<Comp>();

    let mut operator_stack = OperatorStack::default();
    operator_stack.init_component::<Comp>(world.components());

    // Reference world state
    // Here `Comp`s reference other entities by `Name` rather than `Entity`
    let mut ref_comps: Vec<Comp<Name>> = Vec::new();
    let mut ref_undo_stack: VecDeque<Vec<Comp<Name>>> = VecDeque::new();
    let mut ref_undo_preview: Option<Vec<Comp<Name>>> = None;
    let mut ref_redo_stack: Vec<Vec<Comp<Name>>> = Vec::new();
    let mut ref_last_update = None;

    // Add entities based on `init`
    let init_i_to_name = |i: usize| i as Name;
    let init_name_to_i = |i: Name| i as usize;
    ref_comps.extend(enumerate(&init).map(|(i, link)| {
        let link = link.map(|link| init_i_to_name(link.index(init.len())));
        Comp {
            name: init_i_to_name(i),
            link,
        }
    }));
    let init_ents: Vec<Entity> = world.spawn_batch((0..init.len()).map(|_| ())).collect();
    for (&init_ent, ref_comp) in izip!(&init_ents, &ref_comps) {
        world.entity_mut(init_ent).insert(Comp {
            name: ref_comp.name,
            link: ref_comp.link.map(|name| init_ents[init_name_to_i(name)]),
        });
    }

    // Name assignment
    let mut next_name = init.len() as Name;

    tracing::warn!("Start");

    for cmd in cmds {
        match cmd {
            Cmd::Spawn { preview } => {
                if ref_undo_preview.is_some() {
                    tracing::debug!(
                        "Preview undo stack is not empty; refusing \
                        to perform invalid operation"
                    );
                    continue;
                }

                let name = next_name;
                next_name += 1;

                tracing::info!(name, "Spawning an entity");

                // Update the reference world state
                if preview {
                    ref_undo_preview = Some(ref_comps.clone());
                } else {
                    ref_undo_stack.push_back(ref_comps.clone());
                    ref_redo_stack.clear();
                    ref_last_update = None;
                }

                ref_comps.push(Comp { name, link: None });

                // Update the world state
                let operator = Box::new(SpawnOperator {
                    comp: Comp { name, link: None },
                });
                if preview {
                    operator_stack
                        .execute_preview(&mut world, operator)
                        .unwrap();
                } else {
                    operator_stack.execute(&mut world, operator).unwrap();
                }
            }
            Cmd::Despawn { names, preview } => {
                if ref_undo_preview.is_some() {
                    tracing::debug!(
                        "Preview undo stack is not empty; refusing \
                        to perform invalid operation"
                    );
                    continue;
                }

                // Choose the entities to despawn
                if ref_comps.is_empty() {
                    tracing::debug!("Got `Despawn`, but the world contains no entities");
                    continue;
                }
                let mut i: Vec<_> = names.map(|i| i.index(ref_comps.len())).into();
                i.sort_unstable();
                i.dedup();

                // Update the reference world state
                if preview {
                    ref_undo_preview = Some(ref_comps.clone());
                } else {
                    ref_undo_stack.push_back(ref_comps.clone());
                    ref_redo_stack.clear();
                    ref_last_update = None;
                }

                let names: Vec<_> = rev(&i).map(|&i| ref_comps.swap_remove(i).name).collect();
                tracing::info!(?names, "Despawning an entity",);

                // Unlink references to `comp`
                for comp2 in &mut ref_comps {
                    if comp2.link.is_some_and(|n| names.contains(&n)) {
                        comp2.link = None;
                    }
                }

                // Update the world state
                let entities: Vec<Entity> = world
                    .query::<(Entity, &Comp)>()
                    .iter(&world)
                    .filter_map(match_ok!(, (e, Comp { name, .. }) if names.contains(name) => e))
                    .collect();

                let operator = Box::new(DespawnOperator { entities });
                if preview {
                    operator_stack
                        .execute_preview(&mut world, operator)
                        .unwrap();
                } else {
                    operator_stack.execute(&mut world, operator).unwrap();
                }
            }
            Cmd::Update {
                name,
                link,
                preview,
            } => {
                if ref_undo_preview.is_some() {
                    tracing::debug!(
                        "Preview undo stack is not empty; refusing \
                        to perform invalid operation"
                    );
                    continue;
                }

                // Choose the entity to update
                if ref_comps.is_empty() {
                    tracing::debug!("Got `Update`, but the world contains no entities");
                    continue;
                }
                let i = name.index(ref_comps.len());
                let name = ref_comps[i].name;

                // Choose the new entity to link
                let i_link = link.index(ref_comps.len());
                let link_name = ref_comps[i_link].name;

                // Update the reference world state
                if preview {
                    ref_undo_preview = Some(ref_comps.clone());
                } else if ref_last_update == Some(name) {
                    // Merge into the last update
                    tracing::debug!("Not creating a new undo stack entry");
                    assert!(ref_redo_stack.is_empty());
                } else {
                    ref_undo_stack.push_back(ref_comps.clone());
                    ref_redo_stack.clear();
                }

                let comp = &mut ref_comps[i];
                comp.link = Some(link_name);

                if !preview {
                    ref_last_update = Some(name);
                }

                tracing::info!(name, link = link_name, "Linking an entity");

                // Update the world state
                let mut qs_comp = world.query::<(Entity, &Comp)>();
                let (entity, _comp) = qs_comp
                    .iter(&world)
                    .find(|x| x.1.name == comp.name)
                    .expect("unable to find entity with a given name");
                let (link, _comp) = qs_comp
                    .iter(&world)
                    .find(|x| x.1.name == link_name)
                    .expect("unable to find entity with a given name");

                let operator = Box::new(UpdateOperator { entity, link });
                if preview {
                    operator_stack
                        .execute_preview(&mut world, operator)
                        .unwrap();
                } else {
                    operator_stack.execute(&mut world, operator).unwrap();
                }
            }
            Cmd::Undo => {
                if let Some(new_ref_comps) = ref_undo_preview.take() {
                    tracing::info!("Undo (preview)");
                    ref_comps = new_ref_comps;

                    tracing::debug!(
                        operator = ?operator_stack.preview_undo_stack_peek()
                            .expect("The previw undo stack is unexpectedly empty"),
                        "The preview operator to undo"
                    );
                    operator_stack.clear_preview(&mut world);
                    continue;
                }

                let Some(new_ref_comps) = ref_undo_stack.pop_back() else {
                    tracing::debug!("Got `Undo`, but the undo stack is empty");
                    unwrap_match!(operator_stack.undo_stack_peek(), None);
                    continue;
                };

                tracing::info!("Undo");

                ref_redo_stack.push(ref_comps);
                ref_comps = new_ref_comps;
                ref_last_update = None;

                tracing::debug!(
                    operator = ?operator_stack.undo_stack_peek()
                        .expect("The undo stack is unexpectedly empty"),
                    "The operator to undo"
                );
                operator_stack.undo(&mut world);
            }
            Cmd::Redo => {
                if ref_undo_preview.is_some() {
                    tracing::debug!(
                        "Preview undo stack is not empty; refusing \
                        to perform invalid operation"
                    );
                    continue;
                }

                let Some(new_ref_comps) = ref_redo_stack.pop() else {
                    tracing::debug!("Got `Dedo`, but the redo stack is empty");
                    unwrap_match!(operator_stack.redo_stack_peek(), None);
                    continue;
                };

                tracing::info!("Redo");

                ref_undo_stack.push_back(ref_comps);
                ref_comps = new_ref_comps;
                ref_last_update = None;

                tracing::debug!(
                    operator = ?operator_stack.redo_stack_peek()
                        .expect("The redo stack is unexpectedly empty"),
                    "The operator to redo"
                );
                operator_stack.redo(&mut world);
            }
        }

        // Trim the undo stack
        if ref_undo_stack.len() > max_undo_stack_len {
            ref_undo_stack.pop_front();
            operator_stack.shift_undo_stack();
        }

        // Dump the world states
        ref_comps.sort_unstable();
        tracing::trace!(?ref_comps, "Current reference world state");

        let mut comps: Vec<(Entity, Comp)> = world
            .query::<(Entity, &Comp)>()
            .iter(&world)
            .map(|(e, c)| (e, *c))
            .collect();
        comps.sort_unstable();
        tracing::trace!(?comps, "Current world state");

        operator_stack
            .validate()
            .expect("`OperatorStack` validation failed");

        // Convert `comps: Comp<Entity>` -> `comps_converted: Comp<Name>`
        let mut comps_converted: Vec<Comp<Name>> = comps
            .iter()
            .map(|(_, comp)| {
                let link = if let Some(ent) = comp.link {
                    let i = comps
                        .binary_search_by_key(&ent, |x| x.0)
                        .unwrap_or_else(|_| panic!("dangling link: {ent}"));
                    Some(comps[i].1.name)
                } else {
                    None
                };

                Comp {
                    name: comp.name,
                    link,
                }
            })
            .collect();
        comps_converted.sort_unstable();

        // The world state must exactly match the reference world state
        assert_eq!(
            ref_comps, comps_converted,
            "the world (right) is not in the expected state (left)"
        );
    }
}

#[derive(Debug)]
struct CompStashed {
    name: Name,
    link: Option<VirtEntity>,
}

impl stash::StashableComponent for Comp {
    type Stashed = CompStashed;

    fn stash(&mut self, cx: &mut dyn stash::StashCx) -> Self::Stashed {
        let &mut Comp { name, link } = self;
        CompStashed {
            name,
            link: link.map(|entity| cx.entity_to_virt(entity)),
        }
    }

    fn unstash(CompStashed { name, link }: Self::Stashed, cx: &mut dyn stash::StashCx) -> Comp {
        Comp {
            name,
            link: link.map(|virt_entity| cx.entity_to_phys(virt_entity)),
        }
    }
}

#[derive(Debug)]
struct SpawnOperator {
    comp: Comp,
}

#[derive(Debug)]
struct SpawnOperatorExecuted {
    comp: Comp,
    virt_entity: VirtEntity,
}

impl operator::Operator for SpawnOperator {
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let virt_entity = cx.new_virt_entity();
        Ok(operator::OperatorRedo::redo(
            Box::new(SpawnOperatorExecuted {
                comp: self.comp,
                virt_entity,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for SpawnOperatorExecuted {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        let entity = cx.world_mut().spawn(self.comp).id();
        cx.spawned(entity, self.virt_entity);
        tracing::debug!(%entity, virt_entity = %self.virt_entity, "Spawned an entity");
        self
    }
}

impl operator::OperatorUndo for SpawnOperatorExecuted {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        cx.despawn(self.virt_entity);
        self
    }
}

#[derive(Debug)]
struct DespawnOperator {
    entities: Vec<Entity>,
}

#[derive(Debug)]
struct DespawnOperatorExecuted {
    virt_entities: Vec<VirtEntity>,
    referents: Vec<(VirtEntity, VirtEntity)>,
}

impl operator::Operator for DespawnOperator {
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let virt_entities = self
            .entities
            .iter()
            .map(|&x| cx.entity_to_virt(x))
            .collect();

        let referents = cx
            .world_mut()
            .query::<(Entity, &Comp)>()
            .iter(cx.world())
            .filter_map(match_ok!(, (entity, &Comp { link: Some(link), .. })
                if self.entities.contains(&link)
                => (cx.entity_to_virt(entity), cx.entity_to_virt(link))))
            .collect();

        Ok(operator::OperatorRedo::redo(
            Box::new(DespawnOperatorExecuted {
                virt_entities,
                referents,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for DespawnOperatorExecuted {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        for &(virt_entity, _) in &self.referents {
            let entity = cx.entity_to_phys(virt_entity);
            let mut comp = cx.world_mut().get_mut::<Comp>(entity).unwrap();
            comp.link = None;
        }

        cx.despawn_and_stash(&self.virt_entities);

        self
    }
}

impl operator::OperatorUndo for DespawnOperatorExecuted {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        cx.respawn_from_stash(&self.virt_entities);

        for &(virt_entity, virt_link) in &self.referents {
            let entity = cx.entity_to_phys(virt_entity);
            let link = cx.entity_to_phys(virt_link);
            let mut comp = cx.world_mut().get_mut::<Comp>(entity).unwrap();
            comp.link = Some(link);
        }

        self
    }
}

#[derive(Debug)]
struct UpdateOperator {
    entity: Entity,
    link: Entity,
}

#[derive(Debug)]
struct UpdateOperatorExecuted {
    virt_entity: VirtEntity,
    virt_link: Option<VirtEntity>,
}

impl operator::Operator for UpdateOperator {
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let operator = Box::new(UpdateOperatorExecuted {
            virt_entity: cx.entity_to_virt(self.entity),
            virt_link: Some(cx.entity_to_virt(self.link)),
        });

        if let Some(pa) =
            prev_operator.and_then(|x| x.as_any_mut().downcast_mut::<UpdateOperatorExecuted>())
        {
            if pa.virt_entity == operator.virt_entity {
                let mut comp = cx.world_mut().get_mut::<Comp>(self.entity).unwrap();
                comp.link = Some(self.link);
                return Err(operator::OperatorExecuteError::MergedIntoPrevOperator);
            }
        }

        Ok(operator::OperatorRedo::redo(
            operator,
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for UpdateOperatorExecuted {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        self.redo_or_undo(cx)
    }
}

impl operator::OperatorUndo for UpdateOperatorExecuted {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        self.redo_or_undo(cx)
    }
}

impl UpdateOperatorExecuted {
    fn redo_or_undo(
        mut self: Box<Self>,
        cx: &mut (impl operator::OperatorCx + ?Sized),
    ) -> Box<Self> {
        let entity = cx.entity_to_phys(self.virt_entity);
        let new_link = self.virt_link.map(|x| cx.entity_to_phys(x));

        // Swap `self.virt_link` and `comp.link` with conversion
        let mut comp = cx.world_mut().get_mut::<Comp>(entity).unwrap();
        let old_link = replace(&mut comp.link, new_link);

        self.virt_link = old_link.map(|x| cx.entity_to_virt(x));

        self
    }
}
