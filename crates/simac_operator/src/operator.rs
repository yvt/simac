//! Traits to implement undoable operators
use as_any_min::AsAny;
use bevy_ecs::prelude::*;
use std::fmt;

use crate::stack::{EntityToPhysError, VirtEntity};

/// A [`World`] mutation that has not been applied yet.
///
/// Pass an object of type implementing this trait to
/// [`crate::stack::OperatorStack::execute`] to perform update.
pub trait Operator: fmt::Debug + Send + Sync + 'static {
    /// Execute the operator for the first time.
    fn execute(
        self: Box<Self>,
        cx: &mut dyn OperatorExecuteCx,
        prev_operator: Option<&mut dyn OperatorUndo>,
    ) -> Result<Box<dyn OperatorUndo>, OperatorExecuteError>;
}

/// A [`World`] mutation that has been reverted and can reapply.
pub trait OperatorRedo: AsAny + fmt::Debug + Send + Sync + 'static {
    /// Redo the operator.
    ///
    /// This method should perfom the exact same operations as the original
    /// [`Operator`] except that it must reuse the [`VirtEntity`]s allocated by
    /// it and stored in `self`.
    fn redo(self: Box<Self>, cx: &mut dyn OperatorForwardCx) -> Box<dyn OperatorUndo>;
}

/// A [`World`] mutation that has been applied and can be reverted.
pub trait OperatorUndo: AsAny + fmt::Debug + Send + Sync + 'static {
    /// Undo the operator.
    ///
    /// This method should perform the opposite operations of the
    /// [`OperatorRedo`] counterpart of this instance, in the opposite order.
    /// Examples:
    ///
    /// - If [`OperatorRedo`] spawns an entity with
    ///   [`OperatorForwardCx::spawned`], [`OperatorUndo`] should despawn the
    ///   entity with [`OperatorBackwardCx::despawn`].
    ///
    /// - If [`OperatorRedo`] despawns an entity with
    ///   [`OperatorForwardCx::despawn_and_stash`], [`OperatorUndo`] should
    ///   respawn the entity with [`OperatorBackwardCx::respawn_from_stash`].
    ///
    /// - If [`OperatorRedo`] modifies an entity component, [`OperatorUndo`]
    ///   should revert the modification.
    fn undo(self: Box<Self>, cx: &mut dyn OperatorBackwardCx) -> Box<dyn OperatorRedo>;
}

#[derive(Debug)]
pub enum OperatorExecuteError {
    /// The operator was aborted before making any changes to the [`World`].
    Aborted(anyhow::Error),
    /// The operator successfully executed, but instead of creating a new
    /// [`OperatorUndo`], it merged the undo data into the provided
    /// [`OperatorUndo`].
    MergedIntoPrevOperator,
}

impl From<anyhow::Error> for OperatorExecuteError {
    fn from(value: anyhow::Error) -> Self {
        Self::Aborted(value)
    }
}

/// The interface for [`Operator`] and the related traits to make
/// modifications to a [`World`] while tracking the changes.
pub trait OperatorCx {
    /// Get a reference to the [`World`].
    fn world(&self) -> &World;

    /// Get a mutable reference to the [`World`].
    ///
    /// **Warning:** Do not despawn entities directly on the returned [`World`].
    fn world_mut(&mut self) -> &mut World;

    /// Get te current manifestation of the provided [`VirtEntity`].
    ///
    /// May panic if `entity` is not manifested as a physical entity right now.
    #[track_caller]
    fn entity_to_phys(&self, entity: VirtEntity) -> Entity;

    /// Get te current manifestation of the provided [`VirtEntity`].
    ///
    /// May return `Err` if `entity` is not currently manifested as a physical
    /// entity.
    fn try_entity_to_phys(&self, entity: VirtEntity) -> Result<Entity, EntityToPhysError>;

    /// Get the [`VirtEntity`] representing the provided [`Entity`].
    #[track_caller]
    fn entity_to_virt(&self, entity: Entity) -> VirtEntity;
}

/// [`OperatorCx`] with methods specific to [`Operator::execute`].
pub trait OperatorExecuteCx: OperatorForwardCx {
    /// Upcast `self` to `&mut dyn `[`OperatorForwardCx`].
    // FIXME: Remove this method when dyn upcasting is stabilized
    fn as_forward_cx_mut(&mut self) -> &mut dyn OperatorForwardCx;

    /// Create a new virtual entity.
    ///
    /// You must associate it with a physical entity by calling
    /// [`OperatorForwardCx::spawned`] before returning the control from
    /// [`Operator::execute`].
    fn new_virt_entity(&mut self) -> VirtEntity;
}

/// [`OperatorCx`] with methods specific to [`Operator::execute`] and
/// [`OperatorRedo::redo`].
pub trait OperatorForwardCx: OperatorCx {
    /// Associate the given virtual entity with the given physical entity that
    /// the caller has just spawned.
    #[track_caller]
    fn spawned(&mut self, entity: Entity, virt_entity: VirtEntity);

    /// Despawn the physical entities of `virt_entities`, stashing the state of
    /// the existing components.
    ///
    /// This will render all references to the physical entities invalid. The
    /// caller should ensure that all outstanding references from other
    /// entities' stashable components are removed prior to the method call to
    /// avoid potential panics. If entities form circular references, they must
    /// be despawned all at once with this method. The same applies for
    /// [`OperatorBackwardCx::respawn_from_stash`].
    ///
    /// The behavior is unspecified if `virt_entity` contains duplicate
    /// elements.
    #[track_caller]
    fn despawn_and_stash(&mut self, virt_entities: &[VirtEntity]);
}

/// [`OperatorCx`] with methods specific to [`OperatorUndo::undo`].
pub trait OperatorBackwardCx: OperatorCx {
    /// Respawn the given entities that were despawned during the opposite
    /// operation ([`OperatorForwardCx::despawn_and_stash`]).
    ///
    /// The behavior is unspecified if `virt_entity` contains duplicate
    /// elements.
    #[track_caller]
    fn respawn_from_stash(&mut self, virt_entity: &[VirtEntity]);

    /// Despawn the given `entity`, which was spawned during the opposite
    /// operation ([`OperatorForwardCx::spawned`]).
    #[track_caller]
    fn despawn(&mut self, virt_entity: VirtEntity);
}
