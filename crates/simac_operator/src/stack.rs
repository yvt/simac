//! Operator undo stack
use anyhow::Result;
use bevy_ecs::{entity::EntityHashMap, prelude::*};
use itertools::izip;
use std::{collections::VecDeque, fmt};
use try_match::unwrap_match;

use crate::{operator, stash};

#[cfg(test)]
mod tests;

/// Manages undo/redo stacks and the mapping between virtual and physical
/// entities.
///
/// # `OperatorStack` as Resource
///
/// It is recommended to store `OperatorStack` as a resource ([`Resource`]) so
/// that it can be accessed by systems. You can use [`World::resource_scope`] to
/// call `OperatorStack`'s methods taking `&mut `[`World`] as a parameter.
/// Example:
///
/// ```rust
/// use bevy_ecs::prelude::*;
/// use simac_operator::stack::OperatorStack;
///
/// fn do_undo(world: &mut World) {
///     world
///         .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
///             operator_stack.undo(world)
///         });
/// }
/// ```
///
/// Note that operators have no access to the `OperatorStack` resource since it
/// is temporarily removed from the world during the `resource_scope` call.
///
/// # Panic Safety
///
/// This type's methods are not panic safe. A panic may render it in an invalid
/// state.
#[derive(Debug, Resource)]
pub struct OperatorStack {
    virt_entities: VirtEntityDataMap,
    stashes: stash::Stashes,
    /// An operator that has been executed in preview mode and is ready to
    /// cancel.
    undo_preview: Option<OperatorEntry<Box<dyn operator::OperatorUndo>>>,
    /// Applied operators that are ready to undo.
    undo_stack: VecDeque<OperatorEntry<Box<dyn operator::OperatorUndo>>>,
    /// Rolled-back operators that are ready to redo.
    redo_stack: Vec<OperatorEntry<Box<dyn operator::OperatorRedo>>>,
    merge_allowed: bool,
}

/// An undoable/redoable operator.
#[derive(Debug)]
struct OperatorEntry<T> {
    operator: T,
    /// Virtual entities spawned by this operator.
    spawned: Vec<VirtEntity>,
    /// Virtual entities despawned by this operator.
    despawned: Vec<VirtEntity>,
}

/// A virtual form (stable identifier) of [`Entity`] created by
/// [`operator::Operator::execute`].
///
/// It can manifest as different [`Entity`]s (called *physical* entities) as
/// the entity is despawned and respawned by undo and redo operations.
///
/// # Rationale
///
/// After despawning an entity, it is not possible to recreate the entity with
/// the same [`Entity`]. Therefore, in order to support undo/redo operations,
/// operator objects need to reference entities by persisting identifiers rather
/// than [`Entity`]s.
///
/// # Internal Representation
///
/// For a virtual entity NOT spawned by an operator, [`VirtEntity`] is
/// internally represented by the first [`Entity`] associated with the virtual
/// entity. [`OperatorStack`] allocates an internal data structure only for
/// virtual entities that have been spawned or despawned by operators, thus
/// avoiding memory leak with externally-managed entities.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VirtEntity(Entity);

/// A `HashSet` pre-configured to use `EntityHash` hashing.
pub type VirtEntityHashSet = std::collections::HashSet<VirtEntity, bevy_ecs::entity::EntityHash>;

/// A `HashMap` pre-configured to use `EntityHash` hashing.
pub type VirtEntityHashMap<V> =
    std::collections::HashMap<VirtEntity, V, bevy_ecs::entity::EntityHash>;

#[derive(Default, Debug)]
struct VirtEntityDataMap {
    /// [`VirtEntity`] -> [`VirtEntityData`]
    data_map: VirtEntityHashMap<VirtEntityData>,
    /// [`VirtEntityData::Spawned::entity`] -> [`VirtEntity`]
    phys_to_virt: EntityHashMap<VirtEntity>,
}

#[derive(Debug)]
enum VirtEntityData {
    /// This virtual entity was just created by
    /// [`operator::OperatorExecuteCx::new_virt_entity`], awaiting physical
    /// entity association.
    New,
    /// This virtual entity is materialized as a physical entity. That is,
    /// the operator spawning the entity has been applied (not reverted), and
    /// any operator despawning the entity has not yet been applied.
    Spawned {
        /// The current manifestation of the virtual entity.
        entity: Entity,
    },
    /// This virtual entity was despawned by [`operator::Operator`] or
    /// [`operator::OperatorRedo`]. That is, an operator despawned it, and the
    /// operator has not been reverted.
    DespawnedWithStash { stash_key: stash::EntityStashKey },
    /// This virtual entity was despawned by [`operator::OperatorUndo`]. That
    /// is, the operator spawning the entity has been reverted.
    Despawned,
}

#[derive(Debug, Clone, Copy, thiserror::Error)]
pub enum EntityToPhysError {
    #[error("call `spawned` first")]
    NotInitialized,
    #[error("not materialized")]
    Despawned,
}

impl Default for OperatorStack {
    fn default() -> Self {
        Self {
            virt_entities: <_>::default(),
            stashes: <_>::default(),
            undo_preview: None,
            undo_stack: <_>::default(),
            redo_stack: <_>::default(),
            merge_allowed: true,
        }
    }
}

impl OperatorStack {
    /// Register a type implementing [`stash::StashableComponent`].
    pub fn init_component<T: Component + stash::StashableComponent>(
        &mut self,
        components: &bevy_ecs::component::Components,
    ) {
        self.stashes.init_component::<T>(components);
    }

    /// Execute `operator`.
    ///
    /// Propagates the error if [`operator::Operator::execute`] returns
    /// [`operator::OperatorExecuteError::Aborted`].
    ///
    /// # Panics
    ///
    /// This method will panic if the [preview undo stack](#preview-mode) is
    /// not empty.
    #[tracing::instrument(skip_all)]
    pub fn execute(
        &mut self,
        world: &mut World,
        operator: Box<dyn operator::Operator>,
    ) -> Result<()> {
        self.execute_inner(world, operator, false)
    }

    /// Execute `operator` in the preview mode.
    ///
    /// Propagates the error if [`operator::Operator::execute`] returns
    /// [`operator::OperatorExecuteError::Aborted`].
    ///
    /// # Preview Mode
    ///
    /// The preview mode is an alternate mode of operator execution used to
    /// preview the effect of an unfinalized operation without disturbing the
    /// undo and redo stacks.
    ///
    /// In the preview mode, an executed operator is pushed to a special undo
    /// stack called a *preview undo stack*, which can only hold one entry.
    /// Undoing the preview operator should be done by calling
    /// [`Self::clear_preview`] instead of [`Self::undo`].
    ///
    /// While the preview undo stack is occupied, most operations mutating a
    /// stack (except for [`Self::clear_preview`]) are disallowed and will
    /// panic.
    /// The caller must first call [`Self::clear_preview`] before executing
    /// other operators (abandoning the original operation) or executing the
    /// original operator again but this time in the normal mode (thus
    /// finalizing the operation).
    ///
    /// An operator executed in the preview mode is never merged with a previous
    /// operation (`Operator::execute()` always receives `None` as
    /// `prev_operator`), ensuring that [`Self::clear_preview`] undoes the
    /// preview operation only.
    ///
    /// # Panics
    ///
    /// This method will panic if the [preview undo stack](#preview-mode) is
    /// not empty.
    /// Call [`Self::clear_preview`] first before calling this method to update
    /// the preview.
    #[tracing::instrument(skip_all)]
    pub fn execute_preview(
        &mut self,
        world: &mut World,
        operator: Box<dyn operator::Operator>,
    ) -> Result<()> {
        self.execute_inner(world, operator, true)
    }

    /// An inner method of [`Self::execute`] and [`Self::execute_preview`].
    fn execute_inner(
        &mut self,
        world: &mut World,
        operator: Box<dyn operator::Operator>,
        preview: bool,
    ) -> Result<()> {
        if !preview {
            self.assert_undo_preview_empty();
        }

        // Find the last mergable operator
        let mut prev_operator = if !preview && self.redo_stack.is_empty() && self.merge_allowed {
            self.undo_stack.back_mut()
        } else {
            None
        };

        let mut spawned = Vec::new();
        let mut despawned = Vec::new();
        let operator = match operator.execute(
            &mut OperatorCxImpl {
                op: OperatorOp::Execute {
                    spawned: &mut spawned,
                    despawned: &mut despawned,
                },
                world,
                virt_entities: &mut self.virt_entities,
                stashes: &mut self.stashes,
            },
            // FIXME: Figure out why `as _` is needed here
            prev_operator
                .as_mut()
                .map(|operator| &mut *operator.operator as _),
        ) {
            Ok(operator) => operator,
            Err(operator::OperatorExecuteError::Aborted(e)) => {
                tracing::debug!("The operator was aborted");
                assert!(
                    spawned.is_empty(),
                    "the operator was aborted but spawned entities"
                );
                assert!(
                    despawned.is_empty(),
                    "the operator was aborted but despawned entities"
                );
                return Err(e);
            }
            Err(operator::OperatorExecuteError::MergedIntoPrevOperator) => {
                let prev_operator = prev_operator.expect(
                    "the operator returned `MergedIntoPrevOperator` even though \
                    we didn't provide `prev_operator`",
                );

                for virt_entity in &spawned {
                    match &self.virt_entities.data_map[virt_entity] {
                        VirtEntityData::New => panic!(
                            "the operator created a new virtual entity {virt_entity} \
                            but did not associate it with a physical entity"
                        ),
                        VirtEntityData::Spawned { .. }
                        | VirtEntityData::DespawnedWithStash { .. } => {}
                        VirtEntityData::Despawned => unreachable!(),
                    }
                }

                prev_operator.spawned.extend(spawned);
                prev_operator.despawned.extend(despawned);
                return Ok(());
            }
        };

        for virt_entity in &spawned {
            match &self.virt_entities.data_map[virt_entity] {
                VirtEntityData::New => panic!(
                    "the operator created a new virtual entity {virt_entity} \
                    but did not associate it with a physical entity"
                ),
                VirtEntityData::Spawned { .. } | VirtEntityData::DespawnedWithStash { .. } => {}
                VirtEntityData::Despawned => unreachable!(),
            }
        }

        tracing::debug!(operator_undo = ?operator, "The operator is complete");

        let stack_entry = OperatorEntry {
            operator,
            spawned,
            despawned,
        };

        if preview {
            self.undo_preview = Some(stack_entry);
        } else {
            self.clear_redo_stack();

            self.undo_stack.push_back(stack_entry);

            self.merge_allowed = true;
        }

        Ok(())
    }

    /// Undo the last executed operation (if any).
    ///
    /// # Panics
    ///
    /// This method will panic if the [preview undo stack](#preview-mode) is
    /// not empty.
    #[tracing::instrument(skip_all)]
    pub fn undo(&mut self, world: &mut World) {
        self.assert_undo_preview_empty();

        let Some(OperatorEntry {
            operator,
            spawned,
            despawned,
        }) = self.undo_stack.pop_back()
        else {
            return;
        };

        let operator = operator.undo(&mut OperatorCxImpl {
            op: OperatorOp::Undo,
            world,
            virt_entities: &mut self.virt_entities,
            stashes: &mut self.stashes,
        });

        self.redo_stack.push(OperatorEntry {
            operator,
            spawned,
            despawned,
        });
    }

    /// Redo the last undone operation (if any).
    ///
    /// # Panics
    ///
    /// This method will panic if the [preview undo stack](#preview-mode) is
    /// not empty.
    #[tracing::instrument(skip_all)]
    pub fn redo(&mut self, world: &mut World) {
        self.assert_undo_preview_empty();

        let Some(OperatorEntry {
            operator,
            spawned,
            despawned,
        }) = self.redo_stack.pop()
        else {
            return;
        };

        let operator = operator.redo(&mut OperatorCxImpl {
            op: OperatorOp::Redo,
            world,
            virt_entities: &mut self.virt_entities,
            stashes: &mut self.stashes,
        });

        self.undo_stack.push_back(OperatorEntry {
            operator,
            spawned,
            despawned,
        });

        self.merge_allowed = false;
    }

    /// Undo the operator executed by [`Self::execute_preview`].
    ///
    /// This method calls [`operator::OperatorUndo::undo`] as [`Self::undo`]
    /// does but discards the resulting [`operator::OperatorRedo`] immediately.
    ///
    /// # Panics
    ///
    /// This method will panic if the [preview undo stack](#preview-mode) is
    /// empty.
    #[tracing::instrument(skip_all)]
    pub fn clear_preview(&mut self, world: &mut World) {
        let Some(OperatorEntry {
            operator,
            spawned,
            despawned: _,
        }) = self.undo_preview.take()
        else {
            panic!("preview undo stack is empty");
        };

        operator.undo(&mut OperatorCxImpl {
            op: OperatorOp::Undo,
            world,
            virt_entities: &mut self.virt_entities,
            stashes: &mut self.stashes,
        });

        // Forget all virtual entities spawned by this operator
        for virt_entity in spawned {
            unwrap_match!(
                self.virt_entities.data_map.remove(&virt_entity),
                Some(VirtEntityData::Despawned)
            );
        }
    }

    #[track_caller]
    #[inline]
    fn assert_undo_preview_empty(&self) {
        assert!(
            self.undo_preview.is_none(),
            "this operation is illegal when preview undo stack is not empty"
        );
    }

    /// Remove all redoable operators.
    #[tracing::instrument(skip_all)]
    pub fn clear_redo_stack(&mut self) {
        while let Some(entry) = self.redo_stack.pop() {
            // Forget all virtual entities spawned by `entry`
            for virt_entity in entry.spawned {
                unwrap_match!(
                    self.virt_entities.data_map.remove(&virt_entity),
                    Some(VirtEntityData::Despawned)
                );
            }
        }
    }

    /// Remove one operator from the bottom of the undo stack.
    ///
    /// This method is intended to be used to limit memory consumption of the
    /// undo stack.
    pub fn shift_undo_stack(&mut self) {
        let Some(entry) = self.undo_stack.pop_front() else { return };

        // Forget all virtual entities despawned by `entry`
        for virt_entity in entry.despawned {
            let stash_key = unwrap_match!(
                self.virt_entities.data_map.remove(&virt_entity),
                Some(VirtEntityData::DespawnedWithStash { stash_key })
            );

            self.stashes.remove(stash_key);
        }
    }

    /// Get the next operator to undo ([`Self::undo`]).
    pub fn undo_stack_peek(&self) -> Option<&dyn operator::OperatorUndo> {
        Some(&*self.undo_stack.back()?.operator)
    }

    /// Get the next operator to redo ([`Self::redo`]).
    pub fn redo_stack_peek(&self) -> Option<&dyn operator::OperatorRedo> {
        Some(&*self.redo_stack.last()?.operator)
    }

    /// Get the preview operator to undo ([`Self::clear_preview`]).
    pub fn preview_undo_stack_peek(&self) -> Option<&dyn operator::OperatorUndo> {
        Some(&*self.undo_preview.as_ref()?.operator)
    }

    /// Get the number of operators in the undo stack.
    pub fn undo_stack_len(&self) -> usize {
        self.undo_stack.len()
    }

    /// Get the number of operators in the redo stack.
    pub fn redo_stack_len(&self) -> usize {
        self.redo_stack.len()
    }

    /// Get te current manifestation of the provided [`VirtEntity`].
    ///
    /// May panic if `entity` is not manifested as a physical entity right now.
    pub fn entity_to_phys(&self, entity: VirtEntity) -> Entity {
        self.virt_entities.entity_to_phys(entity)
    }

    /// Get te current manifestation of the provided [`VirtEntity`].
    ///
    /// May return `Err` if `entity` is not currently manifested as a physical
    /// entity.
    pub fn try_entity_to_phys(&self, entity: VirtEntity) -> Result<Entity, EntityToPhysError> {
        self.virt_entities.try_entity_to_phys(entity)
    }

    /// Get the [`VirtEntity`] representing the provided [`Entity`].
    pub fn entity_to_virt(&self, entity: Entity) -> VirtEntity {
        self.virt_entities.entity_to_virt(entity)
    }
}

impl VirtEntityDataMap {
    #[track_caller]
    fn entity_to_phys(&self, entity: VirtEntity) -> Entity {
        self.try_entity_to_phys(entity)
            .unwrap_or_else(|e| panic!("{entity:?}: {e}"))
    }

    fn try_entity_to_phys(&self, entity: VirtEntity) -> Result<Entity, EntityToPhysError> {
        match self.data_map.get(&entity) {
            None => Ok(entity.0),
            Some(VirtEntityData::New) => Err(EntityToPhysError::NotInitialized),
            Some(&VirtEntityData::Spawned { entity }) => Ok(entity),
            Some(VirtEntityData::DespawnedWithStash { .. } | VirtEntityData::Despawned) => {
                Err(EntityToPhysError::Despawned)
            }
        }
    }

    fn entity_to_virt(&self, entity: Entity) -> VirtEntity {
        self.phys_to_virt
            .get(&entity)
            .copied()
            .unwrap_or(VirtEntity(entity))
    }
}

struct OperatorCxImpl<'a> {
    op: OperatorOp<'a>,
    world: &'a mut World,
    virt_entities: &'a mut VirtEntityDataMap,
    stashes: &'a mut stash::Stashes,
}

enum OperatorOp<'a> {
    /// [`operator::Operator::execute`]
    Execute {
        spawned: &'a mut Vec<VirtEntity>,
        despawned: &'a mut Vec<VirtEntity>,
    },
    /// [`operator::OperatorRedo::redo`]
    Redo,
    /// [`operator::OperatorUndo::undo`]
    Undo,
}

impl operator::OperatorCx for OperatorCxImpl<'_> {
    fn world(&self) -> &World {
        self.world
    }

    fn world_mut(&mut self) -> &mut World {
        self.world
    }

    fn entity_to_phys(&self, entity: VirtEntity) -> Entity {
        self.virt_entities.entity_to_phys(entity)
    }

    fn try_entity_to_phys(&self, entity: VirtEntity) -> Result<Entity, EntityToPhysError> {
        self.virt_entities.try_entity_to_phys(entity)
    }

    fn entity_to_virt(&self, entity: Entity) -> VirtEntity {
        self.virt_entities.entity_to_virt(entity)
    }
}

impl operator::OperatorExecuteCx for OperatorCxImpl<'_> {
    fn as_forward_cx_mut(&mut self) -> &mut dyn operator::OperatorForwardCx {
        self
    }

    fn new_virt_entity(&mut self) -> VirtEntity {
        // Get a unique `Entity`
        let entity_mut = self.world.spawn_empty();
        let entity = entity_mut.id();
        entity_mut.despawn();

        let virt_entity = VirtEntity(entity);
        tracing::debug!(?virt_entity, "Allocated a virtual entity ID");

        assert!(
            self.virt_entities
                .data_map
                .insert(virt_entity, VirtEntityData::New)
                .is_none()
        );

        let OperatorOp::Execute { spawned, .. } = &mut self.op else {
            unreachable!()
        };
        spawned.push(virt_entity);

        virt_entity
    }
}

impl operator::OperatorForwardCx for OperatorCxImpl<'_> {
    fn spawned(&mut self, entity: Entity, virt_entity: VirtEntity) {
        let virt_entity_data = self
            .virt_entities
            .data_map
            .get_mut(&virt_entity)
            .unwrap_or_else(|| panic!("unknown virtual entity: {virt_entity:?}"));

        match self.op {
            OperatorOp::Execute { .. } => {
                assert!(
                    matches!(virt_entity_data, VirtEntityData::New),
                    "{virt_entity:?} does not point to a virtual entity \
                    that was just recently created by `OperatorExecuteCx::\
                    new_virt_entity` and awaiting physical entity association"
                );
            }
            OperatorOp::Redo => {
                assert!(
                    matches!(virt_entity_data, VirtEntityData::Despawned),
                    "{virt_entity:?} does not point to a virtual entity \
                    that was despawned by `OperatorUndo`"
                );
            }
            OperatorOp::Undo => unreachable!(),
        }

        *virt_entity_data = VirtEntityData::Spawned { entity };

        assert!(
            self.virt_entities
                .phys_to_virt
                .insert(entity, virt_entity)
                .is_none(),
            "{entity:?} is already associated with a virtual entity"
        );

        tracing::debug!(
            %entity,
            %virt_entity,
            "Associated the virtual entity to a newly spawned entity",
        );
    }

    fn despawn_and_stash(&mut self, virt_entities: &[VirtEntity]) {
        let entities: Vec<(Entity, stash::EntityStashKey)> = virt_entities
            .iter()
            .map(|&virt_entity| {
                // Find the physical entity
                let entity = self.virt_entities.entity_to_phys(virt_entity);

                // Stash entity components
                let mut entity_mut = self.world.entity_mut(entity);
                let stash_key = self.stashes.stash(
                    &mut entity_mut,
                    &mut StashCxImpl {
                        virt_entities: self.virt_entities,
                    },
                );

                tracing::debug!(%entity, %virt_entity, "Stashed an entity's components");

                (entity, stash_key)
            })
            .collect();

        for ((entity, stash_key), &virt_entity) in izip!(entities, virt_entities) {
            // Despawn the entity
            self.world.despawn(entity);

            // Store the stashed data
            self.virt_entities.data_map.insert(
                virt_entity,
                VirtEntityData::DespawnedWithStash { stash_key },
            );
            self.virt_entities.phys_to_virt.remove(&entity);

            tracing::debug!(%entity, %virt_entity, "Despawned a stashed entity");
        }

        match self.op {
            OperatorOp::Execute {
                ref mut despawned, ..
            } => despawned.extend_from_slice(virt_entities),
            OperatorOp::Redo => {}
            OperatorOp::Undo => unreachable!(),
        }
    }
}

impl operator::OperatorBackwardCx for OperatorCxImpl<'_> {
    fn despawn(&mut self, virt_entity: VirtEntity) {
        // Find the physical entity
        let entity = self.virt_entities.entity_to_phys(virt_entity);

        // Despawn the entity
        assert!(self.world.despawn(entity));

        // Store the stashed data
        self.virt_entities
            .data_map
            .insert(virt_entity, VirtEntityData::Despawned);
        assert_eq!(
            self.virt_entities.phys_to_virt.remove(&entity),
            Some(virt_entity)
        );

        tracing::debug!(%entity, %virt_entity, "Despawned an entity without stashing");
    }

    fn respawn_from_stash(&mut self, virt_entities: &[VirtEntity]) {
        let entities: Vec<(Entity, stash::EntityStashKey)> = virt_entities
            .iter()
            .map(|&virt_entity| {
                let virt_entity_data = self
                    .virt_entities
                    .data_map
                    .get_mut(&virt_entity)
                    .unwrap_or_else(|| panic!("unknown virtual entity: {virt_entity:?}"));
                let stash_key = match *virt_entity_data {
                    VirtEntityData::New => unreachable!(),
                    VirtEntityData::Spawned { entity } => {
                        panic!("{virt_entity:?} is already spawned as {entity:?}")
                    }
                    VirtEntityData::DespawnedWithStash { stash_key } => stash_key,
                    VirtEntityData::Despawned => {
                        panic!(
                            "{virt_entity:?} does not point to a virtual entity \
                            despawned by `Operator` or `OperatorRedo`"
                        )
                    }
                };

                // Re-spawn the entity
                let entity = self.world.spawn_empty().id();

                *virt_entity_data = VirtEntityData::Spawned { entity };
                self.virt_entities.phys_to_virt.insert(entity, virt_entity);

                tracing::debug!(%entity, %virt_entity, "Respawned a stashed entity");

                (entity, stash_key)
            })
            .collect();

        for ((entity, stash_key), virt_entity) in izip!(entities, virt_entities) {
            // Unstash the components
            let mut entity_mut = self.world.entity_mut(entity);
            self.stashes.unstash(
                stash_key,
                &mut entity_mut,
                &mut StashCxImpl {
                    virt_entities: self.virt_entities,
                },
            );

            tracing::debug!(%entity, %virt_entity, "Unstashed an entity's components");
        }
    }
}

struct StashCxImpl<'a> {
    virt_entities: &'a mut VirtEntityDataMap,
}

impl stash::StashCx for StashCxImpl<'_> {
    fn entity_to_phys(&self, entity: VirtEntity) -> Entity {
        self.virt_entities.entity_to_phys(entity)
    }

    fn entity_to_virt(&self, entity: Entity) -> VirtEntity {
        self.virt_entities.entity_to_virt(entity)
    }
}

impl fmt::Display for VirtEntity {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Virt({})", self.0)
    }
}
