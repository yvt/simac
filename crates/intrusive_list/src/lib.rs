//! Intrusive doubly linked list with indices implementing
//! [`typed_index::TypedIndex`].
use std::ops;

// TODO: Publish this to crates.io

pub mod accessor;
pub mod accessor_cell_typed;
pub mod accessor_mut;
pub mod accessor_typed;
pub mod accessor_typed_mut;

/// Circualr linked list header.
#[derive(Debug, Copy, Clone)]
pub struct Head<Index = usize> {
    pub first: Option<Index>,
}

impl<Index> Default for Head<Index> {
    #[inline]
    fn default() -> Self {
        Self { first: None }
    }
}

/// Links to neighbor items.
#[derive(Debug, Copy, Clone)]
pub struct Link<Index = usize> {
    pub prev: Index,
    pub next: Index,
}

impl<Index> Head<Index> {
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.first.is_none()
    }
}

pub trait CellLike {
    type Target;

    fn get(&self) -> Self::Target;
    fn set(&self, value: Self::Target);

    fn modify(&self, f: impl FnOnce(&mut Self::Target))
    where
        Self: Sized,
    {
        let mut x = self.get();
        f(&mut x);
        self.set(x);
    }
}

impl<Element: Copy> CellLike for std::cell::Cell<Element> {
    type Target = Element;

    fn get(&self) -> Self::Target {
        self.get()
    }
    fn set(&self, value: Self::Target) {
        self.set(value);
    }
}

impl<Element: CellLike> CellLike for &Element {
    type Target = Element::Target;

    fn get(&self) -> Self::Target {
        (*self).get()
    }
    fn set(&self, value: Self::Target) {
        (*self).set(value);
    }
}

#[cfg(test)]
mod tests {
    pub(crate) fn push<Element>(this: &mut Vec<Element>, x: Element) -> usize {
        let i = this.len();
        this.push(x);
        i
    }

    pub(crate) fn push_typed<Element>(this: &mut Vec<Element>, x: Element) -> VecIndex {
        let i = this.len();
        this.push(x);
        VecIndex(i)
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub(crate) struct VecIndex(pub usize);

    impl<T> typed_index::TypedIndex<Vec<T>> for VecIndex {
        type Raw = usize;

        fn from_raw(x: Self::Raw) -> Self {
            Self(x)
        }

        fn raw(self) -> Self::Raw {
            self.0
        }
    }
}
