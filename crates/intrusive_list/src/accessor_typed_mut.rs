use crate::*;

impl<Index> Head<Index> {
    #[inline]
    pub fn accessor_typed_mut<'a, Pool, MapLink, Element>(
        &'a mut self,
        pool: &'a mut Pool,
        map_link: MapLink,
    ) -> ListAccessorMutTyped<'a, Index, Pool, MapLink>
    where
        Pool: 'a
            + ?Sized
            + typed_index::IndexExt<Index, Output = Element>
            + typed_index::IndexMutExt<Index>,
        MapLink: FnMut(&mut Element) -> &mut Option<Link<Index>>,
    {
        ListAccessorMutTyped {
            head: self,
            pool,
            map_link,
        }
    }
}

/// Mutable accessor to a linked list.
#[derive(Debug)]
pub struct ListAccessorMutTyped<'a, Index, Pool: ?Sized, MapLink> {
    head: &'a mut Head<Index>,
    pool: &'a mut Pool,
    map_link: MapLink,
}

impl<Index, Pool, MapLink, Element> ListAccessorMutTyped<'_, Index, Pool, MapLink>
where
    Pool: ?Sized + typed_index::IndexExt<Index, Output = Element> + typed_index::IndexMutExt<Index>,
    MapLink: FnMut(&mut Element) -> &mut Option<Link<Index>>,
    Index: PartialEq + Clone,
{
    pub fn head(&self) -> &Head<Index> {
        self.head
    }

    pub fn head_mut(&mut self) -> &mut Head<Index> {
        self.head
    }

    pub fn pool(&self) -> &Pool {
        self.pool
    }

    pub fn pool_mut(&mut self) -> &mut Pool {
        self.pool
    }

    pub fn is_empty(&self) -> bool {
        self.head.is_empty()
    }

    pub fn front(&mut self) -> Option<Index> {
        self.head.first.clone()
    }

    pub fn back(&mut self) -> Option<Index> {
        self.head.first.clone().map(|p| {
            (self.map_link)(self.pool.index_mut_typed(p))
                .as_ref()
                .unwrap()
                .prev
                .clone()
        })
    }

    pub fn front_data(&mut self) -> Option<&mut Element> {
        if let Some(p) = self.front() {
            Some(self.pool.index_mut_typed(p))
        } else {
            None
        }
    }

    pub fn back_data(&mut self) -> Option<&mut Element> {
        if let Some(p) = self.back() {
            Some(self.pool.index_mut_typed(p))
        } else {
            None
        }
    }

    /// Insert `item` before the position `p` (if `at` is `Some(p)`) or to the
    /// the list's back (if `at` is `None`).
    pub fn insert(&mut self, item: Index, at: Option<Index>) {
        {
            debug_assert!(
                (self.map_link)(self.pool.index_mut_typed(item.clone())).is_none(),
                "item is already linked"
            );
        }

        if let Some(first) = self.head.first.clone() {
            let (next, update_first) = if let Some(at) = at {
                let update_first = at == first;
                (at, update_first)
            } else {
                (first, false)
            };

            let prev = (self.map_link)(self.pool.index_mut_typed(next.clone()))
                .as_mut()
                .unwrap()
                .prev
                .clone();
            (self.map_link)(self.pool.index_mut_typed(prev.clone()))
                .as_mut()
                .unwrap()
                .next = item.clone();
            (self.map_link)(self.pool.index_mut_typed(next.clone()))
                .as_mut()
                .unwrap()
                .prev = item.clone();
            *(self.map_link)(self.pool.index_mut_typed(item.clone())) = Some(Link { prev, next });

            if update_first {
                self.head.first = Some(item);
            }
        } else {
            debug_assert!(at.is_none());

            let link = (self.map_link)(self.pool.index_mut_typed(item.clone()));
            self.head.first = Some(item.clone());
            *link = Some(Link {
                prev: item.clone(),
                next: item,
            });
        }
    }

    pub fn push_back(&mut self, item: Index) {
        self.insert(item, None);
    }

    pub fn push_front(&mut self, item: Index) {
        let at = self.front();
        self.insert(item, at);
    }

    /// Remove `item` from the list. Returns `item`.
    pub fn remove(&mut self, item: Index) -> Index {
        {
            debug_assert!(
                (self.map_link)(self.pool.index_mut_typed(item.clone())).is_some(),
                "item is not linked"
            );
        }

        let link: Link<Index> = {
            let link_ref = (self.map_link)(self.pool.index_mut_typed(item.clone()));
            if self.head.first.as_ref() == Some(&item) {
                let next = link_ref.as_ref().unwrap().next.clone();
                if next == item {
                    // The list just became empty
                    self.head.first = None;
                    *link_ref = None;
                    return item;
                }

                // Move the head pointer
                self.head.first = Some(next);
            }

            link_ref.clone().unwrap()
        };

        (self.map_link)(self.pool.index_mut_typed(link.prev.clone()))
            .as_mut()
            .unwrap()
            .next = link.next.clone();
        (self.map_link)(self.pool.index_mut_typed(link.next))
            .as_mut()
            .unwrap()
            .prev = link.prev;
        *(self.map_link)(self.pool.index_mut_typed(item.clone())) = None;

        item
    }

    pub fn pop_back(&mut self) -> Option<Index> {
        self.back().map(|item| self.remove(item))
    }

    pub fn pop_front(&mut self) -> Option<Index> {
        self.front().map(|item| self.remove(item))
    }

    /// Call the specified closure for every element, passing a mutable
    /// reference for each of them.
    pub fn for_each_mut<B>(
        &mut self,
        mut f: impl FnMut(Index, &mut Element) -> ops::ControlFlow<B>,
    ) -> ops::ControlFlow<B> {
        let Some(first) = self.head.first.clone() else {
            return ops::ControlFlow::Continue(());
        };
        let mut current = first.clone();
        loop {
            let next = (self.map_link)(self.pool.index_mut_typed(current.clone()))
                .as_ref()
                .unwrap()
                .next
                .clone();
            f(current.clone(), self.pool.index_mut_typed(current))?;
            if next == first {
                break;
            } else {
                current = next;
            }
        }
        ops::ControlFlow::Continue(())
    }
}

impl<'a, Index, Pool: 'a + ?Sized, MapLink> ops::Deref
    for ListAccessorMutTyped<'a, Index, Pool, MapLink>
{
    type Target = Pool;

    fn deref(&self) -> &Self::Target {
        self.pool
    }
}

impl<'a, Index, Pool: 'a + ?Sized, MapLink> ops::DerefMut
    for ListAccessorMutTyped<'a, Index, Pool, MapLink>
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.pool
    }
}

impl<'a, Index, Pool, MapLink, Element> Extend<Index>
    for ListAccessorMutTyped<'a, Index, Pool, MapLink>
where
    Pool: 'a
        + ?Sized
        + typed_index::IndexExt<Index, Output = Element>
        + typed_index::IndexMutExt<Index>,
    MapLink: FnMut(&mut Element) -> &mut Option<Link<Index>>,
    Index: PartialEq + Clone,
{
    fn extend<I: IntoIterator<Item = Index>>(&mut self, iter: I) {
        for item in iter {
            self.push_back(item);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::push_typed;

    #[test]
    fn basic_mut() {
        let mut pool = Vec::new();
        let mut head = Head::new();
        let mut accessor = head.accessor_typed_mut(&mut pool, |&mut (_, ref mut link)| link);

        let ptr1 = push_typed(&mut accessor, (1, None));
        accessor.push_back(ptr1);

        let ptr2 = push_typed(&mut accessor, (2, None));
        accessor.push_back(ptr2);

        let ptr3 = push_typed(&mut accessor, (3, None));
        accessor.push_front(ptr3);

        println!("{:?}", (accessor.pool(), accessor.head()));

        assert!(!accessor.is_empty());
        assert_eq!(accessor.front(), Some(ptr3));
        assert_eq!(accessor.back(), Some(ptr2));
        assert_eq!(accessor.front_data().unwrap().0, 3);
        assert_eq!(accessor.back_data().unwrap().0, 2);

        let mut items = Vec::new();
        accessor.for_each_mut(|_, &mut (x, _)| {
            items.push(x);
            ops::ControlFlow::Continue::<()>(())
        });
        assert_eq!(items, vec![3, 1, 2]);

        accessor.remove(ptr1);
        accessor.remove(ptr2);
        accessor.remove(ptr3);

        assert!(accessor.is_empty());
    }

    #[test]
    fn basic() {
        let mut pool = Vec::new();
        let mut head = Head::new();
        let (_, ptr2, ptr3) = {
            let mut accessor = head.accessor_typed_mut(&mut pool, |&mut (_, ref mut link)| link);

            let ptr1 = push_typed(&mut accessor, (1, None));
            accessor.push_back(ptr1);

            let ptr2 = push_typed(&mut accessor, (2, None));
            accessor.push_back(ptr2);

            let ptr3 = push_typed(&mut accessor, (3, None));
            accessor.push_front(ptr3);

            println!("{:?}", (accessor.pool(), accessor.head()));

            (ptr1, ptr2, ptr3)
        };

        let accessor = head.accessor_typed(&pool, |(_, link)| link);
        assert!(!accessor.is_empty());
        assert_eq!(accessor.front(), Some(ptr3));
        assert_eq!(accessor.back(), Some(ptr2));
        assert_eq!(accessor.front_data().unwrap().0, 3);
        assert_eq!(accessor.back_data().unwrap().0, 2);

        let items: Vec<_> = accessor.iter().map(|(_, &(x, _))| x).collect();
        assert_eq!(items, vec![3, 1, 2]);
    }
}
