use crate::*;

impl<Index> Head<Index> {
    #[inline]
    pub fn accessor_typed<'a, Pool, MapLink, Element>(
        &'a self,
        pool: &'a Pool,
        map_link: MapLink,
    ) -> ListAccessorTyped<'a, Index, Pool, MapLink>
    where
        Pool: 'a + ?Sized + typed_index::IndexExt<Index, Output = Element>,
        MapLink: Fn(&Element) -> &Option<Link<Index>>,
    {
        ListAccessorTyped {
            head: self,
            pool,
            map_link,
        }
    }
}

/// Accessor to a linked list.
#[derive(Debug)]
pub struct ListAccessorTyped<'a, Index, Pool: ?Sized, MapLink> {
    head: &'a Head<Index>,
    pool: &'a Pool,
    map_link: MapLink,
}

impl<Index, Pool, MapLink, Element> ListAccessorTyped<'_, Index, Pool, MapLink>
where
    Pool: ?Sized + typed_index::IndexExt<Index, Output = Element>,
    MapLink: Fn(&Element) -> &Option<Link<Index>>,
    Index: PartialEq + Clone,
{
    pub fn head(&self) -> &Head<Index> {
        self.head
    }

    pub fn pool(&self) -> &Pool {
        self.pool
    }

    pub fn is_empty(&self) -> bool {
        self.head.is_empty()
    }

    pub fn front(&self) -> Option<Index> {
        self.head.first.clone()
    }

    pub fn back(&self) -> Option<Index> {
        self.head.first.clone().map(|p| {
            (self.map_link)(self.pool.index_typed(p))
                .as_ref()
                .unwrap()
                .prev
                .clone()
        })
    }

    pub fn front_data(&self) -> Option<&Element> {
        if let Some(p) = self.front() {
            Some(self.pool.index_typed(p))
        } else {
            None
        }
    }

    pub fn back_data(&self) -> Option<&Element> {
        if let Some(p) = self.back() {
            Some(self.pool.index_typed(p))
        } else {
            None
        }
    }

    pub fn iter(&self) -> Iter<&Self, Index> {
        Iter {
            next: self.head.first.clone(),
            accessor: self,
        }
    }

    #[track_caller]
    pub fn validate(
        &self,
    ) -> Result<(), impl std::error::Error + use<Index, Pool, MapLink, Element>>
    where
        Index: std::fmt::Debug,
    {
        let Some(first) = &self.head.first else {
            return Ok(());
        };

        #[derive(thiserror::Error, Debug)]
        enum Error<Index: std::fmt::Debug> {
            #[error("`map_link` returned `None` for element at {0:?}")]
            LinkNone(Index),
            #[error(
                "element {0:?} says the next element is {1:?}, but \
                element {1:?} says the previous element is {2:?}"
            )]
            LinkInconsistent(Index, Index, Index),
        }

        let mut current = first.clone();
        loop {
            let link = (self.map_link)(self.pool.index_typed(current.clone()))
                .as_ref()
                .ok_or_else(|| Error::LinkNone(current.clone()))?;
            let next_link = (self.map_link)(self.pool.index_typed(link.next.clone()))
                .as_ref()
                .ok_or_else(|| Error::LinkNone(link.next.clone()))?;
            if current != next_link.prev {
                return Err(Error::LinkInconsistent(
                    current,
                    link.next.clone(),
                    next_link.prev.clone(),
                ));
            }
            if link.next == *first {
                break;
            } else {
                current = link.next.clone();
            }
        }

        Ok(())
    }
}

impl<'a, Index, Pool: 'a + ?Sized, MapLink> ops::Deref
    for ListAccessorTyped<'a, Index, Pool, MapLink>
{
    type Target = Pool;

    fn deref(&self) -> &Self::Target {
        self.pool
    }
}

/// An iterator over the elements of [`ListAccessorTyped`].
#[derive(Debug)]
pub struct Iter<Element, Index> {
    accessor: Element,
    next: Option<Index>,
}

impl<'a, Index, Pool, MapLink, Element> Iterator
    for Iter<&'_ ListAccessorTyped<'a, Index, Pool, MapLink>, Index>
where
    Pool: ?Sized + typed_index::IndexExt<Index, Output = Element>,
    MapLink: 'a + Fn(&Element) -> &Option<Link<Index>>,
    Element: 'a,
    Index: PartialEq + Clone,
{
    type Item = (Index, &'a Element);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(next) = self.next.take() {
            let new_next = (self.accessor.map_link)(self.accessor.pool.index_typed(next.clone()))
                .as_ref()
                .unwrap()
                .next
                .clone();
            if Some(&new_next) == self.accessor.head.first.as_ref() {
                self.next = None;
            } else {
                self.next = Some(new_next);
            }
            Some((next.clone(), self.accessor.pool.index_typed(next)))
        } else {
            None
        }
    }
}
