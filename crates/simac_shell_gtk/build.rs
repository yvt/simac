use std::{env, path::Path};

#[path = "src/config.rs"]
#[allow(dead_code)]
mod config;

fn main() {
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let out_dir = Path::new(&out_dir);

    // [tag:shell_gtk_resources_compile]
    println!("cargo::rerun-if-changed=data/resources");
    let prefix = format!("/{}", config::APP_ID.replace('.', "/"));
    let builder = gvdb::gresource::BundleBuilder::from_directory(
        &prefix,
        Path::new("data/resources"),
        true,
        true,
    )
    .expect("BundleBuilder::from_directory");
    let data = builder.build().expect("BundleBuilder::build");
    std::fs::write(out_dir.join("compiled.gresource"), data)
        .expect("failed to write `compiled.gresource`");
}
