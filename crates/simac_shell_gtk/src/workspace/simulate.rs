//! Simulation
use glib::{prelude::*, subclass::prelude::*};
use simac_operator::stack::OperatorStack;
use simac_session::sim_control;
use std::time::Duration;

use super::{St, Workspace, WorkspaceInner, imp_workspace};
use crate::utils::cell::CellExt as _;

impl Workspace {
    #[tracing::instrument(name = "Workspace::simulation_timer", skip_all)]
    pub(super) fn simulation_timer(&self) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");
        let mut st = inner.shared.st.lock().unwrap();
        let st = &mut *st; // enable split borrow

        // Update simulation parameter to advance time
        st.world.insert_resource(sim_control::Frame {
            time_delta: 1.0 / 60.0, // simulation timer period
            force_update: false,
        });

        st.sch_frame.run(&mut st.world);

        // Get the simulation report
        let sim_control_feedback: &sim_control::SimControlFeedback =
            st.world.get(inner.shared.ent_workspace).unwrap();

        // Update `simulation_aborted`
        if inner
            .simulation_aborted
            .neq_assign(sim_control_feedback.last_error.is_err())
        {
            self.notify_simulation_aborted();
        }

        // Stop simulation timer on error
        if sim_control_feedback.last_error.is_err() {
            tracing::debug!("Got error, setting `simulation-active` to `false`");
            self.set_simulation_active(false);
            return;
        }

        // Read the new `time_scale_max_est`
        let time_scale_max_est = sim_control_feedback.time_scale_max_est;

        if time_scale_max_est != inner.time_scale_max_est.get() {
            tracing::debug!(time_scale_max_est, "Updating `time_scale_max_est`");
            inner.time_scale_max_est.set(time_scale_max_est);
            self.notify_time_scale_actual();
        }
    }

    /// Update the simulation without advancing the time.
    ///
    /// Call this method to update simulation views after inserting a
    /// new view, modifying the circuit, or manipulating the operator stack.
    /// This method also updates some GObject properties to reflect the current
    /// state of [`St`].
    ///
    /// The caller is responsible for locking [`St`] and passing the
    /// mutable reference to this method.
    ///
    /// The caller is also responsible for freezing property notifications of
    /// `self`.
    #[tracing::instrument(name = "Workspace::update_simulation", skip_all)]
    pub(super) fn update_simulation(&self, inner: &WorkspaceInner, st: &mut St) {
        // Emit `update` signal` on main loop
        // (Deferring the emission to avoid reentrancy problems)
        let f = glib::clone!(
            #[weak(rename_to = this)]
            self,
            move || this.emit_by_name("update", &[])
        );
        glib::spawn_future_local(async move { f() });

        // Update simulation parameter to not advance time
        st.world.insert_resource(sim_control::Frame {
            time_delta: 0.0,
            force_update: true,
        });

        st.sch_frame.run(&mut st.world);

        // Update `can-undo` and `can-redo`
        let operator_stack = st.world.resource::<OperatorStack>();
        let can_undo = operator_stack.undo_stack_len() != 0;
        if can_undo != self.imp().can_undo.get() {
            self.imp().can_undo.set(can_undo);
            self.notify_can_undo();
        }

        let can_redo = operator_stack.redo_stack_len() != 0;
        if can_redo != self.imp().can_redo.get() {
            self.imp().can_redo.set(can_redo);
            self.notify_can_redo();
        }

        // Get the simulation report
        let sim_control_feedback: &sim_control::SimControlFeedback =
            st.world.get(inner.shared.ent_workspace).unwrap();

        // Update `simulation_aborted`
        if inner
            .simulation_aborted
            .neq_assign(sim_control_feedback.last_error.is_err())
        {
            self.notify_simulation_aborted();
        }

        // Stop simulation timer on error
        if sim_control_feedback.last_error.is_err() {
            tracing::debug!("Got error, setting `simulation-active` to `false`");
            self.set_simulation_active(false);
        }
    }
}

impl imp_workspace::Workspace {
    /// The getter for `simulation-active` property.
    pub(super) fn is_simulation_active(&self) -> bool {
        let inner = self.inner.get().expect("uninitialized");
        inner.simulation_timeout.inspect(Option::is_some)
    }

    /// The setter for `simulation-active` property.
    #[tracing::instrument(name = "Workspace::set_simulation_active")]
    pub(super) fn set_simulation_active(&self, value: bool) {
        let inner = self.inner.get().expect("uninitialized");

        if value {
            if self.is_simulation_active() {
                return;
            }

            tracing::debug!("Starting simulation");

            let this = self.obj().downgrade();
            inner
                .simulation_timeout
                .set(Some(glib::timeout_add_local_full(
                    Duration::from_millis(1000 / 60),
                    glib::Priority::LOW,
                    move || {
                        let Some(this) = this.upgrade() else {
                            return glib::ControlFlow::Break;
                        };
                        Workspace::simulation_timer(&this);
                        glib::ControlFlow::Continue
                    },
                )));
        } else {
            let Some(source) = inner.simulation_timeout.take() else {
                return;
            };
            tracing::debug!("Stopping simulation");
            source.remove();
        }

        self.obj().notify_time_scale_actual();
    }

    /// The getter for `time-step` property.
    pub(super) fn time_step(&self) -> f64 {
        let inner = self.inner.get().expect("uninitialized");
        inner.time_step.get()
    }

    /// The setter for `time-step` property.
    pub(super) fn set_time_step(&self, value: f64) {
        let inner = self.inner.get().expect("uninitialized");
        let mut st = inner.shared.st.lock().unwrap();
        let mut sim_control = st
            .world
            .get_mut::<sim_control::SimControl>(inner.shared.ent_workspace)
            .unwrap();

        inner.time_step.set(value);
        sim_control.time_step = value;
    }

    /// The getter for `time-scale` property.
    pub(super) fn time_scale(&self) -> f64 {
        let inner = self.inner.get().expect("uninitialized");
        inner.time_scale.get()
    }

    /// The setter for `time-scale` property.
    pub(super) fn set_time_scale(&self, value: f64) {
        let inner = self.inner.get().expect("uninitialized");
        let mut st = inner.shared.st.lock().unwrap();
        let mut sim_control = st
            .world
            .get_mut::<sim_control::SimControl>(inner.shared.ent_workspace)
            .unwrap();

        inner.time_scale.set(value);
        sim_control.time_scale = value;
        self.obj().notify_time_scale_actual();
    }

    /// The getter for `time-scale-actual` property.
    pub(super) fn time_scale_actual(&self) -> f64 {
        let inner = self.inner.get().expect("uninitialized");
        if inner.simulation_timeout.inspect(Option::is_some) {
            // The actual time scale is limited by both the current setting
            // (`time_scale`) and the processing power (`max_time_scale_est`)
            inner.time_scale_max_est.get().min(inner.time_scale.get())
        } else {
            // If the simulation is stopped, the actual time scale is zero
            0.0
        }
    }

    /// The getter for `simulation-aborted` property.
    pub(super) fn simulation_aborted(&self) -> bool {
        let inner = self.inner.get().expect("uninitialized");
        inner.simulation_aborted.get()
    }
}
