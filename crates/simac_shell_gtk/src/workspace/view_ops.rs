//! [`View`] operations
use bevy_ecs::prelude::*;
use bevy_math::{IVec2, Vec2};
use glib::{prelude::*, subclass::prelude::*};
use simac_operator::stack::OperatorStack;
use simac_render::{
    draw,
    modes::{Mode, Modifiers},
    pick,
};
use simac_session::layout;
use std::sync::Arc;

use super::{ActiveMode, Tool, View, imp_view, view_render, workspace_ops::ModeCxImpl};

#[derive(Debug, Clone, Copy)]
pub struct MouseEvent {
    pub mouse_position: Vec2,
    pub modifiers: Modifiers,
}

impl View {
    #[tracing::instrument(name = "View::start_mouse_drag", skip_all)]
    pub fn start_mouse_drag(
        &self,
        view_transform: &view_render::ViewTransform,
        MouseEvent {
            mouse_position,
            modifiers,
        }: MouseEvent,
        tool: Tool,
    ) -> bool {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");
        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        if workspace_st.mode.is_some() {
            tracing::debug!("Another mode is already active");
            return false;
        }

        let view_param = view_render::view_param(inner, view_transform);

        let mouse_position_world = view_param.unproject(mouse_position);

        // Find the element under the mouse pointer
        let hot = workspace_st
            .world
            .run_system_cached_with(
                SysFindHotSpotInput::run,
                SysFindHotSpotInput {
                    view_param,
                    mouse_position,
                },
            )
            .expect("system `SysFindHotSpotInput::run`");
        tracing::debug!(?mouse_position, ?mouse_position_world, ?hot);

        // If `hot` points to a selected item, manipulate the whole selection
        let use_selection = 'a: {
            let Some(hot) = &hot else { break 'a false };
            let comp_selection: &simac_render::modes::select::SelectedCircuitItemsPhys =
                workspace_st.world.get(inner.ent_selection).unwrap();
            let needle = match *hot {
                pick::HotSpot::Device { ent_device } => ent_device,
                pick::HotSpot::WireTerminalNode { ent_node, .. } => ent_node,
                pick::HotSpot::OpenTerminalNode { ent_device, .. } => ent_device,
                pick::HotSpot::OrphanNode { ent_node } => ent_node,
            };
            comp_selection.0.contains(&needle)
        };
        let selected_items = use_selection.then(|| {
            let comp_selection: &simac_render::modes::select::SelectedCircuitItems =
                workspace_st.world.get(inner.ent_selection).unwrap();
            Arc::clone(&comp_selection.ent_items)
        });

        let mut mode_cx = ModeCxImpl::from_workspace_st(workspace_st);
        let mut mode: Box<dyn Mode> = 'mode: {
            // Tool-specific behavior
            match tool {
                Tool::Move | Tool::Displace => {}
                Tool::Wire => {
                    // Decide the first endpoint of the wire to create.
                    // We can only attach a wire to a node for now.
                    // [tag:shell_gtk_wire_endpoint_node]
                    // FIXME: Support attaching a wire to a midpoint of a wire
                    let endpoint0 = match hot {
                        Some(pick::HotSpot::Device { .. }) | None => None,
                        Some(
                            pick::HotSpot::WireTerminalNode { ent_node, .. }
                            | pick::HotSpot::OpenTerminalNode { ent_node, .. }
                            | pick::HotSpot::OrphanNode { ent_node },
                        ) => {
                            let &layout::Position(position) = mode_cx.world.get(ent_node).unwrap();
                            Some(position)
                        }
                    };

                    // Switch to the default behavior if a selected item is
                    // being pointed at
                    if selected_items.is_none() {
                        // Otherwise, start drawing a wire
                        break 'mode Box::new(simac_render::modes::wire::DrawWire::new(
                            workspace_inner.shared.ent_main_circuit,
                            mouse_position_world,
                            endpoint0,
                        ));
                    }
                }
            }

            // Default behavior: Move selected or hot items or start rectangular
            // selection
            if let Some(hot) = hot {
                let displace = tool != Tool::Move;
                if let Some(selected_items) = selected_items {
                    Box::new(
                        simac_render::modes::moving::MoveByMouseMode::from_selection(
                            &selected_items,
                            workspace_inner.shared.ent_main_circuit,
                            mouse_position_world,
                            displace,
                            &mut mode_cx,
                        ),
                    )
                } else {
                    Box::new(simac_render::modes::moving::MoveByMouseMode::from_hot_spot(
                        hot,
                        workspace_inner.shared.ent_main_circuit,
                        mouse_position_world,
                        displace,
                        &mut mode_cx,
                    ))
                }
            } else {
                // Start rectangular selection
                Box::new(simac_render::modes::select::SelectRectByMouseMode::new(
                    workspace_inner.shared.ent_main_circuit,
                    inner.ent_selection,
                    mouse_position_world,
                ))
            }
        };

        mode.set_modifiers(modifiers);

        // Apply the mode operator in preview mode
        let operator = mode.operator(&mut mode_cx);
        tracing::debug!(?mode, ?operator, "Applying mode operator in preview mode");
        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                if let Err(error) = operator_stack.execute_preview(world, operator) {
                    // This could be `todo!` since we can handle it without knowing
                    // the specific type of `error`
                    unreachable!("mode operator failed unexpectedly: {error:?}");
                }
            });

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);

        // This mode is active now
        workspace_st.mode = Some(ActiveMode {
            mode,
            needs_to_reapply: false,
            mouse_position: Some(mouse_position_world),
            modifiers,
        });
        inner.is_mode_owner.set(true);
        self.notify_mode_owner();

        true
    }

    #[tracing::instrument(name = "View::update_mouse_drag", skip_all)]
    pub fn update_mouse_drag(
        &self,
        view_transform: &view_render::ViewTransform,
        MouseEvent {
            mouse_position,
            modifiers,
        }: MouseEvent,
    ) {
        let inner = self.imp().inner.get().expect("uninitialized");

        if !inner.is_mode_owner.get() {
            return tracing::debug!("`self` does not have an owned `Mode`; returning");
        }

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();

        let view_param = view_render::view_param(inner, view_transform);
        let mouse_position = view_param.unproject(mouse_position);

        let mode = workspace_st.mode.as_mut().expect(
            "`is_mode_owner` is set to `true`, so `mode` should also be \
             set to `Some(_)`",
        );

        if Some(mouse_position) != mode.mouse_position {
            mode.mode.mouse_move(mouse_position);
            mode.mouse_position = Some(mouse_position);
            mode.needs_to_reapply = true;
        }

        if modifiers != mode.modifiers {
            mode.mode.set_modifiers(modifiers);
            mode.modifiers = modifiers;
            mode.needs_to_reapply = true;
        }
    }

    #[tracing::instrument(name = "View::end_mouse_drag", skip_all)]
    pub fn end_mouse_drag(
        &self,
        view_transform: &view_render::ViewTransform,
        MouseEvent {
            mouse_position,
            modifiers,
        }: MouseEvent,
    ) {
        // Update `mode` one last time using the final mouse position
        {
            let inner = self.imp().inner.get().expect("uninitialized");

            if !inner.is_mode_owner.get() {
                return tracing::debug!("`self` does not have an owned `Mode`; returning");
            }

            let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
            let mut workspace_st = workspace_inner.shared.st.lock().unwrap();

            let view_param = view_render::view_param(inner, view_transform);
            let mouse_position = view_param.unproject(mouse_position);

            let mode = workspace_st.mode.as_mut().expect(
                "`is_mode_owner` is set to `true`, so `mode` should also be \
                 set to `Some(_)`",
            );
            mode.mode.mouse_move(mouse_position);
            mode.mode.set_modifiers(modifiers);
        }

        self.commit_mode();
    }

    #[tracing::instrument(name = "View::mouse_click", skip_all)]
    pub fn mouse_click(
        &self,
        view_transform: &view_render::ViewTransform,
        MouseEvent {
            mouse_position,
            modifiers,
        }: MouseEvent,
    ) -> bool {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");
        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        let view_param = view_render::view_param(inner, view_transform);

        // Find the element under the mouse pointer
        let hot = workspace_st
            .world
            .run_system_cached_with(
                SysFindHotSpotInput::run,
                SysFindHotSpotInput {
                    view_param,
                    mouse_position,
                },
            )
            .expect("system `SysFindHotSpotInput::run`");
        tracing::debug!(?mouse_position, ?hot);

        workspace_st
            .world
            .run_system_cached_with(
                simac_render::modes::select::SysSelectHotSpot::run,
                simac_render::modes::select::SysSelectHotSpot {
                    ent_selection: inner.ent_selection,
                    hot_spot: hot,
                    additive: modifiers.contains(Modifiers::CONTROL),
                },
            )
            .expect("system `SysSelectHotSpot::run`");

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);

        true
    }

    /// Complete [`super::St::mode`] owned by `self`.
    #[tracing::instrument(name = "View::commit_mode", skip_all)]
    fn commit_mode(&self) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        assert!(inner.is_mode_owner.get());

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();

        let mut mode = workspace_st.mode.take().expect(
            "`is_mode_owner` is set to `true`, so `mode` should also be \
             set to `Some(_)`",
        );
        inner.is_mode_owner.set(false);
        self.notify_mode_owner();

        // Re-apply the mode operator in normal mode
        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.clear_preview(world)
            });

        let operator = mode
            .mode
            .operator(&mut ModeCxImpl::from_workspace_st(&workspace_st));
        tracing::debug!(mode = ?mode.mode, ?operator, "Applying mode operator");
        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                if let Err(error) = operator_stack.execute(world, operator) {
                    // This could be `todo!` since we can handle it without knowing
                    // the specific type of `error`
                    unreachable!("mode operator failed unexpectedly: {error:?}");
                }
            });

        inner
            .workspace
            .update_simulation(workspace_inner, &mut workspace_st);
    }

    /// Cancel [`super::St::mode`] if it's owned by `self`.
    #[tracing::instrument(name = "View::cancel_mode", skip_all)]
    pub fn cancel_mode(&self) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        if !inner.is_mode_owner.get() {
            return tracing::debug!("`self` does not have an owned `Mode`; returning");
        }

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        // Cancel the active mode owned by this view
        assert!(workspace_st.mode.is_some());
        workspace_st.mode = None;
        inner.is_mode_owner.set(false);
        self.notify_mode_owner();

        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.clear_preview(world)
            });

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);
    }

    #[tracing::instrument(name = "View::select_all", skip_all)]
    pub fn select_all(&self) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        let _guard = inner.workspace.freeze_notify();

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        workspace_st
            .world
            .run_system_cached_with(
                simac_render::modes::select::SysSelectAllInput::run,
                simac_render::modes::select::SysSelectAllInput {
                    ent_selection: inner.ent_selection,
                },
            )
            .expect("system `SysSelectAllInput::run`");

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);
    }

    #[tracing::instrument(name = "View::nudge", skip_all)]
    pub fn nudge(&self, offset: IVec2, displace: bool) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        let _guard = inner.workspace.freeze_notify();

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        if workspace_st.mode.is_some() {
            return tracing::info!("Unable to nudge because a mode is active");
        }

        let comp_selection: &simac_render::modes::select::SelectedCircuitItems =
            workspace_st.world.get(inner.ent_selection).unwrap();
        let selected_items = Arc::clone(&comp_selection.ent_items);

        // Create a `moving::MoveByMouseMode` for temporary use
        let mut mode_cx = ModeCxImpl::from_workspace_st(workspace_st);
        let mut mode = simac_render::modes::moving::MoveByMouseMode::from_selection(
            &selected_items,
            workspace_inner.shared.ent_main_circuit,
            Vec2::ZERO,
            displace,
            &mut mode_cx,
        );
        mode.mouse_move(offset.as_vec2());

        // Apply the mode operator
        let operator = mode.operator(&mut mode_cx);

        tracing::debug!(?mode, ?operator, "Applying single-shot mode operator");
        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                if let Err(error) = operator_stack.execute(world, operator) {
                    // This could be `todo!` since we can handle it without
                    // knowing the specific type of `error`
                    unreachable!("mode operator failed unexpectedly: {error:?}");
                }
            });

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);
    }

    #[tracing::instrument(name = "View::rotate", skip_all)]
    pub fn rotate(&self, rotation: layout::Rotation, displace: bool) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        let _guard = inner.workspace.freeze_notify();

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        if workspace_st.mode.is_some() {
            return tracing::info!("Unable to rotate items because a mode is active");
        }

        let operator = {
            let operator_stack: &OperatorStack = workspace_st.world.resource();
            let comp_selection: &simac_render::modes::select::SelectedCircuitItems =
                workspace_st.world.get(inner.ent_selection).unwrap();
            let ent_circuit = workspace_inner.shared.ent_main_circuit;
            if displace {
                let (ent_devices, ent_nodes) =
                    simac_render::modes::moving::displace_target_from_selection(
                        ent_circuit,
                        &workspace_st.world,
                        &comp_selection.ent_items,
                        |e| operator_stack.try_entity_to_phys(e).ok(),
                    );
                let target = simac_session::ops::item_displace::Target::Items {
                    ent_devices,
                    ent_nodes,
                };
                match simac_session::ops::item_displace::ItemDisplace::new_rotate(
                    &workspace_st.world,
                    ent_circuit,
                    target,
                    rotation,
                ) {
                    Ok(operator) => Box::new(operator) as _,
                    Err(error) => {
                        return tracing::info!(
                            %error,
                            "Unable to create an operator to rotate items in displace mode",
                        );
                    }
                }
            } else {
                let ent_items = simac_render::modes::moving::move_target_from_selection(
                    ent_circuit,
                    &workspace_st.world,
                    &comp_selection.ent_items,
                    |e| operator_stack.try_entity_to_phys(e).ok(),
                );
                match simac_session::ops::item_move::ItemMove::new_rotate(
                    &workspace_st.world,
                    ent_items,
                    rotation,
                ) {
                    Ok(operator) => Box::new(operator) as _,
                    Err(error) => {
                        return tracing::info!(
                            %error,
                            "Unable to create an operator to rotate items in move mode",
                        );
                    }
                }
            }
        };

        tracing::debug!(?operator, "Applying operator");
        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.execute(world, operator).expect("execute")
            });

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);
    }

    #[tracing::instrument(name = "View::remove_selected", skip_all)]
    pub fn remove_selected(&self) {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        let _guard = inner.workspace.freeze_notify();

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        if workspace_st.mode.is_some() {
            return tracing::info!("Unable to remove items because a mode is active");
        }

        let ent_items: Vec<Entity> = {
            let operator_stack: &OperatorStack = workspace_st.world.resource();
            let comp_selection: &simac_render::modes::select::SelectedCircuitItems =
                workspace_st.world.get(inner.ent_selection).unwrap();
            comp_selection
                .ent_items
                .iter()
                .filter_map(|&e| operator_stack.try_entity_to_phys(e).ok())
                .collect()
        };

        let operator = Box::new(simac_session::ops::item_remove::ItemRemove {
            ent_circuit: workspace_inner.shared.ent_main_circuit,
            // `ent_items` might contain non-existent entities, but
            // `ItemRemove` just ignores them
            ent_items,
        });

        tracing::debug!(?operator, "Applying operator");
        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.execute(world, operator).expect("execute")
            });

        inner
            .workspace
            .update_simulation(workspace_inner, workspace_st);
    }
}

impl imp_view::View {
    pub(super) fn is_mode_owner(&self) -> bool {
        let inner = self.inner.get().expect("uninitialized");
        inner.is_mode_owner.get()
    }
}

/// A Bevy system to call [`pick::find_hot_spot`].
struct SysFindHotSpotInput {
    view_param: draw::ViewParam,
    mouse_position: Vec2,
}

impl SysFindHotSpotInput {
    fn run(
        In(Self {
            view_param,
            mouse_position,
        }): In<Self>,
        find_hot_spot_param: pick::FindHotSpotParam,
    ) -> Option<pick::HotSpot> {
        pick::find_hot_spot(&find_hot_spot_param, &view_param, mouse_position)
    }
}
