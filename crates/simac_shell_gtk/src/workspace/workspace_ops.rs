//! [`Workspace`] operations (undo stack management)
use bevy_ecs::prelude::*;
use glib::subclass::prelude::*;
use simac_operator::stack::OperatorStack;

#[cfg(doc)]
use simac_render::modes::Mode;

use super::{St, Workspace, WorkspaceInner};

impl Workspace {
    /// Reapply [`Mode::operator`] based on the current state of
    /// [`super::ActiveMode::mode`].
    #[tracing::instrument(skip_all)]
    pub(super) fn reapply_mode_operator(&self, inner: &WorkspaceInner, st: &mut St) {
        let Some(mut mode) = st.mode.take() else { return };

        st.world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.clear_preview(world)
            });

        // Reapply the mode operator in preview mode
        let operator = mode.mode.operator(&mut ModeCxImpl::from_workspace_st(st));
        tracing::debug!(
            mode = ?mode.mode,
            ?operator,
            "Applying mode operator in preview mode",
        );
        st.world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                if let Err(error) = operator_stack.execute_preview(world, operator) {
                    // This could be `todo!` since we can handle it without knowing
                    // the specific type of `error`
                    unreachable!("mode operator failed unexpectedly: {error:?}");
                }
            });

        // Put `mode` back
        st.mode = Some(mode);

        self.update_simulation(inner, st);
    }

    #[tracing::instrument(name = "Workspace::undo", skip_all)]
    pub fn undo(&self) {
        let workspace_inner = self.imp().inner.get().expect("uninitialized");
        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();

        if workspace_st.mode.is_some() {
            return tracing::info!("Unable to undo because a mode is active");
        }

        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.undo(world)
            });

        self.update_simulation(workspace_inner, &mut workspace_st);
    }

    #[tracing::instrument(name = "Workspace::redo", skip_all)]
    pub fn redo(&self) {
        let workspace_inner = self.imp().inner.get().expect("uninitialized");
        let mut st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *st; // enable split borrow

        if workspace_st.mode.is_some() {
            return tracing::info!("Unable to redo because a mode is active");
        }

        workspace_st
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack.redo(world)
            });

        self.update_simulation(workspace_inner, workspace_st);
    }
}
pub(super) struct ModeCxImpl<'a> {
    pub(super) world: &'a World,
    operator_stack: &'a OperatorStack,
}

impl<'a> ModeCxImpl<'a> {
    pub fn from_workspace_st(st: &'a St) -> Self {
        Self {
            world: &st.world,
            operator_stack: st.world.resource(),
        }
    }
}

impl simac_render::modes::ModeCx for ModeCxImpl<'_> {
    fn world(&self) -> &World {
        self.world
    }

    fn entity_to_phys(&self, entity: simac_operator::VirtEntity) -> Entity {
        self.operator_stack.entity_to_phys(entity)
    }

    fn try_entity_to_phys(
        &self,
        entity: simac_operator::VirtEntity,
    ) -> Result<Entity, simac_operator::stack::EntityToPhysError> {
        self.operator_stack.try_entity_to_phys(entity)
    }

    fn entity_to_virt(&self, entity: Entity) -> simac_operator::VirtEntity {
        self.operator_stack.entity_to_virt(entity)
    }
}
