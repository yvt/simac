//! Property access to [`RenderOpts`]
use bevy_ecs::prelude::*;
use simac_session::render::RenderOpts;

use super::imp_workspace;

impl imp_workspace::Workspace {
    /// The getter for `current-speed` property.
    pub(super) fn current_speed(&self) -> f64 {
        let inner = self.inner.get().expect("uninitialized");
        let st = inner.shared.st.lock().unwrap();
        let render_opts: &RenderOpts = st.world.get(inner.shared.ent_workspace).unwrap();
        render_opts.current_speed
    }

    /// The setter for `current-speed` property.
    pub(super) fn set_current_speed(&self, value: f64) {
        let inner = self.inner.get().expect("uninitialized");
        let mut st = inner.shared.st.lock().unwrap();
        let mut render_opts: Mut<RenderOpts> =
            st.world.get_mut(inner.shared.ent_workspace).unwrap();
        render_opts.current_speed = value;
    }

    /// The getter for `potential-color-range` property.
    pub(super) fn potential_color_range(&self) -> f64 {
        let inner = self.inner.get().expect("uninitialized");
        let st = inner.shared.st.lock().unwrap();
        let render_opts: &RenderOpts = st.world.get(inner.shared.ent_workspace).unwrap();
        render_opts.potential_color_range
    }

    /// The setter for `potential-color-range` property.
    pub(super) fn set_potential_color_range(&self, value: f64) {
        let inner = self.inner.get().expect("uninitialized");
        let mut st = inner.shared.st.lock().unwrap();
        let mut render_opts: Mut<RenderOpts> =
            st.world.get_mut(inner.shared.ent_workspace).unwrap();
        render_opts.potential_color_range = value;
    }
}
