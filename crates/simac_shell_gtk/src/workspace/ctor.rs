//! Workspace construction
use anyhow::{Context as _, Result};
use bevy_ecs::prelude::*;
use core::fmt;
use futures::FutureExt;
use gio::prelude::*;
use glib::{subclass::prelude::*, translate::ToGlibPtr};
use simac_render::{draw, pick};
use simac_session::{expr, kio, layout, sim_control};
use std::{
    cell::Cell,
    hash::{Hash, Hasher},
    sync::{Arc, Mutex},
};

use super::{Shared, St, WorkspaceInner, view_bounds};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, SystemSet)]
enum FrameSystem {
    PositionTerminalNodes,
}

#[derive(Clone)]
pub(super) enum Path {
    Std,
    Static(&'static str),
    GioFile(gio::File),
}

impl fmt::Debug for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Path::Std => f.write_str("Std"),
            Path::Static(_) => f.write_str("Static(..)"),
            Path::GioFile(file) => write!(f, "GioFile({:?})", file.uri()),
        }
    }
}

impl PartialEq for Path {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Path::Std, Path::Std) => true,
            (Path::Static(lhs), Path::Static(rhs)) => lhs == rhs,
            (Path::GioFile(lhs), Path::GioFile(rhs)) => lhs.equal(rhs),
            _ => false,
        }
    }
}

impl Eq for Path {}

impl Hash for Path {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Path::Std => state.write_u8(0),
            Path::Static(data) => {
                state.write_u8(1);
                data.hash(state);
            }
            Path::GioFile(file) => {
                state.write_u8(2);
                let ptr: *const gio::ffi::GFile = file.to_glib_none().0;
                unsafe { gio::ffi::g_file_hash(ptr.cast()) }.hash(state)
            }
        }
    }
}

impl Default for super::Workspace {
    /// Construct [`Self`] with an example circuit.
    fn default() -> Self {
        Self::new_from_path(&Path::Static(include_str!(
            "../../../../examples/passive/resistor.simac"
        )))
        .now_or_never()
        .expect("`new_from_path` blocked unexpectedly")
        .expect("failed to load example workspace")
    }
}

impl super::Workspace {
    pub async fn new_from_gio_file(file: gio::File) -> Result<Self> {
        Self::new_from_path(&Path::GioFile(file)).await
    }

    #[tracing::instrument]
    async fn new_from_path(main_file_path: &Path) -> Result<Self> {
        struct StdEnv;

        impl kio::Env for StdEnv {
            type Path = Path;
        }

        impl kio::PreloadEnv for StdEnv {
            #[tracing::instrument(level = "debug", skip(self), ret)]
            fn resolve_path(
                &self,
                referer: &Self::Path,
                path_str: &str,
            ) -> Result<Self::Path, Self::ResolvePathError> {
                if path_str == "builtin:std" {
                    return Ok(Path::Std);
                }

                match referer {
                    Path::Std => unreachable!(),
                    Path::Static(_) => unreachable!(),
                    Path::GioFile(file) => Ok(Path::GioFile(file.resolve_relative_path(path_str))),
                }
            }

            type ResolvePathError = glib::Error;

            #[tracing::instrument(level = "debug", skip(self), ret)]
            async fn read_file(&self, path: &Self::Path) -> Result<Vec<u8>, Self::ReadFileError> {
                match path {
                    Path::Std => {
                        Ok(include_bytes!("../../../simac_session/std.simac")[..].to_owned())
                    }
                    Path::Static(data) => Ok(data.as_bytes().to_owned()),
                    Path::GioFile(file) => {
                        tracing::debug!("Reading a `GFile`");
                        let (data, etag) = file.load_contents_future().await?;
                        tracing::debug!(
                            data.len = data.len(),
                            ?etag,
                            "Completed reading a `GFile`"
                        );
                        Ok(data[..].to_owned())
                    }
                }
            }

            type ReadFileError = glib::Error;

            #[tracing::instrument(level = "debug", skip(self), ret)]
            fn get_loaded_file(&self, _path: &Self::Path) -> Option<Entity> {
                None
            }
        }

        let mut load_cx = kio::LoadCx::default();
        load_cx.preload(&StdEnv, main_file_path).await?;

        Self::new_from_load_cx(load_cx, main_file_path)
    }

    fn new_from_load_cx(load_cx: kio::LoadCx<Path>, main_file_path: &Path) -> Result<Self> {
        let mut world = World::default();

        let mut operator_stack = simac_operator::stack::OperatorStack::default();
        simac_session::init_stashable_components(&mut operator_stack, &mut world);
        world.insert_resource(operator_stack);

        simac_session::init_world(&mut world);

        // Create a frame update schedule
        // FIXME: All but `sim_control` need to run only after modification
        let mut sch_frame = Schedule::default();
        sch_frame.add_systems(
            (
                expr::system_configs_compile(),
                sim_control::system_configs_update(|f: Res<sim_control::Frame>| *f),
            )
                .chain(),
        );
        sch_frame.add_systems(draw::update_symbols);
        sch_frame.add_systems(pick::update_symbols);
        sch_frame.add_systems(
            layout::position_terminal_nodes.in_set(FrameSystem::PositionTerminalNodes),
        );
        sch_frame.add_systems(simac_render::modes::select::SelectedCircuitItemsPhys::system_update);
        sch_frame.add_systems(
            view_bounds::sys_update_circuit_aabb.after(FrameSystem::PositionTerminalNodes),
        );

        // Load `load_cx` into `world`
        let loaded = world
            .run_system_cached_with(sys_kio_load, load_cx)
            .unwrap()?;

        // Find the main file's entity
        let ent_file = loaded.file_ent_map[main_file_path];

        // Find the loaded file's workspace entity
        // (note: Imported files may provide their own ones, so
        // make sure we look for the one from the main file)
        let (ent_workspace, sim_control, _) = world
            .query::<(Entity, &sim_control::SimControl, &kio::FileMember)>()
            .iter(&world)
            .find(|(_, _, file_member)| file_member.0 == ent_file)
            .context("unable to locate workspace node")?;

        // Read current settings of the `SimControl`
        let sim_control::SimControl {
            time_step,
            time_scale,
            ent_root_circuit: ent_main_circuit,
        } = *sim_control;

        // Activate and initialize the `SimControl`
        world.entity_mut(ent_workspace).insert(sim_control::Active);

        let this: Self = glib::Object::new();

        _ = this.imp().inner.set(WorkspaceInner {
            main_file: Cell::new(match main_file_path {
                Path::Std => unreachable!(),
                Path::Static(_) => None,
                Path::GioFile(file) => Some(file.clone()),
            }),
            simulation_timeout: Cell::new(None),
            subframe: Cell::new(0.0),
            time_step: Cell::new(time_step),
            time_scale: Cell::new(time_scale),
            time_scale_max_est: Cell::new(0.0), // set after simulation
            simulation_aborted: Cell::new(false),
            save_busy: Cell::new(false),
            shared: Arc::new(Shared {
                st: Mutex::new(St {
                    world,
                    sch_frame,
                    mode: None,
                }),
                ent_file,
                ent_main_circuit,
                ent_workspace,
            }),
        });

        // Start simulation
        this.set_simulation_active(true);

        Ok(this)
    }
}

fn sys_kio_load(
    In(load_cx): In<kio::LoadCx<Path>>,
    commands: Commands<'_, '_>,
    circuits: Query<kio::ExistingCircuitData>,
) -> Result<kio::Loaded<Path>, kio::LoadError<Path>> {
    load_cx.load(commands, circuits)
}

#[cfg(test)]
mod tests {
    use super::super::Workspace;
    use macro_rules_attribute::apply;

    #[apply(glib_test!)]
    async fn load_example() {
        // `Workspace::default` should not panic
        Workspace::default();
    }

    #[apply(glib_test!)]
    async fn load_file() {
        let path = std::path::Path::new(env!("CARGO_MANIFEST_DIR"))
            .join("../../examples/passive/resistor.simac");
        let file = gio::File::for_path(dbg!(path));
        Workspace::new_from_gio_file(file)
            .await
            .expect("failed to load workspace");
    }
}
