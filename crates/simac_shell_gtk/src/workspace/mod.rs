//! Application main data model
use bevy_ecs::prelude::*;
use bevy_math::IRect;
use glib::{
    prelude::*,
    subclass::{Signal, prelude::*},
};
use simac_render::{
    draw,
    modes::{Mode, Modifiers},
};
use std::{
    cell::{Cell, OnceCell, RefCell},
    marker::PhantomData,
    sync::{Arc, Mutex, OnceLock},
};

#[cfg(doc)]
use simac_operator::stack::OperatorStack;
#[cfg(doc)]
use simac_session::{render::RenderOpts, sim_control};

use crate::utils::{cell::CellExt, value_ty::IRectBoxed};

mod ctor;
mod render_opts;
mod save;
mod simulate;
mod view_bounds;
mod view_manage;
mod view_ops;
mod view_render;
mod workspace_ops;
pub use save::*;
pub use view_ops::*;
pub use view_render::*;

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, glib::Enum, glib::Variant)]
#[enum_type(name = "SimacTool")]
pub enum Tool {
    Move,
    #[default]
    Displace,
    Wire,
}

glib::wrapper! {
    pub struct Workspace(ObjectSubclass<imp_workspace::Workspace>);
}

glib::wrapper! {
    pub struct View(ObjectSubclass<imp_view::View>);
}

mod imp_workspace {
    use super::*;

    #[derive(derive_debug::Dbg, Default, glib::Properties)]
    #[properties(wrapper_type = super::Workspace)]
    pub struct Workspace {
        #[property(name ="simulation-active", type = bool,
        get = Self::is_simulation_active, set = Self::set_simulation_active)]
        #[property(name = "time-step", type = f64, get = Self::time_step, set = Self::set_time_step)]
        #[property(name = "time-scale", type = f64, get = Self::time_scale, set = Self::set_time_scale)]
        #[property(name = "time-scale-actual", type = f64, get = Self::time_scale_actual)]
        #[property(name = "current-speed", type = f64,
        get = Self::current_speed, set = Self::set_current_speed)]
        #[property(name = "simulation-aborted", type = bool, get = Self::simulation_aborted)]
        #[property(name = "potential-color-range", type = f64,
        get = Self::potential_color_range, set = Self::set_potential_color_range)]
        #[property(name = "main-file", type = Option<gio::File>, get = Self::main_file)]
        #[property(name = "save-busy", type = bool, get = Self::save_busy)]
        pub(super) inner: OnceCell<WorkspaceInner>,
        /// The last known value of `operator_stack.undo_stack_len() != 0`.
        #[property(get)]
        pub(super) can_undo: Cell<bool>,
        /// The last known value of `operator_stack.redo_stack_len() != 0`.
        #[property(get)]
        pub(super) can_redo: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Workspace {
        const NAME: &'static str = "SimacWorkspace";
        type Type = super::Workspace;
    }
}

// A single module cannot contain multiple instances of `#[derive(Properties)]`
// <https://github.com/gtk-rs/gtk-rs-core/issues/1110>
mod imp_view {
    use super::*;

    #[derive(derive_debug::Dbg, Default, glib::Properties)]
    #[properties(wrapper_type = super::View)]
    pub struct View {
        #[property(name = "mode-owner", type = bool, get = Self::is_mode_owner)]
        pub(super) inner: OnceCell<ViewInner>,
        /// The world-space AABB (axis-aligned bounding box) of all items in the
        /// circuit.
        #[property(get = Self::bounding_rect)]
        _bounding_rect: PhantomData<IRectBoxed>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for View {
        const NAME: &'static str = "SimacView";
        type Type = super::View;
    }
}

#[derive(derive_debug::Dbg)]
struct WorkspaceInner {
    shared: Arc<Shared>,
    #[dbg(formatter = "CellExt::fmt_debug")]
    main_file: Cell<Option<gio::File>>,
    /// The timeout source ID if simulation is running.
    #[dbg(formatter = "CellExt::fmt_debug")]
    simulation_timeout: Cell<Option<glib::SourceId>>,
    subframe: Cell<f64>,
    time_step: Cell<f64>,
    /// Cached value of [`sim_control::SimControl::time_scale`].
    time_scale: Cell<f64>,
    /// Last-seen value of
    /// [`sim_control::SimControlFeedback::time_scale_max_est`].
    time_scale_max_est: Cell<f64>,
    /// Was simulation stopped due to error?
    simulation_aborted: Cell<bool>,
    /// Is a save operation in progress?
    save_busy: Cell<bool>,
}

/// [`Workspace`]-specific data shared between threads.
#[derive(derive_debug::Dbg)]
struct Shared {
    st: Mutex<St>,
    ent_file: Entity,
    ent_main_circuit: Entity,
    ent_workspace: Entity,
}

/// Workspace mutable state
#[derive(derive_debug::Dbg)]
struct St {
    /// The Bevy ECS world.
    ///
    /// # Resources
    ///
    /// - [`OperatorStack`]
    world: World,
    #[dbg(skip)]
    sch_frame: Schedule,
    /// The currently active mode.
    ///
    /// If it's set, the preview undo stack of the [`OperatorStack`] carries an
    /// operator produced by the mode.
    /// (In this state, most mutating methods of [`OperatorStack`] will panic;
    /// see [`OperatorStack::execute_preview`] for details.)
    ///
    /// If it's set, exactly one view should have [`ViewInner::is_mode_owner`]
    /// set.
    mode: Option<ActiveMode>,
}

#[derive(Debug)]
struct ActiveMode {
    mode: Box<dyn Mode>,
    needs_to_reapply: bool,
    /// The last mouse position reported by [`Mode::mouse_move`].
    mouse_position: Option<bevy_math::Vec2>,
    /// The last modifier flags reported by [`Mode::set_modifiers`].
    modifiers: Modifiers,
}

#[derive(derive_debug::Dbg)]
struct ViewInner {
    render_state: RefCell<ViewRenderState>,
    workspace: Workspace,
    ent_sim_view: Entity,
    ent_selection: Entity,
    /// Was [`St::mode`] started by this view?
    is_mode_owner: Cell<bool>,
    /// The last observed value of `bounding-rect` property.
    last_bounding_rect: Cell<IRect>,
}

#[derive(derive_debug::Dbg)]
struct ViewRenderState {
    view_state: draw::ViewState,
    #[dbg(skip)]
    tessellator: epaint::Tessellator,
}

#[glib::derived_properties]
impl ObjectImpl for imp_workspace::Workspace {
    fn signals() -> &'static [Signal] {
        static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
        SIGNALS.get_or_init(|| {
            vec![
                // `update`: Emitted when a significant change to a circuit
                // occurs. Simulation steps do not emit this signal.
                //
                // This signal is emiited from the main loop to avoid reentrancy
                // issues.
                Signal::builder("update").build(),
            ]
        })
    }
}

impl Drop for imp_workspace::Workspace {
    fn drop(&mut self) {
        let Some(inner) = self.inner.get_mut() else {
            return;
        };

        if let Some(source) = inner.simulation_timeout.take() {
            source.remove();
        }
    }
}
