//! Bounding box calculation
use bevy_ecs::prelude::*;
use bevy_math::IRect;
use glib::{prelude::*, subclass::prelude::*};
use simac_session::{circuit, layout};

use crate::utils::{cell::CellExt as _, value_ty::IRectBoxed};

use super::imp_view;

/// The component to store the calculated AABB (axis-aligned bounding box) of
/// a circuit.
///
/// All circuits with this component will have their AABBs automatically
/// calculated by [`sys_update_circuit_aabb`]. The result is available by the
/// `bounding-box` property of [`super::View`].
///
/// Do not add this component directly; use [`aabb_ref`] and [`aabb_release`]
/// instead.
#[derive(Debug, Default, PartialEq, Component)]
pub(super) struct Aabb {
    ref_count: usize,
    aabb: IRect,
}

/// The Bevy system to update [`Aabb`].
pub(super) fn sys_update_circuit_aabb(
    mut circuits: Query<(Entity, &circuit::Circuit, Mut<Aabb>)>,
    positions: Query<&layout::Position>,
) {
    for (ent_circuit, circuit, mut comp_aabb) in circuits.iter_mut() {
        tracing::trace!(%ent_circuit, "Recalculating the AABB of a circuit");

        // Do not shrink a previously calculated AABB - only expand it. This
        // ensures that an annoying scrolling behavior does not occur when you
        // drag circuit items situated away from the boundary.
        let mut aabb = comp_aabb.aabb;

        let mut process_entity = |ent: Entity| {
            let Ok(layout::Position(position)) = positions.get(ent) else {
                return;
            };

            aabb.min = aabb.min.min(*position);
            aabb.max = aabb.max.max(*position);
        };

        circuit
            .devices
            .iter()
            .copied()
            .for_each(&mut process_entity);
        circuit.nodes.iter().copied().for_each(&mut process_entity);

        if comp_aabb.set_if_neq(Aabb { aabb, ..*comp_aabb }) {
            tracing::debug!(%ent_circuit, ?aabb, "New circuit AABB");
        }
    }
}

/// Increment the AABB reference count for the specified circuit entity.
///
/// Make sure to call [`aabb_release`] when AABB calculation is no longer
/// necessary.
pub(super) fn aabb_ref(world: &mut World, ent_circuit: Entity) {
    world
        .entity_mut(ent_circuit)
        .entry::<Aabb>()
        .or_default()
        .ref_count += 1
}

/// Decrement the AABB reference count for the specified circuit entity.
pub(super) fn aabb_release(world: &mut World, ent_circuit: Entity) {
    let mut ent_circuit = world.entity_mut(ent_circuit);
    let mut aabb = ent_circuit.get_mut::<Aabb>().unwrap();
    aabb.ref_count -= 1;
    if aabb.ref_count == 0 {
        ent_circuit.remove::<Aabb>();
    }
}

impl imp_view::View {
    /// The getter for `bounding-rect` property.
    pub(super) fn bounding_rect(&self) -> IRectBoxed {
        let inner = self.inner.get().expect("uninitialized");
        IRectBoxed(inner.last_bounding_rect.get())
    }

    /// Reevaluate the value of `bounding-rect` property and generate a
    /// property notification if necessary.
    pub(super) fn update_bounding_rect(&self) {
        let _guard = self.obj().freeze_notify();

        let inner = self.inner.get().expect("uninitialized");

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");
        let workspace_st = workspace_inner.shared.st.lock().unwrap();

        let ent_circuit = workspace_inner.shared.ent_main_circuit;
        let Some(&Aabb { aabb, .. }) = workspace_st.world.get(ent_circuit) else {
            unreachable!("{ent_circuit} is missing `Aabb` component")
        };

        if inner.last_bounding_rect.neq_assign(aabb) {
            tracing::debug!(self = ?self.obj(), ?aabb, "Emitting `notify::bounding-rect`");
            self.obj().notify_bounding_rect();
        }
    }
}
