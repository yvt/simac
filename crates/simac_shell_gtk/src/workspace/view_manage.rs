//! [`View`] lifecycle operations
use bevy_math::IRect;
use glib::prelude::*;
use glib::subclass::prelude::*;
use simac_render::draw;
use simac_session::{sim_control, simulate};
use std::cell::{Cell, RefCell};

use super::{View, ViewInner, ViewRenderState, Workspace, imp_view, view_bounds};

impl Workspace {
    #[tracing::instrument(name = "Workspace::new_view", level = "debug", skip_all, ret)]
    pub fn new_view(&self, tessellator: epaint::Tessellator) -> View {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");

        let (ent_sim_view, ent_selection);
        {
            let mut st = inner.shared.st.lock().unwrap();
            let st = &mut *st; // enable split borrow

            ent_sim_view = st
                .world
                .spawn(sim_control::SubcircuitViewBundle {
                    subcircuit_view_params: simulate::SimSubcircuitViewParams { path: Vec::new() },
                    subcircuit_view: sim_control::SubcircuitView {
                        ent_sim: inner.shared.ent_workspace,
                    },
                    subcircuit_view_output: simulate::SimSubcircuitViewOutput::default(),
                })
                .id();

            let ent_circuit = inner.shared.ent_main_circuit;

            ent_selection = st
                .world
                .spawn((
                    simac_render::modes::select::SelectedCircuitItems::new(ent_circuit),
                    simac_render::modes::select::SelectedCircuitItemsPhys::default(),
                ))
                .id();

            // Request AABB calculation for the circuit
            view_bounds::aabb_ref(&mut st.world, inner.shared.ent_main_circuit);

            // Fill the view output
            self.update_simulation(inner, st);
        }

        let view: View = glib::Object::new();
        _ = view.imp().inner.set(ViewInner {
            render_state: RefCell::new(ViewRenderState {
                view_state: draw::ViewState::default(),
                tessellator,
            }),
            workspace: self.clone(),
            ent_sim_view,
            ent_selection,
            is_mode_owner: Cell::new(false),
            last_bounding_rect: Cell::new(IRect::new(i32::MIN, i32::MIN, i32::MIN, i32::MIN)),
        });

        self.connect_closure(
            "update",
            true,
            glib::closure_local!(
                #[watch]
                view,
                move |_: Self| {
                    // Recalculate `bounding-rect` on `::update`
                    view.imp().update_bounding_rect();
                }
            ),
        );

        // Recalculate `:bounding-rect` for the first time
        view.imp().update_bounding_rect();

        view
    }
}

#[glib::derived_properties]
impl ObjectImpl for imp_view::View {
    fn dispose(&self) {
        self.obj().cancel_mode();
    }
}

impl Drop for imp_view::View {
    #[tracing::instrument(name = "View::drop", skip(self))]
    fn drop(&mut self) {
        let Some(inner) = self.inner.take() else {
            return;
        };

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");

        let mut workspace_st = workspace_inner.shared.st.lock().unwrap();
        let workspace_st = &mut *workspace_st; // enable split borrow

        tracing::debug!(?inner.ent_sim_view, "Despawning simulation view");
        workspace_st.world.despawn(inner.ent_sim_view);

        tracing::debug!(?inner.ent_sim_view, "Despawning selection store");
        workspace_st.world.despawn(inner.ent_selection);

        view_bounds::aabb_release(
            &mut workspace_st.world,
            workspace_inner.shared.ent_main_circuit,
        );
    }
}

impl View {
    #[inline]
    pub fn workspace(&self) -> Workspace {
        let inner = self.imp().inner.get().expect("uninitialized");
        inner.workspace.clone()
    }
}
