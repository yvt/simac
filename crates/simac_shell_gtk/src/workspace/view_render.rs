//! [`View`] rendering
use bevy_ecs::{prelude::*, system::SystemChangeTick};
use glib::{prelude::*, subclass::prelude::*};
use simac_render::{draw, pick};
use std::{sync::Arc, time::Instant};

use super::{Tool, View, ViewInner, workspace_ops};

#[derive(Debug, Clone, Copy)]
pub struct ViewTransform {
    pub translate: bevy_math::Vec2,
    pub scale: f32,
}

impl View {
    pub fn set_tessellator(&self, tessellator: epaint::Tessellator) {
        let inner = self.imp().inner.get().expect("uninitialized");
        let mut st = inner.render_state.borrow_mut();
        st.tessellator = tessellator;
    }

    /// Acquire a [`draw::Frame`] to present.
    pub fn acquire_frame(
        &self,
        view_transform: &ViewTransform,
        mut mouse_position: Option<bevy_math::Vec2>,
        tool: Tool,
    ) -> draw::Frame {
        let _guard = self.freeze_notify();

        let inner = self.imp().inner.get().expect("uninitialized");
        let mut st = inner.render_state.borrow_mut();

        let workspace_inner = inner.workspace.imp().inner.get().expect("uninitialized");

        let view_param = view_param(inner, view_transform);

        // TODO: Pipelined rendering
        let mut frame;
        {
            let mut workspace_st = workspace_inner.shared.st.lock().unwrap();

            // If a mode is active, update the preview if necessary
            let mode_visual = if let Some(mode) = &mut workspace_st.mode {
                if mode.needs_to_reapply {
                    tracing::debug!("`needs_to_reapply` is set; reapplying mode operator");
                    mode.needs_to_reapply = false;
                    inner
                        .workspace
                        .reapply_mode_operator(workspace_inner, &mut workspace_st);
                }

                // If a mode is active, suppress the hot effect trigger by
                // mouse motion
                mouse_position = None;

                // Reborrow `workspace_st.mode` immutably
                let mode = workspace_st.mode.as_ref().unwrap();

                // Generate `ModeVisual`
                let mut mode_cx = workspace_ops::ModeCxImpl::from_workspace_st(&workspace_st);
                let operator_stack: &simac_operator::stack::OperatorStack =
                    workspace_st.world.resource();
                mode.mode.visual(
                    &mut mode_cx,
                    operator_stack.preview_undo_stack_peek().unwrap(),
                )
            } else {
                <_>::default()
            };

            frame = workspace_st
                .world
                .run_system_cached_with(
                    SysUpdatePreInput::run,
                    SysUpdatePreInput {
                        view_param,
                        view_state: &mut st.view_state,
                        ent_render_opts: workspace_inner.shared.ent_workspace,
                        mouse_position,
                        mode_visual,
                        ent_selection: inner.ent_selection,
                        tool,
                        speed: inner.workspace.time_scale_actual(),
                    },
                )
                .expect("system `sys_update_pre`");
        }

        // TODO: Provide a clip rect
        frame.update_post(&mut st.tessellator, epaint::Rect::EVERYTHING);

        frame
    }
}

pub(super) fn view_param(
    view_inner: &ViewInner,
    &ViewTransform { translate, scale }: &ViewTransform,
) -> draw::ViewParam {
    let workspace_inner = view_inner
        .workspace
        .imp()
        .inner
        .get()
        .expect("uninitialized");
    draw::ViewParam {
        ent_circuit: workspace_inner.shared.ent_main_circuit,
        ent_sim_view: Some(view_inner.ent_sim_view),
        scale,
        translate,
    }
}

struct SysUpdatePreInput<'a> {
    view_param: draw::ViewParam,
    view_state: &'a mut draw::ViewState,
    ent_render_opts: Entity,
    mouse_position: Option<bevy_math::Vec2>,
    mode_visual: draw::ModeVisual,
    ent_selection: Entity,
    tool: Tool,
    speed: f64,
}

impl SystemInput for SysUpdatePreInput<'_> {
    type Param<'i> = SysUpdatePreInput<'i>;
    type Inner<'i> = SysUpdatePreInput<'i>;

    fn wrap(this: Self::Inner<'_>) -> Self::Param<'_> {
        this
    }
}

impl SysUpdatePreInput<'_> {
    fn run(
        SysUpdatePreInput {
            view_param,
            view_state,
            ent_render_opts,
            mouse_position,
            mode_visual,
            ent_selection,
            tool,
            speed,
            // cannot use the lifetime parameter from the `impl` block because
            // it is early-bound and would disqualify `run` from being a
            // system function
        }: SysUpdatePreInput<'_>,
        q_selected: Query<Ref<simac_render::modes::select::SelectedCircuitItemsPhys>>,
        q_render_opts: Query<Ref<simac_session::render::RenderOpts>>,
        update_param: draw::UpdateParam,
        find_hot_spot_param: pick::FindHotSpotParam,
        tick: SystemChangeTick,
        mut last_update_time: Local<'_, Option<Instant>>,
    ) -> draw::Frame {
        // TODO: Re-use presented `Frame`w
        let mut frame = draw::Frame::default();

        // Get the render options
        let render_opts = q_render_opts.get(ent_render_opts).unwrap();

        // Get the element under the mouse pointer
        let hot =
            mouse_position.and_then(|p| pick::find_hot_spot(&find_hot_spot_param, &view_param, p));
        tracing::debug!(?mouse_position, ?hot, "Active hot spot");

        // Highlight it if the current tool can operate on it
        let hot = match tool {
            Tool::Move | Tool::Displace => hot,
            // We can only attach a wire to a node for now.
            // [ref:shell_gtk_wire_endpoint_node]
            Tool::Wire => hot.filter(|hot| !matches!(hot, pick::HotSpot::Device { .. })),
        };

        // Get selection
        let selected_items = q_selected
            .get(ent_selection)
            .map(|x| Arc::clone(&x.0))
            .unwrap_or_default();

        // Calculate time step
        let now = Instant::now();
        let time_step = last_update_time.map_or(0.0, |t| {
            now.saturating_duration_since(t).as_secs_f64().min(0.1)
        });
        *last_update_time = Some(now);

        view_state.update_pre(
            update_param,
            &view_param,
            &draw::ViewInteract {
                hot,
                selected_items,
                mode: mode_visual,
            },
            &render_opts,
            &mut frame,
            time_step * speed,
            tick.this_run(),
        );

        frame
    }
}
