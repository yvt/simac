use anyhow::Result;
use bevy_ecs::prelude::*;
use gio::prelude::*;
use glib::subclass::prelude::*;
use simac_operator::stack::OperatorStack;
use simac_session::kio;

use super::{ctor::Path, imp_workspace};
use crate::utils::cell::CellExt;

/// The error type for the synchronous part of
/// [`super::Workspace::save`].
#[derive(Debug)]
pub enum StartSaveError {
    /// Another save operation is in progress.
    AlreadyInProgress,
    /// The main file is not set, and a new one was not provided by
    /// parameter `new_main_file`.
    NoFile,
}

impl super::Workspace {
    /// Save the workspace state to the current main file or a provided new main
    /// file.
    ///
    /// # Cancellation
    ///
    /// Synchronous cancellation is not supported.
    pub fn save(
        &self,
        new_main_file: Option<gio::File>,
    ) -> Result<impl Future<Output = Result<()>> + use<>, StartSaveError> {
        let inner = self.imp().inner.get().expect("uninitialized");

        // Precondition checks
        if inner.save_busy.get() {
            return Err(StartSaveError::AlreadyInProgress);
        }

        let Some(new_main_file) = new_main_file.or_else(|| inner.main_file.get_by_clone()) else {
            return Err(StartSaveError::NoFile);
        };

        Ok(self.clone().save_inner(new_main_file))
    }

    async fn save_inner(self, new_main_file: gio::File) -> Result<()> {
        let inner = self.imp().inner.get().expect("uninitialized");

        // Start save
        scopeguard::defer! {
            inner.save_busy.set(false);
            self.notify("save-busy");
        }
        inner.save_busy.set(true);
        self.notify("save-busy");

        let ent_file = inner.shared.ent_file;
        let save_cx: kio::SaveCx<Path> = {
            let mut st = inner.shared.st.lock().unwrap();

            // Resolve `Id` collisions so that `Id`-based references are not
            // broken by serialization [ref:kio_id_collision]
            let plan = st
                .world
                .run_system_cached(simac_session::meta::assign_ids::sys_plan_assign_ids)
                .unwrap();
            if plan.is_empty() {
                tracing::debug!(
                    "`sys_plan_assign_ids` returned a plan; no `Id` \
                    reassignment is necessary prior to saving"
                );
            } else {
                tracing::debug!(
                    ?plan,
                    "`sys_plan_assign_ids` returned a non-empty plan; executing it"
                );

                let operator = Box::new(simac_session::ops::assign_ids::AssignIds { plan });
                st.world
                    .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                        operator_stack.execute(world, operator).expect("execute")
                    });
            }

            // Update the file entity's path
            {
                let mut comp_file: Mut<kio::File<Path>> = st.world.get_mut(ent_file).unwrap();
                comp_file.path = Path::GioFile(new_main_file.clone());
            }

            // Create `SaveCx`
            st.world
                .run_system_cached_with(sys_kio_presave, ent_file)
                .unwrap()
        };

        save_cx.save(&StdEnv).await?;

        // Update `main_file`
        let old_main_file = inner.main_file.take();
        if old_main_file
            .as_ref()
            .is_none_or(|x| !x.equal(&new_main_file))
        {
            inner.main_file.set(Some(new_main_file));
            self.notify("main-file");
        } else {
            inner.main_file.set(old_main_file);
        }

        Ok(())
    }
}

fn sys_kio_presave(
    In(ent_file): In<Entity>,
    param: kio::PresaveParam<'_, '_, Path>,
) -> kio::SaveCx<Path> {
    kio::SaveCx::presave(&StdEnv, &param, &[ent_file])
}

struct StdEnv;

impl kio::Env for StdEnv {
    type Path = Path;
}

impl kio::PresaveEnv for StdEnv {}

impl kio::SaveEnv for StdEnv {
    fn relativize_path(
        &self,
        referer: &Self::Path,
        target: &Self::Path,
        old_path_str: Option<&str>,
    ) -> Result<String, Self::RelativizePathError> {
        let target = match target {
            Path::Std => return Ok("builtin:std".to_owned()),
            Path::Static(_) => unreachable!(),
            Path::GioFile(file) => file,
        };

        if let Path::GioFile(referer) = referer {
            // Check if `old_path_str` refers to `target`
            if let Some(old_path_str) = old_path_str {
                if referer.resolve_relative_path(old_path_str).equal(target) {
                    return Ok(old_path_str.to_owned());
                }
            }

            // Create a new relative path string
            if let Some(path_str) = referer.relative_path(target) {
                return path_str.into_os_string().into_string().map_err(|_| {
                    glib::Error::new(
                        gio::IOErrorEnum::InvalidFilename,
                        "target file path is not a valid Unicode string",
                    )
                });
            }
        }

        // Fall back to an absolute path
        if let Some(path) = target
            .path()
            .and_then(|x| x.into_os_string().into_string().ok())
        {
            return Ok(path);
        }

        // Fall back to a URL
        Ok(target.uri().into())
    }

    type RelativizePathError = glib::Error;

    async fn write_file(
        &self,
        _ent_file: Entity,
        path: &Self::Path,
        data: Vec<u8>,
    ) -> Result<(), Self::WriteFileError> {
        let Path::GioFile(file) = path else {
            return Err(glib::Error::new(
                gio::IOErrorEnum::PermissionDenied,
                "read-only file",
            ));
        };

        match file
            .replace_contents_future(data, None, true, gio::FileCreateFlags::empty())
            .await
        {
            Ok(_) => Ok(()),
            Err((_, e)) => Err(e),
        }
    }

    type WriteFileError = glib::Error;
}

impl imp_workspace::Workspace {
    /// The getter for `main-file` property.
    pub(super) fn main_file(&self) -> Option<gio::File> {
        let inner = self.inner.get().expect("uninitialized");
        inner.main_file.get_by_clone()
    }

    /// The getter for `save-busy` property.
    pub(super) fn save_busy(&self) -> bool {
        let inner = self.inner.get().expect("uninitialized");
        inner.save_busy.get()
    }
}
