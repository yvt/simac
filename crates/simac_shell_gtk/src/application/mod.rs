//! Implements a subclass of [`libadwaita::Application`].
use gtk::prelude::*;
use libadwaita::subclass::prelude::*;

use crate::{mainwindow, workspace};

pub(crate) fn workspace_gtk_file_filter() -> gtk::FileFilter {
    let filter = gtk::FileFilter::new();
    filter.set_name(Some("SimAC Files (*.simac)"));
    filter.add_pattern("*.simac");
    filter
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, libadwaita::Application,
        @implements gio::ActionMap;
}

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct Application {}

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "SimacApplication";
        type Type = super::Application;
        type ParentType = libadwaita::Application;
    }
}

impl ObjectImpl for imp::Application {
    fn constructed(&self) {
        let this = self.obj();

        this.set_flags(gio::ApplicationFlags::HANDLES_OPEN);
        this.set_option_context_parameter_string(Some("[FILES…]"));

        self.parent_constructed();
    }
}

impl ApplicationImpl for imp::Application {
    fn startup(&self) {
        let this = self.obj();
        self.parent_startup();

        this.set_accels_for_action("app.new-workspace", &["<primary>n"]);
        this.set_accels_for_action("app.open-workspace", &["<primary>o"]);
        this.set_accels_for_action("win.save-workspace", &["<primary>s"]);
        this.set_accels_for_action("win.save-as-workspace", &["<shift><primary>s"]);
        this.set_accels_for_action("window.close", &["<primary>w"]);
        this.set_accels_for_action("win.undo", &["<primary>z"]);
        this.set_accels_for_action("win.redo", &["<shift><primary>z"]);
        this.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);

        this.add_action_entries([
            gio::ActionEntry::builder("new-workspace")
                .activate(|app: &Application, _, _| {
                    app.imp().new_window();
                })
                .build(),
            gio::ActionEntry::builder("open-workspace")
                .activate(|app: &Application, _, _| app.imp().pick_workspace_to_open())
                .build(),
            gio::ActionEntry::builder("about")
                .activate(|app: &Application, _, _| app.imp().about())
                .build(),
        ]);
    }

    fn activate(&self) {
        self.parent_activate();

        self.new_window();
    }

    fn open(&self, files: &[gio::File], _hint: &str) {
        for file in files {
            self.new_window_for_file(file);
        }
    }
}

impl GtkApplicationImpl for imp::Application {}

impl AdwApplicationImpl for imp::Application {}

impl imp::Application {
    fn new_window(&self) {
        let window: mainwindow::MainWindow = glib::Object::builder()
            .property("application", &*self.obj())
            .property("initial-workspace", workspace::Workspace::default())
            .build();
        window.present();
    }

    fn new_window_for_file(&self, file: &gio::File) {
        let window: mainwindow::MainWindow = glib::Object::builder()
            .property("application", &*self.obj())
            .property("initial-workspace-file", file)
            .build();
        window.present();
    }

    fn pick_workspace_to_open(&self) {
        let this = self.obj();
        gtk::FileDialog::builder()
            .default_filter(&workspace_gtk_file_filter())
            .title("Select Workspace to Open")
            .build()
            .open(
                gtk::Window::NONE,
                gio::Cancellable::NONE,
                glib::clone!(
                    #[weak]
                    this,
                    move |result| {
                        let Ok(file) = result else { return };
                        this.open(&[file], "");
                    }
                ),
            );
    }

    fn about(&self) {
        let parent = self
            .obj()
            .windows()
            .into_iter()
            .find(|window| window.is::<mainwindow::MainWindow>());

        let window = libadwaita::AboutWindow::builder()
            .application_name("SimAC")
            .version(env!("CARGO_PKG_VERSION"))
            .comments("An analog circuit simulator")
            .build();
        window.set_transient_for(parent.as_ref());
        window.present();
    }
}
