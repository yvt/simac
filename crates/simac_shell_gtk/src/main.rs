use libadwaita::prelude::*;
extern crate gtk4 as gtk;

#[cfg(test)]
#[macro_use]
mod tests;

#[macro_use]
mod utils;

mod application;
mod config;
mod mainwindow;
mod schematic;
mod workspace;

fn main() {
    // TODO: Use `glib`-based logger
    _ = tracing_subscriber::fmt()
        .with_env_filter(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(tracing_subscriber::filter::LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_ansi(is_terminal::is_terminal(std::io::stdout()))
        .try_init();

    glib::log_set_default_handler(glib::rust_log_handler);

    // Load GL pointers from epoxy (GL context management library used by GTK).
    {
        #[cfg(target_os = "macos")]
        let library = unsafe { libloading::os::unix::Library::new("libepoxy.0.dylib") }.unwrap();
        #[cfg(all(unix, not(target_os = "macos")))]
        let library = unsafe { libloading::os::unix::Library::new("libepoxy.so.0") }.unwrap();
        #[cfg(windows)]
        let library = libloading::os::windows::Library::open_already_loaded("libepoxy-0.dll")
            .or_else(|_| libloading::os::windows::Library::open_already_loaded("epoxy-0.dll"))
            .unwrap();

        epoxy::load_with(|name| {
            unsafe { library.get::<_>(name.as_bytes()) }
                .map(|symbol| *symbol)
                .unwrap_or(std::ptr::null())
        });
    }

    // Load the resources compiled by [ref:shell_gtk_resources_compile]
    gio::resources_register_include!("compiled.gresource").unwrap();

    let application: application::Application = glib::Object::builder()
        .property("application-id", config::APP_ID)
        .build();

    application.run();
}
