//! A widget to render and editor schematics
use bevy_math::Vec2;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use itertools::izip;
use simac_session::layout::{Angle, Rotation};
use std::{
    cell::{Cell, RefCell},
    fmt,
    marker::PhantomData,
};

use crate::workspace;

// TODO: Editing
// TODO: Panning view
// TODO: Variable zoom

mod input;
mod render;
mod scrollable;

glib::wrapper! {
    pub struct SchematicArea(ObjectSubclass<imp::SchematicArea>)
        @extends gtk::Widget, gtk::GLArea,
        @implements gtk::Scrollable;
}

mod imp {
    use bevy_math::IVec2;

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::SchematicArea)]
    pub struct SchematicArea {
        #[property(override_interface = gtk::Scrollable, get, set = Self::set_hadjustment)]
        pub(super) hadjustment: RefCell<gtk::Adjustment>,
        #[property(override_interface = gtk::Scrollable, get, set = Self::set_vadjustment)]
        pub(super) vadjustment: RefCell<gtk::Adjustment>,
        #[property(override_interface = gtk::Scrollable,
            get = Self::scroll_policy, set = Self::set_ignore_scroll_policy)]
        pub(super) _hscroll_policy: PhantomData<gtk::ScrollablePolicy>,
        #[property(override_interface = gtk::Scrollable,
            get = Self::scroll_policy, set = Self::set_ignore_scroll_policy)]
        pub(super) _vscroll_policy: PhantomData<gtk::ScrollablePolicy>,
        pub(super) widget_size: Cell<IVec2>,
        pub(super) workspace: Cell<Option<workspace::Workspace>>,
        pub(super) realized: RefCell<Option<Realized>>,
        pub(super) drag: RefCell<Option<Drag>>,
        pub(super) mouse_position: Cell<Option<Vec2>>,
        #[property(type = Option<workspace::View>, get = Self::view)]
        _view: (),
        pub(super) mode_owner_bind: Cell<Option<glib::SignalHandlerId>>,
        pub(super) bounding_rect_bind: Cell<Option<glib::SignalHandlerId>>,
        #[property(get, set, builder(<_>::default()))]
        pub(super) tool: Cell<workspace::Tool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SchematicArea {
        const NAME: &'static str = "SimacSchematicArea";
        type Type = super::SchematicArea;
        type ParentType = gtk::GLArea;
        type Interfaces = (gtk::Scrollable,);

        fn class_init(klass: &mut Self::Class) {
            klass.install_action(
                "schematic.cancel-mode",
                None,
                |this: &super::SchematicArea, _, _| {
                    let realized = this.imp().realized.borrow();
                    let Some(realized) = &*realized else { return };
                    let Some(view) = &realized.view else { return };
                    view.cancel_mode();
                },
            );
            klass.add_binding_action(
                gtk::gdk::Key::Escape,
                gtk::gdk::ModifierType::empty(),
                "schematic.cancel-mode",
            );

            klass.install_action(
                "schematic.select-all",
                None,
                |this: &super::SchematicArea, _, _| {
                    let realized = this.imp().realized.borrow();
                    let Some(realized) = &*realized else { return };
                    let Some(view) = &realized.view else { return };
                    view.select_all();
                },
            );
            klass.add_binding_action(
                gtk::gdk::Key::A,
                gtk::gdk::ModifierType::CONTROL_MASK,
                "schematic.select-all",
            );

            klass.install_action(
                "schematic.remove",
                None,
                |this: &super::SchematicArea, _, _| {
                    let realized = this.imp().realized.borrow();
                    let Some(realized) = &*realized else { return };
                    let Some(view) = &realized.view else { return };
                    view.remove_selected();
                },
            );
            klass.add_binding_action(
                gtk::gdk::Key::Delete,
                gtk::gdk::ModifierType::empty(),
                "schematic.remove",
            );
            klass.add_binding_action(
                gtk::gdk::Key::KP_Delete,
                gtk::gdk::ModifierType::empty(),
                "schematic.remove",
            );

            for (action_names, offset, key) in [
                (
                    ["schematic.nudge-up", "schematic.nudge-up-displace"],
                    [0, -1],
                    gtk::gdk::Key::Up,
                ),
                (
                    ["schematic.nudge-down", "schematic.nudge-down-displace"],
                    [0, 1],
                    gtk::gdk::Key::Down,
                ),
                (
                    ["schematic.nudge-left", "schematic.nudge-left-displace"],
                    [-1, 0],
                    gtk::gdk::Key::Left,
                ),
                (
                    ["schematic.nudge-right", "schematic.nudge-right-displace"],
                    [1, 0],
                    gtk::gdk::Key::Right,
                ),
            ] {
                for (action_name, displace, modifier) in izip!(
                    action_names,
                    [false, true],
                    [
                        gtk::gdk::ModifierType::CONTROL_MASK,
                        gtk::gdk::ModifierType::empty(),
                    ]
                ) {
                    klass.install_action(
                        action_name,
                        None,
                        move |this: &super::SchematicArea, _, _| {
                            let realized = this.imp().realized.borrow();
                            let Some(realized) = &*realized else { return };
                            let Some(view) = &realized.view else { return };
                            view.nudge(offset.into(), displace);
                        },
                    );
                    klass.add_binding_action(key, modifier, action_name);
                }
            }

            for (action_names, rotation, key) in [
                (
                    [
                        "schematic.rotate-90-ccw",
                        "schematic.rotate-90-ccw-displace",
                    ],
                    Angle::_270.into(),
                    gtk::gdk::Key::bracketleft,
                ),
                (
                    ["schematic.rotate-90-cw", "schematic.rotate-90-cw-displace"],
                    Angle::_90.into(),
                    gtk::gdk::Key::bracketright,
                ),
                (
                    [
                        "schematic.flip-horzizontal",
                        "schematic.flip-horzizontal-displace",
                    ],
                    Rotation::FLIP_X,
                    gtk::gdk::Key::H,
                ),
                (
                    [
                        "schematic.flip-vertical",
                        "schematic.flip-vertical-displace",
                    ],
                    Rotation::FLIP_Y,
                    gtk::gdk::Key::V,
                ),
            ] {
                for (action_name, displace, modifier) in izip!(
                    action_names,
                    [false, true],
                    [
                        gtk::gdk::ModifierType::CONTROL_MASK,
                        gtk::gdk::ModifierType::empty(),
                    ]
                ) {
                    klass.install_action(
                        action_name,
                        None,
                        move |this: &super::SchematicArea, _, _| {
                            let realized = this.imp().realized.borrow();
                            let Some(realized) = &*realized else { return };
                            let Some(view) = &realized.view else { return };
                            view.rotate(rotation, displace);
                        },
                    );
                    klass.add_binding_action(key, modifier, action_name);
                }
            }
        }
    }
}

impl Default for SchematicArea {
    fn default() -> Self {
        glib::Object::new()
    }
}

struct Realized {
    tick_callback: gtk::TickCallbackId,
    size: [i32; 2],
    scale: f32,
    view: Option<workspace::View>,
}

/// The state of an ongoing drag action.
#[derive(Debug)]
struct Drag {
    start_mouse_position: Vec2,
    /// Set if this is definitely a drag, not a click.
    drag_accepted: bool,
    event_seqence: Option<gtk::gdk::EventSequence>,
}

#[glib::derived_properties]
impl ObjectImpl for imp::SchematicArea {
    fn constructed(&self) {
        self.parent_constructed();

        self.obj().set_focusable(true);

        self.init_input();
        self.init_scrollable();
    }

    fn dispose(&self) {
        if let (Some(bind), Some(view)) = (self.mode_owner_bind.take(), self.obj().view()) {
            view.disconnect(bind);
        }

        if let (Some(bind), Some(view)) = (self.bounding_rect_bind.take(), self.obj().view()) {
            view.disconnect(bind);
        }
    }
}

impl WidgetImpl for imp::SchematicArea {
    fn realize(&self) {
        self.parent_realize();

        let tick_callback = self.obj().add_tick_callback(|this, _frame_clock| {
            this.queue_draw();
            glib::ControlFlow::Continue
        });

        assert!(
            self.realized
                .replace(Some(Realized {
                    tick_callback,
                    view: None,
                    size: [0, 0],
                    scale: 1.0,
                }))
                .is_none()
        );
        self.obj().notify_view();
    }

    fn unrealize(&self) {
        if let Some(realized) = self.realized.take() {
            realized.tick_callback.remove();
            self.obj().notify_view();
        }

        self.parent_unrealize();
    }

    fn size_allocate(&self, width: i32, height: i32, baseline: i32) {
        self.widget_size.set([width, height].into());
        self.configure_scroll_adjustments();

        self.parent_size_allocate(width, height, baseline);
    }
}

impl fmt::Debug for imp::SchematicArea {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use glib::translate::ToGlibPtr;
        let ptr: *mut _ = self.obj().to_glib_none().0;
        write!(f, "{ptr:?}")
    }
}

impl SchematicArea {
    pub fn set_workspace(&self, workspace: Option<workspace::Workspace>) {
        self.imp().workspace.set(workspace);
    }
}

impl imp::SchematicArea {
    fn view(&self) -> Option<workspace::View> {
        let realized = self.realized.borrow();
        let realized = realized.as_ref()?;
        realized.view.clone()
    }
}
