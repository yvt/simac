//! [`gtk::Scrollable`] implementation
use bevy_math::Vec2;
use gtk::{prelude::*, subclass::prelude::*};
use itertools::izip;
use std::cell::Cell;

use super::imp;
use crate::workspace;

impl ScrollableImpl for imp::SchematicArea {}

impl imp::SchematicArea {
    pub(super) fn init_scrollable(&self) {
        // Watch `View:bounding-rect`
        let old_view = Cell::new(None::<workspace::View>);
        self.obj().connect_view_notify(move |this| {
            if let (Some(bind), Some(view)) =
                (this.imp().bounding_rect_bind.take(), old_view.take())
            {
                view.disconnect(bind);
            }

            if let Some(view) = this.view() {
                let bind = view.connect_bounding_rect_notify(glib::clone!(
                    #[weak]
                    this,
                    move |_| this.imp().configure_scroll_adjustments(),
                ));
                this.imp().bounding_rect_bind.set(Some(bind));
                old_view.set(Some(view));

                this.imp().configure_scroll_adjustments();
            }
        });
    }

    pub(super) fn set_hadjustment(&self, adjustment: Option<gtk::Adjustment>) {
        let adjustment = adjustment.unwrap_or_default();

        adjustment.connect_value_changed(glib::clone!(
            #[weak(rename_to = this)]
            self.obj(),
            move |_| this.queue_draw()
        ));

        self.hadjustment.replace(adjustment);
    }

    pub(super) fn set_vadjustment(&self, adjustment: Option<gtk::Adjustment>) {
        let adjustment = adjustment.unwrap_or_default();

        adjustment.connect_value_changed(glib::clone!(
            #[weak(rename_to = this)]
            self.obj(),
            move |_| this.queue_draw()
        ));

        self.vadjustment.replace(adjustment);
    }

    pub(super) fn set_ignore_scroll_policy(&self, scroll_policy: gtk::ScrollablePolicy) {
        tracing::debug!(?scroll_policy, "Ignoring the attempt to set scroll policy");
    }

    pub(super) fn scroll_policy(&self) -> gtk::ScrollablePolicy {
        gtk::ScrollablePolicy::Minimum
    }

    /// Reconfigure [`Self::hadjustment`] and [`Self::vadjustment`] based on the
    /// current situation.
    pub(super) fn configure_scroll_adjustments(&self) {
        let view = self.view();
        let bounding_rect = view.map_or(<_>::default(), |view| view.bounding_rect().0);

        let scale = self.viewport_scale() as f64;
        let margin = 40.0;

        // For each axis...
        for (adjustment, widget_dim, min, max) in izip!(
            [&self.hadjustment, &self.vadjustment],
            <[i32; 2]>::from(self.widget_size.get()),
            <[i32; 2]>::from(bounding_rect.min),
            <[i32; 2]>::from(bounding_rect.max),
        ) {
            let [min, max] = [min, max].map(|x| x as f64 * scale);
            let [min, max] = [min - margin, max + margin];

            let widget_dim = widget_dim as f64;

            // adjustment value 0 = coordinate 0 is at center
            let min = min.min(widget_dim * -0.5) + widget_dim * 0.5;
            let max = max.max(widget_dim * 0.5) + widget_dim * 0.5;

            let adjustment = adjustment.borrow();
            adjustment.configure(
                adjustment.value().clamp(min, max - widget_dim),
                min,
                max,
                widget_dim * 0.1,
                widget_dim * 0.9,
                widget_dim,
            );
        }
        // TODO
    }

    pub(super) fn view_transform(&self) -> workspace::ViewTransform {
        let widget_size = (self.widget_size.get() / 2).as_vec2();
        let adjustment_values: Vec2 = [&self.hadjustment, &self.vadjustment]
            .map(|a| a.borrow().value() as f32)
            .into();
        workspace::ViewTransform {
            translate: widget_size - adjustment_values,
            scale: self.viewport_scale(),
        }
    }

    fn viewport_scale(&self) -> f32 {
        // FIXME: Don't hard code `scale` here
        10.0
    }
}
