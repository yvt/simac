use anyhow::Result;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use libadwaita::gdk;
use std::{cell::RefCell, rc::Rc};

use super::imp;
use crate::utils::{cell::CellExt as _, glcx, type_map::RefCellTypeMapExt as _};

/// [`gdk::GLContext`]-specific resources
struct GlCxRes {
    egui_painter: RefCell<egui_glow::Painter>,
    epaint_texture_atlas: RefCell<epaint::TextureAtlas>,
}

impl GLAreaImpl for imp::SchematicArea {
    // TODO: Override `create_context` to reuse a single `GLContext` in as many
    // `Self` instances as possible

    #[tracing::instrument(name = "SchematicArea::resize", skip(width, height))]
    fn resize(&self, width: i32, height: i32) {
        tracing::debug!(width, height);

        let mut realized = self.realized.borrow_mut();
        let Some(realized) = &mut *realized else {
            return tracing::warn!("`realized` is not set");
        };

        realized.size = [width, height];

        realized.scale = width as f32 / self.obj().width() as f32;
        if realized.scale.is_nan() {
            realized.scale = 1.0;
        }
    }

    #[tracing::instrument(name = "SchematicArea::render", skip(cx))]
    fn render(&self, cx: &gdk::GLContext) -> glib::Propagation {
        let _guard = self.obj().freeze_notify();

        let mut realized = self.realized.borrow_mut();
        let Some(realized) = &mut *realized else {
            tracing::warn!("`realized` is not set");
            return glib::Propagation::Stop;
        };

        let cx_res = glcx::gdk_gl_context_resources(cx)
            .get_cloned_or_try_insert_with(|| -> Result<_> {
                let egui_painter = egui_glow::Painter::new(glcx::glow_cx_arc(), "", None, false)?;

                let texture_atlas_width = egui_painter.max_texture_side();
                let epaint_texture_atlas = RefCell::new(epaint::TextureAtlas::new([
                    texture_atlas_width,
                    ((1 << 19) / texture_atlas_width).max(64),
                ]));

                Ok(Rc::new(GlCxRes {
                    egui_painter: RefCell::new(egui_painter),
                    epaint_texture_atlas,
                }))
            })
            .expect("failed to initialize the painter");

        // Set `view`
        let workspace = self.workspace.get_by_clone();
        if realized
            .view
            .as_ref()
            .is_some_and(|view| Some(&view.workspace()) != workspace.as_ref())
        {
            realized.view = None;
            self.obj().notify_view();
        }
        if realized.view.is_none() {
            if let Some(workspace) = workspace.clone() {
                let epaint_texture_atlas = cx_res.epaint_texture_atlas.borrow();
                realized.view = Some(workspace.new_view(epaint::Tessellator::new(
                    // TODO: Recreate `Tessellator` on scale change
                    realized.scale,
                    epaint::TessellationOptions::default(),
                    epaint_texture_atlas.size(),
                    epaint_texture_atlas.prepared_discs(),
                )));
                self.obj().notify_view();
            }
        }
        let Some(view) = &mut realized.view else {
            return glib::Propagation::Stop;
        };

        // Render
        let mut egui_painter = cx_res.egui_painter.borrow_mut();
        egui_painter.clear(realized.size.map(|x| x as u32), [0.1, 0.1, 0.1, 1.0]);

        let frame = view.acquire_frame(
            &self.view_transform(),
            self.mouse_position.get(),
            self.tool.get(),
        );

        if let Some(delta) = cx_res.epaint_texture_atlas.borrow_mut().take_delta() {
            egui_painter.set_texture(epaint::TextureId::default(), &delta);
        }

        egui_painter.paint_primitives(
            realized.size.map(|x| x as u32),
            realized.scale,
            &frame.clipped_primitive,
        );

        glib::Propagation::Stop
    }
}
