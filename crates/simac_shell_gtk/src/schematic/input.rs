//! Input handling
use gtk::{gdk, prelude::*, subclass::prelude::*};
use simac_render::modes::Modifiers;
use std::cell::Cell;

use super::{Drag, SchematicArea, imp};
use crate::workspace;

impl imp::SchematicArea {
    /// Called by `ObjectImpl::constructed`. Install event controllers and
    /// signal handlers.
    pub(super) fn init_input(&self) {
        let ec_motion = gtk::EventControllerMotion::new();
        ec_motion.connect_motion(glib::clone!(
            #[weak(rename_to = this)]
            self,
            move |_, x, y| {
                this.mouse_position
                    .set(Some([x, y].map(|x| x as f32).into()))
            }
        ));
        ec_motion.connect_leave(glib::clone!(
            #[weak(rename_to = this)]
            self,
            move |_| this.mouse_position.set(None)
        ));
        self.obj().add_controller(ec_motion);

        let ec_click = gtk::GestureClick::new();
        ec_click.set_button(1);
        ec_click.connect_stopped(glib::clone!(
            #[weak(rename_to = this)]
            self,
            move |ec| {
                let _span = tracing::info_span!("GestureClick::stopped", ?ec).entered();

                let mut drag_ref = this.drag.borrow_mut();
                let Some(drag) = &mut *drag_ref else { return };

                // The time/distance threshold has been exceeded. This is
                // definitely a drag, not a click.

                // Just a precaution
                if drag.event_seqence != ec.current_sequence() {
                    tracing::warn!("`drag.event_sequence` mismatch");
                    return;
                }

                if drag.drag_accepted {
                    tracing::debug!("The current drag operation has already been accepted");
                    return;
                }

                tracing::debug!("Start true mouse drag");

                let view_transform = this.view_transform();

                let success = 'a: {
                    let realized = this.realized.borrow();
                    let Some(realized) = &*realized else { break 'a false };
                    let Some(view) = &realized.view else { break 'a false };

                    view.start_mouse_drag(
                        &view_transform,
                        workspace::MouseEvent {
                            mouse_position: drag.start_mouse_position,
                            modifiers: convert_gdk_modifiers(ec.current_event_state()),
                        },
                        this.tool.get(),
                    )
                };

                if success {
                    tracing::debug!("Mouse drag accepted by the view");
                    drag.drag_accepted = true;
                } else {
                    tracing::debug!("Mouse drag rejected by the view");
                    *drag_ref = None;
                    drop(drag_ref);
                    ec.set_state(gtk::EventSequenceState::Denied);
                }
            }
        ));
        self.obj().add_controller(ec_click.clone());

        let ec_drag = gtk::GestureDrag::new();
        ec_drag.connect_drag_begin(glib::clone!(
            #[weak(rename_to = this)]
            self,
            move |ec, x, y| {
                let _span = tracing::info_span!("GestureDrag::drag-begin", ?ec).entered();

                this.grab_focus();

                let mut drag_ref = this.drag.borrow_mut();
                if drag_ref.is_some() {
                    tracing::debug!("There is already an ongoing drag operation");
                    return;
                }

                let mouse_position = bevy_math::DVec2::new(x, y).as_vec2();
                this.mouse_position.set(Some(mouse_position));

                tracing::debug!(%mouse_position, "Start provisional mouse drag");

                *drag_ref = Some(Drag {
                    start_mouse_position: mouse_position,
                    drag_accepted: false,
                    event_seqence: ec.current_sequence(),
                });
            }
        ));
        ec_drag.connect_drag_update(glib::clone!(
            #[weak(rename_to = this)]
            self,
            move |ec, x, y| {
                let _span = tracing::info_span!("GestureDrag::drag-update", ?ec).entered();

                let drag = this.drag.borrow();
                let Some(drag) = &*drag else { return };

                if !drag.drag_accepted {
                    return;
                }

                let view_transform = this.view_transform();

                let realized = this.realized.borrow();
                let Some(realized) = &*realized else { return };
                let Some(view) = &realized.view else { return };

                let mouse_position =
                    bevy_math::DVec2::new(x, y).as_vec2() + drag.start_mouse_position;
                tracing::debug!(%mouse_position, "Update mouse drag");
                view.update_mouse_drag(
                    &view_transform,
                    workspace::MouseEvent {
                        mouse_position,
                        modifiers: convert_gdk_modifiers(ec.current_event_state()),
                    },
                );
            }
        ));
        ec_drag.connect_drag_end(glib::clone!(
            #[weak(rename_to = this)]
            self,
            move |ec, x, y| {
                let _span = tracing::info_span!("GestureDrag::drag-end", ?ec).entered();

                let Some(drag) = this.drag.borrow_mut().take() else { return };

                let view_transform = this.view_transform();

                let modifiers = convert_gdk_modifiers(ec.current_event_state());

                let realized = this.realized.borrow();
                let Some(realized) = &*realized else { return };
                let Some(view) = &realized.view else { return };

                if drag.drag_accepted {
                    let mouse_position =
                        bevy_math::DVec2::new(x, y).as_vec2() + drag.start_mouse_position;
                    tracing::debug!(%mouse_position, "End true mouse drag");
                    view.end_mouse_drag(
                        &view_transform,
                        workspace::MouseEvent {
                            mouse_position,
                            modifiers,
                        },
                    );
                } else {
                    tracing::debug!("End mouse click");
                    view.mouse_click(
                        &view_transform,
                        workspace::MouseEvent {
                            mouse_position: drag.start_mouse_position,
                            modifiers,
                        },
                    );
                }
            }
        ));
        self.obj().add_controller(ec_drag.clone());
        ec_drag.group_with(&ec_click);

        // `self.view.mode-owner` -> `self.action_set_enabled(..)`
        let old_view = Cell::new(None::<workspace::View>);
        self.obj().connect_view_notify(move |this| {
            if let (Some(bind), Some(view)) = (this.imp().mode_owner_bind.take(), old_view.take()) {
                view.disconnect(bind);
            }

            if let Some(view) = this.view() {
                let sync = |this: &SchematicArea, view: &workspace::View| {
                    let enable = view.mode_owner();
                    tracing::debug!(
                        enable,
                        "Toggling the enable state of `schematic.cancel-mode`"
                    );
                    this.action_set_enabled("schematic.cancel-mode", enable);
                };

                sync(this, &view);

                this.imp()
                    .mode_owner_bind
                    .set(Some(view.connect_mode_owner_notify(glib::clone!(
                        #[weak]
                        this,
                        move |view| sync(&this, view),
                    ))));
                old_view.set(Some(view));
            }
        });
    }
}

fn convert_gdk_modifiers(gdk_mods: gdk::ModifierType) -> Modifiers {
    [
        (gdk::ModifierType::SHIFT_MASK, Modifiers::SHIFT),
        (gdk::ModifierType::CONTROL_MASK, Modifiers::CONTROL),
        (gdk::ModifierType::ALT_MASK, Modifiers::ALT),
    ]
    .into_iter()
    .filter(|&(src, _)| gdk_mods.contains(src))
    .fold(Modifiers::empty(), |out, (_, dst)| out | dst)
}
