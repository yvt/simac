// [tag:shell_gtk_config_app_id]
pub const APP_ID: &str = unwrap_or(option_env!("APP_ID"), "jp.yvt.SimacShellGtk.Devel");

const fn unwrap_or<'a>(lhs: Option<&'a str>, rhs: &'a str) -> &'a str {
    let Some(lhs) = lhs else { return rhs };
    lhs
}
