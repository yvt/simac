use futures::FutureExt;
use std::panic::AssertUnwindSafe;

pub(crate) use simac_test_utils::init;

macro_rules! glib_test {
    (
        $(#[$meta:meta])*
        async fn $ident:ident() $block:block
    ) => {
        $(#[$meta])*
        #[test]
        fn $ident() {
            crate::tests::init();

            crate::tests::spawn_on_main_context(async {
                $block
            });
        }
    }
}

pub(crate) fn spawn_on_main_context<T: 'static>(f: impl Future<Output = T> + 'static) -> T {
    let (send, recv) = std::sync::mpsc::channel();

    let main_context = glib::MainContext::new();
    let _guard = main_context.acquire().unwrap();
    let main_loop = glib::MainLoop::new(Some(&main_context), false);
    let main_loop2 = main_loop.clone();
    main_context.spawn_local(async move {
        scopeguard::defer!(main_loop2.quit());

        _ = send.send(AssertUnwindSafe(f).catch_unwind().await);
    });
    main_loop.run();

    match recv.recv().unwrap() {
        Ok(x) => x,
        Err(e) => std::panic::resume_unwind(e),
    }
}
