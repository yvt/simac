//! Newtypes to Rust types in [`glib::Value`]

#[derive(Debug, Clone, Copy, PartialEq, Eq, glib::Boxed)]
#[boxed_type(name = "SimacIRect")]
pub struct IRectBoxed(pub bevy_math::IRect);
