//! Utilities for [`type_map`]
use std::cell::RefCell;

pub(crate) trait RefCellTypeMapExt {
    /// Get an existing value or create and insert one if it doesn't exist yet.
    fn get_cloned_or_try_insert_with<T: 'static + Clone, E>(
        &self,
        f: impl FnOnce() -> Result<T, E>,
    ) -> Result<T, E>;
}

impl RefCellTypeMapExt for RefCell<type_map::TypeMap> {
    fn get_cloned_or_try_insert_with<T: 'static + Clone, E>(
        &self,
        f: impl FnOnce() -> Result<T, E>,
    ) -> Result<T, E> {
        if let Some(x) = self.borrow_mut().get().cloned() {
            return Ok(x);
        }

        let value = f()?;

        let mut this = self.borrow_mut();
        match this.entry() {
            type_map::Entry::Occupied(entry) => Ok(T::clone(entry.get())),
            type_map::Entry::Vacant(entry) => Ok(T::clone(entry.insert(value))),
        }
    }
}
