//! Path formatting
use std::path::Path;

pub fn fmt_path(path: &Path) -> glib::GString {
    if let Ok(path) = path.strip_prefix(glib::home_dir()) {
        glib::gformat!("~/{}", path.display())
    } else {
        glib::gformat!("{}", path.display())
    }
}
