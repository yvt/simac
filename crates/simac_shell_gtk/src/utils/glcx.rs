//! GL context
use glib::prelude::*;
use libadwaita::gdk;
use std::{
    cell::RefCell,
    sync::{Arc, OnceLock},
};

static GLOW_CX_ARC: OnceLock<Arc<glow::Context>> = OnceLock::new();

/// Get the process-global instance of [`glow::Context`].
pub(crate) fn glow_cx_arc() -> Arc<glow::Context> {
    Arc::clone(GLOW_CX_ARC.get_or_init(|| unsafe {
        Arc::new(glow::Context::from_loader_function(|s| {
            epoxy::get_proc_addr(s) as *const _
        }))
    }))
}

pub(crate) fn gdk_gl_context_resources(cx: &gdk::GLContext) -> &RefCell<type_map::TypeMap> {
    static QUARK: OnceLock<glib::Quark> = OnceLock::new();

    let quark =
        *QUARK.get_or_init(|| glib::Quark::from_static_str(glib::gstr!("simac-glcx-resources")));

    // Safety: (1) `TypeMap` is `!Send`, and `GLContext` is `!Send` too.
    // (2) This `Quark` is solely used for this purpose.
    unsafe {
        crate::utils::gobjectdata::get_or_insert_with(quark, cx.upcast_ref(), Default::default)
    }
}
