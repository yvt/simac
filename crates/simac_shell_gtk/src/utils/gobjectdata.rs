use glib::prelude::*;
use std::sync::Mutex;

/// Get or create an object associated with a [`glib::Object`] instance, keyed
/// by `quark`.
///
/// # Safety
///
/// - If the object is considered `Send`, `T` may be dropped in a different
///   thread.
///
/// - The data slot for `quark` must not be used for other purposes. It can only
///   be cleared by the [`glib::Object`] finalizer.
///
pub(crate) unsafe fn get_or_insert_with<T: 'static>(
    quark: glib::Quark,
    object: &glib::Object,
    new_value: impl FnOnce() -> T,
) -> &T {
    static SET_LOCK: Mutex<()> = Mutex::new(());

    unsafe {
        if let Some(value) = object.qdata(quark) {
            // Safety: `*x` outlives `*object`
            return value.as_ref();
        }

        let value = new_value();

        {
            // Lock a mutex to ensure atomicity
            let _guard = SET_LOCK.lock().unwrap();

            if let Some(x) = object.qdata(quark) {
                // Safety: `*x` outlives `*object`
                return x.as_ref();
            }

            object.set_qdata(quark, value);
        }

        // Safety: `*x` outlives `*object`
        object.qdata(quark).unwrap().as_ref()
    }
}
