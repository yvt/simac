//! Utilities for [`Cell`]
use std::cell::Cell;

pub(crate) trait CellExt {
    type Element;

    fn get_by_clone(&self) -> Self::Element
    where
        Self::Element: Clone + Default;

    fn inspect<Output>(&self, f: impl FnOnce(&Self::Element) -> Output) -> Output
    where
        Self::Element: Default;

    fn fmt_debug(&self) -> String
    where
        Self::Element: Default + std::fmt::Debug;

    /// If `new_value` is different from the current value, store it and
    /// return `true`.
    fn neq_assign(&self, new_value: Self::Element) -> bool
    where
        Self::Element: Clone + Default + PartialEq;
}

impl<T> CellExt for Cell<T> {
    type Element = T;

    #[inline]
    fn get_by_clone(&self) -> Self::Element
    where
        Self::Element: Clone + Default,
    {
        self.inspect(T::clone)
    }

    #[inline]
    fn inspect<Output>(&self, f: impl FnOnce(&Self::Element) -> Output) -> Output
    where
        Self::Element: Default,
    {
        let x = self.take();
        let out = f(&x);
        self.set(x);
        out
    }

    fn fmt_debug(&self) -> String
    where
        Self::Element: Default + std::fmt::Debug,
    {
        self.inspect(|x| format!("{x:?}"))
    }

    fn neq_assign(&self, new_value: Self::Element) -> bool
    where
        Self::Element: Clone + Default + PartialEq,
    {
        if self.get_by_clone() != new_value {
            self.set(new_value);
            true
        } else {
            false
        }
    }
}
