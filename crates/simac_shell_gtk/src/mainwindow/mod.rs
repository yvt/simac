use gtk::subclass::prelude::*;
use indoc::indoc;
use libadwaita::{prelude::*, subclass::prelude::*};
use simac_numfmt::ruler_log;
use std::{
    cell::{Cell, OnceCell},
    rc::Rc,
    time::Instant,
};
use tracing::Instrument;

use crate::{
    schematic,
    utils::{cell::CellExt as _, pathfmt::fmt_path},
    workspace,
};

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, libadwaita::ApplicationWindow;
}

mod imp {
    use super::*;

    #[derive(glib::Properties)]
    #[properties(wrapper_type = super::MainWindow)]
    pub struct MainWindow {
        #[property(get)]
        pub(super) workspace: OnceCell<workspace::Workspace>,
        #[property(get)]
        pub(super) workspace_load_in_progress: Cell<bool>,
        #[property(name = "initial-workspace", type = Option<workspace::Workspace>,
        set = Self::set_initial_workspace, construct_only)]
        #[property(name = "initial-workspace-file", type = Option<gio::File>,
        set = Self::set_initial_workspace_file, construct_only)]
        /// The workspace to use when the window is constructed
        /// ([`ObjectImpl::constructed`]).
        pub(super) workspace_source: Cell<Option<WorkspaceSource>>,
        pub(super) schematic: OnceCell<schematic::SchematicArea>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        const NAME: &'static str = "SimacMainWindow";
        type Type = super::MainWindow;
        type ParentType = libadwaita::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.install_action("win.save-workspace", None, |this, _, _| this.imp().save());
            klass.install_action("win.save-as-workspace", None, |this, _, _| {
                this.imp().save_as()
            });
            klass.install_action("win.undo", None, |this, _, _| {
                let Some(workspace) = this.imp().workspace.get() else { return };
                workspace.undo();
            });
            klass.install_action("win.redo", None, |this, _, _| {
                let Some(workspace) = this.imp().workspace.get() else { return };
                workspace.redo();
            });
            klass.install_action(
                "win.tool-switch",
                Some(&workspace::Tool::static_variant_type()),
                |this, _, param| {
                    let Some(schematic) = this.imp().schematic.get() else { return };
                    let tool: workspace::Tool = param.unwrap().try_get().unwrap();
                    schematic.set_tool(tool);
                },
            );
        }
    }
}

impl Default for MainWindow {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl Default for imp::MainWindow {
    fn default() -> Self {
        Self {
            workspace: OnceCell::new(),
            workspace_load_in_progress: Cell::new(true),
            workspace_source: Cell::new(None),
            schematic: OnceCell::new(),
        }
    }
}

/// Specifies the source of a [`workspace::Workspace`].
enum WorkspaceSource {
    Concrete(workspace::Workspace),
    File(gio::File),
}

#[glib::derived_properties]
impl ObjectImpl for imp::MainWindow {
    fn constructed(&self) {
        self.obj().set_default_width(800);
        self.obj().set_default_height(500);

        if crate::config::APP_ID.ends_with(".Devel") {
            self.obj().add_css_class("devel");
        }

        /// Call the specified closure when `workspace` is set to a non-null
        /// value.
        macro_rules! register_on_workspace_ready_success {
            ($f:expr) => {
                let f = $f;
                self.obj().connect_workspace_notify(move |this| {
                    if let Some(workspace) = this.imp().workspace.get() {
                        f(this, workspace);
                    }
                });
            };
        }

        // Main menu
        // ------------------------------------------------------------------
        let main_menu = gio::Menu::new();
        main_menu.append_section(None, &{
            let menu = gio::Menu::new();
            menu.append(Some("_New Project"), Some("app.new-workspace"));
            menu.append(Some("_Open Project…"), Some("app.open-workspace"));
            menu
        });
        main_menu.append_section(None, &{
            let menu = gio::Menu::new();
            menu.append(Some("_Save Project…"), Some("win.save-workspace"));
            menu.append(Some("Save Project _As…"), Some("win.save-as-workspace"));
            menu
        });
        main_menu.append_section(None, &{
            let menu = gio::Menu::new();
            menu.append(Some("_Keyboard Shortcuts…"), Some("win.show-help-overlay"));
            menu.append(Some("A_bout Simac…"), Some("app.about"));
            menu
        });

        // Workspace settings
        // ------------------------------------------------------------------

        // Settings popover contents
        let settings = gtk::Box::new(gtk::Orientation::Vertical, 5);

        let mut i = 0;
        let mut settings_heading = |title: &str| {
            i += 1;
            gtk::Label::builder()
                .label(title)
                .css_classes(["heading"])
                .halign(gtk::Align::Start)
                .margin_top(if i == 1 { 5 } else { 10 })
                .margin_bottom(5)
                .build()
        };

        let settings_append_row =
            |list: &gtk::ListBox, title: &str, subtitle: &str, widget: &gtk::Widget| {
                let label = gtk::Box::builder()
                    .orientation(gtk::Orientation::Vertical)
                    .valign(gtk::Align::Center)
                    .build();
                label.append(
                    &gtk::Label::builder()
                        .label(title)
                        .css_classes(["title"])
                        .halign(gtk::Align::Start)
                        .hexpand(true)
                        .build(),
                );
                label.append(
                    &gtk::Label::builder()
                        .label(subtitle)
                        .css_classes(["subtitle"])
                        .halign(gtk::Align::Start)
                        .hexpand(true)
                        .build(),
                );

                let b = gtk::Box::new(gtk::Orientation::Horizontal, 0);
                b.append(&label);
                b.append(widget);
                list.append(
                    &gtk::ListBoxRow::builder()
                        .activatable(false)
                        .child(&b)
                        .build(),
                );
            };

        // Simulation setting values
        let simulation_speed_adjustment = gtk::Adjustment::new(0.0, -5.0, 0.0, 0.1, 0.5, 0.0);
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            simulation_speed_adjustment,
            move |_: &_, workspace: &workspace::Workspace| {
                workspace
                    .bind_property("time-scale", &simulation_speed_adjustment, "value")
                    .transform_from(|_, x| Some(f64::powf(10., x)))
                    .transform_to(|_, x| Some(f64::log10(x)))
                    .bidirectional()
                    .sync_create()
                    .build();
            }
        ));

        let current_speed_adjustment = gtk::Adjustment::new(0.0, 3.0, 8.0, 0.1, 0.5, 0.0);
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            current_speed_adjustment,
            move |_: &_, workspace: &workspace::Workspace| {
                workspace
                    .bind_property("current-speed", &current_speed_adjustment, "value")
                    .transform_from(|_, x| Some(f64::powf(10., x)))
                    .transform_to(|_, x| Some(f64::log10(x)))
                    .bidirectional()
                    .sync_create()
                    .build();
            }
        ));

        let time_step_adjustment = gtk::Adjustment::new(0.0, -36.0, 0.0, 1.0, 3.0, 0.0);
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            time_step_adjustment,
            move |_: &_, workspace: &workspace::Workspace| {
                workspace
                    .bind_property("time-step", &time_step_adjustment, "value")
                    .transform_from(|_, x| Some(ruler_log::ruler_exp(f64::round(x) as i32)))
                    .transform_to(|_, x| Some(ruler_log::ruler_log_round(x) as f64))
                    .bidirectional()
                    .sync_create()
                    .build();
            }
        ));

        // Simulation settings
        settings.append(&settings_heading("Simulation"));

        let simulation_list = gtk::ListBox::builder()
            .selection_mode(gtk::SelectionMode::None)
            .css_classes(["rich-list", "boxed-list"])
            .build();

        settings.append(&simulation_list);

        settings_append_row(
            &simulation_list,
            "Simulation Speed",
            "May be limited by CPU power",
            &gtk::Scale::builder()
                .adjustment(&simulation_speed_adjustment)
                .width_request(150)
                .build()
                .upcast(),
        );
        {
            let spin = gtk::SpinButton::builder()
                .adjustment(&time_step_adjustment)
                .width_request(150)
                .build();
            spin.connect_output(|spin| {
                let linear_value = ruler_log::ruler_exp(f64::round(spin.value()) as i32);
                spin.set_text(&format!(
                    "{}s",
                    simac_numfmt::si_prefix::WithSiPrefix(linear_value)
                ));
                glib::Propagation::Stop
            });
            spin.connect_input(|spin| {
                let text = spin.text();
                let Some(linear_value) =
                    simac_numfmt::si_prefix::parse_with_si_prefix(text.trim_end_matches('s'))
                else {
                    return Some(Err(()));
                };
                let log_value = ruler_log::ruler_log_round(linear_value);
                Some(Ok(log_value as f64))
            });
            settings_append_row(
                &simulation_list,
                "Maximum Time Step",
                "Lower values are more accurate but slower to simulate",
                &spin.upcast(),
            );
        }

        // Render setting values
        let potential_color_range_adjustment =
            gtk::Adjustment::new(0.25, 0.25, 1e4, 0.25, 1.0, 0.0);
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            potential_color_range_adjustment,
            move |_: &_, workspace: &workspace::Workspace| {
                workspace
                    .bind_property(
                        "potential-color-range",
                        &potential_color_range_adjustment,
                        "value",
                    )
                    .bidirectional()
                    .sync_create()
                    .build();
            }
        ));

        // Render settings
        settings.append(&settings_heading("Display"));

        let render_list = gtk::ListBox::builder()
            .selection_mode(gtk::SelectionMode::None)
            .css_classes(["rich-list", "boxed-list"])
            .build();

        settings.append(&render_list);

        settings_append_row(
            &render_list,
            "Current Speed",
            "Controls the animation speed of electric currents",
            &gtk::Scale::builder()
                .adjustment(&current_speed_adjustment)
                .width_request(150)
                .build()
                .upcast(),
        );
        {
            let spin = gtk::SpinButton::builder()
                .adjustment(&potential_color_range_adjustment)
                .width_request(150)
                .build();
            spin.connect_output(|spin| {
                spin.set_text(&format!(
                    "±{}V",
                    simac_numfmt::si_prefix::WithSiPrefix(spin.value())
                ));
                glib::Propagation::Stop
            });
            spin.connect_input(|spin| {
                let text = spin.text();
                let Some(value) = simac_numfmt::si_prefix::parse_with_si_prefix(
                    text.trim_end_matches('V').trim_start_matches('±'),
                ) else {
                    return Some(Err(()));
                };
                Some(Ok(value))
            });
            settings_append_row(
                &render_list,
                "Voltage Coloring Range",
                "Maximum/minimum voltage to the ground to visualize",
                &spin.upcast(),
            );
        }

        // Settings popover
        let settings_popover = gtk::Popover::builder().child(&settings).build();

        // Simulation status and actions
        // ------------------------------------------------------------------

        let simulation_actions = gtk::ActionBar::new();

        let simulation_run = gtk::ToggleButton::builder()
            .icon_name("play-symbolic")
            .tooltip_text("Run Simulation")
            .build();
        let simulation_stop = gtk::ToggleButton::builder()
            .icon_name("pause-symbolic")
            .tooltip_text("Pause Simulation")
            .group(&simulation_run)
            .build();
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            simulation_run,
            #[strong]
            simulation_stop,
            move |_: &_, workspace: &workspace::Workspace| {
                workspace
                    .bind_property("simulation-active", &simulation_run, "active")
                    .bidirectional()
                    .sync_create()
                    .build();
                workspace
                    .bind_property("simulation-active", &simulation_stop, "active")
                    .bidirectional()
                    .invert_boolean()
                    .sync_create()
                    .build();
            }
        ));

        let simulation_control = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        simulation_control.add_css_class("linked");

        simulation_control.append(&simulation_run);
        simulation_control.append(&simulation_stop);

        simulation_actions.pack_start(&simulation_control);

        // Simulation error popover
        let simulation_error_description = gtk::Label::builder()
            .label(indoc! {"
                <b>The solver was unable to find the initial or next state \
                of the circuit.</b>
                This can be caused by a number of reasons, including:
                 • Loop with zero resistance
                 • Floating circuit (not connected to any voltage reference, \
                 such as the ground or a single-terminal voltage source)
                 • Too large simulation timestep
                 • Too large voltage or current\
            "})
            .wrap(true)
            .use_markup(true)
            .max_width_chars(60)
            .build();
        let simulation_error_popover = gtk::Popover::builder()
            .child(&simulation_error_description)
            .build();

        let simulation_error_icon = gtk::MenuButton::builder()
            .tooltip_text("Simulation Error")
            .icon_name("warning-symbolic")
            .css_classes(["error"])
            .popover(&simulation_error_popover)
            .build();
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            simulation_error_icon,
            move |_: &_, workspace: &workspace::Workspace| {
                // Not using `visible` to maintain a stable layout
                workspace
                    .bind_property("simulation-aborted", &simulation_error_icon, "sensitive")
                    .sync_create()
                    .build();
                workspace
                    .bind_property("simulation-aborted", &simulation_error_icon, "can-target")
                    .sync_create()
                    .build();
                workspace
                    .bind_property("simulation-aborted", &simulation_error_icon, "opacity")
                    .transform_to(|_, x: bool| Some(x as u8 as f64))
                    .sync_create()
                    .build();
            }
        ));
        simulation_actions.pack_start(&simulation_error_icon);

        let simulation_speed_actual = gtk::Label::builder()
            .tooltip_text("Effective Simulation Speed")
            .css_classes(["numeric"])
            .width_chars(13)
            .xalign(1.0)
            .build();
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            simulation_speed_actual,
            move |_: &_, workspace: &workspace::Workspace| {
                workspace
                    .bind_property("time-scale-actual", &simulation_speed_actual, "label")
                    .transform_to(|_, time_scale: f64| {
                        Some(if time_scale == 0.0 {
                            "Stopped".to_owned()
                        } else {
                            format!(
                                "{:.2}s/s",
                                simac_numfmt::si_prefix::WithSiPrefix(time_scale)
                            )
                        })
                    })
                    .sync_create()
                    .build();
            }
        ));
        simulation_actions.pack_start(&simulation_speed_actual);

        simulation_actions.pack_start(
            &gtk::Scale::builder()
                .adjustment(&simulation_speed_adjustment)
                .hexpand(true)
                .tooltip_text("Simulation Speed")
                .build(),
        );

        simulation_actions.pack_end(
            &gtk::MenuButton::builder()
                .icon_name("settings-symbolic")
                .tooltip_text("Settings")
                .popover(&settings_popover)
                .build(),
        );

        simulation_actions.set_sensitive(false);
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            simulation_actions,
            move |_: &_, _: &workspace::Workspace| simulation_actions.set_sensitive(true),
        ));

        // Main
        // ------------------------------------------------------------------

        let schematic = schematic::SchematicArea::default();
        _ = self.schematic.set(schematic.clone());
        schematic.set_hexpand(true);
        schematic.set_vexpand(true);
        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            schematic,
            move |_: &_, workspace: &workspace::Workspace| schematic
                .set_workspace(Some(workspace.clone())),
        ));

        let schematic_scroll = gtk::ScrolledWindow::builder().child(&schematic).build();

        let spinner = gtk::Spinner::builder().spinning(true).build();
        let spinner_bind = self
            .obj()
            .bind_property("workspace-load-in-progress", &spinner, "visible")
            .sync_create()
            .build();

        let header_title = libadwaita::WindowTitle::builder().title("Loading…").build();

        let header_with_spinner = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        header_with_spinner.append(&spinner);
        header_with_spinner.append(&header_title);

        let header_main = libadwaita::HeaderBar::builder()
            .title_widget(&header_with_spinner)
            .build();

        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            header_title,
            #[strong]
            spinner,
            move |_: &_, workspace: &workspace::Workspace| {
                gtk::ClosureExpression::with_callback(
                    [
                        gtk::PropertyExpression::new(
                            workspace::Workspace::static_type(),
                            gtk::Expression::NONE,
                            "main-file",
                        ),
                        gtk::PropertyExpression::new(
                            workspace::Workspace::static_type(),
                            gtk::Expression::NONE,
                            "save-busy",
                        ),
                    ],
                    |values| -> glib::GString {
                        let file: Option<gio::File> = values[1].get().unwrap();
                        let busy: bool = values[2].get().unwrap();

                        if busy {
                            return glib::gstr!("Saving…").into();
                        }

                        match file.and_then(|file| file.basename()) {
                            Some(basename) => glib::gformat!("{}", basename.display()),
                            None => glib::gstr!("New Workspace").into(),
                        }
                    },
                )
                .bind(&header_title, "title", Some(workspace));
                workspace
                    .bind_property("main-file", &header_title, "subtitle")
                    .transform_to(|_, file: Option<gio::File>| {
                        let path = file
                            .and_then(|file| file.parent())
                            .and_then(|file| file.path());
                        Some(match path {
                            Some(path) => fmt_path(&path),
                            None => glib::gstr!("Draft").into(),
                        })
                    })
                    .sync_create()
                    .build();

                // Spin it when saving
                spinner_bind.unbind();
                workspace
                    .bind_property("save-busy", &spinner, "visible")
                    .sync_create()
                    .build();
            }
        ));

        gtk::ClosureExpression::with_callback(
            [
                gtk::PropertyExpression::new(
                    libadwaita::WindowTitle::static_type(),
                    gtk::Expression::NONE,
                    "title",
                ),
                gtk::PropertyExpression::new(
                    libadwaita::WindowTitle::static_type(),
                    gtk::Expression::NONE,
                    "subtitle",
                ),
            ],
            |values| {
                let title: &str = values[1].get().unwrap();
                let subtitle: &str = values[2].get().unwrap();
                let mut window_title = if subtitle.is_empty() {
                    title.to_owned()
                } else {
                    format!("{title} ({subtitle})")
                };
                window_title.push_str(" - SimAC");
                window_title
            },
        )
        .bind(&*self.obj(), "title", Some(&header_title));

        let show_left_sidebar = gtk::ToggleButton::builder()
            .icon_name("sidebar-show-symbolic")
            .tooltip_text("Toggle Left Panel")
            .build();
        header_main.pack_start(&show_left_sidebar);

        let menu_button = gtk::MenuButton::builder()
            .icon_name("open-menu-symbolic")
            .primary(true)
            .menu_model(&main_menu)
            .tooltip_text("Main Menu")
            .build();
        header_main.pack_end(&menu_button);

        let show_right_sidebar = gtk::ToggleButton::builder()
            .icon_name("sidebar-show-right-symbolic")
            .tooltip_text("Toggle Right Panel")
            .build();
        header_main.pack_end(&show_right_sidebar);

        let main_overlay = libadwaita::ToastOverlay::new();
        main_overlay.set_child(Some(&schematic_scroll));

        let main_pane = libadwaita::ToolbarView::builder()
            .content(&main_overlay)
            .build();
        main_pane.add_top_bar(&header_main);
        main_pane.add_bottom_bar(&simulation_actions);

        // Simulation error notification
        // ------------------------------------------------------------------

        register_on_workspace_ready_success!(glib::clone!(
            #[strong]
            main_overlay,
            move |this: &MainWindow, workspace: &workspace::Workspace| {
                let simulation_active = Cell::new(workspace.simulation_active());
                let last_toast = Cell::new(None::<(libadwaita::Toast, Instant)>);

                let main_overlay = main_overlay.clone();
                let signal_handler_id =
                    workspace.connect_simulation_active_notify(move |workspace| {
                        tracing::trace!("Received `notify::simulation-active`");
                        if !simulation_active.neq_assign(workspace.simulation_active()) {
                            // Ignore the signal if no change
                            tracing::trace!("`simulation-active` did not change");
                            return;
                        }

                        // If `simulation-active` changed to `false` and
                        // `simulation-aborted` is `true`...
                        if !simulation_active.get() && workspace.simulation_aborted() {
                            tracing::debug!(
                                "Displaying a toast notification because \
                                simulation was paused due to error"
                            );

                            // Dismiss an existing toast notification of the
                            // same type
                            if let Some((toast, time)) = last_toast.take() {
                                if time.elapsed().as_secs() < 1 {
                                    tracing::debug!(
                                        "Too soon since the last notification; \
                                        not presenting new one"
                                    );
                                    last_toast.set(Some((toast, time)));
                                    return;
                                }
                                toast.dismiss();
                            }

                            // Display a toast notification
                            let toast = libadwaita::Toast::new("Simulation paused due to error");
                            last_toast.set(Some((toast.clone(), Instant::now())));
                            main_overlay.add_toast(toast);
                        }
                    });
                let signal_handler_id = Cell::new(Some(signal_handler_id));
                this.connect_destroy(glib::clone!(
                    #[strong]
                    workspace,
                    move |_| {
                        if let Some(signal_handler_id) = signal_handler_id.take() {
                            workspace.disconnect(signal_handler_id);
                        }
                    }
                ));
            }
        ));

        // Left sidebar
        // ------------------------------------------------------------------

        // Search
        let search_entry = gtk::SearchEntry::builder()
            .hexpand(true)
            .placeholder_text("Search devices")
            .build();

        let search_bar = gtk::SearchBar::builder().child(&search_entry).build();

        // Header
        let header1 = libadwaita::HeaderBar::builder()
            .title_widget(&libadwaita::WindowTitle::builder().title("Toolbox").build())
            .build();

        let search_button = gtk::ToggleButton::builder()
            .icon_name("edit-find-symbolic")
            .tooltip_text("Search")
            .build();
        header1.pack_start(&search_button);

        search_button
            .bind_property("active", &search_bar, "search-mode-enabled")
            .bidirectional()
            .sync_create()
            .build();

        // Toolbox
        let toolbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        toolbox.add_css_class("toolbar");

        let bind_tool = |button: &gtk::ToggleButton, tool: workspace::Tool| {
            button.set_action_name(Some("win.tool-switch"));
            button.set_action_target(Some(tool.to_variant()));
            schematic
                .bind_property("tool", button, "active")
                .transform_to(move |_, x: workspace::Tool| Some(x == tool))
                .sync_create()
                .build();
        };

        let tool_displace = gtk::ToggleButton::builder()
            .icon_name("move-tool-symbolic")
            .tooltip_markup(
                "<b>Move Tool</b>\n\
                Move items, detaching and reattaching them \
                to the rest of the circuit at the new position",
            )
            .hexpand(true)
            .build();
        let tool_move = gtk::ToggleButton::builder()
            .icon_name("transform-symbolic")
            .tooltip_markup(
                "<b>Arrange Tool</b>\n\
                Move items, preserving connections",
            )
            .group(&tool_displace)
            .hexpand(true)
            .build();
        bind_tool(&tool_displace, workspace::Tool::Displace);
        bind_tool(&tool_move, workspace::Tool::Move);

        let tools_move = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        tools_move.add_css_class("linked");
        tools_move.append(&tool_displace);
        tools_move.append(&tool_move);

        toolbox.append(&tools_move);

        let tool_wire = gtk::ToggleButton::builder()
            .icon_name("draw-line-symbolic")
            .tooltip_markup(
                "<b>Wire Tool</b>\n\
                Add a wire segment",
            )
            .group(&tool_displace)
            .css_classes(["raised"])
            .build();
        bind_tool(&tool_wire, workspace::Tool::Wire);
        toolbox.append(&tool_wire);

        // Available devices
        let device_list = gtk::ListView::builder().build();

        // Sidebar
        let sidebar1 = libadwaita::ToolbarView::builder()
            .content(&device_list)
            .build();
        sidebar1.add_top_bar(&header1);
        sidebar1.add_top_bar(&toolbox);
        sidebar1.add_top_bar(&search_bar);

        let split1 = libadwaita::OverlaySplitView::builder()
            .content(&main_pane)
            .sidebar(&sidebar1)
            .build();

        split1
            .bind_property("show-sidebar", &show_left_sidebar, "active")
            .bidirectional()
            .sync_create()
            .build();

        // Right sidebar
        // ------------------------------------------------------------------

        let inspector_pane = libadwaita::StatusPage::builder()
            .title("No Selection")
            .description("Select components to edit their properties")
            .icon_name("wrench-wide-symbolic")
            .css_classes(["compact"])
            .build();

        // TODO: Display scopes below

        let header2 = libadwaita::HeaderBar::builder().show_title(false).build();
        // TODO: Set `tooltip_text`
        header2.pack_start(
            &gtk::Button::builder()
                .icon_name("edit-undo")
                .sensitive(false)
                .action_name("win.undo")
                .build(),
        );
        header2.pack_start(
            &gtk::Button::builder()
                .icon_name("edit-redo")
                .sensitive(false)
                .action_name("win.redo")
                .build(),
        );

        let sidebar2 = libadwaita::ToolbarView::builder()
            .content(&inspector_pane)
            .build();
        sidebar2.add_top_bar(&header2);

        let split2 = libadwaita::OverlaySplitView::builder()
            .content(&split1)
            .sidebar_position(gtk::PackType::End)
            .sidebar(&sidebar2)
            .build();

        split2
            .bind_property("show-sidebar", &show_right_sidebar, "active")
            .bidirectional()
            .sync_create()
            .build();

        // ------------------------------------------------------------------

        // Disable saving if already saving
        register_on_workspace_ready_success!(
            |this: &MainWindow, workspace: &workspace::Workspace| {
                let signal_handler_id = workspace.connect_save_busy_notify(glib::clone!(
                    #[weak]
                    this,
                    move |workspace| {
                        let enable = !workspace.save_busy();
                        this.action_set_enabled("win.save-workspace", enable);
                        this.action_set_enabled("win.save-as-workspace", enable);
                    }
                ));
                let signal_handler_id = Cell::new(Some(signal_handler_id));
                this.connect_destroy(glib::clone!(
                    #[strong]
                    workspace,
                    move |_| {
                        if let Some(signal_handler_id) = signal_handler_id.take() {
                            workspace.disconnect(signal_handler_id);
                        }
                    }
                ));
            }
        );

        // Enable undo/redo actions
        self.obj().action_set_enabled("win.undo", false);
        self.obj().action_set_enabled("win.redo", false);
        register_on_workspace_ready_success!(
            |this: &MainWindow, workspace: &workspace::Workspace| {
                let sync = |this: &MainWindow, workspace: &workspace::Workspace| {
                    this.action_set_enabled("win.undo", workspace.can_undo());
                    this.action_set_enabled("win.redo", workspace.can_redo());
                };
                let signal_handler_id1 = workspace.connect_can_undo_notify(glib::clone!(
                    #[weak]
                    this,
                    move |workspace| sync(&this, workspace),
                ));
                let signal_handler_id2 = workspace.connect_can_redo_notify(glib::clone!(
                    #[weak]
                    this,
                    move |workspace| sync(&this, workspace),
                ));
                let signal_handler_id = Cell::new(Some([signal_handler_id1, signal_handler_id2]));
                this.connect_destroy(glib::clone!(
                    #[strong]
                    workspace,
                    move |_| {
                        if let Some([signal_handler_id1, signal_handler_id2]) =
                            signal_handler_id.take()
                        {
                            workspace.disconnect(signal_handler_id1);
                            workspace.disconnect(signal_handler_id2);
                        }
                    }
                ));
            }
        );

        // ------------------------------------------------------------------

        self.obj().set_content(Some(&split2));

        // Start loading a `Workspace`
        match self
            .workspace_source
            .take()
            .expect("workspace source not provided")
        {
            WorkspaceSource::Concrete(workspace) => self.on_workspace_ready(Ok(workspace)),
            WorkspaceSource::File(file) => {
                let this = self.downgrade();
                let signal_handler_id_cell = Rc::new(Cell::new(None));

                // Start a task
                let task = glib::clone!(
                    #[strong]
                    signal_handler_id_cell,
                    async move {
                        let workspace = workspace::Workspace::new_from_gio_file(file).await;

                        let Some(this) = this.upgrade() else {
                            tracing::debug!("The parent `MainWindow` is gone, aborting the task");
                            return;
                        };

                        // FIXME: Error message could be more user-friendly
                        this.on_workspace_ready(workspace.map_err(|e| e.to_string()));

                        if let Some(signal_handler_id) = signal_handler_id_cell.take() {
                            this.obj().disconnect(signal_handler_id);
                        }
                    }
                );
                let task = glib::spawn_future_local(
                    task.instrument(tracing::info_span!("`WorkspaceSource::File` loader")),
                );
                let task = Cell::new(Some(task));

                // Cancel the task on `destroy` signal
                let signal_handler_id = self.obj().connect_destroy(move |_| {
                    let Some(task) = task.take() else { return };

                    // Cancel the loading task if the window is destroyed
                    tracing::debug!(
                        source = ?task.source(),
                        "Aborting the file loading task because the window is being destroyed",
                    );
                    task.abort()
                });
                signal_handler_id_cell.set(Some(signal_handler_id));
            }
        }

        self.parent_constructed();
    }
}

impl WidgetImpl for imp::MainWindow {}

impl WindowImpl for imp::MainWindow {}

impl ApplicationWindowImpl for imp::MainWindow {}

impl AdwApplicationWindowImpl for imp::MainWindow {}

impl imp::MainWindow {
    /// The setter for `initial-workspace` construct-only property.
    fn set_initial_workspace(&self, workspace: Option<workspace::Workspace>) {
        let Some(workspace) = workspace else { return };
        self.workspace_source
            .set(Some(WorkspaceSource::Concrete(workspace)));
    }

    /// The setter for `initial-workspace-file` construct-only property.
    fn set_initial_workspace_file(&self, file: Option<gio::File>) {
        let Some(file) = file else { return };
        self.workspace_source.set(Some(WorkspaceSource::File(file)));
    }

    fn on_workspace_ready(&self, workspace: Result<workspace::Workspace, String>) {
        self.workspace_load_in_progress.set(false);
        self.obj().notify_workspace_load_in_progress();

        match workspace {
            Ok(workspace) => {
                self.workspace
                    .set(workspace)
                    .expect("workspace is already set");
                self.obj().notify_workspace();
            }
            Err(error) => {
                // Display an error dialog
                let this = self.obj();
                let dialog = libadwaita::MessageDialog::new(
                    Some(&*this),
                    Some("Could not open workspace"),
                    Some(&error),
                );
                dialog.add_responses(&[("", "_Close")]);
                dialog.set_default_response(Some(""));
                dialog.choose(
                    gio::Cancellable::NONE,
                    glib::clone!(
                        #[weak]
                        this,
                        move |_| this.close(),
                    ),
                );
            }
        }
    }

    #[tracing::instrument(skip_all)]
    fn save(&self) {
        let Some(workspace) = self.workspace.get() else {
            return tracing::info!("Workspace is not set yet; ignoring command");
        };

        match self.save_inner(workspace, None) {
            Ok(()) => {}
            Err(workspace::StartSaveError::NoFile) => self.save_as(),
            Err(workspace::StartSaveError::AlreadyInProgress) => {
                tracing::info!("Save operation is already in progress")
            }
        }
    }

    #[tracing::instrument(skip_all)]
    fn save_as(&self) {
        let Some(workspace) = self.workspace.get() else {
            return tracing::info!("Workspace is not set yet; ignoring command");
        };

        let this = self.obj();
        let dialog = gtk::FileDialog::builder()
            .title("Save Workspace As")
            .build();
        if let Some(file) = workspace.main_file() {
            dialog.set_initial_file(Some(&file));
        } else {
            let name = glib::DateTime::now_local()
                .and_then(|now| now.format("%Y-%m-%d %H-%M-%S.simac"))
                .ok();
            dialog.set_initial_name(Some(name.as_deref().unwrap_or("circuit.simac")));
            // TODO: Show last save directory by default
        }
        dialog.save(
            Some(&*this),
            gio::Cancellable::NONE,
            glib::clone!(
                #[weak]
                this,
                #[weak]
                workspace,
                move |result| {
                    let Ok(file) = result else { return };
                    match this.imp().save_inner(&workspace, Some(file)) {
                        Ok(()) => {}
                        Err(workspace::StartSaveError::NoFile) => unreachable!(),
                        Err(workspace::StartSaveError::AlreadyInProgress) => {
                            tracing::info!("Save operation is already in progress")
                        }
                    }
                }
            ),
        );
    }

    #[tracing::instrument(skip_all)]
    fn save_inner(
        &self,
        workspace: &workspace::Workspace,
        new_main_file: Option<gio::File>,
    ) -> Result<(), workspace::StartSaveError> {
        tracing::debug!(?new_main_file);
        let task = workspace.save(new_main_file)?;
        glib::spawn_future_local(task);
        Ok(())
    }
}
