//! Simulation
//!
//! # Device output variables
//!
//! The simuluation produces *device output variables* for every device
//! instance present in the provided circuit.
//!
//! Every device's output variable list starts with the set of basic variables
//! specific to each device type as shown below:
//!
//! - [`circuit::SubcircuitDevice`]: The outbound current of each
//!   terminal in [`circuit::Device::terminals`].
//!
//! - [`circuit::TerminalDevice`]: The outbound current.
//!
//! - [`circuit::ElementDevice`][]: None.
//!
//! - [`circuit::WireDevice`]: The current flowing from terminal `0` to
//!   terminal `1`.
use bevy_ecs::{entity::EntityHashMap, prelude::*};
use slotmap::SecondaryMap;
use std::{
    mem::take,
    time::{Duration, Instant},
};

use crate::flat::{FlatDeviceI, FlatNodeI};

#[cfg(doc)]
use crate::circuit;

#[cfg(doc)]
use crate::flat;

mod export;
pub use export::export_subcircuit_view;
mod sync;
pub use sync::sync_circuit;

#[cfg(test)]
mod tests;

/// Simulation settings.
#[derive(Component, Debug)]
pub struct SimParams {
    /// The simulation time step.
    pub time_step: f64,
    /// The minimum simulation time step.
    /// Set to the same value as [`Self::time_step`] to disable adaptive
    /// (variable) time step control.
    pub min_time_step: f64,
    /// The maximum number of full time steps per simulation system execution.
    pub max_num_steps: u64,
    /// The time limit for simulation iteration.
    pub timeout: Duration,
}

/// General information about simulation results,
/// including the actual number of time steps.
///
/// Automatically added to entities with [`SimParams`].
#[derive(Component, Debug)]
pub struct SimStatus {
    /// The simulation time elapsed during the last update.
    /// The maximum possible value is the product of [`SimParams::time_step`]
    /// and [`SimParams::max_num_steps`].
    pub time_delta: f64,
    /// The time took to advance the simulation by [`Self::time_delta`].
    pub elapsed: Duration,
}

#[derive(Component, Debug)]
pub struct SimSubcircuitViewParams {
    /// The path to the subcircuit device instance to render the overview of.
    ///
    /// The empty path specifies to render the overview of the root circuit
    /// ([`flat::Flattened::root_circuit`]).
    pub path: Vec<Entity>,
}

#[derive(Component, Debug)]
pub struct SimSubcircuitViewOutput {
    /// `Err(_)` if an error has been detected during the last export.
    pub result: Result<(), SimSubcircuitViewError>,
    /// Each node's electric potential.
    pub nodes: EntityHashMap<f64>,
    /// The starting index in [`Self::device_vars`] for each device's [output
    /// variables][1].
    ///
    /// [1]: self#device-output-variables
    pub device_vars_start: EntityHashMap<usize>,
    /// All [device output variables][1].
    ///
    /// [1]: self#device-output-variables
    pub device_vars: Vec<f64>,
}

#[derive(Debug, Clone, thiserror::Error, PartialEq)]
pub enum SimSubcircuitViewError {
    #[error("unable to locate simulation setting entity {0:?}")]
    SimNotFound(Entity),
    #[error("unable to locate flattened circuit entity {0:?}")]
    FlattenedNotFound(Entity),
    #[error("unable to locate subcircuit with path {0:?}")]
    CircuitNotFound(Vec<Entity>),
}

// Simulation state
// -------------------------------------------------------------------------

/// Encapsulates a simulation state.
#[derive(Component, Debug)]
pub struct SimState {
    world: Box<dyn simac_simulate::WorldCore>,
    /// Indicates whether [`step_simulation`] should calculate the new
    /// operating point on next call.
    ///
    /// It should be set when [`Self::world`] is modified.
    needs_update: bool,
    devices: SecondaryMap<FlatDeviceI, SimDevice>,
    nodes: SecondaryMap<FlatNodeI, SimNode>,
}

/// An instantiated device (circuit element) in [`SimState`].
#[derive(Debug)]
struct SimDevice {
    elem_id: Option<simac_simulate::ElemId>,
    /// If this is a `Terminal` or `Wire` device, this contains the node
    /// representing the branch current. This node does not have a corresponding
    /// [`SimNode`] or [`FlatNodeI`].
    current_node_id: Option<simac_simulate::NodeId>,
}

/// An instantiated node in [`SimState`].
#[derive(Debug)]
struct SimNode {
    node_id: simac_simulate::NodeId,
}

/// Update a simulation based on its simulation settings ([`SimParams`]).
pub fn step_simulation(
    sim_params: &SimParams,
    sim_state: &mut SimState,
) -> Result<SimStatus, simac_simulate::StepError> {
    // Find the new operating point first
    if take(&mut sim_state.needs_update) {
        let start = Instant::now();
        if let Err(error) = sim_state
            .world
            .step(0.0, 0.0, &simac_simulate::SimParams::default())
        {
            sim_state.needs_update = true;
            tracing::debug!(
                elapsed = ?start.elapsed(),
                %error,
                "Failed to calculate new operating point",
            );
            return Err(error);
        }
        tracing::debug!(elapsed = ?start.elapsed(), "Calculated new operating point");
    } else {
        tracing::trace!("Skipping operating point update");
    }

    let start = Instant::now();
    let mut num_steps = 0;
    let mut time_delta = 0.0;
    let max_time_delta = sim_params.time_step * sim_params.max_num_steps as f64;
    loop {
        // Find the next time step size
        let desired_time_step = (max_time_delta - time_delta).min(sim_params.time_step);
        if desired_time_step < sim_params.min_time_step {
            break;
        }

        num_steps += 1;

        match sim_state.world.step(
            desired_time_step,
            sim_params.min_time_step,
            &simac_simulate::SimParams::default(),
        ) {
            Ok(actual_time_step) => time_delta += actual_time_step,
            Err(error) => {
                let elapsed = start.elapsed();
                tracing::debug!(
                    num_steps,
                    ?elapsed,
                    %error,
                    "Failed to run a simulation step",
                );
                return Err(error);
            }
        }

        if start.elapsed() > sim_params.timeout {
            break;
        }
    }

    let elapsed = start.elapsed();

    tracing::debug!(
        num_steps,
        time_delta,
        ?elapsed,
        time_slice_usage = %format_args!(
            "{:.02}%",
            elapsed.as_secs_f64() / sim_params.timeout.as_secs_f64() * 100.0,
        ),
        "Ran simulation steps",
    );

    Ok(SimStatus {
        time_delta,
        elapsed,
    })
}
