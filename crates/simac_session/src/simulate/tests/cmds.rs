//! A [`proptest`]-based test case to provide randomly-changing sets of
//! components to [`crate::circuit`]
use proptest::prelude::*;
use proptest_derive::Arbitrary;
use simac_elements::ElemTy;

use super::*;
use crate::tests::{proptest_index_bevy_world_entity, proptest_index_iter};

proptest! {
    #[test]
    fn pt_cmds(cmds: Vec<Cmd>) {
        pt_cmds_inner(cmds);
    }
}

#[derive(Arbitrary, Debug)]
enum Cmd {
    Update,
    InsertCircuit,
    RemoveCircuit {
        circuit: prop::sample::Index,
    },
    InsertNode {
        parent_circuit: Option<prop::sample::Index>,
        ground: bool,
    },
    RemoveNode {
        node: prop::sample::Index,
    },
    ModifyNode {
        node: prop::sample::Index,
        parent_circuit: Option<prop::sample::Index>,
        // changing `ground` is not supported
    },
    InsertDevice {
        parent_circuit: Option<prop::sample::Index>,
        terminals: [prop::sample::Index; MAX_NUM_TERINALS],
        device: DeviceTy,
    },
    RemoveDevice {
        device: prop::sample::Index,
    },
    ModifyDevice {
        device: prop::sample::Index,
        parent_circuit: Option<prop::sample::Index>,
        terminals: [prop::sample::Index; MAX_NUM_TERINALS],
        // changing `device` is not supported
    },
}

#[derive(Arbitrary, Debug)]
enum DeviceTy {
    Subcircuit {
        circuit: prop::sample::Index,
    },
    Terminal {
        #[proptest(strategy = "any_terminal_i()")]
        terminal_i: usize,
    },
    Element {
        #[proptest(filter = "|ty| ty.num_nodes() <= MAX_NUM_TERINALS")]
        elem_ty: ElemTy,
    },
    Wire,
}

#[derive(Component)]
struct DontDelete;

#[derive(Component)]
struct Parent(Option<Entity>);

const MAX_NUM_TERINALS: usize = 4;

fn any_terminal_i() -> impl Strategy<Value = usize> {
    0..MAX_NUM_TERINALS
}

fn pt_cmds_inner(cmds: Vec<Cmd>) {
    crate::tests::init();

    let mut schedule = Schedule::default();
    schedule.add_systems((expr::system_configs_compile(), sys_update).chain());

    let mut world = World::default();

    crate::init_world(&mut world);

    // Create a circuit entity
    let ent_circuit = world
        .spawn((circuit::CircuitBundle::default(), DontDelete))
        .id();

    // Create a flattener
    let flatten = flat::Flatten {
        root_circuit_params: Vec::new(),
    };
    let flattened = flat::Flattened::new(ent_circuit);

    // Create a simulation
    let sim_params = SimParams {
        time_step: 1.0e-6,
        min_time_step: 1.0e-6,
        max_num_steps: 0,
        timeout: Duration::MAX,
    };
    let sim_state = SimState::default();

    let test_res = TestRes {
        flatten,
        flattened,
        sim_params,
        sim_state,
    };

    let sim_view_params = SimSubcircuitViewParams { path: vec![] };
    let mut sim_view = SimSubcircuitViewOutput::default();

    tracing::info!(
        ?ent_circuit,
        ?test_res,
        ?sim_view_params,
        "Created a circuit, flattener, simulation, and subcircuit view"
    );

    world.insert_resource(test_res);

    // Execute commands
    for cmd in cmds {
        match cmd {
            Cmd::Update => {
                tracing::info!("Updating simulation");
                schedule.run(&mut world);
            }

            Cmd::InsertCircuit => {
                let entity = world.spawn(circuit::CircuitBundle::default()).id();
                tracing::info!(?entity, "Spawned a circuit entity");
            }
            Cmd::RemoveCircuit { circuit } => {
                let Some(entity) = proptest_index_bevy_world_entity::<(
                    With<circuit::Circuit>,
                    Without<DontDelete>,
                )>(circuit, &mut world) else {
                    tracing::trace!("No circuits found, ignoring `RemoveCircuit`");
                    continue;
                };

                world.despawn(entity);

                tracing::info!(?entity, "Despawned a circuit entity");
            }

            Cmd::InsertNode {
                parent_circuit,
                ground,
            } => {
                let parent_circuit = parent_circuit.map(|circuit| {
                    proptest_index_bevy_world_entity::<With<circuit::Circuit>>(circuit, &mut world)
                        .unwrap()
                });

                let mut entity =
                    world.spawn((circuit::NodeBundle::default(), Parent(parent_circuit)));
                if ground {
                    entity.insert(circuit::GroundNode);
                }
                let entity = entity.id();

                if let Some(parent_circuit) = parent_circuit {
                    let mut circuit: Mut<circuit::Circuit> = world.get_mut(parent_circuit).unwrap();
                    circuit.nodes.insert(entity);
                }

                tracing::info!(?entity, ground, ?parent_circuit, "Spawned a node entity");
            }
            Cmd::RemoveNode { node } => {
                let Some(node) =
                    proptest_index_bevy_world_entity::<With<circuit::Node>>(node, &mut world)
                else {
                    tracing::trace!("No nodes found, ignoring `RemoveNode`");
                    continue;
                };

                if let Parent(Some(parent_circuit)) = *world.get(node).unwrap() {
                    if let Some(mut circuit) = world.get_mut::<circuit::Circuit>(parent_circuit) {
                        circuit.nodes.remove(&node);
                    }
                }

                world.despawn(node);

                tracing::info!(?node, "Despawned a circuit entity");
            }
            Cmd::ModifyNode {
                node,
                parent_circuit,
            } => {
                let Some(node) =
                    proptest_index_bevy_world_entity::<With<circuit::Node>>(node, &mut world)
                else {
                    tracing::trace!("No nodes found, ignoring `RemoveNode`");
                    continue;
                };
                let parent_circuit = parent_circuit.map(|circuit| {
                    proptest_index_bevy_world_entity::<With<circuit::Circuit>>(circuit, &mut world)
                        .unwrap()
                });

                // Reparent the entity
                let old_parent_circuit = std::mem::replace(
                    &mut world.get_mut::<Parent>(node).unwrap().0,
                    parent_circuit,
                );
                if let Some(circuit) = old_parent_circuit {
                    if let Some(mut circuit) = world.get_mut::<circuit::Circuit>(circuit) {
                        circuit.nodes.remove(&node);
                    }
                }
                if let Some(circuit) = parent_circuit {
                    if let Some(mut circuit) = world.get_mut::<circuit::Circuit>(circuit) {
                        circuit.nodes.insert(node);
                    }
                }

                tracing::info!(?node, ?parent_circuit, "Modified a circuit entity");
            }

            Cmd::InsertDevice {
                parent_circuit,
                terminals,
                device,
            } => {
                let parent_circuit = parent_circuit.map(|circuit| {
                    proptest_index_bevy_world_entity::<With<circuit::Circuit>>(circuit, &mut world)
                        .unwrap()
                });

                let Some(terminals) =
                    proptest_index_circuit_nodes(&mut world, &terminals, parent_circuit)
                else {
                    tracing::trace!("No nodes available, ignoring `InsertDevice`");
                    continue;
                };

                let num_terminals = match device {
                    DeviceTy::Subcircuit { .. } => 4,
                    DeviceTy::Terminal { .. } => 1,
                    DeviceTy::Element { elem_ty } => elem_ty.num_nodes(),
                    DeviceTy::Wire => 2,
                };

                let device_bundle = circuit::DeviceBundle {
                    device: circuit::Device {
                        terminals: terminals[..num_terminals].to_owned().into(),
                    },
                };

                let mut entity = match device {
                    DeviceTy::Subcircuit { circuit } => {
                        // Only choose circuits with greater IDs to avoid
                        // recursion, which are not handled well at the moment
                        let mut circuits = world.query_filtered::<Entity, With<circuit::Circuit>>();
                        let Some(circuit) = proptest_index_iter(
                            circuit,
                            circuits.iter(&world).filter(|&child| {
                                if let Some(parent) = parent_circuit {
                                    child > parent
                                } else {
                                    true
                                }
                            }),
                        ) else {
                            tracing::debug!("No child circuits available, ignoring `InsertDevice`");
                            continue;
                        };

                        world.spawn(circuit::SubcircuitDeviceBundle {
                            device: device_bundle,
                            subcircuit_device: circuit::SubcircuitDevice { circuit },
                        })
                    }
                    DeviceTy::Terminal { terminal_i } => {
                        world.spawn(circuit::TerminalDeviceBundle {
                            device: device_bundle,
                            terminal_device: circuit::TerminalDevice { terminal_i },
                        })
                    }
                    DeviceTy::Element { elem_ty } => world.spawn((
                        circuit::ElementDeviceBundle {
                            device: device_bundle,
                            element_device: circuit::ElementDevice { elem_ty },
                        },
                        expr::ElementDevice {
                            params: vec![], // TODO
                        },
                    )),
                    DeviceTy::Wire => world.spawn(circuit::WireDeviceBundle {
                        device: device_bundle,
                        wire: circuit::WireDevice,
                    }),
                };

                entity.insert(Parent(parent_circuit));
                let entity = entity.id();

                if let Some(parent_circuit) = parent_circuit {
                    let mut circuit: Mut<circuit::Circuit> = world.get_mut(parent_circuit).unwrap();
                    circuit.devices.insert(entity);
                }

                let entity = world.entity(entity);
                tracing::info!(
                    entity = ?entity.id(),
                    ?parent_circuit,
                    entity.terminals = ?entity.get::<circuit::Device>().unwrap().terminals,
                    entity.subcircuit = entity.get::<circuit::SubcircuitDevice>().map(debug),
                    entity.terminal = entity.get::<circuit::TerminalDevice>().map(debug),
                    entity.element = entity.get::<circuit::ElementDevice>().map(debug),
                    entity.wire = entity.get::<circuit::WireDevice>().map(debug),
                    "Spawned a device entity"
                );
            }
            Cmd::RemoveDevice { device } => {
                let Some(device) =
                    proptest_index_bevy_world_entity::<With<circuit::Device>>(device, &mut world)
                else {
                    tracing::trace!("No devices found, ignoring `RemoveDevice`");
                    continue;
                };

                if let Parent(Some(parent_circuit)) = *world.get(device).unwrap() {
                    if let Some(mut circuit) = world.get_mut::<circuit::Circuit>(parent_circuit) {
                        circuit.devices.remove(&device);
                    }
                }

                world.despawn(device);

                tracing::info!(?device, "Despawned a circuit entity");
            }
            Cmd::ModifyDevice {
                device,
                parent_circuit,
                terminals,
            } => {
                let Some(device) =
                    proptest_index_bevy_world_entity::<With<circuit::Device>>(device, &mut world)
                else {
                    tracing::trace!("No devices found, ignoring `ModifyDevice`");
                    continue;
                };

                let mut parent_circuit = parent_circuit.map(|circuit| {
                    proptest_index_bevy_world_entity::<With<circuit::Circuit>>(circuit, &mut world)
                        .unwrap()
                });

                let Some(terminals) =
                    proptest_index_circuit_nodes(&mut world, &terminals, parent_circuit)
                else {
                    tracing::debug!("No nodes available, ignoring `ModifyDevice`");
                    continue;
                };

                // Update `terminals`
                let mut comp_device = world.get_mut::<circuit::Device>(device).unwrap();
                let num_terminals = comp_device.terminals.len();
                comp_device.terminals = terminals[..num_terminals].to_owned().into();

                // Reparent the entity
                if world.get::<circuit::SubcircuitDevice>(device).is_some() {
                    tracing::trace!(
                        "We'll avoid reparenting a subcircuit device because \
                        we don't handle recursion very well atm"
                    );
                    parent_circuit = world.get::<Parent>(device).unwrap().0;
                }

                let old_parent_circuit = std::mem::replace(
                    &mut world.get_mut::<Parent>(device).unwrap().0,
                    parent_circuit,
                );
                if let Some(circuit) = old_parent_circuit {
                    if let Some(mut circuit) = world.get_mut::<circuit::Circuit>(circuit) {
                        circuit.devices.remove(&device);
                    }
                }
                if let Some(circuit) = parent_circuit {
                    if let Some(mut circuit) = world.get_mut::<circuit::Circuit>(circuit) {
                        circuit.devices.insert(device);
                    }
                }

                let terminals = &world.get::<circuit::Device>(device).unwrap().terminals;
                tracing::info!(
                    ?device,
                    ?parent_circuit,
                    ?terminals,
                    "Modified a circuit entity"
                );
            }
        } // match cmd
    } // for cmd

    // Check that `SimState` is consistent
    let TestRes {
        sim_params,
        sim_state,
        ..
    }: &TestRes = world.get_resource().unwrap();
    validate_sim(None, sim_params, sim_state)
        .with_context(|| {
            format!(
                "simulation state validation failed.\n\n\
                sim_params = {sim_params:#?}\n\n\
                sim_state = {sim_state:#?}\n"
            )
        })
        .expect("`SimState` should be consistent");

    // Call `validate_sim` with `world` only after the simulation state is
    // synchronized with the world
    schedule.run(&mut world);

    let TestRes {
        sim_params,
        sim_state,
        flattened,
        ..
    }: &TestRes = world.get_resource().unwrap();
    export_subcircuit_view(sim_state, flattened, &sim_view_params, &mut sim_view);
    validate_sim(Some(flattened), sim_params, sim_state)
        .with_context(|| {
            format!(
                "simulation state validation failed.\n\n\
                sim_params = {sim_params:#?}\n\n\
                sim_state = {sim_state:#?}\n"
            )
        })
        .expect("`SimState` should be consistent");
}

fn proptest_index_circuit_nodes(
    world: &mut World,
    indices: &[prop::sample::Index; MAX_NUM_TERINALS],
    parent_circuit: Option<Entity>,
) -> Option<[Entity; MAX_NUM_TERINALS]> {
    if let Some(parent_circuit) = parent_circuit {
        // Choose from the nodes in the circuit
        let nodes = &world.get::<circuit::Circuit>(parent_circuit).unwrap().nodes;
        let nodes: Vec<_> = nodes.iter().copied().collect();
        if nodes.is_empty() {
            None
        } else {
            Some(indices.map(|index| *index.get(&nodes)))
        }
    } else {
        // Choose from any nodes
        proptest_index_bevy_world_entity::<With<circuit::Node>>(indices[0], world)?;
        Some(indices.map(|index| {
            proptest_index_bevy_world_entity::<With<circuit::Node>>(index, world).unwrap()
        }))
    }
}
