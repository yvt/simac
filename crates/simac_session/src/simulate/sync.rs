//! Systems to synchronize the circuit entities and the simulation environment
use super::*;
use crate::flat::{FlatDevice, Flattened};

impl Default for SimState {
    fn default() -> Self {
        Self {
            world: <Box<
                simac_simulate::layer_autoground::AutoGround<
                    simac_simulate::core_host::WorldCoreImpl,
                >,
            >>::default(),
            needs_update: false,
            devices: Default::default(),
            nodes: Default::default(),
        }
    }
}

/// Synchronize the simulation world with a given [`Flattened`].
///
/// The call has no effect if `flattened` hasn't changed since the last call
/// for `sim_state`.
#[tracing::instrument(level = "debug", skip_all)]
pub fn sync_circuit(sim_state: &mut SimState, flattened: &Flattened) {
    // Remove device instances that are no longer valid
    sim_state.devices.retain(|device_i, sim_device| {
        if flattened.devices.contains_key(device_i) {
            return true;
        }
        tracing::debug!(?device_i, "Removing device");

        if let Some(elem_id) = sim_device.elem_id {
            sim_state.world.remove_elem(elem_id);
            sim_state.needs_update = true;
        }

        if let Some(node_id) = sim_device.current_node_id {
            sim_state.world.remove_node(node_id);
            sim_state.needs_update = true;
        }

        false
    });

    // Remove node instances that are no longer valid
    //
    // Do this after removing devices because removing nodes that are still
    // referenced by a device is illegal.
    sim_state.nodes.retain(|node_i, sim_node| {
        if flattened.nodes.contains_key(node_i) {
            return true;
        }
        tracing::debug!(?node_i, "Removing node");

        if sim_node.node_id != simac_simulate::NodeId::GROUND {
            sim_state.world.remove_node(sim_node.node_id);
            sim_state.needs_update = true;
        }

        false
    });

    // Instantiate new nodes
    for (node_i, flat_node) in flattened.nodes.iter() {
        let slotmap::secondary::Entry::Vacant(entry) = sim_state.nodes.entry(node_i).unwrap()
        else {
            continue;
        };

        let flat_node_def = &flattened.node_defs[flat_node.node_def_i];

        // Allocate a `simac_simulate::NodeId`
        let node_id = if flat_node_def.is_ground {
            simac_simulate::NodeId::GROUND
        } else {
            sim_state.world.insert_node()
        };

        entry.insert(SimNode { node_id });

        sim_state.needs_update = true;

        tracing::debug!(
            node_def_i = ?flat_node.node_def_i,
            ent_node = ?flat_node_def.entity,
            ?node_i,
            ?node_id,
            "Instantiated a node",
        );
    }

    // Instantiate new devices
    //
    // Do this after instantiating nodes because referencing non-existent
    // nodes is illegal.
    let mut cx = InstantiateDeviceCx {
        world: &mut *sim_state.world,
        nodes: &sim_state.nodes,
        node_ids: Vec::with_capacity(4),
        states: Vec::with_capacity(4),
    };
    for (device_i, flat_device) in flattened.devices.iter() {
        let slotmap::secondary::Entry::Vacant(entry) = sim_state.devices.entry(device_i).unwrap()
        else {
            continue;
        };

        entry.insert(cx.instantiate_device(flat_device));

        sim_state.needs_update = true;

        tracing::debug!(
            device_def_i = ?flat_device.device_def_i,
            ent_device = ?flattened.device_defs[flat_device.device_def_i].entity,
            ?device_i,
            elem_ty = ?flat_device.elem_ty,
            "Instantiated a device"
        );
    }
}

struct InstantiateDeviceCx<'a> {
    world: &'a mut dyn simac_simulate::WorldCore,
    nodes: &'a SecondaryMap<FlatNodeI, SimNode>,
    /// A temporary storage used by [`Self::instantiate_device`].
    node_ids: Vec<simac_simulate::NodeId>,
    /// A temporary storage used by [`Self::instantiate_device`].
    states: Vec<f64>,
}

impl InstantiateDeviceCx<'_> {
    fn instantiate_device(&mut self, flat_device: &FlatDevice) -> SimDevice {
        // Convert `FlatDevice::terminal_nodes` to `[NodeId]`.
        let node_ids = &mut self.node_ids;
        node_ids.clear();
        node_ids.extend(
            flat_device
                .terminal_nodes
                .iter()
                .map(|&node_i| self.nodes[node_i].node_id),
        );

        let current_node_id = (flat_device.elem_ty == Some(simac_elements::ElemTy::Wire)
            && node_ids.len() == 2)
            .then(|| {
                // Create an extra node for branch current
                let node_id = self.world.insert_node();
                node_ids.push(node_id);
                node_id
            });

        tracing::trace!(
            ?node_ids,
            "The terminals of the device we're about to instantiate"
        );

        // Create a circuit element
        let elem_id = flat_device.elem_ty.map(|elem_ty| {
            let num_states = elem_ty.num_states();
            if self.states.len() < num_states {
                self.states.resize(num_states, 0.0);
            }
            self.world
                .insert_elem(elem_ty, &self.states[..num_states], node_ids)
        });

        if let Some(elem_id) = elem_id {
            for (i, &param) in flat_device.elem_params.iter().enumerate() {
                self.world.set_elem_param(elem_id, i, param);
            }
        }

        SimDevice {
            elem_id,
            current_node_id,
        }
    }
}
