//! Systems to export a subset of simulation results
use super::*;
use crate::flat::{FlatDeviceDefTy, Flattened};

impl Default for SimSubcircuitViewOutput {
    fn default() -> Self {
        SimSubcircuitViewOutput {
            result: Ok(()),
            nodes: Default::default(),
            device_vars_start: Default::default(),
            device_vars: Default::default(),
        }
    }
}

/// Export a simulation state to [`SimSubcircuitViewOutput`].
///
/// This has no effect if `sim_state` and `view_params` have not changed since
/// the last call.
#[tracing::instrument(level = "debug", skip_all)]
pub fn export_subcircuit_view(
    sim_state: &SimState,
    flattened: &Flattened,
    view_params: &SimSubcircuitViewParams,
    view_out: &mut SimSubcircuitViewOutput,
) {
    view_out.nodes.clear();
    view_out.device_vars_start.clear();
    view_out.device_vars.clear();

    // Locate the `SimCircuit` specified by `view_params.path`
    let mut circuit_i = flattened.root_circuit_i;
    for &ent_device in &view_params.path {
        let Some(&device_def_i) = flattened.device_def_map.get(&ent_device) else {
            tracing::debug!(
                ?ent_device,
                "Unable to locate the `SimDeviceDef` for a device entity from `path`"
            );
            view_out.result = Err(SimSubcircuitViewError::CircuitNotFound(
                view_params.path.clone(),
            ));
            return;
        };
        let Some(&next_circuit_i) = flattened.circuit_map.get(&(circuit_i, device_def_i)) else {
            tracing::debug!(
                ?ent_device,
                parent_circuit_i = ?circuit_i,
                ?device_def_i,
                "Unable to locate the `SimCircuit` for a device entity from `path`"
            );
            view_out.result = Err(SimSubcircuitViewError::CircuitNotFound(
                view_params.path.clone(),
            ));
            return;
        };
        circuit_i = next_circuit_i;
    }

    let circuit = &flattened.circuits[circuit_i];

    // Export the electric potentials of nodes
    for (node_i, node) in circuit
        .child_nodes
        .accessor(&flattened.nodes, |x| &x.sibling_nodes)
        .iter()
    {
        let node_def = &flattened.node_defs[node.node_def_i];
        let ent_node = node_def.entity;
        let Some(sim_node) = sim_state.nodes.get(node_i) else {
            tracing::warn!(?node_i, "`SimNode` not found");
            continue;
        };
        let voltage = sim_state.world.node(sim_node.node_id);
        tracing::trace!(?node_i, ?ent_node, voltage, "Exporting a node potential");
        view_out.nodes.insert(ent_node, voltage);
    }

    // Export the device output variables of non-subcircuit devices
    for (device_i, device) in circuit
        .child_devices
        .accessor(&flattened.devices, |x| &x.sibling_devices)
        .iter()
    {
        let device_def = &flattened.device_defs[device.device_def_i];

        let Some(sim_device) = sim_state.devices.get(device_i) else {
            tracing::warn!(?device_i, "`SimDevice` not found");
            continue;
        };

        let start = view_out.device_vars.len();
        view_out.device_vars_start.insert(device_def.entity, start);

        match &device_def.ty {
            FlatDeviceDefTy::Subcircuit { .. } => {
                unreachable!("`SimCircuit::child_devices` should not contain subcircuit devices")
            }
            FlatDeviceDefTy::Terminal { .. } => {}
            FlatDeviceDefTy::Element { .. } => {
                // TODO
                tracing::trace!(
                    ?device_i,
                    ent_device = ?device_def.entity,
                    "Exporting no variables for element device",
                );
                continue;
            }
            FlatDeviceDefTy::Wire { .. } => {}
        };

        // Export the wire element's branch current
        let node_id = sim_device
            .current_node_id
            .expect("`Terminal` and `Wire` should have `current_node_id` set");
        let current = sim_state.world.node(node_id);
        view_out.device_vars.push(current);
        tracing::trace!(
            ?device_i,
            ent_device = ?device_def.entity,
            current,
            "Exporting this element device's branch current",
        );
    }

    // Export the device output variables of subcircuit devices
    for (child_circuit_i, child_circuit) in circuit
        .child_circuits
        .accessor(&flattened.circuits, |x| &x.sibling_circuits)
        .iter()
    {
        let child_device_def_i = child_circuit
            .subcircuit_device_def_i
            .expect("child circuit should be an instance of a subcirucit device");
        let child_device_def = &flattened.device_defs[child_device_def_i];

        // Reserve the space for terminal currents
        let start = view_out.device_vars.len();
        view_out
            .device_vars_start
            .insert(child_device_def.entity, start);
        view_out
            .device_vars
            .extend((0..child_device_def.ent_terminals.len()).map(|_| 0.0));
        let terminal_currents = &mut view_out.device_vars[start..];

        // Fetch the currents of child terminal devices
        for (grandchild_device_i, grandchild_device) in child_circuit
            .child_devices
            .accessor(&flattened.devices, |x| &x.sibling_devices)
            .iter()
        {
            let grandchild_device_def = &flattened.device_defs[grandchild_device.device_def_i];
            let FlatDeviceDefTy::Terminal { terminal_i, .. } = grandchild_device_def.ty else {
                // We'll not find any more terminal devices because of
                // [ref:flat_child_device_order]
                break;
            };

            let Some(sim_grandchild_device) = sim_state.devices.get(grandchild_device_i) else {
                tracing::warn!(?grandchild_device_i, "`SimDevice` not found");
                continue;
            };

            let Some(p_terminal_current) = terminal_currents.get_mut(terminal_i) else {
                tracing::trace!(
                    ?grandchild_device_i,
                    terminal_i,
                    "This grandchild terminal device's terminal index is out of bounds, \
                    not fetching its current"
                );
                continue;
            };

            // Fetch the terminal's outbound current
            let node_id = sim_grandchild_device
                .current_node_id
                .expect("`Terminal` should have `current_node_id` set");
            let current = sim_state.world.node(node_id);

            tracing::trace!(
                ?grandchild_device_i,
                terminal_i,
                current,
                "The current flowing into this grandchild terminal device"
            );

            *p_terminal_current += current;
        }

        tracing::trace!(
            ?child_circuit_i,
            ent_device = ?child_device_def.entity,
            ?terminal_currents,
            "Exporting this subcircuit device's terminal currents",
        );
    }

    view_out.result = Ok(());
}
