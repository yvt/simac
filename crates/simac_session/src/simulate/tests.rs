use super::*;
use anyhow::{Context, Result};

use crate::{circuit, expr, flat};

mod cmds;

#[derive(Debug, Resource)]
struct TestRes {
    flatten: flat::Flatten,
    flattened: flat::Flattened,
    sim_params: SimParams,
    sim_state: SimState,
}

/// (Bevy system)
fn sys_update(
    mut test_res: ResMut<'_, TestRes>,
    flat_sync_params: flat::SyncCircuitParams<'_, '_>,
    flat_setup_params: flat::SetupCircuitParams<'_, '_>,
) {
    let TestRes {
        flatten,
        flattened,
        sim_params,
        sim_state,
    } = &mut *test_res;
    flat::sync_circuit(flatten, flattened, &flat_sync_params);
    flat::setup_circuit(flatten, flattened, &flat_setup_params);
    sync_circuit(sim_state, flattened);
    if let Err(error) = step_simulation(sim_params, sim_state) {
        tracing::info!(%error, "`step_simulation` failed");
    }
}

/// Check the consistency of the specified [`SimState`].
fn validate_sim(
    flattened: Option<&flat::Flattened>,
    _sim_params: &SimParams,
    sim_state: &SimState,
) -> Result<()> {
    let Some(flattened) = flattened else { return Ok(()) };

    // `devices` should be synchronized
    anyhow::ensure!(sim_state.devices.len() == flattened.devices.len());
    for (device_i, _) in flattened.devices.iter() {
        anyhow::ensure!(sim_state.devices.contains_key(device_i));
    }

    // `nodes` should be synchronized
    anyhow::ensure!(sim_state.nodes.len() == flattened.nodes.len());
    for (node_i, _) in flattened.nodes.iter() {
        anyhow::ensure!(sim_state.nodes.contains_key(node_i));
    }

    Ok(())
}
