//! Operators

pub mod assign_ids;
pub mod item_attach;
pub mod item_detach;
pub mod item_displace;
pub mod item_move;
pub mod item_remove;
pub mod wire_add;

#[cfg(test)]
mod tests;
