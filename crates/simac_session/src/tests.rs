pub(crate) mod circuit_examples;
pub(crate) mod circuit_union_find;
pub(crate) mod circuit_validate;

pub(crate) use simac_test_utils::init;

pub(crate) fn proptest_index_iter<T: Clone>(
    index: proptest::sample::Index,
    iter: impl IntoIterator<Item = T>,
) -> Option<T> {
    let mut iter = iter.into_iter();
    match iter.size_hint() {
        (0, Some(0)) => return None,
        (lower, Some(upper)) if lower == upper => {
            let i = index.index(upper);
            return iter.nth(i);
        }
        _ => (),
    }

    let items = Vec::from_iter(iter);
    if items.is_empty() { None } else { Some(index.get(&items).clone()) }
}

pub(crate) fn proptest_index_bevy_world_entity<F: bevy_ecs::query::QueryFilter>(
    index: proptest::sample::Index,
    world: &mut bevy_ecs::world::World,
) -> Option<bevy_ecs::entity::Entity> {
    proptest_index_iter(
        index,
        world
            .query_filtered::<bevy_ecs::entity::Entity, F>()
            .iter(world),
    )
}
