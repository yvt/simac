//! Provides a Bevy system to (re)assign [`Id`]s so that `Id`-based references
//! are free of ambiguities.
//!
//! [`crate::kio`] assumes that saved circuits are free of such ambiguities
//! (`[ref:kio_id_collision]`).
use ascii::{AsMutAsciiStr as _, AsciiChar};
use bevy_ecs::prelude::*;
use bevy_math::IVec2;
use hashbrown::{HashTable, hash_table};
use macro_rules_attribute::derive;
use std::{borrow::Cow, fmt::Write as _};
use try_match::match_ok;
use typed_index::{IndexExt, IndexMutExt as _, VecExt};

use crate::{
    circuit, layout,
    meta::{Id, TerminalNode},
};

#[derive(Debug, bevy_ecs::system::SystemParam)]
pub struct AssignIdsParam<'w, 's> {
    q_circuit: Query<'w, 's, (Entity, &'static circuit::Circuit)>,
    q_id: Query<'w, 's, &'static Id>,
    q_device: Query<'w, 's, DeviceQueryData>,
    q_node: Query<'w, 's, NodeQueryData, With<circuit::Node>>,
}

#[derive(Debug, bevy_ecs::query::QueryData)]
struct DeviceQueryData {
    comp_device: &'static circuit::Device,
    is_subcircuit: Has<circuit::SubcircuitDevice>,
    is_element: Has<circuit::ElementDevice>,
    is_terminal: Has<circuit::TerminalDevice>,
    comp_position: Option<&'static layout::Position>,
}

#[derive(Debug, bevy_ecs::query::QueryData)]
struct NodeQueryData {
    is_terminal_node: Has<TerminalNode>,
    comp_position: Option<&'static layout::Position>,
}

/// A result of [`sys_plan_assign_ids`].
#[derive(Debug, Clone)]
pub struct AssignIdsPlan {
    /// New [`Id`]s to add to entities.
    pub new_id_assignments: Vec<(Entity, String)>,
}

/// The Bevy system to (re)assign [`Id`]s so that `Id`-based references are free
/// of ambiguities.
///
/// TODO: Make this work on [`crate::meta::ParamDefMeta::id`] etc as well.
///
/// TODO: Fix invalid `Id`s that break [`crate::kio`].
///
/// TODO: Make this work on circuit `Id`s as well.
///
/// The result is invariant over hashtable iteration order.
/// This is guaranteed by sorting circuit items by "item keys"
/// (`[ref:assign_id_item_key]`) before performing order-dependent algorithm
/// steps.
/// The item keys are currently derived from item positions.
/// This means that invariance can be lost if two items with conflicting or
/// missing [`Id`]s are placed at the same location.
///
/// > This invariance has little value for normal usage, but snapshot tests
/// > could randomly fail without it.
///
/// [1]: crate::kio
#[tracing::instrument(level = "debug", skip_all)]
pub fn sys_plan_assign_ids(param: AssignIdsParam<'_, '_>) -> AssignIdsPlan {
    let mut cx = AssignIdCx {
        items: Vec::new(),
        id_map: <_>::default(),
        id_class_map: <_>::default(),
        next_id: String::new(),
        new_id_assignments: Vec::new(),
    };

    // Process every circuit
    for (ent_circuit, comp_circuit) in param.q_circuit.iter() {
        let _span = tracing::debug_span!("process_circuit", %ent_circuit).entered();
        process_circuit(&mut cx, &param, comp_circuit);
    }

    AssignIdsPlan {
        new_id_assignments: cx.new_id_assignments,
    }
} // fn sys_plan_assign_ids

impl AssignIdsPlan {
    pub fn is_empty(&self) -> bool {
        self.new_id_assignments.is_empty()
    }
}

struct AssignIdCx<'a> {
    items: Vec<Item<'a>>,
    /// The list of items for each unique `Id` ([`Item::id`]).
    /// Each hashmap value points to the first item.
    id_map: HashTable<ItemI>,
    /// The list of items for each unique `Id` class ([`Item::id_class`]).
    /// Each hashmap value points to the first item.
    id_class_map: HashTable<ItemI>,
    next_id: String,
    /// The new [`Id`]s to assign.
    new_id_assignments: Vec<(Entity, String)>,
}

/// A circuit item.
struct Item<'a> {
    ent_item: Entity,
    /// The item's [`Id`]. `Some(Cow::Owned(_))` if a new `Id` needs to be
    /// assigned to the item.
    id: Option<Cow<'a, String>>,
    id_class: &'a str,
    key: ItemKey,
    /// The next item in the singly-linked list headed by `id_map` or
    /// `item_noid`.
    next_of_id: ItemI,
    /// The next item in the singly-linked list headed by `id_class_map`.
    next_of_id_class: ItemI,
}

/// An index into [`AssignIdCx::items`].
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ItemI(usize);

typed_index::impl_typed_index!(impl TypedIndex<Vec<Item<'_>>> for ItemI);

impl ItemI {
    const NONE: Self = Self(usize::MAX);
}

/// A value associated with an [`Item`] that is expected to be unique to that
/// item. `[tag:assign_id_item_key]`
///
/// We currently use values derived from [`layout::Position`] as item keys.
type ItemKey = u64;

/// An inner function of [`sys_plan_assign_ids`] to process a single circuit.
fn process_circuit<'a>(
    AssignIdCx {
        items,
        id_map,
        id_class_map,
        next_id,
        new_id_assignments,
    }: &mut AssignIdCx<'a>,
    AssignIdsParam {
        q_circuit: _,
        q_id,
        q_device,
        q_node,
    }: &'a AssignIdsParam<'_, '_>,
    comp_circuit: &circuit::Circuit,
) {
    let hash = |x: &str| fxhash::hash64(x);

    // Discover items and group them by `Id`s
    // ------------------------------------------------------------------

    items.clear();

    let mut visit_item = |ent_item: Entity, needs_id: bool, position: IVec2| {
        let id = match_ok!(q_id.get(ent_item), Ok(Id(_0)));

        if id.is_none() && !needs_id {
            tracing::trace!(
                %ent_item,
                "This entity is missing `Id`, but this type of entity \
                does not require one",
            );
            return;
        }

        // Convert `layout::Position` to `ItemKey`
        let key = u64::from(position.x.abs_diff(i32::MIN))
            | (u64::from(position.y.abs_diff(i32::MIN)) << 32);
        tracing::trace!(%ent_item, key);

        items.push(Item {
            ent_item,
            id: id.map(Cow::Borrowed),
            id_class: id.map(|id| id_class_of(id)).unwrap_or(""),
            key,
            next_of_id: ItemI::NONE,       // set later
            next_of_id_class: ItemI::NONE, // set later
        });
    };

    // [ref:kio_id_circuit_item]
    // Certain types of circuit items (devices and nodes) are affected by
    // `Id` collision. Our task is to fix that.
    for &ent_item in &comp_circuit.devices {
        let qi_device = q_device
            .get(ent_item)
            .unwrap_or_else(|e| panic!("{ent_item} lacks `Device`: {e}"));

        let mut position = IVec2::ZERO;

        if let Some(comp_position) = qi_device.comp_position {
            // Use the device's position as `ItemKey`
            position = comp_position.0;
        } else {
            // Use the mean of the terminal positions as `ItemKey`
            let terminals = &qi_device.comp_device.terminals;
            for &ent_node in terminals.iter() {
                let qi_node = q_node
                    .get(ent_node)
                    .unwrap_or_else(|e| panic!("{ent_node} lacks `Node`: {e}"));

                if let Some(comp_position) = qi_node.comp_position {
                    // Wrap-around does not break invariant
                    position = position.wrapping_add(comp_position.0);
                }
            }

            if !terminals.is_empty() {
                position /= terminals.len() as i32;
            }
        };

        visit_item(ent_item, device_needs_id(&qi_device), position);
    }

    for &ent_item in &comp_circuit.nodes {
        let qi_node = q_node
            .get(ent_item)
            .unwrap_or_else(|e| panic!("{ent_item} lacks `Node`: {e}"));

        if node_ignores_id(&qi_node) {
            continue;
        }

        let position = qi_node.comp_position.map(|p| p.0).unwrap_or_default();

        visit_item(ent_item, true, position);
    }

    // Sort items by `ItemKey` so that the result is invariant over
    // hashtable iteration order
    let mut item_i_list_sorted: Vec<ItemI> = items.keys_typed().collect();
    item_i_list_sorted.sort_unstable_by_key(|&i| items.index_typed(i).key);

    // Insert them into `id`-keyed lists
    let mut skip_circuit = true;
    let mut item_noid = ItemI::NONE;
    id_map.clear();

    for &item_i in &item_i_list_sorted {
        let Item {
            ref id, ent_item, ..
        } = *items.index_typed(item_i);

        let p_first_item_i_of_id = if let Some(id) = id {
            let entry = id_map.entry(
                hash(id),
                |&i| **items.index_typed(i).id.as_ref().unwrap() == **id,
                |&i| hash(items.index_typed(i).id.as_ref().unwrap()),
            );
            if matches!(entry, hash_table::Entry::Occupied(_)) {
                skip_circuit = false;
            }
            entry.or_insert(ItemI::NONE).into_mut()
        } else {
            tracing::debug!(%ent_item, "This entity is missing `Id`");
            skip_circuit = false;
            &mut item_noid
        };

        items.index_mut_typed(item_i).next_of_id = std::mem::replace(p_first_item_i_of_id, item_i);
    }

    if skip_circuit {
        // Nothing to do for this circuit
        return;
    }

    // Assign `Id`s to items missing ones
    // ------------------------------------------------------------------

    let mut cur = item_noid;
    while let Some(item) = items.get_mut_typed(cur) {
        let item_i = cur;
        cur = item.next_of_id;

        // Choose default `Id` class for this type of item
        let id_class = if let Ok(qi_device) = q_device.get(item.ent_item) {
            if qi_device.is_terminal { "terminal" } else { "device" }
        } else {
            "node"
        };

        // Choose default `Id` for this type of item
        let id = format!("{id_class}1");

        tracing::debug!(
            ent_item = %item.ent_item,
            id,
            id_class,
            "Assigning a default `Id` to an entity with missing `Id`",
        );

        // Insert this `item_i` into an `id`-keyed list
        let p_first_item_i = id_map
            .entry(
                hash(&id),
                |&i| **items.index_typed(i).id.as_ref().unwrap() == id,
                |&i| hash(items.index_typed(i).id.as_ref().unwrap()),
            )
            .or_insert(ItemI::NONE)
            .into_mut();
        let item = items.index_mut_typed(item_i);
        item.next_of_id = std::mem::replace(p_first_item_i, item_i);

        // Set `Item::id`. Make sure to use `Cow::Owned` to indicate that
        // `Id` needs updating for this item.
        item.id = Some(Cow::Owned(id));
        item.id_class = id_class;
    }

    // Group items by `Id` classes
    // ------------------------------------------------------------------

    id_class_map.clear();

    for &item_i in &item_i_list_sorted {
        let item = items.index_typed(item_i);

        // Insert this `item_i` into an `id_class`-keyed list
        let p_first_item_i = id_class_map
            .entry(
                hash(item.id_class),
                |&i| items.index_typed(i).id_class == item.id_class,
                |&i| hash(items.index_typed(i).id_class),
            )
            .or_insert(ItemI::NONE)
            .into_mut();
        let item = items.index_mut_typed(item_i);
        item.next_of_id_class = std::mem::replace(p_first_item_i, item_i);
    }

    // Resolve duplicates
    // ------------------------------------------------------------------

    // For each `id_class`...
    for &first_item_i in id_class_map.iter() {
        // Is `next_id` initialized for this `id_class`?
        let mut next_id_set = false;

        let mut cur = first_item_i;
        while let Some(item) = items.get_typed(cur) {
            let item_i = cur;
            cur = item.next_of_id_class;

            // If this item is the last element of the `id`-keyed list,
            // preserve its `id`.
            if item.next_of_id == ItemI::NONE {
                tracing::trace!(
                    ent_item = %item.ent_item,
                    id = item.id.as_ref().unwrap().as_str(),
                    "Not Updating this entity's `Id` because it's the last item \
                    of the `id`-keyed list it belongs to"
                );
                continue;
            }

            // We are going to reassign the `id` of this item. Remove it from
            // `id_map` first.
            //
            // Usually this requires iterating over the singly-linked list
            // headed by `id_map[id]` to locate `item_i`, but at this stage we
            // only need the following invariants of `id_map[_]`, so this step
            // can be simplified greatly: (a) `items[id_map[id]].id == id`; (b)
            // the last item of the list headed by `id_map[id]` stays there.
            {
                let id = item.id.as_ref().unwrap();
                let p_first_item_i = id_map
                    .find_mut(hash(id), |&i| {
                        **items.index_typed(i).id.as_ref().unwrap() == **id
                    })
                    .unwrap();
                if *p_first_item_i == item_i {
                    // We need to bring another element to the front to preserve
                    // the invariant `items[id_map[id]].id == id`.
                    *p_first_item_i = item.next_of_id;
                }
            }

            // Find an unused `id` of the same class.
            if !next_id_set {
                // Initialize `next_id`
                next_id_set = true;
                next_id.clear();
                write!(next_id, "{}1", item.id_class).unwrap();
            }

            let id_map_entry = loop {
                if let hash_table::Entry::Vacant(entry) = id_map.entry(
                    hash(next_id),
                    |&i| **items.index_typed(i).id.as_ref().unwrap() == *next_id,
                    |&i| hash(items.index_typed(i).id.as_ref().unwrap()),
                ) {
                    break entry;
                }

                tracing::trace!(
                    next_id,
                    "This `Id` collides an existing one; trying the next one",
                );
                id_class_next(next_id, item.id_class);
            };

            // Assign this `next_id` to this item
            let item = items.index_mut_typed(item_i);
            item.id = Some(Cow::Owned(next_id.clone()));
            id_map_entry.insert(item_i);

            tracing::debug!(
                ent_item = %item.ent_item,
                id = next_id,
                "Assigning a new `Id` to this entity",
            );

            id_class_next(next_id, item.id_class);
        }
    }

    // Append items with `id = Some(Owned(_))` to `new_id_assignments`
    // ------------------------------------------------------------------

    new_id_assignments.extend(
        items
            .drain(..)
            .filter_map(|i| match_ok!(i.id, Some(Cow::Owned(id)) => (i.ent_item, id))),
    );
}

/// Get a flag indicating whether [`Id`] is mandatory for the type of device
/// represented by `qi_device`.
fn device_needs_id(qi_device: &DeviceQueryDataItem) -> bool {
    // [ref:kio_subcircuit_device_needs_id]
    // [ref:kio_element_device_needs_id]
    // [ref:kio_terminal_device_needs_id]
    qi_device.is_subcircuit || qi_device.is_element || qi_device.is_terminal
}

/// Get a flag indicating whether [`Id`] has no effect for the type of node
/// represented by `qi_node`.
fn node_ignores_id(qi_node: &NodeQueryDataItem) -> bool {
    // [ref:kio_terminal_node_ignores_id]
    qi_node.is_terminal_node
}

/// Find the `Id` class of a given `Id`.
fn id_class_of(id: &str) -> &str {
    let bytes = id.as_bytes();

    // Find trailing digits
    let mut num_suffix_digits = bytes
        .iter()
        .rev()
        .take_while(|b| b.is_ascii_digit())
        .count();

    // Ignore extra leading zeros
    if num_suffix_digits != 0 {
        num_suffix_digits -= bytes[bytes.len() - num_suffix_digits..bytes.len() - 1]
            .iter()
            .take_while(|&&b| b == b'0')
            .count();
    }

    &id[..id.len() - num_suffix_digits]
}

/// Find the next `Id` belonging to the same `Id` class.
fn id_class_next(id: &mut String, id_class: &str) {
    str_incr(id, id_class.len());
}

/// Increment the number in `s[start_i..]`.
fn str_incr(s: &mut String, start_i: usize) -> &str {
    let num = s.slice_ascii_mut(start_i..).unwrap().as_mut_slice();

    for by in num.iter_mut().rev() {
        *by = match by {
            AsciiChar::_0 => AsciiChar::_1,
            AsciiChar::_1 => AsciiChar::_2,
            AsciiChar::_2 => AsciiChar::_3,
            AsciiChar::_3 => AsciiChar::_4,
            AsciiChar::_4 => AsciiChar::_5,
            AsciiChar::_5 => AsciiChar::_6,
            AsciiChar::_6 => AsciiChar::_7,
            AsciiChar::_7 => AsciiChar::_8,
            AsciiChar::_8 => AsciiChar::_9,
            AsciiChar::_9 => {
                *by = AsciiChar::_0;
                continue;
            }
            _ => unreachable!(),
        };

        return s;
    }

    // `999..` -> `1000..`
    s.insert(start_i, '1');

    s
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        kio::test_utils::Workspace,
        tests::circuit_examples::{
            ExampleCircuitPath, any_example_circuit_path, example_circuit_paths,
        },
    };
    use bevy_ecs::entity::EntityHashMap;
    use itertools::chain;
    use proptest::prelude::*;

    /// Check that [`sys_plan_assign_ids`] returns an empty plan for example
    /// circuits.
    #[test]
    fn noop_example_circuits() {
        crate::tests::init();

        for path in example_circuit_paths() {
            tracing::info!(?path);
            let Workspace { mut world, .. } = path.load();

            let plan = world.run_system_cached(sys_plan_assign_ids).unwrap();
            tracing::info!(?plan);
            assert!(plan.is_empty());
        }
    }

    proptest! {
        /// Replace or remove some of the [`Id`]s in example circuits and check
        /// that [`sys_plan_assign_ids`] reassigns [`Id`]s as necessary.
        #[test]
        fn pt_mutate(
            path in any_example_circuit_path(),
            item_renames in prop::collection::vec(
                (
                    any::<prop::sample::Index>(),
                    // Provide a small set of names to choose from to create
                    // `Id` collisions on purpose
                    prop::option::of("_test_[012]?"),
                ),
                0..20,
            ),
        ) {
            crate::tests::init();
            pt_mutate_inner(path, item_renames)?;
        }
    }

    fn pt_mutate_inner(
        path: ExampleCircuitPath,
        item_renames: Vec<(prop::sample::Index, Option<String>)>,
    ) -> prop::test_runner::TestCaseResult {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut id_map: EntityHashMap<String> = world
            .query::<(Entity, &Id)>()
            .iter(&world)
            .map(|(ent, id)| (ent, id.0.clone()))
            .collect();

        // List all entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_items: Vec<_> = chain!(&comp_circuit.devices, &comp_circuit.nodes)
            .copied()
            .collect();
        ent_all_items.sort_unstable();

        // Decide the items to rename
        prop_assume!(!ent_all_items.is_empty());
        let item_renames: Vec<(Entity, Option<String>)> = item_renames
            .into_iter()
            .map(|(i, id)| (*i.get(&ent_all_items), id))
            .collect();
        tracing::info!(?item_renames);

        // Rename the items to create `Id` collisions
        for (ent, new_id) in item_renames {
            let mut world_ent = world.entity_mut(ent);
            if let Some(id) = new_id {
                world_ent.insert(Id(id));
            } else {
                world_ent.remove::<Id>();
            }

            // Since we assign a potentially duplicate `Id` to `ent` or
            // strip it of an existing `Id`, we can't expect
            // `sys_plan_assign_ids` to leave `ent` untouched.
            id_map.remove(&ent);
        }

        // Check for `Id` collisions and create a plan to fix that
        let AssignIdsPlan { new_id_assignments } =
            world.run_system_cached(sys_plan_assign_ids).unwrap();
        tracing::info!(?new_id_assignments);

        // Execute the plan
        world
            .insert_or_spawn_batch(
                new_id_assignments
                    .into_iter()
                    .map(|(ent_item, id)| (ent_item, Id(id))),
            )
            .expect("insert_or_spawn_batch");

        // Check for `Id` collisions. `sys_plan_assign_ids` should return
        // an empty plan this time.
        let plan = world.run_system_cached(sys_plan_assign_ids).unwrap();
        tracing::info!(?plan);
        assert!(
            plan.is_empty(),
            "`sys_plan_assign_ids` returned a non-empty plan even though we have \
            executed the previous plan"
        );

        // Check that entities not involved in `Id` collisions are untouched
        for (ent, old_id) in id_map {
            let Id(id) = world.get(ent).unwrap();
            assert_eq!(
                *id, old_id,
                "{ent} unexpected had its `Id` changed from \
                {old_id:?} to {id:?}"
            );
        }

        Ok(())
    }

    #[test]
    fn id_class() {
        assert_eq!(id_class_of(""), "");
        assert_eq!(id_class_of("50"), "");
        assert_eq!(id_class_of("123"), "");
        assert_eq!(id_class_of("0"), "");
        assert_eq!(id_class_of("05"), "0");

        assert_eq!(id_class_of("foo"), "foo");
        assert_eq!(id_class_of("foo50"), "foo");
        assert_eq!(id_class_of("foo123"), "foo");
        assert_eq!(id_class_of("foo0"), "foo");
        assert_eq!(id_class_of("foo00"), "foo0");
        assert_eq!(id_class_of("foo05"), "foo0");
    }

    #[test]
    fn str_incr_cases() {
        assert_eq!(str_incr(&mut String::new(), 0), "1");
        assert_eq!(
            str_incr(&mut "9".repeat(100), 0),
            format!("1{}", "0".repeat(100)),
        );
    }

    proptest! {
        #[test]
        fn pt_id_class_next(id in "a{0,1}[01]{0,2}|[a-z0-9]{0,6}") {
            let id_class = id_class_of(&id);

            let mut id2 = id.clone();
            id_class_next(&mut id2, id_class);
            let id_class2 = id_class_of(&id2);

            // `id_class_next` should not change id class
            prop_assert_eq!(id_class, id_class2);
        }

        #[test]
        fn pt_str_incr(prefix: String, x in 0..u32::MAX - 1) {
            let mut st = format!("{prefix}{x}");
            str_incr(&mut st, prefix.len());

            let expected = format!("{prefix}{}", x + 1);
            prop_assert_eq!(st, expected);
        }
    }
}
