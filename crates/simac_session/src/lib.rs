pub mod circuit;
pub mod expr;
pub mod flat;
pub mod kio;
pub mod layout;
pub mod meta;
pub mod ops;
pub mod render;
pub mod sim_control;
pub mod simulate;

#[cfg(test)]
mod tests;

/// Register required components to `world`.
///
/// Note: This function does not register components to
/// [`simac_operator::stack::OperatorStack`][] (yet?).
pub fn init_world(world: &mut bevy_ecs::world::World) {
    expr::init_world(world);
    sim_control::init_world(world);
}

/// Register components to [`simac_operator::stack::OperatorStack`].
///
/// # Notable Omissions
///
/// - [`crate::kio::File`] is excluded because it is a generic type.
pub fn init_stashable_components(
    operator_stack: &mut simac_operator::stack::OperatorStack,
    world: &mut bevy_ecs::world::World,
) {
    macro_rules! init {
        ($Ty:ty) => {{
            world.register_component::<$Ty>();
            operator_stack.init_component::<$Ty>(world.components());
        }};
    }

    init!(circuit::Circuit);
    init!(circuit::Device);
    init!(circuit::ElementDevice);
    init!(circuit::GroundNode);
    init!(circuit::Node);
    init!(circuit::SubcircuitDevice);
    init!(circuit::TerminalDevice);
    init!(circuit::WireDevice);

    init!(expr::ElementDevice);
    init!(expr::SubcircuitDevice);

    init!(flat::Flatten);

    init!(kio::FileMember);
    init!(kio::OriginalAstCircuit);
    init!(kio::OriginalAstFile);
    init!(kio::OriginalAstItem);
    init!(kio::OriginalKdl);

    init!(layout::Position);
    init!(layout::Rotation);
    init!(layout::Symbol);

    init!(meta::CircuitMeta);
    init!(meta::Id);
    init!(meta::Label);
    init!(meta::TerminalNode);

    init!(sim_control::SimControl);
}
