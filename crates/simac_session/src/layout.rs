//! Bevy components to define circuit ([`crate::circuit`]) schematics
use bevy_ecs::prelude::*;
use bevy_math::IVec2;
use enum_map::Enum;
use itertools::izip;
use macro_rules_attribute::derive;
use simac_operator::StashableComponent;
use svgtypes::SimplePathSegment;

use crate::{circuit, meta};

// -------------------------------------------------------------------------

/// The position of a device ([`circuit::Device`]) or node ([`circuit::Node`]).
///
/// For terminal nodes ([`meta::TerminalNode`]), it should be implicitly set by
/// [`position_terminal_nodes`].
#[derive(Component, StashableComponent!, Default, Debug, Clone, Copy)]
pub struct Position(pub IVec2);

bitflags::bitflags! {
    /// The rotation and reflection ([improper rotation][1]) of a device
    /// ([`circuit::Device`]).
    ///
    /// [1]: https://en.wikipedia.org/wiki/Improper_rotation
    #[derive(
        Component, StashableComponent!, Default, Debug, Clone, Copy,
        PartialEq, Eq, Hash,
    )]
    pub struct Rotation: u8 {
        /// Swap X and Y coordinates (reflection across line `x = y`).
        const TRANSPOSE = 0b001;
        /// Invert X coordinates (reflection across line `x = 0`).
        const FLIP_X = 0b010;
        /// Invert Y coordinates (reflection across line `y = 0`).
        const FLIP_Y = 0b100;
    }
}

impl Rotation {
    /// Apply `self `to a value of a [`IVec2`]-like type.
    #[inline]
    pub fn apply<T, E>(self, v: T) -> T
    where
        T: Into<[E; 2]>,
        [E; 2]: Into<T>,
        E: std::ops::Neg<Output = E>,
    {
        let [mut x, mut y]: [E; 2] = v.into();
        if self.contains(Self::TRANSPOSE) {
            (x, y) = (y, x);
        }
        if self.contains(Self::FLIP_X) {
            x = -x;
        }
        if self.contains(Self::FLIP_Y) {
            y = -y;
        }
        [x, y].into()
    }

    /// Decompose `self` into [`Angle`] and [`Self::FLIP_X`].
    ///
    /// ```rust
    /// use simac_session::layout::{Angle::*, Rotation};
    ///
    /// for angle in [_0, _90, _180, _270] {
    ///     for flip_x in [false, true] {
    ///         let mut rotation = Rotation::from(angle);
    ///         if flip_x {
    ///             rotation.toggle(Rotation::FLIP_X);
    ///         }
    ///
    ///         let (angle_, flip_x_) = rotation.to_angle_and_flip_x();
    ///
    ///         assert_eq!((angle, flip_x), (angle_, flip_x_));
    ///     }
    /// }
    /// ```
    #[inline]
    pub fn to_angle_and_flip_x(self) -> (Angle, bool) {
        match self.bits() {
            0b000 => (Angle::_0, false),
            0b011 => (Angle::_90, false),
            0b110 => (Angle::_180, false),
            0b101 => (Angle::_270, false),
            0b010 => (Angle::_0, true),
            0b001 => (Angle::_90, true),
            0b100 => (Angle::_180, true),
            0b111 => (Angle::_270, true),
            8.. => unreachable!(),
        }
    }

    /// Calculate the inverse of `self`.
    #[inline]
    pub fn inverse(self) -> Self {
        Self::from_bits_truncate(
            (0b0111_0110_0011_0100_0101_0010_0001_0000 >> (self.bits() * 4)) as _,
        )
    }
}

/// Combine two rotations
/// (`(after * before).apply(x) == after.apply(before.apply(x))`).
impl std::ops::Mul<Rotation> for Rotation {
    type Output = Self;

    fn mul(self, mut rhs: Rotation) -> Self::Output {
        if self.contains(Self::TRANSPOSE) {
            // Swap `FLIP_{X,Y}` in `rhs`
            rhs = Self::from_bits_truncate(
                (0b0111_0110_0011_0010_0101_0100_0001_0000 >> (rhs.bits() * 4)) as _,
            );
        }
        self ^ rhs
    }
}

/// A rotation angle in multiples of 90 degrees.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Angle {
    #[default]
    _0,
    _90,
    _180,
    _270,
}

impl Angle {
    /// Rotate a 2D vector of a [`IVec2`]-like type.
    #[inline]
    pub fn rotate<T, E>(self, v: T) -> T
    where
        T: Into<[E; 2]>,
        [E; 2]: Into<T>,
        E: std::ops::Neg<Output = E>,
    {
        let [x, y]: [E; 2] = v.into();
        match self {
            Self::_0 => [x, y],
            Self::_90 => [-y, x],
            Self::_180 => [-x, -y],
            Self::_270 => [y, -x],
        }
        .into()
    }
}

impl From<Angle> for Rotation {
    fn from(value: Angle) -> Self {
        match value {
            Angle::_0 => Self::empty(),
            Angle::_90 => Self::TRANSPOSE | Self::FLIP_X,
            Angle::_180 => Self::FLIP_X | Self::FLIP_Y,
            Angle::_270 => Self::TRANSPOSE | Self::FLIP_Y,
        }
    }
}

impl TryFrom<i32> for Angle {
    type Error = ();

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::_0),
            90 => Ok(Self::_90),
            180 => Ok(Self::_180),
            270 => Ok(Self::_270),
            _ => Err(()),
        }
    }
}

impl From<Angle> for u16 {
    fn from(value: Angle) -> Self {
        match value {
            Angle::_0 => 0,
            Angle::_90 => 90,
            Angle::_180 => 180,
            Angle::_270 => 270,
        }
    }
}

// -------------------------------------------------------------------------

/// The bundle for the layout aspect of devices ([`circuit::DeviceBundle`])
/// except for wires (which are fixed to nodes).
#[derive(Bundle, Debug)]
pub struct DeviceLayoutBundle {
    pub position: Position,
    pub rotation: Rotation,
}

/// The bundle for the layout aspect of nodes ([`circuit::NodeBundle`]).
#[derive(Bundle, Debug)]
pub struct NodeLayoutBundle {
    pub position: Position,
}

// -------------------------------------------------------------------------

/// The component to define circuit ([`circuit::Circuit`]) symbols for
/// when they are used as subcircuits ([`circuit::SubcircuitDevice`]).
#[derive(Component, StashableComponent!, Default, Debug, Clone)]
pub struct Symbol {
    pub terminals: Vec<SymbolTerminal>,
    pub paths: Vec<Path>,
    // TODO: Texts
}

#[derive(Debug, Clone)]
pub struct SymbolTerminal {
    pub position: IVec2,
}

#[derive(Debug, Clone)]
pub struct Path {
    pub fill: FillColor,
    pub stroke: StrokeColor,
    pub stroke_width: u8,
    pub segments: Vec<SimplePathSegment>,
}

impl Default for Path {
    fn default() -> Self {
        Self {
            fill: FillColor::default(),
            stroke: StrokeColor::Const(Color::Default),
            stroke_width: 1,
            segments: Vec::new(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FillColor {
    Const(Color),
    /// Electric potential dependent coloring.
    /// Specifies the terminal index to use the potential of.
    Potential(usize),
}

impl Default for FillColor {
    fn default() -> Self {
        FillColor::Const(Color::None)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum StrokeColor {
    Const(Color),
    /// Electric potential dependent coloring.
    /// Specifies the terminal indices to use the potentials of.
    Potential([usize; 2]),
}

impl Default for StrokeColor {
    fn default() -> Self {
        StrokeColor::Const(Color::Default)
    }
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Enum)]
pub enum Color {
    /// Transparent.
    #[default]
    None,
    /// The default foreground color.
    Default,
    Red,
    Yellow,
    Green,
    Blue,
    Purple,
    Brown,
}

// -------------------------------------------------------------------------

/// The [`bevy_ecs::query::QueryData`] for [`position_terminal_nodes`] to
/// query for devices.
#[derive(bevy_ecs::query::QueryData)]
pub struct PositionTerminalNodesDeviceQueryData<'w> {
    entity: Entity,
    comp_device: Ref<'w, circuit::Device>,
    comp_position: Ref<'w, Position>,
    comp_rotation: Ref<'w, Rotation>,
}

/// The Bevy system to update the [`Position`] of all entities with
/// [`meta::TerminalNode`].
#[tracing::instrument(level = "trace", skip_all)]
pub fn position_terminal_nodes(
    q_subcircuits: Query<(
        PositionTerminalNodesDeviceQueryData,
        Ref<'_, circuit::SubcircuitDevice>,
    )>,
    q_terminals: Query<PositionTerminalNodesDeviceQueryData, With<circuit::TerminalDevice>>,
    q_symbols: Query<Ref<Symbol>>,
    mut q_nodes: Query<&mut Position, (With<meta::TerminalNode>, Without<circuit::Device>)>,
) {
    // Position the terminals of subcircuit devices
    for (qi_subcircuit, comp_subcircuit) in q_subcircuits.iter() {
        let Ok(comp_symbol) = q_symbols.get(comp_subcircuit.circuit) else {
            tracing::trace!(
                ?qi_subcircuit.entity,
                "Skipping this subcircuit device because \
                the target circuit does not have a `Symbol`"
            );
            continue;
        };

        if !qi_subcircuit.comp_device.is_changed()
            && !comp_subcircuit.is_changed()
            && !comp_symbol.is_changed()
            && !qi_subcircuit.comp_position.is_changed()
            && !qi_subcircuit.comp_rotation.is_changed()
        {
            tracing::trace!(
                ?qi_subcircuit.entity,
                "Skipping this subcircuit device because \
                the inputs did not change since the last system execution"
            );
            continue;
        }

        tracing::trace!(
            ?qi_subcircuit.entity,
            subcircuit.comp_device.is_changed = qi_subcircuit.comp_device.is_changed(),
            subcircuit.comp_position.is_changed = qi_subcircuit.comp_position.is_changed(),
            subcircuit.comp_rotation.is_changed = qi_subcircuit.comp_rotation.is_changed(),
            comp_subcircuit.is_changed = comp_subcircuit.is_changed(),
            ?comp_subcircuit.circuit,
            comp_symbol.is_changed = comp_symbol.is_changed(),
        );

        position_subcircuit_terminal_nodes(&mut q_nodes, &qi_subcircuit, &comp_symbol);
    }

    // Position the terminals of terminal devices
    for qi_terminal in q_terminals.iter() {
        if !qi_terminal.comp_device.is_changed() && !qi_terminal.comp_position.is_changed() {
            tracing::trace!(
                ?qi_terminal.entity,
                "Skipping this terminal device because \
                the inputs did not change since the last system execution"
            );
            continue;
        }

        tracing::trace!(
            ?qi_terminal.entity,
            subcircuit.comp_device.is_changed = qi_terminal.comp_device.is_changed(),
            subcircuit.comp_position.is_changed = qi_terminal.comp_position.is_changed(),
        );

        position_terminal_terminal_node(&mut q_nodes, &qi_terminal);
    }

    // Wire devices do not have fixed terminal nodes.

    // Element devices do not support schematic rendering (at the moment)
}

/// The Bevy system to update the [`Position`] of the entities with
/// [`meta::TerminalNode`] belonging to specific device entities.
///
/// Does not perform change detection.
#[tracing::instrument(level = "trace", skip_all)]
pub(crate) fn position_terminal_nodes_specific(
    InRef(ent_devices): InRef<[Entity]>,
    q_subcircuits: Query<(
        PositionTerminalNodesDeviceQueryData,
        Ref<'_, circuit::SubcircuitDevice>,
    )>,
    q_terminals: Query<PositionTerminalNodesDeviceQueryData, With<circuit::TerminalDevice>>,
    q_symbols: Query<&Symbol>,
    mut q_nodes: Query<&mut Position, (With<meta::TerminalNode>, Without<circuit::Device>)>,
) {
    for &ent_device in ent_devices {
        if let Ok((qi_subcircuit, comp_subcircuit)) = q_subcircuits.get(ent_device) {
            let Ok(comp_symbol) = q_symbols.get(comp_subcircuit.circuit) else {
                tracing::trace!(
                    ?qi_subcircuit.entity,
                    "Skipping this subcircuit device because \
                    the target circuit does not have a `Symbol`"
                );
                continue;
            };

            tracing::trace!(?qi_subcircuit.entity);

            position_subcircuit_terminal_nodes(&mut q_nodes, &qi_subcircuit, comp_symbol);
        } else if let Ok(qi_terminal) = q_terminals.get(ent_device) {
            tracing::trace!(?qi_terminal.entity);

            position_terminal_terminal_node(&mut q_nodes, &qi_terminal);
        }

        // Wire devices do not have fixed terminal nodes.

        // Element devices do not support schematic rendering (at the moment)
    }
}

/// Position the terminals of a subcircuit device.
#[tracing::instrument(level = "trace", skip_all)]
fn position_subcircuit_terminal_nodes(
    q_nodes: &mut Query<&mut Position, (With<meta::TerminalNode>, Without<circuit::Device>)>,
    qi_subcircuit: &PositionTerminalNodesDeviceQueryDataItem,
    comp_symbol: &Symbol,
) {
    for (&ent_node, lay_terminal) in izip!(
        qi_subcircuit.comp_device.terminals.iter(),
        &comp_symbol.terminals
    ) {
        let Ok(mut comp_node_pos) = q_nodes.get_mut(ent_node) else {
            tracing::trace!(
                ?qi_subcircuit.entity,
                ?ent_node,
                "A terminal of this subcircuit device connects to a \
                non-existent node entity or one lacking `TerminalNode`"
            );
            continue;
        };

        let new_pos = qi_subcircuit.comp_position.0
            + qi_subcircuit.comp_rotation.apply(lay_terminal.position);
        if new_pos != comp_node_pos.0 {
            comp_node_pos.0 = new_pos;
        }
    }
}

/// Position the terminal of a terminal device.
#[tracing::instrument(level = "trace", skip_all)]
fn position_terminal_terminal_node(
    q_nodes: &mut Query<&mut Position, (With<meta::TerminalNode>, Without<circuit::Device>)>,
    qi_terminal: &PositionTerminalNodesDeviceQueryDataItem,
) {
    let [ent_node] = qi_terminal.comp_device.terminals[..] else {
        unreachable!("terminal device must have one terminal")
    };

    let Ok(mut comp_node_pos) = q_nodes.get_mut(ent_node) else {
        tracing::trace!(
            ?qi_terminal.entity,
            ?ent_node,
            "A terminal of this subcircuit device connects to a \
            non-existent node entity or one lacking `TerminalNode`"
        );
        return;
    };

    let new_pos = qi_terminal.comp_position.0;
    if new_pos != comp_node_pos.0 {
        comp_node_pos.0 = new_pos;
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use proptest::prelude::*;

    pub(crate) fn any_rotation() -> impl Strategy<Value = Rotation> {
        prop::bits::u8::masked(Rotation::all().bits()).prop_map(Rotation::from_bits_truncate)
    }

    proptest! {
        #[test]
        fn pt_rotation_inverse(rot in any_rotation()) {
            let rot_inv = rot.inverse();
            prop_assert_eq!(
                rot * rot_inv,
                Rotation::empty(),
                "{:?} (`rot.inverse()`) is not inverse of `rot`",
                rot_inv,
            );
        }

        #[test]
        fn pt_rotation_combine(
            rot_before in any_rotation(),
            rot_after in any_rotation(),
            vec in prop::array::uniform2(-4..=4),
        ) {
            let rot = rot_after * rot_before;
            prop_assert_eq!(
                rot.apply(vec),
                rot_after.apply(rot_before.apply(vec))
            );
        }
    }
}
