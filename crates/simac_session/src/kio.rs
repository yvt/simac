//! KDL ([`kdl`]) I/O for circuits ([`circuit`]), their layout ([`layout`]),
//! their programs ([`expr`]), and simulation settings ([`simulate`]).
//!
//! # ID collisions
//!
//! `[tag:kio_id_collision]`
//! In a KDL file, objects refer to other objects by their string IDs. It is
//! up to this module's users to resolve ID collisions prior to saving.
//! Failure to do so might result in a corrupted file.
//! The types of IDs with a risk of collision include:
//!
//! - `[ref:kio_id_circuit_item]` The [`meta::Id`]s of subcircuit, element,
//!   and terminal devices and non-terminal nodes.
//!
//!     - `[ref:kio_terminal_node_ignores_id]` The terminal nodes of non-wire
//!       devices ([`meta::TerminalNode`]) can not have [`meta::Id`]s.
//!       They are always referred to by combinations of device and terminal
//!       IDs.
//!
//! - TODO: Document other string IDs
//!
//! [`meta::assign_ids`] and [`ops::assign_ids`] can be used to resolve all
//! collisions in a [`World`].
use bevy_ecs::prelude::*;
use macro_rules_attribute::derive;
use simac_operator::{StashCx, StashableComponent, VirtEntity};
use std::sync::Arc;

#[cfg(doc)]
use crate::{circuit, expr, layout, meta, ops, simulate};

mod ast;
mod load;
mod save;
pub mod test_utils;
pub use ast::{ErrorKind as ParseErrorKind, ParseError};
pub use load::*;
pub use save::*;

// Components
// -------------------------------------------------------------------------

/// The component attached to circuit entities ([`crate::circuit::Circuit`]) to
/// indicate the containing files ([`File`]).
#[derive(Component, Debug)]
pub struct FileMember(pub Entity);

/// The component for loaded files.
///
/// # Stashing
///
/// Entities containing `File` reference each other by [`Import::ent_file`].
///
/// File entities are referenced by their members by [`FileMember`].
#[derive(Component, Debug)]
pub struct File<Path> {
    pub path: Path,
    /// Import directives found in the original file.
    ///
    /// These are used to maintain the relationship between original import IDs
    /// ([`Import::id`]) and imported files ([`Import::ent_file`]) when saving
    /// the file, even after the imported file paths change.
    pub imports: Vec<Import>,
}

/// An import directive.
#[derive(Debug, Clone)]
pub struct Import {
    pub id: String,
    pub ent_file: Entity,
}

/// The optional component for loaded file, circuit, and circuit item entities,
/// etc., to store the original KDL DOM, used for format-preserving
/// serialization.
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct OriginalKdl(Arc<kdlplus::Arena>);

/// The component to associate [`File`] with an AST node.
/// Requires [`OriginalKdl`].
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct OriginalAstFile(ast::File);

/// The component to associate [`circuit::Circuit`] with an AST node.
/// Requires [`OriginalKdl`].
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct OriginalAstCircuit(ast::Circuit);

/// The component to associate circuit item entities with an AST node.
/// Requires [`OriginalKdl`].
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct OriginalAstItem(ast::Item);

impl StashableComponent for FileMember {
    type Stashed = VirtEntity;

    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed {
        cx.entity_to_virt(self.0)
    }

    fn unstash(stashed: Self::Stashed, cx: &mut dyn StashCx) -> Self {
        Self(cx.entity_to_phys(stashed))
    }
}

/// Used by the [`trait@StashableComponent`] implementation of [`Path`].
#[derive(Debug)]
pub struct FileStashed<Path> {
    path: Path,
    imports: Vec<(String, VirtEntity)>,
}

impl<Path> StashableComponent for File<Path>
where
    Path: Clone + std::fmt::Debug + Send + Sync + 'static,
{
    type Stashed = FileStashed<Path>;

    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed {
        let Self { path, imports } = self;
        FileStashed {
            path: path.clone(),
            imports: imports
                .iter()
                .map(|import| (import.id.clone(), cx.entity_to_virt(import.ent_file)))
                .collect(),
        }
    }

    fn unstash(FileStashed { path, imports }: Self::Stashed, cx: &mut dyn StashCx) -> Self {
        Self {
            path,
            imports: imports
                .into_iter()
                .map(|(id, ent_file)| Import {
                    id,
                    ent_file: cx.entity_to_phys(ent_file),
                })
                .collect(),
        }
    }
}
