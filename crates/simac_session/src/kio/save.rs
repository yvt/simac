use bevy_ecs::{entity::EntityHashMap, prelude::*};
use bevy_utils::hashbrown::hash_map;
use fxhash::FxHashMap;
use itertools::{chain, izip};
use slotmap::Key;
use std::{error::Error, sync::Arc};
use try_match::match_ok;
use typed_index::{IndexExt, VecExt as _, impl_typed_index};

use crate::{
    circuit, expr,
    kio::{
        File, FileMember, Import, OriginalAstCircuit, OriginalAstFile, OriginalAstItem,
        OriginalKdl, ast,
        load::{Env, Path},
    },
    layout, meta, render, sim_control,
};

mod sort;

pub trait PresaveEnv: Env {
    /// Generate a default import ID for a given file path.
    fn default_import_id_for_path(&self, _referer: &Self::Path, _target: &Self::Path) -> String {
        "import".to_owned()
    }
}

#[expect(async_fn_in_trait)]
pub trait SaveEnv: Env {
    /// Construct a path string that, when [interpreted][1] in the context of
    /// `referer`, refers to `target`.
    ///
    /// If `old_path_str` is given, this method may return it as-is if it's a
    /// valid result.
    ///
    /// [1]: super::PreloadEnv::resolve_path
    fn relativize_path(
        &self,
        referer: &Self::Path,
        target: &Self::Path,
        old_path_str: Option<&str>,
    ) -> Result<String, Self::RelativizePathError>;

    type RelativizePathError: Error;

    async fn write_file(
        &self,
        ent_file: Entity,
        path: &Self::Path,
        data: Vec<u8>,
    ) -> Result<(), Self::WriteFileError>;

    type WriteFileError: Error;
}

#[derive(Debug, thiserror::Error)]
pub enum SaveError<Path, RelativizePathError, WriteFileError> {
    #[error("failed to relativize path {target:?} in {referer:?}: {error}")]
    RelativizePath {
        #[source]
        error: RelativizePathError,
        referer: Path,
        target: Path,
    },
    #[error("failed to read file {path:?}: {error}")]
    WriteFile {
        #[source]
        error: WriteFileError,
        path: Path,
    },
}

/// A structure holding circuits in an intermediate form that are yet to be
/// committed to disk.
#[derive(Debug)]
pub struct SaveCx<Path> {
    files: Vec<PresaveFile<Path>>,
    /// The paths of imported files.
    file_path_map: EntityHashMap<Path>,
}

#[derive(Debug)]
struct PresaveFile<Path> {
    path: Path,
    ent_file: Entity,
    kp_arena_old: Option<Arc<kdlplus::Arena>>,
    ast_file_new: ast::File,
    // FIXME: Avoid copying whole `ast::File` from `OriginalAstFile`
    ast_file_old: Option<ast::File>,
    imports: Imports,
}

#[derive(Debug, bevy_ecs::system::SystemParam)]
pub struct PresaveParam<'w, 's, Path: self::Path> {
    q_id: Query<'w, 's, &'static meta::Id>,
    q_file: Query<'w, 's, FileQueryData<Path>>,
    q_workspace: Query<'w, 's, WorkspaceQueryData>,
    q_circuit: Query<'w, 's, CircuitQueryData>,
    q_target_circuit: Query<'w, 's, TargetCircuitQueryData>,
    q_meta_circuit: Query<'w, 's, &'static meta::CircuitMeta>,
    q_node: Query<'w, 's, NodeQueryData>,
    q_terminal_node: Query<'w, 's, &'static meta::TerminalNode>,
    q_device: Query<'w, 's, DeviceQueryData>,
    q_target_device: Query<'w, 's, TargetDeviceQueryData>,
}

#[derive(bevy_ecs::query::QueryData)]
struct FileQueryData<Path: self::Path> {
    comp_file: &'static File<Path>,
    comp_original_kdl: Option<&'static OriginalKdl>,
    comp_original_ast_file: Option<&'static OriginalAstFile>,
}

#[derive(bevy_ecs::query::QueryData)]
struct WorkspaceQueryData {
    ent: Entity,
    comp_sim_control: &'static sim_control::SimControl,
    comp_render_opts: &'static render::RenderOpts,
    comp_file_member: &'static FileMember,
}

#[derive(bevy_ecs::query::QueryData)]
struct CircuitQueryData {
    ent: Entity,
    comp_circuit: &'static circuit::Circuit,
    comp_meta_id: Option<&'static meta::Id>,
    comp_meta_circuit: Option<&'static meta::CircuitMeta>,
    comp_file_member: &'static FileMember,
    comp_original_kdl: Option<&'static OriginalKdl>,
    comp_original_ast_circuit: Option<&'static OriginalAstCircuit>,
}

#[derive(bevy_ecs::query::QueryData)]
struct TargetCircuitQueryData {
    comp_meta_id: &'static meta::Id,
    comp_file_member: &'static FileMember,
    comp_meta_circuit: Option<&'static meta::CircuitMeta>,
}

#[derive(bevy_ecs::query::QueryData)]
struct NodeQueryData {
    _comp_node: &'static circuit::Node,
    has_ground_node: Has<circuit::GroundNode>,
    comp_layout_position: Option<&'static layout::Position>,
    comp_meta_id: Option<&'static meta::Id>,
    has_meta_terminal_node: Has<meta::TerminalNode>,
    comp_original_kdl: Option<&'static OriginalKdl>,
    comp_original_ast_item: Option<&'static OriginalAstItem>,
}

#[derive(bevy_ecs::query::QueryData)]
struct DeviceQueryData {
    comp_device: &'static circuit::Device,
    comp_subcircuit_device: Option<&'static circuit::SubcircuitDevice>,
    comp_terminal_device: Option<&'static circuit::TerminalDevice>,
    comp_element_device: Option<&'static circuit::ElementDevice>,
    comp_wire_device: Option<&'static circuit::WireDevice>,
    comp_layout_position: Option<&'static layout::Position>,
    comp_layout_rotation: Option<&'static layout::Rotation>,
    comp_expr_subcircuit_device: Option<&'static expr::SubcircuitDevice>,
    comp_expr_element_device: Option<&'static expr::ElementDevice>,
    comp_meta_id: Option<&'static meta::Id>,
    comp_original_kdl: Option<&'static OriginalKdl>,
    comp_original_ast_item: Option<&'static OriginalAstItem>,
}

#[derive(bevy_ecs::query::QueryData)]
struct TargetDeviceQueryData {
    comp_meta_id: &'static meta::Id,
    comp_device: &'static circuit::Device,
    comp_subcircuit_device: Option<&'static circuit::SubcircuitDevice>,
    is_element_device: Has<circuit::ElementDevice>,
    is_terminal_device: Has<circuit::TerminalDevice>,
}

impl<Path: self::Path> SaveCx<Path> {
    /// Extract serializable data from the specified file entities and their
    /// members.
    #[tracing::instrument(skip_all)]
    pub fn presave<PE: PresaveEnv<Path = Path>>(
        env: &PE,
        param: &PresaveParam<'_, '_, Path>,
        ent_files: &[Entity],
    ) -> Self {
        let mut file_map: EntityHashMap<PresaveFile<Path>> = <_>::default();

        for &ent_file in ent_files {
            let qd_file = param.q_file.get(ent_file).unwrap_or_else(|e| {
                panic!("unable to get components of file entity {ent_file:?}: {e}")
            });

            let ast_file_new = ast::File {
                kp_doc_i: kdlplus::DocumentI::null(), // set later
                meta: ast::FileMeta {
                    version: Some(ast::VERSION),
                },
                workspace: None,      // set later
                imports: Vec::new(),  // set later
                circuits: Vec::new(), // set later
            };

            let path = qd_file.comp_file.path.clone();

            tracing::debug!(?ent_file, ?path, "Found a file to save");

            file_map.insert(
                ent_file,
                PresaveFile {
                    path,
                    ent_file,
                    kp_arena_old: qd_file.comp_original_kdl.map(|comp| Arc::clone(&comp.0)),
                    ast_file_new,
                    ast_file_old: qd_file.comp_original_ast_file.map(|comp| comp.0.clone()),
                    imports: Imports::new(&qd_file.comp_file.imports),
                },
            );
        }

        for WorkspaceQueryDataItem {
            ent,
            comp_sim_control,
            comp_render_opts,
            comp_file_member,
        } in param.q_workspace.iter()
        {
            let Some(file) = file_map.get_mut(&comp_file_member.0) else {
                continue;
            };

            tracing::debug!(
                ent_file = ?file.ent_file,
                ?ent,
                "Found a workspace entity",
            );

            let root_circuit_id = match param.q_id.get(comp_sim_control.ent_root_circuit) {
                Ok(id) => id.0.clone(),
                Err(_) => {
                    tracing::warn!(
                        ?comp_sim_control.ent_root_circuit,
                        "The root circuit does not have an ID",
                    );
                    String::new()
                }
            };

            file.ast_file_new.workspace = Some(ast::Workspace {
                root_circuit_id,
                time_step: Some(comp_sim_control.time_step),
                time_scale: Some(comp_sim_control.time_scale),
                current_speed: Some(comp_render_opts.current_speed),
                potential_color_range: Some(comp_render_opts.potential_color_range),
            });
        }

        for qd_circuit @ CircuitQueryDataItem {
            ent,
            comp_file_member,
            comp_meta_id,
            ..
        } in param.q_circuit.iter()
        {
            let Some(file) = file_map.get_mut(&comp_file_member.0) else {
                continue;
            };
            let comp_file = param.q_file.get(comp_file_member.0).unwrap().comp_file;

            tracing::debug!(
                ent_file = ?file.ent_file,
                ?ent,
                "Found a circuit entity"
            );

            let _span = tracing::info_span!(
                "presave_circuit",
                ent_file = ?file.ent_file,
                ?ent,
                id = ?comp_meta_id,
            )
            .entered();
            Self::presave_circuit(env, param, qd_circuit, file, comp_file);
        }

        // Sort circuits to preserve the original order
        for file in file_map.values_mut() {
            let circuits = &mut file.ast_file_new.circuits;
            let old_circuits = file.ast_file_old.as_ref().map_or(&[][..], |x| &x.circuits);
            sort::sort_preserving(circuits, old_circuits, ExtractKeyImpl);

            tracing::debug!(
                circuits.kp_node_i = ?circuits.iter()
                    .map(|item| item.kp_node_i)
                    .collect::<Vec<_>>(),
                old_circuits.kp_node_i = ?old_circuits.iter()
                    .map(|item| item.kp_node_i)
                    .collect::<Vec<_>>(),
                "Sorted circuits to preserve the original order"
            );
        }

        // Collect the paths of imported files
        let mut file_path_map: EntityHashMap<Path> = <_>::default();
        for import in file_map
            .values()
            .flat_map(|presave_file| &presave_file.imports.imports)
        {
            file_path_map.entry(import.ent_file).or_insert_with(|| {
                let qd_file = param.q_file.get(import.ent_file).unwrap();
                qd_file.comp_file.path.clone()
            });
        }

        Self {
            files: file_map.into_values().collect(),
            file_path_map,
        }
    }

    /// An inner function of [`Self::presave`].
    fn presave_circuit<PE: PresaveEnv<Path = Path>>(
        env: &PE,
        param: &PresaveParam<'_, '_, Path>,
        CircuitQueryDataItem {
            ent: _,
            comp_circuit:
                circuit::Circuit {
                    devices,
                    nodes,
                    num_params: _,
                },
            comp_meta_id,
            comp_meta_circuit,
            comp_file_member: _,
            comp_original_kdl,
            comp_original_ast_circuit,
        }: CircuitQueryDataItem<'_>,
        file: &mut PresaveFile<Path>,
        comp_file: &File<Path>,
    ) {
        let mut items = Vec::new();

        // Find the old KDL nodes of this circuit and the items that do not
        // have `OriginalAstItem`
        let old_items: &[ast::Item];
        let kp_node_i: kdlplus::NodeI;
        match (
            comp_original_kdl,
            comp_original_ast_circuit,
            &file.kp_arena_old,
        ) {
            (Some(comp_original_kdl), Some(comp_original_ast_circuit), Some(kp_arena_old))
                if Arc::ptr_eq(&comp_original_kdl.0, kp_arena_old) =>
            {
                old_items = &comp_original_ast_circuit.0.items[..];
                kp_node_i = comp_original_ast_circuit.0.kp_node_i;
            }
            _ => {
                // TODO: We could copy the original nodes into the
                // new arena
                old_items = &[];
                kp_node_i = kdlplus::NodeI::null();
            }
        }

        // Add parameter definition items to `items`
        let old_param_defs: FxHashMap<&str, &ast::ParamDef> = old_items
            .iter()
            .filter_map(match_ok!(, ast::Item::ParamDef(x) => (x.id.as_str(), x)))
            .collect();
        for param_def in comp_meta_circuit.map_or(&[][..], |x| &x.params) {
            let old_param_def = old_param_defs.get(param_def.id.as_str());
            items.push(ast::Item::ParamDef(ast::ParamDef {
                kp_node_i: old_param_def
                    .map(|x| x.kp_node_i)
                    .unwrap_or(kdlplus::NodeI::null()),
                id: param_def.id.clone(),
                // If `default_val` equals `+0.0` (default value) and
                // the old item did not specify it, keep it unspecified
                default_val: (param_def.default_val.to_bits() != 0
                    || old_param_def.is_some_and(|x| x.default_val.is_some()))
                .then_some(param_def.default_val),
                // If `description` is empty (default value) and
                // the old item did not specify it, keep it unspecified
                description: (!param_def.description.is_empty()
                    || old_param_def.is_some_and(|x| x.description.is_some()))
                .then(|| param_def.description.clone()),
            }));
        }

        // Add device items to `items`
        let mut cx = PresaveCircuitCx {
            env,
            param,
            file,
            comp_file,
            items: &mut items,
        };

        for &ent_device in devices.iter() {
            cx.presave_device(ent_device);
        }

        // Add node items to `items`
        for &ent_node in nodes.iter() {
            cx.presave_node(ent_node);
        }

        // Keep symbol item
        if let Some(old_item) = old_items
            .iter()
            .find_map(match_ok!(, ast::Item::Symbol(_0)))
        {
            items.push(ast::Item::Symbol(ast::Symbol {
                kp_node_i: old_item.kp_node_i,
                // TODO: Save the contents of symbol item. We currently
                // rely on the unparsing code to leave it alone
                // [ref:unparse_symbol]
                items: Vec::new(),
            }))
        }

        // Sort new `items` by `ItemKey`
        //
        // `ItemKey` is typically unique to each item, so this makes `items`
        // invariant over hashtable iteration order.
        // This has little value for normal usage, but without this,
        // snapshot tests could randomly fail.
        items.sort_unstable_by(|x, y| Ord::cmp(&ItemKey::from_item(x), &ItemKey::from_item(y)));

        // Preserve original order of preexisting `items`
        let old_items = comp_original_ast_circuit
            .as_ref()
            .map_or(&[][..], |x| &x.0.items);
        sort::sort_preserving(&mut items, old_items, ExtractKeyImpl);

        tracing::debug!(
            items.kp_node_i = ?items.iter()
                .map(|item| item.kp_node_i())
                .collect::<Vec<_>>(),
            old_items.kp_node_i = ?old_items.iter()
                .map(|item| item.kp_node_i())
                .collect::<Vec<_>>(),
            "Sorted items to preserve the original order"
        );

        let id = comp_meta_id.map(|x| x.0.clone()).unwrap_or_else(|| {
            tracing::warn!("The circuit entity is missing `Id`");
            String::new()
        });

        file.ast_file_new.circuits.push(ast::Circuit {
            kp_node_i,
            id,
            items,
        });
    }

    pub async fn save<SE: SaveEnv<Path = Path>>(
        self,
        env: &SE,
    ) -> Result<(), SaveError<Path, SE::RelativizePathError, SE::WriteFileError>> {
        let files = self
            .files
            .into_iter()
            .map(|presave_file| Self::save_file(env, &self.file_path_map, presave_file));

        futures::future::try_join_all(files).await?;

        Ok(())
    }

    /// An inner function of [`Self::save`] to process a single file.
    #[tracing::instrument(skip_all, fields(ent_file = ?ent_file))]
    async fn save_file<SE: SaveEnv<Path = Path>>(
        env: &SE,
        file_path_map: &EntityHashMap<Path>,
        PresaveFile {
            path,
            ent_file,
            kp_arena_old,
            mut ast_file_new,
            ast_file_old,
            imports,
        }: PresaveFile<Path>,
    ) -> Result<(), SaveError<Path, SE::RelativizePathError, SE::WriteFileError>> {
        tracing::debug!(?ent_file);

        let mut kp_arena = kp_arena_old
            .map(|x| kdlplus::Arena::clone(&x))
            .unwrap_or_default();

        // Set `ast::File::imports`
        let old_imports: FxHashMap<&str, _> = ast_file_old
            .as_ref()
            .map_or(&[][..], |x| &x.imports)
            .iter()
            .map(|import| (import.id.as_str(), import))
            .collect();

        ast_file_new.imports = imports
            .imports
            .into_iter()
            .map(|import| {
                // The old path string; preserve it if possible
                let old_path_str = old_imports.get(import.id.as_str()).map(|x| x.path.as_str());

                // The target file path
                let target = &file_path_map[&import.ent_file];

                // Relativize the target file path
                let target = env
                    .relativize_path(&path, target, old_path_str)
                    .map_err(|error| SaveError::RelativizePath {
                        error,
                        referer: path.clone(),
                        target: target.clone(),
                    })?;

                Ok(ast::Import {
                    id: import.id,
                    path: target,
                })
            })
            .collect::<Result<_, _>>()?;

        // Convert `ast::File` back to a KDL document
        if let Some(ast_file_old) = &ast_file_old {
            // Overwrite the existing KDL document
            ast_file_new.kp_doc_i = ast_file_old.kp_doc_i;
        } else {
            // Create a new KDL document
            ast_file_new.kp_doc_i = kp_arena.documents.insert(<_>::default());
        }
        ast_file_new.unparse(&mut (), &mut kp_arena);

        // Convert the KDL document to KDL source code
        let k_doc = kp_arena.export_kdl_document(ast_file_new.kp_doc_i);
        tracing::debug!(?k_doc);

        let data = k_doc.to_string();

        // Save the KDL source code
        env.write_file(ent_file, &path, data.into())
            .await
            .map_err(|error| SaveError::WriteFile { error, path })
    }
}

/// Variables shared by inner functions of [`SaveCx::presave_circuit`].
struct PresaveCircuitCx<'a, 'w, 's, Path: self::Path, PE> {
    env: &'a PE,
    param: &'a PresaveParam<'w, 's, Path>,
    file: &'a mut PresaveFile<Path>,
    comp_file: &'a File<Path>,
    items: &'a mut Vec<ast::Item>,
}

impl<Path: self::Path, PE: PresaveEnv<Path = Path>> PresaveCircuitCx<'_, '_, '_, Path, PE> {
    /// An inner function of [`SaveCx::presave_circuit`] to convert a device
    /// entity into an [`ast::Item`].
    #[tracing::instrument(skip(self))]
    fn presave_device(&mut self, ent_device: Entity) {
        let Ok(DeviceQueryDataItem {
            comp_device,
            comp_subcircuit_device,
            comp_terminal_device,
            comp_element_device,
            comp_wire_device,
            comp_layout_position,
            comp_layout_rotation,
            comp_expr_subcircuit_device,
            comp_expr_element_device,
            comp_meta_id,
            comp_original_kdl,
            comp_original_ast_item,
        }) = self.param.q_device.get(ent_device)
        else {
            return tracing::warn!(?ent_device, "The device entity could not be found");
        };

        let kp_node_i = match (
            comp_original_kdl,
            comp_original_ast_item,
            &self.file.kp_arena_old,
        ) {
            (Some(comp_original_kdl_of_node), Some(comp_original_ast_item), Some(kp_arena_old))
                if Arc::ptr_eq(&comp_original_kdl_of_node.0, kp_arena_old) =>
            {
                comp_original_ast_item.0.kp_node_i()
            }
            _ => {
                // TODO: We could copy the original node into the
                // new arena
                kdlplus::NodeI::null()
            }
        };

        if let Some(comp_subcircuit_device) = comp_subcircuit_device {
            return self.presave_device_subcircuit(
                ent_device,
                kp_node_i,
                comp_subcircuit_device,
                comp_layout_position,
                comp_layout_rotation,
                comp_expr_subcircuit_device,
                comp_meta_id,
                comp_original_ast_item,
            );
        }

        if let Some(comp_element_device) = comp_element_device {
            return self.presave_device_element(
                ent_device,
                kp_node_i,
                comp_element_device,
                comp_layout_position,
                comp_layout_rotation,
                comp_expr_element_device,
                comp_meta_id,
                comp_original_ast_item,
            );
        }

        if let Some(comp_terminal_device) = comp_terminal_device {
            return self.presave_device_terminal(
                ent_device,
                kp_node_i,
                comp_terminal_device,
                comp_layout_position,
                comp_layout_rotation,
                comp_meta_id,
                comp_original_ast_item,
            );
        }

        if let Some(comp_wire_device) = comp_wire_device {
            return self.presave_device_wire(
                kp_node_i,
                comp_device,
                comp_wire_device,
                comp_meta_id,
            );
        }

        tracing::warn!(?ent_device, "An unknown type of device entity");
    }

    /// An inner function of [`Self::presave_device`]
    /// to process a subcircuit device.
    #[tracing::instrument(skip_all, name = "subcircuit")]
    fn presave_device_subcircuit(
        &mut self,
        ent_device: Entity,
        kp_node_i: kdlplus::NodeI,
        comp_subcircuit_device: &circuit::SubcircuitDevice,
        comp_layout_position: Option<&layout::Position>,
        comp_layout_rotation: Option<&layout::Rotation>,
        comp_expr_subcircuit_device: Option<&expr::SubcircuitDevice>,
        comp_meta_id: Option<&meta::Id>,
        comp_original_ast_item: Option<&OriginalAstItem>,
    ) {
        let old_item =
            comp_original_ast_item.and_then(|x| match_ok!(&x.0, ast::Item::Subcircuit(_0)));

        // [tag:kio_subcircuit_device_needs_id]
        let id = get_or_warn_id(ent_device, comp_meta_id);

        let Ok(qd_target_circuit) = self
            .param
            .q_target_circuit
            .get(comp_subcircuit_device.circuit)
        else {
            return tracing::warn!(
                ?ent_device,
                circuit = ?comp_subcircuit_device.circuit,
                "The target circuit does not exist or is lacking components",
            );
        };

        let Ok(qd_target_file) = self.param.q_file.get(qd_target_circuit.comp_file_member.0) else {
            return tracing::warn!(
                ent_target_circuit = ?comp_subcircuit_device.circuit,
                ent_file = ?qd_target_circuit.comp_file_member.0,
                circuit = ?comp_subcircuit_device.circuit,
                "The target circuit does not exist or is lacking components",
            );
        };
        let target_file_id = self.file.imports.get_or_insert_import_id(
            self.env,
            qd_target_circuit.comp_file_member.0,
            qd_target_file.comp_file,
            self.comp_file,
        );

        let target_circuit_id = qd_target_circuit.comp_meta_id.0.clone();

        let layout = convert_device_layout(
            comp_layout_position,
            comp_layout_rotation,
            old_item.map(|x| &x.layout),
        );

        let mut params: Vec<ast::Param> = izip!(
            chain!(
                comp_expr_subcircuit_device.map_or(&[][..], |x| &x.params),
                // Treat missing parameter values as `None` (unspecified)
                std::iter::repeat_with(|| &None),
            ),
            qd_target_circuit
                .comp_meta_circuit
                .map_or(&[][..], |x| &x.params)
        )
        .filter_map(|(expr, param_def)| {
            let value = expr.clone()?;
            Some(ast::Param {
                id: param_def.id.clone(),
                value,
            })
        })
        .collect();

        // Preserve the old order in `params`
        sort::sort_preserving(
            &mut params,
            old_item.map_or(&[][..], |x| &x.params),
            ExtractKeyImpl,
        );

        self.items.push(ast::Item::Subcircuit(ast::Subcircuit {
            kp_node_i,
            id,
            target_file_id: target_file_id.to_owned(),
            target_circuit_id,
            layout,
            params,
        }));
    }

    /// An inner function of [`Self::presave_device`]
    /// to process an element device.
    #[tracing::instrument(skip_all, name = "element")]
    fn presave_device_element(
        &mut self,
        ent_device: Entity,
        kp_node_i: kdlplus::NodeI,
        comp_element_device: &circuit::ElementDevice,
        comp_layout_position: Option<&layout::Position>,
        comp_layout_rotation: Option<&layout::Rotation>,
        comp_expr_element_device: Option<&expr::ElementDevice>,
        comp_meta_id: Option<&meta::Id>,
        comp_original_ast_item: Option<&OriginalAstItem>,
    ) {
        let old_item = comp_original_ast_item.and_then(|x| match_ok!(&x.0, ast::Item::Element(_0)));

        // [tag:kio_element_device_needs_id]
        let id = get_or_warn_id(ent_device, comp_meta_id);

        let ty = comp_element_device.elem_ty;

        let mut params: Vec<ast::Param> = izip!(
            chain!(
                comp_expr_element_device.map_or(&[][..], |x| &x.params),
                // Treat missing parameter values as `None` (unspecified)
                std::iter::repeat_with(|| &None),
            ),
            0..ty.num_params(),
        )
        .filter_map(|(expr, param_i)| {
            let value = expr.clone()?;
            let Some(id) = ty.param_name(param_i) else {
                tracing::warn!(param_i, "Unnamed parameter cannot be referred to");
                return None;
            };
            Some(ast::Param {
                id: id.to_owned(),
                value,
            })
        })
        .collect();

        // Preserve the old order in `params`
        sort::sort_preserving(
            &mut params,
            old_item.map_or(&[][..], |x| &x.params),
            ExtractKeyImpl,
        );

        let layout = convert_device_layout(
            comp_layout_position,
            comp_layout_rotation,
            old_item.map(|x| &x.layout),
        );

        self.items.push(ast::Item::Element(ast::Element {
            kp_node_i,
            id,
            ty,
            params,
            layout,
        }));
    }

    /// An inner function of [`Self::presave_device`]
    /// to process a terminal device.
    #[tracing::instrument(skip_all, name = "terminal")]
    fn presave_device_terminal(
        &mut self,
        ent_device: Entity,
        kp_node_i: kdlplus::NodeI,
        _comp_terminal_device: &circuit::TerminalDevice,
        comp_layout_position: Option<&layout::Position>,
        comp_layout_rotation: Option<&layout::Rotation>,
        comp_meta_id: Option<&meta::Id>,
        comp_original_ast_item: Option<&OriginalAstItem>,
    ) {
        // [tag:kio_terminal_device_needs_id]
        let id = get_or_warn_id(ent_device, comp_meta_id);

        let layout = convert_device_layout(
            comp_layout_position,
            comp_layout_rotation,
            comp_original_ast_item
                .and_then(|x| match_ok!(&x.0, ast::Item::Terminal(x) => &x.layout)),
        );

        self.items.push(ast::Item::Terminal(ast::Terminal {
            kp_node_i,
            id,
            layout,
        }));
    }

    /// An inner function of [`Self::presave_device`]
    /// to process a wire device.
    #[tracing::instrument(skip_all, name = "wire")]
    fn presave_device_wire(
        &mut self,
        kp_node_i: kdlplus::NodeI,
        comp_device: &circuit::Device,
        _comp_wire_device: &circuit::WireDevice,
        comp_meta_id: Option<&meta::Id>,
    ) {
        let id = comp_meta_id.map(|x| x.0.clone());

        let [n0, n1] = comp_device.terminals[..] else {
            return tracing::warn!("The wire device does not have exactly two terminals");
        };
        let [Some(n0), Some(n1)] = [n0, n1].map(|e| self.ent_node_to_node_ref(e)) else {
            return;
        };

        self.items.push(ast::Item::Wire(ast::Wire {
            kp_node_i,
            id,
            nodes: [n0, n1],
        }));
    }

    #[tracing::instrument(skip(self))]
    fn ent_node_to_node_ref(&self, ent_node: Entity) -> Option<ast::NodeRef> {
        // If it's a non-terminal node, return its `id`
        let Ok(&meta::TerminalNode { device: ent_device }) =
            self.param.q_terminal_node.get(ent_node)
        else {
            let Ok(id) = self.param.q_id.get(ent_node) else {
                tracing::warn!(
                    ?ent_node,
                    "The referenced node is not a terminal node \
                    and does not have an ID"
                );
                return None;
            };
            return Some(ast::NodeRef {
                item_id: id.0.clone(),
                member: None,
            });
        };

        tracing::debug!(?ent_device);

        // Find the device owning the terminal node
        let Ok(TargetDeviceQueryDataItem {
            comp_meta_id,
            comp_device,
            comp_subcircuit_device,
            is_element_device,
            is_terminal_device,
        }) = self.param.q_target_device.get(ent_device)
        else {
            tracing::warn!(
                ?ent_node,
                ?ent_device,
                "The owner of the terminal node is not a valid device"
            );
            return None;
        };

        let Some(terminal_i) = comp_device.terminals.iter().position(|&e| e == ent_node) else {
            tracing::warn!(
                ?ent_node,
                ?ent_device,
                "Unable to find the terminal node in `Device::terminals`"
            );
            return None;
        };

        tracing::debug!(terminal_i);

        // Classify the device
        if let Some(comp_subcircuit_device) = comp_subcircuit_device {
            tracing::debug!("It's a subcircuit device");

            let ent_target_circuit = comp_subcircuit_device.circuit;
            let Ok(qd_meta_circuit) = self.param.q_meta_circuit.get(ent_target_circuit) else {
                tracing::warn!(
                    ?ent_node,
                    ?ent_device,
                    ?ent_target_circuit,
                    "The target circuit does not have `CircuitMeta`"
                );
                return None;
            };

            let Some(member) = qd_meta_circuit.terminal_names.get(terminal_i) else {
                tracing::warn!(
                    ?ent_node,
                    ?ent_device,
                    terminal_i,
                    "Unable to find the terminal node in `Device::terminals`"
                );
                return None;
            };

            Some(ast::NodeRef {
                item_id: comp_meta_id.0.clone(),
                member: Some(member.clone()),
            })
        } else if is_element_device {
            tracing::debug!("It's an element device");

            Some(ast::NodeRef {
                item_id: comp_meta_id.0.clone(),
                member: Some(terminal_i.to_string()),
            })
        } else if is_terminal_device {
            tracing::debug!("It's a terminal device");

            Some(ast::NodeRef {
                item_id: comp_meta_id.0.clone(),
                member: None,
            })
        } else {
            tracing::warn!(?ent_node, ?ent_device, "An unknown type of device");
            None
        }
    }

    /// An inner function of [`SaveCx::presave_circuit`] to convert a node
    /// entity into an [`ast::Item`].
    #[tracing::instrument(skip(self))]
    fn presave_node(&mut self, ent_node: Entity) {
        let Ok(NodeQueryDataItem {
            _comp_node: _,
            has_ground_node,
            comp_layout_position,
            comp_meta_id,
            has_meta_terminal_node,
            comp_original_kdl,
            comp_original_ast_item,
        }) = self.param.q_node.get(ent_node)
        else {
            return tracing::warn!(?ent_node, "The node entity could not be found");
        };

        if has_meta_terminal_node {
            // [tag:kio_terminal_node_ignores_id]
            // Terminal nodes are implicitly created along with
            // devices
            return tracing::trace!("Skipping the terminal node");
        }

        let kp_node_i = match (
            comp_original_kdl,
            comp_original_ast_item,
            &self.file.kp_arena_old,
        ) {
            (Some(comp_original_kdl_of_node), Some(comp_original_ast_item), Some(kp_arena_old))
                if Arc::ptr_eq(&comp_original_kdl_of_node.0, kp_arena_old) =>
            {
                comp_original_ast_item.0.kp_node_i()
            }
            _ => {
                // TODO: We could copy the original node into the
                // new arena
                kdlplus::NodeI::null()
            }
        };

        // [tag:kio_node_needs_id]
        let id = get_or_warn_id(ent_node, comp_meta_id);

        let is_ground = has_ground_node;

        let x = comp_layout_position.map(|p| p.0.x).unwrap_or(0);
        let y = comp_layout_position.map(|p| p.0.y).unwrap_or(0);

        // For each field, if the new value == the default value and
        // the old AST did not specify it, keep it unspecified
        let old_item = comp_original_ast_item.and_then(|x| match_ok!(&x.0, ast::Item::Node(_0)));
        let x = (x != 0 || old_item.is_some_and(|o| o.x.is_some())).then_some(x);
        let y = (y != 0 || old_item.is_some_and(|o| o.y.is_some())).then_some(y);

        self.items.push(ast::Item::Node(ast::Node {
            kp_node_i,
            id,
            is_ground,
            x,
            y,
        }));
    }
}

fn convert_device_layout(
    comp_layout_position: Option<&layout::Position>,
    comp_layout_rotation: Option<&layout::Rotation>,
    old: Option<&ast::DeviceLayout>,
) -> ast::DeviceLayout {
    let x = comp_layout_position.map(|p| p.0.x).unwrap_or(0);
    let y = comp_layout_position.map(|p| p.0.y).unwrap_or(0);
    let rotation = comp_layout_rotation.copied().unwrap_or_default();
    let (angle, flip) = rotation.to_angle_and_flip_x();
    ast::DeviceLayout {
        // For each field, if the new value == the default value and
        // the old AST did not specify it, keep it unspecified
        x: (x != 0 || old.is_some_and(|o| o.x.is_some())).then_some(x),
        y: (y != 0 || old.is_some_and(|o| o.y.is_some())).then_some(y),
        angle: (angle != layout::Angle::_0 || old.is_some_and(|o| o.angle.is_some()))
            .then_some(angle),
        // Add `flip=#true` or remove it altogether only if `flip` is
        // different from the old value
        flip: (flip || old.is_some_and(|o| o.flip == Some(flip))).then_some(flip),
    }
}

fn get_or_warn_id(ent: Entity, comp_meta_id: Option<&meta::Id>) -> String {
    let Some(comp_meta_id) = comp_meta_id else {
        tracing::warn!(?ent, "This entity is missing `Id`");
        return String::new();
    };
    comp_meta_id.0.clone()
}

/// A new set of import directives.
#[derive(Debug)]
struct Imports {
    imports: Vec<Import>,
    import_unused: Vec<bool>,
    ent_file_to_i: EntityHashMap<ImportI>,
}

/// [`Imports::imports`]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ImportI(usize);

impl_typed_index!(impl TypedIndex<Vec<Import>> for ImportI);

impl Imports {
    fn new(imports: &[Import]) -> Self {
        let imports = imports.to_owned();
        let ent_file_to_i = imports
            .iter_typed()
            .map(|(i, import)| (import.ent_file, i))
            .collect();
        Self {
            import_unused: vec![true; imports.len()],
            imports,
            ent_file_to_i,
        }
    }

    /// Get the import ID of a given file and mark it as used.
    fn get_or_insert_import_id<PE>(
        &mut self,
        env: &PE,
        ent_file: Entity,
        comp_file: &File<PE::Path>,
        comp_file_referrer: &File<PE::Path>,
    ) -> &str
    where
        PE: PresaveEnv,
    {
        let import_i = match self.ent_file_to_i.entry(ent_file) {
            hash_map::Entry::Occupied(entry) => *entry.get(),
            hash_map::Entry::Vacant(entry) => {
                let id = env.default_import_id_for_path(&comp_file_referrer.path, &comp_file.path);
                // TODO: Deduplication
                *entry.insert(self.imports.push_typed(Import { id, ent_file }))
            }
        };
        self.import_unused[import_i.0] = false;
        &self.imports.index_typed(import_i).id
    }
}

struct ExtractKeyImpl;

impl sort::ExtractKey<ast::Circuit> for ExtractKeyImpl {
    type Key<'a> = kdlplus::NodeI;

    fn extract_key<'a>(&self, item: &'a ast::Circuit) -> Self::Key<'a> {
        item.kp_node_i
    }

    #[inline]
    fn reborrow_ref<'a, 'long: 'short, 'short>(
        &self,
        key: &'a Self::Key<'long>,
    ) -> &'a Self::Key<'short> {
        key
    }
}

impl sort::ExtractKey<ast::Item> for ExtractKeyImpl {
    type Key<'a> = kdlplus::NodeI;

    fn extract_key<'a>(&self, item: &'a ast::Item) -> Self::Key<'a> {
        item.kp_node_i()
    }

    #[inline]
    fn reborrow_ref<'a, 'long: 'short, 'short>(
        &self,
        key: &'a Self::Key<'long>,
    ) -> &'a Self::Key<'short> {
        key
    }
}

impl sort::ExtractKey<ast::Param> for ExtractKeyImpl {
    type Key<'a> = &'a str;

    fn extract_key<'a>(&self, element: &'a ast::Param) -> Self::Key<'a> {
        &element.id
    }

    #[inline]
    fn reborrow_ref<'a, 'long: 'short, 'short>(
        &self,
        key: &'a Self::Key<'long>,
    ) -> &'a Self::Key<'short> {
        key
    }
}

/// A sorting key for [`ast::Item`].
#[derive(PartialOrd, PartialEq, Ord, Eq)]
enum ItemKey<'a> {
    Symbol,
    Param(&'a str),
    DeviceId(&'a str),
    NodeId(&'a str),
    Wire {
        nodes: [(&'a str, Option<&'a str>); 2],
    },
}

impl<'a> ItemKey<'a> {
    fn from_item(item: &'a ast::Item) -> Self {
        match item {
            ast::Item::Subcircuit(item) => Self::DeviceId(&item.id),
            ast::Item::Element(item) => Self::DeviceId(&item.id),
            ast::Item::Wire(item) => Self::Wire {
                nodes: item
                    .nodes
                    .each_ref()
                    .map(|node| (node.item_id.as_str(), node.member.as_deref())),
            },
            ast::Item::Terminal(item) => Self::DeviceId(&item.id),
            ast::Item::Node(item) => Self::NodeId(&item.id),
            ast::Item::Symbol(_) => Self::Symbol,
            ast::Item::ParamDef(item) => Self::Param(&item.id),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::kio;
    use crate::kio::test_utils::StdEnv;
    use futures::executor::block_on;
    use pretty_assertions::assert_eq;
    use std::path::PathBuf;

    #[test]
    fn roundtrip() {
        crate::tests::init();

        roundtrip_inner("std.simac".into());
        for path in crate::tests::circuit_examples::example_circuit_paths() {
            roundtrip_inner(path.0);
        }
    }

    #[tracing::instrument(level = "error")]
    fn roundtrip_inner(path: PathBuf) {
        let mut env = StdEnv::default();
        env.should_make_version_explicit = true;

        // Keep the original contents of the file
        let kdl_old = block_on(kio::PreloadEnv::read_file(&env, &path)).expect("read_file");
        let kdl_old = String::from_utf8(kdl_old).unwrap();

        if !kdl_old.contains("time_scale") && kdl_old.contains("workspace {") {
            // FIXME: `kio::save` creates `time_step` and `time_scale` entries,
            // which causes this test to fail
            tracing::debug!("Skipping this file because it does not contain `time_scale` entry");
            return;
        }

        // Load the file
        let mut world = World::default();
        let mut load_cx = kio::LoadCx::default();
        block_on(load_cx.preload(&env, &path))
            .map_err(anyhow::Error::from)
            .expect("preload");

        fn sys_load(
            In(load_cx): In<kio::LoadCx<PathBuf>>,
            commands: Commands<'_, '_>,
            circuits: Query<kio::ExistingCircuitData>,
        ) -> Result<kio::Loaded<PathBuf>, kio::LoadError<PathBuf>> {
            load_cx.load(commands, circuits)
        }

        let loaded = world
            .run_system_cached_with(sys_load, load_cx)
            .expect("load")
            .map_err(anyhow::Error::from)
            .expect("load");
        tracing::info!(?world);

        let ent_file = loaded.file_ent_map[&path];

        // Save the file
        fn sys_presave(
            In((env, ent_file)): In<(StdEnv, Entity)>,
            param: PresaveParam<'_, '_, PathBuf>,
        ) -> SaveCx<PathBuf> {
            SaveCx::presave(&env, &param, &[ent_file])
        }
        let save_cx = world
            .run_system_cached_with(sys_presave, (env.clone(), ent_file))
            .expect("presave");
        tracing::info!(?save_cx);

        block_on(save_cx.save(&env))
            .map_err(anyhow::Error::from)
            .expect("save");

        // Compare the saved file to the original
        let kdl_new = block_on(kio::PreloadEnv::read_file(&env, &path)).expect("read_file");
        let kdl_new = String::from_utf8(kdl_new).unwrap();
        assert_eq!(kdl_new, kdl_old);
    }
}
