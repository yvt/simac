//! Sort slice elements to preserve the order of persisting elements
use hashbrown::HashTable;
use itertools::enumerate;
use std::hash::Hash;

pub trait ExtractKey<T> {
    type Key<'a>: Hash + Eq
    where
        T: 'a;

    fn extract_key<'a>(&self, element: &'a T) -> Self::Key<'a>;

    fn reborrow_ref<'a, 'long: 'short, 'short>(
        &self,
        key: &'a Self::Key<'long>,
    ) -> &'a Self::Key<'short>;
}

pub fn sort_preserving<T>(slice: &mut [T], template: &[T], extract_key: impl ExtractKey<T>) {
    if template.is_empty() {
        return;
    }

    struct Entry<Key> {
        key: Key,
        template_i: usize,
    }
    let mut template_i_map = HashTable::with_capacity(template.len());
    for (template_i, element) in enumerate(template) {
        let key = extract_key.extract_key(element);
        template_i_map.insert_unique(fxhash::hash64(&key), Entry { key, template_i }, |entry| {
            fxhash::hash64(&entry.key)
        });
    }

    slice.sort_by_cached_key(|element| {
        let key = extract_key.extract_key(element);
        template_i_map
            .find(fxhash::hash64(&key), |entry| {
                *extract_key.reborrow_ref(&entry.key) == key
            })
            .map(|entry| entry.template_i)
            // Place new elements last
            .unwrap_or(usize::MAX)
    });
}
