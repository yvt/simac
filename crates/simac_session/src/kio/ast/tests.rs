use super::*;
use pretty_assertions::assert_eq;

struct LoadedFile {
    kdl: String,
    kp_arena: kdlplus::Arena,
    kp_doc_i: kdlplus::DocumentI,
    ast_file: File,
}

fn load_std() -> LoadedFile {
    let kdl = std::fs::read_to_string("std.simac").expect("failed to load `std.simac`");
    let k_doc: kdl::KdlDocument = kdl.parse().expect("KdlDocument::parse");
    let mut kp_arena = kdlplus::Arena::default();
    let kp_doc_i = kp_arena.import_kdl_document(&k_doc);
    let ast_file = File::parse(&kp_arena, kp_doc_i).expect("File::parse");
    LoadedFile {
        kdl,
        kp_arena,
        kp_doc_i,
        ast_file,
    }
}

/// Unparse a parsed [`File`] and check that the result is identical to the
/// original KDL document.
#[test]
fn unparse_identical() {
    crate::tests::init();

    let mut file = load_std();

    tracing::debug!(?file.kp_arena, "Before unparse");
    file.ast_file.clone().unparse(&mut (), &mut file.kp_arena);
    tracing::debug!(?file.kp_arena, "After unparse");
    file.kp_arena
        .validate()
        .expect("post-unparse kdlplus validation failed");

    let k_doc_new = file.kp_arena.export_kdl_document(file.kp_doc_i);
    let kdl_new = k_doc_new.to_string();
    assert_eq!(kdl_new, file.kdl);
}
