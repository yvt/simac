use fxhash::FxHashSet;
use slotmap::Key as _;

use super::UnparseCallbacks;

/// Callback functions for [`kdl_synchronize_nodes`].
pub(super) trait SynchronizeNodesCallbacks {
    /// Get the [`kdlplus::NodeI`] associated with the new AST item at index
    /// `i`.
    ///
    /// `i` must be in range `0..num_new_ast_items`.
    fn new_ast_item_node(
        &mut self,
        _i: usize,
        _kp_all_nodes: &mut kdlplus::NodeArena,
    ) -> kdlplus::NodeI {
        kdlplus::NodeI::null()
    }
}

pub(super) struct SynchronizedNode {
    pub kp_node_i: kdlplus::NodeI,
    /// If set, `kp_node_i` is a fresh node with an empty name.
    pub new: bool,
}

/// Synchronize a given range of a [`kdlplus::Document`]'s children, specified
/// by `kp_old_nodes_{start,end}`, with the provided new collection.
///
/// Returns `num_new_ast_items` [`SynchronizedNode`]s.
#[tracing::instrument(
    level = "debug",
    skip_all,
    fields(
        kp_old_nodes = ?kp_old_nodes_start..kp_old_nodes_end,
        num_new_ast_items = num_new_ast_items,
    ),
)]
pub(super) fn kdl_synchronize_nodes(
    cb: &mut dyn UnparseCallbacks,
    sncb: &mut dyn SynchronizeNodesCallbacks,
    kp_node_arena: &mut kdlplus::NodeArena,
    kp_parent_document_i: kdlplus::DocumentI,
    kp_parent_document: &mut kdlplus::Document,
    kp_old_nodes_start: Option<kdlplus::NodeI>,
    kp_old_nodes_end: Option<kdlplus::NodeI>,
    num_new_ast_items: usize,
) -> Vec<SynchronizedNode> {
    let mut kp_old_node_i_set: FxHashSet<kdlplus::NodeI> = <_>::default();

    assert!(
        kp_old_nodes_start
            .is_none_or(|i| kp_node_arena[i].parent_document_i == Some(kp_parent_document_i)),
        "document `kp_parent_document_i` doesn't contain node `kp_old_nodes_start`",
    );
    assert!(
        kp_old_nodes_end
            .is_none_or(|i| kp_node_arena[i].parent_document_i == Some(kp_parent_document_i)),
        "document `kp_parent_document_i` doesn't contain node `kp_old_nodes_end`",
    );

    // Remove all nodes in `kp_old_nodes_{start,end}` and store them in
    // `kp_old_node_i_set`
    {
        let mut cursor = kp_old_nodes_start;
        let mut kp_nodes = kp_parent_document.nodes_mut(kp_node_arena);
        while let Some(kp_node_i) = cursor {
            if cursor == kp_old_nodes_end {
                break;
            }

            cursor = kp_nodes.next(kp_node_i);
            kp_nodes.remove(kp_node_i);

            kp_old_node_i_set.insert(kp_node_i);
        }

        assert_eq!(
            cursor, kp_old_nodes_end,
            "`kp_old_nodes_end` does not follow `kp_old_nodes_start`"
        );
    }

    tracing::trace!(?kp_old_node_i_set);

    // Check every new AST item and create `[SynchronizedNode]`
    let out: Vec<SynchronizedNode> = (0..num_new_ast_items)
        .map(|i| {
            let kp_node_i = sncb.new_ast_item_node(i, kp_node_arena);

            if kp_old_node_i_set.remove(&kp_node_i) {
                // Reuse this node
                tracing::debug!(new_ast_item_i = i, ?kp_node_i, "Reusing the node");

                SynchronizedNode {
                    kp_node_i,
                    new: false,
                }
            } else {
                // Create a new node
                let kp_node_i = kp_node_arena.insert(kdlplus::Node {
                    parent_document_i: Some(kp_parent_document_i),
                    ..kdlplus::Node::new(String::new())
                });

                tracing::debug!(new_ast_item_i = i, ?kp_node_i, "Creating a new node");

                SynchronizedNode {
                    kp_node_i,
                    new: true,
                }
            }
        })
        .collect();

    // (Re-)insert the new nodes before `kp_old_nodes_end` in order
    for node in out.iter() {
        kp_parent_document
            .nodes_mut(kp_node_arena)
            .insert(node.kp_node_i, kp_old_nodes_end);
    }

    // Remove unused nodes
    for kp_node_i in kp_old_node_i_set {
        tracing::debug!(?kp_node_i, "Removing an unused node");

        kp_node_arena[kp_node_i].parent_document_i = None;
        cb.remove_node(kp_node_arena, kp_node_i);
    }

    out
}
