use array_ext::ArrayN as _;
use fxhash::FxHashMap;
use itertools::{izip, rev};
use slotmap::Key;
use std::{
    fmt,
    ops::{Range, RangeInclusive},
};
use try_match::match_ok;

use crate::{expr, layout};

mod synchronize;
#[cfg(test)]
mod tests;

pub type ParseResult<T> = Result<T, ParseError>;

#[derive(Debug, thiserror::Error)]
#[error("{kind}")]
pub struct ParseError {
    pub span: miette::SourceSpan,
    pub kind: ErrorKind,
}

impl ParseError {
    #[inline]
    fn new(span: miette::SourceSpan, kind: ErrorKind) -> Self {
        Self { span, kind }
    }
}

#[derive(Debug, PartialEq, Eq, thiserror::Error)]
pub enum ErrorKind {
    #[error("expected EOF")]
    ExpectedEof,
    #[error("expected children")]
    ExpectedChildren,
    #[error("expected node `{expected}`")]
    UnexpectedNodeName { expected: &'static str },
    #[error("unexpected argument")]
    UnexpectedArg,
    #[error("unrecognized item type")]
    UnexpectedItemNodeName,
    #[error("unrecognized symbol item type")]
    UnexpectedSymbolItemNodeName,
    #[error("property `{name}` missing")]
    MissingProp { name: &'static str },
    #[error("child node `{name}` missing")]
    MissingChildNode { name: &'static str },
    #[error("bad value type, string expected")]
    ArgNotString,
    #[error("bad value type, integer expected")]
    ArgNotInt,
    #[error("bad value type, boolean expected")]
    ArgNotBool,
    #[error("bad value type, number expected")]
    ArgNotNumber,
    #[error("bad value type, number or string expected")]
    ArgNotNumberOrString,
    #[error("value out of range")]
    ArgOutOfRange,
    #[error("{expected_min}..={expected_max} arguments expected, found {actual}")]
    ArgCountMismatch {
        expected_min: usize,
        expected_max: usize,
        actual: usize,
    },
    #[error("incompatible file format version: {0:?} ({VERSION:?} is the known latest version)")]
    VersionIncompatible([u32; 3]),
    #[error("import name must not be empty")]
    ImportNameEmpty,
    #[error("import name must not contain period (`.`)")]
    ImportNamePeriod,
    #[error("node reference contains an empty segment")]
    NodeRefEmptySegment,
    #[error("node reference contains extra segments")]
    NodeRefExtraSegments,
    #[error("unknown element type")]
    UnknownElemTy,
    #[error("subcircuit name contains an unexpected number of segments")]
    SubcircuitBadNumSegments,
    #[error("unrecognized color")]
    UnexpectedColor,
    #[error("invalid SVG path string")]
    ParseSvgPath {
        #[source]
        error: svgtypes::Error,
    },
}

#[derive(Debug, Clone)]
pub(super) struct File {
    pub kp_doc_i: kdlplus::DocumentI,
    pub meta: FileMeta,
    pub workspace: Option<Workspace>,
    pub imports: Vec<Import>,
    pub circuits: Vec<Circuit>,
}

#[derive(Debug, Clone)]
pub(super) struct FileMeta {
    /// The file format version.
    pub version: Option<[u32; 3]>,
}

pub(super) const VERSION: [u32; 3] = [0, 0, 0];

#[derive(Debug, Clone)]
pub(super) struct Import {
    pub id: String,
    pub path: String,
}

#[derive(Debug, Clone)]
pub(super) struct Workspace {
    pub root_circuit_id: String,
    pub time_step: Option<f64>,
    pub time_scale: Option<f64>,
    pub current_speed: Option<f64>,
    pub potential_color_range: Option<f64>,
}

#[derive(Debug, Clone)]
pub(super) struct Circuit {
    pub kp_node_i: kdlplus::NodeI,
    pub id: String,
    pub items: Vec<Item>,
}

#[derive(Debug, Clone)]
pub(super) enum Item {
    Subcircuit(Subcircuit),
    Element(Element),
    Wire(Wire),
    Terminal(Terminal),
    Node(Node),
    Symbol(Symbol),
    ParamDef(ParamDef),
}

#[derive(Debug, Clone)]
pub(super) struct Subcircuit {
    pub kp_node_i: kdlplus::NodeI,
    pub id: String,
    /// [`Import::id`]
    pub target_file_id: String,
    /// [`Circuit::id`]
    pub target_circuit_id: String,
    pub layout: DeviceLayout,
    pub params: Vec<Param>,
}

#[derive(Debug, Clone)]
pub(super) struct Element {
    pub kp_node_i: kdlplus::NodeI,
    pub id: String,
    pub ty: simac_elements::ElemTy,
    pub params: Vec<Param>,
    pub layout: DeviceLayout,
}

#[derive(Debug, Clone)]
pub(super) struct Wire {
    pub kp_node_i: kdlplus::NodeI,
    pub id: Option<String>,
    pub nodes: [NodeRef; 2],
}

#[derive(Debug, Clone)]
pub(super) struct Terminal {
    pub kp_node_i: kdlplus::NodeI,
    pub id: String,
    pub layout: DeviceLayout,
}

#[derive(Debug, Clone)]
pub(super) struct DeviceLayout {
    pub x: Option<i32>,
    pub y: Option<i32>,
    pub angle: Option<layout::Angle>,
    pub flip: Option<bool>,
}

#[derive(Debug, Clone)]
pub(super) struct Node {
    pub kp_node_i: kdlplus::NodeI,
    pub id: String,
    pub is_ground: bool,
    pub x: Option<i32>,
    pub y: Option<i32>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub(super) struct NodeRef {
    pub item_id: String,
    /// - [`Node`][]: `None`
    /// - [`Terminal`][]: `None`
    /// - [`Element`][]: A terminal index
    /// - [`Subcircuit`][]: [`Terminal::id`]
    pub member: Option<String>,
}

/// A device parameter.
#[derive(Debug, Clone)]
pub(super) struct Param {
    pub id: String,
    pub value: expr::Expr,
}

#[derive(Debug, Clone)]
pub(super) struct Symbol {
    pub kp_node_i: kdlplus::NodeI,
    pub items: Vec<SymbolItem>,
}

#[derive(Debug, Clone)]
pub(super) enum SymbolItem {
    Terminal(SymbolTerminal),
    Path(SymbolPath),
}

#[derive(Debug, Clone)]
pub(super) struct SymbolTerminal {
    pub id: String,
    pub x: i32,
    pub y: i32,
}

#[derive(Debug, Clone)]
pub(super) struct SymbolPath {
    pub fill: Option<FillColor>,
    pub stroke: Option<StrokeColor>,
    pub width: Option<u8>,
    pub d: Vec<svgtypes::SimplePathSegment>,
}

#[derive(Debug, Clone)]
pub(super) enum FillColor {
    Const(layout::Color),
    Potential(String),
}

#[derive(Debug, Clone)]
pub(super) enum StrokeColor {
    Const(layout::Color),
    Potential([String; 2]),
}

/// A circuit parameter definition.
#[derive(Debug, Clone)]
pub(super) struct ParamDef {
    pub kp_node_i: kdlplus::NodeI,
    pub id: String,
    pub default_val: Option<f64>,
    pub description: Option<String>,
}

/// Callback functions for [`File::unparse()`].
pub trait UnparseCallbacks {
    /// Called when a [`kdlplus::Document`] is detached from [`File::kp_doc_i`].
    /// The default implementation does nothing.
    fn remove_document(
        &mut self,
        kp_document_arena: &mut kdlplus::DocumentArena,
        kp_document_i: kdlplus::DocumentI,
    ) {
        tracing::trace!(?kp_document_i, "Detached a document");
        _ = kp_document_arena;
    }

    /// Called when a [`kdlplus::Node`] is detached from [`File::kp_doc_i`].
    /// The default implementation does nothing.
    fn remove_node(&mut self, kp_node_arena: &mut kdlplus::NodeArena, kp_node_i: kdlplus::NodeI) {
        tracing::trace!(?kp_node_i, "Detached a node");
        _ = kp_node_arena;
    }

    /// Called when a [`kdlplus::Entry`] is detached from [`File::kp_doc_i`].
    /// The default implementation does nothing.
    fn remove_entry(
        &mut self,
        kp_entry_arena: &mut kdlplus::EntryArena,
        kp_entry_i: kdlplus::EntryI,
    ) {
        tracing::trace!(?kp_entry_i, "Detached an entry");
        _ = kp_entry_arena;
    }
}

impl UnparseCallbacks for () {}

impl File {
    #[tracing::instrument(name = "File::parse", skip_all)]
    pub fn parse(kp_arena: &kdlplus::Arena, kp_doc_i: kdlplus::DocumentI) -> ParseResult<Self> {
        let kp_doc = &kp_arena.documents[kp_doc_i];
        let kp_doc_nodes = kp_doc.nodes(&kp_arena.nodes);
        let mut kp_iter = kp_doc_nodes.iter().peekable();

        // Find and parse `Meta`
        let Some((kp_meta_i, kp_meta)) = kp_iter.next() else {
            return Err(ParseError::new(
                kp_doc.span,
                ErrorKind::UnexpectedNodeName { expected: "simac" },
            ));
        };
        if kp_meta.name.value() != "simac" {
            return Err(ParseError::new(
                kp_meta.name.span(),
                ErrorKind::UnexpectedNodeName { expected: "simac" },
            ));
        }
        let meta = FileMeta::parse(kp_arena, kp_meta_i)?;

        // Find and parse `Workspace`
        let mut workspace = None;
        if kp_iter
            .peek()
            .is_some_and(|(_, x)| x.name.value() == "workspace")
        {
            let (kp_workspace_i, _) = kp_iter.next().unwrap();
            workspace = Some(Workspace::parse(kp_arena, kp_workspace_i)?);
        }

        // Find and parse `Import`
        let mut imports = Vec::new();
        while kp_iter
            .peek()
            .is_some_and(|(_, x)| x.name.value() == "import")
        {
            let (kp_import_i, _) = kp_iter.next().unwrap();
            imports.push(Import::parse(kp_arena, kp_import_i)?);
        }

        // Find and parse `Circuit`
        let mut circuits = Vec::new();
        while kp_iter
            .peek()
            .is_some_and(|(_, x)| x.name.value() == "circuit")
        {
            let (kp_circuit_i, _) = kp_iter.next().unwrap();
            circuits.push(Circuit::parse(kp_arena, kp_circuit_i)?);
        }

        if let Some((_, rest)) = kp_iter.next() {
            return Err(ParseError::new(rest.span, ErrorKind::ExpectedEof));
        }

        Ok(Self {
            kp_doc_i,
            meta,
            imports,
            workspace,
            circuits,
        })
    }

    /// Update the contents of `kp_arena.documents[old_file.kp_doc_i]` with
    /// `self`, reusing existing nodes as much as possible.
    #[tracing::instrument(name = "File::unparse", skip_all)]
    pub fn unparse(&mut self, cb: &mut dyn UnparseCallbacks, kp_arena: &mut kdlplus::Arena) {
        let Self {
            kp_doc_i,
            ref mut meta,
            ref mut workspace,
            ref mut imports,
            ref mut circuits,
        } = *self;
        macro_rules! kp_doc {
            () => {
                kp_arena.documents[kp_doc_i]
            };
        }
        let kp_doc = &mut kp_doc!();

        let mut kp_cur_node_i = kp_doc.nodes.first;

        // Find and unparse `Meta`
        // ------------------------------------------------------------------
        let kp_meta_i =
            if let Some(i) = kp_cur_node_i.filter(|&i| kp_arena.nodes[i].name.value() == "simac") {
                kp_cur_node_i = kp_doc.nodes(&kp_arena.nodes).next(i);
                i
            } else {
                let i = kp_arena.nodes.insert(kdlplus::Node {
                    parent_document_i: Some(kp_doc_i),
                    ..kdlplus::Node::new("simac")
                });
                kp_doc
                    .nodes_mut(&mut kp_arena.nodes)
                    .insert(i, kp_cur_node_i);
                i
            };
        meta.unparse(cb, kp_arena, kp_meta_i);

        // Find and unparse `Workspace`
        // ------------------------------------------------------------------
        let kp_doc = &mut kp_doc!();
        let workspace_y_kp_workspace_i = if let Some(i) =
            kp_cur_node_i.filter(|&i| kp_arena.nodes[i].name.value() == "workspace")
        {
            kp_cur_node_i = kp_doc.nodes(&kp_arena.nodes).next(i);
            if let Some(workspace) = workspace {
                Some((workspace, i))
            } else {
                kp_doc.nodes_mut(&mut kp_arena.nodes).remove(i);
                kp_arena.nodes[i].parent_document_i = None;
                cb.remove_node(&mut kp_arena.nodes, i);
                None
            }
        } else if let Some(workspace) = workspace {
            let i = kp_arena.nodes.insert(kdlplus::Node {
                parent_document_i: Some(kp_doc_i),
                ..kdlplus::Node::new("workspace")
            });
            kp_doc
                .nodes_mut(&mut kp_arena.nodes)
                .insert(i, kp_cur_node_i);
            Some((workspace, i))
        } else {
            None
        };

        if let Some((workspace, kp_workspace_i)) = workspace_y_kp_workspace_i {
            workspace.unparse(cb, kp_arena, kp_workspace_i);
        }

        // Find and unparse `Import`s
        // ------------------------------------------------------------------
        let kp_doc = &kp_doc!();

        // Find the old KDL nodes
        let kp_import_start_i = kp_cur_node_i;
        while let Some(i) = kp_cur_node_i.filter(|&i| kp_arena.nodes[i].name.value() == "import") {
            kp_cur_node_i = kp_doc.nodes(&kp_arena.nodes).next(i);
        }
        let kp_import_end_i = kp_cur_node_i;

        // Create a mapping from new/old `Import`s to old KDL nodes because
        // `Import` doesn't contain assocaiting information
        let kp_import_nodes = || {
            std::iter::successors(kp_import_start_i, |&i| {
                kp_doc.nodes(&kp_arena.nodes).next(i)
            })
        };
        let import_map: FxHashMap<String, kdlplus::NodeI> = kp_import_nodes()
            .filter_map(|kp_node_i| {
                let import = Import::parse(kp_arena, kp_node_i).ok()?;
                Some((import.id, kp_node_i))
            })
            .collect();

        let new_import_node: Vec<kdlplus::NodeI> = imports
            .iter()
            .map(|import| {
                import_map
                    .get(&import.id)
                    .copied()
                    .unwrap_or_else(kdlplus::NodeI::null)
            })
            .collect();

        struct ImportSyncCallbacksImpl<'a> {
            new_import_node: &'a [kdlplus::NodeI],
        }

        impl synchronize::SynchronizeNodesCallbacks for ImportSyncCallbacksImpl<'_> {
            fn new_ast_item_node(
                &mut self,
                i: usize,
                _kp_all_nodes: &mut kdlplus::NodeArena,
            ) -> kdlplus::NodeI {
                self.new_import_node[i]
            }
        }

        let import_sync_list = synchronize::kdl_synchronize_nodes(
            cb,
            &mut ImportSyncCallbacksImpl {
                new_import_node: &new_import_node,
            },
            &mut kp_arena.nodes,
            kp_doc_i,
            &mut kp_doc!(),
            kp_import_start_i,
            kp_import_end_i,
            new_import_node.len(),
        );

        for (import, import_sync) in izip!(imports, import_sync_list) {
            if import_sync.new {
                kp_arena.nodes[import_sync.kp_node_i].name = "import".into();
            }
            import.unparse(cb, kp_arena, import_sync.kp_node_i);
        }

        // Find and unparse `Circuit`s
        // ------------------------------------------------------------------
        let kp_doc = &kp_doc!();

        // Find the old KDL nodes
        let kp_circuit_start_i = kp_cur_node_i;
        while let Some(i) = kp_cur_node_i.filter(|&i| kp_arena.nodes[i].name.value() == "circuit") {
            kp_cur_node_i = kp_doc.nodes(&kp_arena.nodes).next(i);
        }
        let kp_circuit_end_i = kp_cur_node_i;

        struct CircuitSyncCallbacks<'a> {
            new_circuits: &'a [Circuit],
        }

        impl synchronize::SynchronizeNodesCallbacks for CircuitSyncCallbacks<'_> {
            fn new_ast_item_node(
                &mut self,
                i: usize,
                _kp_all_nodes: &mut kdlplus::NodeArena,
            ) -> kdlplus::NodeI {
                self.new_circuits[i].kp_node_i
            }
        }

        let circuit_sync_list = synchronize::kdl_synchronize_nodes(
            cb,
            &mut CircuitSyncCallbacks {
                new_circuits: circuits,
            },
            &mut kp_arena.nodes,
            kp_doc_i,
            &mut kp_doc!(),
            kp_circuit_start_i,
            kp_circuit_end_i,
            circuits.len(),
        );

        for (circuit, circuit_sync) in izip!(circuits, circuit_sync_list) {
            if circuit_sync.new {
                kp_arena.nodes[circuit_sync.kp_node_i].name = "circuit".into();
            }
            circuit.unparse(cb, kp_arena, circuit_sync.kp_node_i);
        }
    }
}

impl FileMeta {
    #[tracing::instrument(name = "FileMeta::parse", level = "debug", skip_all)]
    fn parse(kp_arena: &kdlplus::Arena, kp_meta_i: kdlplus::NodeI) -> ParseResult<Self> {
        let kp_meta = &kp_arena.nodes[kp_meta_i];
        let kp_children = kp_meta.children.map(|i| &kp_arena.documents[i]);

        let version = 'a: {
            let Some((_, kp_version)) = kp_children.and_then(|d| d.get(&kp_arena.nodes, "version"))
            else {
                tracing::debug!("`version` node not found, assuming {VERSION:?}");
                break 'a None;
            };
            let args = kdl_expect_args(kp_arena, kp_version, 3..=3)?;
            let [v0, v1, v2] = [args[0].1, args[1].1, args[2].1].map(kdl_expect_int);
            let version = [v0?, v1?, v2?];
            if version[0] != VERSION[0] {
                return Err(ParseError::new(
                    args[0].1.span,
                    ErrorKind::VersionIncompatible(version),
                ));
            } else if version[1] > VERSION[1] {
                return Err(ParseError::new(
                    args[1].1.span,
                    ErrorKind::VersionIncompatible(version),
                ));
            }
            Some(version)
        };

        Ok(Self { version })
    }

    #[tracing::instrument(name = "FileMeta::unparse", level = "debug", skip_all)]
    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_meta_i: kdlplus::NodeI,
    ) {
        let Self { version } = self;

        if kp_arena.nodes[kp_meta_i].children.is_some() || version.is_some() {
            let (kp_children_i, kp_children) =
                kdl_ensure_document(&mut kp_arena.documents, &mut kp_arena.nodes, kp_meta_i);

            let version = version.unwrap_or(VERSION);

            // Replace or add `version` node
            let kp_version_i = kp_children.get(&kp_arena.nodes, "version").map(|x| x.0);
            let kp_version_i = kdl_ensure_node(
                &mut kp_arena.nodes,
                kp_children_i,
                kp_children,
                kp_version_i,
                "version",
            );
            let kp_version = &mut kp_arena.nodes[kp_version_i];
            kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_version);
            let [(_, v0), (_, v1), (_, v2)] =
                kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_version_i, kp_version);
            kdl_set_int(v0, version[0].into());
            kdl_set_int(v1, version[1].into());
            kdl_set_int(v2, version[2].into());
        }
    }
}

impl Import {
    fn parse(kp_arena: &kdlplus::Arena, kp_meta_i: kdlplus::NodeI) -> ParseResult<Self> {
        let kp_meta = &kp_arena.nodes[kp_meta_i];

        // Arguments
        let args = kdl_expect_args(kp_arena, kp_meta, 2..=2)?;
        let name = args[0]
            .1
            .value
            .as_string()
            .ok_or_else(|| ParseError::new(args[0].1.span, ErrorKind::ArgNotString))?
            .to_owned();
        let path = args[1]
            .1
            .value
            .as_string()
            .ok_or_else(|| ParseError::new(args[1].1.span, ErrorKind::ArgNotString))?
            .to_owned();

        // TODO: Deny properties

        if name.is_empty() {
            return Err(ParseError::new(args[0].1.span, ErrorKind::ImportNameEmpty));
        } else if name.contains('.') {
            return Err(ParseError::new(args[0].1.span, ErrorKind::ImportNamePeriod));
        }

        Ok(Self { id: name, path })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_import_i: kdlplus::NodeI,
    ) {
        let Self { id, path } = self;
        let kp_import = &mut kp_arena.nodes[kp_import_i];

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_import);

        let [(_, kp_name), (_, kp_path)] =
            kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_import_i, kp_import);

        kdl_set_string(kp_name, id);
        kdl_set_string(kp_path, path);
    }
}

impl Workspace {
    #[tracing::instrument(name = "Workspace::parse", level = "debug", skip_all)]
    fn parse(kp_arena: &kdlplus::Arena, kp_meta_i: kdlplus::NodeI) -> ParseResult<Self> {
        let kp_meta = &kp_arena.nodes[kp_meta_i];

        let kp_children_i = kp_meta
            .children
            .ok_or_else(|| ParseError::new(kp_meta.span, ErrorKind::ExpectedChildren))?;
        let kp_children = &kp_arena.documents[kp_children_i];

        let mut root_circuit_id = None;
        let mut time_step = None;
        let mut time_scale = None;
        let mut current_speed = None;
        let mut potential_color_range = None;

        for (_, kp_node) in kp_children.nodes(&kp_arena.nodes).iter() {
            match kp_node.name.value() {
                "root" => {
                    root_circuit_id = Some(
                        kdl_expect_string(kdl_expect_args(kp_arena, kp_node, 1..=1)?[0].1)?
                            .to_owned(),
                    );
                }
                "time_step" => {
                    time_step = Some(kdl_expect_real(
                        kdl_expect_args(kp_arena, kp_node, 1..=1)?[0].1,
                    )?);
                }
                "time_scale" => {
                    time_scale = Some(kdl_expect_real(
                        kdl_expect_args(kp_arena, kp_node, 1..=1)?[0].1,
                    )?);
                }
                "current_speed" => {
                    current_speed = Some(kdl_expect_real(
                        kdl_expect_args(kp_arena, kp_node, 1..=1)?[0].1,
                    )?);
                }
                "potential_color_range" => {
                    potential_color_range = Some(kdl_expect_real(
                        kdl_expect_args(kp_arena, kp_node, 1..=1)?[0].1,
                    )?);
                }
                _ => {
                    tracing::debug!(
                        name = kp_node.name.value(),
                        "Ignoring an unrecognized child node"
                    );
                }
            }
        }

        Ok(Self {
            root_circuit_id: root_circuit_id.ok_or_else(|| {
                ParseError::new(kp_meta.span, ErrorKind::MissingChildNode { name: "root" })
            })?,
            time_step,
            time_scale,
            current_speed,
            potential_color_range,
        })
    }

    #[tracing::instrument(name = "Workspace::unparse", level = "debug", skip_all)]
    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_workspace_i: kdlplus::NodeI,
    ) {
        let Self {
            ref root_circuit_id,
            time_step,
            time_scale,
            current_speed,
            potential_color_range,
        } = *self;

        let (kp_children_i, kp_children) =
            kdl_ensure_document(&mut kp_arena.documents, &mut kp_arena.nodes, kp_workspace_i);

        let mut kp_root_circuit_id_i = None;
        let mut kp_time_step_i = None;
        let mut kp_time_scale_i = None;
        let mut kp_current_speed_i = None;
        let mut kp_potential_color_range_i = None;

        for (kp_node_i, kp_node) in kp_children.nodes(&kp_arena.nodes).iter() {
            match kp_node.name.value() {
                "root" => kp_root_circuit_id_i = Some(kp_node_i),
                "time_step" => kp_time_step_i = Some(kp_node_i),
                "time_scale" => kp_time_scale_i = Some(kp_node_i),
                "current_speed" => kp_current_speed_i = Some(kp_node_i),
                "potential_color_range" => kp_potential_color_range_i = Some(kp_node_i),
                _ => {
                    tracing::debug!(
                        name = kp_node.name.value(),
                        "Ignoring an unrecognized child node"
                    );
                }
            }
        }

        // Unparse `root_circuit_id`
        let kp_root_circuit_id_i = kdl_ensure_node(
            &mut kp_arena.nodes,
            kp_children_i,
            kp_children,
            kp_root_circuit_id_i,
            "root",
        );
        let [(_, a0)] = kdl_ensure_args_const(
            cb,
            &mut kp_arena.entries,
            kp_root_circuit_id_i,
            &mut kp_arena.nodes[kp_root_circuit_id_i],
        );
        kdl_set_string(a0, root_circuit_id);

        // Unparse `time_step`
        if let Some((kp_node_i, value)) = kdl_ensure_node_optional(
            cb,
            &mut kp_arena.nodes,
            kp_children_i,
            kp_children,
            kp_time_step_i,
            "time_step",
            time_step,
        ) {
            let [(_, a0)] = kdl_ensure_args_const(
                cb,
                &mut kp_arena.entries,
                kp_node_i,
                &mut kp_arena.nodes[kp_node_i],
            );
            kdl_set_real(a0, value);
        }

        // Unparse `time_scale`
        if let Some((kp_node_i, value)) = kdl_ensure_node_optional(
            cb,
            &mut kp_arena.nodes,
            kp_children_i,
            kp_children,
            kp_time_scale_i,
            "time_scale",
            time_scale,
        ) {
            let [(_, a0)] = kdl_ensure_args_const(
                cb,
                &mut kp_arena.entries,
                kp_node_i,
                &mut kp_arena.nodes[kp_node_i],
            );
            kdl_set_real(a0, value);
        }

        // Unparse `current_speed`
        if let Some((kp_node_i, value)) = kdl_ensure_node_optional(
            cb,
            &mut kp_arena.nodes,
            kp_children_i,
            kp_children,
            kp_current_speed_i,
            "current_speed",
            current_speed,
        ) {
            let [(_, a0)] = kdl_ensure_args_const(
                cb,
                &mut kp_arena.entries,
                kp_node_i,
                &mut kp_arena.nodes[kp_node_i],
            );
            kdl_set_real(a0, value);
        }

        // Unparse `potential_color_range`
        if let Some((kp_node_i, value)) = kdl_ensure_node_optional(
            cb,
            &mut kp_arena.nodes,
            kp_children_i,
            kp_children,
            kp_potential_color_range_i,
            "potential_color_range",
            potential_color_range,
        ) {
            let [(_, a0)] = kdl_ensure_args_const(
                cb,
                &mut kp_arena.entries,
                kp_node_i,
                &mut kp_arena.nodes[kp_node_i],
            );
            kdl_set_real(a0, value);
        }
    }
}

impl Circuit {
    #[tracing::instrument(name = "Circuit::parse", level = "debug", skip_all, fields(id))]
    fn parse(kp_arena: &kdlplus::Arena, kp_meta_i: kdlplus::NodeI) -> ParseResult<Self> {
        let kp_meta = &kp_arena.nodes[kp_meta_i];
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_meta, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        if tracing::span_enabled!(tracing::Level::DEBUG) {
            tracing::Span::current().record("id", &id);
        }

        // Items
        let mut items = Vec::new();

        if let Some(kp_children_i) = kp_meta.children {
            let kp_children = &kp_arena.documents[kp_children_i];
            for (kp_item_i, _) in kp_children.nodes(&kp_arena.nodes).iter() {
                items.push(Item::parse(kp_arena, kp_item_i)?);
            }
        }

        Ok(Self {
            kp_node_i: kp_meta_i,
            id,
            items,
        })
    }

    #[tracing::instrument(
        name = "Circuit::unparse",
        level = "debug",
        skip_all,
        fields(id = self.id),
    )]
    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_circuit_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i,
            id,
            items,
        } = self;
        let (kp_children_i, kp_children) =
            kdl_ensure_document(&mut kp_arena.documents, &mut kp_arena.nodes, kp_circuit_i);
        let kp_circuit = &mut kp_arena.nodes[kp_circuit_i];

        *kp_node_i = kp_circuit_i;

        // Arguments
        let [(_, k_id)] =
            kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_circuit_i, kp_circuit);
        kdl_set_string(k_id, id);

        // Items
        struct ItemSyncCallbacks<'a> {
            new_items: &'a [Item],
        }

        impl synchronize::SynchronizeNodesCallbacks for ItemSyncCallbacks<'_> {
            fn new_ast_item_node(
                &mut self,
                i: usize,
                _kp_all_nodes: &mut kdlplus::NodeArena,
            ) -> kdlplus::NodeI {
                self.new_items[i].kp_node_i()
            }
        }

        let kp_old_nodes_start = kp_children.nodes.first;

        let item_sync_list = synchronize::kdl_synchronize_nodes(
            cb,
            &mut ItemSyncCallbacks { new_items: items },
            &mut kp_arena.nodes,
            kp_children_i,
            kp_children,
            kp_old_nodes_start,
            None,
            items.len(),
        );

        for (item, item_sync) in izip!(items, item_sync_list) {
            item.unparse(cb, kp_arena, item_sync.kp_node_i);
        }
    }
}

impl Item {
    #[tracing::instrument(name = "Item::parse", level = "debug", skip_all)]
    fn parse(kp_arena: &kdlplus::Arena, kp_item_i: kdlplus::NodeI) -> ParseResult<Self> {
        let kp_item = &kp_arena.nodes[kp_item_i];
        let name = kp_item.name.value();

        if name == "-" {
            Ok(Self::Wire(Wire::parse(kp_arena, kp_item_i, kp_item)?))
        } else if name == "node" {
            Ok(Self::Node(Node::parse(
                false, kp_arena, kp_item_i, kp_item,
            )?))
        } else if name == "@ground" {
            // TODO: Maybe deny user use
            Ok(Self::Node(Node::parse(true, kp_arena, kp_item_i, kp_item)?))
        } else if name == "terminal" {
            Ok(Self::Terminal(Terminal::parse(
                kp_arena, kp_item_i, kp_item,
            )?))
        } else if let Some(name) = name.strip_prefix('@') {
            // TODO: Maybe deny user use
            Ok(Self::Element(Element::parse(
                name, kp_arena, kp_item_i, kp_item,
            )?))
        } else if name.contains('.') {
            Ok(Self::Subcircuit(Subcircuit::parse(
                name, kp_arena, kp_item_i, kp_item,
            )?))
        } else if name == "symbol" {
            Ok(Self::Symbol(Symbol::parse(kp_arena, kp_item_i, kp_item)?))
        } else if name == "param" {
            Ok(Self::ParamDef(ParamDef::parse(
                kp_arena, kp_item_i, kp_item,
            )?))
        } else {
            Err(ParseError::new(
                kp_item.name.span(),
                ErrorKind::UnexpectedItemNodeName,
            ))
        }
    }

    pub fn kp_node_i(&self) -> kdlplus::NodeI {
        match *self {
            Item::Subcircuit(Subcircuit { kp_node_i, .. })
            | Item::Element(Element { kp_node_i, .. })
            | Item::Wire(Wire { kp_node_i, .. })
            | Item::Terminal(Terminal { kp_node_i, .. })
            | Item::Node(Node { kp_node_i, .. })
            | Item::Symbol(Symbol { kp_node_i, .. })
            | Item::ParamDef(ParamDef { kp_node_i, .. }) => kp_node_i,
        }
    }

    #[tracing::instrument(name = "Item::unparse", level = "debug", skip(self, cb, kp_arena))]
    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let kp_item = &mut kp_arena.nodes[kp_item_i];
        match self {
            Item::Wire(item) => {
                item.kp_node_i = kp_item_i;
                kdl_set_identifier(&mut kp_item.name, "-");
                item.unparse(cb, kp_arena, kp_item_i);
            }
            Item::Node(item) => {
                item.kp_node_i = kp_item_i;
                kdl_set_identifier(
                    &mut kp_item.name,
                    ["node", "@ground"][item.is_ground as usize],
                );
                item.unparse(cb, kp_arena, kp_item_i);
            }
            Item::Terminal(item) => {
                item.kp_node_i = kp_item_i;
                kdl_set_identifier(&mut kp_item.name, "terminal");
                item.unparse(cb, kp_arena, kp_item_i);
            }
            Item::Element(item) => {
                // `kdl_set_identifier` is done in it
                item.kp_node_i = kp_item_i;
                item.unparse(cb, kp_arena, kp_item_i);
            }
            Item::Subcircuit(item) => {
                // `kdl_set_identifier` is done in it
                item.kp_node_i = kp_item_i;
                item.unparse(cb, kp_arena, kp_item_i);
            }
            Item::Symbol(item) => {
                item.kp_node_i = kp_item_i;
                kdl_set_identifier(&mut kp_item.name, "symbol");
                item.unparse(cb, kp_arena, kp_item_i);
            }
            Item::ParamDef(item) => {
                item.kp_node_i = kp_item_i;
                kdl_set_identifier(&mut kp_item.name, "param");
                item.unparse(cb, kp_arena, kp_item_i);
            }
        }
    }
}

impl Subcircuit {
    fn parse(
        name: &str,
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        let mut parts = name.split('.');
        let target_file_id = parts.next().unwrap().to_owned();
        let target_circuit_id = parts
            .next()
            .ok_or_else(|| {
                ParseError::new(kp_item.name.span(), ErrorKind::SubcircuitBadNumSegments)
            })?
            .to_owned();
        if parts.next().is_some() {
            return Err(ParseError::new(
                kp_item.name.span(),
                ErrorKind::SubcircuitBadNumSegments,
            ));
        }

        // Arguments
        let args = kdl_expect_args(kp_arena, kp_item, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        // Props
        let layout = DeviceLayout::parse(kp_arena, kp_item_i, kp_item)?;
        let params = Param::parse_params(kp_arena, kp_item_i, kp_item)?;

        Ok(Self {
            kp_node_i: kp_item_i,
            target_file_id,
            target_circuit_id,
            params,
            layout,
            id,
        })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i: _,
            id,
            target_file_id,
            target_circuit_id,
            layout,
            params,
        } = self;
        let kp_item = &mut kp_arena.nodes[kp_item_i];

        let name = format!("{target_file_id}.{target_circuit_id}");
        kdl_set_identifier(&mut kp_item.name, &name);

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_item);

        // Arguments
        let [(_, k_id)] = kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
        kdl_set_string(k_id, id);

        // Props
        layout.unparse(cb, kp_arena, kp_item_i);
        Param::unparse_params(params, cb, kp_arena, kp_item_i);
    }
}

impl Element {
    fn parse(
        name: &str,
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        let ty = name
            .parse()
            .map_err(|_| ParseError::new(kp_item.name.span(), ErrorKind::UnknownElemTy))?;
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_item, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        // Props
        let layout = DeviceLayout::parse(kp_arena, kp_item_i, kp_item)?;
        let params = Param::parse_params(kp_arena, kp_item_i, kp_item)?;

        Ok(Self {
            kp_node_i: kp_item_i,
            ty,
            id,
            params,
            layout,
        })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i: _,
            id,
            ty,
            params,
            layout,
        } = self;
        let kp_item = &mut kp_arena.nodes[kp_item_i];

        let name = format!("@{}", ty.name());
        kdl_set_identifier(&mut kp_item.name, &name);

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_item);

        // Arguments
        let [(_, k_id)] = kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
        kdl_set_string(k_id, id);

        // Props
        layout.unparse(cb, kp_arena, kp_item_i);
        Param::unparse_params(params, cb, kp_arena, kp_item_i);
    }
}

impl Wire {
    fn parse(
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_item, 2..=3)?;
        let (id, n0, n1) = match *args {
            [id, n0, n1] => (Some(id), n0, n1),
            [n0, n1] => (None, n0, n1),
            _ => unreachable!(),
        };
        let id = id
            .map(|x| kdl_expect_string(x.1))
            .transpose()?
            .map(<_>::to_owned);
        let n0 = NodeRef::parse_entry(n0.1)?;
        let n1 = NodeRef::parse_entry(n1.1)?;
        Ok(Self {
            kp_node_i: kp_item_i,
            id,
            nodes: [n0, n1],
        })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i: _,
            id,
            nodes,
        } = self;
        let kp_item = &mut kp_arena.nodes[kp_item_i];

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_item);

        // Arguments
        let (k_n0, k_n1) = if let Some(id) = id {
            let [(_, k_id), (_, k_n0), (_, k_n1)] =
                kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
            kdl_set_string(k_id, id);
            (k_n0, k_n1)
        } else {
            let [(_, k_n0), (_, k_n1)] =
                kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
            (k_n0, k_n1)
        };
        nodes[0].unparse_entry(k_n0);
        nodes[1].unparse_entry(k_n1);
    }
}

impl Terminal {
    fn parse(
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_item, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        // Props
        let layout = DeviceLayout::parse(kp_arena, kp_item_i, kp_item)?;

        Ok(Self {
            kp_node_i: kp_item_i,
            id,
            layout,
        })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i: _,
            id,
            layout,
        } = self;
        let kp_item = &mut kp_arena.nodes[kp_item_i];

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_item);

        // Arguments
        let [(_, k_id)] = kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
        kdl_set_string(k_id, id);

        // Props
        layout.unparse(cb, kp_arena, kp_item_i);
    }
}

impl DeviceLayout {
    fn parse(
        kp_arena: &kdlplus::Arena,
        _k_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        let mut this = Self {
            x: None,
            y: None,
            angle: None,
            flip: None,
        };
        for (_, entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &entry.name else { continue };
            // TODO: Deny duplicates
            match name.value() {
                "x" => this.x = Some(kdl_expect_int(entry)?),
                "y" => this.y = Some(kdl_expect_int(entry)?),
                "angle" => {
                    this.angle = Some(
                        kdl_expect_int::<i32>(entry)?
                            .try_into()
                            .map_err(|_| ParseError::new(entry.span, ErrorKind::ArgOutOfRange))?,
                    )
                }
                "flip" => this.flip = Some(kdl_expect_bool(entry)?),
                _ => {}
            }
        }
        Ok(this)
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self { x, y, angle, flip } = *self;

        let kp_item = &mut kp_arena.nodes[kp_item_i];

        let mut kp_x_i = None;
        let mut kp_y_i = None;
        let mut kp_angle_i = None;
        let mut kp_flip_i = None;

        for (kp_entry_i, kp_entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &kp_entry.name else { continue };
            match name.value() {
                "x" => kp_x_i = Some(kp_entry_i),
                "y" => kp_y_i = Some(kp_entry_i),
                "angle" => kp_angle_i = Some(kp_entry_i),
                "flip" => kp_flip_i = Some(kp_entry_i),
                _ => {}
            }
        }

        // Unparse `x`
        if let Some((kp_x_i, value)) =
            kdl_ensure_entry_optional(cb, &mut kp_arena.entries, kp_item_i, kp_item, kp_x_i, x)
        {
            let kp_entry = &mut kp_arena.entries[kp_x_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("x"));
            kdl_set_int(kp_entry, value.into());
        }

        // Unparse `y`
        if let Some((kp_y_i, value)) =
            kdl_ensure_entry_optional(cb, &mut kp_arena.entries, kp_item_i, kp_item, kp_y_i, y)
        {
            let kp_entry = &mut kp_arena.entries[kp_y_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("y"));
            kdl_set_int(kp_entry, value.into());
        }

        // Unparse `angle`
        if let Some((kp_angle_i, value)) = kdl_ensure_entry_optional(
            cb,
            &mut kp_arena.entries,
            kp_item_i,
            kp_item,
            kp_angle_i,
            angle,
        ) {
            let kp_entry = &mut kp_arena.entries[kp_angle_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("angle"));
            kdl_set_int(kp_entry, u16::from(value).into());
        }

        // Unparse `flip`
        if let Some((kp_flip_i, value)) = kdl_ensure_entry_optional(
            cb,
            &mut kp_arena.entries,
            kp_item_i,
            kp_item,
            kp_flip_i,
            flip,
        ) {
            let kp_entry = &mut kp_arena.entries[kp_flip_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("flip"));
            kdl_set_bool(kp_entry, value);
        }
    }
}

impl Node {
    fn parse(
        is_ground: bool,
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_item, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        // Props
        let mut x = None;
        let mut y = None;
        for (_, entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &entry.name else { continue };
            // TODO: Deny duplicates
            match name.value() {
                "x" => x = Some(kdl_expect_int(entry)?),
                "y" => y = Some(kdl_expect_int(entry)?),
                _ => {}
            }
        }

        Ok(Self {
            kp_node_i: kp_item_i,
            id,
            is_ground,
            x,
            y,
        })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i: _,
            id,
            is_ground: _,
            x,
            y,
        } = self;
        let kp_item = &mut kp_arena.nodes[kp_item_i];

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_item);

        // Arguments
        let [(_, k_id)] = kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
        kdl_set_string(k_id, id);

        // Props
        let mut kp_x_i = None;
        let mut kp_y_i = None;

        for (kp_entry_i, kp_entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &kp_entry.name else { continue };
            match name.value() {
                "x" => kp_x_i = Some(kp_entry_i),
                "y" => kp_y_i = Some(kp_entry_i),
                _ => {}
            }
        }

        // Unparse `x`
        if let Some((kp_x_i, value)) =
            kdl_ensure_entry_optional(cb, &mut kp_arena.entries, kp_item_i, kp_item, kp_x_i, *x)
        {
            let kp_entry = &mut kp_arena.entries[kp_x_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("x"));
            kdl_set_int(kp_entry, value.into());
        }

        // Unparse `y`
        if let Some((kp_y_i, value)) =
            kdl_ensure_entry_optional(cb, &mut kp_arena.entries, kp_item_i, kp_item, kp_y_i, *y)
        {
            let kp_entry = &mut kp_arena.entries[kp_y_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("y"));
            kdl_set_int(kp_entry, value.into());
        }
    }
}

impl NodeRef {
    fn parse_entry(kp_entry: &kdlplus::Entry) -> ParseResult<Self> {
        Self::parse_str(kdl_expect_string(kp_entry)?)
            .map_err(|kind| ParseError::new(kp_entry.span, kind))
    }

    fn parse_str(value: &str) -> Result<Self, ErrorKind> {
        let mut parts = value.split('.');
        let item_id = parts.next().unwrap().to_owned();
        let member = parts.next().map(<_>::to_owned);
        if parts.next().is_some() {
            return Err(ErrorKind::NodeRefExtraSegments);
        }

        if item_id.is_empty() || matches!(&member, Some(x) if x.is_empty()) {
            return Err(ErrorKind::NodeRefEmptySegment);
        }

        Ok(Self { item_id, member })
    }

    fn unparse_entry(&self, kp_entry: &mut kdlplus::Entry) {
        kdl_set_string(kp_entry, &self.to_string());
    }
}

impl fmt::Display for NodeRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.item_id)?;
        if let Some(x) = &self.member {
            write!(f, ".{x}")?;
        }
        Ok(())
    }
}

impl Param {
    const NAME_PREFIX: &'static str = "param.";

    fn parse_params(
        kp_arena: &kdlplus::Arena,
        _k_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Vec<Self>> {
        let mut params = Vec::new();
        for (_, entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &entry.name else { continue };
            let Some(name) = name.value().strip_prefix(Self::NAME_PREFIX) else {
                continue;
            };
            params.push(Self {
                id: name.to_owned(),
                value: expr::Expr::parse(entry)?,
            });
        }
        Ok(params)
    }

    fn unparse_params(
        params: &mut [Self],
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        use imara_diff::intern::{InternedInput, Interner, Token};

        // Find existing params
        let mut interner: Interner<String> = <_>::default();

        struct ParamInfo {
            id: Token,
            /// In `new_params`, this is set if [`Self::kp_entry_i`] must be
            /// reinserted to a new place.
            ///
            /// In `old_params`, this has no effect.
            reorder: bool,
            /// In `new_params`, `Key::null()` means it cannot reuse an old KDL
            /// entry.
            ///
            /// In `old_params`, `Key::null()` means its old KDL entry is
            /// reused somewhere.
            kp_entry_i: kdlplus::EntryI,
        }

        let kp_item = &mut kp_arena.nodes[kp_item_i];
        let mut old_params: Vec<ParamInfo> = kp_item
            .entries(&kp_arena.entries)
            .iter()
            .filter_map(|(kp_entry_i, kp_entry)| {
                let name = kp_entry.name.as_ref()?;
                let id = name.value().strip_prefix(Self::NAME_PREFIX)?;
                Some(ParamInfo {
                    id: interner.intern(id.to_owned()),
                    kp_entry_i,
                    reorder: false,
                })
            })
            .collect();

        // Calculate diff to reuse old KDL entries with no reordering
        // among themselves as well as other (non-`Param`) entries
        let mut new_params: Vec<ParamInfo> = params
            .iter()
            .map(|param| ParamInfo {
                id: interner.intern(param.id.clone()),
                kp_entry_i: kdlplus::EntryI::null(), // set later
                reorder: false,
            })
            .collect();

        let imara_input = InternedInput {
            before: old_params.iter().map(|x| x.id).collect(),
            after: new_params.iter().map(|x| x.id).collect(),
            interner,
        };

        struct ImaraSink<'a> {
            old_params: &'a mut [ParamInfo],
            new_params: &'a mut [ParamInfo],
            old_cur_i: usize,
            new_cur_i: usize,
        }

        impl imara_diff::Sink for ImaraSink<'_> {
            type Out = ();

            fn process_change(&mut self, old_range: Range<u32>, new_range: Range<u32>) {
                let old_range = [old_range.start, old_range.end].map(|x| x as usize);
                let new_range = [new_range.start, new_range.end].map(|x| x as usize);

                // `old_params[old_cur_i..old_range[0]]` and
                // `new_params[new_cur_i..new_range[1]]` have identical `id`s.
                // Reuse the old KDL entries with no reordering
                assert_eq!(old_range[0] - self.old_cur_i, new_range[0] - self.new_cur_i);

                for (old_param, new_param) in izip!(
                    &mut self.old_params[self.old_cur_i..old_range[0]],
                    &mut self.new_params[self.new_cur_i..new_range[0]]
                ) {
                    new_param.kp_entry_i = old_param.kp_entry_i;
                    old_param.kp_entry_i = kdlplus::EntryI::null();
                }

                self.old_cur_i = old_range[1];
                self.new_cur_i = new_range[1];
            }

            fn finish(mut self) -> Self::Out {
                let old_len = self.old_params.len() as u32;
                let new_len = self.new_params.len() as u32;
                self.process_change(old_len..old_len, new_len..new_len);
            }
        }

        imara_diff::diff(
            imara_diff::Algorithm::Histogram,
            &imara_input,
            ImaraSink {
                old_params: &mut old_params,
                new_params: &mut new_params,
                old_cur_i: 0,
                new_cur_i: 0,
            },
        );

        // Search for more reuse opportunities, allowing reordering this time
        old_params.sort_by_key(|pi| pi.id.0);

        for new_param in &mut new_params {
            if !new_param.kp_entry_i.is_null() {
                continue;
            }

            // Find the first `old_params[i]` with matching `id`
            let id = new_param.id;
            let Ok(mut i) = old_params.binary_search_by_key(&id.0, |old_param| old_param.id.0)
            else {
                continue;
            };
            while old_params
                .get(i.wrapping_sub(1))
                .is_some_and(|old_param| old_param.id == id)
            {
                i -= 1;
            }

            let old_param = &mut old_params[i];
            new_param.kp_entry_i = old_param.kp_entry_i;
            old_param.kp_entry_i = kdlplus::EntryI::null();
            new_param.reorder = true;
        }

        // Remove unused KDL entries
        for old_param in &old_params {
            let kp_entry_i = old_param.kp_entry_i;
            if kp_entry_i.is_null() {
                continue;
            }

            kp_item
                .entries_mut(&mut kp_arena.entries)
                .remove(kp_entry_i);
            kp_arena.entries[kp_entry_i].parent_node_i = None;
            cb.remove_entry(&mut kp_arena.entries, kp_entry_i);
        }

        // Detach the reused KDL entries with `reorder == true`
        for new_param in &new_params {
            if new_param.reorder {
                kp_item
                    .entries_mut(&mut kp_arena.entries)
                    .remove(new_param.kp_entry_i);
            }
        }

        // Create new KDL entries, but mark them with `reorder = true` and leave
        // them detached for now
        for new_param in &mut new_params {
            if !new_param.kp_entry_i.is_null() {
                continue;
            }

            new_param.kp_entry_i = kp_arena.entries.insert(kdlplus::Entry {
                parent_node_i: Some(kp_item_i),
                // `value` is set later
                ..kdlplus::Entry::new(kdl::KdlValue::Null)
            });
            new_param.reorder = true;
        }

        // Re-attach the new/reused KDL entries with `reorder == true`.
        //
        // Place each entry as close to the tail as possible because:
        // 1. It's a tad easier to implement
        // 2. We don't want place them before arguments (id-less entries)
        let mut insert_at = None;
        for new_param in rev(&new_params) {
            if new_param.reorder {
                kp_item
                    .entries_mut(&mut kp_arena.entries)
                    .insert(new_param.kp_entry_i, insert_at);
            }

            insert_at = Some(new_param.kp_entry_i);
        }

        // Unparse `Param::value`
        for (new_param, param) in izip!(&new_params, params) {
            param
                .value
                .unparse_entry(&mut kp_arena.entries[new_param.kp_entry_i]);
        }
    }
}

impl expr::Expr {
    fn parse(kp_entry: &kdlplus::Entry) -> ParseResult<Self> {
        if let Ok(value) = kdl_expect_string(kp_entry) {
            return Ok(Self::Complex {
                source: value.to_owned(),
            });
        }

        let value: f64 = kdl_expect_real(kp_entry).map_err(|mut e| {
            if e.kind == ErrorKind::ArgNotNumber {
                e.kind = ErrorKind::ArgNotNumberOrString;
            }
            e
        })?;
        Ok(Self::Literal { value })
    }

    fn unparse_entry(&self, kp_entry: &mut kdlplus::Entry) {
        match *self {
            Self::Literal { value } => kdl_set_real(kp_entry, value),
            Self::Complex { ref source } => kdl_set_string(kp_entry, source),
        }
    }
}

impl Symbol {
    fn parse(
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        // Arguments
        kdl_expect_args(kp_arena, kp_item, 0..=0)?;

        // Items
        let mut items = Vec::new();

        if let Some(kp_children_i) = kp_item.children {
            let kp_children = &kp_arena.documents[kp_children_i];
            for (_, kp_sitem) in kp_children.nodes(&kp_arena.nodes).iter() {
                items.push(SymbolItem::parse(kp_arena, kp_sitem)?);
            }
        }

        Ok(Self {
            kp_node_i: kp_item_i,
            items,
        })
    }

    fn unparse(
        &mut self,
        _cb: &mut dyn UnparseCallbacks,
        _kp_arena: &mut kdlplus::Arena,
        _kp_item_i: kdlplus::NodeI,
    ) {
        // TODO: `Symbol::unparse` [tag:unparse_symbol]
    }
}

impl SymbolItem {
    fn parse(kp_arena: &kdlplus::Arena, kp_sitem: &kdlplus::Node) -> ParseResult<Self> {
        let name = kp_sitem.name.value();

        if name == "g" {
            Ok(Self::Path(SymbolPath::parse(kp_arena, kp_sitem)?))
        } else if name == "terminal" {
            Ok(Self::Terminal(SymbolTerminal::parse(kp_arena, kp_sitem)?))
        } else {
            Err(ParseError::new(
                kp_sitem.name.span(),
                ErrorKind::UnexpectedSymbolItemNodeName,
            ))
        }
    }
}

impl SymbolTerminal {
    fn parse(kp_arena: &kdlplus::Arena, kp_sitem: &kdlplus::Node) -> ParseResult<Self> {
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_sitem, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        // Props
        let mut x = None;
        let mut y = None;
        for (_, entry) in kp_sitem.entries(&kp_arena.entries).iter() {
            let Some(name) = &entry.name else { continue };
            // TODO: Deny duplicates
            match name.value() {
                "x" => x = Some(kdl_expect_int(entry)?),
                "y" => y = Some(kdl_expect_int(entry)?),
                _ => {}
            }
        }

        Ok(Self {
            id,
            x: x.ok_or_else(|| kdl_error_missing_prop(kp_sitem, "x"))?,
            y: y.ok_or_else(|| kdl_error_missing_prop(kp_sitem, "y"))?,
        })
    }
}

impl SymbolPath {
    fn parse(kp_arena: &kdlplus::Arena, kp_sitem: &kdlplus::Node) -> ParseResult<Self> {
        // Arguments
        kdl_expect_args(kp_arena, kp_sitem, 0..=0)?;

        // Props
        let mut fill = None;
        let mut stroke = None;
        let mut width = None;
        let mut d = None;
        for (_, entry) in kp_sitem.entries(&kp_arena.entries).iter() {
            let Some(name) = &entry.name else { continue };
            // TODO: Deny duplicates
            match name.value() {
                "fill" => fill = Some(FillColor::parse(entry)?),
                "stroke" => stroke = Some(StrokeColor::parse(entry)?),
                "width" => width = Some(kdl_expect_int(entry)?),
                "d" => {
                    d = Some(
                        svgtypes::SimplifyingPathParser::from(kdl_expect_string(entry)?)
                            .collect::<Result<Vec<_>, svgtypes::Error>>()
                            .map_err(|error| {
                                ParseError::new(entry.span, ErrorKind::ParseSvgPath { error })
                            })?,
                    )
                }
                _ => {}
            }
        }

        Ok(Self {
            fill,
            stroke,
            width,
            d: d.ok_or_else(|| kdl_error_missing_prop(kp_sitem, "d"))?,
        })
    }
}

impl FillColor {
    fn parse(kp_entry: &kdlplus::Entry) -> ParseResult<Self> {
        let value = kdl_expect_string(kp_entry)?;

        if let Some(color) = parse_color(value) {
            return Ok(Self::Const(color));
        }

        if let Some((terminal, "")) = parse_color_terminal(value) {
            return Ok(Self::Potential(terminal.to_owned()));
        }

        Err(ParseError::new(kp_entry.span, ErrorKind::UnexpectedColor))
    }
}

impl StrokeColor {
    fn parse(kp_entry: &kdlplus::Entry) -> ParseResult<Self> {
        let value = kdl_expect_string(kp_entry)?;

        if let Some(color) = parse_color(value) {
            return Ok(Self::Const(color));
        }

        'a: {
            let Some((terminal_start, rest)) = parse_color_terminal(value) else {
                break 'a;
            };

            if rest.is_empty() {
                // Uniform
                return Ok(Self::Potential([
                    terminal_start.to_owned(),
                    terminal_start.to_owned(),
                ]));
            }

            let Some(rest) = rest.trim_start().strip_prefix(',') else {
                break 'a;
            };

            let Some((terminal_end, "")) = parse_color_terminal(rest) else {
                break 'a;
            };

            // Gradient
            return Ok(Self::Potential([
                terminal_start.to_owned(),
                terminal_end.to_owned(),
            ]));
        }

        Err(ParseError::new(kp_entry.span, ErrorKind::UnexpectedColor))
    }
}

impl ParamDef {
    fn parse(
        kp_arena: &kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
        kp_item: &kdlplus::Node,
    ) -> ParseResult<Self> {
        // Arguments
        let args = kdl_expect_args(kp_arena, kp_item, 1..=1)?;
        let id = kdl_expect_string(args[0].1)?.to_owned();

        // Props
        let mut default_val = None;
        let mut description = None;
        for (_, entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &entry.name else { continue };
            // TODO: Deny duplicates
            match name.value() {
                "default" => default_val = Some(kdl_expect_real(entry)?),
                "help" => description = Some(kdl_expect_string(entry)?.to_owned()),
                _ => {}
            }
        }

        Ok(Self {
            kp_node_i: kp_item_i,
            id,
            default_val,
            description,
        })
    }

    fn unparse(
        &mut self,
        cb: &mut dyn UnparseCallbacks,
        kp_arena: &mut kdlplus::Arena,
        kp_item_i: kdlplus::NodeI,
    ) {
        let Self {
            kp_node_i: _,
            id,
            default_val,
            description,
        } = self;
        let kp_item = &mut kp_arena.nodes[kp_item_i];

        kdl_ensure_no_document(cb, &mut kp_arena.documents, kp_item);

        // Arguments
        let [(_, k_id)] = kdl_ensure_args_const(cb, &mut kp_arena.entries, kp_item_i, kp_item);
        kdl_set_string(k_id, id);

        // Props
        let mut kp_default_val_i = None;
        let mut kp_description_i = None;

        for (kp_entry_i, kp_entry) in kp_item.entries(&kp_arena.entries).iter() {
            let Some(name) = &kp_entry.name else { continue };
            match name.value() {
                "default" => kp_default_val_i = Some(kp_entry_i),
                "help" => kp_description_i = Some(kp_entry_i),
                _ => {}
            }
        }

        // Unparse `default_val`
        if let Some((kp_default_val_i, value)) = kdl_ensure_entry_optional(
            cb,
            &mut kp_arena.entries,
            kp_item_i,
            kp_item,
            kp_default_val_i,
            *default_val,
        ) {
            let kp_entry = &mut kp_arena.entries[kp_default_val_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("default"));
            kdl_set_real(kp_entry, value);
        }

        // Unparse `description`
        if let Some((kp_description_i, value)) = kdl_ensure_entry_optional(
            cb,
            &mut kp_arena.entries,
            kp_item_i,
            kp_item,
            kp_description_i,
            description.as_ref(),
        ) {
            let kp_entry = &mut kp_arena.entries[kp_description_i];
            kdl_set_identifier_optional(&mut kp_entry.name, Some("help"));
            kdl_set_string(kp_entry, value);
        }
    }
}

fn kdl_expect_args<'a>(
    kp_arena: &'a kdlplus::Arena,
    kp_node: &'a kdlplus::Node,
    count: RangeInclusive<usize>,
) -> ParseResult<Vec<(kdlplus::EntryI, &'a kdlplus::Entry)>> {
    let entries: Vec<_> = kp_node
        .entries(&kp_arena.entries)
        .iter()
        .filter(|(_, e)| e.name.is_none())
        .collect();
    if count.contains(&entries.len()) {
        Ok(entries)
    } else {
        Err(ParseError::new(
            kp_node.span,
            ErrorKind::ArgCountMismatch {
                expected_min: *count.start(),
                expected_max: *count.end(),
                actual: entries.len(),
            },
        ))
    }
}

fn kdl_expect_bool(kp_entry: &kdlplus::Entry) -> ParseResult<bool> {
    kp_entry
        .value
        .as_bool()
        .ok_or_else(|| ParseError::new(kp_entry.span, ErrorKind::ArgNotBool))
}

fn kdl_expect_int<T: num_traits::FromPrimitive>(kp_entry: &kdlplus::Entry) -> ParseResult<T> {
    T::from_i128(
        kp_entry
            .value
            .as_integer()
            .ok_or_else(|| ParseError::new(kp_entry.span, ErrorKind::ArgNotInt))?,
    )
    .ok_or_else(|| ParseError::new(kp_entry.span, ErrorKind::ArgOutOfRange))
}

fn kdl_expect_real<T: num_traits::FromPrimitive>(kp_entry: &kdlplus::Entry) -> ParseResult<T> {
    // `KdlValue::{as_i64,as_f64}` match mutually-exclusive variants
    if let Ok(x) = kdl_expect_int(kp_entry) {
        return Ok(x);
    }

    T::from_f64(
        kp_entry
            .value
            .as_float()
            .ok_or_else(|| ParseError::new(kp_entry.span, ErrorKind::ArgNotNumber))?,
    )
    .ok_or_else(|| ParseError::new(kp_entry.span, ErrorKind::ArgOutOfRange))
}

fn kdl_expect_string(kp_entry: &kdlplus::Entry) -> ParseResult<&str> {
    kp_entry
        .value
        .as_string()
        .ok_or_else(|| ParseError::new(kp_entry.span, ErrorKind::ArgNotString))
}

fn kdl_error_missing_prop(kp_node: &kdlplus::Node, name: &'static str) -> ParseError {
    ParseError::new(kp_node.span, ErrorKind::MissingProp { name })
}

/// Parse a terminal potential reference in a color value (e.g., `e[base]`).
///
/// If successful, returns two substrings of `value`: the terminal name and
/// the remaining part of the string.
fn parse_color_terminal(mut value: &str) -> Option<(&str, &str)> {
    value = value.trim().strip_prefix("e[")?;
    // FIXME: Deny terminal names containing `]` to guarantee roundtrip
    // serialization
    let i = value.find(']')?;
    let terminal_id = &value[..i];
    let rest = &value[i + 1..];
    Some((terminal_id, rest))
}

fn parse_color(value: &str) -> Option<layout::Color> {
    match value {
        "none" => Some(layout::Color::None),
        "default" => Some(layout::Color::Default),
        "red" => Some(layout::Color::Red),
        "yellow" => Some(layout::Color::Yellow),
        "green" => Some(layout::Color::Green),
        "blue" => Some(layout::Color::Blue),
        "purple" => Some(layout::Color::Purple),
        "brown" => Some(layout::Color::Brown),
        _ => None,
    }
}

/// Update the string value of [`kdl::KdlIdentifier`]. Preserves the non-default
/// representation if possible.
fn kdl_set_identifier(kp_node: &mut kdl::KdlIdentifier, value: &str) {
    if kp_node.value() != value {
        kp_node.set_value(value);
        kp_node.clear_format();
    }
}

/// Update the string value of `Option<`[`kdl::KdlIdentifier`]`>`.
/// Preserves the non-default representation if possible.
fn kdl_set_identifier_optional(kp_node: &mut Option<kdl::KdlIdentifier>, value: Option<&str>) {
    match (&mut *kp_node, value) {
        (None, None) => {}
        (None, Some(value)) => *kp_node = Some(kdl::KdlIdentifier::from(value)),
        (Some(_), None) => *kp_node = None,
        (Some(kp_node), Some(value)) => {
            if kp_node.value() != value {
                kp_node.set_value(value);
                kp_node.clear_format();
            }
        }
    }
}

/// Insert a child document if it doesn't exist yet.
fn kdl_ensure_document<'a>(
    kp_document_arena: &'a mut kdlplus::DocumentArena,
    kp_node_arena: &mut kdlplus::NodeArena,
    kp_node_i: kdlplus::NodeI,
) -> (kdlplus::DocumentI, &'a mut kdlplus::Document) {
    let kp_children_i = *kp_node_arena[kp_node_i].children.get_or_insert_with(|| {
        kp_document_arena.insert(kdlplus::Document {
            parent_node_i: Some(kp_node_i),
            ..<_>::default()
        })
    });
    (kp_children_i, &mut kp_document_arena[kp_children_i])
}

/// Remove a child document.
fn kdl_ensure_no_document(
    cb: &mut dyn UnparseCallbacks,
    kp_document_arena: &mut kdlplus::DocumentArena,
    kp_node: &mut kdlplus::Node,
) {
    let Some(kp_children_i) = kp_node.children.take() else {
        return;
    };
    kp_document_arena[kp_children_i].parent_node_i = None;
    cb.remove_document(kp_document_arena, kp_children_i);
}

/// Insert a node if it doesn't exist yet.
fn kdl_ensure_node(
    kp_node_arena: &mut kdlplus::NodeArena,
    kp_parent_document_i: kdlplus::DocumentI,
    kp_parent_document: &mut kdlplus::Document,
    kp_node_i: Option<kdlplus::NodeI>,
    name: &str,
) -> kdlplus::NodeI {
    kp_node_i.unwrap_or_else(|| {
        let kp_node_i = kp_node_arena.insert(kdlplus::Node {
            parent_document_i: Some(kp_parent_document_i),
            ..kdlplus::Node::new(name)
        });
        kp_parent_document
            .nodes_mut(kp_node_arena)
            .push_back(kp_node_i);
        kp_node_i
    })
}

/// Insert or remove a node as necessary to ensure its (non-)existence
/// depending on whether `data` is `Some(_)`.
///
/// Returns a node and `data_` if and only if `data` is `Some(data_)`.
fn kdl_ensure_node_optional<T>(
    cb: &mut dyn UnparseCallbacks,
    kp_all_nodes: &mut kdlplus::NodeArena,
    kp_parent_document_i: kdlplus::DocumentI,
    kp_parent_document: &mut kdlplus::Document,
    kp_node_i: Option<kdlplus::NodeI>,
    name: &str,
    data: Option<T>,
) -> Option<(kdlplus::NodeI, T)> {
    match (kp_node_i, data) {
        (None, None) => None,
        (Some(node_i), Some(data)) => Some((node_i, data)),
        (None, Some(data)) => {
            // Insert a node
            let kp_node_i = kp_all_nodes.insert(kdlplus::Node {
                parent_document_i: Some(kp_parent_document_i),
                ..kdlplus::Node::new(name)
            });
            kp_parent_document
                .nodes_mut(kp_all_nodes)
                .push_back(kp_node_i);
            Some((kp_node_i, data))
        }
        (Some(kp_node_i), None) => {
            // Remove the existing node
            kp_parent_document.nodes_mut(kp_all_nodes).remove(kp_node_i);
            kp_all_nodes[kp_node_i].parent_document_i = None;
            cb.remove_node(kp_all_nodes, kp_node_i);
            None
        }
    }
}

/// Ensure `kp_node` contains `N` arguments by removing or inserting ones
/// as necessary.
///
/// Returns the indices of and references to the `N` arguments.
fn kdl_ensure_args_const<'a, const N: usize>(
    cb: &mut dyn UnparseCallbacks,
    kp_entries: &'a mut kdlplus::EntryArena,
    kp_node_i: kdlplus::NodeI,
    kp_node: &mut kdlplus::Node,
) -> [(kdlplus::EntryI, &'a mut kdlplus::Entry); N] {
    let kp_entry_i_list = kdl_ensure_args(cb, kp_entries, kp_node_i, kp_node, N);
    let kp_entry_i_list: [kdlplus::EntryI; N] = kp_entry_i_list.try_into().unwrap();
    let kp_entry_list = kp_entries
        .get_disjoint_mut(kp_entry_i_list)
        .unwrap_or_else(|| panic!("`EntryArena::get_disjoint_mut` failed: {kp_entry_i_list:?}"));
    kp_entry_i_list.zip_with(kp_entry_list, |x, y| (x, y))
}

/// Ensure `kp_node` contains `count` arguments by removing or inserting ones
/// as necessary.
///
/// Returns the indices of the `count` arguments.
fn kdl_ensure_args(
    cb: &mut dyn UnparseCallbacks,
    kp_entries: &mut kdlplus::EntryArena,
    kp_node_i: kdlplus::NodeI,
    kp_node: &mut kdlplus::Node,
    count: usize,
) -> Vec<kdlplus::EntryI> {
    // Find the existing arguments
    let mut entries: Vec<_> = kp_node
        .entries(kp_entries)
        .iter()
        .filter_map(|(e_i, e)| match_ok!(e.name, None => e_i))
        .collect();

    // Adjust argument count
    while entries.len() > count {
        let kp_entry_i = entries.pop().unwrap();
        kp_node.entries_mut(kp_entries).remove(kp_entry_i);
        kp_entries[kp_entry_i].parent_node_i = None;
        cb.remove_entry(kp_entries, kp_entry_i);
    }

    while entries.len() < count {
        let kp_entry = kdlplus::Entry {
            parent_node_i: Some(kp_node_i),
            ..kdlplus::Entry::new(kdl::KdlValue::String(String::new()))
        };
        let kp_entry_i = kp_entries.insert(kp_entry);

        let insert_at = entries
            .last()
            .map(|&i| kp_entries[i].sibling_entries.unwrap().next)
            .filter(|&i| Some(i) != kp_node.entries.first);
        kp_node
            .entries_mut(kp_entries)
            .insert(kp_entry_i, insert_at);
        entries.push(kp_entry_i);
    }

    entries
}

/// Update `kp_entry` so that [`kdl_expect_bool()`] returns a given value for
/// it.
fn kdl_set_bool(kp_entry: &mut kdlplus::Entry, x: bool) {
    if kdl_expect_bool(kp_entry).is_ok_and(|old| old == x) {
        return;
    }
    kp_entry.value = kdl::KdlValue::Bool(x);
    kp_entry.reset_value_repr();
}

/// Update `kp_entry` so that [`kdl_expect_int()`] returns a given value for
/// it.
fn kdl_set_int(kp_entry: &mut kdlplus::Entry, x: i64) {
    if kdl_expect_int(kp_entry).is_ok_and(|old: i64| old == x) {
        return;
    }
    kp_entry.value = kdl::KdlValue::Integer(x.into());
    kp_entry.reset_value_repr();
}

/// Update `kp_entry` so that [`kdl_expect_real()`] returns a given value for
/// it.
fn kdl_set_real(kp_entry: &mut kdlplus::Entry, x: f64) {
    if kdl_expect_real(kp_entry).is_ok_and(|old: f64| old == x) {
        return;
    }
    kp_entry.value = kdl::KdlValue::Float(x);
    kp_entry.reset_value_repr();
}

/// Update `kp_entry` so that [`kdl_expect_string()`] returns a given value for
/// it.
fn kdl_set_string(kp_entry: &mut kdlplus::Entry, x: &str) {
    if kdl_expect_string(kp_entry).is_ok_and(|old| old == x) {
        return;
    }
    kp_entry.value = kdl::KdlValue::String(x.to_owned());
    kp_entry.reset_value_repr();
}

/// Insert or remove an entry as necessary to ensure its (non-)existence
/// depending on whether `data` is `Some(_)`.
///
/// Returns a node and `data_` if and only if `data` is `Some(data_)`.
fn kdl_ensure_entry_optional<T>(
    cb: &mut dyn UnparseCallbacks,
    kp_all_entries: &mut kdlplus::EntryArena,
    kp_parent_node_i: kdlplus::NodeI,
    kp_parent_node: &mut kdlplus::Node,
    kp_entry_i: Option<kdlplus::EntryI>,
    data: Option<T>,
) -> Option<(kdlplus::EntryI, T)> {
    match (kp_entry_i, data) {
        (None, None) => None,
        (Some(entry_i), Some(data)) => Some((entry_i, data)),
        (None, Some(data)) => {
            // Insert a entry
            let kp_entry_i = kp_all_entries.insert(kdlplus::Entry {
                parent_node_i: Some(kp_parent_node_i),
                ..kdlplus::Entry::new(kdl::KdlValue::Null)
            });
            kp_parent_node
                .entries_mut(kp_all_entries)
                .push_back(kp_entry_i);
            Some((kp_entry_i, data))
        }
        (Some(kp_entry_i), None) => {
            // Remove the existing entry
            kp_parent_node
                .entries_mut(kp_all_entries)
                .remove(kp_entry_i);
            kp_all_entries[kp_entry_i].parent_node_i = None;
            cb.remove_entry(kp_all_entries, kp_entry_i);
            None
        }
    }
}
