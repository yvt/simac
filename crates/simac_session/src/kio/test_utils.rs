use futures::executor::block_on;
use fxhash::FxHashMap;
use itertools::chain;
use macro_rules_attribute::derive;
use std::{
    path::PathBuf,
    sync::{LazyLock, Mutex},
};

use super::*;
use crate::{circuit, meta, sim_control};

static STD_PATH: LazyLock<PathBuf> =
    LazyLock::new(|| concat!(env!("CARGO_MANIFEST_DIR"), "/std.simac").into());

#[derive(Default, Clone)]
pub(super) struct StdEnv {
    state: Arc<Mutex<StdEnvState>>,
    /// If set, [`PreloadEnv::read_file`] rewrites `simac` node to write out
    /// `ast::VERSION` explicitly.
    pub should_make_version_explicit: bool,
}

#[derive(Default)]
struct StdEnvState {
    pub written_files: FxHashMap<PathBuf, Vec<u8>>,
}

impl Env for StdEnv {
    type Path = PathBuf;
}

impl PreloadEnv for StdEnv {
    #[tracing::instrument(level = "debug", skip(self), ret)]
    fn resolve_path(
        &self,
        referer: &Self::Path,
        path_str: &str,
    ) -> Result<Self::Path, Self::ResolvePathError> {
        if path_str == "builtin:std" {
            Ok(STD_PATH.clone())
        } else {
            referer.join(path_str).canonicalize()
        }
    }

    type ResolvePathError = std::io::Error;

    #[tracing::instrument(level = "debug", skip(self), ret)]
    async fn read_file(&self, path: &Self::Path) -> Result<Vec<u8>, Self::ReadFileError> {
        let state = self.state.lock().unwrap();
        if let Some(data) = state.written_files.get(path) {
            return Ok(data.clone());
        }

        let mut data = std::fs::read_to_string(path)?;
        if self.should_make_version_explicit {
            data = kdl_make_version_explicit(data);
        }
        Ok(data.into_bytes())
    }

    type ReadFileError = std::io::Error;

    #[tracing::instrument(level = "debug", skip(self), ret)]
    fn get_loaded_file(&self, _path: &Self::Path) -> Option<Entity> {
        None
    }
}

impl PresaveEnv for StdEnv {}

impl SaveEnv for StdEnv {
    fn relativize_path(
        &self,
        referer: &Self::Path,
        target: &Self::Path,
        old_path_str: Option<&str>,
    ) -> Result<String, Self::RelativizePathError> {
        if *target == *STD_PATH {
            return Ok("builtin:std".to_owned());
        }

        if let Some(old_path_str) = old_path_str {
            if referer
                .join(old_path_str)
                .canonicalize()
                .is_ok_and(|x| x == *target)
            {
                return Ok(old_path_str.to_string());
            }
        }

        Ok(target
            .canonicalize()?
            .to_str()
            .ok_or_else(|| std::io::Error::other("path not UTF-8"))?
            .to_owned())
    }

    type RelativizePathError = std::io::Error;

    async fn write_file(
        &self,
        _ent_file: Entity,
        path: &Self::Path,
        data: Vec<u8>,
    ) -> Result<(), Self::WriteFileError> {
        let mut state = self.state.lock().unwrap();
        *state.written_files.entry(path.clone()).or_default() = data;
        Ok(())
    }

    type WriteFileError = std::convert::Infallible;
}

/// Rewrite `simac` node to write out [`ast::VERSION`] explicitly.
fn kdl_make_version_explicit(kdl: String) -> String {
    let mut k_doc: kdl::KdlDocument = kdl.parse().expect("parse");

    let k_meta = k_doc
        .get_mut("simac")
        .expect("unable to locate `simac` node");
    let k_meta_children = k_meta.ensure_children();
    if let Some(k_version) = k_meta_children.get("version") {
        let [v0, v1, v2] = k_version.entries() else {
            unreachable!("incorrect entry count for `version` node")
        };
        let version = [v0, v1, v2].map(|n| n.value().as_integer().unwrap() as u32);
        if version == ast::VERSION {
            return kdl;
        }
        panic!(
            "The provided file has version {version:?}, which is \
            different from current version {:?}. This file is \
            unsuitable for roundtrip conversion test because \
            version migration might alter the file syntax.",
            ast::VERSION,
        );
    }

    tracing::debug!("Inserting `version` node");

    let mut k_version = kdl::KdlNode::new("version");
    k_version
        .entries_mut()
        .extend(ast::VERSION.map(|i| i128::from(i).into()));
    k_meta_children.nodes_mut().push(k_version);

    k_doc.to_string()
}

#[tracing::instrument(skip(world))]
pub fn load(world: &mut World, path: PathBuf) -> Loaded<PathBuf> {
    load_inner(world, &StdEnv::default(), &path)
}

/// Load a workspace from a provided string.
///
/// Returns [`Loaded`] and the file entity.
#[tracing::instrument(skip(world, kdl))]
pub fn load_str(world: &mut World, kdl: &str) -> (Loaded<PathBuf>, Entity) {
    let env = StdEnv::default();

    // Fake file path
    let path = PathBuf::from("fakefile.kdl");

    // Write `kdl` to a fake file
    tracing::trace!(?path, kdl);
    block_on(env.write_file(Entity::PLACEHOLDER, &path, kdl.as_bytes().to_owned()))
        .expect("write_file");

    let loaded = load_inner(world, &env, &path);

    // Find the file entity corresponding to `path`
    let ent_file = loaded.file_ent_map[&path];

    (loaded, ent_file)
}

fn load_inner(world: &mut World, env: &StdEnv, path: &PathBuf) -> Loaded<PathBuf> {
    let mut load_cx = LoadCx::default();
    block_on(load_cx.preload(env, path))
        .map_err(anyhow::Error::from)
        .expect("preload");

    fn sys_load(
        In(load_cx): In<LoadCx<PathBuf>>,
        commands: Commands<'_, '_>,
        circuits: Query<ExistingCircuitData>,
    ) -> Result<Loaded<PathBuf>, LoadError<PathBuf>> {
        load_cx.load(commands, circuits)
    }

    world
        .run_system_cached_with(sys_load, load_cx)
        .unwrap()
        .map_err(anyhow::Error::from)
        .expect("load")
}

/// Save a workspace to a string.
#[tracing::instrument(skip(world))]
pub fn save_to_str(world: &mut World, ent_file: Entity) -> String {
    let env = StdEnv::default();

    fn sys_presave(
        In((env, ent_file)): In<(StdEnv, Entity)>,
        param: PresaveParam<'_, '_, PathBuf>,
    ) -> SaveCx<PathBuf> {
        SaveCx::presave(&env, &param, &[ent_file])
    }

    let save_cx = world
        .run_system_cached_with(sys_presave, (env.clone(), ent_file))
        .expect("presave");
    tracing::debug!(?save_cx);

    // Write to an on-memory file
    block_on(save_cx.save(&env))
        .map_err(anyhow::Error::from)
        .expect("save");

    // Read the on-memory file
    let comp_file: &File<<StdEnv as Env>::Path> = world
        .get(ent_file)
        .unwrap_or_else(|| panic!("{ent_file} lacks `File<Path>`"));

    let kdl = block_on(PreloadEnv::read_file(&env, &comp_file.path)).expect("read_file");

    String::from_utf8(kdl).expect("written KDL file is not a valid UTF-8 string")
}

/// A loaded workspace.
#[derive(Debug)]
pub struct Workspace {
    pub world: World,
    pub loaded: Loaded<PathBuf>,
    pub ent_file: Entity,
    pub ent_sim: Entity,
    pub ent_main_circuit: Entity,
}

impl Workspace {
    /// Load a workspace from a file.
    pub fn load_file(path: &std::path::Path) -> Self {
        let path = path.to_owned();
        let mut world = World::default();
        let loaded = load(&mut world, path.clone());
        tracing::info!(?world);

        let ent_file = loaded.file_ent_map[&path];

        Self::load_inner(world, loaded, ent_file)
    }

    /// Load a workspace from a file.
    pub fn load_str(kdl: &str) -> Self {
        let mut world = World::default();
        let (loaded, ent_file) = load_str(&mut world, kdl);
        Self::load_inner(world, loaded, ent_file)
    }

    fn load_inner(mut world: World, loaded: Loaded<PathBuf>, ent_file: Entity) -> Self {
        let (ent_sim, sim_control, _) = world
            .query::<(Entity, &sim_control::SimControl, &FileMember)>()
            .iter(&world)
            .find(|(_, _, file_member)| file_member.0 == ent_file)
            .expect("unable to locate workspace node");

        let ent_main_circuit = sim_control.ent_root_circuit;

        Self {
            world,
            loaded,
            ent_file,
            ent_sim,
            ent_main_circuit,
        }
    }

    pub fn save_to_str(&mut self) -> String {
        save_to_str(&mut self.world, self.ent_file)
    }

    pub fn main_circuit_item_by_id(&self, id: &str) -> Option<Entity> {
        let comp_circuit: &circuit::Circuit = self.world.get(self.ent_main_circuit).unwrap();
        chain!(&comp_circuit.devices, &comp_circuit.nodes).find_map(|&e| {
            let meta::Id(id_) = self.world.get(e)?;
            (id_ == id).then_some(e)
        })
    }
}
