use bevy_ecs::{entity::EntityHashMap, prelude::*};
use futures::StreamExt;
use fxhash::FxHashMap;
use itertools::{Itertools, izip};
use scopeguard::{ScopeGuard, guard};
use std::{cell::OnceCell, error::Error, fmt::Debug, hash::Hash, sync::Arc};
use try_match::match_ok;
use typed_index::{IndexExt, IndexMutExt, VecExt, impl_typed_index};

use crate::{
    circuit, expr,
    kio::{
        File, FileMember, Import, OriginalAstCircuit, OriginalAstFile, OriginalAstItem,
        OriginalKdl, ParseError, ast,
    },
    layout, meta, render, sim_control,
};

pub trait Env {
    /// The type for paths. They're assumed to be normalized so that they can
    /// be compared correctly.
    type Path: Path;
}

/// The trait to define filesystem operations for [`LoadCx::preload`].
#[expect(async_fn_in_trait)]
pub trait PreloadEnv: Env {
    /// Interpret the specified path string in the context of the specified
    /// file.
    fn resolve_path(
        &self,
        referer: &Self::Path,
        path_str: &str,
    ) -> Result<Self::Path, Self::ResolvePathError>;

    type ResolvePathError: Error;

    /// Read all bytes from the specified file.
    async fn read_file(&self, path: &Self::Path) -> Result<Vec<u8>, Self::ReadFileError>;

    type ReadFileError: Error;

    /// Get a file entity ([`File`]) for the specified path if it's already
    /// open.
    fn get_loaded_file(&self, path: &Self::Path) -> Option<Entity>;
}

/// The trait alias for types usable as a path type in [`Env`] and [`LoadCx`].
pub trait Path: Clone + Eq + Hash + Debug + Send + Sync + Sized + 'static {}
impl<T> Path for T where T: Clone + Eq + Hash + Debug + Send + Sync + 'static {}

#[derive(Debug, thiserror::Error)]
pub enum PreloadError<Path, ResolvePathError, ReadFileError> {
    #[error("failed to resolve path {path_str} in {referer:?}: {error}")]
    ResolvePath {
        #[source]
        error: ResolvePathError,
        path_str: String,
        referer: Path,
    },
    #[error("failed to read file {path:?}: {error}")]
    ReadFile {
        #[source]
        error: ReadFileError,
        path: Path,
    },
    #[error("failed to process input file")]
    Load(#[source] LoadError<Path>),
}

#[derive(Debug, thiserror::Error)]
pub enum LoadError<Path> {
    #[error("file {path:?} does not contain valid UTF-8 text")]
    FileNotUtf8 { path: Path },
    #[error("failed to parse file {path:?}")]
    ParseKdl {
        #[source]
        error: kdl::KdlError,
        path: Path,
    },
    #[error("failed to parse file {path:?}")]
    Parse {
        #[source]
        error: ParseError,
        source_code: String,
        path: Path,
    },
    #[error("failed to import file {path:?}")]
    Import {
        #[source]
        error: ImportErrorKind,
        // TODO: `SourceSpan`
        path: Path,
    },
}

#[derive(Debug, Clone, thiserror::Error, PartialEq, Eq)]
pub enum ImportErrorKind {
    #[error("no import named `{file_id}`")]
    ImportNotFound { file_id: String },
    #[error("import `{file_id}` does not provide a circuit named `{circuit_id}`")]
    CircuitNotFound { file_id: String, circuit_id: String },
    #[error("current file does not provide a circuit named `{circuit_id}`")]
    LocalCircuitNotFound { circuit_id: String },
    #[error("no item named `{item_id}`")]
    ItemNotFound { item_id: String },
    #[error("item `{item_id}` requires specifying a terminal name to use as a node")]
    ItemMemberMissing { item_id: String },
    #[error("item `{item_id}` does not provide a terminal named `{member}`")]
    ItemMemberNotFound { item_id: String, member: String },
    #[error("no terminal named `{terminal_id}`")]
    TerminalNotFound { terminal_id: String },
    #[error("symbol terminal is missing for terminal `{terminal_id}`")]
    SymbolTerminalMissing { terminal_id: String },
    #[error("element type '{elem_ty}' does not have a parameter named `{param_id}`")]
    ElementParamNotFound {
        elem_ty: &'static str,
        param_id: String,
    },
    #[error("parameter `{param_id}` (of element type '{elem_ty}') is set more than once")]
    ElementParamDuplicate {
        elem_ty: &'static str,
        param_id: &'static str,
    },
    #[error("circuit '{file_id}.{circuit_id}' does not have a parameter named `{param_id}`")]
    CircuitParamNotFound {
        file_id: String,
        circuit_id: String,
        param_id: String,
    },
    #[error("parameter `{param_id}` (of circuit '{file_id}.{circuit_id}') is set more than once")]
    CircuitParamDuplicate {
        file_id: String,
        circuit_id: String,
        param_id: String,
    },
}

/// A structure holding newly-loaded circuits that are yet
/// to be committed to a [`World`].
#[derive(Debug)]
pub struct LoadCx<Path> {
    files: Vec<PreloadFile<Path>>,
    file_map: FxHashMap<Path, FileI>,
}

/// [`LoadCx::files`]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct FileI(usize);

impl_typed_index!(impl<Path> TypedIndex<Vec<PreloadFile<Path>>> for FileI);

#[derive(Debug)]
struct PreloadFile<Path> {
    path: Path,
    ty: PreloadFileTy,
}

#[derive(Debug)]
enum PreloadFileTy {
    Loaded(Entity),
    // This variant is substantially larger than the other. Box it to reduce
    // the size difference. (Clippy lint `large_enum_variant`)
    ToLoad(Box<PreloadFileToLoad>),
}

impl PreloadFileTy {
    const PLACEHOLDER: Self = Self::Loaded(Entity::PLACEHOLDER);
}

#[derive(Debug)]
struct PreloadFileToLoad {
    kp_arena: Arc<kdlplus::Arena>,
    ast_file: ast::File,
    /// Import directives with their target files resolved to [`FileI`]
    imports: Vec<(String, FileI)>,
    /// A hashtable to look up entries in [`Self::imports`]
    import_map: FxHashMap<String, FileI>,
}

/// The output type of [`LoadCx::load`].
#[derive(Debug)]
pub struct Loaded<Path> {
    /// The file entities of the loaded files and the files referenced by them.
    pub file_ent_map: FxHashMap<Path, Entity>,
}

impl<Path> Default for LoadCx<Path> {
    fn default() -> Self {
        Self {
            files: Vec::new(),
            file_map: FxHashMap::default(),
        }
    }
}

impl<Path: self::Path> LoadCx<Path> {
    /// Perform the preliminary steps of loading a document file, including
    /// parsing and recursively locating imported files.
    #[tracing::instrument(skip_all)]
    pub async fn preload<PE>(
        &mut self,
        env: &PE,
        path: &PE::Path,
    ) -> Result<(), PreloadError<PE::Path, PE::ResolvePathError, PE::ReadFileError>>
    where
        PE: PreloadEnv<Path = Path>,
    {
        let mut ongoing_loads = futures::stream::FuturesUnordered::default();
        let start_load = |path: Path| async move {
            tracing::debug!(?path, "Calling `read_file`");
            let data = match env.read_file(&path).await {
                Ok(data) => data,
                Err(error) => return Err(PreloadError::ReadFile { error, path }),
            };

            // TODO: Return `file_i` instead of `path` because `files[file_i]`
            // is faster to evaluate than `file_map[&path]`
            Ok((path, data))
        };

        // Start reading `path`
        ongoing_loads.push(start_load(path.clone()));
        let file_i = self.files.push_typed(PreloadFile {
            path: path.clone(),
            ty: PreloadFileTy::PLACEHOLDER,
        });
        self.file_map.insert(path.clone(), file_i);

        while let Some(result) = ongoing_loads.next().await {
            // Process the result of `read_file`
            let (path, data) = result?;

            tracing::debug!(?path, data.len = data.len(), "`read_file` complete");

            let data = match String::from_utf8(data) {
                Ok(data) => data,
                Err(_) => return Err(PreloadError::Load(LoadError::FileNotUtf8 { path })),
            };
            tracing::trace!(?data);

            let k_doc: kdl::KdlDocument = match data.parse() {
                Ok(k_doc) => k_doc,
                Err(error) => return Err(PreloadError::Load(LoadError::ParseKdl { error, path })),
            };

            let mut kp_arena = kdlplus::Arena::default();
            let kp_doc_i = kp_arena.import_kdl_document(&k_doc);

            let ast_file = match ast::File::parse(&kp_arena, kp_doc_i) {
                Ok(file) => file,
                Err(error) => {
                    return Err(PreloadError::Load(LoadError::Parse {
                        error,
                        source_code: data,
                        path,
                    }));
                }
            };
            tracing::trace!(?ast_file);

            // Resolve imports
            let imports: Vec<(String, FileI)> = ast_file
                .imports
                .iter()
                .map(|ast_import| {
                    // Resolve the import path
                    let import_path =
                        env.resolve_path(&path, &ast_import.path).map_err(|error| {
                            PreloadError::ResolvePath {
                                error,
                                path_str: ast_import.path.clone(),
                                referer: path.clone(),
                            }
                        })?;

                    // Is the referred file already loaded?
                    let file_i =
                        *self
                            .file_map
                            .entry(import_path)
                            .or_insert_with_key(|import_path| {
                                let ty = if let Some(entity) = env.get_loaded_file(import_path) {
                                    // `env` says it's already loaded
                                    PreloadFileTy::Loaded(entity)
                                } else {
                                    // We need to load it
                                    ongoing_loads.push(start_load(import_path.clone()));
                                    PreloadFileTy::PLACEHOLDER
                                };
                                self.files.push_typed(PreloadFile {
                                    path: import_path.clone(),
                                    ty,
                                })
                            });

                    tracing::trace!(ast_import.id, ?file_i, "Processsed an import directive");

                    Ok((ast_import.id.clone(), file_i))
                })
                .collect::<Result<_, PreloadError<_, _, _>>>()?;

            let import_map = imports
                .iter()
                .map(|(id, file_i)| (id.clone(), *file_i))
                .collect();

            let file_i = self.file_map[&path];
            self.files.index_mut_typed(file_i).ty =
                PreloadFileTy::ToLoad(Box::new(PreloadFileToLoad {
                    kp_arena: Arc::new(kp_arena),
                    ast_file,
                    imports,
                    import_map,
                }));
        }

        Ok(())
    }

    /// Generate commands to load entities into a Bevy world.
    ///
    /// # Error handling
    ///
    /// To avoid entity leaks, the caller must flush the generated commands
    /// whether the method call was successful or not.
    #[tracing::instrument(skip_all)]
    pub fn load(
        self,
        // TODO: Consider taking `&mut World`
        mut commands: Commands<'_, '_>,
        circuits: Query<ExistingCircuitData>,
    ) -> Result<Loaded<Path>, LoadError<Path>> {
        // Entities to despawn on failure
        let mut guard = guard((Vec::new(), &mut commands), |(new_entities, commands)| {
            tracing::debug!(
                new_entities.len = new_entities.len(),
                "Despawning the created entities before returning unsuccessfully"
            );
            tracing::trace!(?new_entities, "The entities to despawn");
            for ent in new_entities {
                commands.entity(ent).despawn();
            }
        });
        let (new_entities, commands) = &mut *guard;

        // Create/get file entities
        // -------------------------------------------------------------------
        #[derive(Debug)]
        struct FileEntity(Entity);
        impl_typed_index!(impl TypedIndex<Vec<FileEntity>> for FileI);

        let ent_files: Vec<FileEntity> = self
            .files
            .iter()
            .map(|file| match file.ty {
                PreloadFileTy::Loaded(entity) => FileEntity(entity),
                PreloadFileTy::ToLoad(ref file_tl) => {
                    let ent_file = commands
                        .spawn((
                            OriginalKdl(Arc::clone(&file_tl.kp_arena)),
                            OriginalAstFile(file_tl.ast_file.clone()),
                        ))
                        .id();
                    new_entities.push(ent_file);
                    FileEntity(ent_file)
                }
            })
            .collect();

        let ent_to_file: EntityHashMap<FileI> = ent_files
            .iter_typed()
            .map(|(file_i, &FileEntity(entity))| (entity, file_i))
            .collect();

        tracing::trace!(?ent_files);

        // Insert `File` components to the file entities
        for (file, &FileEntity(ent_file)) in izip!(&self.files, &ent_files) {
            let PreloadFileTy::ToLoad(file_tl) = &file.ty else { continue };

            let imports = file_tl
                .imports
                .iter()
                .map(|(id, target_file_i)| Import {
                    id: id.clone(),
                    ent_file: ent_files.index_typed(*target_file_i).0,
                })
                .collect();

            commands.entity(ent_file).insert(File {
                path: file.path.clone(),
                imports,
            });
        }

        // Create/get circuit entities (but don't add components yet)
        // -------------------------------------------------------------------
        #[derive(Debug)]
        struct FileCircuits(FxHashMap<String, Entity>);
        impl_typed_index!(impl TypedIndex<Vec<FileCircuits>> for FileI);

        let mut ent_circuits: Vec<FileCircuits> = (0..self.files.len())
            .map(|_| FileCircuits(FxHashMap::default()))
            .collect();

        for ExistingCircuitDataItem {
            ent_circuit,
            file_member: FileMember(file),
            id: meta::Id(id),
            ..
        } in circuits.iter()
        {
            let Some(&file_i) = ent_to_file.get(file) else {
                continue;
            };

            assert!(matches!(
                self.files.index_typed(file_i).ty,
                PreloadFileTy::Loaded { .. }
            ));

            ent_circuits
                .index_mut_typed(file_i)
                .0
                .insert(id.clone(), ent_circuit);
        }

        for (ent_circuits, file) in izip!(&mut ent_circuits, &self.files) {
            let PreloadFileTy::ToLoad(file_tl) = &file.ty else {
                continue;
            };

            // TODO: Deny duplicate IDs
            ent_circuits.0 = file_tl
                .ast_file
                .circuits
                .iter()
                .map(|ast_circuit| {
                    let ent_circuit = commands.spawn_empty().id();
                    new_entities.push(ent_circuit);
                    (ast_circuit.id.clone(), ent_circuit)
                })
                .collect();
        }

        tracing::trace!(?ent_circuits);

        // Create `CircuitMeta`s (but don't insert them yet)
        // -------------------------------------------------------------------
        struct NewCircuitInfo<'a> {
            ast_circuit: &'a ast::Circuit,
            circuit: OnceCell<circuit::Circuit>,
            meta: meta::CircuitMeta,
            symbol: OnceCell<layout::Symbol>,
            file_i: FileI,
            file_tl: &'a PreloadFileToLoad,
        }

        let new_circuit_info_map: EntityHashMap<NewCircuitInfo<'_>> =
            izip!(&ent_circuits, self.files.iter_typed())
                .filter_map(|(ent_circuits, (file_i, file))| {
                    let file_tl = match_ok!(&file.ty, PreloadFileTy::ToLoad(_0))?;
                    Some((ent_circuits, file_tl, file_i))
                })
                .flat_map(|(ent_circuits, file_tl, file_i)| {
                    file_tl.ast_file.circuits.iter().map(move |ast_circuit| {
                        (
                            ast_circuit,
                            ent_circuits.0[&ast_circuit.id],
                            file_tl,
                            file_i,
                        )
                    })
                })
                .map(|(ast_circuit, ent_circuit, file_tl, file_i)| {
                    // Locate all terminals defined in the circuit
                    let mut terminal_names: Vec<String> = ast_circuit
                        .items
                        .iter()
                        .filter_map(match_ok!(, ast::Item::Terminal(_0)))
                        .map(|terminal| terminal.id.clone())
                        .collect();
                    terminal_names.sort_unstable();
                    terminal_names.dedup();

                    // Locate all circuit parameters
                    let params: Vec<meta::ParamDefMeta> = ast_circuit
                        .items
                        .iter()
                        .filter_map(match_ok!(, ast::Item::ParamDef(_0)))
                        .map(|param_def| meta::ParamDefMeta {
                            id: param_def.id.clone(),
                            default_val: param_def.default_val.unwrap_or(0.0),
                            description: param_def.description.clone().unwrap_or_default(),
                        })
                        .collect();
                    // TODO: Deny duplicate param defs

                    let meta = meta::CircuitMeta {
                        terminal_names,
                        params,
                    };

                    (
                        ent_circuit,
                        NewCircuitInfo {
                            ast_circuit,
                            circuit: OnceCell::new(), // `Circuit` is set later
                            meta,
                            symbol: OnceCell::new(), // `Circuit` is set later
                            file_i,
                            file_tl,
                        },
                    )
                })
                .collect();

        // Create `Circuit`s (but don't insert them yet)
        // -------------------------------------------------------------------
        let mut ent_node_list = Vec::new();

        let mut new_items: FxHashMap<&str, NewItem<'_>> = FxHashMap::default();
        enum NewItem<'a> {
            Subcircuit {
                /// The starting index in `ent_node_list`
                start_node_i: usize,
                target_meta: Option<&'a meta::CircuitMeta>,
            },
            Element {
                /// The starting index in `ent_node_list`
                start_node_i: usize,
                num_nodes: usize,
            },
            Terminal {
                ent_node: Entity,
            },
            Node {
                ent_node: Entity,
            },
        }

        for new_circuit_info in new_circuit_info_map.values() {
            let mut circuit = circuit::Circuit {
                num_params: new_circuit_info.meta.params.len(),
                ..Default::default()
            };
            let new_circuit_file_path =
                || self.files.index_typed(new_circuit_info.file_i).path.clone();
            let kp_arena = &new_circuit_info.file_tl.kp_arena;

            // `ent_nodes` stores lists of terminal nodes defined in the circuit
            // (Some of `new_items` reference this)
            ent_node_list.clear();

            // `new_items` stores all non-wire items defined in the circuit
            // (can be referenced by `ast::NodeRef`)
            new_items.clear();

            // The closure to construct a component bundle for terminal nodes
            let terminal_node_bundle = |device: Entity| {
                (
                    circuit::NodeBundle::default(),
                    meta::TerminalNode { device },
                    // `Position` value is implicitly set by `position_terminal_nodes`
                    layout::Position::default(),
                )
            };

            // The closure to spawn a variable number of terminal nodes
            let mut spawn_terminal_nodes =
                |num: usize, device: Entity, commands: &mut Commands<'_, '_>| {
                    let start_node_i = ent_node_list.len();
                    ent_node_list.extend(
                        (0..num).map(move |_| commands.spawn(terminal_node_bundle(device)).id()),
                    );

                    let nodes = Arc::from(&ent_node_list[start_node_i..]);

                    (start_node_i, nodes)
                };

            // The closure to convert `ast::DeviceLayout`
            let convert_device_layout = |layout: &ast::DeviceLayout| {
                let mut rotation: layout::Rotation = layout.angle.unwrap_or_default().into();
                if layout.flip == Some(true) {
                    rotation.toggle(layout::Rotation::FLIP_X);
                }
                layout::DeviceLayoutBundle {
                    position: layout::Position(
                        [layout.x.unwrap_or(0), layout.y.unwrap_or(0)].into(),
                    ),
                    rotation,
                }
            };

            // Spawn circuit items except wires
            for item in &new_circuit_info.ast_circuit.items {
                match item {
                    ast::Item::Subcircuit(item) => {
                        // Resolve target file
                        let Some(&target_file_i) = new_circuit_info
                            .file_tl
                            .import_map
                            .get(&item.target_file_id)
                        else {
                            return Err(LoadError::Import {
                                error: ImportErrorKind::ImportNotFound {
                                    file_id: item.target_file_id.clone(),
                                },
                                path: new_circuit_file_path(),
                            });
                        };

                        // Resolve target circuit
                        let target_file_circuits = ent_circuits.index_typed(target_file_i);
                        let Some(&ent_target_circuit) =
                            target_file_circuits.0.get(&item.target_circuit_id)
                        else {
                            return Err(LoadError::Import {
                                error: ImportErrorKind::CircuitNotFound {
                                    file_id: item.target_file_id.clone(),
                                    circuit_id: item.target_circuit_id.clone(),
                                },
                                path: new_circuit_file_path(),
                            });
                        };

                        // Get the target circuit's `CircuitMeta`
                        let target_meta =
                            if let Ok(existing_circuit) = circuits.get(ent_target_circuit) {
                                existing_circuit.meta.or_else(|| {
                                    tracing::warn!(
                                        ?item,
                                        ?ent_target_circuit,
                                        "The target circuit of the subcircuit device \
                                        does not have a `CircuitMeta` component",
                                    );
                                    None
                                })
                            } else {
                                Some(&new_circuit_info_map[&ent_target_circuit].meta)
                            };

                        // Resolve parameters
                        let param_defs = target_meta.map(|x| &x.params[..]).unwrap_or(&[]);
                        let mut params: Vec<_> = param_defs.iter().map(|_| None).collect();

                        for param in item.params.iter() {
                            let Some(param_i) = param_defs
                                .iter()
                                .position(|param_def| param_def.id == param.id)
                            else {
                                return Err(LoadError::Import {
                                    error: ImportErrorKind::CircuitParamNotFound {
                                        file_id: item.target_file_id.clone(),
                                        circuit_id: item.target_circuit_id.clone(),
                                        param_id: param.id.clone(),
                                    },
                                    path: new_circuit_file_path(),
                                });
                            };

                            if params[param_i].is_some() {
                                return Err(LoadError::Import {
                                    error: ImportErrorKind::CircuitParamDuplicate {
                                        file_id: item.target_file_id.clone(),
                                        circuit_id: item.target_circuit_id.clone(),
                                        param_id: param.id.clone(),
                                    },
                                    path: new_circuit_file_path(),
                                });
                            }

                            params[param_i] = Some(param.value.clone());
                        }

                        // Spawn the device entity
                        let ent_item = commands.spawn_empty().id();
                        circuit.devices.insert(ent_item);

                        // Spawn terminal nodes
                        let (start_node_i, terminals) = spawn_terminal_nodes(
                            target_meta.map_or(0, |x| x.terminal_names.len()),
                            ent_item,
                            commands,
                        );

                        // Finish building the device entity
                        commands.entity(ent_item).insert((
                            circuit::SubcircuitDeviceBundle {
                                device: circuit::DeviceBundle {
                                    device: circuit::Device { terminals },
                                },
                                subcircuit_device: circuit::SubcircuitDevice {
                                    circuit: ent_target_circuit,
                                },
                            },
                            expr::SubcircuitDevice { params },
                            meta::Id(item.id.clone()),
                            convert_device_layout(&item.layout),
                            OriginalKdl(Arc::clone(kp_arena)),
                            OriginalAstItem(ast::Item::Subcircuit(item.clone())),
                        ));

                        // Insert it into `new_items`
                        new_items.insert(
                            &item.id,
                            NewItem::Subcircuit {
                                start_node_i,
                                target_meta,
                            },
                        );
                    }
                    ast::Item::Element(item) => {
                        // Resolve parameters
                        let mut params: Vec<_> = (0..item.ty.num_params()).map(|_| None).collect();

                        for param in item.params.iter() {
                            // n.b. unnamed parameters cannot be referred to
                            let Some(param_i) = (0..params.len())
                                .find(|&i| item.ty.param_name(i) == Some(&param.id))
                            else {
                                return Err(LoadError::Import {
                                    error: ImportErrorKind::ElementParamNotFound {
                                        elem_ty: item.ty.name(),
                                        param_id: param.id.clone(),
                                    },
                                    path: new_circuit_file_path(),
                                });
                            };

                            if params[param_i].is_some() {
                                return Err(LoadError::Import {
                                    error: ImportErrorKind::ElementParamDuplicate {
                                        elem_ty: item.ty.name(),
                                        param_id: item.ty.param_name(param_i).unwrap(),
                                    },
                                    path: new_circuit_file_path(),
                                });
                            }
                            params[param_i] = Some(param.value.clone());
                        }

                        // Spawn the device entity
                        let ent_item = commands.spawn_empty().id();
                        circuit.devices.insert(ent_item);

                        // Spawn terminal nodes
                        let num_nodes = item.ty.num_nodes();
                        let (start_node_i, terminals) =
                            spawn_terminal_nodes(num_nodes, ent_item, commands);

                        // Finish building the device entity
                        commands.entity(ent_item).insert((
                            circuit::ElementDeviceBundle {
                                device: circuit::DeviceBundle {
                                    device: circuit::Device { terminals },
                                },
                                element_device: circuit::ElementDevice { elem_ty: item.ty },
                            },
                            expr::ElementDevice { params },
                            meta::Id(item.id.clone()),
                            convert_device_layout(&item.layout),
                            OriginalKdl(Arc::clone(kp_arena)),
                            OriginalAstItem(ast::Item::Element(item.clone())),
                        ));

                        // Insert it into `new_items`
                        new_items.insert(
                            &item.id,
                            NewItem::Element {
                                start_node_i,
                                num_nodes,
                            },
                        );
                    }
                    ast::Item::Terminal(item) => {
                        // Find the terminal index
                        let terminal_i = new_circuit_info
                            .meta
                            .terminal_names
                            .iter()
                            .position(|x| *x == item.id)
                            .unwrap();

                        // Spawn the device entity
                        let ent_item = commands.spawn_empty().id();
                        circuit.devices.insert(ent_item);

                        // Spawn terminal nodes
                        let (_, terminals) = spawn_terminal_nodes(1, ent_item, commands);
                        let ent_node = terminals[0];

                        // Finish building the device entity
                        commands.entity(ent_item).insert((
                            circuit::TerminalDeviceBundle {
                                device: circuit::DeviceBundle {
                                    device: circuit::Device { terminals },
                                },
                                terminal_device: circuit::TerminalDevice { terminal_i },
                            },
                            meta::Id(item.id.clone()),
                            convert_device_layout(&item.layout),
                            OriginalKdl(Arc::clone(kp_arena)),
                            OriginalAstItem(ast::Item::Terminal(item.clone())),
                        ));

                        // Insert it into `new_items`
                        new_items.insert(&item.id, NewItem::Terminal { ent_node });
                    }
                    ast::Item::Node(item) => {
                        // Spawn the node entity
                        let mut ent_node = commands.spawn((
                            circuit::NodeBundle::default(),
                            meta::Id(item.id.clone()),
                            layout::NodeLayoutBundle {
                                position: layout::Position(
                                    [item.x.unwrap_or(0), item.y.unwrap_or(0)].into(),
                                ),
                            },
                            OriginalKdl(Arc::clone(kp_arena)),
                            OriginalAstItem(ast::Item::Node(item.clone())),
                        ));
                        circuit.nodes.insert(ent_node.id());

                        if item.is_ground {
                            ent_node.insert(circuit::GroundNode);
                        }

                        // Insert it into `new_items`
                        new_items.insert(
                            &item.id,
                            NewItem::Node {
                                ent_node: ent_node.id(),
                            },
                        );
                    }
                    // Ignore wires in this step
                    ast::Item::Wire(_) => continue,
                    // Not an actual circuit item
                    ast::Item::Symbol(_) | ast::Item::ParamDef(_) => continue,
                } // match item
            } // for item

            // Spawn wires
            for item in &new_circuit_info.ast_circuit.items {
                // Ignore all but wires in this step
                let ast::Item::Wire(item) = item else {
                    continue;
                };

                // Resolve `NodeRef`s
                let terminals = item.nodes.each_ref().map(|node_ref| {
                    // [tag:kio_id_circuit_item]
                    let Some(target_item) = new_items.get(node_ref.item_id.as_str()) else {
                        return Err(ImportErrorKind::ItemNotFound {
                            item_id: node_ref.item_id.clone(),
                        });
                    };
                    match *target_item {
                        NewItem::Subcircuit {
                            start_node_i,
                            target_meta,
                        } => {
                            let Some(member) = &node_ref.member else {
                                return Err(ImportErrorKind::ItemMemberMissing {
                                    item_id: node_ref.item_id.clone(),
                                });
                            };
                            let Some(terminal_i) = target_meta.and_then(|meta| {
                                meta.terminal_names.iter().position(|x| x == member)
                            }) else {
                                return Err(ImportErrorKind::ItemMemberNotFound {
                                    item_id: node_ref.item_id.clone(),
                                    member: member.clone(),
                                });
                            };
                            Ok(ent_node_list[start_node_i + terminal_i])
                        }
                        NewItem::Element {
                            start_node_i,
                            num_nodes,
                        } => {
                            let Some(member) = &node_ref.member else {
                                return Err(ImportErrorKind::ItemMemberMissing {
                                    item_id: node_ref.item_id.clone(),
                                });
                            };
                            let Some(terminal_i) =
                                member.parse::<usize>().ok().filter(|&i| i < num_nodes)
                            else {
                                return Err(ImportErrorKind::ItemMemberNotFound {
                                    item_id: node_ref.item_id.clone(),
                                    member: member.clone(),
                                });
                            };
                            Ok(ent_node_list[start_node_i + terminal_i])
                        }
                        NewItem::Terminal { ent_node } | NewItem::Node { ent_node } => {
                            if let Some(member) = &node_ref.member {
                                return Err(ImportErrorKind::ItemMemberNotFound {
                                    item_id: node_ref.item_id.clone(),
                                    member: member.clone(),
                                });
                            }
                            Ok(ent_node)
                        }
                    }
                });
                // FIXME: Use `<[_; _]>::try_map` (rust#79711) when
                // it's stabilized
                let [t0, t1] = terminals.map(|n| {
                    n.map_err(|error| LoadError::Import {
                        error,
                        path: new_circuit_file_path(),
                    })
                });
                let terminals = [t0?, t1?].into();

                // Spawn the device entity
                let mut ent_item = commands.spawn((
                    circuit::WireDeviceBundle {
                        device: circuit::DeviceBundle {
                            device: circuit::Device { terminals },
                        },
                        wire: circuit::WireDevice,
                    },
                    OriginalKdl(Arc::clone(kp_arena)),
                    OriginalAstItem(ast::Item::Wire(item.clone())),
                ));
                if let Some(id) = &item.id {
                    ent_item.insert(meta::Id(id.clone()));
                }
                circuit.devices.insert(ent_item.id());
            } // for item

            circuit.nodes.extend(ent_node_list.drain(..));

            // Assign `Circuit`
            new_circuit_info.circuit.set(circuit).ok().unwrap();
        } // for new_circuit

        // Create `Symbol`s (but don't insert them yet)
        // -------------------------------------------------------------------
        for (&ent_circuit, new_circuit_info) in new_circuit_info_map.iter() {
            let new_circuit_file_path =
                || self.files.index_typed(new_circuit_info.file_i).path.clone();

            // Find `ast::Symbol` defining the symbol for this circuit
            // TODO: Deny duplicates
            let Some(ast_symbol) = new_circuit_info
                .ast_circuit
                .items
                .iter()
                .find_map(match_ok!(, ast::Item::Symbol(_0)))
            else {
                tracing::debug!(
                    ?ent_circuit,
                    "This circuit does not have a `Symbol` item; not creating a `Symbol` component"
                );
                continue;
            };

            let mut symbol = layout::Symbol::default();

            let mut terminals: Vec<_> = (0..new_circuit_info.meta.terminal_names.len())
                .map(|_| None)
                .collect();

            let terminal_id_to_i = |id: &str| {
                new_circuit_info
                    .meta
                    .terminal_names
                    .iter()
                    .position(|x| *x == id)
                    .ok_or_else(|| LoadError::Import {
                        error: ImportErrorKind::TerminalNotFound {
                            terminal_id: id.to_owned(),
                        },
                        path: new_circuit_file_path(),
                    })
            };

            for item in &ast_symbol.items {
                match item {
                    ast::SymbolItem::Terminal(item) => {
                        let terminal_i = terminal_id_to_i(&item.id)?;

                        // TODO: Deny multiple symbols on a single terminal
                        terminals[terminal_i] = Some(layout::SymbolTerminal {
                            position: [item.x, item.y].into(),
                        });
                    }
                    ast::SymbolItem::Path(item) => {
                        let fill = match &item.fill {
                            None => layout::FillColor::default(),
                            Some(ast::FillColor::Const(color)) => layout::FillColor::Const(*color),
                            Some(ast::FillColor::Potential(id)) => {
                                let terminal_i = terminal_id_to_i(id)?;
                                layout::FillColor::Potential(terminal_i)
                            }
                        };

                        let stroke = match &item.stroke {
                            None => layout::StrokeColor::default(),
                            Some(ast::StrokeColor::Const(color)) => {
                                layout::StrokeColor::Const(*color)
                            }
                            Some(ast::StrokeColor::Potential([id0, id1])) => {
                                let terminal_i0 = terminal_id_to_i(id0)?;
                                let terminal_i1 = terminal_id_to_i(id1)?;
                                layout::StrokeColor::Potential([terminal_i0, terminal_i1])
                            }
                        };

                        symbol.paths.push(layout::Path {
                            fill,
                            stroke,
                            stroke_width: item.width.unwrap_or(1),
                            segments: item.d.clone(),
                        });
                    }
                }
            }

            symbol.terminals = terminals
                .into_iter()
                .enumerate()
                .map(|(terminal_i, lay_terminal)| {
                    lay_terminal.ok_or_else(|| LoadError::Import {
                        error: ImportErrorKind::SymbolTerminalMissing {
                            terminal_id: new_circuit_info.meta.terminal_names[terminal_i].clone(),
                        },
                        path: new_circuit_file_path(),
                    })
                })
                .try_collect()?;

            new_circuit_info.symbol.set(symbol).ok().unwrap();
        }

        // Finish building the circuit entities
        // -------------------------------------------------------------------
        for (ent_circuit, new_circuit_info) in new_circuit_info_map {
            let ent_file = ent_files.index_typed(new_circuit_info.file_i).0;
            let mut ent_circuit = commands.entity(ent_circuit);
            ent_circuit.insert((
                new_circuit_info
                    .circuit
                    .into_inner()
                    .expect("`Circuit` is not set"),
                new_circuit_info.meta,
                FileMember(ent_file),
                meta::Id(new_circuit_info.ast_circuit.id.clone()),
                OriginalKdl(Arc::clone(&new_circuit_info.file_tl.kp_arena)),
                OriginalAstCircuit(new_circuit_info.ast_circuit.clone()),
            ));
            if let Some(symbol) = new_circuit_info.symbol.into_inner() {
                ent_circuit.insert(symbol);
            }
        }

        // Create workspace entities
        // -------------------------------------------------------------------
        for (file_i, file) in self.files.iter_typed() {
            let file_i: FileI = file_i;
            let PreloadFileTy::ToLoad(file_tl) = &file.ty else {
                continue;
            };
            let ent_file = ent_files.index_typed(file_i).0;
            let _span = tracing::debug_span!("Creating a workspace entity", ?file_i).entered();

            let Some(ast_workspace) = &file_tl.ast_file.workspace else {
                tracing::debug!(
                    "Workspace node not found, \
                    skipping workspace entity creation"
                );
                continue;
            };

            // Resolve root circiuit
            let file_circuits = ent_circuits.index_typed(file_i);
            let Some(&ent_root_circuit) = file_circuits.0.get(&ast_workspace.root_circuit_id)
            else {
                return Err(LoadError::Import {
                    error: ImportErrorKind::LocalCircuitNotFound {
                        circuit_id: ast_workspace.root_circuit_id.clone(),
                    },
                    path: file.path.clone(),
                });
            };

            let ent_workspace = commands
                .spawn((
                    sim_control::SimControl {
                        ent_root_circuit,
                        time_step: ast_workspace
                            .time_step
                            .unwrap_or(sim_control::SimControl::DEFAULT_TIME_STEP),
                        time_scale: ast_workspace
                            .time_scale
                            .unwrap_or(sim_control::SimControl::DEFAULT_TIME_SCALE),
                    },
                    render::RenderOpts {
                        potential_color_range: ast_workspace
                            .potential_color_range
                            .unwrap_or(render::RenderOpts::DEFAULT_POTENTIAL_COLOR_RANGE),
                        current_speed: ast_workspace
                            .current_speed
                            .unwrap_or(render::RenderOpts::DEFAULT_CURRENT_SPEED),
                    },
                    FileMember(ent_file),
                ))
                .id();
            new_entities.push(ent_workspace);

            tracing::debug!(?ent_workspace, "Created a workspace entity");
        }

        // Cancel the failure behavior because the load operation was successful
        ScopeGuard::into_inner(guard);

        let loaded = Loaded {
            file_ent_map: self
                .file_map
                .into_iter()
                .map(|(path, file_i)| (path, ent_files.index_typed(file_i).0))
                .collect(),
        };

        Ok(loaded)
    }
}

/// Used by [`LoadCx::load`]
#[derive(bevy_ecs::query::QueryData)]
pub struct ExistingCircuitData {
    ent_circuit: Entity,
    file_member: &'static FileMember,
    id: &'static meta::Id,
    meta: Option<&'static meta::CircuitMeta>,
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use crate::kio::test_utils::load;
    use anyhow::{Context, Result};
    use std::path::PathBuf;

    fn validate_loaded_world(world: &World) -> Result<()> {
        for ent in world.iter_entities() {
            (|| -> Result<()> {
                // Loaded `File` requires `OriginalAstFile`
                anyhow::ensure!(
                    !ent.contains::<File<PathBuf>>() || ent.contains::<OriginalAstFile>()
                );

                // Loaded `Circuit` requires `OriginalAstCircuit`
                anyhow::ensure!(
                    !ent.contains::<circuit::Circuit>() || ent.contains::<OriginalAstCircuit>()
                );

                // A loaded circuit item entity requires `OriginalAstItem`
                // unless it also contains `TerminalNode`
                anyhow::ensure!(
                    !(ent.contains::<circuit::Device>() || ent.contains::<circuit::Node>())
                        || ent.contains::<meta::TerminalNode>()
                        || ent.contains::<OriginalAstItem>()
                );

                // `OriginalAst*` requires `OriginalKdl`
                anyhow::ensure!(
                    !ent.contains::<OriginalAstFile>() || ent.contains::<OriginalKdl>()
                );
                anyhow::ensure!(
                    !ent.contains::<OriginalAstCircuit>() || ent.contains::<OriginalKdl>()
                );
                anyhow::ensure!(
                    !ent.contains::<OriginalAstItem>() || ent.contains::<OriginalKdl>()
                );

                Ok(())
            })()
            .with_context(|| format!("entity {:?}", ent.id()))?;
        }

        Ok(())
    }

    #[test]
    fn load_std() {
        crate::tests::init();

        let path = PathBuf::from("std.simac");

        let mut world = World::default();
        let loaded = load(&mut world, path.clone());
        tracing::info!(?world);

        let ent_file = loaded.file_ent_map[&path];

        for ent in world.iter_entities() {
            if ent.contains::<circuit::Circuit>() {
                let file_member: &FileMember = ent
                    .get()
                    .expect("newly loaded `Circuit` does not have `FileMember`");
                assert_eq!(file_member.0, ent_file);
            }
        }

        validate_loaded_world(&world).expect("validate_loaded_world");
    }

    #[test]
    fn load_example_passive_resistor() {
        crate::tests::init();

        let path = PathBuf::from("../../examples/passive/resistor.simac");

        let mut world = World::default();
        let loaded = load(&mut world, path.clone());
        tracing::info!(?world);

        let ent_file = loaded.file_ent_map[&path];

        // `resistor.simac` provides exactly one circuit
        let num_example_circuits = world
            .iter_entities()
            .filter(|ent| {
                ent.contains::<circuit::Circuit>() && {
                    let file_member: &FileMember = ent
                        .get()
                        .expect("newly loaded `Circuit` does not have `FileMember`");
                    file_member.0 == ent_file
                }
            })
            .count();
        assert_eq!(num_example_circuits, 1);

        validate_loaded_world(&world).expect("validate_loaded_world");
    }
}
