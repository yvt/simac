//! Flattening circuits and evaluating expressions
use bevy_ecs::{entity::EntityHashMap, prelude::*};
use fxhash::FxHashMap;
use macro_rules_attribute::derive;
use simac_elements::ElemTy;
use simac_operator::StashableComponent;
use slotmap::{HopSlotMap, new_key_type};
use std::sync::Arc;

#[cfg(doc)]
use crate::circuit;

mod setup;
pub use setup::{SetupCircuitParams, setup_circuit};
mod sync;
pub use sync::{SyncCircuitParams, sync_circuit};

#[cfg(test)]
mod tests;

/// Flattening parameters.
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct Flatten {
    /// The root circuit parameters.
    pub root_circuit_params: Vec<f64>,
}

// Flattening output
// -------------------------------------------------------------------------

/// A flattening result.
#[derive(Component, Debug)]
pub struct Flattened {
    pub root_circuit: Entity,
    pub root_circuit_def_i: FlatCircuitDefI,
    pub root_circuit_i: FlatCircuitI,
    pub circuit_def_map: EntityHashMap<FlatCircuitDefI>,
    pub circuit_defs: HopSlotMap<FlatCircuitDefI, FlatCircuitDef>,
    pub device_def_map: EntityHashMap<FlatDeviceDefI>,
    pub device_defs: HopSlotMap<FlatDeviceDefI, FlatDeviceDef>,
    pub node_def_map: EntityHashMap<FlatNodeDefI>,
    pub node_defs: HopSlotMap<FlatNodeDefI, FlatNodeDef>,
    pub circuits: HopSlotMap<FlatCircuitI, FlatCircuit>,
    pub devices: HopSlotMap<FlatDeviceI, FlatDevice>,
    pub nodes: HopSlotMap<FlatNodeI, FlatNode>,
    /// `circuit_map[(circuits[i].parent,
    /// circuits[i].subcircuit_device_def_i)] == i`.
    /// Excludes the root circuit (whose `parent == None`).
    pub circuit_map: FxHashMap<(FlatCircuitI, FlatDeviceDefI), FlatCircuitI>,
    /// `node_map[(nodes[i].parent, nodes[i].node_def_i)] == i`
    pub node_map: FxHashMap<(FlatCircuitI, FlatNodeDefI), FlatNodeI>,
}

new_key_type! {
    /// [`Flattened::circuit_defs`]
    pub struct FlatCircuitDefI;
}

/// An unflattened circuit in [`Flattened`].
#[derive(Debug)]
pub struct FlatCircuitDef {
    entity: Entity,
    child_devices: intrusive_list::Head<FlatDeviceDefI>,
    child_nodes: intrusive_list::Head<FlatNodeDefI>,
    instances: intrusive_list::Head<FlatCircuitI>,
}

new_key_type! {
    /// [`Flattened::device_defs`]
    pub struct FlatDeviceDefI;
}

/// An unflattened device in [`Flattened`].
#[derive(Debug)]
pub struct FlatDeviceDef {
    /// The original device entity. Immutable.
    pub entity: Entity,
    /// The parent [`FlatCircuitDef`] in which this device is defined. Immutable.
    parent: FlatCircuitDefI,
    /// The previous/next `Self`s in [`FlatCircuitDef::child_devices`].
    sibling_devices: Option<intrusive_list::Link<FlatDeviceDefI>>,
    /// [`circuit::Device::terminals`]. Immutable.
    pub ent_terminals: Arc<[Entity]>,
    /// Device type-specific data. The variant is immutable, but some variant
    /// fields are not.
    pub ty: FlatDeviceDefTy,
}

#[derive(Debug)]
pub enum FlatDeviceDefTy {
    Subcircuit {
        instances: intrusive_list::Head<FlatCircuitI>,
        /// [`circuit::SubcircuitDevice::circuit`]. Immutable.
        circuit: Entity,
    },
    Terminal {
        instances: intrusive_list::Head<FlatDeviceI>,
        /// [`circuit::TerminalDevice::terminal_i`]. Immutable.
        terminal_i: usize,
    },
    Element {
        instances: intrusive_list::Head<FlatDeviceI>,
        /// [`circuit::ElementDevice::elem_ty`]. Immutable.
        elem_ty: ElemTy,
    },
    Wire {
        // TODO: Remove this, it's equivalent to `ElemTy::Wire`
        instances: intrusive_list::Head<FlatDeviceI>,
    },
}

new_key_type! {
    /// [`Flattened::node_defs`]
    pub struct FlatNodeDefI;
}

/// An unflattened node in [`Flattened`].
#[derive(Debug)]
pub struct FlatNodeDef {
    pub entity: Entity,
    parent: FlatCircuitDefI,
    /// [`circuit::GroundNode`]. Immutable.
    pub is_ground: bool,
    /// The previous/next `Self`s in [`FlatCircuitDef::child_nodes`].
    sibling_nodes: Option<intrusive_list::Link<FlatNodeDefI>>,
    instances: intrusive_list::Head<FlatNodeI>,
}

new_key_type! {
    /// [`Flattened::circuits`]
    pub struct FlatCircuitI;
}

/// A flattened circuit instance in [`Flattened`].
///
/// It's created for every valid instance of a subcircuit device.
#[derive(Debug)]
pub struct FlatCircuit {
    /// The parent circuit if it's not the root circuit.
    parent: Option<FlatCircuitI>,
    /// The previous/next `Self`s in [`Self::child_circuits`].
    pub sibling_circuits: Option<intrusive_list::Link<FlatCircuitI>>,
    /// All child devices.
    ///
    /// `[tag:flat_child_device_order]` Terminal devices should be sorted before
    /// everything else.
    pub child_devices: intrusive_list::Head<FlatDeviceI>,
    pub child_circuits: intrusive_list::Head<FlatCircuitI>,
    pub child_nodes: intrusive_list::Head<FlatNodeI>,
    circuit_def_i: FlatCircuitDefI,
    /// The previous/next `Self`s in [`FlatCircuitDef::instances`].
    sibling_circuit_instances: Option<intrusive_list::Link<FlatCircuitI>>,
    /// The subcircuit device `self` is an instance of. `None` if it's the
    /// root circuit ([`Flattened::root_circuit`]).
    pub subcircuit_device_def_i: Option<FlatDeviceDefI>,
    /// The previous/next `Self`s in [`FlatDeviceDefTy::Subcircuit::instances`]
    /// if [`Self::subcircuit_device_def_i`] is `Some(_)`.
    sibling_subcircuit_instances: Option<intrusive_list::Link<FlatCircuitI>>,
    /// The evaluated circuit parameters.
    pub params: Vec<f64>,
}

new_key_type! {
    /// [`Flattened::devices`]
    pub struct FlatDeviceI;
}

/// A flattened device (circuit element) in [`Flattened`].
#[derive(Debug)]
pub struct FlatDevice {
    parent: FlatCircuitI,
    /// The previous/next `Self`s in [`FlatCircuit::child_devices`].
    pub sibling_devices: Option<intrusive_list::Link<FlatDeviceI>>,
    pub device_def_i: FlatDeviceDefI,
    /// The previous/next `Self`s in [`FlatDeviceDefTy::Terminal::instances`],
    /// etc.
    sibling_instances: Option<intrusive_list::Link<FlatDeviceI>>,
    pub elem_ty: Option<ElemTy>,
    /// The terminal nodes.
    ///
    /// If this device is [`FlatDeviceDefTy::Terminal`] or
    /// [`FlatDeviceDefTy::Wire`], this is missing the third node representing
    /// the branch current.
    pub terminal_nodes: Vec<FlatNodeI>,
    pub elem_params: Vec<f64>,
}

new_key_type! {
    /// [`Flattened::nodes`]
    pub struct FlatNodeI;
}

/// A flattened node in [`Flattened`].
#[derive(Debug)]
pub struct FlatNode {
    parent: FlatCircuitI,
    /// The previous/next `Self`s in [`FlatCircuit::child_nodes`].
    pub sibling_nodes: Option<intrusive_list::Link<FlatNodeI>>,
    pub node_def_i: FlatNodeDefI,
    /// The previous/next `Self`s in [`FlatNodeDef::instances`].
    sibling_instances: Option<intrusive_list::Link<FlatNodeI>>,
}
