//! Systems to recalculate element parameters
use macro_rules_attribute::derive;
use std::ops::ControlFlow;

use super::*;
use crate::{circuit, expr};

#[derive(bevy_ecs::system::SystemParam)]
pub struct SetupCircuitParams<'w, 's> {
    circuit_setup_programs: Query<'w, 's, &'static expr::SetupProgram>,
    program_data: expr::ProgramDataParam<'w, 's>,
    circuits: Query<'w, 's, &'static circuit::Circuit>,
}

/// Evaluate a circuit setup programs ([`expr::SetupProgram`]) to
/// recalculate element parameters.
///
/// Returns a flag indicating whether `flattened` has been modified.
#[tracing::instrument(level = "debug", skip_all)]
pub fn setup_circuit(
    flatten: &Flatten,
    flattened: &mut Flattened,
    params: &SetupCircuitParams<'_, '_>,
) -> bool {
    let changed;

    // Set the root circuit's parameters
    {
        let flat_circuit = &mut flattened.circuits[flattened.root_circuit_i];
        let flat_circuit_def = &mut flattened.circuit_defs[flattened.root_circuit_def_i];
        let circuit = params
            .circuits
            .get(flat_circuit_def.entity)
            .expect("`Circuit` not found");

        let old_param_vals = flat_circuit.params.to_owned();

        flat_circuit.params.clone_from(&flatten.root_circuit_params);
        flat_circuit.params.resize(circuit.num_params, 0.0);

        changed = flat_circuit.params != old_param_vals;
    }

    // Recalculate all (sub-) circuits' device parameters
    let mut circuit_stack = vec![flattened.root_circuit_i];
    let mut cx = SetupCx {
        changed,
        params,
        flattened,
        evaluator: expr::Evaluator::default(),
        param_vals: Vec::with_capacity(16),
    };

    while let Some(circuit_i) = circuit_stack.pop() {
        cx.process_circuit(circuit_i);

        // Traverse children
        // TODO: Use dirty flags to skip unmodified subtrees
        let circuit = &cx.flattened.circuits[circuit_i];
        for (child_circuit_i, _) in circuit
            .child_circuits
            .accessor(&cx.flattened.circuits, |x| &x.sibling_circuits)
            .iter()
        {
            circuit_stack.push(child_circuit_i);
        }
    }

    cx.changed
}

struct SetupCx<'a> {
    changed: bool,
    flattened: &'a mut Flattened,
    params: &'a SetupCircuitParams<'a, 'a>,
    evaluator: expr::Evaluator,
    param_vals: Vec<f64>,
}

impl SetupCx<'_> {
    #[tracing::instrument(level = "debug", skip_all, fields(?circuit_i))]
    fn process_circuit(&mut self, circuit_i: FlatCircuitI) {
        let circuit = &mut self.flattened.circuits[circuit_i];
        let circuit_def = &self.flattened.circuit_defs[circuit.circuit_def_i];
        let Ok(circuit_setup_program) = self.params.circuit_setup_programs.get(circuit_def.entity)
        else {
            tracing::warn!(?circuit_i, "The circuit does not have `SetupProgram`");
            return;
        };

        // Evaluate this circuit's device parameters
        // TODO: Skip this if `circuit_setup_program` is unmodified
        let evaluation = self.evaluator.eval_setup(
            circuit_setup_program,
            &self.params.program_data,
            &circuit.params,
        );

        circuit
            .child_devices
            .accessor_mut(&mut self.flattened.devices, |x| &mut x.sibling_devices)
            .for_each_mut(|_device_i, flat_device| {
                let device_def = &self.flattened.device_defs[flat_device.device_def_i];

                if !matches!(device_def.ty, FlatDeviceDefTy::Element { .. }) {
                    return ControlFlow::Continue::<()>(());
                }

                // Save the old values
                self.param_vals.clone_from(&flat_device.elem_params);

                // Copy the new parameter values
                evaluation
                    .copy_element_device_params_to(device_def.entity, &mut flat_device.elem_params);

                // Did they change?
                if self.param_vals != flat_device.elem_params {
                    self.changed = true;
                }

                ControlFlow::Continue::<()>(())
            });

        { circuit.child_circuits }
            .accessor_mut(&mut self.flattened.circuits, |x| &mut x.sibling_circuits)
            .for_each_mut(|_circuit_i, flat_circuit| {
                let device_def =
                    &self.flattened.device_defs[flat_circuit.subcircuit_device_def_i.unwrap()];
                let child_circuit_def = &self.flattened.circuit_defs[flat_circuit.circuit_def_i];
                let circuit = self.params.circuits.get(child_circuit_def.entity).unwrap();

                // Save the old values
                self.param_vals.clone_from(&flat_circuit.params);

                // Copy the new parameter values
                flat_circuit.params.resize(circuit.num_params, 0.0);
                evaluation
                    .copy_subcircuit_device_params_to(device_def.entity, &mut flat_circuit.params);

                // Did they change?
                if self.param_vals != flat_circuit.params {
                    self.changed = true;
                }

                ControlFlow::Continue::<()>(())
            });
    }
}
