use super::*;
use anyhow::{Context, Result};
use fxhash::FxHashSet;
use itertools::{Itertools, chain};
use macro_rules_attribute::derive;

use crate::{circuit, expr};

mod cmds;

#[derive(Debug, Resource)]
struct TestRes {
    flatten: Flatten,
    flattened: Flattened,
}

/// (Bevy system)
fn sys_update(
    mut test_res: ResMut<'_, TestRes>,
    flat_sync_params: SyncCircuitParams<'_, '_>,
    flat_setup_params: SetupCircuitParams<'_, '_>,
) {
    let TestRes { flatten, flattened } = &mut *test_res;
    sync_circuit(flatten, flattened, &flat_sync_params);
    setup_circuit(flatten, flattened, &flat_setup_params);
}

/// Check the consistency of the specified [`Flattened`].
fn validate_flat(world: Option<&World>, _flatten: &Flatten, flattened: &Flattened) -> Result<()> {
    // It should contain the def and instance of the root circuit
    anyhow::ensure!(
        flattened
            .circuit_defs
            .contains_key(flattened.root_circuit_def_i)
    );
    anyhow::ensure!(flattened.circuits.contains_key(flattened.root_circuit_i));

    // `Flattened` fields
    anyhow::ensure!(flattened.circuit_defs.len() == flattened.circuit_def_map.len());
    anyhow::ensure!(flattened.device_defs.len() == flattened.device_def_map.len());
    anyhow::ensure!(flattened.node_defs.len() == flattened.node_def_map.len());

    anyhow::ensure!(flattened.circuits.len() == flattened.circuit_map.len() + 1);
    anyhow::ensure!(flattened.nodes.len() == flattened.node_map.len());

    // `FlatCircuitDef`
    let mut linked_device_def = FxHashSet::default();
    let mut linked_node_def = FxHashSet::default();

    for (&entity, &circuit_def_i) in flattened.circuit_def_map.iter() {
        let circuit_def = &flattened.circuit_defs[circuit_def_i];
        (|| -> Result<()> {
            anyhow::ensure!(circuit_def.entity == entity);

            let child_devices = circuit_def
                .child_devices
                .accessor(&flattened.device_defs, |x| &x.sibling_devices);
            child_devices
                .validate()
                .context("`child_devices` linked list is corrupted")?;

            for (device_def_i, device_def) in child_devices.iter() {
                anyhow::ensure!(
                    device_def.parent == circuit_def_i,
                    "{device_def_i:?}.parent = {:?} ({circuit_def_i:?} expected)",
                    device_def.parent,
                );
                linked_device_def.insert(device_def_i);
            }

            let child_nodes = circuit_def
                .child_nodes
                .accessor(&flattened.node_defs, |x| &x.sibling_nodes);
            child_nodes
                .validate()
                .context("`child_nodes` linked list is corrupted")?;

            for (node_def_i, node_def) in child_nodes.iter() {
                anyhow::ensure!(
                    node_def.parent == circuit_def_i,
                    "{node_def_i:?}.parent = {:?} ({circuit_def_i:?} expected)",
                    node_def.parent,
                );
                linked_node_def.insert(node_def_i);
            }

            let child_device_def_i_set: Vec<_> = child_devices
                .iter()
                .map(|x| x.0)
                .sorted_unstable()
                .collect();
            let child_node_def_i_set: Vec<_> =
                child_nodes.iter().map(|x| x.0).sorted_unstable().collect();

            let instances = circuit_def
                .instances
                .accessor(&flattened.circuits, |x| &x.sibling_circuit_instances);
            instances
                .validate()
                .context("`instances` linked list is corrupted")?;

            for (circuit_i, circuit) in instances.iter() {
                anyhow::ensure!(
                    circuit.circuit_def_i == circuit_def_i,
                    "{circuit_i:?}.circuit_def_i = {:?} ({circuit_def_i:?} expected)",
                    circuit.circuit_def_i,
                );

                // Every circuit instance must contain the instance of every
                // child from the circuit definition
                let got_child_device_def_i_set: Vec<_> = chain(
                    circuit
                        .child_devices
                        .accessor(&flattened.devices, |x| &x.sibling_devices)
                        .iter()
                        .map(|x| x.1.device_def_i),
                    circuit
                        .child_circuits
                        .accessor(&flattened.circuits, |x| &x.sibling_circuits)
                        .iter()
                        .map(|x| x.1.subcircuit_device_def_i.unwrap()),
                )
                .sorted_unstable()
                .collect();
                anyhow::ensure!(
                    child_device_def_i_set == got_child_device_def_i_set,
                    "{circuit_i:?}'s child device set is inconsistent with {circuit_def_i:?}.\n\n\
                    {circuit_def_i:?}: {child_device_def_i_set:?}\n\n\
                    {circuit_i:?}: {got_child_device_def_i_set:?}"
                );

                let got_child_node_def_i_set: Vec<_> = circuit
                    .child_nodes
                    .accessor(&flattened.nodes, |x| &x.sibling_nodes)
                    .iter()
                    .map(|x| x.1.node_def_i)
                    .sorted_unstable()
                    .collect();
                anyhow::ensure!(
                    child_node_def_i_set == got_child_node_def_i_set,
                    "{circuit_i:?}'s child node set is inconsistent with {circuit_def_i:?}.\n\n\
                    {circuit_def_i:?}: {child_node_def_i_set:?}\n\n\
                    {circuit_i:?}: {got_child_node_def_i_set:?}"
                );
            } // for (circuit_i, circuit)

            Ok(())
        })()
        .with_context(|| format!("{circuit_def_i:?} {:?}", circuit_def.entity))?;
    }

    // `FlatDeviceDef`
    for (&entity, &device_def_i) in flattened.device_def_map.iter() {
        let device_def = &flattened.device_defs[device_def_i];
        (|| -> Result<()> {
            anyhow::ensure!(device_def.entity == entity);
            anyhow::ensure!(
                linked_device_def.contains(&device_def_i),
                "not referenced by any `FlatCircuitDef`s"
            );

            // `device_def.parent` is easier to validate
            // from `FlatCircuitDefI` side

            for (i, entity) in device_def.ent_terminals.iter().enumerate() {
                let node_def_i = *flattened.node_def_map.get(entity).with_context(|| {
                    format!(
                        "terminal {i} refers to node entity {entity:?}, which \
                        has not been mapped to `FlatNodeDef`"
                    )
                })?;
                let node_def = &flattened.node_defs[node_def_i];
                anyhow::ensure!(
                    node_def.parent == device_def.parent,
                    "terminal {i} connects to {node_def_i:?}, \
                    which is a member of {:?}, not {:?}",
                    node_def.parent,
                    device_def.parent,
                );
            }

            match &device_def.ty {
                FlatDeviceDefTy::Subcircuit { instances, circuit } => {
                    let instances = instances
                        .accessor(&flattened.circuits, |x| &x.sibling_subcircuit_instances);
                    instances
                        .validate()
                        .context("`instances` linked list is corrupted")?;

                    for (circuit_i, circuit) in instances.iter() {
                        anyhow::ensure!(
                            circuit.subcircuit_device_def_i == Some(device_def_i),
                            "{circuit_i:?}.subcircuit_device_def_i = {:?} \
                            ({device_def_i:?} expected)",
                            circuit.subcircuit_device_def_i,
                        );
                    }

                    anyhow::ensure!(
                        flattened.circuit_def_map.contains_key(circuit),
                        "circuit_def_map.keys ∌ {circuit:?}"
                    );
                }
                FlatDeviceDefTy::Terminal {
                    instances: _,
                    terminal_i: _,
                } => {
                    anyhow::ensure!(device_def.ent_terminals.len() == 1);
                }
                FlatDeviceDefTy::Element {
                    instances: _,
                    elem_ty,
                } => {
                    anyhow::ensure!(device_def.ent_terminals.len() == elem_ty.num_nodes());
                }
                FlatDeviceDefTy::Wire { instances: _ } => {
                    anyhow::ensure!(device_def.ent_terminals.len() == 2);
                }
            }

            match &device_def.ty {
                FlatDeviceDefTy::Subcircuit { .. } => {}
                FlatDeviceDefTy::Terminal { instances, .. }
                | FlatDeviceDefTy::Element { instances, .. }
                | FlatDeviceDefTy::Wire { instances } => {
                    let instances =
                        instances.accessor(&flattened.devices, |x| &x.sibling_instances);
                    instances
                        .validate()
                        .context("`instances` linked list is corrupted")?;

                    for (device_i, device) in instances.iter() {
                        anyhow::ensure!(
                            device.device_def_i == device_def_i,
                            "{device_i:?}.device_def_i = {:?} ({device_def_i:?} expected)",
                            device.device_def_i,
                        );
                    }
                }
            }

            Ok(())
        })()
        .with_context(|| format!("{device_def_i:?} {:?}", device_def.entity))?;
    }

    // `FlatNodeDef`
    for (&entity, &node_def_i) in flattened.node_def_map.iter() {
        let node_def = &flattened.node_defs[node_def_i];
        (|| -> Result<()> {
            anyhow::ensure!(node_def.entity == entity);
            anyhow::ensure!(
                linked_node_def.contains(&node_def_i),
                "not referenced by any `FlatCircuitDef`s"
            );

            // `node_def.parent` is easier to check from `FlatCircuitDefI` side

            if let Some(world) = world {
                anyhow::ensure!(
                    node_def.is_ground
                        == world.get::<circuit::GroundNode>(node_def.entity).is_some()
                );
            }

            let instances = node_def
                .instances
                .accessor(&flattened.nodes, |x| &x.sibling_instances);
            instances
                .validate()
                .context("`instances` linked list is corrupted")?;

            for (node_i, node) in instances.iter() {
                anyhow::ensure!(
                    node.node_def_i == node_def_i,
                    "{node_i:?}.node_def_i = {:?} ({node_def_i:?} expected)",
                    node.node_def_i,
                );
            }

            Ok(())
        })()
        .with_context(|| format!("{node_def_i:?} {:?}", node_def.entity))?;
    }

    // `FlatCircuit`
    let mut linked_devices = FxHashSet::default();
    let mut linked_nodes = FxHashSet::default();
    let mut linked_circuits = FxHashSet::default();

    for (circuit_i, circuit) in flattened.circuits.iter() {
        let circuit_i: FlatCircuitI = circuit_i;
        (|| -> Result<()> {
            anyhow::ensure!((circuit_i == flattened.root_circuit_i) == circuit.parent.is_none());

            if let (Some(parent), Some(device_def_i)) =
                (circuit.parent, circuit.subcircuit_device_def_i)
            {
                anyhow::ensure!(
                    flattened.circuit_map.get(&(parent, device_def_i)).copied() == Some(circuit_i)
                );
            }

            let circuit_def_i = circuit.circuit_def_i;
            let circuit_def = flattened.circuit_defs.get(circuit_def_i).with_context(|| {
                format!("`circuit.circuit_def_i` ({circuit_def_i:?}) is invalid")
            })?;

            let child_devices = circuit
                .child_devices
                .accessor(&flattened.devices, |x| &x.sibling_devices);
            child_devices
                .validate()
                .context("`child_devices` linked list is corrupted")?;

            for (device_i, device) in child_devices.iter() {
                anyhow::ensure!(
                    device.parent == circuit_i,
                    "{device_i:?}.parent = {:?} ({circuit_i:?} expected)",
                    device.parent,
                );
                linked_devices.insert(device_i);
            }

            // Check [ref:flat_child_device_order]
            let map = |d: &FlatDevice| {
                let device_def = &flattened.device_defs[d.device_def_i];
                matches!(device_def.ty, FlatDeviceDefTy::Terminal { .. })
            };
            anyhow::ensure!(
                child_devices
                    .iter()
                    .tuple_windows()
                    .all(|((_, d0), (_, d1))| map(d0) >= map(d1)),
                "`child_devices` is not partitioned into terminals and non-terinals",
            );

            let child_circuits = circuit
                .child_circuits
                .accessor(&flattened.circuits, |x| &x.sibling_circuits);
            child_circuits
                .validate()
                .context("`child_circuits` linked list is corrupted")?;

            for (child_circuit_i, child_circuit) in child_circuits.iter() {
                anyhow::ensure!(
                    child_circuit.parent == Some(circuit_i),
                    "{child_circuit_i:?}.parent = {:?} ({circuit_i:?} expected)",
                    Some(child_circuit.parent),
                );
                linked_circuits.insert(child_circuit_i);
            }

            let child_nodes = circuit
                .child_nodes
                .accessor(&flattened.nodes, |x| &x.sibling_nodes);
            child_nodes
                .validate()
                .context("`child_nodes` linked list is corrupted")?;

            for (node_i, node) in child_nodes.iter() {
                anyhow::ensure!(
                    node.parent == circuit_i,
                    "{node_i:?}.parent = {:?} ({circuit_i:?} expected)",
                    node.parent,
                );
                linked_nodes.insert(node_i);
            }

            // `circuit.circuit_def_i` is checked elsewhere

            anyhow::ensure!(
                (circuit_i == flattened.root_circuit_i)
                    == circuit.subcircuit_device_def_i.is_none()
            );

            if let Some(world) = world {
                let comp_circuit: &circuit::Circuit =
                    world.get(circuit_def.entity).with_context(|| {
                        format!("`Circuit` component not found in {:?}", circuit_def.entity)
                    })?;

                anyhow::ensure!(circuit.params.len() == comp_circuit.num_params);
            }

            Ok(())
        })()
        .with_context(|| format!("{circuit_i:?} (from {:?})", circuit.circuit_def_i))?;
    } // for (circuit_i, circuit)

    for (circuit_i, _) in flattened.circuits.iter() {
        anyhow::ensure!(
            circuit_i == flattened.root_circuit_i || linked_circuits.contains(&circuit_i),
            "{circuit_i:?}: not referenced by any `FlatCircuit`s"
        );
    }

    // `FlatDevice`
    for (device_i, device) in flattened.devices.iter() {
        let device_i: FlatDeviceI = device_i;
        (|| -> Result<()> {
            anyhow::ensure!(
                linked_devices.contains(&device_i),
                "not referenced by any `FlatCircuit`s"
            );

            let device_def_i = device.device_def_i;
            let device_def = flattened
                .device_defs
                .get(device_def_i)
                .with_context(|| format!("`device.device_def_i` ({device_def_i:?}) is invalid"))?;

            if let FlatDeviceDefTy::Element { elem_ty, .. } = device_def.ty {
                anyhow::ensure!(device.elem_params.len() == elem_ty.num_params());
            } else {
                anyhow::ensure!(device.elem_params.len() == 0);
            }

            Ok(())
        })()
        .with_context(|| format!("{device_i:?} (from {:?})", device.device_def_i))?;
    } // for (device_i, device)

    // `FlatNode`
    for (&(circuit_i, node_def_i), &node_i) in flattened.node_map.iter() {
        let node = &flattened.nodes[node_i];
        (|| -> Result<()> {
            anyhow::ensure!((circuit_i, node_def_i) == (node.parent, node.node_def_i));
            anyhow::ensure!(
                linked_nodes.contains(&node_i),
                "not referenced by any `FlatCircuit`s"
            );
            Ok(())
        })()
        .with_context(|| format!("{node_i:?} (from {:?})", node.node_def_i))?;
    } // for (node_i, node)

    Ok(())
}
