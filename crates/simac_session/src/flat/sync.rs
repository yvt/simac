//! Systems to flatten circuits
use bevy_ecs::prelude::*;
use bevy_utils::hashbrown::hash_map;
use macro_rules_attribute::derive;
use std::ops::ControlFlow;

use super::*;
use crate::circuit;

#[derive(bevy_ecs::system::SystemParam)]
pub struct SyncCircuitParams<'w, 's> {
    circuits: Query<'w, 's, &'static circuit::Circuit>,
    devices: Query<'w, 's, &'static circuit::Device>,
    subcircuit_devices: Query<'w, 's, &'static circuit::SubcircuitDevice>,
    terminal_devices: Query<'w, 's, &'static circuit::TerminalDevice>,
    element_devices: Query<'w, 's, &'static circuit::ElementDevice>,
    wire_devices: Query<'w, 's, &'static circuit::WireDevice>,
    nodes: Query<'w, 's, &'static circuit::Node>,
    ground_nodes: Query<'w, 's, &'static circuit::GroundNode>,
}

/// Synchronize the device and node instances in [`Flattened`].
///
/// Returns a flag indicating whether `flattened` has been modified.
#[tracing::instrument(level = "debug", skip_all)]
pub fn sync_circuit(
    _flatten: &Flatten,
    flattened: &mut Flattened,
    params @ SyncCircuitParams {
        circuits,
        devices,
        subcircuit_devices,
        terminal_devices,
        element_devices,
        wire_devices,
        nodes,
        ground_nodes,
    }: &SyncCircuitParams<'_, '_>,
) -> bool {
    let mut changed = false;

    if !circuits.contains(flattened.root_circuit) {
        tracing::warn!(
            root_circuit = ?flattened.root_circuit,
            "This flattening setting refers to a non-existent circuit entity",
        );
        return changed;
    }

    // Remove `Flat*Def` that are no longer valid
    // -----------------------------------------------------------------------
    let mut circuit_defs_to_remove: Vec<FlatCircuitDefI> = vec![];
    let mut mark_circuit_def_for_removal = |i: FlatCircuitDefI| circuit_defs_to_remove.push(i);

    for &circuit_def_i in flattened.circuit_def_map.values() {
        let circuit_def = &flattened.circuit_defs[circuit_def_i];
        let _e = tracing::debug_span!("Checking circuit for invalid items",
                    ?circuit_def_i, entity = ?circuit_def.entity)
        .entered();

        if !circuits.contains(circuit_def.entity) {
            tracing::debug!("The entity no longer exists, removing `FlatCircuitDef`");
            mark_circuit_def_for_removal(circuit_def_i);
        }
    }

    for circuit_def_i in circuit_defs_to_remove.drain(..) {
        flattened.remove_circuit_def(circuit_def_i);
        changed = true;
    }

    let mut device_defs_to_remove: Vec<FlatDeviceDefI> = vec![];
    let mut mark_device_def_for_removal = |i: FlatDeviceDefI| device_defs_to_remove.push(i);

    for &device_def_i in flattened.device_def_map.values() {
        let device_def = &mut flattened.device_defs[device_def_i];
        let _e = tracing::trace_span!("Checking device for invalid items",
                    ?device_def_i, entity = ?device_def.entity)
        .entered();

        let Ok(comp_device) = devices.get(device_def.entity) else {
            tracing::debug!("The entity no longer exists, removing `FlatDeviceDef`");
            mark_device_def_for_removal(device_def_i);
            continue;
        };

        // Make sure its parent hasn't changed. Remove it if this is no longer
        // the case.
        let parent = &flattened.circuit_defs[device_def.parent];
        let comp_parent = circuits.get(parent.entity).unwrap();
        if !comp_parent.devices.contains(&device_def.entity) {
            tracing::debug!(
                ent_parent = ?parent.entity,
                "The entity is no longer a child of the previous parent,
                removing `FlatDeviceDef`"
            );
            mark_device_def_for_removal(device_def_i);
            continue;
        }

        if !Arc::ptr_eq(&device_def.ent_terminals, &comp_device.terminals)
            && device_def.ent_terminals != comp_device.terminals
        {
            // FIXME: We could avoid subcircuit reinstantiation by simply
            // reinstantiating terminal devices
            tracing::debug!("`Device::terminals` changed, removing `FlatDeviceDef`");
            mark_device_def_for_removal(device_def_i);
            continue;
        }

        // We must remove `ElemId`s before removing `NodeId`s they reference
        // because of `WorldCore::remove_node`'s requirements
        if !comp_device
            .terminals
            .iter()
            .all(|&e| comp_parent.nodes.contains(&e))
        {
            tracing::debug!(
                "`Device::terminals` now refers to invalid nodes, \
                removing `FlatDeviceDef`"
            );
            mark_device_def_for_removal(device_def_i);
            continue;
        }

        match &device_def.ty {
            FlatDeviceDefTy::Subcircuit { circuit, .. } => {
                let Ok(comp_subcircuit_device) = subcircuit_devices.get(device_def.entity) else {
                    panic!(
                        "entity {:?}: device type has changed, which is not supported",
                        device_def.entity
                    );
                };

                if *circuit != comp_subcircuit_device.circuit {
                    tracing::debug!(
                        "`SubcircuitDevice::circuit` changed, \
                        removing `FlatDeviceDef`"
                    );
                    mark_device_def_for_removal(device_def_i);
                    continue;
                }

                if !flattened.circuit_def_map.contains_key(circuit) {
                    tracing::debug!(
                        "`SubcircuitDevice::circuit` now refers to an invalid circuit, \
                        removing `FlatDeviceDef`"
                    );
                    mark_device_def_for_removal(device_def_i);
                    continue;
                }
            }
            FlatDeviceDefTy::Terminal { terminal_i, .. } => {
                let Ok(comp_terminal_device) = terminal_devices.get(device_def.entity) else {
                    panic!(
                        "entity {:?}: device type has changed, which is not supported",
                        device_def.entity
                    );
                };

                if *terminal_i != comp_terminal_device.terminal_i {
                    tracing::debug!(
                        "`TerminalDevice::terminal_i` changed, \
                        removing `FlatDeviceDef`"
                    );
                    mark_device_def_for_removal(device_def_i);
                    continue;
                }
            }
            FlatDeviceDefTy::Element { elem_ty, .. } => {
                let Ok(comp_element_device) = element_devices.get(device_def.entity) else {
                    panic!(
                        "entity {:?}: device type has changed, which is not supported",
                        device_def.entity
                    );
                };

                if *elem_ty != comp_element_device.elem_ty {
                    tracing::debug!(
                        "`ElementDevice::elem_ty` changed, \
                        removing `FlatDeviceDef`"
                    );
                    mark_device_def_for_removal(device_def_i);
                    continue;
                }
            }
            FlatDeviceDefTy::Wire { .. } => {
                if !wire_devices.contains(device_def.entity) {
                    panic!(
                        "entity {:?}: device type has changed, which is not supported",
                        device_def.entity
                    );
                }
            }
        }
    }

    for device_def_i in device_defs_to_remove {
        flattened.remove_device_def(device_def_i);
        changed = true;
    }

    let mut node_defs_to_remove: Vec<FlatNodeDefI> = vec![];
    let mut mark_node_def_for_removal = |i: FlatNodeDefI| node_defs_to_remove.push(i);

    for &node_def_i in flattened.node_def_map.values() {
        let node_def = &flattened.node_defs[node_def_i];
        let _e = tracing::trace_span!("Checking node for invalid items",
                    ?node_def_i, entity = ?node_def.entity)
        .entered();

        if !nodes.contains(node_def.entity) {
            tracing::debug!("The entity no longer exists, removing `FlatNodeDef`");
            mark_node_def_for_removal(node_def_i);
            continue;
        }

        // Make sure its parent hasn't changed. Remove it if this is no longer
        // the case.
        {
            let parent = &flattened.circuit_defs[node_def.parent];
            let comp_parent = circuits.get(parent.entity).unwrap();
            if !comp_parent.nodes.contains(&node_def.entity) {
                tracing::debug!(
                    ent_parent = ?parent.entity,
                    "The entity is no longer a child of the previous parent,
                    removing `FlatNodeDef`"
                );
                mark_node_def_for_removal(node_def_i);
                continue;
            }
        }

        // Changing `is_ground` means changing `NodeId`,
        // which is a bit too tedious to carry out
        if node_def.is_ground != ground_nodes.contains(node_def.entity) {
            panic!(
                "entity {:?}: `GroundNode` presence changed, which is not supported",
                node_def.entity,
            );
        }
    }

    for node_def_i in node_defs_to_remove {
        flattened.remove_node_def(node_def_i);
        changed = true;
    }

    // Remove `FlatCircuitDef`s with no instances
    // -----------------------------------------------------------------------
    circuit_defs_to_remove.extend(
        flattened
            .circuit_def_map
            .values()
            .copied()
            .filter(|&circuit_def_i| flattened.circuit_defs[circuit_def_i].instances.is_empty()),
    );

    for circuit_def_i in circuit_defs_to_remove {
        tracing::debug!(
            ?circuit_def_i,
            "The `FlatCircuitDef` has no instances, removing it"
        );
        flattened.remove_circuit_def(circuit_def_i);
    }

    // Add new `Flat*Def`s
    // -----------------------------------------------------------------------
    let mut cx = InsertCx {
        all_circuit_def_i_list: flattened.circuit_def_map.values().copied().collect(),
        flattened,
        params,
        changed,
    };
    cx.process_all();

    cx.changed
}

impl Flattened {
    pub fn new(root_circuit: Entity) -> Self {
        let mut this = Flattened {
            root_circuit,
            // set later
            root_circuit_def_i: FlatCircuitDefI::default(),
            // set later
            root_circuit_i: FlatCircuitI::default(),
            circuit_def_map: Default::default(),
            circuit_defs: Default::default(),
            device_def_map: Default::default(),
            device_defs: Default::default(),
            node_def_map: Default::default(),
            node_defs: Default::default(),
            circuits: Default::default(),
            devices: Default::default(),
            nodes: Default::default(),
            circuit_map: Default::default(),
            node_map: Default::default(),
        };

        // Create a `FlatCircuit[Def]` for the root circuit
        this.root_circuit_def_i = this.circuit_defs.insert_with_key(|circuit_def_i| {
            let mut circuit_def = None;
            this.root_circuit_i = this.circuits.insert_with_key(|circuit_i| {
                circuit_def = Some(FlatCircuitDef {
                    entity: root_circuit,
                    child_devices: intrusive_list::Head::default(),
                    child_nodes: intrusive_list::Head::default(),
                    instances: intrusive_list::Head {
                        first: Some(circuit_i),
                    },
                });

                FlatCircuit {
                    parent: None,
                    sibling_circuits: None,
                    child_devices: intrusive_list::Head::default(),
                    child_circuits: intrusive_list::Head::default(),
                    child_nodes: intrusive_list::Head::default(),
                    circuit_def_i,
                    sibling_circuit_instances: Some(intrusive_list::Link {
                        prev: circuit_i,
                        next: circuit_i,
                    }),
                    subcircuit_device_def_i: None,
                    sibling_subcircuit_instances: None,
                    // set by `crate::flat::setup`
                    params: Vec::new(),
                }
            });
            circuit_def.unwrap()
        });

        this.circuit_def_map
            .insert(root_circuit, this.root_circuit_def_i);

        this
    }

    /// Remove a [`FlatCircuitDef`], its children, and all instances.
    fn remove_circuit_def(&mut self, circuit_def_i: FlatCircuitDefI) {
        macro_rules! circuit_def {
            () => {
                self.circuit_defs[circuit_def_i]
            };
        }

        assert_ne!(
            circuit_def!().entity,
            self.root_circuit,
            "invalidating or removing the root circuit def is not allowed"
        );

        // Remove all instances (`FlatCircuit`)
        while let Some(node_def_i) = circuit_def!().instances.first {
            self.remove_circuit(node_def_i);
        }

        // Remove all children
        while let Some(device_def_i) = circuit_def!().child_devices.first {
            self.remove_device_def(device_def_i);
        }

        while let Some(node_def_i) = circuit_def!().child_nodes.first {
            self.remove_node_def(node_def_i);
        }

        // Finally, remove the `FlatCircuitDef`
        let circuit_def = self.circuit_defs.remove(circuit_def_i).unwrap();
        self.circuit_def_map.remove(&circuit_def.entity);
    }

    /// Remove a [`FlatDeviceDef`] and all instances.
    fn remove_device_def(&mut self, device_def_i: FlatDeviceDefI) {
        macro_rules! device_def {
            () => {
                self.device_defs[device_def_i]
            };
        }

        // Remove all instances
        loop {
            match device_def!().ty {
                FlatDeviceDefTy::Subcircuit { instances, .. } => {
                    let Some(circuit_i) = instances.first else {
                        break;
                    };
                    self.remove_circuit(circuit_i);
                }
                FlatDeviceDefTy::Terminal { instances, .. }
                | FlatDeviceDefTy::Element { instances, .. }
                | FlatDeviceDefTy::Wire { instances } => {
                    let Some(device_i) = instances.first else {
                        break;
                    };
                    self.remove_device(device_i);
                }
            }
        }

        // Remove `device_def_i` from its parent
        let parent_circuit_def = &mut self.circuit_defs[device_def!().parent];
        parent_circuit_def
            .child_devices
            .accessor_mut(&mut self.device_defs, |x| &mut x.sibling_devices)
            .remove(device_def_i);

        // Finally, remove the `FlatDeviceDef`
        let device_def = self.device_defs.remove(device_def_i).unwrap();
        self.device_def_map.remove(&device_def.entity);
    }

    fn remove_node_def(&mut self, node_def_i: FlatNodeDefI) {
        macro_rules! node_def {
            () => {
                self.node_defs[node_def_i]
            };
        }

        // Remove all instances
        while let Some(node_i) = node_def!().instances.first {
            self.remove_node(node_i);
        }

        // Remove `node_def_i` from its parent
        let parent_circuit_def = &mut self.circuit_defs[node_def!().parent];
        parent_circuit_def
            .child_nodes
            .accessor_mut(&mut self.node_defs, |x| &mut x.sibling_nodes)
            .remove(node_def_i);

        // Finally, remove the `FlatNodeDef`
        let node_def = self.node_defs.remove(node_def_i).unwrap();
        self.node_def_map.remove(&node_def.entity);
    }

    /// Remove a [`FlatCircuit`].
    fn remove_circuit(&mut self, circuit_i: FlatCircuitI) {
        macro_rules! circuit {
            () => {
                self.circuits[circuit_i]
            };
        }

        assert_ne!(
            circuit_i, self.root_circuit_i,
            "de-instantiating the root circuit is not allowed"
        );
        let FlatCircuit {
            parent: Some(parent_circuit_i),
            subcircuit_device_def_i: Some(parent_device_def_i),
            ..
        } = circuit!()
        else {
            panic!("non-root circuit should have a parent")
        };

        // Remove `circuit_i` from its parent
        //
        // This is a bit complicated due to `intrusive_list` not supporting
        // the pattern where a `Head` is contained by the same collection the
        // `Head` points to.
        {
            let circuits = &mut self.circuits;
            let mut parent_child_circuits = circuits[parent_circuit_i].child_circuits;
            parent_child_circuits
                .accessor_mut(circuits, |x| &mut x.sibling_circuits)
                .remove(circuit_i);
            circuits[parent_circuit_i].child_circuits = parent_child_circuits;
        }

        // Remove children
        while let Some(device_i) = circuit!().child_devices.first {
            self.remove_device(device_i);
        }

        while let Some(circuit_i) = circuit!().child_circuits.first {
            self.remove_circuit(circuit_i);
        }

        while let Some(node_i) = circuit!().child_nodes.first {
            self.remove_node(node_i);
        }

        // Remove `circuit_i` from `FlatCircuitDef::instances`
        let circuit_def = &mut self.circuit_defs[circuit!().circuit_def_i];
        circuit_def
            .instances
            .accessor_mut(&mut self.circuits, |x| &mut x.sibling_circuit_instances)
            .remove(circuit_i);

        // Remove `circuit_i` from `FlatDeviceDefTy::Subcircuit::instances`
        let parent_device_def = &mut self.device_defs[parent_device_def_i];
        let FlatDeviceDefTy::Subcircuit { instances, .. } = &mut parent_device_def.ty else {
            unreachable!()
        };
        instances
            .accessor_mut(&mut self.circuits, |x| &mut x.sibling_subcircuit_instances)
            .remove(circuit_i);

        // Remove `FlatCircuit`
        self.circuits.remove(circuit_i).unwrap();
        self.circuit_map
            .remove(&(parent_circuit_i, parent_device_def_i));
    }

    /// Remove a [`FlatDevice`].
    fn remove_device(&mut self, device_i: FlatDeviceI) {
        let FlatDevice {
            parent,
            device_def_i,
            ..
        } = self.devices[device_i];

        // Remove `device_i` from its parent
        let parent_circuit = &mut self.circuits[parent];
        parent_circuit
            .child_devices
            .accessor_mut(&mut self.devices, |x| &mut x.sibling_devices)
            .remove(device_i);

        // Remove `device_i` from `FlatDeviceDefTy::*::instances`.
        let device_def = &mut self.device_defs[device_def_i];
        match &mut device_def.ty {
            FlatDeviceDefTy::Subcircuit { .. } => unreachable!(),
            FlatDeviceDefTy::Terminal { instances, .. }
            | FlatDeviceDefTy::Element { instances, .. }
            | FlatDeviceDefTy::Wire { instances } => {
                instances
                    .accessor_mut(&mut self.devices, |x| &mut x.sibling_instances)
                    .remove(device_i);
            }
        }

        // Remove `FlatDevice`
        self.devices.remove(device_i).unwrap();
    }

    /// Remove a [`FlatNode`].
    fn remove_node(&mut self, node_i: FlatNodeI) {
        let FlatNode {
            parent, node_def_i, ..
        } = self.nodes[node_i];

        // Remove `device_i` from its parent
        let parent_circuit = &mut self.circuits[parent];
        parent_circuit
            .child_nodes
            .accessor_mut(&mut self.nodes, |x| &mut x.sibling_nodes)
            .remove(node_i);

        // Remove `device_i` from `FlatDeviceDefTy::*::instances`.
        let node_def = &mut self.node_defs[node_def_i];
        node_def
            .instances
            .accessor_mut(&mut self.nodes, |x| &mut x.sibling_instances)
            .remove(node_i);

        // Remove `FlatNode`
        let node = self.nodes.remove(node_i).unwrap();

        self.node_map.remove(&(node.parent, node.node_def_i));
    }
}

struct InsertCx<'a> {
    flattened: &'a mut Flattened,
    params: &'a SyncCircuitParams<'a, 'a>,
    /// [`FlatCircuitDefI`]s to process. New [`FlatCircuitDef`]s will be added
    /// as we go.
    all_circuit_def_i_list: Vec<FlatCircuitDefI>,
    /// Set if [`Self::flattened`] is mutated.
    changed: bool,
}

impl InsertCx<'_> {
    fn process_all(&mut self) {
        let mut i = 0;
        while let Some(&circuit_def_i) = self.all_circuit_def_i_list.get(i) {
            self.process_circuit_def(circuit_def_i);
            i += 1;
        }
    }

    #[tracing::instrument(level = "debug", skip(self))]
    fn process_circuit_def(&mut self, circuit_def_i: FlatCircuitDefI) {
        macro_rules! circuit_def {
            () => {
                self.flattened.circuit_defs[circuit_def_i]
            };
        }
        let mut circuit_def = &mut circuit_def!();
        let comp_circuit = self.params.circuits.get(circuit_def.entity).unwrap();

        let last_old_child_node_def_i = circuit_def
            .child_nodes
            .accessor(&self.flattened.node_defs, |x| &x.sibling_nodes)
            .back();

        let last_old_child_device_def_i = circuit_def
            .child_devices
            .accessor(&self.flattened.device_defs, |x| &x.sibling_devices)
            .back();

        tracing::trace!("Checking the circuit for new items");

        // Create and add child `FlatNodeDef`s
        //
        // We need to do this before adding new `FlatDeviceDef`s so that all
        // referenced `FlatNodeDef`s are present.
        for &ent_node in comp_circuit.nodes.iter() {
            let hash_map::Entry::Vacant(entry) = self.flattened.node_def_map.entry(ent_node) else {
                continue;
            };

            let _e = tracing::debug_span!("Creating `FlatNodeDef`", ?ent_node).entered();

            if !self.params.nodes.contains(ent_node) {
                tracing::trace!("The node entity doesn't exist");
                continue;
            }

            let is_ground = self.params.ground_nodes.contains(ent_node);

            let node_def_i = self.flattened.node_defs.insert(FlatNodeDef {
                entity: ent_node,
                parent: circuit_def_i,
                is_ground,
                sibling_nodes: None,
                instances: intrusive_list::Head::default(),
            });
            entry.insert(node_def_i);
            tracing::debug!(?node_def_i, "Created `FlatNodeDef`");

            circuit_def
                .child_nodes
                .accessor_mut(&mut self.flattened.node_defs, |x| &mut x.sibling_nodes)
                .push_back(node_def_i);
        }

        // Create and add child `FlatDeviceDef`s
        for &ent_device in comp_circuit.devices.iter() {
            let hash_map::Entry::Vacant(entry) = self.flattened.device_def_map.entry(ent_device)
            else {
                continue;
            };

            let _e = tracing::debug_span!("Creating `FlatDeviceDef`", ?ent_device).entered();

            let Ok(comp_device) = self.params.devices.get(ent_device) else {
                tracing::trace!("The device entity doesn't exist");
                continue;
            };

            if !comp_device
                .terminals
                .iter()
                .all(|e| comp_circuit.nodes.contains(e))
            {
                tracing::trace!("A device terminal connects to a non-existent node");
                continue;
            }

            let ty = if let Ok(comp_subcircuit_device) =
                self.params.subcircuit_devices.get(ent_device)
            {
                if !self
                    .params
                    .circuits
                    .contains(comp_subcircuit_device.circuit)
                {
                    tracing::trace!(
                        "The subcircuit device refers to a \
                        non-existent circuit, not creating `FlatDeviceDef`"
                    );
                    continue;
                }

                FlatDeviceDefTy::Subcircuit {
                    instances: intrusive_list::Head::default(),
                    circuit: comp_subcircuit_device.circuit,
                }
            } else if let Ok(comp_terminal_device) = self.params.terminal_devices.get(ent_device) {
                // [tag:flat_terminal_dev_num_terminals]
                if comp_device.terminals.len() != 1 {
                    tracing::trace!(
                        len_got = comp_device.terminals.len(),
                        len_expected = 1,
                        "The terminal device contains an invalid number of \
                        terminals, not creating `FlatDeviceDef`"
                    );
                    continue;
                }

                // [tag:flat_terminal_dev_not_root]
                if circuit_def_i == self.flattened.root_circuit_def_i {
                    tracing::trace!(
                        "The terminal device is defined inside the root circuit, \
                        not creating `FlatDeviceDef`"
                    );
                    continue;
                }

                FlatDeviceDefTy::Terminal {
                    instances: intrusive_list::Head::default(),
                    terminal_i: comp_terminal_device.terminal_i,
                }
            } else if let Ok(comp_element_device) = self.params.element_devices.get(ent_device) {
                // [tag:flat_element_dev_num_terminals]
                let len_expected = comp_element_device.elem_ty.num_nodes();
                if comp_device.terminals.len() != len_expected {
                    tracing::trace!(
                        len_got = comp_device.terminals.len(),
                        len_expected,
                        elem_ty = ?comp_element_device.elem_ty,
                        "The element device contains an invalid number of \
                        terminals, not creating `FlatDeviceDef`"
                    );
                    continue;
                }

                FlatDeviceDefTy::Element {
                    instances: intrusive_list::Head::default(),
                    elem_ty: comp_element_device.elem_ty,
                }
            } else if self.params.wire_devices.contains(ent_device) {
                // [tag:flat_wire_dev_num_terminals]
                if comp_device.terminals.len() != 2 {
                    tracing::trace!(
                        len_got = comp_device.terminals.len(),
                        len_expected = 2,
                        "The wire device contains an invalid number of \
                        terminals, not creating `FlatDeviceDef`"
                    );
                    continue;
                }

                FlatDeviceDefTy::Wire {
                    instances: intrusive_list::Head::default(),
                }
            } else {
                panic!("entity {ent_device:?} is a device of unknown type");
            };

            let device_def_i = self.flattened.device_defs.insert(FlatDeviceDef {
                entity: ent_device,
                parent: circuit_def_i,
                sibling_devices: None,
                ent_terminals: Arc::clone(&comp_device.terminals),
                ty,
            });
            entry.insert(device_def_i);
            tracing::debug!(?device_def_i, "Created `FlatDeviceDef`");

            circuit_def
                .child_devices
                .accessor_mut(&mut self.flattened.device_defs, |x| &mut x.sibling_devices)
                .push_back(device_def_i);
        } // for ent_device

        // Create an instance (`FlatNode`) for every
        // `(new_node_def_i, existing_circuit_i)`.
        //
        // We must do this before creating `FlatDevice`s to uphold
        // [ref:flat_device_terminal_node].
        //
        // Iterate over `circuit_def.child_nodes[last_old_child_node_def_i + 1..]`
        // in reverse
        if let Some(mut node_def_i) = circuit_def
            .child_nodes
            .accessor(&self.flattened.node_defs, |x| &x.sibling_nodes)
            .back()
        {
            while Some(node_def_i) != last_old_child_node_def_i {
                // Iterate over `circuit_def.instances`
                circuit_def
                    .instances
                    .accessor_mut(&mut self.flattened.circuits, |x| {
                        &mut x.sibling_circuit_instances
                    })
                    .for_each_mut(|circuit_i, circuit| {
                        Self::instantiate_node(
                            node_def_i,
                            &mut self.flattened.node_defs[node_def_i],
                            circuit_i,
                            circuit,
                            &mut self.flattened.nodes,
                            &mut self.flattened.node_map,
                        );
                        ControlFlow::Continue::<(), ()>(())
                    });

                self.changed = true;

                if Some(node_def_i) == circuit_def.child_nodes.first {
                    break;
                } else {
                    node_def_i = (self.flattened.node_defs[node_def_i])
                        .sibling_nodes
                        .unwrap()
                        .prev;
                }
            }
        }

        // Create an instance (`FlatDevice` or `FlatCircuit`) for every
        // `(new_device_def_i, existing_circuit_i)`.
        //
        // Iterate over `circuit_def.child_devices[last_old_child_device_def_i +
        // 1..]` in reverse
        if let Some(mut device_def_i) = circuit_def
            .child_devices
            .accessor(&self.flattened.device_defs, |x| &x.sibling_devices)
            .back()
        {
            while Some(device_def_i) != last_old_child_device_def_i {
                // Iterate over `circuit_def.instances`. Note that new items
                // may be pushed during iteration.
                if let Some(first_circuit_i) = circuit_def.instances.first {
                    let mut circuit_i = first_circuit_i;
                    loop {
                        self.instantiate_device(device_def_i, circuit_i);

                        let next_circuit_i = self.flattened.circuits[circuit_i]
                            .sibling_circuit_instances
                            .unwrap()
                            .next;
                        if next_circuit_i == first_circuit_i {
                            break;
                        } else {
                            circuit_i = next_circuit_i;
                        }
                    }
                }

                circuit_def = &mut circuit_def!();
                if Some(device_def_i) == circuit_def.child_devices.first {
                    break;
                } else {
                    device_def_i = (self.flattened.device_defs[device_def_i])
                        .sibling_devices
                        .unwrap()
                        .prev;
                }
            }
        }
    } // fn process_circuit_def

    /// Create an instance ([`FlatNode`]) of the specified [`FlatNodeDef`].
    #[tracing::instrument(level = "trace", skip_all, fields(?node_def_i, ?parent_circuit_i))]
    fn instantiate_node(
        node_def_i: FlatNodeDefI,
        node_def: &mut FlatNodeDef,
        parent_circuit_i: FlatCircuitI,
        parent_circuit: &mut FlatCircuit,
        nodes: &mut HopSlotMap<FlatNodeI, FlatNode>,
        node_map: &mut FxHashMap<(FlatCircuitI, FlatNodeDefI), FlatNodeI>,
    ) {
        // Insert a `FlatNode`
        let node_i = nodes.insert(FlatNode {
            parent: parent_circuit_i,
            sibling_nodes: None, // set later
            node_def_i,
            sibling_instances: None, // set later
        });
        node_map.insert((parent_circuit_i, node_def_i), node_i);

        // Link it to `FlatNodeDef::instances`
        node_def
            .instances
            .accessor_mut(nodes, |x| &mut x.sibling_instances)
            .push_back(node_i);

        // Link it to `FlatCircuit::child_nodes`
        parent_circuit
            .child_nodes
            .accessor_mut(nodes, |x| &mut x.sibling_nodes)
            .push_back(node_i);

        tracing::debug!(
            ?node_def_i,
            ?parent_circuit_i,
            ?node_i,
            "Instantiated a node"
        );
    }

    /// Create an instance ([`FlatDevice`] or [`FlatCircuit`]) of the
    /// specified [`FlatDeviceDef`].
    #[tracing::instrument(level = "debug", skip(self))]
    fn instantiate_device(&mut self, device_def_i: FlatDeviceDefI, parent_circuit_i: FlatCircuitI) {
        let device_def = &mut self.flattened.device_defs[device_def_i];

        // All nodes attached to the device's terminals should have been
        // instantiated as `FlatNode` at this point. [tag:flat_device_terminal_node]
        #[cfg(debug_assertions)]
        for (i, &ent_node) in device_def.ent_terminals.iter().enumerate() {
            let Some(&node_def_i) = self.flattened.node_def_map.get(&ent_node) else {
                panic!(
                    "{device_def_i:?} in {parent_circuit_i:?}: \
                    terminal {i} is connected to node {ent_node:?}, which doesn't \
                    have a `FlatNodeDef`"
                )
            };
            if !self
                .flattened
                .node_map
                .contains_key(&(parent_circuit_i, node_def_i))
            {
                panic!(
                    "{device_def_i:?} in {parent_circuit_i:?}: \
                    terminal {i} is connected to node {ent_node:?} ({node_def_i:?}), \
                    which doesn't have a `FlatNode`"
                )
            }
        }

        if let FlatDeviceDefTy::Subcircuit { circuit, .. } = device_def.ty {
            let circuit_def_i = self.ensure_circuit_def(circuit);
            return self.instantiate_circuit(device_def_i, circuit_def_i, parent_circuit_i);
        }

        let is_terminal = matches!(device_def.ty, FlatDeviceDefTy::Terminal { .. });

        // Convert `FlatDeviceDef::ent_terminals` to `[NodeId]`.
        let mut terminal_nodes = Vec::with_capacity(2);
        terminal_nodes.extend(device_def.ent_terminals.iter().map(|&ent_node| {
            // This will not panic thanks to [ref:flat_device_terminal_node]
            let node_def_i = self.flattened.node_def_map[&ent_node];
            self.flattened.node_map[&(parent_circuit_i, node_def_i)]
        }));

        let mut elem_params = Vec::new();

        // Determine the circuit element type and node mapping (`terminal_nodes`).
        // `elem_ty = None` if we'll instantiate no elements.
        let elem_ty = match device_def.ty {
            // `Subcircuit` is ruled out above
            FlatDeviceDefTy::Subcircuit { .. } => unreachable!(),
            FlatDeviceDefTy::Terminal { terminal_i, .. } => 'a: {
                // [ref:flat_terminal_dev_num_terminals]
                assert_eq!(terminal_nodes.len(), 1);

                // Find the circuit containing the outer subcircuit device
                let parent_circuit = &self.flattened.circuits[parent_circuit_i];
                let grandparent_circuit_i = parent_circuit
                    .parent
                    // [ref:flat_terminal_dev_not_root]
                    .expect("terminal device def should not appear in root circuit");

                // Find the outer subcircuit device (the one that instantiated
                // `parent_circuit_i`)
                let parent_device_def_i = parent_circuit
                    .subcircuit_device_def_i
                    // [ref:flat_terminal_dev_not_root]
                    .expect("terminal device def should not appear in root circuit");
                let parent_device_def = &self.flattened.device_defs[parent_device_def_i];

                // Find the outer subcircuit device's terminal `terminal_i`
                let Some(&ent_outer_terminal) = parent_device_def.ent_terminals.get(terminal_i)
                else {
                    tracing::debug!(
                        ?parent_device_def_i,
                        terminal_i,
                        "The parent subcircuit device doesn't have this terminal; \
                        generating no wires for the terminal device"
                    );
                    break 'a None;
                };

                // This will not panic thanks to [ref:flat_device_terminal_node]
                let outer_node_def_i = self.flattened.node_def_map[&ent_outer_terminal];
                let outer_node_i =
                    self.flattened.node_map[&(grandparent_circuit_i, outer_node_def_i)];

                // Connect `node_ids[0]` to the outer subcircuit device's
                // terminal `terminal_i`
                terminal_nodes.push(outer_node_i);

                // `Wire` has two terminal nodes. It actually requires an extra
                // node, but we can omit that in `terminal_nodes`.
                assert_eq!(terminal_nodes.len(), 2);

                Some(ElemTy::Wire)
            }
            FlatDeviceDefTy::Element { elem_ty, .. } => {
                // [ref:flat_element_dev_num_terminals]
                assert_eq!(terminal_nodes.len(), elem_ty.num_nodes());

                // The element parameters are set by `crate::flat::setup`
                elem_params.extend((0..elem_ty.num_params()).map(|_| 0.0));

                Some(elem_ty)
            }
            FlatDeviceDefTy::Wire { .. } => {
                // [ref:flat_wire_dev_num_terminals]
                // `Wire` has two terminal nodes. It actually requires an extra
                // node, but we can omit that in `terminal_nodes`.
                assert_eq!(terminal_nodes.len(), 2);
                Some(ElemTy::Wire)
            }
        };

        tracing::trace!(
            ?terminal_nodes,
            "The terminals of the device we're about to instantiate"
        );

        // Insert a `FlatDevice`
        let device_i = self.flattened.devices.insert(FlatDevice {
            parent: parent_circuit_i,
            sibling_devices: None, // set later
            device_def_i,
            sibling_instances: None, // set later
            elem_ty,
            terminal_nodes,
            elem_params,
        });

        tracing::debug!(
            ?device_def_i,
            ?device_i,
            ?parent_circuit_i,
            ?elem_ty,
            "Instantiated a device"
        );

        self.changed = true;

        // Link it to the parent's `FlatCircuit::child_devices`, setting
        // `FlatDevice::sibling_devices`
        let parent_circuit = &mut self.flattened.circuits[parent_circuit_i];
        let mut child_devices = parent_circuit
            .child_devices
            .accessor_mut(&mut self.flattened.devices, |x| &mut x.sibling_devices);
        if is_terminal {
            // [ref:flat_child_device_order]
            child_devices.push_front(device_i);
        } else {
            child_devices.push_back(device_i);
        }

        // Link it to `FlatDeviceDef::instances`, setting
        // `FlatDevice::sibling_instances`
        let device_def = &mut self.flattened.device_defs[device_def_i];
        let instances = match &mut device_def.ty {
            // `Subcircuit` is ruled out above
            FlatDeviceDefTy::Subcircuit { .. } => unreachable!(),
            FlatDeviceDefTy::Terminal { instances, .. }
            | FlatDeviceDefTy::Element { instances, .. }
            | FlatDeviceDefTy::Wire { instances } => instances,
        };
        instances
            .accessor_mut(&mut self.flattened.devices, |x| &mut x.sibling_instances)
            .push_back(device_i);
    }

    /// An internal function of [`Self::instantiate_device`].
    /// Create an instance of the specified subcircuit [`FlatDeviceDef`].
    #[tracing::instrument(level = "debug", skip(self, device_def_i, parent_circuit_i))]
    fn instantiate_circuit(
        &mut self,
        device_def_i: FlatDeviceDefI,
        circuit_def_i: FlatCircuitDefI,
        parent_circuit_i: FlatCircuitI,
    ) {
        macro_rules! circuit_def {
            () => {
                self.flattened.circuit_defs[circuit_def_i]
            };
        }

        // Check for recursion
        {
            let mut current = Some(parent_circuit_i);
            while let Some(circuit_i) = current {
                let circuit = &self.flattened.circuits[circuit_i];
                if circuit.circuit_def_i == circuit_def_i {
                    todo!("handle recursion gracefully");
                }
                current = circuit.parent;
            }
        }

        // Insert a `FlatCircuit`
        let circuit_i = self.flattened.circuits.insert(FlatCircuit {
            parent: Some(parent_circuit_i),
            sibling_circuits: None, // set later
            child_devices: intrusive_list::Head::default(),
            child_circuits: intrusive_list::Head::default(),
            child_nodes: intrusive_list::Head::default(),
            circuit_def_i,
            sibling_circuit_instances: None, // set later
            subcircuit_device_def_i: Some(device_def_i),
            sibling_subcircuit_instances: None, // set later
            // set by `crate::flat::setup`
            params: Vec::new(),
        });
        self.flattened
            .circuit_map
            .insert((parent_circuit_i, device_def_i), circuit_i);

        macro_rules! circuit {
            () => {
                self.flattened.circuits[circuit_i]
            };
        }

        tracing::debug!(
            ?device_def_i,
            ?circuit_def_i,
            ?parent_circuit_i,
            ?circuit_i,
            "Instantiated a circuit, now instantiating the children"
        );

        self.changed = true;

        // Link it to the parent's `FlatCircuit::child_circuits`, setting
        // `FlatCircuit::sibling_circuits`
        //
        // This is a bit complicated due to `intrusive_list` not supporting
        // the pattern where a `Head` is contained by the same collection the
        // `Head` points to.
        {
            let circuits = &mut self.flattened.circuits;
            let mut parent_child_circuits = circuits[parent_circuit_i].child_circuits;
            parent_child_circuits
                .accessor_mut(circuits, |x| &mut x.sibling_circuits)
                .push_back(circuit_i);
            circuits[parent_circuit_i].child_circuits = parent_child_circuits;
        }

        // Link it to `FlatCircuitDef::instances`, setting
        // `FlatCircuit::sibling_circuit_instances`
        circuit_def!()
            .instances
            .accessor_mut(&mut self.flattened.circuits, |x| {
                &mut x.sibling_circuit_instances
            })
            .push_back(circuit_i);

        // Link it `FlatDeviceDefTy::Subcircuit::instances`, setting
        // `FlatCircuit::sibling_subcircuit_instances`
        let FlatDeviceDefTy::Subcircuit { instances, .. } =
            &mut self.flattened.device_defs[device_def_i].ty
        else {
            unreachable!("`device_def_i` does not refer to a subcircuit device")
        };
        instances
            .accessor_mut(&mut self.flattened.circuits, |x| {
                &mut x.sibling_subcircuit_instances
            })
            .push_back(circuit_i);

        // Create an instance (`FlatNode`) for every
        // `(existing_node_def_i, new_circuit_i)`.
        //
        // We must do this before creating `FlatDevice`s to uphold
        // [ref:flat_device_terminal_node].
        let circuit = &mut circuit!();
        circuit_def!()
            .child_nodes
            .accessor_mut(&mut self.flattened.node_defs, |x| &mut x.sibling_nodes)
            .for_each_mut(|node_def_i, node_def| {
                Self::instantiate_node(
                    node_def_i,
                    node_def,
                    circuit_i,
                    circuit,
                    &mut self.flattened.nodes,
                    &mut self.flattened.node_map,
                );
                ControlFlow::Continue::<(), ()>(())
            });

        // Create an instance (`FlatDevice`) for every
        // `(existing_device_def_i, new_circuit_i)`.
        //
        // Can't use the accessor because we need to relinquish mutable borrows
        // to call `instantiate_device`.
        if let Some(first_device_def_i) = circuit_def!().child_devices.first {
            let mut device_def_i = first_device_def_i;
            loop {
                self.instantiate_device(device_def_i, circuit_i);

                let next = (self.flattened.device_defs[device_def_i])
                    .sibling_devices
                    .unwrap()
                    .next;
                if next == first_device_def_i {
                    break;
                } else {
                    device_def_i = next;
                }
            }
        }
    }

    /// Ensure a [`FlatCircuitDef`] has been created for the specified entity.
    #[tracing::instrument(skip(self), level = "debug")]
    fn ensure_circuit_def(&mut self, ent_circuit: Entity) -> FlatCircuitDefI {
        let entry = match self.flattened.circuit_def_map.entry(ent_circuit) {
            hash_map::Entry::Vacant(entry) => entry,
            hash_map::Entry::Occupied(entry) => {
                tracing::trace!("It's already present, not creating another");
                return *entry.get();
            }
        };

        debug_assert!(self.params.circuits.contains(ent_circuit));

        // Create an empty `FlatCircuitDef`
        let circuit_def_i = self.flattened.circuit_defs.insert(FlatCircuitDef {
            entity: ent_circuit,
            child_devices: intrusive_list::Head::default(),
            child_nodes: intrusive_list::Head::default(),
            instances: intrusive_list::Head::default(),
        });
        entry.insert(circuit_def_i);
        self.all_circuit_def_i_list.push(circuit_def_i);
        tracing::debug!(?circuit_def_i, "Created a `FlatCircuitDef`");

        circuit_def_i
    }
}
