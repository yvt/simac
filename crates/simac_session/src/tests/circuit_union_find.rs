//! Reachability (connectedness) query for circuit items
use bevy_ecs::{entity::EntityHashMap, prelude::*};
use fxhash::FxHashSet;
use itertools::chain;
use std::mem::replace;

use crate::circuit;

use super::circuit_examples;

/// A disjoint-set data structure that can answer whether any two given items
/// are connected.
#[derive(Debug, Clone)]
pub struct CircuitUnionFind {
    ent_to_i: EntityHashMap<usize>,
    roots: Vec<usize>,
}

#[derive(bevy_ecs::system::SystemParam)]
pub struct CircuitUnionFindParams<'w, 's> {
    q_circuit: Query<'w, 's, &'static circuit::Circuit>,
    q_device: Query<'w, 's, &'static circuit::Device>,
}

/// Construct [`CircuitUnionFind`]
pub fn sys_union_find(params: CircuitUnionFindParams<'_, '_>) -> CircuitUnionFind {
    // List all circuit item entities
    let mut ents = Vec::new();
    let mut ent_to_i: EntityHashMap<usize> = <_>::default();
    for comp_circuit in params.q_circuit.iter() {
        for &ent in chain!(&comp_circuit.devices, &comp_circuit.nodes) {
            ent_to_i.entry(ent).or_insert_with(|| {
                let i = ents.len();
                ents.push(ent);
                i
            });
        }
    }

    let mut union_find = UnionFind::new(ents.len());

    for comp_circuit in params.q_circuit.iter() {
        for &ent_device in &comp_circuit.devices {
            let comp_device = params.q_device.get(ent_device).unwrap();
            let i1 = ent_to_i[&ent_device];
            for ent_node in &*comp_device.terminals {
                let i2 = ent_to_i[ent_node];
                union_find.union(i1, i2);
            }
        }
    }

    let roots = (0..ents.len()).map(|i| union_find.root_i_of(i)).collect();

    CircuitUnionFind { ent_to_i, roots }
}

impl CircuitUnionFind {
    /// The disjoint-set data structure root node of a given node.
    ///
    /// Two nodes are connected if and only if they have the same root node.
    pub fn root_i_of(&self, e: Entity) -> usize {
        self.roots[*self
            .ent_to_i
            .get(&e)
            .unwrap_or_else(|| panic!("unknown entity {e}"))]
    }

    /// Get a flag indicating whether there exists a path between circuit items
    /// `e0` and `e1`.
    #[expect(dead_code)]
    pub fn is_connected(&self, e0: Entity, e1: Entity) -> bool {
        self.root_i_of(e0) == self.root_i_of(e1)
    }
}

#[derive(Debug, Clone)]
struct UnionFind {
    nodes: Vec<Node>,
}

#[derive(Debug, Clone, Copy)]
struct Node {
    /// The parent node index. Root nodes point to themselves.
    parent: usize,
    /// The number of descendants plus one.
    size: usize,
}

impl UnionFind {
    fn new(len: usize) -> Self {
        Self {
            nodes: (0..len).map(|i| Node { parent: i, size: 1 }).collect(),
        }
    }

    /// Find the representative (root) node of the set that the specified node
    /// belongs to.
    ///
    /// This method requires a mutable receiver for path compression.
    fn root_i_of(&mut self, mut i: usize) -> usize {
        let nodes = &mut self.nodes[..];

        // Find the root node
        let mut root = i;
        while nodes[root].parent != root {
            root = nodes[root].parent;
        }

        // Path compression
        while nodes[i].parent != root {
            i = replace(&mut nodes[i].parent, root);
        }

        root
    }

    /// Merge two respective sets that the specified nodes belong to, if they
    /// are distinct.
    fn union(&mut self, i1: usize, i2: usize) {
        let mut r1 = self.root_i_of(i1);
        let mut r2 = self.root_i_of(i2);
        if r1 == r2 {
            return;
        }

        // Make sure `r1.size >= r2.size`
        let nodes = &mut self.nodes[..];
        if nodes[r1].size < nodes[r2].size {
            (r1, r2) = (r2, r1);
        }

        // Make `r1` the parent of `r2`
        nodes[r2].parent = r1;
        nodes[r1].size += nodes[r2].size;
    }
}

/// Check that [`CircuitUnionFind`] reports the correct result for an example
/// circuit that we know is connected.
#[test]
fn connected_example_circuits() {
    crate::tests::init();

    let path = circuit_examples::ExampleCircuitPath("../../examples/passive/resistor.simac".into());
    let crate::kio::test_utils::Workspace {
        mut world,
        ent_main_circuit,
        ..
    } = path.load();

    let uf = world.run_system_cached(sys_union_find).unwrap();

    // List all device entities in the circuit
    let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
    let ent_items: Vec<_> = comp_circuit.devices.iter().copied().collect();

    let roots: FxHashSet<usize> = ent_items.iter().map(|&e| uf.root_i_of(e)).collect();

    // All circuit items of `ent_main_circuit` must have one shared root
    assert_eq!(roots.len(), 1);
}
