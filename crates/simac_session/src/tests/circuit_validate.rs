//! Validation and pretty-printing of circuits
use anyhow::{Context, Result};
use bevy_ecs::prelude::*;
use macro_rules_attribute::derive;
use std::fmt;

use crate::{circuit, expr, meta};

#[derive(bevy_ecs::system::SystemParam)]
pub struct ValidateParams<'w, 's> {
    q_circuit: Query<'w, 's, (Entity, &'static circuit::Circuit)>,
    q_device: Query<'w, 's, &'static circuit::Device>,
    q_subcircuit_device: Query<'w, 's, &'static circuit::SubcircuitDevice>,
    q_terminal_device: Query<'w, 's, &'static circuit::TerminalDevice>,
    q_element_device: Query<'w, 's, &'static circuit::ElementDevice>,
    q_wire_device: Query<'w, 's, &'static circuit::WireDevice>,
    q_node: Query<'w, 's, &'static circuit::Node>,
    q_ground_node: Query<'w, 's, &'static circuit::GroundNode>,
    q_terminal_node: Query<'w, 's, &'static meta::TerminalNode>,
    q_id: Query<'w, 's, &'static meta::Id>,
    q_expr_subcircuit_device: Query<'w, 's, &'static expr::SubcircuitDevice>,
    q_expr_element_device: Query<'w, 's, &'static expr::ElementDevice>,
}

#[tracing::instrument(skip_all)]
pub fn sys_validate(params: ValidateParams<'_, '_>) -> Result<()> {
    for (ent_circuit, comp_circuit) in params.q_circuit.iter() {
        let _span = tracing::info_span!("Validating a circuit", %ent_circuit);
        validate_circuit(comp_circuit, &params).with_context(|| {
            let mut msg = format!("failed to validate circuit {ent_circuit}\n");
            msg.push_str("------------------------");
            write_circuit(&mut msg, ent_circuit, Some(comp_circuit), &params).unwrap();
            msg.push_str("------------------------");

            msg
        })?;
    }

    Ok(())
}

fn validate_circuit(
    comp_circuit: &circuit::Circuit,
    params @ ValidateParams { q_node, .. }: &ValidateParams<'_, '_>,
) -> Result<()> {
    for &ent_device in &comp_circuit.devices {
        validate_circuit_device(comp_circuit, ent_device, params)
            .with_context(|| format!("failed to validate device {ent_device}"))?;
    }

    for &ent_node in &comp_circuit.nodes {
        let _: &circuit::Node = q_node
            .get(ent_node)
            .ok()
            .with_context(|| format!("{ent_node} lacks `Node`"))?;
    }

    Ok(())
}

fn validate_circuit_device(
    comp_circuit: &circuit::Circuit,
    ent_device: Entity,
    ValidateParams {
        q_circuit,
        q_device,
        q_subcircuit_device,
        q_terminal_device,
        q_element_device,
        q_wire_device,
        q_terminal_node,
        q_expr_subcircuit_device,
        q_expr_element_device,
        ..
    }: &ValidateParams<'_, '_>,
) -> Result<()> {
    let comp_device: &circuit::Device = q_device
        .get(ent_device)
        .ok()
        .context("entity lacks `Device`")?;

    for (i, &ent_node) in comp_device.terminals.iter().enumerate() {
        anyhow::ensure!(
            comp_circuit.nodes.contains(&ent_node),
            "terminal {i} is attached to {ent_node}, which is not part of this circuit"
        );
    }

    let mut tys = Vec::with_capacity(1);

    if let Ok(&circuit::SubcircuitDevice { circuit }) = q_subcircuit_device.get(ent_device) {
        tys.push("SubcircuitDevice");

        let Ok((_, &circuit::Circuit { num_params, .. })) = q_circuit.get(circuit) else {
            anyhow::bail!("target circuit {circuit} does not exist or lacks `Circuit` component");
        };

        if let Ok(expr::SubcircuitDevice { params }) = q_expr_subcircuit_device.get(ent_device) {
            anyhow::ensure!(
                params.len() == num_params,
                "instance of circuit {circuit} should contain {num_params} params \
                (got {actual})",
                actual = params.len(),
            )
        }
    }

    if let Ok(circuit::TerminalDevice { terminal_i: _ }) = q_terminal_device.get(ent_device) {
        tys.push("TerminalDevice");
    }

    if let Ok(circuit::ElementDevice { elem_ty }) = q_element_device.get(ent_device) {
        tys.push("ElementDevice");

        anyhow::ensure!(
            elem_ty.num_nodes() == comp_device.terminals.len(),
            "element device of type {elem_ty} should contain {expected} terminals \
            (got {actual})",
            elem_ty = elem_ty.name(),
            expected = elem_ty.num_nodes(),
            actual = comp_device.terminals.len(),
        );

        if let Ok(expr::ElementDevice { params }) = q_expr_element_device.get(ent_device) {
            anyhow::ensure!(
                elem_ty.num_params() == params.len(),
                "element device of type {elem_ty} should contain {expected} \
                params (got {actual})",
                elem_ty = elem_ty.name(),
                expected = elem_ty.num_params(),
                actual = params.len(),
            );
        }
    }

    if let Ok(circuit::WireDevice {}) = q_wire_device.get(ent_device) {
        tys.push("WireDevice");
    }

    if !q_wire_device.contains(ent_device) {
        for (i, &ent_node) in comp_device.terminals.iter().enumerate() {
            anyhow::ensure!(
                q_terminal_node.contains(ent_node),
                "device is not a wire, but terminal {i} ({ent_node}) \
                lacks `TerminalNode`"
            );
        }
    }

    match tys.len() {
        0 => anyhow::bail!("entity lacks device-type-specific component"),
        1 => {}
        _ => anyhow::bail!(
            "entity contains more than one device-type-specific \
            components: {tys:?}"
        ),
    }

    Ok(())
}

#[tracing::instrument(skip_all)]
pub fn sys_print_circuit_to_string(
    In(ent_circuit): In<Entity>,
    params: ValidateParams<'_, '_>,
) -> String {
    let comp_circuit = params.q_circuit.get(ent_circuit).ok().map(|x| x.1);
    let mut out = String::new();
    write_circuit(&mut out, ent_circuit, comp_circuit, &params).unwrap();
    out
}

fn write_circuit(
    out: &mut dyn fmt::Write,
    ent_circuit: Entity,
    comp_circuit: Option<&circuit::Circuit>,
    ValidateParams {
        q_device,
        q_subcircuit_device,
        q_terminal_device,
        q_element_device,
        q_wire_device,
        q_node,
        q_ground_node,
        q_terminal_node,
        q_id,
        ..
    }: &ValidateParams<'_, '_>,
) -> fmt::Result {
    let ent_and_id = |ent: Entity| {
        if let Ok(meta::Id(id)) = q_id.get(ent) {
            format!("{ent} <{id}>")
        } else {
            ent.to_string()
        }
    };

    writeln!(out, "circuit {}", ent_and_id(ent_circuit))?;

    let Some(comp_circuit) = comp_circuit else {
        return writeln!(out, "  <not found>");
    };

    for &ent_device in &comp_circuit.devices {
        writeln!(out, "  device {}", ent_and_id(ent_device))?;

        if let Ok(circuit::Device { terminals }) = q_device.get(ent_device) {
            for (i, &ent_node) in terminals.iter().enumerate() {
                writeln!(out, "    terminal {i} = {ent_node}")?;
            }
        } else {
            writeln!(out, "    <not found>")?;
        }

        if let Ok(circuit::SubcircuitDevice { circuit }) = q_subcircuit_device.get(ent_device) {
            writeln!(out, "    subcircuit {}", ent_and_id(*circuit))?;
        }

        if let Ok(circuit::TerminalDevice { terminal_i }) = q_terminal_device.get(ent_device) {
            writeln!(out, "    terminal {terminal_i}")?;
        }

        if let Ok(circuit::ElementDevice { elem_ty }) = q_element_device.get(ent_device) {
            writeln!(out, "    element {}", elem_ty.name())?;
        }

        if let Ok(circuit::WireDevice { .. }) = q_wire_device.get(ent_device) {
            writeln!(out, "    wire")?;
        }

        writeln!(out)?;
    }

    for &ent_node in &comp_circuit.nodes {
        writeln!(out, "  node {}", ent_and_id(ent_node))?;

        if !q_node.contains(ent_node) {
            writeln!(out, "    <not found>")?;
        }

        if q_ground_node.contains(ent_node) {
            writeln!(out, "    ground")?;
        }

        if let Ok(&meta::TerminalNode { device }) = q_terminal_node.get(ent_node) {
            writeln!(out, "    terminal of {}", ent_and_id(device))?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{kio::test_utils::Workspace, tests::circuit_examples::example_circuit_paths};

    #[test]
    fn validate_example_circuits() {
        crate::tests::init();

        for path in example_circuit_paths() {
            let Workspace { mut world, .. } = path.load();
            world
                .run_system_cached(sys_validate)
                .unwrap()
                .with_context(|| format!("failed to validate {path:?}"))
                .unwrap();
        }
    }
}
