//! A helper module to load example circuits
use proptest::prelude::*;
use std::{fmt, path::PathBuf};

use crate::{kio, tests::circuit_validate::sys_print_circuit_to_string};

/// An example circuit that can be loaded by [`load_circuit`] or
/// [`Self::load`].
#[derive(Clone)]
pub(crate) struct ExampleCircuitPath(pub PathBuf);

/// Get an iterator producing a list of available example circuits.
pub(crate) fn example_circuit_paths() -> impl Iterator<Item = ExampleCircuitPath> {
    [
        "bjt/common-collector-npn.simac",
        "bjt/common-collector-pnp.simac",
        "bjt/common-emitter-npn.simac",
        "bjt/multivibrator-astable.simac",
        "passive/resistor.simac",
        "diode/bridge.simac",
        "tests/empty.simac",
        "tests/orphan-items.simac",
        "tests/diode-bridge-flip.simac",
        "tests/resistor-with-extra-wire.simac",
    ]
    .iter()
    .map(|s| ExampleCircuitPath(std::path::Path::new("../../examples").join(s)))
}

impl fmt::Debug for ExampleCircuitPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "ExampleCircuitPath({:?}):", self.0)?;

        // Pretty-print the loaded circuit in `ExampleCircuitPath::fmt` so that
        // we can locate the origin of each entity on test failure
        let text = std::panic::catch_unwind(|| {
            let kio::test_utils::Workspace {
                mut world,
                ent_main_circuit,
                ..
            } = self.load();
            world
                .run_system_cached_with(sys_print_circuit_to_string, ent_main_circuit)
                .unwrap()
        });
        match text {
            Ok(text) => f.write_str(text.trim()),
            Err(_) => f.write_str("< pretty printer panicked >"),
        }
    }
}

impl ExampleCircuitPath {
    /// Load an example circuit.
    pub fn load(&self) -> kio::test_utils::Workspace {
        kio::test_utils::Workspace::load_file(&self.0)
    }
}

/// A proptest strategy to get a path to an arbitrary example circuit.
pub(crate) fn any_example_circuit_path() -> impl Strategy<Value = ExampleCircuitPath> {
    prop::sample::select(example_circuit_paths().collect::<Vec<_>>())
}
