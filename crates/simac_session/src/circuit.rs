//! Bevy components to define circuits, devices, and their connectivity
use bevy_ecs::{entity::EntityHashSet, prelude::*};
use macro_rules_attribute::derive;
use simac_elements::ElemTy;
use simac_operator::{StashCx, StashableComponent, VirtEntity};
use std::sync::Arc;

// -------------------------------------------------------------------------

/// The bundle for circuits.
#[derive(Bundle, Debug, Default)]
pub struct CircuitBundle {
    pub circuit: Circuit,
}

/// The component for circuits.
///
/// # Stashing
///
/// Circuits are referenced by subcircuit devices by
/// [`SubcircuitDevice::circuit`].
#[derive(Component, Debug, Default)]
pub struct Circuit {
    pub devices: EntityHashSet,
    pub nodes: EntityHashSet,
    pub num_params: usize,
}

// -------------------------------------------------------------------------

/// The bundle for devices (circuit elements).
#[derive(Bundle, Debug)]
pub struct DeviceBundle {
    pub device: Device,
}

/// The component for devices (circuit elements).
///
/// # Stashing
///
/// Device entities are referenced by their containing circuit entities by
/// [`Circuit::devices`].
///
/// For non-wire devices, a device entity and its terminal node entities
/// form circular references by [`crate::meta::TerminalNode::device`]. They must
/// be despawn-and-stashed all at once.
#[derive(Component, Debug)]
pub struct Device {
    /// The nodes ([`Node`]) to connect the terminals to.
    ///
    /// For a non-wire device, the specified nodes should be considered
    /// permanently associated with the device and therefore this field is
    /// immutable.
    pub terminals: Arc<[Entity]>,
}

// -------------------------------------------------------------------------

/// The bundle for subcircuit devices.
#[derive(Bundle, Debug)]
pub struct SubcircuitDeviceBundle {
    pub device: DeviceBundle,
    pub subcircuit_device: SubcircuitDevice,
}

/// The component for subcircuit devices.
///
/// The parameters are calculated by [`crate::expr::SetupProgram`].
#[derive(Component, Debug)]
pub struct SubcircuitDevice {
    /// The circuit definition ([`CircuitBundle`]) to instantiate.
    pub circuit: Entity,
}

// -------------------------------------------------------------------------

/// The bundle for terminal devices ([`TerminalDevice`]).
#[derive(Bundle, Debug)]
pub struct TerminalDeviceBundle {
    pub device: DeviceBundle,
    pub terminal_device: TerminalDevice,
}

/// The component for terminal "devices", which represent connections to
/// the containing circuit's terminals. Each terminal "device" has one terminal
/// ([`Device::terminals`]).
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct TerminalDevice {
    /// The terminal index.
    pub terminal_i: usize,
}

// -------------------------------------------------------------------------

/// The bundle for (low-level) circuit elemen devices ([`ElementDevice`]).
#[derive(Bundle, Debug)]
pub struct ElementDeviceBundle {
    pub device: DeviceBundle,
    pub element_device: ElementDevice,
}

/// The component for (low-level) circuit element devices. A circuit element
/// device has the same number of terminals as the nodes defined by the eleent
/// type ([`ElemTy::num_nodes`]).
///
/// The parameters are calculated by [`crate::expr::SetupProgram`].
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct ElementDevice {
    /// The element type. This is immutable.
    pub elem_ty: ElemTy,
}

// -------------------------------------------------------------------------

#[derive(Bundle, Debug)]
pub struct WireDeviceBundle {
    pub device: DeviceBundle,
    pub wire: WireDevice,
}

/// The component for wires. A wire has two terminals ([`Device::terminals`]).
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct WireDevice;

// -------------------------------------------------------------------------

/// The bundle for nodes.
#[derive(Bundle, Debug, Default)]
pub struct NodeBundle {
    pub node: Node,
}

/// A marker component for nodes.
#[derive(Component, StashableComponent!, Debug, Default, Clone)]
pub struct Node;

/// The component for ground nodes.
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct GroundNode;

// `StashableComponent` manual impls
// -------------------------------------------------------------------------

/// Used by the [`trait@StashableComponent`] implementation of [`Circuit`].
#[derive(Debug)]
pub struct CircuitStashed {
    devices: Vec<VirtEntity>,
    nodes: Vec<VirtEntity>,
    num_params: usize,
}

impl StashableComponent for Circuit {
    type Stashed = CircuitStashed;

    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed {
        let Self {
            ref devices,
            ref nodes,
            num_params,
        } = *self;
        CircuitStashed {
            devices: devices.iter().map(|&e| cx.entity_to_virt(e)).collect(),
            nodes: nodes.iter().map(|&e| cx.entity_to_virt(e)).collect(),
            num_params,
        }
    }

    fn unstash(
        CircuitStashed {
            devices,
            nodes,
            num_params,
        }: Self::Stashed,
        cx: &mut dyn StashCx,
    ) -> Self {
        Self {
            devices: devices.into_iter().map(|e| cx.entity_to_phys(e)).collect(),
            nodes: nodes.into_iter().map(|e| cx.entity_to_phys(e)).collect(),
            num_params,
        }
    }
}

/// Used by the [`trait@StashableComponent`] implementation of [`Device`].
#[derive(Debug)]
pub struct DeviceStashed {
    terminals: Vec<VirtEntity>,
}

impl StashableComponent for Device {
    type Stashed = DeviceStashed;

    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed {
        let Self { terminals } = self;
        DeviceStashed {
            terminals: terminals.iter().map(|&e| cx.entity_to_virt(e)).collect(),
        }
    }

    fn unstash(DeviceStashed { terminals }: Self::Stashed, cx: &mut dyn StashCx) -> Self {
        Self {
            terminals: terminals.iter().map(|&e| cx.entity_to_phys(e)).collect(),
        }
    }
}

impl StashableComponent for SubcircuitDevice {
    type Stashed = VirtEntity;

    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed {
        let Self { circuit } = *self;
        cx.entity_to_virt(circuit)
    }

    fn unstash(stashed: Self::Stashed, cx: &mut dyn StashCx) -> Self {
        Self {
            circuit: cx.entity_to_phys(stashed),
        }
    }
}
