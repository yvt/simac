//! Simulation control
use bevy_ecs::prelude::*;
use macro_rules_attribute::derive;
use simac_operator::StashableComponent;
use std::time::Duration;

use crate::{flat, simulate};

#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct SimControl {
    pub ent_root_circuit: Entity,
    pub time_step: f64,
    /// The requested simulation time scale.
    pub time_scale: f64,
}

impl SimControl {
    pub const DEFAULT_TIME_STEP: f64 = 1.0e-5;
    pub const DEFAULT_TIME_SCALE: f64 = 1.0e-3;
}

#[derive(Component, Debug)]
pub struct SimControlFeedback {
    /// The estimated maximum simulation time scale.
    pub time_scale_max_est: f64,
    /// `Err(_)` if the last update was failure.
    pub last_error: Result<(), simac_simulate::StepError>,
}

#[derive(Component, Debug)]
pub struct Active;

#[derive(Component, Debug)]
struct SimControlState {
    subframe: f64,
}

/// A lazily initialized [`flat::Flattened`].
#[derive(Component, Default, Debug)]
struct LazyFlattened(Option<flat::Flattened>);

#[derive(Component, Debug)]
pub struct SubcircuitView {
    pub ent_sim: Entity,
}

#[derive(Bundle, Debug)]
pub struct SubcircuitViewBundle {
    pub subcircuit_view: SubcircuitView,
    pub subcircuit_view_params: simulate::SimSubcircuitViewParams,
    pub subcircuit_view_output: simulate::SimSubcircuitViewOutput,
}

// Systems
// -------------------------------------------------------------------------

pub(crate) fn init_world(world: &mut World) {
    world.register_required_components_with::<SimControl, SimControlFeedback>(|| {
        SimControlFeedback {
            time_scale_max_est: 0.0,
            last_error: Ok(()),
        }
    });
    world.register_required_components_with::<SimControl, SimControlState>(|| SimControlState {
        subframe: 0.0,
    });
    world.register_required_components::<SimControl, LazyFlattened>();
    world.register_required_components::<SimControl, simulate::SimState>();
}

#[derive(Debug, Resource, Clone, Copy)]
pub struct Frame {
    pub time_delta: f64,
    pub force_update: bool,
}

pub fn system_configs_update<M>(
    frame_source: impl IntoSystem<(), Frame, M>,
) -> bevy_ecs::schedule::SystemConfigs {
    (frame_source.pipe(sys_simulate), sys_export_subcircuit_views).chain()
}

fn sys_simulate(
    In(frame): In<Frame>,
    mut sims: Query<
        (
            &SimControl,
            &mut SimControlState,
            &mut SimControlFeedback,
            &mut simulate::SimState,
            &mut LazyFlattened,
        ),
        With<Active>,
    >,
    flat_sync_param: flat::SyncCircuitParams<'_, '_>,
    flat_setup_param: flat::SetupCircuitParams<'_, '_>,
) {
    for (
        sim_control,
        mut sim_control_state,
        mut sim_control_feedback,
        mut sim_state,
        mut lazy_flattened,
    ) in sims.iter_mut()
    {
        // Initialize `LazyFlattened`
        if lazy_flattened.0.is_none() {
            lazy_flattened.set_changed();
        }
        let flattened = lazy_flattened
            .bypass_change_detection()
            .0
            .get_or_insert_with(|| flat::Flattened::new(sim_control.ent_root_circuit));

        // Determine the number of time steps to make
        sim_control_state.subframe += frame.time_delta * sim_control.time_scale;
        let num_steps = (sim_control_state.subframe / sim_control.time_step).floor() as u64;
        sim_control_state.subframe = (sim_control_state.subframe
            - num_steps as f64 * sim_control.time_step)
            // precaution against rounding errors and NaN
            .max(0.0);

        if num_steps == 0 && !frame.force_update {
            tracing::trace!("`num_steps == 0`; not running `sch_frame`");
            return;
        }

        // Update simulation parameter
        let sim_params = simulate::SimParams {
            time_step: sim_control.time_step,
            min_time_step: sim_control.time_step.min(1e-12), // FIXME: Make this configurable
            max_num_steps: num_steps,
            timeout: Duration::from_secs_f64(frame.time_delta),
        };
        tracing::debug!(?sim_params);

        // Synchronize the simulation world
        let flatten = flat::Flatten {
            root_circuit_params: vec![],
        };
        let changed = flat::sync_circuit(&flatten, flattened, &flat_sync_param);

        let changed = changed | flat::setup_circuit(&flatten, flattened, &flat_setup_param);

        simulate::sync_circuit(&mut sim_state, flattened);

        if changed {
            lazy_flattened.set_changed();
        }

        // Update simulation
        // TODO: Run simulation in background thread
        let sim_status = match simulate::step_simulation(&sim_params, &mut sim_state) {
            Ok(sim_status) => {
                sim_control_feedback.last_error = Ok(());
                sim_status
            }
            Err(error) => {
                sim_control_feedback.last_error = Err(error);
                continue;
            }
        };

        if sim_params.max_num_steps == 0 {
            // The `time_scale_actual` update routine would \
            // produce erroneous output if `num_steps == 0`
            continue;
        }

        // Estimate the actual time scale
        sim_control_feedback.time_scale_max_est =
            sim_status.time_delta / sim_status.elapsed.as_secs_f64();
    }
}

fn sys_export_subcircuit_views(
    sims: Query<(Ref<simulate::SimState>, &LazyFlattened)>,
    mut views: Query<(
        Entity,
        Ref<SubcircuitView>,
        Ref<simulate::SimSubcircuitViewParams>,
        &mut simulate::SimSubcircuitViewOutput,
    )>,
) {
    for (ent_view, view, view_params, mut view_out) in views.iter_mut() {
        let _span = tracing::info_span!("Exporting a subcircuit view", ?ent_view).entered();

        let Ok((sim_state, flattened)) = sims.get(view.ent_sim) else {
            tracing::warn!(?view.ent_sim, "`SimControl` not found");
            continue;
        };

        let Some(flattened) = &flattened.0 else {
            tracing::warn!(%view.ent_sim, "`LazyFlattened` is not initialized yet");
            continue;
        };

        if !sim_state.is_changed() && !view_params.is_changed() {
            // The inputs did not change, no need to update
            tracing::trace!(?view, "Inputs did not change, skipping");
            continue;
        }

        simulate::export_subcircuit_view(&sim_state, flattened, &view_params, &mut view_out);
    }
}
