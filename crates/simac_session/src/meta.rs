//! Metadata components attached to circuit component entities
//! ([`crate::circuit`]), which have no effect on simulation
//! ([`crate::simulate`])
use bevy_ecs::prelude::*;
use macro_rules_attribute::derive;
use simac_operator::{StashCx, StashableComponent, VirtEntity};

#[cfg(doc)]
use crate::circuit;

pub mod assign_ids;

/// The component for identifiers, which are supposed to be unique within some
/// scope.
#[derive(Component, StashableComponent!, Debug, Clone, Default)]
pub struct Id(pub String);

#[derive(Component, StashableComponent!, Debug, Clone, Default)]
pub struct Label(pub String);

/// The component for circuit ([`circuit::Circuit`]) metadata.
#[derive(Component, StashableComponent!, Debug, Clone, Default)]
pub struct CircuitMeta {
    pub terminal_names: Vec<String>,
    /// The parameter definitions. The length must be equal to
    /// [`circuit::Circuit::num_params`].
    pub params: Vec<ParamDefMeta>,
}

#[derive(Debug, Clone)]
pub struct ParamDefMeta {
    pub id: String,
    pub description: String,
    pub default_val: f64,
}

/// A node ([`circuit::Node`]) that is statically associated with a terminal
/// of a non-wire device ([`circuit::Device`]).
///
/// # Stashing
///
/// See [`circuit::Device`].
#[derive(Component, Debug)]
pub struct TerminalNode {
    /// The device ([`circuit::Device`]) this terminal belongs to.
    pub device: Entity,
}

impl StashableComponent for TerminalNode {
    type Stashed = VirtEntity;

    fn stash(&mut self, cx: &mut dyn StashCx) -> Self::Stashed {
        cx.entity_to_virt(self.device)
    }

    fn unstash(stashed: Self::Stashed, cx: &mut dyn StashCx) -> Self {
        Self {
            device: cx.entity_to_phys(stashed),
        }
    }
}
