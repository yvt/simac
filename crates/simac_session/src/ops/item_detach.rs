//! Disconnect devices from the rest of the circuit
//!
//! For every terminal of every device included in the input set, if this node
//! is attached to any other devices not included in the input set, a node will
//! be created at the same position, and some of the connections to the node are
//! reattached to this new node according to the following rules:
//!
//! - Let `Cinner` be the set of the connections from the devices included in
//!   the input set and `Couter` the rest.
//!
//! - If the node is a terminal node of non-wire device `d`:
//!
//!     - If `d` is included in the input set, all connections in `Couter` are
//!       migrated to the new node.
//!
//!     - If `d` is not included in the input set, all connections in `Cinner`
//!       are migrated to the new node.
//!
//! > This ensures that the relationship between devices and their terminal
//! > nodes ([`meta::TerminalNode`]) is preserved, and (equivalently) that the
//! > new nodes never have [`meta::TerminalNode`].
//!
//! - If the node is a non-terminal node, all connections in `Couter` are
//!   migrated to the new node.
use bevy_ecs::{
    entity::{EntityHashMap, EntityHashSet},
    prelude::*,
};
use itertools::izip;
use simac_operator::{VirtEntity, operator};
use std::{mem::replace, sync::Arc};

use crate::{circuit, layout, meta};

/// Disconnect devices from the rest of the circuit
#[derive(Debug, Clone)]
pub struct ItemDetach {
    pub ent_circuit: Entity,
    /// The items to detach.
    pub target: Target,
}

#[derive(Debug, Clone)]
pub enum Target {
    /// Devices. Duplicates are allowed.
    Devices {
        ent_devices: Vec<Entity>,
        /// The nodes to exclude from the detaching operation. Nodes that are
        /// not terminals of any devices in `ent_devices` are ignored.
        ent_exclude_nodes: EntityHashSet,
    },
    /// A single device terminal.
    DeviceTerminal {
        ent_device: Entity,
        terminal_i: usize,
    },
}

#[derive(Debug)]
struct ItemDetachUr {
    virt_circuit: VirtEntity,
    new_nodes: Vec<NewNode>,
    reattaches: Vec<Reattach>,
}

/// An instruction to create a new node.
#[derive(Debug)]
struct NewNode {
    virt_node: VirtEntity,
    position: layout::Position,
}

/// An instruction to replace a device's terminal node.
#[derive(Debug)]
struct Reattach {
    virt_device: VirtEntity,
    /// An index into [`circuit::Device::terminals`].
    terminal_i: usize,
    /// The new node to attach it to.
    virt_node: VirtEntity,
}

impl operator::Operator for ItemDetach {
    #[tracing::instrument(skip_all)]
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let Self {
            ent_circuit,
            mut target,
        } = *self;
        let mut world = cx.world();

        let comp_circuit: &circuit::Circuit = world
            .get(ent_circuit)
            .expect("`ent_circuit` does not contain `Circuit`");

        let virt_circuit = cx.entity_to_virt(ent_circuit);

        /// A terminal of a device.
        struct Attachment {
            ent_device: Entity,
            terminal_i: usize,
            next_attachment_i: usize,
            is_inner: bool,
        }

        /// Node information.
        struct Node {
            /// The first `Attachment` attached to the node.
            first_attachment_i: usize,
        }

        const NONE: usize = usize::MAX;

        let mut attachments: Vec<Attachment> = Vec::new();
        let mut nodes: EntityHashMap<Node> = <_>::default();

        match &mut target {
            Target::Devices {
                ent_devices: ent_items,
                ent_exclude_nodes,
            } => {
                ent_items.sort_unstable();
                ent_items.dedup();

                nodes.reserve(ent_items.len() * 2);

                for &ent_device in &*ent_items {
                    let circuit::Device { terminals, .. } = world
                        .get(ent_device)
                        .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));

                    // Add nodes we're interested in to `nodes`
                    for &ent_node in terminals.iter() {
                        if ent_exclude_nodes.contains(&ent_node) {
                            continue;
                        }
                        nodes.entry(ent_node).or_insert_with(|| Node {
                            first_attachment_i: NONE,
                        });
                    }
                }
            }
            &mut Target::DeviceTerminal {
                ent_device,
                terminal_i,
            } => {
                let circuit::Device { terminals, .. } = world
                    .get(ent_device)
                    .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));

                // Add nodes we're interested in to `nodes`
                nodes.insert(
                    terminals[terminal_i],
                    Node {
                        first_attachment_i: NONE,
                    },
                );
            }
        }

        // Find all connections to `nodes`
        for &ent_device in &comp_circuit.devices {
            let circuit::Device { terminals, .. } = world
                .entity(ent_device)
                .get()
                .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));

            let is_inner = match &target {
                Target::Devices { ent_devices, .. } => {
                    ent_devices.binary_search(&ent_device).is_ok()
                }
                &Target::DeviceTerminal {
                    ent_device: e,
                    terminal_i: _,
                } => ent_device == e,
            };

            for (terminal_i, &ent_node) in terminals.iter().enumerate() {
                let Some(node) = nodes.get_mut(&ent_node) else {
                    continue;
                };

                let is_inner = match target {
                    Target::Devices { .. } => is_inner,
                    Target::DeviceTerminal {
                        ent_device: _,
                        terminal_i: i,
                    } => is_inner && terminal_i == i,
                };

                let attachment_i = attachments.len();
                attachments.push(Attachment {
                    ent_device,
                    terminal_i,
                    next_attachment_i: node.first_attachment_i,
                    is_inner,
                });
                node.first_attachment_i = attachment_i;
            }
        }

        // Create instructions for the operator based on the collected data
        let mut new_nodes = Vec::new();
        let mut reattaches = Vec::new();

        for (&ent_node, node) in &nodes {
            let iter_attachments = || {
                let mut cur = node.first_attachment_i;
                let attachments = &attachments;
                std::iter::from_fn(move || {
                    let attachment = attachments.get(cur)?;
                    cur = attachment.next_attachment_i;
                    Some(attachment)
                })
            };

            let (num_all, num_inner) =
                iter_attachments().fold((0, 0), |(num_all, num_inner), attachment| {
                    (num_all + 1, num_inner + attachment.is_inner as usize)
                });
            if num_inner == 0 || num_inner == num_all {
                // All or none of the connections are to `ent_items`
                continue;
            }

            // Create a new node at the same position.
            let position = *cx
                .world()
                .get(ent_node)
                .unwrap_or_else(|| panic!("{ent_node} lacks `Position`"));
            let virt_new_node = cx.new_virt_entity();

            world = cx.world();

            new_nodes.push(NewNode {
                virt_node: virt_new_node,
                position,
            });

            // Which set of connections should be reattached to `new_nodes`,
            // the one from `ent_items` (`is_inner == true`) or the other?
            let should_reattach_inner =
                world
                    .get(ent_node)
                    .is_some_and(|meta::TerminalNode { device }| {
                        // If `ent_node` is a terminal node (of a non-wire
                        // device), then the reattach the set NOT containing
                        // that device.
                        match &target {
                            Target::Devices { ent_devices, .. } => {
                                ent_devices.binary_search(device).is_err()
                            }
                            Target::DeviceTerminal { ent_device, .. } => device != ent_device,
                        }
                    });

            reattaches.extend(
                iter_attachments()
                    .filter(|attachment| attachment.is_inner == should_reattach_inner)
                    .inspect(|attachment| {
                        debug_assert!(
                            world
                                .get::<circuit::WireDevice>(attachment.ent_device)
                                .is_some(),
                            "attempted to reattach a terminal of {}, which is
                            not a wire device",
                            attachment.ent_device,
                        )
                    })
                    .map(|attachment| Reattach {
                        virt_device: cx.entity_to_virt(attachment.ent_device),
                        terminal_i: attachment.terminal_i,
                        virt_node: virt_new_node,
                    }),
            )
        }

        reattaches.sort_unstable_by_key(|r| r.virt_device);

        tracing::debug!(?new_nodes, ?reattaches);

        Ok(operator::OperatorRedo::redo(
            Box::new(ItemDetachUr {
                virt_circuit,
                new_nodes,
                reattaches,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for ItemDetachUr {
    fn redo(
        mut self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        let Self {
            virt_circuit,
            new_nodes,
            reattaches,
        } = &mut *self;

        let ent_circuit = cx.entity_to_phys(*virt_circuit);

        // Spawn `new_nodes`
        let world = cx.world_mut();
        let ent_new_nodes: Vec<Entity> = new_nodes
            .iter()
            .map(|new_node| {
                world
                    .spawn((circuit::NodeBundle::default(), new_node.position))
                    .id()
            })
            .collect();

        let mut circuit: Mut<circuit::Circuit> = world.get_mut(ent_circuit).unwrap();
        circuit.nodes.extend(ent_new_nodes.iter().copied());

        for (new_node, ent_new_node) in izip!(new_nodes, ent_new_nodes) {
            cx.spawned(ent_new_node, new_node.virt_node);
        }

        // Reattach wires to new nodes
        swap_reattach_nodes(reattaches, cx);

        self
    }
}

impl operator::OperatorUndo for ItemDetachUr {
    fn undo(
        mut self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        let Self {
            virt_circuit,
            new_nodes,
            reattaches,
        } = &mut *self;

        // Reattach wires to old nodes
        swap_reattach_nodes(reattaches, cx);

        // Despawn `new_nodes`
        let ent_new_nodes: Vec<Entity> = new_nodes
            .iter()
            .map(|new_node| cx.entity_to_phys(new_node.virt_node))
            .collect();

        for new_node in new_nodes {
            cx.despawn(new_node.virt_node);
        }

        let ent_circuit = cx.entity_to_phys(*virt_circuit);
        let mut circuit: Mut<circuit::Circuit> = cx.world_mut().get_mut(ent_circuit).unwrap();
        let circuit = &mut *circuit;
        for ent_new_node in ent_new_nodes {
            circuit.nodes.remove(&ent_new_node);
        }

        self
    }
}

/// Swap `reattach.virt_node` and
/// `devices[reattach.virt_device].terminals[reattach.terminal_i]` for every
/// [`Reattach`].
// FIXME: Take `&mut dyn OperatorCx` when dyn upcasting is stabilized
fn swap_reattach_nodes(reattaches: &mut [Reattach], cx: &mut (impl operator::OperatorCx + ?Sized)) {
    let mut empty_terminals: Arc<[Entity]> = Arc::new([]);

    for reattaches in reattaches.chunk_by_mut(|r0, r1| r0.virt_device == r1.virt_device) {
        let virt_device = reattaches[0].virt_device;
        let ent_device = cx.entity_to_phys(virt_device);

        // Take `Device::terminals`
        let mut device: Mut<circuit::Device> = cx.world_mut().get_mut(ent_device).unwrap();
        let mut terminals_arc = replace(&mut device.terminals, empty_terminals);
        let terminals = Arc::make_mut(&mut terminals_arc);

        // Update `terminals`
        for Reattach {
            terminal_i,
            virt_node,
            ..
        } in reattaches
        {
            // Swap `terminals[*terminal_i]` and `*virt_node`
            let ent_node = cx.entity_to_phys(*virt_node);
            let ent_old_node = replace(&mut terminals[*terminal_i], ent_node);
            *virt_node = cx.entity_to_virt(ent_old_node);
        }

        // Put `Device::terminals` back
        let mut device: Mut<circuit::Device> = cx.world_mut().get_mut(ent_device).unwrap();
        empty_terminals = replace(&mut device.terminals, terminals_arc);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        kio::test_utils::Workspace,
        tests::circuit_examples::{ExampleCircuitPath, any_example_circuit_path},
    };
    use fxhash::FxHashSet;
    use itertools::enumerate;
    use proptest::prelude::*;
    use simac_operator::stack::OperatorStack;

    proptest! {
        /// Check that [`ItemDetach`] is no-op if all items or no items are
        /// specified.
        #[test]
        fn pt_detach_noop(
            path in any_example_circuit_path(),
            all: bool,
            exclude_bits in prop::bits::u64::between(0, 50),
        ) {
            crate::tests::init();
            pt_detach_noop_inner(path, all, exclude_bits);
        }

        #[test]
        fn pt_detach_any_devices(
            path in any_example_circuit_path(),
            remove_bits in prop::bits::u64::between(0, 50),
            exclude_bits in prop::bits::u64::between(0, 50),
        ) {
            crate::tests::init();
            pt_detach_any_devices_inner(path, remove_bits, exclude_bits);
        }

        #[test]
        fn pt_detach_device_terminal(
            path in any_example_circuit_path(),
            device_i: prop::sample::Index,
            terminal_i: prop::sample::Index,
        ) {
            crate::tests::init();
            pt_detach_device_terminal_inner(path, device_i, terminal_i)?;
        }
    }

    fn pt_detach_noop_inner(path: ExampleCircuitPath, all: bool, exclude_bits: u64) {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // List all node entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_nodes: Vec<_> = comp_circuit.nodes.iter().copied().collect();
        ent_all_nodes.sort_unstable();

        // List the device entities to detach
        let ent_items = if all {
            comp_circuit.devices.iter().copied().collect()
        } else {
            Vec::new()
        };

        // List the node entities to exclude from detaching
        let ent_exclude_nodes = enumerate(&ent_all_nodes)
            .filter_map(|(i, &ent_item)| {
                prop::bits::BitSetLike::test(&exclude_bits, i).then_some(ent_item)
            })
            .collect();

        // Apply the operator
        let operator = Box::new(ItemDetach {
            ent_circuit: ent_main_circuit,
            target: Target::Devices {
                ent_devices: ent_items,
                ent_exclude_nodes,
            },
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        let undo: &ItemDetachUr = operator_stack
            .undo_stack_peek()
            .unwrap()
            .as_any()
            .downcast_ref()
            .unwrap();
        tracing::debug!(?undo);

        assert!(undo.new_nodes.is_empty());
        assert!(undo.reattaches.is_empty());
    }

    fn pt_detach_any_devices_inner(path: ExampleCircuitPath, remove_bits: u64, exclude_bits: u64) {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // List all entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_items: Vec<_> = comp_circuit.devices.iter().copied().collect();
        ent_all_items.sort_unstable();

        let mut ent_all_nodes: Vec<_> = comp_circuit.nodes.iter().copied().collect();
        ent_all_nodes.sort_unstable();

        // List the entities to detach
        let ent_items: Vec<_> = enumerate(&ent_all_items)
            .filter_map(|(i, &ent_item)| {
                prop::bits::BitSetLike::test(&remove_bits, i).then_some(ent_item)
            })
            .collect();

        // List the nodes to exclude from detaching
        let ent_exclude_nodes: EntityHashSet = enumerate(&ent_all_nodes)
            .filter_map(|(i, &ent_item)| {
                prop::bits::BitSetLike::test(&exclude_bits, i).then_some(ent_item)
            })
            .collect();

        // List the effective subset of `ent_exclude_nodes`
        let mut ent_exclude_nodes_effective: EntityHashSet = <_>::default();
        for &ent_device in &ent_items {
            let comp_device: &circuit::Device = world.get(ent_device).unwrap();
            ent_exclude_nodes_effective.extend(
                comp_device
                    .terminals
                    .iter()
                    .copied()
                    .filter(|ent_node| ent_exclude_nodes.contains(ent_node)),
            );
        }

        // Apply the operator
        let operator = Box::new(ItemDetach {
            ent_circuit: ent_main_circuit,
            target: Target::Devices {
                ent_devices: ent_items.clone(),
                ent_exclude_nodes: ent_exclude_nodes.clone(),
            },
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        if ent_exclude_nodes_effective.is_empty() {
            // Check that `ent_items` and the other part of the circuit are
            // completely separated now
            let uf = world
                .run_system_cached(crate::tests::circuit_union_find::sys_union_find)
                .unwrap();

            let inner_roots: FxHashSet<usize> =
                ent_items.iter().map(|&e| uf.root_i_of(e)).collect();
            let outer_roots: FxHashSet<usize> = ent_all_items
                .iter()
                .filter(|e| ent_items.binary_search(e).is_err())
                .map(|&e| uf.root_i_of(e))
                .collect();

            assert!(
                inner_roots.intersection(&outer_roots).next().is_none(),
                "the specified items are still connected to \
                the rest of the circuit.\n\
                \n\
                ent_items = {ent_items:?}\n\
                \n\
                -----------------------\n\
                {}
                -----------------------\n",
                world
                    .run_system_cached_with(
                        crate::tests::circuit_validate::sys_print_circuit_to_string,
                        ent_main_circuit,
                    )
                    .unwrap(),
            );
        }

        let undo: &ItemDetachUr = operator_stack
            .undo_stack_peek()
            .unwrap()
            .as_any()
            .downcast_ref()
            .unwrap();

        // Devices should not be detached from any nodes in `ent_exclude_nodes`
        for &Reattach {
            virt_device,
            terminal_i,
            ..
        } in &undo.reattaches
        {
            let ent_device = operator_stack.entity_to_phys(virt_device);
            let comp_device: &circuit::Device = world.get(ent_device).unwrap();
            let ent_node = comp_device.terminals[terminal_i];
            assert!(!ent_exclude_nodes.contains(&ent_node));
        }

        // Undo the operator and check structural integrity
        operator_stack.undo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-undo validation failed");
    }

    fn pt_detach_device_terminal_inner(
        path: ExampleCircuitPath,
        device_i: prop::sample::Index,
        terminal_i: prop::sample::Index,
    ) -> prop::test_runner::TestCaseResult {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // List all device entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_items: Vec<_> = comp_circuit.devices.iter().copied().collect();
        ent_all_items.sort_unstable();

        // Find the terminal to detach
        prop_assume!(!ent_all_items.is_empty());
        let ent_device = *device_i.get(&ent_all_items);
        let comp_device: &circuit::Device = world.get(ent_device).unwrap();
        prop_assume!(!comp_device.terminals.is_empty());
        let terminal_i = terminal_i.index(comp_device.terminals.len());

        // Apply the operator
        let operator = Box::new(ItemDetach {
            ent_circuit: ent_main_circuit,
            target: Target::DeviceTerminal {
                ent_device,
                terminal_i,
            },
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        // Check that the device terminal is open now
        let ent_node = world.get::<circuit::Device>(ent_device).unwrap().terminals[terminal_i];

        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let attachments: Vec<_> = comp_circuit
            .devices
            .iter()
            .flat_map(|&ent_device| {
                // Find terminals attached to `ent_node`
                world
                    .get::<circuit::Device>(ent_device)
                    .unwrap()
                    .terminals
                    .iter()
                    .filter_map(move |&e| (e == ent_node).then_some((ent_device, e)))
            })
            .collect();

        assert!(
            attachments.len() == 1,
            "the specified terminal ({ent_device}, terminal {terminal_i}) is \
            still connected\n\
            \n\
            attachments = {attachments:?}\n\
            \n\
            -----------------------\n\
            {}
            -----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        // Undo the operator and check structural integrity
        operator_stack.undo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-undo validation failed");

        Ok(())
    }
}
