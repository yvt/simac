//! Move circuit items, maintaining topology
//!
//! This has no effect if a terminal node is specified; the owning device should
//! be specified instead.
use bevy_ecs::prelude::*;
use bevy_math::{I64Vec2, IVec2};
use simac_operator::{VirtEntity, operator};

use crate::{circuit, layout, meta};

/// Move circuit items, maintaining topology
#[derive(Debug, Clone)]
pub struct ItemMove {
    /// The list of entities to move. Duplicates are allowed.
    pub entities: Vec<Entity>,
    pub delta: I64Vec2,
    pub delta_rotation: layout::Rotation,
}

#[derive(Debug)]
struct ItemMoveUr {
    virt_entities: Vec<VirtEntity>,
    delta: I64Vec2,
    delta_rotation: layout::Rotation,
}

/// The error type for [`ItemMove::new_rotate`].
#[derive(Debug, Clone, Copy, thiserror::Error)]
pub enum NewRotateError {
    #[error("no valid entities provided")]
    NoValidItems,
}

impl ItemMove {
    /// Create an [`ItemMove`] to rotate specified items by their central point.
    ///
    /// *Multiplicativity:*
    /// Given a set of items, if you apply a series of rotations using the
    /// operators returned by this function, the net effect is the same as
    /// applying a single operator based on the product of all rotations.
    ///
    /// > This means that, if you apply rotation repeatedly, the items return to
    /// > the original state after every full revolution.
    ///
    /// If the items are close to the boundary of the representable range of
    /// [`IVec2`], they might be pushed away from the boundary to avoid
    /// overflow. In such cases, multiplicativity is lost.
    #[tracing::instrument(skip(world, entities))]
    pub fn new_rotate(
        world: &World,
        entities: Vec<Entity>,
        delta_rotation: layout::Rotation,
    ) -> Result<Self, NewRotateError> {
        // Find the bounding box of the entities
        let mut position_min = IVec2::splat(i32::MAX);
        let mut position_max = IVec2::splat(i32::MIN);

        let mut check_ent = |ent_item: Entity| {
            let Some(&layout::Position(position)) = world.get(ent_item) else {
                return;
            };
            position_min = position_min.min(position);
            position_max = position_max.max(position);
        };

        for &ent_item in &entities {
            let entity_ref = world.entity(ent_item);

            if entity_ref.contains::<meta::TerminalNode>() {
                continue;
            }

            check_ent(ent_item);

            // Include device terminals for bounding box calculation
            // (The origin point of a device can be very off-center)
            let Some(circuit::Device { terminals, .. }) = entity_ref.get() else {
                continue;
            };

            for &ent_node in terminals.iter() {
                check_ent(ent_node);
            }
        }

        if position_min.x > position_max.x {
            return Err(NewRotateError::NoValidItems);
        }

        let delta = delta_to_rotate_by_center(position_min, position_max, delta_rotation);

        Ok(Self {
            entities,
            delta,
            delta_rotation,
        })
    }
}

impl operator::Operator for ItemMove {
    #[tracing::instrument(skip_all)]
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let Self {
            entities,
            delta,
            delta_rotation,
        } = *self;
        let world = cx.world();

        // Find the actual entities to modify
        let mut virt_entities = Vec::with_capacity(entities.len());
        let mut position_min = I64Vec2::splat(i32::MAX.into());
        let mut position_max = I64Vec2::splat(i32::MIN.into());

        for entity in entities {
            let entity_ref = world.entity(entity);

            if !entity_ref.contains::<circuit::Node>() && !entity_ref.contains::<circuit::Device>()
            {
                tracing::debug!(%entity, "This entity is neither a node nor a device; ignoring");
                continue;
            }

            if entity_ref.contains::<meta::TerminalNode>() {
                // If it's a terminal node, ignore it
                continue;
            }

            if entity_ref.contains::<circuit::WireDevice>() {
                // A wire device does not have a position
                continue;
            }

            let Some(&layout::Position(position)) = entity_ref.get() else {
                tracing::debug!(
                    %entity,
                    "This entity is unexpectedly lacking `Position` component; ignoring",
                );
                continue;
            };

            // FIXME: When moving a device, make sure the positions of terminal
            // nodes don't overflow

            let position = I64Vec2::from(position);

            let position = delta_rotation.apply(position);

            position_min = position_min.min(position);
            position_max = position_max.max(position);

            virt_entities.push(cx.entity_to_virt(entity));
        }

        virt_entities.sort_unstable();
        virt_entities.dedup();

        tracing::debug!(?virt_entities, "Final set of entities to modify");

        // Clamp `delta` so the final positions will not overflow
        let delta_max = I64Vec2::splat(i32::MAX.into()).saturating_sub(position_max);
        let delta_min = I64Vec2::splat(i32::MIN.into()).saturating_sub(position_min);
        let delta = delta.clamp(delta_min, delta_max);

        Ok(operator::OperatorRedo::redo(
            Box::new(ItemMoveUr {
                virt_entities,
                delta,
                delta_rotation,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for ItemMoveUr {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        self.apply(cx, true);
        self
    }
}

impl operator::OperatorUndo for ItemMoveUr {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        self.apply(cx, false);
        self
    }
}

impl ItemMoveUr {
    fn apply(&self, cx: &mut (impl operator::OperatorCx + ?Sized), forward: bool) {
        let entities: Vec<Entity> = self
            .virt_entities
            .iter()
            .map(|&x| cx.entity_to_phys(x))
            .collect();
        let world = cx.world_mut();
        let rotation = if forward {
            self.delta_rotation
        } else {
            self.delta_rotation.inverse()
        };
        for entity in entities {
            let Some(mut comp_position) = world.get_mut::<layout::Position>(entity) else {
                continue;
            };
            let mut position: I64Vec2 = comp_position.0.into();
            if forward {
                position = rotation.apply(position);
                position += self.delta;
            } else {
                position -= self.delta;
                position = rotation.apply(position);
            }
            comp_position.0 = position.try_into().expect("position overflow");

            if self.delta_rotation != layout::Rotation::default() {
                let Some(mut comp_rotation) = world.get_mut::<layout::Rotation>(entity) else {
                    continue;
                };

                *comp_rotation = rotation * *comp_rotation;
            }
        }
    }
}

/// Calculate the value of [`ItemMove::delta`] based on the bounding box of
/// items to rotate them by their central point.
pub(crate) fn delta_to_rotate_by_center(
    position_min: IVec2,
    position_max: IVec2,
    delta_rotation: layout::Rotation,
) -> I64Vec2 {
    // Find the pivot point (fixed-point numbers with one fractional bit)
    let [position_min, position_max] = [position_min, position_max].map(I64Vec2::from);
    let pivot = position_min + position_max;

    // Flip rounding mode depending on whether the bounding box size is
    // `(odd, even)` or `(even, odd)`. If the size is `(odd, odd)` or `(even,
    // even)`, or if `delta_rotation` does not contain `TRANSPOSE`, this has no
    // effect.
    //
    // This ensures that the bounding box is preserved after applying two
    // `Rotation`s containing `TRANSPOSE` in succession.
    let odd_width = (position_min.x ^ position_max.x) & 1;

    // Find `delta`
    (pivot - delta_rotation.apply(pivot) + I64Vec2::splat(odd_width)) >> 1
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use insta::assert_snapshot;
    use proptest::prelude::*;

    use crate::ops::tests::apply_op_on_main_circuit;

    proptest! {
        #[test]
        fn pt_nodes(
            positions: Vec<[i32; 2]>,
            delta: [i32; 2],
            delta_rotation in layout::tests::any_rotation(),
        ) {
            pt_nodes_inner(positions, delta, delta_rotation)?;
        }
    }

    fn pt_nodes_inner(
        positions: Vec<[i32; 2]>,
        delta: [i32; 2],
        delta_rotation: layout::Rotation,
    ) -> prop::test_runner::TestCaseResult {
        crate::tests::init();

        let positions: Vec<_> = positions.into_iter().map(IVec2::from).collect();
        let delta: I64Vec2 = IVec2::from(delta).into();

        let mut world = World::new();

        // Spawn non-terminal node entities
        let ent_items: Vec<Entity> = world
            .spawn_batch(
                positions
                    .iter()
                    .map(|&p| (layout::Position(p), circuit::Node)),
            )
            .collect();
        assert_eq!(ent_items.len(), positions.len());

        let mut operator_stack = simac_operator::stack::OperatorStack::default();

        // Execute the operator
        operator_stack
            .execute(
                &mut world,
                Box::new(ItemMove {
                    entities: ent_items.clone(),
                    delta,
                    delta_rotation,
                }),
            )
            .unwrap();

        let op_undo: &ItemMoveUr = operator_stack
            .undo_stack_peek()
            .unwrap()
            .as_any()
            .downcast_ref()
            .unwrap();

        // If `delta` is different from the original value, it was clamped to
        // avoid overflow. That means at least one node should now be located on
        // the boundary of the representable range.
        if delta != op_undo.delta {
            let new_positions: Vec<_> = world
                .query::<&layout::Position>()
                .iter_many(&world, &ent_items)
                .map(|c| c.0)
                .collect();
            assert_eq!(new_positions.len(), positions.len());

            prop_assert!(
                new_positions
                    .iter()
                    .flat_map(|&p| <[_; 2]>::from(p))
                    .any(|x| x == i32::MAX || x == i32::MIN)
            );
        }

        Ok(())
    }

    /// Rotate a subcircuit device.
    #[test]
    fn rotate_subcircuit_device() {
        crate::tests::init();

        let out = apply_op_on_main_circuit(
            indoc! { r#"circuit main {
                std.resistor r x=-2 y=0
                node n x=4 y=0
                - r.- n
            }"# },
            &mut |workspace| {
                let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
                Box::new(
                    ItemMove::new_rotate(
                        &workspace.world,
                        vec![ent_device],
                        layout::Angle::_90.into(),
                    )
                    .expect("new_rotate"),
                )
            },
        );
        assert_snapshot!(out, @r#"
        circuit main {
            std.resistor r x=1 y=-2 angle=90
            node n x=4 y=0
            - r.- n
        }
        "#);
    }

    /// Rotate a wire device.
    #[test]
    fn rotate_wire_device() {
        crate::tests::init();

        let out = apply_op_on_main_circuit(
            indoc! { r#"circuit main {
                node n1 x=1 y=0
                node n2 x=6 y=3
                - n1 n2
            }"# },
            &mut |workspace| {
                let ent_n1 = workspace.main_circuit_item_by_id("n1").unwrap();
                let ent_n2 = workspace.main_circuit_item_by_id("n2").unwrap();
                Box::new(
                    ItemMove::new_rotate(
                        &workspace.world,
                        vec![ent_n1, ent_n2],
                        layout::Rotation::TRANSPOSE,
                    )
                    .expect("new_rotate"),
                )
            },
        );
        assert_snapshot!(out, @r#"
        circuit main {
            node n1 x=2 y=-1
            node n2 x=5 y=4
            - n1 n2
        }
        "#);
    }

    proptest! {
        /// Check that items remain stationary if we apply a series of
        /// [`layout::Rotation`]s that sum up to identity, using the operators
        /// returned by [`ItemMove::new_rotate`]. (This is guaranteed by its
        /// multiplicativity.)
        #[test]
        fn pt_rotate_cycle(
            positions in (-4..4_i32, -4..4_i32, 0..4_i32, 0..4_i32)
                .prop_map(|(x1, y1, xd, yd)| {
                    let p1 = IVec2::from([x1, y1]);
                    let d = IVec2::from([xd, yd]);
                    [p1, p1 + d]
                }),
            rotations in prop::collection::vec(layout::tests::any_rotation(), 0..4)
                .prop_map(|mut rotations| {
                    let sum = rotations
                        .iter()
                        .fold(layout::Rotation::empty(), |sum, &x| x * sum);
                    if !sum.is_empty() {
                        rotations.push(sum.inverse());
                    }
                    rotations
                }),
        ) {
            pt_rotate_cycle_inner(positions, rotations)?;
        }
    }

    fn pt_rotate_cycle_inner(
        positions: [IVec2; 2],
        rotations: Vec<layout::Rotation>,
    ) -> prop::test_runner::TestCaseResult {
        crate::tests::init();

        let mut world = World::new();

        // Spawn non-terminal node entities
        let ent_items: Vec<Entity> = world
            .spawn_batch(positions.map(|p| (layout::Position(p), circuit::Node)))
            .collect();
        assert_eq!(ent_items.len(), positions.len());

        let mut new_positions = positions;

        let mut operator_stack = simac_operator::stack::OperatorStack::default();

        for &rotation in &rotations {
            let _span = tracing::info_span!("Applying `Rotation`", ?rotation).entered();

            // Execute the operator
            let operator = ItemMove::new_rotate(&world, ent_items.clone(), rotation).unwrap();
            tracing::debug!(?operator);
            operator_stack
                .execute(&mut world, Box::new(operator))
                .unwrap();

            // Read the new positions
            new_positions = std::array::from_fn(|i| {
                let &layout::Position(p) = world.get(ent_items[i]).unwrap();
                p
            });
            tracing::debug!(?new_positions);
        }

        // The nodes should be in the original positions
        prop_assert_eq!(new_positions, positions);

        Ok(())
    }
}
