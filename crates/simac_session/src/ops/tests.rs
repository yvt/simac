//! Test utilities for [`crate::ops`]
use bevy_ecs::prelude::*;
use simac_operator::{operator::Operator, stack::OperatorStack};

use crate::kio::test_utils::Workspace;

/// Load a circuit, apply a given operator, and return the resulting circuit in
/// a saved form.
pub(super) fn apply_op_on_main_circuit(
    kdl: &str,
    make_op: &mut dyn FnMut(&mut Workspace) -> Box<dyn Operator>,
) -> String {
    assert!(kdl.starts_with("circuit main {"));

    // Add common part
    let kdl = format!(
        "simac

workspace {{
    root main
}}

import std builtin:std

{kdl}"
    );
    tracing::trace!(kdl);

    let mut ws = Workspace::load_str(&kdl);

    let mut operator_stack = OperatorStack::default();
    crate::init_stashable_components(&mut operator_stack, &mut ws.world);
    ws.world.insert_resource(operator_stack);

    ws.world
        .run_system_cached(crate::layout::position_terminal_nodes)
        .expect("SysIdPositionTerminalNodes");

    // Apply the mode operator
    let operator = make_op(&mut ws);
    tracing::debug!(?operator, "Applying mode operator");
    ws.world
        .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
            operator_stack
                .execute(world, operator)
                .expect("mode operator failed");
        });

    // Assign `Id`s before saving
    assign_ids(&mut ws.world);

    // Save the result (apply)
    let mut kdl_out1 = ws.save_to_str();

    // Undo and redo the operator.
    tracing::debug!("Undoing and redoing");
    ws.world
        .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
            operator_stack.undo(world); // `assign_id`
            operator_stack.undo(world); // `operator`
            operator_stack.redo(world); // `operator`
            operator_stack.redo(world); // `assign_id`
        });

    // Save the result (apply+undo+redo)
    let kdl_out2 = ws.save_to_str();

    // All results should be equal
    assert_eq!(
        kdl_out1, kdl_out2,
        "resulting workspace files do not match \
        (left = apply, right = apply+undo+redo)."
    );

    // Strip common part
    let i = kdl_out1
        .find("circuit main {")
        .unwrap_or_else(|| panic!("saved file does not contain main circuit:\n{kdl_out1}"));
    kdl_out1.drain(..i);

    kdl_out1
}

/// Resolve `Id` collisions and missing `Id`s.
fn assign_ids(world: &mut World) {
    let plan = world
        .run_system_cached(crate::meta::assign_ids::sys_plan_assign_ids)
        .expect("sys_plan_assign_ids");
    let operator = Box::new(crate::ops::assign_ids::AssignIds { plan });
    world.resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
        operator_stack.execute(world, operator).expect("AssignIds")
    });
}
