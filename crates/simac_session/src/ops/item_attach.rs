//! Connect devices to the rest of the circuit by merging overlapping nodes
//!
//! # Operation
//!
//! The devices in the input set are replaced with their terminal nodes.
//!
//! For every node `n_inner` in the inner set (the input set), node `n_outer`
//! matching the following criteria are found:
//!
//! - `n_outer` is a member of the outer set (the complement of the inner set).
//!
//! - `n_inner` and `n_outer` overlap (have the same [`layout::Position`]).
//!
//! - One of `n_inner` and `n_outer` is a non-terminal node (not having
//!   [`meta::TerminalNode`]).
//!
//! If `n_outer` is a terminal node, `n_inner` is destroyed. Otherwise,
//! `n_outer` is destroyed. All connections to the destroyed node are reattached
//! to the other node.
//!
//! > This ensures that the relationship between devices and their terminal
//! > nodes ([`meta::TerminalNode`]) is preserved, and (equivalently) that the
//! > destroyed nodes never have [`meta::TerminalNode`].
//!
//! If the inner set or the outer set contains overlapping nodes, some of the
//! nodes might be excluded from consideration.
//!
//! > The assumption is that a circuit normally does not contain any overlapping
//! > nodes. Such nodes are only temporarily created by an intermediate step of
//! > a user-facing operation and subsequently eliminated by the execution of
//! > [`ItemAttach`].
use bevy_ecs::{entity::EntityHashMap, prelude::*};
use bevy_math::IVec2;
use fxhash::FxHashMap;
use simac_operator::{VirtEntity, operator};
use std::{mem::replace, sync::Arc};

use crate::{circuit, layout, meta};

/// Disconnect devices from the rest of the circuit
#[derive(Debug, Clone)]
pub struct ItemAttach {
    pub ent_circuit: Entity,
    /// The list of circuit item entities to attach. Duplicates are allowed.
    pub ent_items: Vec<Entity>,
}

/// The [`operator::OperatorUndo`] and [`operator::OperatorRedo`] implementation
/// for [`ItemAttach`].
#[derive(Debug)]
pub(super) struct ItemAttachUr {
    virt_circuit: VirtEntity,
    remove_nodes: Vec<VirtEntity>,
    reattaches: Vec<Reattach>,
    attach_points: Vec<IVec2>,
    /// The map of merged nodes: `[(old, new)]`
    merged_nodes: Vec<(VirtEntity, VirtEntity)>,
}

/// An instruction to replace a device's terminal node.
#[derive(Debug)]
struct Reattach {
    virt_device: VirtEntity,
    /// An index into [`circuit::Device::terminals`].
    terminal_i: usize,
    /// The new node to attach it to.
    virt_node: VirtEntity,
}

impl operator::Operator for ItemAttach {
    #[tracing::instrument(skip_all)]
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let Self {
            ent_circuit,
            mut ent_items,
        } = *self;
        let world = cx.world();

        let comp_circuit: &circuit::Circuit = world
            .get(ent_circuit)
            .expect("`ent_circuit` does not contain `Circuit`");

        let virt_circuit = cx.entity_to_virt(ent_circuit);

        // Replace devices with their terminals
        let mut i = 0;
        while let Some(&ent_item) = ent_items.get(i) {
            let Some(circuit::Device { terminals, .. }) = world.get(ent_item) else {
                i += 1;
                continue;
            };

            // Remove `ent_item` (device) from `ent_items`
            ent_items.swap_remove(i);

            // Add `terminals` to `ent_items`
            ent_items.extend(terminals.iter().copied());
        }

        ent_items.sort_unstable();
        ent_items.dedup();

        /// A terminal of a device.
        struct Attachment {
            ent_device: Entity,
            terminal_i: usize,
            next_attachment_i: usize,
        }

        /// Node information.
        struct Node {
            /// The first `Attachment` attached to the node.
            first_attachment_i: usize,
        }

        #[derive(Debug)]
        struct Overlap {
            ent_node_inner: Entity,
            node_inner_is_terminal: bool,
            ent_node_outer: Entity,
            node_outer_is_terminal: bool,
        }

        const NONE: usize = usize::MAX;

        let mut overlaps: FxHashMap<IVec2, Overlap> = <_>::default();

        // Find outer-inner node overlaps
        for &ent_node in &ent_items {
            let &layout::Position(position) = world
                .get(ent_node)
                .unwrap_or_else(|| panic!("{ent_node} lacks `Position`"));

            let node_inner_is_terminal = world.get::<meta::TerminalNode>(ent_node).is_some();

            // Insert `ent_node` to `overlaps`. Ignore duplicates.
            let Some(old_overlap) = overlaps.insert(
                position,
                Overlap {
                    ent_node_inner: ent_node,
                    node_inner_is_terminal,
                    // Use `ent_circuit` as a placeholder for a non-existent node
                    ent_node_outer: ent_circuit,
                    node_outer_is_terminal: false,
                },
            ) else {
                continue;
            };

            // Ignore overlaps within the inner set
            tracing::trace!(
                old_node = %old_overlap.ent_node_inner,
                new_node = %ent_node,
                "Ignoring overlapping inner node",
            );
        }

        for &ent_node in &comp_circuit.nodes {
            if ent_items.binary_search(&ent_node).is_ok() {
                continue;
            }

            let &layout::Position(position) = world
                .get(ent_node)
                .unwrap_or_else(|| panic!("{ent_node} lacks `Position`"));

            let Some(overlap) = overlaps.get_mut(&position) else { continue };

            let node_outer_is_terminal = world.get::<meta::TerminalNode>(ent_node).is_some();

            if overlap.node_inner_is_terminal && node_outer_is_terminal {
                tracing::trace!(
                    ent_node_inner = %overlap.ent_node_inner,
                    ent_node_outer = %ent_node,
                    "Ignoring this outer node because both nodes would be \
                    `TerminalNode` for this `Overlap`",
                );
                continue;
            }

            let old_node = replace(&mut overlap.ent_node_outer, ent_node);
            overlap.node_outer_is_terminal = node_outer_is_terminal;

            if old_node != ent_circuit {
                // Ignore overlaps within the inner set
                tracing::trace!(
                    %old_node,
                    new_node = %ent_node,
                    "Ignoring overlapping outer node",
                );
            }
        }

        overlaps.retain(|_, o| o.ent_node_outer != ent_circuit);

        tracing::debug!(?overlaps, "Overlapping nodes");

        // Collect all nodes we're interested in into `nodes`
        let mut nodes: EntityHashMap<Node> = overlaps
            .values()
            .flat_map(|o| [o.ent_node_inner, o.ent_node_outer])
            .map(|ent_node| {
                (
                    ent_node,
                    Node {
                        first_attachment_i: NONE,
                    },
                )
            })
            .collect();

        // Find all connections to `nodes`
        let mut attachments: Vec<Attachment> = Vec::new();
        for &ent_device in &comp_circuit.devices {
            let circuit::Device { terminals, .. } = world
                .entity(ent_device)
                .get()
                .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));

            for (terminal_i, &ent_node) in terminals.iter().enumerate() {
                let Some(node) = nodes.get_mut(&ent_node) else {
                    continue;
                };
                let attachment_i = attachments.len();
                attachments.push(Attachment {
                    ent_device,
                    terminal_i,
                    next_attachment_i: node.first_attachment_i,
                });
                node.first_attachment_i = attachment_i;
            }
        }

        // Create instructions for the operator based on the collected data
        let mut remove_nodes = Vec::with_capacity(overlaps.len());
        let mut reattaches = Vec::with_capacity(overlaps.len());
        let mut merged_nodes = Vec::with_capacity(overlaps.len());

        for &Overlap {
            ent_node_inner,
            node_inner_is_terminal: _,
            ent_node_outer,
            node_outer_is_terminal,
        } in overlaps.values()
        {
            let iter_attachments = |first_attachment_i: usize| {
                let mut cur = first_attachment_i;
                let attachments = &attachments;
                std::iter::from_fn(move || {
                    let attachment = attachments.get(cur)?;
                    cur = attachment.next_attachment_i;
                    Some(attachment)
                })
            };

            // Which node should be removed?
            let should_remove_inner = node_outer_is_terminal;

            let ent_remove_node = [ent_node_outer, ent_node_inner][should_remove_inner as usize];
            let ent_keep_node = [ent_node_outer, ent_node_inner][!should_remove_inner as usize];

            let virt_remove_node = cx.entity_to_virt(ent_remove_node);
            let virt_keep_node = cx.entity_to_virt(ent_keep_node);

            tracing::debug!(
                %ent_remove_node,
                %ent_keep_node,
                "Generating instructions to migrate connections",
            );

            reattaches.extend(
                iter_attachments(nodes[&ent_remove_node].first_attachment_i)
                    .inspect(|attachment| {
                        debug_assert!(
                            world
                                .get::<circuit::WireDevice>(attachment.ent_device)
                                .is_some(),
                            "attempted to reattach a terminal of {}, which is
                            not a wire device",
                            attachment.ent_device,
                        )
                    })
                    .map(|attachment| Reattach {
                        virt_device: cx.entity_to_virt(attachment.ent_device),
                        terminal_i: attachment.terminal_i,
                        virt_node: virt_keep_node,
                    }),
            );

            remove_nodes.push(virt_remove_node);

            merged_nodes.push((virt_remove_node, virt_keep_node));
        }

        reattaches.sort_unstable_by_key(|r| r.virt_device);

        let attach_points = overlaps.keys().copied().collect();

        tracing::debug!(?remove_nodes, ?reattaches, ?attach_points);

        Ok(operator::OperatorRedo::redo(
            Box::new(ItemAttachUr {
                virt_circuit,
                remove_nodes,
                reattaches,
                attach_points,
                merged_nodes,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for ItemAttachUr {
    fn redo(
        mut self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        let Self {
            virt_circuit,
            remove_nodes,
            reattaches,
            attach_points: _,
            merged_nodes: _,
        } = &mut *self;

        // Reattach wires to new nodes
        swap_reattach_nodes(reattaches, cx);

        // Despawn `remove_nodes`
        let ent_remove_nodes: Vec<Entity> = remove_nodes
            .iter()
            .map(|&virt_node| cx.entity_to_phys(virt_node))
            .collect();

        let ent_circuit = cx.entity_to_phys(*virt_circuit);
        let mut circuit: Mut<circuit::Circuit> = cx.world_mut().get_mut(ent_circuit).unwrap();
        let circuit = &mut *circuit;
        for ent_node in ent_remove_nodes {
            circuit.nodes.remove(&ent_node);
        }

        cx.despawn_and_stash(remove_nodes);

        self
    }
}

impl operator::OperatorUndo for ItemAttachUr {
    fn undo(
        mut self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        let Self {
            virt_circuit,
            remove_nodes,
            reattaches,
            attach_points: _,
            merged_nodes: _,
        } = &mut *self;

        // Respawn `remove_nodes`
        cx.respawn_from_stash(remove_nodes);

        let ent_remove_nodes: Vec<Entity> = remove_nodes
            .iter()
            .map(|&virt_node| cx.entity_to_phys(virt_node))
            .collect();

        let ent_circuit = cx.entity_to_phys(*virt_circuit);
        let mut circuit: Mut<circuit::Circuit> = cx.world_mut().get_mut(ent_circuit).unwrap();
        let circuit = &mut *circuit;
        circuit.nodes.extend(ent_remove_nodes);

        // Reattach wires to old nodes
        swap_reattach_nodes(reattaches, cx);

        self
    }
}

impl ItemAttachUr {
    /// Get the points at which the items included in [`ItemAttach::ent_items`]
    /// are attached the rest of the circuit.
    pub(super) fn attach_points(&self) -> &[IVec2] {
        &self.attach_points
    }

    /// Get the map of merged nodes: `[(old, new)]`
    pub(super) fn merged_nodes(&self) -> &[(VirtEntity, VirtEntity)] {
        &self.merged_nodes
    }
}

/// Swap `reattach.virt_node` and
/// `devices[reattach.virt_device].terminals[reattach.terminal_i]` for every
/// [`Reattach`].
// FIXME: Take `&mut dyn OperatorCx` when dyn upcasting is stabilized
fn swap_reattach_nodes(reattaches: &mut [Reattach], cx: &mut (impl operator::OperatorCx + ?Sized)) {
    let mut empty_terminals: Arc<[Entity]> = Arc::new([]);

    for reattaches in reattaches.chunk_by_mut(|r0, r1| r0.virt_device == r1.virt_device) {
        let virt_device = reattaches[0].virt_device;
        let ent_device = cx.entity_to_phys(virt_device);

        // Take `Device::terminals`
        let mut device: Mut<circuit::Device> = cx.world_mut().get_mut(ent_device).unwrap();
        let mut terminals_arc = replace(&mut device.terminals, empty_terminals);
        let terminals = Arc::make_mut(&mut terminals_arc);

        // Update `terminals`
        for Reattach {
            terminal_i,
            virt_node,
            ..
        } in reattaches
        {
            // Swap `terminals[*terminal_i]` and `*virt_node`
            let ent_node = cx.entity_to_phys(*virt_node);
            let ent_old_node = replace(&mut terminals[*terminal_i], ent_node);
            *virt_node = cx.entity_to_virt(ent_old_node);
        }

        // Put `Device::terminals` back
        let mut device: Mut<circuit::Device> = cx.world_mut().get_mut(ent_device).unwrap();
        empty_terminals = replace(&mut device.terminals, terminals_arc);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        kio::test_utils::Workspace,
        ops::item_detach::Target,
        tests::circuit_examples::{ExampleCircuitPath, any_example_circuit_path},
    };
    use bevy_ecs::entity::EntityHashSet;
    use itertools::{chain, enumerate};
    use proptest::prelude::*;
    use simac_operator::stack::OperatorStack;

    use crate::ops::item_detach::ItemDetach;

    #[derive(Debug, Clone, proptest_derive::Arbitrary)]
    enum DetachTarget {
        Devices {
            #[proptest(strategy = "prop::bits::u64::between(0, 50)")]
            devices_bits: u64,
            #[proptest(strategy = "prop_oneof![Just(0), prop::bits::u64::between(0, 50)]")]
            exclude_nodes_bits: u64,
        },
        DeviceTerminal {
            device_i: prop::sample::Index,
            terminal_i: prop::sample::Index,
        },
    }

    proptest! {
        /// Check that [`ItemAttach`] is no-op for example circuits regardless
        /// of parameters.
        #[test]
        fn pt_attach_noop(
            path in any_example_circuit_path(),
            remove_bits in prop::bits::u64::between(0, 50),
        ) {
            crate::tests::init();
            pt_attach_noop_inner(path, remove_bits);
        }

        /// Check that [`ItemAttach`] reverts the effect of [`ItemDetach`]
        /// for example circuits given that the same set of devices are
        /// provided as their parameters.
        #[test]
        fn pt_reattach(
            path in any_example_circuit_path(),
            detach_target: DetachTarget,
        ) {
            crate::tests::init();
            pt_reattach_inner(path, detach_target)?;
        }
    }

    fn pt_attach_noop_inner(path: ExampleCircuitPath, remove_bits: u64) {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // Update terminal node positions
        world
            .run_system_cached(layout::position_terminal_nodes)
            .unwrap();

        // List all entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_items: Vec<_> = chain!(&comp_circuit.devices, &comp_circuit.nodes)
            .copied()
            .collect();
        ent_all_items.sort_unstable();

        // List the entities to detach
        let ent_items: Vec<_> = enumerate(&ent_all_items)
            .take(remove_bits.ilog2() as usize + 1)
            .filter_map(|(i, &ent_item)| {
                prop::bits::BitSetLike::test(&remove_bits, i).then_some(ent_item)
            })
            .collect();

        // Apply the operator
        let operator = Box::new(ItemAttach {
            ent_circuit: ent_main_circuit,
            ent_items: ent_items.clone(),
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        // The operator should be no-op because the example circuits do not
        // have any overlapping nodes.
        let undo: &ItemAttachUr = operator_stack
            .undo_stack_peek()
            .unwrap()
            .as_any()
            .downcast_ref()
            .unwrap();
        tracing::debug!(?undo);

        assert!(undo.remove_nodes.is_empty());
        assert!(undo.reattaches.is_empty());
    }

    fn pt_reattach_inner(
        path: ExampleCircuitPath,
        detach_target: DetachTarget,
    ) -> prop::test_runner::TestCaseResult {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // Update terminal node positions
        world
            .run_system_cached(layout::position_terminal_nodes)
            .unwrap();

        let circuit_invariant_before = CircuitInvariant::new(&world, ent_main_circuit);

        // List all device entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_devices: Vec<_> = comp_circuit.devices.iter().copied().collect();
        ent_all_devices.sort_unstable();
        let mut ent_all_nodes: Vec<_> = comp_circuit.nodes.iter().copied().collect();
        ent_all_nodes.sort_unstable();

        // List the entities to detach
        let (detach_target, ent_attach_items) = match detach_target {
            DetachTarget::Devices {
                devices_bits,
                exclude_nodes_bits,
            } => {
                let ent_devices: Vec<_> = enumerate(&ent_all_devices)
                    .filter_map(|(i, &ent_item)| {
                        prop::bits::BitSetLike::test(&devices_bits, i).then_some(ent_item)
                    })
                    .collect();

                let ent_exclude_nodes = enumerate(&ent_all_nodes)
                    .filter_map(|(i, &ent_item)| {
                        prop::bits::BitSetLike::test(&exclude_nodes_bits, i).then_some(ent_item)
                    })
                    .collect();

                (
                    Target::Devices {
                        ent_devices: ent_devices.clone(),
                        ent_exclude_nodes,
                    },
                    ent_devices,
                )
            }
            DetachTarget::DeviceTerminal {
                device_i,
                terminal_i,
            } => {
                prop_assume!(!ent_all_devices.is_empty());
                let ent_device = *device_i.get(&ent_all_devices);
                let device = world.get::<circuit::Device>(ent_device).unwrap();
                prop_assume!(!device.terminals.is_empty());
                let terminal_i = terminal_i.index(device.terminals.len());
                (
                    Target::DeviceTerminal {
                        ent_device,
                        terminal_i,
                    },
                    vec![device.terminals[terminal_i]],
                )
            }
        };

        // Detach `ent_items`
        let operator = Box::new(ItemDetach {
            ent_circuit: ent_main_circuit,
            target: detach_target,
        });
        tracing::info!(?operator, "Applying detaching operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        // Reattach `ent_items`
        let operator = Box::new(ItemAttach {
            ent_circuit: ent_main_circuit,
            ent_items: ent_attach_items,
        });
        tracing::info!(?operator, "Applying attaching operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        // The circuit should be back to the original state
        let circuit_invariant_after = CircuitInvariant::new(&world, ent_main_circuit);
        assert_eq!(
            circuit_invariant_before,
            circuit_invariant_after,
            "operations did not preserve circuit invariants.\n\
            \n\
            -----------------------\n\
            {}
            -----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        Ok(())
    }

    /// Properties of a circuit that does not change after detaching and
    /// reattaching operations, provided that the circuit does not contain any
    /// overlapping nodes.
    #[derive(Debug, PartialEq)]
    struct CircuitInvariant {
        /// A mapping from devices to their terminal nodes.
        devices: EntityHashMap<Arc<[Entity]>>,
        nodes: EntityHashSet,
    }

    impl CircuitInvariant {
        fn new(world: &World, ent_circuit: Entity) -> Self {
            let comp_circuit: &circuit::Circuit = world.get(ent_circuit).unwrap();
            let devices = comp_circuit
                .devices
                .iter()
                .map(|&ent_device| {
                    let comp_device: &circuit::Device = world.get(ent_device).unwrap();
                    (ent_device, Arc::clone(&comp_device.terminals))
                })
                .collect();
            Self {
                devices,
                nodes: comp_circuit.nodes.clone(),
            }
        }
    }
}
