//! Apply [`AssignIdsPlan`]
use std::mem::replace;

use bevy_ecs::{prelude::*, world::Entry};
use itertools::izip;
use simac_operator::{VirtEntity, operator};

use crate::meta::{Id, assign_ids::AssignIdsPlan};

/// Apply [`AssignIdsPlan`]
#[derive(Debug, Clone)]
pub struct AssignIds {
    pub plan: AssignIdsPlan,
}

#[derive(Debug)]
struct AssignIdsUr {
    /// [`Id`] insertions and deletions
    id_assignments: Vec<(VirtEntity, Option<Id>)>,
}

impl operator::Operator for AssignIds {
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let AssignIdsPlan { new_id_assignments } = self.plan;

        let id_assignments = new_id_assignments
            .into_iter()
            .map(|(ent, id)| (cx.entity_to_virt(ent), Some(Id(id))))
            .collect();

        Ok(operator::OperatorRedo::redo(
            Box::new(AssignIdsUr { id_assignments }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for AssignIdsUr {
    fn redo(
        mut self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        self.apply(cx);
        self
    }
}

impl operator::OperatorUndo for AssignIdsUr {
    fn undo(
        mut self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        self.apply(cx);
        self
    }
}

impl AssignIdsUr {
    fn apply(&mut self, cx: &mut (impl operator::OperatorCx + ?Sized)) {
        let Self { id_assignments } = self;

        let ent_id_assignments: Vec<Entity> = id_assignments
            .iter()
            .map(|&(virt, _)| cx.entity_to_phys(virt))
            .collect();

        let world = cx.world_mut();

        for (ent, (_, p_id)) in izip!(ent_id_assignments, id_assignments) {
            // Exchange `Option<Id>` between `*p_id` and the entity
            match (world.entity_mut(ent).entry::<Id>(), p_id.take()) {
                (Entry::Occupied(entry), Some(new_id)) => {
                    *p_id = Some(replace(&mut *entry.into_mut(), new_id))
                }
                (Entry::Occupied(entry), None) => *p_id = Some(entry.take()),
                (Entry::Vacant(entry), Some(new_id)) => _ = entry.insert(new_id),
                (Entry::Vacant(_), None) => {}
            }
        }
    }
}
