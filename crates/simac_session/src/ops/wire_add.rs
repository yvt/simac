//! Add a wire and optionally attach it to the rest of the circuit
use bevy_ecs::prelude::*;
use bevy_math::IVec2;
use simac_operator::{VirtEntity, operator};

use crate::{
    circuit, layout,
    ops::item_attach::{ItemAttach, ItemAttachUr},
};

/// Add a wire and optionally attach it to the rest of the circuit
#[derive(Debug, Clone)]
pub struct WireAdd {
    pub ent_circuit: Entity,
    pub endpoints: [IVec2; 2],
    /// Should we attach the wire?
    pub attach: bool,
}

/// The [`operator::OperatorUndo`] implementation for [`WireAdd`].
#[derive(Debug)]
pub struct WireAddUndo {
    op_attach: Box<dyn operator::OperatorUndo>,
    inner: WireAddInner,
}

#[derive(Debug)]
struct WireAddRedo {
    op_attach: Box<dyn operator::OperatorRedo>,
    inner: WireAddInner,
}

#[derive(Debug)]
struct WireAddInner {
    virt_circuit: VirtEntity,
    virt_wire: VirtEntity,
    virt_nodes: [VirtEntity; 2],
    endpoints: [IVec2; 2],
}

impl operator::Operator for WireAdd {
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let Self {
            ent_circuit,
            endpoints,
            attach,
        } = *self;

        let virt_circuit = cx.entity_to_virt(ent_circuit);
        let virt_wire = cx.new_virt_entity();
        let virt_nodes = std::array::from_fn(|_| cx.new_virt_entity());

        let inner = WireAddInner {
            virt_circuit,
            virt_wire,
            virt_nodes,
            endpoints,
        };

        inner.redo(cx.as_forward_cx_mut());

        let op_attach = if attach {
            Box::new(ItemAttach {
                ent_circuit,
                ent_items: vec![cx.entity_to_phys(virt_wire)],
            })
        } else {
            simac_operator::ops::noop()
        };
        let op_attach = op_attach
            .execute(cx, None)
            .expect("`ItemDetach` aborted unexpectedly");

        Ok(Box::new(WireAddUndo { op_attach, inner }))
    }
}

impl operator::OperatorRedo for WireAddRedo {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        let Self { op_attach, inner } = *self;
        inner.redo(cx);
        let op_attach = operator::OperatorRedo::redo(op_attach, cx);
        Box::new(WireAddUndo { op_attach, inner })
    }
}

impl operator::OperatorUndo for WireAddUndo {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        let Self { op_attach, inner } = *self;
        let op_attach = operator::OperatorUndo::undo(op_attach, cx);
        inner.undo(cx);
        Box::new(WireAddRedo { op_attach, inner })
    }
}

impl WireAddInner {
    fn redo(&self, cx: &mut dyn operator::OperatorForwardCx) {
        let ent_circuit = cx.entity_to_phys(self.virt_circuit);

        // Spawn the wire and the endpoint nodes
        let world = cx.world_mut();
        let ent_nodes = self.endpoints.map(|position| {
            world
                .spawn((circuit::NodeBundle::default(), layout::Position(position)))
                .id()
        });
        let ent_wire = world
            .spawn(circuit::WireDeviceBundle {
                device: circuit::DeviceBundle {
                    device: circuit::Device {
                        terminals: ent_nodes.into(),
                    },
                },
                wire: circuit::WireDevice,
            })
            .id();
        cx.spawned(ent_nodes[0], self.virt_nodes[0]);
        cx.spawned(ent_nodes[1], self.virt_nodes[1]);
        cx.spawned(ent_wire, self.virt_wire);

        // Add the wire and the endpoint nodes to the circuit
        let mut comp_circuit: Mut<circuit::Circuit> = cx
            .world_mut()
            .get_mut(ent_circuit)
            .expect("`ent_circuit` lacks `Circuit`");
        comp_circuit.devices.insert(ent_wire);
        comp_circuit.nodes.extend(ent_nodes);
    }

    fn undo(&self, cx: &mut dyn operator::OperatorBackwardCx) {
        let ent_circuit = cx.entity_to_phys(self.virt_circuit);
        let ent_wire = cx.entity_to_phys(self.virt_wire);
        let ent_nodes = self.virt_nodes.map(|e| cx.entity_to_phys(e));

        // Remove the wire and the endpoint nodes from the circuit
        let mut comp_circuit: Mut<circuit::Circuit> = cx
            .world_mut()
            .get_mut(ent_circuit)
            .expect("`ent_circuit` lacks `Circuit`");
        assert!(comp_circuit.devices.remove(&ent_wire));
        for ent_node in ent_nodes {
            assert!(comp_circuit.nodes.remove(&ent_node));
        }

        // Despawn the wire and the endpoint nodes
        let [virt_node0, virt_node1] = self.virt_nodes;
        for virt in [self.virt_wire, virt_node0, virt_node1] {
            cx.despawn(virt);
        }
    }
}

impl WireAddUndo {
    /// Get the endpoint nodes.
    pub fn virt_wire(&self) -> VirtEntity {
        self.inner.virt_wire
    }

    /// Get the points at which the wire is attached to the circuit.
    pub fn attach_points(&self) -> &[IVec2] {
        let Some(op_attach): Option<&ItemAttachUr> = self.op_attach.as_any().downcast_ref() else {
            return &[];
        };
        op_attach.attach_points()
    }
}
