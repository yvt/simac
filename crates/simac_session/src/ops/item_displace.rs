//! Move circuit items, altering topology
//!
//! This operator executes [`ItemAttach`], [`ItemDetach`], and [`ItemMove`]
//! in succession.
//!
//! # Invertibility
//!
//! An application of [`ItemDisplace`] is considered *invertible* if its effect
//! can be reversed by applying the same operator with inverted deltas.
//!
//! The causes of losing invertibility include (but not limited to):
//!
//! - A moved node absorbs or is absorbed by an overlapping outer node.
use bevy_ecs::prelude::*;
use bevy_math::{I64Vec2, IVec2};
use simac_operator::{VirtEntity, operator, stack::VirtEntityHashMap};

use crate::{
    circuit,
    layout::{Position, Rotation},
    ops::{
        item_attach::{ItemAttach, ItemAttachUr},
        item_detach::ItemDetach,
        item_move::{ItemMove, NewRotateError, delta_to_rotate_by_center},
    },
};

/// Move circuit items, maintaining topology
#[derive(Debug, Clone)]
pub struct ItemDisplace {
    pub ent_circuit: Entity,
    pub target: Target,
    pub delta: I64Vec2,
    pub delta_rotation: Rotation,
}

#[derive(Debug, Clone)]
pub enum Target {
    /// Lists of circuit items to move. Duplicates are allowed.
    Items {
        /// Devices to detach, move, and reattach.
        ent_devices: Vec<Entity>,
        /// Non-terminal nodes to move and merge.
        /// They are NOT detached from the circuit. The wires connected to them
        /// move along with them.
        /// (No non-wire devices are connected to them by definition.)
        ent_nodes: Vec<Entity>,
    },
    /// A single wire terminal.
    WireTerminal {
        ent_device: Entity,
        terminal_i: bool,
    },
}

#[derive(Debug)]
struct ItemDisplaceRedo {
    op_detach: Box<dyn operator::OperatorRedo>,
    op_move: Box<dyn operator::OperatorRedo>,
    op_attach: Box<dyn operator::OperatorRedo>,
    virt_devices: Vec<VirtEntity>,
    virt_nodes: Vec<VirtEntity>,
}

/// The [`operator::OperatorUndo`] implementation for [`ItemDisplace`].
#[derive(Debug)]
pub struct ItemDisplaceUndo {
    op_detach: Box<dyn operator::OperatorUndo>,
    op_move: Box<dyn operator::OperatorUndo>,
    op_attach: Box<dyn operator::OperatorUndo>,
    virt_devices: Vec<VirtEntity>,
    virt_nodes: Vec<VirtEntity>,
}

impl ItemDisplace {
    /// Create an [`ItemDisplace`] to rotate specified items by their central
    /// point.
    ///
    /// This method provides multiplicativity as [`ItemMove::new_rotate`] does.
    /// However, this property is lost if the resulting operator application
    /// is not [invertible][1].
    ///
    /// If [`Target::WireTerminal`] is given, this function will fail by
    /// returning [`NewRotateError::NoValidItems`].
    ///
    /// [1]: self#invertibility
    #[tracing::instrument(skip(world, target))]
    pub fn new_rotate(
        world: &World,
        ent_circuit: Entity,
        target: Target,
        delta_rotation: Rotation,
    ) -> Result<Self, NewRotateError> {
        // Find the bounding box of the entities
        let mut position_min = IVec2::splat(i32::MAX);
        let mut position_max = IVec2::splat(i32::MIN);

        match &target {
            Target::Items {
                ent_devices,
                ent_nodes,
            } => {
                let mut check_ent = |ent_item: Entity| {
                    let Some(&Position(position)) = world.get(ent_item) else {
                        return;
                    };
                    position_min = position_min.min(position);
                    position_max = position_max.max(position);
                };

                for &ent_device in ent_devices {
                    check_ent(ent_device);

                    // Include device terminals for bounding box calculation
                    // (The origin point of a device can be very off-center)
                    let circuit::Device { terminals, .. } = world
                        .get(ent_device)
                        .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));

                    for &ent_node in terminals.iter() {
                        check_ent(ent_node);
                    }
                }

                for &ent_node in ent_nodes {
                    check_ent(ent_node);
                }
            }
            Target::WireTerminal { .. } => {}
        }

        if position_min.x > position_max.x {
            return Err(NewRotateError::NoValidItems);
        }

        let delta = delta_to_rotate_by_center(position_min, position_max, delta_rotation);

        Ok(Self {
            ent_circuit,
            target,
            delta,
            delta_rotation,
        })
    }
}

impl operator::Operator for ItemDisplace {
    #[tracing::instrument(skip_all)]
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let Self {
            ent_circuit,
            target,
            delta,
            delta_rotation,
        } = *self;

        let world = cx.world_mut();

        // All devices in `ent_items`
        let ent_devices: Vec<_> = match &target {
            Target::Items { ent_devices, .. } => ent_devices.clone(),
            Target::WireTerminal { .. } => Vec::new(),
        };

        if !ent_devices.is_empty() {
            // Ensure `TerminalNode` positions are up to date so that
            // `ItemDetach` creates nodes at the correct positions
            world
                .run_system_cached_with(
                    crate::layout::position_terminal_nodes_specific,
                    &ent_devices,
                )
                .expect("SysIdPositionTerminalNodes");
        }

        let virt_nodes: Vec<_> = match &target {
            Target::Items { ent_nodes, .. } => {
                ent_nodes.iter().map(|&e| cx.entity_to_virt(e)).collect()
            }
            Target::WireTerminal { .. } => Vec::new(),
        };

        // Detach
        // -------------------------------------------------------------------

        // Determine the target of `ItemDetach`
        let detach_target = match &target {
            Target::Items {
                ent_devices,
                ent_nodes,
            } => {
                // Detach devices included in `_ent_items`
                crate::ops::item_detach::Target::Devices {
                    ent_devices: ent_devices.clone(),
                    ent_exclude_nodes: ent_nodes.iter().copied().collect(),
                }
            }
            &Target::WireTerminal {
                ent_device,
                terminal_i,
            } => crate::ops::item_detach::Target::DeviceTerminal {
                ent_device,
                terminal_i: terminal_i as usize,
            },
        };

        // Execute `ItemDetach`
        let op_detach = Box::new(ItemDetach {
            ent_circuit,
            target: detach_target,
        });
        tracing::debug!(?op_detach);
        let op_detach = operator::Operator::execute(op_detach, cx, None)
            .expect("`ItemDetach` aborted unexpectedly");

        // Move
        // -------------------------------------------------------------------

        // List the entities to move
        let entities = match target {
            Target::Items {
                ent_devices,
                ent_nodes,
            } => {
                // - `ent_nodes`
                // - Terminals of wire devices in `ent_devices`
                // - Non-wire devices in `ent_devices`
                let mut ent_move_items = ent_nodes;
                let world = cx.world();
                ent_move_items.reserve(ent_devices.len());
                for &ent_device in &ent_devices {
                    let circuit::Device { terminals, .. } = world
                        .get(ent_device)
                        .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));
                    if world.get::<circuit::WireDevice>(ent_device).is_some() {
                        for &ent_node in terminals.iter() {
                            ent_move_items.push(ent_node);
                        }
                    } else {
                        ent_move_items.push(ent_device);
                    }
                }

                ent_move_items.into_iter().collect()
            }
            Target::WireTerminal {
                ent_device,
                terminal_i,
            } => {
                let circuit::Device { terminals, .. } = cx
                    .world()
                    .get(ent_device)
                    .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));
                vec![terminals[terminal_i as usize]]
            }
        };

        // Execute `ItemMove`
        let op_move = Box::new(ItemMove {
            entities: entities.clone(),
            delta,
            delta_rotation,
        });
        tracing::debug!(?op_move);
        let op_move = operator::Operator::execute(op_move, cx, None)
            .expect("`ItemMove` aborted unexpectedly");

        // Reposition `TerminalNode`s before executing `ItemAttach`
        let virt_devices: Vec<VirtEntity> =
            ent_devices.iter().map(|&e| cx.entity_to_virt(e)).collect();

        if !ent_devices.is_empty() {
            let world = cx.world_mut();
            world
                .run_system_cached_with(
                    crate::layout::position_terminal_nodes_specific,
                    &ent_devices,
                )
                .expect("SysIdPositionTerminalNodes");
        }

        // Reattach
        // -------------------------------------------------------------------

        // List the entities to reattach
        let ent_items = entities;

        // Execute `ItemAttach`
        let op_attach = Box::new(ItemAttach {
            ent_circuit,
            ent_items,
        });
        tracing::debug!(?op_attach);
        let op_attach = operator::Operator::execute(op_attach, cx, None)
            .expect("`ItemAttach` aborted unexpectedly");

        Ok(Box::new(ItemDisplaceUndo {
            op_detach,
            op_move,
            op_attach,
            virt_devices,
            virt_nodes,
        }))
    }
}

impl operator::OperatorRedo for ItemDisplaceRedo {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        let Self {
            op_detach,
            op_move,
            op_attach,
            virt_devices,
            virt_nodes,
        } = *self;

        let ent_devices: Vec<Entity> = virt_devices.iter().map(|&e| cx.entity_to_phys(e)).collect();

        cx.world_mut()
            .run_system_cached_with(
                crate::layout::position_terminal_nodes_specific,
                &ent_devices,
            )
            .expect("SysIdPositionTerminalNodes");

        let op_detach = operator::OperatorRedo::redo(op_detach, cx);
        let op_move = operator::OperatorRedo::redo(op_move, cx);

        cx.world_mut()
            .run_system_cached_with(
                crate::layout::position_terminal_nodes_specific,
                &ent_devices,
            )
            .expect("SysIdPositionTerminalNodes");

        let op_attach = operator::OperatorRedo::redo(op_attach, cx);

        Box::new(ItemDisplaceUndo {
            op_detach,
            op_move,
            op_attach,
            virt_devices,
            virt_nodes,
        })
    }
}

impl operator::OperatorUndo for ItemDisplaceUndo {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        let Self {
            op_detach,
            op_move,
            op_attach,
            virt_devices,
            virt_nodes,
        } = *self;

        let op_attach = operator::OperatorUndo::undo(op_attach, cx);
        let op_move = operator::OperatorUndo::undo(op_move, cx);
        let op_detach = operator::OperatorUndo::undo(op_detach, cx);

        Box::new(ItemDisplaceRedo {
            op_detach,
            op_move,
            op_attach,
            virt_devices,
            virt_nodes,
        })
    }
}

impl ItemDisplaceUndo {
    /// Get the points at which the items included in [`ItemDisplace::target`]
    /// are reattached to the circuit.
    pub fn attach_points(&self) -> &[IVec2] {
        let op_attach: &ItemAttachUr = self
            .op_attach
            .as_any()
            .downcast_ref()
            .expect("`ItemAttach` should produce `ItemAttachUr`");
        op_attach.attach_points()
    }

    /// Get the post-merge nodes corresponding to [`Target::Items::ent_nodes`].
    ///
    /// The `i`-th element contains either `ent_nodes[i]` if it still exists or
    /// the node into which `ent_nodes[i]` was merged.
    pub fn merged_nodes(
        &self,
        mut entity_to_phys: impl FnMut(VirtEntity) -> Entity,
    ) -> Vec<Entity> {
        let op_attach: &ItemAttachUr = self
            .op_attach
            .as_any()
            .downcast_ref()
            .expect("`ItemAttach` should produce `ItemAttachUr`");

        let attach_map: VirtEntityHashMap<VirtEntity> =
            op_attach.merged_nodes().iter().copied().collect();

        self.virt_nodes
            .iter()
            .map(|&e| entity_to_phys(attach_map.get(&e).copied().unwrap_or(e)))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        kio::test_utils::Workspace,
        layout, meta,
        ops::tests::apply_op_on_main_circuit,
        tests::circuit_examples::{ExampleCircuitPath, any_example_circuit_path},
    };
    use fxhash::FxHashSet;
    use indoc::indoc;
    use insta::assert_snapshot;
    use itertools::enumerate;
    use proptest::prelude::*;
    use simac_operator::stack::OperatorStack;

    #[derive(Debug, Clone, proptest_derive::Arbitrary)]
    enum PtTarget {
        Items {
            device_bits: u64,
            node_bits: u64,
        },
        WireTerminal {
            device_i: prop::sample::Index,
            terminal_i: bool,
        },
    }

    impl PtTarget {
        fn into_target(self, world: &World, ent_circuit: Entity) -> Result<Target, TestCaseError> {
            let comp_circuit: &circuit::Circuit = world.get(ent_circuit).unwrap();

            // List the entities to displace
            match self {
                PtTarget::Items {
                    device_bits,
                    node_bits,
                } => {
                    let mut ent_all_devices: Vec<_> =
                        comp_circuit.devices.iter().copied().collect();
                    ent_all_devices.sort_unstable();

                    let ent_devices: Vec<_> = enumerate(&ent_all_devices)
                        .filter_map(|(i, &ent_item)| {
                            prop::bits::BitSetLike::test(&device_bits, i).then_some(ent_item)
                        })
                        .collect();

                    let mut ent_all_nonterminal_nodes: Vec<_> = comp_circuit
                        .nodes
                        .iter()
                        .copied()
                        .filter(|&ent| world.get::<meta::TerminalNode>(ent).is_none())
                        .collect();
                    ent_all_nonterminal_nodes.sort_unstable();

                    let ent_nodes: Vec<_> = enumerate(&ent_all_nonterminal_nodes)
                        .filter_map(|(i, &ent_item)| {
                            prop::bits::BitSetLike::test(&node_bits, i).then_some(ent_item)
                        })
                        .collect();

                    Ok(Target::Items {
                        ent_devices,
                        ent_nodes,
                    })
                }
                PtTarget::WireTerminal {
                    device_i,
                    terminal_i,
                } => {
                    let mut ent_all_wires: Vec<_> = comp_circuit
                        .devices
                        .iter()
                        .copied()
                        .filter(|&e| world.get::<circuit::WireDevice>(e).is_some())
                        .collect();
                    ent_all_wires.sort_unstable();

                    prop_assume!(!ent_all_wires.is_empty());
                    let ent_device = *device_i.get(&ent_all_wires);

                    Ok(Target::WireTerminal {
                        ent_device,
                        terminal_i,
                    })
                }
            }
        }
    }

    proptest! {
        /// Check that [`ItemDisplace`] is no-op for example circuits if
        /// `delta` is a zero vector.
        #[test]
        fn pt_displace_zero_delta(
            path in any_example_circuit_path(),
            target: PtTarget,
        ) {
            crate::tests::init();
            pt_displace_zero_delta_inner(path, target)?;
        }

        /// Check that if we use [`ItemDisplace`] to move items far away
        /// (so that they do not reattach to anything), the items are detached
        /// from the rest of the circuit.
        #[test]
        fn pt_displace_detach(
            path in any_example_circuit_path(),
            remove_bits: u64,
        ) {
            crate::tests::init();
            pt_displace_detach_inner(path, remove_bits);
        }

        /// Check that if we use [`ItemDisplace`] to move items far away
        /// (so that they do not reattach to anything) and then back to the
        /// original place, the result is identical to the original state.
        #[test]
        fn pt_displace_reattach(
            path in any_example_circuit_path(),
            target: PtTarget,
            delta_rotation in layout::tests::any_rotation(),
        ) {
            crate::tests::init();
            pt_displace_reattach_inner(path, target, delta_rotation)?;
        }

        /// Check that [`ItemDisplace`] can be undone and redone correctly.
        #[test]
        fn pt_displace_undo(
            path in any_example_circuit_path(),
            target: PtTarget,
            delta in (-10..10_i32, -10..10_i32),
            delta_rotation in layout::tests::any_rotation(),
        ) {
            crate::tests::init();
            pt_displace_undo_inner(path, target, delta.into(), delta_rotation)?;
        }
    }

    fn pt_displace_zero_delta_inner(
        path: ExampleCircuitPath,
        target: PtTarget,
    ) -> prop::test_runner::TestCaseResult {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // Update terminal node positions
        world
            .run_system_cached(layout::position_terminal_nodes)
            .unwrap();

        let circuit_invariant_before =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);

        // Apply the operator
        let target = target.into_target(&world, ent_main_circuit)?;
        let operator = Box::new(ItemDisplace {
            ent_circuit: ent_main_circuit,
            target,
            delta: I64Vec2::ZERO,
            delta_rotation: Rotation::empty(),
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        // The circuit should be the same to the original state
        let circuit_invariant_after =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);
        assert_eq!(
            circuit_invariant_before,
            circuit_invariant_after,
            "operations did not preserve circuit invariants.\n\n\
            -----------------------\n{}-----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        // Undo the operator and check structural integrity
        operator_stack.undo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-undo validation failed");

        Ok(())
    }

    fn pt_displace_detach_inner(path: ExampleCircuitPath, remove_bits: u64) {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // Update terminal node positions
        world
            .run_system_cached(layout::position_terminal_nodes)
            .unwrap();

        // List all device entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_all_devices: Vec<_> = comp_circuit.devices.iter().copied().collect();
        ent_all_devices.sort_unstable();

        // List the entities to displace
        let ent_devices: Vec<_> = enumerate(&ent_all_devices)
            .filter_map(|(i, &ent_item)| {
                prop::bits::BitSetLike::test(&remove_bits, i).then_some(ent_item)
            })
            .collect();

        // Apply the operator
        let operator = Box::new(ItemDisplace {
            ent_circuit: ent_main_circuit,
            target: Target::Items {
                ent_devices: ent_devices.clone(),
                ent_nodes: Vec::new(),
            },
            delta: [100000, 0].into(),
            delta_rotation: Rotation::empty(),
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        // Check that `ent_items` and the other part of the circuit are
        // completely separated now
        let uf = world
            .run_system_cached(crate::tests::circuit_union_find::sys_union_find)
            .unwrap();

        let inner_roots: FxHashSet<usize> = ent_devices.iter().map(|&e| uf.root_i_of(e)).collect();
        let outer_roots: FxHashSet<usize> = ent_all_devices
            .iter()
            .filter(|e| ent_devices.binary_search(e).is_err())
            .map(|&e| uf.root_i_of(e))
            .collect();

        assert!(
            inner_roots.intersection(&outer_roots).next().is_none(),
            "the specified items are still connected to \
            the rest of the circuit.\n\
            \n\
            ent_items = {ent_devices:?}\n\
            \n\
            -----------------------\n\
            {}
            -----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        // Undo the operator and check structural integrity
        operator_stack.undo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-undo validation failed");
    }

    fn pt_displace_reattach_inner(
        path: ExampleCircuitPath,
        target: PtTarget,
        delta_rotation: Rotation,
    ) -> prop::test_runner::TestCaseResult {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // Update terminal node positions
        world
            .run_system_cached(layout::position_terminal_nodes)
            .unwrap();

        let circuit_invariant_before =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);

        // Apply the operator
        let target = target.into_target(&world, ent_main_circuit)?;
        let operator = Box::new(ItemDisplace {
            ent_circuit: ent_main_circuit,
            target: target.clone(),
            delta: [100000, 0].into(),
            delta_rotation,
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        // Apply the operator with opposite delta
        let operator = Box::new(ItemDisplace {
            ent_circuit: ent_main_circuit,
            target,
            delta: delta_rotation.inverse().apply([-100000, 0]).into(),
            delta_rotation: delta_rotation.inverse(),
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        // The circuit should be back to the original state
        let circuit_invariant_after =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);
        assert_eq!(
            circuit_invariant_before,
            circuit_invariant_after,
            "operations did not preserve circuit invariants.\n\n\
            -----------------------\n{}-----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        // Undo the operator and check structural integrity
        for _ in 0..2 {
            operator_stack.undo(&mut world);

            world
                .run_system_cached(crate::tests::circuit_validate::sys_validate)
                .unwrap()
                .expect("post-undo validation failed");
        }

        Ok(())
    }

    fn pt_displace_undo_inner(
        path: ExampleCircuitPath,
        target: PtTarget,
        delta: IVec2,
        delta_rotation: Rotation,
    ) -> prop::test_runner::TestCaseResult {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // Update terminal node positions
        world
            .run_system_cached(layout::position_terminal_nodes)
            .unwrap();

        let circuit_invariant_before =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);

        let target = target.into_target(&world, ent_main_circuit)?;

        // Remember the old circuit state
        let target_position_before = target_position_vec(&world, &target, None);

        // Apply the operator
        let operator = Box::new(ItemDisplace {
            ent_circuit: ent_main_circuit,
            target: target.clone(),
            delta: delta.into(),
            delta_rotation,
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        let circuit_invariant_after_exec =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);

        // The specified items should move by the correct distance
        let operator_undo: &ItemDisplaceUndo = operator_stack
            .undo_stack_peek()
            .expect("undo stack is empty")
            .as_any()
            .downcast_ref()
            .expect("top element of undo stack should be `ItemDisplaceUndo`");
        let merged_nodes = operator_undo.merged_nodes(|e| operator_stack.entity_to_phys(e));
        let target_position_after = target_position_vec(&world, &target, Some(&merged_nodes));

        let target_position_after_expected: Vec<_> = target_position_before
            .into_iter()
            .map(|x| delta_rotation.apply(x) + delta)
            .collect();

        assert_eq!(
            target_position_after, target_position_after_expected,
            "`ItemDisplace` did not move items by the correct distance",
        );

        // `ItemDisplaceUndo::attach_points` should not panic
        _ = operator_undo.attach_points();

        // Undo the operator
        operator_stack.undo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-undo validation failed");

        // The circuit should be back to the original state except that
        // items may have different `Entity`s now (which is why
        // `VirtCircuitInvariant` stores `VirtEntity` instead of `Entity`)
        let circuit_invariant_after_undo =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);
        assert_eq!(
            circuit_invariant_before,
            circuit_invariant_after_undo,
            "exec+undo did not preserve circuit invariants.\n\n\
            -----------------------\n{}-----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        // Redo the operator
        operator_stack.redo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-redo validation failed");

        // The circuit should be back to the post-execution state likewise
        let circuit_invariant_after_redo =
            VirtCircuitInvariant::new(&mut world, &operator_stack, ent_main_circuit);
        assert_eq!(
            circuit_invariant_after_exec,
            circuit_invariant_after_redo,
            "undo+redo did not preserve circuit invariants.\n\n\
            -----------------------\n{}-----------------------\n",
            world
                .run_system_cached_with(
                    crate::tests::circuit_validate::sys_print_circuit_to_string,
                    ent_main_circuit,
                )
                .unwrap(),
        );

        Ok(())
    }

    /// Properties of a circuit that does not change after detaching and
    /// reattaching operations, provided that the circuit does not contain any
    /// overlapping nodes.
    #[derive(Debug, PartialEq)]
    struct VirtCircuitInvariant {
        /// A mapping from devices to their terminal nodes.
        devices: VirtEntityHashMap<Vec<VirtEntity>>,
        nodes: VirtEntityHashMap<IVec2>,
    }

    impl VirtCircuitInvariant {
        fn new(world: &mut World, operator_stack: &OperatorStack, ent_circuit: Entity) -> Self {
            world
                .run_system_cached(layout::position_terminal_nodes)
                .unwrap();

            let to_virt = |e| operator_stack.entity_to_virt(e);
            let comp_circuit: &circuit::Circuit = world.get(ent_circuit).unwrap();
            let devices = comp_circuit
                .devices
                .iter()
                .map(|&ent_device| {
                    let comp_device: &circuit::Device = world.get(ent_device).unwrap();
                    (
                        to_virt(ent_device),
                        comp_device.terminals.iter().copied().map(to_virt).collect(),
                    )
                })
                .collect();

            let nodes = comp_circuit
                .nodes
                .iter()
                .map(|&ent_node| {
                    let &Position(position) = world.get(ent_node).unwrap();
                    (to_virt(ent_node), position)
                })
                .collect();

            Self { devices, nodes }
        }
    }

    /// Get a [`Vec`] containing the positions of items referred to by a given
    /// [`Target`].
    ///
    /// If `merged_nodes` is set, [`Target::Items::ent_nodes`] is substituted
    /// with this.
    fn target_position_vec(
        world: &World,
        target: &Target,
        merged_nodes: Option<&[Entity]>,
    ) -> Vec<IVec2> {
        let mut out = Vec::new();
        let mut push_position_of = |e: Entity| {
            let &Position(position) = world
                .get(e)
                .unwrap_or_else(|| panic!("{e} lacks `Position`"));
            out.push(position);
        };
        match target {
            Target::Items {
                ent_devices,
                ent_nodes,
            } => {
                for &ent_device in ent_devices {
                    if world.get::<circuit::WireDevice>(ent_device).is_some() {
                        let comp_device = world.get::<circuit::Device>(ent_device).unwrap();
                        for &ent_node in comp_device.terminals.iter() {
                            push_position_of(ent_node);
                        }
                    } else {
                        push_position_of(ent_device);
                    }
                }
                for &ent_node in merged_nodes.unwrap_or(ent_nodes) {
                    push_position_of(ent_node);
                }
            }
            &Target::WireTerminal {
                ent_device,
                terminal_i,
            } => {
                let comp_device = world.get::<circuit::Device>(ent_device).unwrap();
                push_position_of(comp_device.terminals[terminal_i as usize]);
            }
        }
        out
    }

    /// Rotate a subcircuit device.
    #[test]
    fn rotate_subcircuit_device() {
        crate::tests::init();

        let out = apply_op_on_main_circuit(
            indoc! { r#"circuit main {
                std.resistor r x=-2 y=0
                node n x=4 y=0
                - r.- n
            }"# },
            &mut |workspace| {
                let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
                Box::new(
                    ItemDisplace::new_rotate(
                        &workspace.world,
                        workspace.ent_main_circuit,
                        Target::Items {
                            ent_devices: vec![ent_device],
                            ent_nodes: Vec::new(),
                        },
                        layout::Angle::_90.into(),
                    )
                    .expect("new_rotate"),
                )
            },
        );
        assert_snapshot!(out, @r#"
        circuit main {
            std.resistor r x=1 y=-2 angle=90
            node n x=4 y=0
            - node1 n
        node node1 x=3
        }
        "#);
    }
}
