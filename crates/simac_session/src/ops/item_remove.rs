//! Remove and despawn circuit items
//!
//! Every node in the input set ([`ItemRemove::ent_items`]) as well as all
//! attached wires and devices are removed.
//!
//! Every non-wire device included in the input set is removed, and all terminal
//! nodes are converted to non-terminal nodes (by removing
//! [`meta::TerminalNode`] component from them).
//! If this leaves some of the nodes orphaned (having no attached wires), they
//! are removed as well.
//!
//! Every wire device in the input set, the wire is removed.
//! If this leaves some of the former attached nodes orphaned (having no
//! attached wires), they are removed as well.
//!
//! # Complexity
//!
//! `O(comp_circuit.devices.len() + entities.len())` assuming a constant
//! number of terminals per device
use bevy_ecs::{entity::EntityHashMap, prelude::*};
use bevy_utils::hashbrown::hash_map;
use simac_operator::{VirtEntity, operator};
use std::cell::LazyCell;

use crate::{circuit, meta};

/// Remove circuit items
#[derive(Debug, Clone)]
pub struct ItemRemove {
    pub ent_circuit: Entity,
    /// The list of entities to remove. Duplicates are allowed.
    pub ent_items: Vec<Entity>,
}

#[derive(Debug)]
struct ItemRemoveUr {
    virt_circuit: VirtEntity,
    /// The entities to remove.
    ///
    /// `virt_items[..num_devices]` contains devices and
    /// `virt_items[num_devices..]` contains nodes.
    virt_items: Vec<VirtEntity>,
    num_devices: usize,
    /// The terminal nodes to convert to non-terminal nodes.
    terminal_nodes: Vec<TerminalNode>,
}

#[derive(Debug)]
struct TerminalNode {
    virt_node: VirtEntity,
    virt_device: VirtEntity,
}

impl operator::Operator for ItemRemove {
    #[tracing::instrument(skip_all)]
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let Self {
            ent_circuit,
            mut ent_items,
        } = *self;
        let world = cx.world();

        let comp_circuit: &circuit::Circuit = world
            .get(ent_circuit)
            .expect("`ent_circuit` does not contain `Circuit`");

        let virt_circuit = cx.entity_to_virt(ent_circuit);

        /// Nodes with this bit set should have all attached devices removed.
        const REMOVE_NODE_BIT: u32 = 1 << 31;
        const DEVICE: u32 = u32::MAX;
        const DEGREE_MASK: u32 = REMOVE_NODE_BIT - 1;

        // Initialize `node_degree_map` as follows:
        // - `node_degree_map[ent_device] = DEVICE`
        // - `node_degree_map[ent_node] = 0` or `REMOVE_NODE_BIT`
        //   for every node we want to track the degree of
        //
        // Simultaneously, remove all nodes from `ent_items` (leaving only
        // devices) and remove duplicates.
        let mut node_degree_map: EntityHashMap<u32> =
            EntityHashMap::with_capacity_and_hasher(ent_items.len(), Default::default());

        ent_items.retain(|&ent_item| {
            let entity_ref = world.entity(ent_item);

            if comp_circuit.devices.contains(&ent_item) {
                match node_degree_map.entry(ent_item) {
                    // Remove duplicate
                    hash_map::Entry::Occupied(_) => return false,
                    hash_map::Entry::Vacant(entry) => _ = entry.insert(DEVICE),
                }

                let circuit::Device { terminals, .. } = entity_ref
                    .get()
                    .unwrap_or_else(|| panic!("{ent_item} lacks `Device`"));

                // Track the degrees of the terminals
                for &ent_node in terminals.iter() {
                    node_degree_map.entry(ent_node).or_insert(0);
                }

                true
            } else if comp_circuit.nodes.contains(&ent_item) {
                node_degree_map.insert(ent_item, REMOVE_NODE_BIT);

                // Exclude all nodes from `ent_items` for now
                false
            } else {
                tracing::debug!(%ent_item, "This entity is not a circuit member; ignoring");
                false
            }
        });

        // Mark devices attached to marked nodes for removal by adding them to
        // `ent_items`.
        for &ent_device in &comp_circuit.devices {
            let circuit::Device { terminals, .. } = world
                .entity(ent_device)
                .get()
                .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));

            if node_degree_map.contains_key(&ent_device) {
                // Already marked for removal
                continue;
            }

            // If any of the terminals are marked as `REMOVE_NODE_BIT` ...
            let should_remove_device = terminals.iter().any(|&ent_node| {
                node_degree_map
                    .get(&ent_node)
                    .is_some_and(|&flags| flags & REMOVE_NODE_BIT != 0)
            });

            if should_remove_device {
                // Then mark this device for removal.
                ent_items.push(ent_device);

                for &ent_node in terminals.iter() {
                    node_degree_map.entry(ent_node).or_insert(0);
                }
            }
        }

        // Iterate over devices and update `node_degree_map & DEGREE_MASK`
        // - `node_degree_map[ent_node] & DEGREE_MASK = degree` where `degree`
        //   is the number of device terminals attached to `ent_node`
        for &ent_device in &comp_circuit.devices {
            let circuit::Device { terminals, .. } = world
                .entity(ent_device)
                .get()
                .unwrap_or_else(|| panic!("{ent_device} lacks `Device`"));
            for &ent_node in terminals.iter() {
                let Some(flags) = node_degree_map.get_mut(&ent_node) else {
                    continue;
                };
                *flags += 1;
            }
        }

        // Calculate the post-removal node degrees
        for &ent_device in &ent_items {
            let circuit::Device { terminals, .. } = world.get(ent_device).unwrap();

            for &ent_node in terminals.iter() {
                let p_flags = node_degree_map.get_mut(&ent_node).unwrap();
                *p_flags -= 1;
            }
        }

        // Mark orphaned nodes for removal (by appending them to `ent_items`)
        let num_devices = ent_items.len();
        for (&ent_item, &flags) in &node_degree_map {
            if flags == DEVICE || (flags & DEGREE_MASK) != 0 {
                continue;
            }

            ent_items.push(ent_item);
        }

        tracing::debug!(?ent_items);

        // Mark the surviving former terminal nodes to convert non-terminal
        // nodes
        let mut terminal_nodes = Vec::new();
        for &ent_device in &ent_items {
            let entity_ref = world.entity(ent_device);
            if entity_ref.contains::<circuit::WireDevice>() {
                continue;
            }
            let Some(circuit::Device { terminals, .. }) = entity_ref.get() else {
                continue;
            };

            let virt_device = LazyCell::new(|| cx.entity_to_virt(ent_device));

            for &ent_node in terminals.iter() {
                let flags = node_degree_map[&ent_node];
                if flags & DEGREE_MASK != 0 {
                    terminal_nodes.push(TerminalNode {
                        virt_node: cx.entity_to_virt(ent_node),
                        virt_device: *virt_device,
                    });
                }
            }
        }

        // Convert `ent_items` to virtual entities
        let virt_items = ent_items
            .into_iter()
            .map(|x| cx.entity_to_virt(x))
            .collect();

        tracing::debug!(?virt_items, ?terminal_nodes);

        Ok(operator::OperatorRedo::redo(
            Box::new(ItemRemoveUr {
                virt_circuit,
                virt_items,
                num_devices,
                terminal_nodes,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for ItemRemoveUr {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        // Remove the items from the parent circuit
        let ent_circuit = cx.entity_to_phys(self.virt_circuit);
        let ent_items: Vec<_> = self
            .virt_items
            .iter()
            .map(|&x| cx.entity_to_phys(x))
            .collect();

        {
            let mut comp_circuit: Mut<circuit::Circuit> = cx
                .world_mut()
                .get_mut(ent_circuit)
                .expect("`ent_circuit` lacks `Circuit`");

            for ent_device in &ent_items[..self.num_devices] {
                assert!(comp_circuit.devices.remove(ent_device));
            }

            for ent_node in &ent_items[self.num_devices..] {
                assert!(comp_circuit.nodes.remove(ent_node));
            }
        }

        // Convert terminal nodes to non-terminal nodes
        for &TerminalNode { virt_node, .. } in &self.terminal_nodes {
            let ent_node = cx.entity_to_phys(virt_node);
            cx.world_mut()
                .entity_mut(ent_node)
                .remove::<meta::TerminalNode>();
        }

        // Despawn the items
        cx.despawn_and_stash(&self.virt_items);

        self
    }
}

impl operator::OperatorUndo for ItemRemoveUr {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        // Respawn the items
        cx.respawn_from_stash(&self.virt_items);

        // Convert nodes back to terminal nodes
        for &TerminalNode {
            virt_node,
            virt_device,
        } in &self.terminal_nodes
        {
            let ent_node = cx.entity_to_phys(virt_node);
            let ent_device = cx.entity_to_phys(virt_device);
            cx.world_mut()
                .entity_mut(ent_node)
                .insert(meta::TerminalNode { device: ent_device });
        }

        // Add the items to the parent circuit
        let ent_circuit = cx.entity_to_phys(self.virt_circuit);
        let ent_items: Vec<_> = self
            .virt_items
            .iter()
            .map(|&x| cx.entity_to_phys(x))
            .collect();

        {
            let mut comp_circuit: Mut<circuit::Circuit> = cx
                .world_mut()
                .get_mut(ent_circuit)
                .expect("`ent_circuit` lacks `Circuit`");

            for &ent_device in &ent_items[..self.num_devices] {
                assert!(comp_circuit.devices.insert(ent_device));
            }

            for &ent_node in &ent_items[self.num_devices..] {
                assert!(comp_circuit.nodes.insert(ent_node));
            }
        }

        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        kio::test_utils::Workspace,
        tests::circuit_examples::{ExampleCircuitPath, any_example_circuit_path},
    };
    use itertools::{chain, enumerate};
    use proptest::prelude::*;
    use simac_operator::stack::OperatorStack;

    proptest! {
        #[test]
        fn pt_remove_any(
            path in any_example_circuit_path(),
            remove_bits in prop::bits::u64::between(0, 50),
        ) {
            pt_remove_any_inner(path, remove_bits);
        }
    }

    fn pt_remove_any_inner(path: ExampleCircuitPath, remove_bits: u64) {
        let Workspace {
            mut world,
            ent_main_circuit,
            ..
        } = path.load();

        let mut operator_stack = OperatorStack::default();
        crate::init_stashable_components(&mut operator_stack, &mut world);

        // List all entities in the circuit
        let comp_circuit: &circuit::Circuit = world.get(ent_main_circuit).unwrap();
        let mut ent_items: Vec<_> = chain!(&comp_circuit.devices, &comp_circuit.nodes)
            .copied()
            .collect();
        ent_items.sort_unstable();

        // List the entities to remove
        let ent_items: Vec<_> = enumerate(ent_items)
            .take(remove_bits.ilog2() as usize + 1)
            .filter_map(|(i, ent_item)| {
                prop::bits::BitSetLike::test(&remove_bits, i).then_some(ent_item)
            })
            .collect();

        let operator = Box::new(ItemRemove {
            ent_circuit: ent_main_circuit,
            ent_items,
        });
        tracing::info!(?operator, "Applying operator");
        operator_stack
            .execute(&mut world, operator)
            .expect("execute");

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-operation validation failed");

        operator_stack.undo(&mut world);

        world
            .run_system_cached(crate::tests::circuit_validate::sys_validate)
            .unwrap()
            .expect("post-undo validation failed");
    }
}
