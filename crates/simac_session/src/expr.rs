//! Expressions
use bevy_ecs::{prelude::*, query::QueryData, system::SystemParam};
use itertools::izip;
use macro_rules_attribute::derive;
use rune::{runtime::VmResult, termcolor};
use simac_operator::StashableComponent;
use std::{
    fmt::Write as _,
    sync::{Arc, OnceLock},
};

use crate::{circuit, meta};

#[cfg(test)]
mod tests;

pub(crate) fn init_world(world: &mut World) {
    world.register_required_components_with::<circuit::Circuit, SetupProgram>(|| SetupProgram {
        rune_func: None,
        param_vals: Vec::new(),
    });
    world.register_required_components_with::<circuit::Device, DeviceProgramData>(|| {
        DeviceProgramData {
            param_val_start_i: 0,
        }
    });
}

// Compiled expressions
// -------------------------------------------------------------------------

/// A compiled setup script associated with a circuit ([`circuit::Circuit`]).
/// It should be evaluated during circuit flattening.
///
/// This is registered by `init_world` as a required component of
/// [`circuit::Circuit`].
#[derive(Component)]
pub struct SetupProgram {
    rune_func: Option<rune::runtime::SyncFunction>,
    param_vals: Vec<ParamValSource>,
}

#[derive(Debug, Clone, Copy)]
enum ParamValSource {
    /// A static value.
    Static(f64),
    /// An index into [`SetupProgram::rune_func`] outputs.
    Dynamic(usize),
}

/// Per-device data referenced by [`SetupProgram`].
///
/// This is registered by `init_world` as a required component of
/// [`circuit::Device`].
#[derive(Component, Debug, Clone)]
struct DeviceProgramData {
    /// The starting index of the subslice of [`SetupProgram::param_vals`]
    /// representing this device's parameters.
    param_val_start_i: usize,
}

// Uncompiled expressions
// -------------------------------------------------------------------------

/// Uncompiled expressions for an element deivce ([`circuit::ElementDevice`]).
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct ElementDevice {
    /// The element parameters.
    pub params: Vec<Option<Expr>>,
}

/// Uncompiled expressions for a subcircuit deivce
/// ([`circuit::SubcircuitDevice`]).
#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct SubcircuitDevice {
    /// The circuit parameters.
    pub params: Vec<Option<Expr>>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    /// A constant value.
    Literal { value: f64 },
    /// A complex expression.
    Complex { source: String },
}

// Compilation
// -------------------------------------------------------------------------

/// Get the systems to compile [`SetupProgram`].
pub fn system_configs_compile() -> bevy_ecs::schedule::SystemConfigs {
    update_setup_programs.into_configs()
}

#[derive(QueryData)]
#[query_data(mutable)]
struct DeviceQueryData {
    element_device: Option<(
        Ref<'static, circuit::ElementDevice>,
        Ref<'static, ElementDevice>,
    )>,
    subcircuit_device: Option<(
        Ref<'static, circuit::SubcircuitDevice>,
        Ref<'static, SubcircuitDevice>,
    )>,
    device_program_data: &'static mut DeviceProgramData,
}
#[derive(QueryData)]
#[query_data(mutable)]
struct ChildCircuitQueryData {
    circuit: Ref<'static, circuit::Circuit>,
    meta_circuit: Option<Ref<'static, meta::CircuitMeta>>,
}

/// (Bevy System)
fn update_setup_programs(
    mut circuits: Query<(
        Entity,
        Ref<circuit::Circuit>,
        Ref<meta::CircuitMeta>,
        &mut SetupProgram,
    )>,
    child_circuits: Query<ChildCircuitQueryData>,
    mut devices: Query<DeviceQueryData>,
) {
    for (ent_circuit, circuit, meta_circuit, setup_program) in circuits.iter_mut() {
        update_setup_program(
            ent_circuit,
            circuit,
            meta_circuit,
            setup_program,
            &child_circuits,
            &mut devices,
        );
    }
}

#[tracing::instrument(skip_all, fields(?ent_circuit))]
fn update_setup_program(
    ent_circuit: Entity,
    circuit: Ref<circuit::Circuit>,
    meta_circuit: Ref<meta::CircuitMeta>,
    mut setup_program: Mut<SetupProgram>,
    child_circuits: &Query<ChildCircuitQueryData>,
    devices: &mut Query<DeviceQueryData>,
) {
    // Detect input changes
    // ------------------------------------------------------------------

    // If any of these change, recompilation is required:
    //
    //  1. `Circuit::num_params` referenced by
    //     `circuit::SubcircuitDevice::circuit` of a child device
    //     (we allocate that number of  entries in `param_vals`)
    //
    //  2. `CircuitMeta::params::default_val` referenced by
    //     `circuit::SubcircuitDevice::circuit` of a child device
    //
    //  3. `circuit::SubcircuitDevice::circuit` of a child device
    //     (follows from (1)(2))
    //
    //  4. `expr::SubcircuitDevice` of a child device
    //
    //  5. `circuit::ElementDevice::elem_ty` of a child device
    //     (we allocate `elem_ty.num_params()` entries in `param_vals`)
    //
    //  6. `expr::ElementDevice` of a child device
    //
    //  7. This circuit's `Circuit::devices`
    //
    //  8. This circuit's `CircuitMeta::params`
    //     (it contains this circuit's parameter names that `Expr`s can
    //     refer to)
    let child_circuit_changed = |ent: Entity| {
        child_circuits.get(ent).is_ok_and(|qd_child_circuit| {
            // (1)
            qd_child_circuit.circuit.is_changed()
                // (2)
                || qd_child_circuit.meta_circuit.is_some_and(|x| x.is_changed())
        })
    };

    let child_changed = circuit
        .devices
        .iter()
        .flat_map(|&ent| devices.get(ent))
        .any(|qd_device| {
            qd_device.subcircuit_device.is_some_and(
                |(cir_subcircuit_device, expr_subcircuit_device)| {
                    // (3)
                    cir_subcircuit_device.is_changed()
                        // (4)
                        || expr_subcircuit_device.is_changed()
                        // (1) || (2)
                        || child_circuit_changed(cir_subcircuit_device.circuit)
                },
            ) || qd_device.element_device.is_some_and(
                |(cir_element_device, expr_element_device)| {
                    // (5) || (6)
                    cir_element_device.is_changed() || expr_element_device.is_changed()
                },
            )
        });

    // !((7) || (8) || ...)
    if !circuit.is_changed() && !meta_circuit.is_changed() && !child_changed {
        tracing::trace!("No input changes, skipping");
        return;
    }

    // Construct script setup script
    // ------------------------------------------------------------------

    const FN_MAIN: &str = "main";

    let mut setup_source = format!("pub fn {FN_MAIN}(");

    // This circuit's parameters
    for param_def in meta_circuit.params.iter() {
        write!(setup_source, "{}, ", param_def.id).unwrap();
    }
    setup_source.push_str(") {\n");

    // Start emitting an array expression
    setup_source.push_str("  [\n");

    // Scan over `Expr`s to:
    //
    //  1. Build `SetupProgram::param_vals`
    //  2. Collect expressions to evaluate (after they are evaluated, their
    //     results are referenced by `ParamValSource::Dynamic`)
    let mut param_vals: Vec<ParamValSource> = Vec::new();
    let mut num_dynamic_exprs = 0;

    let mut push_dynamic_expr = |expr: &str| {
        let source = ParamValSource::Dynamic(num_dynamic_exprs);
        num_dynamic_exprs += 1;
        writeln!(setup_source, "    {expr},").unwrap();
        source
    };

    for &ent_device in circuit.devices.iter() {
        let _span = tracing::debug_span!(
            "Collecting the expressions associated with a device",
            ?ent_device
        )
        .entered();
        let Ok(mut qd_device) = devices.get_mut(ent_device) else { continue };

        qd_device.device_program_data.param_val_start_i = param_vals.len();

        if let Some((cir_subcircuit_device, expr_subcircuit_device)) = qd_device.subcircuit_device {
            let Ok(qd_child_circuit) = child_circuits.get(cir_subcircuit_device.circuit) else {
                tracing::trace!(
                    target_circuit = ?cir_subcircuit_device.circuit,
                    "The target circuit does not exist, skipping",
                );
                continue;
            };
            let num_params = qd_child_circuit.circuit.num_params;
            let param_defs = qd_child_circuit
                .meta_circuit
                .as_ref()
                .map(|x| &x.params[..])
                .unwrap_or(&[]);

            for i in 0..num_params {
                let expr = expr_subcircuit_device.params.get(i);

                tracing::debug!(
                    i,
                    param_def = ?param_defs.get(i),
                    ?expr,
                    "Subcircuit parameter",
                );

                let param_val_source = match expr {
                    Some(Some(Expr::Literal { value })) => ParamValSource::Static(*value),
                    Some(Some(Expr::Complex { source })) => push_dynamic_expr(source),
                    None | Some(None) => {
                        // Use default value
                        ParamValSource::Static(
                            param_defs
                                .get(i)
                                .map(|param_def| param_def.default_val)
                                .unwrap_or(0.0),
                        )
                    }
                };
                param_vals.push(param_val_source);
            }
        } else if let Some((cir_element_device, expr_element_device)) = qd_device.element_device {
            let elem_ty = cir_element_device.elem_ty;

            for i in 0..elem_ty.num_params() {
                let expr = expr_element_device.params.get(i);

                tracing::debug!(
                    i,
                    name = elem_ty.param_name(i).unwrap_or("< none >"),
                    default_val = ?elem_ty.param_default_val(i),
                    ?expr,
                    "Element parameter",
                );

                let param_val_source = match expr {
                    Some(Some(Expr::Literal { value })) => ParamValSource::Static(*value),
                    Some(Some(Expr::Complex { source })) => push_dynamic_expr(source),
                    None | Some(None) => {
                        // Use default value
                        ParamValSource::Static(elem_ty.param_default_val(i).unwrap_or(0.0))
                    }
                };
                param_vals.push(param_val_source);
            }
        } else {
            // This device does not take any `Expr`s
            tracing::trace!("Ignoring this device");
        }
    }

    setup_source.push_str("  ]\n}");
    tracing::debug!(%setup_source, "Generated circuit setup script");

    // Compile script setup script
    // ------------------------------------------------------------------

    // Prepare compilation
    let mut rune_sources = rune::Sources::new();
    rune_sources
        .insert(rune::Source::memory(&setup_source).expect("rune::Source::memory"))
        .expect("rune::Sources::insert");

    // Compile it
    let (rune_context, rune_runtime_context) = rune_context();
    let mut diagnostics = rune::Diagnostics::new();
    let unit = rune::prepare(&mut rune_sources)
        .with_context(rune_context)
        .with_diagnostics(&mut diagnostics)
        .build()
        .unwrap_or_else(|_| {
            // TODO: Handle build error gracefully
            let mut writer = termcolor::StandardStream::stderr(termcolor::ColorChoice::Auto);
            diagnostics
                .emit(&mut writer, &rune_sources)
                .expect("rune::Diagnostics::emit");
            panic!("rune build error, check stderr");
        });

    tracing::debug!(
        instructions = %fn_formats::DisplayFmt(|f| {
            let mut buf = Vec::new();
            unit.emit_instructions(
                &mut termcolor::NoColor::new(&mut buf),
                &rune_sources,
                true
            ).unwrap();
            f.write_str(&String::from_utf8_lossy(&buf))
        }),
        "Compiled circuit setup script"
    );

    // Create `Vm`
    let vm = rune::Vm::new(Arc::clone(rune_runtime_context), Arc::new(unit));

    let fn_main = vm
        .lookup_function([FN_MAIN])
        .expect("failed to look up setup main function");
    let fn_main = fn_main
        .into_sync()
        .expect("failed to convert setup main function to `SyncFunction`");

    // Store result
    // ------------------------------------------------------------------
    *setup_program = SetupProgram {
        rune_func: Some(fn_main),
        param_vals,
    };
}

/// Get the global Rune context used by this module's compilation process.
fn rune_context() -> &'static (rune::Context, Arc<rune::runtime::RuntimeContext>) {
    static VALUE: OnceLock<(rune::Context, Arc<rune::runtime::RuntimeContext>)> = OnceLock::new();
    VALUE.get_or_init(|| {
        let context = rune::Context::with_config(false).expect("rune::Context::with_config");

        let runtime_context = Arc::new(context.runtime().expect("Context::runtime"));
        (context, runtime_context)
    })
}

// Evaluation
// -------------------------------------------------------------------------

/// A working area for evaluating [`SetupProgram`].
#[derive(Default, Debug)]
pub struct Evaluator {}

/// A [`SystemParam`] used by [`Evaluator::eval_setup`]. Provides access to
/// per-device data associated with a compiled program.
#[derive(SystemParam)]
pub struct ProgramDataParam<'w, 's> {
    q_device: Query<'w, 's, &'static DeviceProgramData>,
}

pub struct SetupProgramEvaluation<'a> {
    /// The evaluated [`ParamValSource::Dynamic`] values or an empty vec if
    /// evaluation failed.
    dynamic_vals: rune::runtime::Vec,
    param_vals: &'a [ParamValSource],
    program_data: &'a ProgramDataParam<'a, 'a>,
}

/// Arguments to a circuit setup program entry point.
///
/// This type implements [`rune::runtime::Args`] and provides a variable number
/// of arguments.
struct MainArgs<'a> {
    setup_param_vals: &'a [f64],
}

impl Evaluator {
    /// Evaluate the specified [`SetupProgram`].
    pub fn eval_setup<'a>(
        &'a mut self,
        setup_program: &'a SetupProgram,
        program_data: &'a ProgramDataParam<'_, '_>,
        setup_param_vals: &[f64],
    ) -> SetupProgramEvaluation<'a> {
        let dynamic_vals = setup_program
            .rune_func
            .as_ref()
            .and_then(|func| {
                match func.call(MainArgs { setup_param_vals }) {
                    VmResult::Ok(x) => Some(x),
                    VmResult::Err(error) => {
                        // TODO: Report evaluation error properly
                        tracing::error!(
                            %error,
                            "Failed to evaluate a circuit setup program",
                        );

                        None
                    }
                }
            })
            .unwrap_or_else(rune::runtime::Vec::new);

        SetupProgramEvaluation {
            dynamic_vals,
            param_vals: &setup_program.param_vals,
            program_data,
        }
    }
}

impl MainArgs<'_> {
    fn try_for_each_value<E>(
        &self,
        mut f: impl FnMut(rune::Value) -> Result<(), E>,
    ) -> Result<(), E> {
        for &val in self.setup_param_vals {
            f(val.into())?;
        }
        Ok(())
    }
}

impl rune::runtime::Args for MainArgs<'_> {
    fn into_stack(self, stack: &mut rune::runtime::Stack) -> VmResult<()> {
        rune::vm_try!(self.try_for_each_value(|x| stack.push(x)));
        VmResult::Ok(())
    }

    fn try_into_vec(self) -> VmResult<rune::alloc::Vec<rune::Value>> {
        let mut vec = rune::vm_try!(rune::alloc::Vec::try_with_capacity(self.count()));
        rune::vm_try!(self.try_for_each_value(|x| vec.try_push(x)));
        VmResult::Ok(vec)
    }

    fn count(&self) -> usize {
        self.setup_param_vals.len()
    }
}

impl SetupProgramEvaluation<'_> {
    /// Get the evaluated parameters of the specified element device.
    pub fn copy_element_device_params_to(&self, ent_device: Entity, out: &mut [f64]) {
        self.copy_device_params_to(ent_device, out);
    }

    /// Get the evaluated parameters of the specified subcircuit device.
    pub fn copy_subcircuit_device_params_to(&self, ent_device: Entity, out: &mut [f64]) {
        self.copy_device_params_to(ent_device, out);
    }

    fn copy_device_params_to(&self, ent_device: Entity, out: &mut [f64]) {
        let device_data = self
            .program_data
            .q_device
            .get(ent_device)
            .expect("`DeviceProgramData` not found");
        for (&source, out) in izip!(&self.param_vals[device_data.param_val_start_i..], out) {
            match source {
                ParamValSource::Static(x) => *out = x,
                ParamValSource::Dynamic(i) => {
                    *out = self
                        .dynamic_vals
                        .get_value(i)
                        .into_result()
                        .unwrap_or_else(|error| {
                            tracing::warn!(
                                ?ent_device,
                                %error,
                                "Failed to export an evaluated parameter value",
                            );
                            Some(0.0)
                        })
                        // OOB likely means `dyanmic_vals` is empty
                        .unwrap_or(0.0);
                }
            }
        }
    }
}
