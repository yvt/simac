//! Rendering settings
//!
//! `simac_render` would be a more suitable place, but we define it in this
//! crate anyway so that [`crate::kio`] can serialize the components.
use bevy_ecs::prelude::*;
use macro_rules_attribute::derive;
use simac_operator::StashableComponent;

#[derive(Component, StashableComponent!, Debug, Clone)]
pub struct RenderOpts {
    /// The maximum absolute value for electric potential visualization.
    pub potential_color_range: f64,
    /// The animation speed for electric current.
    pub current_speed: f64,
}

impl Default for RenderOpts {
    fn default() -> Self {
        Self {
            potential_color_range: Self::DEFAULT_POTENTIAL_COLOR_RANGE,
            current_speed: Self::DEFAULT_CURRENT_SPEED,
        }
    }
}

impl RenderOpts {
    pub const DEFAULT_POTENTIAL_COLOR_RANGE: f64 = 3.0;
    pub const DEFAULT_CURRENT_SPEED: f64 = 5.0e+5;
}
