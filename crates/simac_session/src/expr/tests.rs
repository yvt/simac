use approx::assert_abs_diff_eq;
use bevy_ecs::system::RunSystemOnce;
use macro_rules_attribute::derive;

use super::*;

#[test]
fn eval_simple_missing() {
    // If `Expr` is missing, it's substituted with `ParamDefMeta::default_val`
    eval_simple_common(None, |_| 333.0);
}

#[test]
fn eval_simple_literal() {
    eval_simple_common(Some(Expr::Literal { value: 42.0 }), |_| 42.0);
}

#[test]
fn eval_simple_complex() {
    eval_simple_common(
        Some(Expr::Complex {
            source: "param0 + param1".to_owned(),
        }),
        |input| input.param0 + input.param1,
    );
}

#[test]
fn eval_simple_erroneous() {
    eval_simple_common(
        Some(Expr::Complex {
            source: "param0.this_field_does_not_exist".to_owned(),
        }),
        // Erroneous dynamic values are replaced with `0.`
        // FIXME: Would be nice if they were replaced with `default_val`
        |_| 0.,
    );
}

#[derive(Debug, Clone, Copy)]
struct EvalSimpleInput {
    param0: f64,
    param1: f64,
}

fn eval_simple_common(expr: Option<Expr>, expected_out: fn(EvalSimpleInput) -> f64) {
    crate::tests::init();

    let mut world = World::default();

    init_world(&mut world);

    let ent_child_circuit = world
        .spawn((
            circuit::CircuitBundle {
                circuit: circuit::Circuit {
                    num_params: 1,
                    ..Default::default()
                },
            },
            meta::CircuitMeta {
                terminal_names: Vec::new(),
                params: vec![meta::ParamDefMeta {
                    id: "child_param".to_owned(),
                    description: String::new(),
                    default_val: 333.0,
                }],
            },
        ))
        .id();

    let ent_subcircuit_device = world
        .spawn((
            circuit::SubcircuitDeviceBundle {
                device: circuit::DeviceBundle {
                    device: circuit::Device {
                        terminals: [].into(),
                    },
                },
                subcircuit_device: circuit::SubcircuitDevice {
                    circuit: ent_child_circuit,
                },
            },
            SubcircuitDevice { params: vec![expr] },
        ))
        .id();

    let ent_circuit = world
        .spawn((
            circuit::CircuitBundle {
                circuit: circuit::Circuit {
                    num_params: 1,
                    devices: [ent_subcircuit_device].into_iter().collect(),
                    ..Default::default()
                },
            },
            meta::CircuitMeta {
                terminal_names: Vec::new(),
                params: vec![
                    meta::ParamDefMeta {
                        id: "param0".to_owned(),
                        description: String::new(),
                        default_val: 12345.0,
                    },
                    meta::ParamDefMeta {
                        id: "param1".to_owned(),
                        description: String::new(),
                        default_val: 54321.0,
                    },
                ],
            },
        ))
        .id();

    // Compile the circuit setup program
    let mut schedule = Schedule::default();
    schedule.add_systems(system_configs_compile());
    schedule.run(&mut world);

    for inputs in [(0.0, 0.0), (1.0, 1.0), (4.0, 2.0)]
        .into_iter()
        .map(|x| EvalSimpleInput {
            param0: x.0,
            param1: x.1,
        })
    {
        // Evaluate it
        let mut evaluator = Evaluator::default();
        let out = world
            .run_system_once(
                move |q_setup_program: Query<&SetupProgram>,
                      program_data: ProgramDataParam<'_, '_>| {
                    let setup_program = q_setup_program.get(ent_circuit).unwrap();
                    let evaluation = evaluator.eval_setup(
                        setup_program,
                        &program_data,
                        &[inputs.param0, inputs.param1],
                    );
                    let mut out = [0.0];
                    evaluation.copy_subcircuit_device_params_to(ent_subcircuit_device, &mut out);
                    out[0]
                },
            )
            .unwrap();

        assert_abs_diff_eq!(out, expected_out(inputs), epsilon = 1e-3);
    }
}
