//! Picking an element by a pointer device
use bevy_ecs::prelude::*;
use bevy_math::{
    Dir2, IVec2, Rect, Vec2,
    bounding::{Aabb2d, IntersectsVolume as _, RayCast2d},
};
use itertools::izip;
use simac_session::{circuit, layout, meta};

use crate::draw::ViewParam;

/// Specifies an interactive element that can be picked by a pointer device.
#[derive(Debug, Clone, PartialEq)]
pub enum HotSpot {
    /// A device. The possible actions include:
    ///
    /// - Select the device
    /// - Grab and translate the device
    /// - Perform a device-specific action
    /// - Subdivide a wire device (and draw another wire from there)
    Device { ent_device: Entity },
    /// A node attached to a wire. The possible actions include:
    ///
    /// - Grab and translate the node
    /// - Attach the wire to a node
    /// - Detach the wire from the node
    /// - Draw another wire from this node
    WireTerminalNode {
        ent_device: Entity,
        terminal_i: bool,
        ent_node: Entity,
    },
    /// A terminal node that is not attached to any wires. The possible actions
    /// include:
    ///
    /// - Same as [`Self::Device`]
    /// - Grab and rotate the device
    /// - Draw a wire from the node
    OpenTerminalNode {
        ent_device: Entity,
        terminal_i: usize,
        ent_node: Entity,
    },
    /// A non-terminal node that is not attached to any wires.
    OrphanNode { ent_node: Entity },
}

#[derive(Debug, bevy_ecs::system::SystemParam)]
pub struct FindHotSpotParam<'w, 's> {
    q_circuits: Query<'w, 's, Ref<'static, circuit::Circuit>>,
    q_circuits_with_symbol: Query<'w, 's, CircuitWithSymbolQueryData>,
    q_devices: Query<'w, 's, DeviceQueryData>,
    q_nodes: Query<'w, 's, NodeQueryData, With<circuit::Node>>,
}

#[derive(Debug, bevy_ecs::system::SystemParam)]
pub struct FindItemsInRectParam<'w, 's> {
    q_circuits: Query<'w, 's, Ref<'static, circuit::Circuit>>,
    q_circuits_with_symbol: Query<'w, 's, CircuitWithSymbolQueryData>,
    q_devices: Query<'w, 's, DeviceQueryData>,
    q_nodes: Query<'w, 's, NodeQueryData, With<circuit::Node>>,
    q_nonterminal_nodes:
        Query<'w, 's, NodeQueryData, (With<circuit::Node>, Without<meta::TerminalNode>)>,
}

/// The [`bevy_ecs::query::QueryData`] for [`find_hot_spot`] to
/// query for circuits.
#[derive(bevy_ecs::query::QueryData)]
struct CircuitWithSymbolQueryData {
    comp_circuit: Ref<'static, circuit::Circuit>,
    comp_lay_symbol: &'static layout::Symbol,
    comp_interact_symbol: &'static Symbol,
}

/// The [`bevy_ecs::query::QueryData`] for [`find_hot_spot`] to
/// query for devices.
#[derive(bevy_ecs::query::QueryData)]
struct DeviceQueryData {
    comp_device: &'static circuit::Device,
    is_wire: Has<circuit::WireDevice>,
    comp_subcircuit: Option<&'static circuit::SubcircuitDevice>,
    comp_position_rotation: Option<(&'static layout::Position, &'static layout::Rotation)>,
}

/// The [`bevy_ecs::query::QueryData`] for [`find_hot_spot`] to
/// query for nodes.
#[derive(bevy_ecs::query::QueryData)]
struct NodeQueryData {
    comp_position: &'static layout::Position,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum HotSpotCandidate {
    Node {
        distance: u32,
        detail: HotSpotNodeCandidate,
        ent_node: Entity,
    },
    Device {
        distance: u32,
        ent_device: Entity,
    },
    None,
}

/// Categorization of nodes.
///
/// More than one variant may be emitted for a node. The topmost one is chosen
/// when a node matches multiple categories.
///
/// The following table shows which instances of `Self` are emitted under
/// various cases:
///
/// | Wire attached | `Has<TerminalNode>` | `WT` | `NT` | `Any` |
/// | ------------- | ------------------- | ---- | ---- | ----- |
/// | `false`       | `false`             |      |      | x     |
/// | `true`        | `false`             | x    |      | x     |
/// | `false`       | `true`              |      | x    | x     |
/// | `true`        | `true`              | x    | x    | x     |
///
/// `WT`: [`Self::WireTerminal`],
/// `NT`: [`Self::NonwireTerminal`],
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum HotSpotNodeCandidate {
    /// A terminal of a wire device.
    WireTerminal {
        wire_distance: u32,
        ent_device: Entity,
        terminal_i: bool,
    },
    /// A terminal of a non-wire device.
    ///
    /// If the node is a terminal of both wire and non-wire devices,
    /// [`Self::WireTerminal`] takes precedence over this.
    NonwireTerminal {
        ent_device: Entity,
        terminal_i: usize,
    },
    /// Any node.
    Any,
}

/// A pre-processed [`layout::Symbol`].
#[derive(Debug, Clone, Component, Default)]
pub struct Symbol {
    /// The axis-aligned bounding box.
    aabb: [Vec2; 2],
}

const HIT_TOLERANCE: f32 = 10.0;

const CURVE_TOLERANCE: f32 = 2.0e-1;

/// Find the hot spot at the specified point.
pub fn find_hot_spot(
    param: &FindHotSpotParam,
    view_param: &ViewParam,
    point: Vec2,
) -> Option<HotSpot> {
    let comp_circuit = param
        .q_circuits
        .get(view_param.ent_circuit)
        .expect("`ent_circuit` not found");

    // Reverse-transform `point`
    let point = view_param.unproject(point);

    let tolerance = (HIT_TOLERANCE / view_param.scale).powi(2).to_bits();

    let mut best_candidate = HotSpotCandidate::None;

    let make_node_candidate = |ent_node: Entity, position: IVec2, detail: HotSpotNodeCandidate| {
        let distance = point.distance_squared(position.as_vec2()).to_bits();

        if distance < tolerance {
            HotSpotCandidate::Node {
                distance,
                detail,
                ent_node,
            }
        } else {
            HotSpotCandidate::None
        }
    };

    let mut terminal_positions: Vec<IVec2> = Vec::with_capacity(16);

    for &ent_device in comp_circuit.devices.iter() {
        let Ok(qi_device) = param.q_devices.get(ent_device) else {
            tracing::trace!(
                ?ent_device,
                "This device does not have necessary components; ignoring"
            );
            continue;
        };

        let terminals: &[Entity] = &qi_device.comp_device.terminals;
        terminal_positions.clear();
        terminal_positions.extend(terminals.iter().map(|&e| match param.q_nodes.get(e) {
            Ok(qi_node) => qi_node.comp_position.0,
            Err(_) => [i32::MAX; 2].into(),
        }));

        if qi_device.is_wire {
            let Ok(terminal_positions): Result<[IVec2; 2], _> =
                terminal_positions.as_slice().try_into()
            else {
                tracing::trace!(
                    ?ent_device,
                    "This wire device has an incorrect number of \
                    terminals; ignoring"
                );
                continue;
            };
            let terminal_positions_f = terminal_positions.map(|x| x.as_vec2());

            let length_recip = (terminal_positions_f[1] - terminal_positions_f[0]).length_recip();
            let direction = if length_recip < f32::INFINITY {
                (terminal_positions_f[1] - terminal_positions_f[0]) * length_recip
            } else {
                Vec2::new(1.0, 0.0)
            };

            // Hit-test terminals
            let dot = |v: Vec2| (v.normalize().dot(direction) + 2.0).to_bits();
            best_candidate = best_candidate.min(make_node_candidate(
                terminals[0],
                terminal_positions[0],
                HotSpotNodeCandidate::WireTerminal {
                    ent_device,
                    terminal_i: false,
                    wire_distance: dot(terminal_positions_f[0] - point),
                },
            ));
            best_candidate = best_candidate.min(make_node_candidate(
                terminals[1],
                terminal_positions[1],
                HotSpotNodeCandidate::WireTerminal {
                    ent_device,
                    terminal_i: true,
                    wire_distance: dot(point - terminal_positions_f[1]),
                },
            ));

            // Hit-test line segment
            let point_closest = if length_recip < f32::INFINITY {
                terminal_positions_f[0]
                    + (terminal_positions_f[1] - terminal_positions_f[0])
                        * (direction.dot(point - terminal_positions_f[0]) * length_recip)
                            .clamp(0.0, 1.0)
            } else {
                terminal_positions_f[0]
            };
            let distance = point.distance_squared(point_closest).to_bits();

            if distance >= tolerance {
                continue;
            }

            best_candidate = best_candidate.min(HotSpotCandidate::Device {
                distance,
                ent_device,
            });
        } else if let Some(comp_subcircuit) = qi_device.comp_subcircuit {
            // Hit-test terminals
            for (terminal_i, &ent_node, &position) in izip!(0.., terminals, &terminal_positions) {
                best_candidate = best_candidate.min(make_node_candidate(
                    ent_node,
                    position,
                    HotSpotNodeCandidate::NonwireTerminal {
                        ent_device,
                        terminal_i,
                    },
                ));
            }

            // Get the symbol info
            let Ok(qi_circuit_with_symbol) =
                param.q_circuits_with_symbol.get(comp_subcircuit.circuit)
            else {
                tracing::trace!(
                    ?ent_device,
                    ?comp_subcircuit.circuit,
                    "This subcircuit device refers to a circuit with \
                    insufficient components; ignoring"
                );
                continue;
            };

            let Some((&comp_position, &comp_rotation)) = qi_device.comp_position_rotation else {
                tracing::trace!(
                    ?ent_device,
                    "This device does not have `Position` and `Rotation` \
                        components; ignoring"
                );
                continue;
            };

            let symbol = qi_circuit_with_symbol.comp_interact_symbol;

            // Transform the symbol AABB
            let [c0, c1] = symbol
                .aabb
                .map(|corner| comp_rotation.apply(corner) + comp_position.0.as_vec2());
            let aabb = [c0.min(c1), c0.max(c1)];

            // Find the closest point on the AABB
            let point_closest = point.clamp(aabb[0], aabb[1]);
            let distance = point.distance_squared(point_closest).to_bits();

            if distance >= tolerance {
                continue;
            }

            best_candidate = best_candidate.min(HotSpotCandidate::Device {
                distance,
                ent_device,
            });
        } else {
            // TODO: Terminal device
        }
    } // for ent_device

    for &ent_node in comp_circuit.nodes.iter() {
        let Ok(qi_node) = param.q_nodes.get(ent_node) else {
            tracing::trace!(
                ?ent_node,
                "This node does not have necessary components; ignoring"
            );
            continue;
        };

        best_candidate = best_candidate.min(make_node_candidate(
            ent_node,
            qi_node.comp_position.0,
            HotSpotNodeCandidate::Any,
        ));
    }

    match best_candidate {
        HotSpotCandidate::Node {
            detail, ent_node, ..
        } => match detail {
            HotSpotNodeCandidate::WireTerminal {
                ent_device,
                terminal_i,
                ..
            } => Some(HotSpot::WireTerminalNode {
                ent_device,
                terminal_i,
                ent_node,
            }),
            HotSpotNodeCandidate::NonwireTerminal {
                ent_device,
                terminal_i,
                ..
            } => Some(HotSpot::OpenTerminalNode {
                ent_device,
                terminal_i,
                ent_node,
            }),
            HotSpotNodeCandidate::Any => Some(HotSpot::OrphanNode { ent_node }),
        },
        HotSpotCandidate::Device { ent_device, .. } => Some(HotSpot::Device { ent_device }),
        HotSpotCandidate::None => None,
    }
}

impl Symbol {
    /// Pre-process a [`layout::Symbol`] and store the result into `self`.
    pub fn update(&mut self, symbol: &layout::Symbol) {
        // Calculate the bounding box of the symbol
        let [mut aabb_min, mut aabb_max] =
            [Vec2::splat(f32::INFINITY), Vec2::splat(f32::NEG_INFINITY)];

        let mut emit_point = |point: Vec2| {
            aabb_min = aabb_min.min(point);
            aabb_max = aabb_max.max(point);
        };

        let to_f32 = |x: f64| x as f32;

        for path in &symbol.paths {
            let mut last = [0.0_f32; 2];
            for segment in &path.segments {
                match *segment {
                    svgtypes::SimplePathSegment::MoveTo { x, y }
                    | svgtypes::SimplePathSegment::LineTo { x, y } => {
                        last = [x, y].map(to_f32);
                        emit_point(last.into());
                    }
                    svgtypes::SimplePathSegment::CurveTo {
                        x1,
                        y1,
                        x2,
                        y2,
                        x,
                        y,
                    } => {
                        epaint::CubicBezierShape {
                            points: [
                                last.into(),
                                [x1, y1].map(to_f32).into(),
                                [x2, y2].map(to_f32).into(),
                                [x, y].map(to_f32).into(),
                            ],
                            closed: false,
                            // FIXME: It's wasteful to instantiate `Color32`
                            // `PathStroke` and just throw them away
                            fill: epaint::Color32::default(),
                            stroke: epaint::PathStroke::default(),
                        }
                        .for_each_flattened_with_t(CURVE_TOLERANCE, &mut |pos, _| {
                            emit_point(<[f32; 2]>::from(pos).into())
                        });
                        last = [x, y].map(to_f32);
                    }
                    svgtypes::SimplePathSegment::Quadratic { x1, y1, x, y } => {
                        epaint::QuadraticBezierShape {
                            points: [
                                last.into(),
                                [x1, y1].map(to_f32).into(),
                                [x, y].map(to_f32).into(),
                            ],
                            closed: false,
                            // FIXME: It's wasteful to instantiate `Color32`
                            // `PathStroke` and just throw them away
                            fill: epaint::Color32::default(),
                            stroke: epaint::PathStroke::default(),
                        }
                        .for_each_flattened_with_t(CURVE_TOLERANCE, &mut |pos, _| {
                            emit_point(<[f32; 2]>::from(pos).into())
                        });
                        last = [x, y].map(to_f32);
                    }
                    svgtypes::SimplePathSegment::ClosePath => {}
                }
            }
        }

        self.aabb = if aabb_min.is_finite() && aabb_max.is_finite() {
            [aabb_min, aabb_max]
        } else {
            <_>::default()
        };
    }
}

/// Find the circuit items overlapping with the specified rectangle (specified
/// by world-space coordinates).
///
/// This function returns the following entities:
///
/// - Devices
/// - Non-terminal nodes
pub(crate) fn find_items_in_rect(
    param: &FindItemsInRectParam,
    ent_circuit: Entity,
    rect: Rect,
) -> Vec<Entity> {
    let comp_circuit = param
        .q_circuits
        .get(ent_circuit)
        .expect("`ent_circuit` not found");

    let rect_aabb = Aabb2d {
        min: rect.min,
        max: rect.max,
    };

    let mut out: Vec<Entity> = Vec::new();

    for &ent_device in comp_circuit.devices.iter() {
        let Ok(qi_device) = param.q_devices.get(ent_device) else {
            tracing::trace!(
                ?ent_device,
                "This device does not have necessary components; ignoring"
            );
            continue;
        };

        if qi_device.is_wire {
            // Find the terminal positions
            let [n0, n1] = qi_device.comp_device.terminals[..] else {
                tracing::trace!(
                    ?ent_device,
                    "This wire device has an incorrect number of \
                    terminals; ignoring"
                );
                continue;
            };

            let [p0, p1] = [n0, n1].map(|e| match param.q_nodes.get(e) {
                Ok(qi_node) => qi_node.comp_position.0.as_vec2(),
                Err(_) => [0.0; 2].into(),
            });

            // Hit-test line segment
            let (dir, len) = Dir2::new_and_length(p1 - p0).unwrap_or((Dir2::X, 0.0));
            let ray = RayCast2d::new(p0, dir, len);
            tracing::trace!(?ent_device, %p0, %p1, ?ray, "Hit-testing the wire device");

            if ray.intersects(&rect_aabb) {
                out.push(ent_device);
            }
        } else if let Some(comp_subcircuit) = qi_device.comp_subcircuit {
            // Get the symbol info
            let Ok(qi_circuit_with_symbol) =
                param.q_circuits_with_symbol.get(comp_subcircuit.circuit)
            else {
                tracing::trace!(
                    ?ent_device,
                    ?comp_subcircuit.circuit,
                    "This subcircuit device refers to a circuit with \
                    insufficient components; ignoring"
                );
                continue;
            };

            let Some((&comp_position, &comp_rotation)) = qi_device.comp_position_rotation else {
                tracing::trace!(
                    ?ent_device,
                    "This device does not have `Position` and `Rotation` \
                        components; ignoring"
                );
                continue;
            };

            let symbol = qi_circuit_with_symbol.comp_interact_symbol;

            // Transform the symbol AABB
            let [c0, c1] = symbol
                .aabb
                .map(|corner| comp_rotation.apply(corner) + comp_position.0.as_vec2());
            let aabb = Aabb2d {
                min: c0.min(c1),
                max: c0.max(c1),
            };

            // Hit-test the symbol AABB
            if aabb.intersects(&rect_aabb) {
                out.push(ent_device);
            }
        } else {
            // TODO: Terminal device
        }
    }

    for &ent_node in comp_circuit.nodes.iter() {
        let Ok(qi_node) = param.q_nonterminal_nodes.get(ent_node) else {
            tracing::trace!(
                ?ent_node,
                "This node does not have necessary components; ignoring"
            );
            continue;
        };

        if rect.contains(qi_node.comp_position.0.as_vec2()) {
            out.push(ent_node);
        }
    }

    out
}

/// The Bevy system to pre-process ([`Symbol::update`]) all [`layout::Symbol`]s
/// and produce [`Symbol`].
#[tracing::instrument(skip_all)]
pub fn update_symbols(
    mut commands: Commands<'_, '_>,
    mut q_symbols: Query<(Entity, &layout::Symbol, Option<&mut Symbol>), Changed<layout::Symbol>>,
) {
    for (ent_circuit, comp_lay_symbol, comp_interact_symbol) in q_symbols.iter_mut() {
        if let Some(mut interact_symbol) = comp_interact_symbol {
            interact_symbol.update(comp_lay_symbol);
        } else {
            let mut interact_symbol = Symbol::default();
            interact_symbol.update(comp_lay_symbol);
            commands.entity(ent_circuit).insert(interact_symbol);
        }
    }
}
