//! Modes (operators in an uncommitted state, e.g., dragging devices by mouse)
use as_any_min::AsAny;
use bevy_ecs::prelude::*;
use bevy_math::Vec2;
use simac_operator::{VirtEntity, stack::EntityToPhysError};
use std::fmt;

use crate::draw::ModeVisual;

pub mod moving;
pub mod select;
pub mod wire;

#[cfg(test)]
mod tests;

/// An active mode.
pub trait Mode: AsAny + fmt::Debug + Send + Sync + 'static {
    /// Construct an operator to apply changes based on the current state of
    /// `self`.
    ///
    /// This method can be called for multiple times throughout `self`'s
    /// lifetime to preview the result of the operation under different
    /// parameters.
    fn operator(&mut self, cx: &mut dyn ModeCx) -> Box<dyn simac_operator::operator::Operator>;

    /// Generate a [`ModeVisual`] based on the current state of `self` and
    /// the execution result of the operator returned by [`Self::operator`].
    fn visual(
        &self,
        _cx: &mut dyn ModeCx,
        _undo: &dyn simac_operator::operator::OperatorUndo,
    ) -> ModeVisual {
        ModeVisual::default()
    }

    // TODO: `fn final_operator(self) -> Box<dyn Operator>`

    /// Handle a mouse move event.
    fn mouse_move(&mut self, _position: Vec2) {}

    /// Handle a change in the current modifier key state.
    fn set_modifiers(&mut self, _flags: Modifiers) {}
}

bitflags::bitflags! {
    /// Flags indicating the state of modifier keys.
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct Modifiers: u8 {
        /// The Shift key.
        const SHIFT = 1;

        /// The Control key.
        const CONTROL = 1 << 1;

        /// The Alt key.
        const ALT = 1 << 2;
    }
}

/// The interface for [`Mode`] to interrogate a [`World`].
pub trait ModeCx {
    /// Get a reference to the [`World`].
    fn world(&self) -> &World;

    /// Get te current manifestation of the provided [`VirtEntity`].
    ///
    /// May panic if `entity` is not manifested as a physical entity right now.
    #[track_caller]
    fn entity_to_phys(&self, entity: VirtEntity) -> Entity;

    /// Get te current manifestation of the provided [`VirtEntity`].
    ///
    /// May return `Err` if `entity` is not currently manifested as a physical
    /// entity.
    fn try_entity_to_phys(&self, entity: VirtEntity) -> Result<Entity, EntityToPhysError>;

    /// Get the [`VirtEntity`] representing the provided [`Entity`].
    #[track_caller]
    fn entity_to_virt(&self, entity: Entity) -> VirtEntity;
}
