use bevy_ecs::{
    component::Tick,
    entity::{EntityHashMap, EntityHashSet},
    prelude::*,
};
use bevy_math::{IVec2, Rect, Vec2};
use bevy_utils::hashbrown::hash_map;
use enum_map::enum_map;
use itertools::enumerate;
use rgb::RGB8;
use simac_session::{circuit, layout, render::RenderOpts, simulate};
use std::{
    mem::{replace, take},
    sync::Arc,
};

use crate::pick;

#[derive(Debug, Clone)]
pub struct ViewParam {
    /// The circuit to display.
    pub ent_circuit: Entity,
    /// The simulation view to get latest simulation results from.
    pub ent_sim_view: Option<Entity>,
    pub scale: f32,
    pub translate: Vec2,
}

/// The visual rendering of a mode ([`crate::modes`]).
#[derive(Debug, Clone, Default)]
pub struct ModeVisual {
    /// Active entities (devices or nodes).
    pub(crate) entities: EntityHashSet,
    /// The points at which to indicate that connections are made.
    pub(crate) attach_points: Vec<IVec2>,
    /// A rubberband selection rectangle.
    pub(crate) rubberband_rect: Option<Rect>,
}

/// A view's current interactions.
#[derive(Debug, Clone, Default)]
pub struct ViewInteract {
    /// The currently hot element.
    pub hot: Option<pick::HotSpot>,
    /// The currently selected circuit items.
    pub selected_items: Arc<EntityHashSet>,
    /// The visual rendering of an active mode ([`crate::modes`]).
    pub mode: ModeVisual,
}

#[derive(Debug, Clone, Component, Default)]
pub struct ViewState {
    last_render: Option<Tick>,
    devices: EntityHashMap<DeviceState>,
}

#[derive(Debug, Clone)]
struct DeviceState {
    offset: f32,
}

/// A frame in an unspecified rendering stage.
#[derive(Debug, Default)]
pub struct Frame {
    scale: f32,
    translate: epaint::Vec2,
    symbol_instances: Vec<SymbolInstance>,
    symbol_terminal_potentials: Vec<u8>,
    wires: Vec<Wire>,
    nodes: Vec<IVec2>,
    hot: Option<HotSpot>,
    selected_nodes: Vec<usize>,
    active_nodes: Vec<usize>,
    attach_points: Vec<IVec2>,
    rubberband_rect: Option<Rect>,
    shapes: Vec<epaint::Shape>,
    pub clipped_primitive: Vec<epaint::ClippedPrimitive>,
}

#[derive(Debug)]
struct SymbolInstance<Symbol = Option<Arc<SymbolInner>>> {
    ent_device: Entity,
    selected: bool,
    active: bool,
    symbol: Symbol,
    position: layout::Position,
    rotation: layout::Rotation,
    /// The starting index of the subslice of
    /// [`Frame::symbol_terminal_potentials`] allocated to this symbol instance.
    start_terminal_potential_i: usize,
}

#[derive(Debug)]
struct Wire {
    selected: bool,
    active: bool,
    points: [IVec2; 2],
    offset: f32,
    potential: u8,
}

#[derive(Debug, PartialEq)]
enum HotSpot {
    SymbolInstance { ent_device: Entity },
    Wire { wire_i: usize },
    Node { node_i: usize },
}

/// The system parameters for [`ViewState::update_pre`].
#[derive(Debug, bevy_ecs::system::SystemParam)]
pub struct UpdateParam<'w, 's> {
    q_sim_views: Query<'w, 's, &'static simulate::SimSubcircuitViewOutput>,
    q_circuits: Query<'w, 's, Ref<'static, circuit::Circuit>>,
    q_circuits_with_symbol: Query<'w, 's, CircuitWithSymbolQueryData>,
    q_devices: Query<'w, 's, DeviceQueryData>,
    q_nodes: Query<'w, 's, NodeQueryData>,
}

/// The [`bevy_ecs::query::QueryData`] for [`ViewState::update_pre`] to
/// query for circuits.
#[derive(bevy_ecs::query::QueryData)]
struct CircuitWithSymbolQueryData {
    comp_circuit: Ref<'static, circuit::Circuit>,
    comp_lay_symbol: &'static layout::Symbol,
    comp_draw_symbol: &'static Symbol,
}

/// The [`bevy_ecs::query::QueryData`] for [`ViewState::update_pre`] to
/// query for devices.
#[derive(bevy_ecs::query::QueryData)]
struct DeviceQueryData {
    comp_device: &'static circuit::Device,
    is_wire: Has<circuit::WireDevice>,
    comp_subcircuit: Option<&'static circuit::SubcircuitDevice>,
    comp_position_rotation: Option<(&'static layout::Position, &'static layout::Rotation)>,
}

/// The [`bevy_ecs::query::QueryData`] for [`ViewState::update_pre`] to
/// query for nodes.
#[derive(bevy_ecs::query::QueryData)]
struct NodeQueryData {
    comp_node: &'static circuit::Node,
    comp_position: &'static layout::Position,
}

/// A pre-processed [`layout::Symbol`].
#[derive(Debug, Clone, Component, Default)]
pub struct Symbol(Arc<SymbolInner>);

#[derive(Debug, Clone, Default)]
struct SymbolInner {
    paths: Vec<Path>,
}

/// A flattened [`layout::Path`]
#[derive(Debug, Clone)]
struct Path {
    points: Vec<epaint::Pos2>,
    closed: bool,
    fill: layout::FillColor,
    stroke: layout::StrokeColor,
    stroke_width: u8,
}

const STROKE_WIDTH_BASE: f32 = 2.0e-1;
const STROKE_WIDTH_HOT_EXTRA: f32 = 1.0e-1;

const NODE_DOT_SIZE: f32 = 3.0e-1;
const NODE_DOT_SIZE_HOT: f32 = 4.0e-1;
const NODE_DOT_SIZE_ATTACH: f32 = 12.0e-1;

const CURRENT_DOT_SIZE: f32 = 2.5e-1;

const STROKE_WIDTH_GIZMO: f32 = 1.0;

const CURVE_TOLERANCE: f32 = 2.0e-2;

impl ViewParam {
    #[inline]
    pub fn unproject(&self, point: Vec2) -> Vec2 {
        (point - self.translate) / self.scale
    }
}

impl ViewState {
    /// Perform the early phase of rendering by updating an existing [`Frame`]
    /// object.
    ///
    /// This phase requires access to the world data, so it's designed to run
    /// quickly.
    #[tracing::instrument(skip_all)]
    pub fn update_pre(
        &mut self,
        param: UpdateParam,
        view_param: &ViewParam,
        view_interact: &ViewInteract,
        render_opts: &RenderOpts,
        frame: &mut Frame,
        time_delta: f64,
        tick: Tick,
    ) {
        tracing::debug!(?param, ?view_param, time_delta);

        frame.translate = <[_; 2]>::from(view_param.translate).into();
        frame.scale = view_param.scale;

        // Current dot movement per ampere
        let current_offset_delta = time_delta * render_opts.current_speed;

        let qi_circuit = param
            .q_circuits
            .get(view_param.ent_circuit)
            .expect("`ent_Circuit` not found");
        if self
            .last_render
            .is_none_or(|last_render| qi_circuit.last_changed().is_newer_than(last_render, tick))
        {
            // Circuit updated, re-synchronize
            self.devices.retain(|k, _| qi_circuit.devices.contains(k));
            for &ent_device in qi_circuit.devices.iter() {
                let hash_map::Entry::Vacant(entry) = self.devices.entry(ent_device) else {
                    continue;
                };

                // FIXME: This is wasteful for non-wire deivces
                entry.insert(DeviceState { offset: 0.0 });
            }
        }

        let comp_sim_view_output = view_param
            .ent_sim_view
            .and_then(|e| param.q_sim_views.get(e).ok());

        frame.hot = None;

        // Process devices
        // ------------------------------------------------------------------
        //
        // Iterate over devices to:
        //
        // (a) Add wires (set `frame.wires`)
        // (b) Add circuit symbol instandces (set `frame.symbol_instances`
        //     and `frame.symbol_terminal_potentials`)

        // (a)
        frame.wires.clear();

        // (b)
        let mut symbol_instances: Vec<SymbolInstance<&Arc<SymbolInner>>> =
            Vec::with_capacity(frame.symbol_instances.len());

        frame.symbol_terminal_potentials.clear();

        let map_potential = {
            let factor = 128.0 / render_opts.potential_color_range;
            move |x: f64| (x * factor + 127.5) as u8
        };

        for (&ent_device, st_device) in self.devices.iter_mut() {
            let Ok(qi_device) = param.q_devices.get(ent_device) else {
                tracing::trace!(
                    ?ent_device,
                    "This device does not have necessary components; ignoring"
                );
                continue;
            };

            if qi_device.is_wire {
                // (a)
                let [ent_n0, ent_n1] = qi_device.comp_device.terminals[..] else {
                    unreachable!("wire device should have two terminals")
                };
                let [Ok(n0), Ok(n1)] = [ent_n0, ent_n1].map(|ent_node| param.q_nodes.get(ent_node))
                else {
                    tracing::trace!(
                        ?ent_device,
                        terminals = ?[ent_n0, ent_n1],
                        "This wire device refers to a node with \
                        insufficient components; ignoring"
                    );
                    continue;
                };

                let wire_i = frame.wires.len();

                if matches!(view_interact.hot, Some(pick::HotSpot::Device { ent_device: ent_device_ }) if ent_device_ == ent_device)
                {
                    frame.hot = Some(HotSpot::Wire { wire_i });
                }

                let potential = comp_sim_view_output
                    .and_then(|comp_sim_view_output| comp_sim_view_output.nodes.get(&ent_n0))
                    .copied()
                    .unwrap_or(0.0);
                let potential = map_potential(potential);

                frame.wires.push(Wire {
                    selected: view_interact.selected_items.contains(&ent_device),
                    active: view_interact.mode.entities.contains(&ent_device),
                    points: [n0.comp_position.0, n1.comp_position.0],
                    offset: st_device.offset,
                    potential,
                });

                // Update `offset`
                if let Some(comp_sim_view_output) = comp_sim_view_output {
                    if let Some(&current) = comp_sim_view_output
                        .device_vars_start
                        .get(&ent_device)
                        .and_then(|&i| comp_sim_view_output.device_vars.get(i))
                        .filter(|x| x.is_finite())
                    {
                        st_device.offset += (current * current_offset_delta) as f32;
                        st_device.offset -= st_device.offset.floor();
                    }
                }
            } else if let Some(comp_subcircuit) = qi_device.comp_subcircuit {
                // (b)
                let Ok(qi_circuit_with_symbol) =
                    param.q_circuits_with_symbol.get(comp_subcircuit.circuit)
                else {
                    tracing::trace!(
                        ?ent_device,
                        ?comp_subcircuit.circuit,
                        "This subcircuit device refers to a circuit with \
                        insufficient components; ignoring"
                    );
                    continue;
                };

                let Some((&comp_position, &comp_rotation)) = qi_device.comp_position_rotation
                else {
                    tracing::trace!(
                        ?ent_device,
                        "This device does not have `Position` and `Rotation` \
                        components; ignoring"
                    );
                    continue;
                };

                if matches!(view_interact.hot, Some(pick::HotSpot::Device { ent_device: ent_device_ }) if ent_device_ == ent_device)
                {
                    frame.hot = Some(HotSpot::SymbolInstance { ent_device });
                }

                // Map terminal potentials
                let terminals = &qi_device.comp_device.terminals;
                let terminal_potentials = terminals.iter().map(|ent_node| {
                    let Some(comp_sim_view_output) = comp_sim_view_output else {
                        return map_potential(0.0);
                    };
                    let value = comp_sim_view_output
                        .nodes
                        .get(ent_node)
                        .copied()
                        .unwrap_or(0.0);
                    map_potential(value)
                });

                let start_terminal_potential_i = frame.symbol_terminal_potentials.len();
                frame.symbol_terminal_potentials.extend(terminal_potentials);

                // Push a `SymbolInstance`
                symbol_instances.push(SymbolInstance {
                    ent_device,
                    selected: view_interact.selected_items.contains(&ent_device),
                    active: view_interact.mode.entities.contains(&ent_device),
                    symbol: &qi_circuit_with_symbol.comp_draw_symbol.0,
                    position: comp_position,
                    rotation: comp_rotation,
                    start_terminal_potential_i,
                })
            }
        }

        // (b) Group `symbol_instance` elements by `*const SymbolInner` to
        // minimize the number of `Arc` references to hold
        // (Because atomic ref-counting operations are expensive)
        symbol_instances.sort_unstable_by_key(|x| std::ptr::from_ref::<SymbolInner>(x.symbol));

        // (b) `&Arc<SymbolInner>` -> `Option<Arc<SymbolInner>>` (`None` means
        // identical to the last `Some(_)`)
        frame.symbol_instances.clear();
        frame
            .symbol_instances
            .extend(
                symbol_instances
                    .into_iter()
                    .scan(std::ptr::null(), |last_p_symbol, x| {
                        let p_symbol = std::ptr::from_ref::<SymbolInner>(x.symbol);
                        let symbol = (replace(last_p_symbol, p_symbol) != p_symbol)
                            .then(|| Arc::clone(x.symbol));
                        Some(SymbolInstance {
                            ent_device: x.ent_device,
                            selected: x.selected,
                            active: x.active,
                            symbol,
                            position: x.position,
                            rotation: x.rotation,
                            start_terminal_potential_i: x.start_terminal_potential_i,
                        })
                    }),
            );

        tracing::trace!(?frame.symbol_instances);
        tracing::trace!(?frame.wires);

        // Process nodes
        // ------------------------------------------------------------------

        // (c)
        frame.nodes.clear();
        frame.nodes.reserve(qi_circuit.nodes.len());
        frame.selected_nodes.clear();
        frame.active_nodes.clear();

        for &ent_node in qi_circuit.nodes.iter() {
            let Ok(qi_node) = param.q_nodes.get(ent_node) else {
                tracing::trace!(
                    ?ent_node,
                    "This node does not have necessary components; ignoring"
                );
                continue;
            };

            let node_i = frame.nodes.len();
            match view_interact.hot {
                Some(
                    pick::HotSpot::WireTerminalNode {
                        ent_node: ent_node_,
                        ..
                    }
                    | pick::HotSpot::OpenTerminalNode {
                        ent_node: ent_node_,
                        ..
                    }
                    | pick::HotSpot::OrphanNode {
                        ent_node: ent_node_,
                    },
                ) if ent_node_ == ent_node => frame.hot = Some(HotSpot::Node { node_i }),
                _ => {}
            }

            if view_interact.selected_items.contains(&ent_node) {
                frame.selected_nodes.push(node_i);
            }

            if view_interact.mode.entities.contains(&ent_node) {
                frame.active_nodes.push(node_i);
            }

            frame.nodes.push(qi_node.comp_position.0);
        }

        tracing::trace!(?frame.nodes);

        // Miscellaneous updates
        // ------------------------------------------------------------------

        frame.attach_points = view_interact.mode.attach_points.clone();

        frame.rubberband_rect = view_interact.mode.rubberband_rect;

        self.last_render = Some(tick);

        // To be continued in `update_post`...
    }
}

impl Frame {
    /// Perform the late phase of rendering by updating `self`.
    #[tracing::instrument(skip_all)]
    pub fn update_post(
        &mut self,
        out_tessellator: &mut epaint::Tessellator,
        clip_rect: epaint::Rect,
    ) {
        // And now the conclusion...

        // Convert everything into `Shape`s
        // ------------------------------------------------------------------
        self.shapes.clear();

        let colors = enum_map! {
            layout::Color::None => epaint::Color32::TRANSPARENT,
            layout::Color::Default => epaint::Color32::from_rgb(222, 221, 218),
            layout::Color::Red => epaint::Color32::from_rgb(192, 28, 40),
            layout::Color::Yellow => epaint::Color32::from_rgb(245, 194, 17),
            layout::Color::Green => epaint::Color32::from_rgb(38, 162, 105),
            layout::Color::Blue => epaint::Color32::from_rgb(28, 113, 216),
            layout::Color::Purple => epaint::Color32::from_rgb(145, 65, 172),
            layout::Color::Brown => epaint::Color32::from_rgb(134, 94, 60),
        };

        // Electric potential colormap
        let potential_colors = simac_colormap::POTENTIAL_LIGHT;
        let potential_color = |i: u8| {
            let RGB8 { r, g, b } = potential_colors[i as usize];
            epaint::Color32::from_rgb(r, g, b)
        };

        // Color for "selected" shapes
        let selected_color = epaint::Color32::from_rgb(120, 174, 237);

        // Color for "active" shapes
        let active_color = epaint::Color32::from_rgb(245, 194, 17);

        fn modify_color_hot(c: epaint::Color32, by: u8) -> epaint::Color32 {
            if c.a() == 0 {
                return c;
            }

            debug_assert!(c.is_opaque(), "doesn't support translucent colors");
            epaint::Color32::from_rgb(
                c.r().saturating_add(by),
                c.g().saturating_add(by),
                c.b().saturating_add(by),
            )
        }

        // Wires
        for (wire_i, wire) in enumerate(&self.wires) {
            let points = wire
                .points
                .map(|p| epaint::Pos2::new(p.x as f32, p.y as f32));
            let mut width = STROKE_WIDTH_BASE;
            let mut color = potential_color(wire.potential);
            if self.hot == Some(HotSpot::Wire { wire_i }) || wire.selected || wire.active {
                width += STROKE_WIDTH_HOT_EXTRA;
                if wire.active {
                    color = active_color;
                } else if wire.selected {
                    color = selected_color;
                } else {
                    color = modify_color_hot(color, 32);
                }
            }
            self.shapes
                .push(epaint::Shape::line_segment(points, (width, color)));
        }

        // Symbol instances
        let mut cur_symbol = None;
        for symbol_instance in &self.symbol_instances {
            if let Some(symbol) = &symbol_instance.symbol {
                cur_symbol = Some(symbol);
            }
            let cur_symbol = cur_symbol.expect("no active symbol");

            let terminal_potentials =
                &self.symbol_terminal_potentials[symbol_instance.start_terminal_potential_i..];

            let start_i = self.shapes.len();
            for path in &cur_symbol.paths {
                let width = path.stroke_width as f32 * STROKE_WIDTH_BASE;

                let fill = match path.fill {
                    layout::FillColor::Const(color) => colors[color],
                    layout::FillColor::Potential(terminal_i) => {
                        potential_color(terminal_potentials[terminal_i])
                    }
                };

                let stroke = match path.stroke {
                    layout::StrokeColor::Const(color) => {
                        epaint::PathStroke::new(width, colors[color])
                    }
                    layout::StrokeColor::Potential([terminal_i0, terminal_i1])
                        if terminal_i0 == terminal_i1 =>
                    {
                        let color = potential_color(terminal_potentials[terminal_i0]);
                        epaint::PathStroke::new(width, color)
                    }
                    layout::StrokeColor::Potential([terminal_i0, terminal_i1]) => {
                        let color0 = terminal_potentials[terminal_i0];
                        let color1 = terminal_potentials[terminal_i1];

                        let callback = move |f| {
                            let f = (f * 256.0) as u32;
                            let color = u32::from(color0) * (256 - f) + u32::from(color1) * f;
                            potential_color((color >> 8) as u8)
                        };

                        epaint::PathStroke::new_uv(
                            width,
                            gradient_along_stroke(&path.points, symbol_instance.rotation, callback),
                        )
                    }
                };

                self.shapes.push(epaint::Shape::Path(epaint::PathShape {
                    points: path.points.clone(),
                    closed: path.closed,
                    fill,
                    stroke,
                }));
            }

            for shape in &mut self.shapes[start_i..] {
                let epaint::Shape::Path(shape) = shape else { unreachable!() };
                for point in &mut shape.points {
                    *point = symbol_instance.rotation.apply(*point)
                        + epaint::Vec2::from(
                            <[_; 2]>::from(symbol_instance.position.0).map(|x| x as f32),
                        );
                }
            }

            if self.hot
                == Some(HotSpot::SymbolInstance {
                    ent_device: symbol_instance.ent_device,
                })
                || symbol_instance.selected
                || symbol_instance.active
            {
                for shape in &mut self.shapes[start_i..] {
                    let epaint::Shape::Path(shape) = shape else { unreachable!() };

                    // Make a highlighted device brighter
                    let stroke_is_opaque = match shape.stroke.color {
                        epaint::ColorMode::Solid(color) => color.a() != 0,
                        epaint::ColorMode::UV(_) => true,
                    };

                    if symbol_instance.active {
                        if shape.fill.a() != 0 {
                            shape.fill = active_color;
                        }
                        if stroke_is_opaque {
                            shape.stroke.color = epaint::ColorMode::Solid(active_color);
                        }
                    } else if symbol_instance.selected {
                        if shape.fill.a() != 0 {
                            shape.fill = selected_color;
                        }
                        if stroke_is_opaque {
                            shape.stroke.color = epaint::ColorMode::Solid(selected_color);
                        }
                    } else {
                        shape.fill = modify_color_hot(shape.fill, 32);
                        shape.stroke.color = match take(&mut shape.stroke.color) {
                            epaint::ColorMode::Solid(color) => {
                                epaint::ColorMode::Solid(modify_color_hot(color, 32))
                            }
                            epaint::ColorMode::UV(arc) => {
                                epaint::ColorMode::UV(Arc::new(move |bbox, pos| {
                                    let color = arc(bbox, pos);
                                    modify_color_hot(color, 32)
                                }))
                            }
                        };
                    }

                    // Color a highlighted device
                    if !stroke_is_opaque {
                        shape.stroke.width = STROKE_WIDTH_HOT_EXTRA;
                        shape.stroke.color = epaint::ColorMode::Solid(shape.fill);
                    } else {
                        shape.stroke.width += STROKE_WIDTH_HOT_EXTRA;
                    }
                }
            }
        }

        // Wire electric current
        for wire in &self.wires {
            let points = wire
                .points
                .map(|p| epaint::Pos2::new(p.x as f32, p.y as f32));
            // Limit the length because (1) `i += 1.0_f32` would stop
            // incrementing if `i` becomes too large (2) We'd like to avoid
            // generating too many shapes
            let len = (points[1] - points[0]).length().min(4096.0);
            if len <= 0.0 {
                continue;
            }

            // TODO: Clip the line segment by `clip_rect`

            let num_points = len / 1.5;
            let step = (points[1] - points[0]) / num_points;
            let mut cur_pos = points[0] + step * wire.offset;
            let mut cur_t = wire.offset;
            while cur_t < num_points {
                self.shapes.push(epaint::Shape::circle_filled(
                    cur_pos,
                    CURRENT_DOT_SIZE,
                    epaint::Color32::from_rgb(255, 163, 72),
                ));

                cur_pos += step;
                cur_t += 1.0;
            }
        }

        // Nodes
        let mut iter_selected_nodes = self.selected_nodes.iter().copied().peekable();
        let mut iter_active_nodes = self.active_nodes.iter().copied().peekable();
        for (node_i, p) in enumerate(&self.nodes) {
            let selected = iter_selected_nodes.peek() == Some(&node_i);
            if selected {
                iter_selected_nodes.next();
            }

            let active = iter_active_nodes.peek() == Some(&node_i);
            if active {
                iter_active_nodes.next();
            }

            let mut size = NODE_DOT_SIZE;
            let mut color = epaint::Color32::from_rgb(222, 221, 218);
            if self.hot == Some(HotSpot::Node { node_i }) || selected || active {
                size = NODE_DOT_SIZE_HOT;
                if active {
                    color = active_color;
                } else if selected {
                    color = selected_color;
                } else {
                    color = modify_color_hot(color, 32);
                }
            }
            self.shapes.push(epaint::Shape::circle_filled(
                epaint::Pos2::new(p.x as f32, p.y as f32),
                size,
                color,
            ));
        }

        // Attach points
        let color = active_color.gamma_multiply(0.3);
        for &p in &self.attach_points {
            self.shapes.push(epaint::Shape::circle_filled(
                epaint::Pos2::new(p.x as f32, p.y as f32),
                NODE_DOT_SIZE_ATTACH,
                color,
            ));
        }

        // Transform all `Shape`s
        // ------------------------------------------------------------------

        let transform = epaint::emath::TSTransform::new(self.translate, self.scale);
        for shape in &mut self.shapes {
            shape.transform(transform);
        }

        // Gizmos
        // ------------------------------------------------------------------

        let project = |x| x * self.scale + self.translate;

        if let Some(rect) = self.rubberband_rect {
            let rect =
                [rect.min, rect.max].map(|x| project(epaint::Pos2::from(<[f32; 2]>::from(x))));

            // libadwaita dark default
            let accent_color = epaint::Color32::from_rgb(120, 174, 237);

            self.shapes.push(epaint::Shape::Rect(epaint::RectShape {
                rect: epaint::Rect {
                    min: rect[0],
                    max: rect[1],
                },
                // Nautilus v47 `rubberband`
                rounding: 6.0_f32.into(),
                // The fill alpha is much lower than the original value from
                // libadwaita to compensate for linear blending
                fill: accent_color.linear_multiply(0.05),
                stroke: epaint::Stroke {
                    width: STROKE_WIDTH_GIZMO,
                    color: accent_color,
                },
                blur_width: <_>::default(),
                fill_texture_id: <_>::default(),
                uv: epaint::Rect::ZERO,
            }));
        }

        // Tessellate all `Shape`s
        // ------------------------------------------------------------------
        self.clipped_primitive.clear();
        for shape in self.shapes.drain(..) {
            out_tessellator.tessellate_clipped_shape(
                epaint::ClippedShape { clip_rect, shape },
                &mut self.clipped_primitive,
            );
        }
    }
}

impl Symbol {
    /// Pre-process a [`layout::Symbol`] and store the result into `self`.
    pub fn update(&mut self, symbol: &layout::Symbol) {
        let this = if let Some(x) = Arc::get_mut(&mut self.0) {
            x
        } else {
            self.0 = Arc::default();
            Arc::get_mut(&mut self.0).unwrap()
        };
        this.paths.clear();

        let mut points: Vec<epaint::Pos2> = Vec::new();

        let to_f32 = |x: f64| x as f32;

        for path in symbol.paths.iter() {
            let mut flush_points = |points: &mut Vec<epaint::Pos2>, closed: bool| {
                if points.len() >= 2 {
                    this.paths.push(Path {
                        points: points.clone(),
                        closed,
                        fill: path.fill,
                        stroke: path.stroke,
                        stroke_width: path.stroke_width,
                    });
                }
                points.clear();
            };

            for segment in &path.segments {
                match *segment {
                    svgtypes::SimplePathSegment::MoveTo { x, y } => {
                        flush_points(&mut points, false);
                        points.push([x, y].map(to_f32).into());
                    }
                    svgtypes::SimplePathSegment::LineTo { x, y } => {
                        points.push([x, y].map(to_f32).into());
                    }
                    svgtypes::SimplePathSegment::CurveTo {
                        x1,
                        y1,
                        x2,
                        y2,
                        x,
                        y,
                    } => {
                        let last = points.last().copied().unwrap_or_default();
                        epaint::CubicBezierShape {
                            points: [
                                last,
                                [x1, y1].map(to_f32).into(),
                                [x2, y2].map(to_f32).into(),
                                [x, y].map(to_f32).into(),
                            ],
                            closed: false,
                            // FIXME: It's wasteful to instantiate `Color32`
                            // `PathStroke` and just throw them away
                            fill: epaint::Color32::default(),
                            stroke: epaint::PathStroke::default(),
                        }
                        .for_each_flattened_with_t(CURVE_TOLERANCE, &mut |pos, _| points.push(pos));
                    }
                    svgtypes::SimplePathSegment::Quadratic { x1, y1, x, y } => {
                        let last = points.last().copied().unwrap_or_default();
                        epaint::QuadraticBezierShape {
                            points: [last, [x1, y1].map(to_f32).into(), [x, y].map(to_f32).into()],
                            closed: false,
                            // FIXME: It's wasteful to instantiate `Color32`
                            // `PathStroke` and just throw them away
                            fill: epaint::Color32::default(),
                            stroke: epaint::PathStroke::default(),
                        }
                        .for_each_flattened_with_t(CURVE_TOLERANCE, &mut |pos, _| points.push(pos));
                    }
                    svgtypes::SimplePathSegment::ClosePath => flush_points(&mut points, true),
                }
            }

            flush_points(&mut points, false);
        }
    }
}

/// The Bevy system to pre-process ([`Symbol::update`]) all [`layout::Symbol`]s
/// and produce [`Symbol`].
#[tracing::instrument(skip_all)]
pub fn update_symbols(
    mut commands: Commands<'_, '_>,
    mut q_symbols: Query<(Entity, &layout::Symbol, Option<&mut Symbol>), Changed<layout::Symbol>>,
) {
    for (ent_circuit, comp_lay_symbol, comp_draw_symbol) in q_symbols.iter_mut() {
        if let Some(mut draw_symbol) = comp_draw_symbol {
            draw_symbol.update(comp_lay_symbol);
        } else {
            let mut draw_symbol = Symbol::default();
            draw_symbol.update(comp_lay_symbol);
            commands.entity(ent_circuit).insert(draw_symbol);
        }
    }
}

/// Create a callback closure for [`epaint::PathStroke::new_uv`] to paint the
/// stroke with a gradient (approximately) aligned with its path.
///
/// `rotation` specifies the rotation that is applied on the shape before it is
/// passed to the tessellator.
fn gradient_along_stroke(
    points: &[epaint::Pos2],
    rotation: layout::Rotation,
    sample: impl Fn(f32) -> epaint::Color32 + Send + Sync + 'static,
) -> impl Fn(epaint::Rect, epaint::Pos2) -> epaint::Color32 + Send + Sync + 'static {
    // The endpoints
    let point0 = *points.first().unwrap();
    let point1 = *points.last().unwrap();

    let [point0, point1] = [point0, point1].map(|p| rotation.apply(p));

    // The bounding box of `points`
    let local_bbox = epaint::Rect::from_points(points).expand(STROKE_WIDTH_BASE);

    let local_bbox = epaint::Rect::from_two_pos(
        rotation.apply(local_bbox.min),
        rotation.apply(local_bbox.max),
    );

    // Mapping: Local coordinates -> gradient coordinate
    //     local_to_grad(local_co) = dot(local_co, local_dir) - local_offset
    let diff = point1 - point0;
    let local_dir = diff / diff.length_sq();
    let local_offset = point0.to_vec2().dot(local_dir);

    // Mapping: UV coordinates -> gradient coordinate
    //     uv_to_grad(uv_co) = local_to_grad(uv_co * bbox.size + bbox.min)
    //                       = dot(uv_co, local_dir * bbox.size)
    //                         + dot(bbox.min, local_dir)
    //                         - local_offset
    let uv_dir = local_dir * local_bbox.size();
    let uv_offset = local_offset - local_bbox.min.to_vec2().dot(local_dir);

    move |bbox, pos| {
        let uv = (pos - bbox.min) / bbox.size();
        let grad = uv.dot(uv_dir) - uv_offset;
        sample(grad.clamp(0.0, 1.0))
    }
}
