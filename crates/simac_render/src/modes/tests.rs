use bevy_ecs::prelude::*;
use simac_operator::stack::OperatorStack;
use simac_session::kio::test_utils::Workspace;

use super::{Mode, ModeCx};

/// Load a circuit, create a [`Mode`] by calling `make_mode`, apply its mode
/// operator, and return the resulting circuit in a saved form.
#[tracing::instrument(skip_all)]
pub(crate) fn apply_mode_on_main_circuit(
    kdl: &str,
    make_mode: &mut dyn FnMut(&mut dyn ModeCx, &Workspace) -> Box<dyn Mode>,
) -> String {
    assert!(kdl.starts_with("circuit main {"));

    // Add common part
    let kdl = format!(
        "simac

workspace {{
    root main
}}

import std builtin:std

{kdl}"
    );
    tracing::trace!(kdl);

    let mut ws = Workspace::load_str(&kdl);

    let mut operator_stack = OperatorStack::default();
    simac_session::init_stashable_components(&mut operator_stack, &mut ws.world);
    ws.world.insert_resource(operator_stack);

    ws.world
        .run_system_cached(simac_session::layout::position_terminal_nodes)
        .expect("SysIdPositionTerminalNodes");

    // Create a `Mode`
    let mut cx = ModeCxImpl { world: &ws.world };
    let mut mode = make_mode(&mut cx, &ws);
    tracing::debug!(?mode);

    // Apply the mode operator
    let operator = mode.operator(&mut cx);
    tracing::debug!(?operator, "Applying mode operator");
    ws.world
        .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
            operator_stack
                .execute(world, operator)
                .expect("mode operator failed");
        });

    // Check that `Mode::visual` does not panic
    let operator_stack: &OperatorStack = ws.world.resource();
    let operator_undo = operator_stack.undo_stack_peek().unwrap();
    let mut cx = ModeCxImpl { world: &ws.world };
    let visual = mode.visual(&mut cx, operator_undo);
    tracing::debug!(?visual);

    // Assign `Id`s before saving
    assign_ids(&mut ws.world);

    // Save the result (apply)
    let mut kdl_out1 = ws.save_to_str();

    // Undo and redo the operator.
    tracing::debug!("Undoing and redoing");
    ws.world
        .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
            operator_stack.undo(world); // `assign_id`
            operator_stack.undo(world); // `operator`
            operator_stack.redo(world); // `operator`
            operator_stack.redo(world); // `assign_id`
        });

    // Save the result (apply+undo+redo)
    let kdl_out2 = ws.save_to_str();

    // Undo the operator and apply it again.
    tracing::debug!("Undoing");
    ws.world
        .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
            operator_stack.undo(world); // `assign_id`
            operator_stack.undo(world); // `operator`
        });

    let mut cx = ModeCxImpl { world: &ws.world };
    let operator = mode.operator(&mut cx);
    tracing::debug!(?operator, "Applying mode operator");
    ws.world
        .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
            operator_stack
                .execute(world, operator)
                .expect("mode operator failed");
        });
    assign_ids(&mut ws.world);

    // Save the result (apply+undo+redo+undo+apply)
    let kdl_out3 = ws.save_to_str();

    // All three results should be equal
    assert_eq!(
        kdl_out1, kdl_out2,
        "resulting workspace files do not match \
        (left = apply, right = apply+undo+redo)."
    );
    assert_eq!(
        kdl_out1, kdl_out3,
        "resulting workspace files do not match \
        (left = apply, right = apply+undo+redo+undo+apply)."
    );

    // Strip common part
    let i = kdl_out1
        .find("circuit main {")
        .unwrap_or_else(|| panic!("saved file does not contain main circuit:\n{kdl_out1}"));
    kdl_out1.drain(..i);

    kdl_out1
}

pub(super) struct ModeCxImpl<'a> {
    pub world: &'a World,
}

impl ModeCx for ModeCxImpl<'_> {
    fn world(&self) -> &World {
        self.world
    }

    fn entity_to_phys(&self, entity: simac_operator::VirtEntity) -> Entity {
        let operator_stack: &OperatorStack = self.world.resource();
        operator_stack.entity_to_phys(entity)
    }

    fn try_entity_to_phys(
        &self,
        entity: simac_operator::VirtEntity,
    ) -> Result<Entity, simac_operator::stack::EntityToPhysError> {
        let operator_stack: &OperatorStack = self.world.resource();
        operator_stack.try_entity_to_phys(entity)
    }

    fn entity_to_virt(&self, entity: Entity) -> simac_operator::VirtEntity {
        let operator_stack: &OperatorStack = self.world.resource();
        operator_stack.entity_to_virt(entity)
    }
}

/// Resolve `Id` collisions and missing `Id`s.
// FIXME: Make `Id` assignment deterministic
fn assign_ids(world: &mut World) {
    let plan = world
        .run_system_cached(simac_session::meta::assign_ids::sys_plan_assign_ids)
        .expect("sys_plan_assign_ids");
    let operator = Box::new(simac_session::ops::assign_ids::AssignIds { plan });
    world.resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
        operator_stack.execute(world, operator).expect("AssignIds")
    });
}
