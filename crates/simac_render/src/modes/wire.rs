use bevy_ecs::prelude::*;
use bevy_math::{IVec2, Vec2};
use simac_operator::ops::noop;
use simac_session::{
    circuit,
    ops::wire_add::{WireAdd, WireAddUndo},
};

use crate::draw::ModeVisual;

#[cfg(test)]
mod tests;

/// A mode to add a wire.
#[derive(Debug)]
pub struct DrawWire {
    ent_circuit: Entity,
    endpoints: [IVec2; 2],
    mouse_initial_position: Vec2,
    attach: bool,
}

impl DrawWire {
    pub fn new(
        ent_circuit: Entity,
        mouse_initial_position: Vec2,
        endpoint0: Option<IVec2>,
    ) -> Self {
        let endpoint0 = endpoint0.unwrap_or_else(|| mouse_initial_position.round().as_ivec2());
        Self {
            ent_circuit,
            endpoints: [endpoint0; 2],
            mouse_initial_position,
            attach: true,
        }
    }
}

impl super::Mode for DrawWire {
    fn operator(
        &mut self,
        _cx: &mut dyn super::ModeCx,
    ) -> Box<dyn simac_operator::operator::Operator> {
        if self.endpoints[0] == self.endpoints[1] {
            noop()
        } else {
            Box::new(WireAdd {
                ent_circuit: self.ent_circuit,
                endpoints: self.endpoints,
                attach: self.attach,
            })
        }
    }

    fn visual(
        &self,
        cx: &mut dyn super::ModeCx,
        undo: &dyn simac_operator::operator::OperatorUndo,
    ) -> ModeVisual {
        let world = cx.world();
        let Some(undo): Option<&WireAddUndo> = undo.as_any().downcast_ref() else {
            // `noop` operator was executed
            return <_>::default();
        };

        // Highlight the wire and its endpoints
        let ent_wire = cx.entity_to_phys(undo.virt_wire());
        let comp_device: &circuit::Device = world.get(ent_wire).unwrap();
        let entities = [ent_wire, comp_device.terminals[0], comp_device.terminals[1]]
            .into_iter()
            .collect();

        // Highlight the attachment points
        let attach_points = undo.attach_points().to_owned();

        ModeVisual {
            entities,
            attach_points,
            ..<_>::default()
        }
    }

    fn mouse_move(&mut self, position: Vec2) {
        self.endpoints[1] = self.endpoints[0]
            .saturating_add((position - self.mouse_initial_position).round().as_ivec2());
    }

    fn set_modifiers(&mut self, flags: super::Modifiers) {
        self.attach = !flags.contains(super::Modifiers::SHIFT);
    }
}
