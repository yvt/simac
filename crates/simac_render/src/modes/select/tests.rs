use bevy_ecs::prelude::*;
use indoc::indoc;
use insta::assert_ron_snapshot;
use simac_operator::stack::OperatorStack;
use simac_session::{circuit, kio::test_utils::Workspace, meta};
use std::sync::Arc;

use super::{SelectRectByMouseMode, SelectedCircuitItems, SysSelectAllInput, SysSelectHotSpot};
use crate::{
    modes::{Mode, ModeCx, Modifiers, tests::ModeCxImpl},
    pick,
};

fn modify_selection(
    kdl: &str,
    init_selection: &[&str],
    op: &mut dyn FnMut(&mut Workspace, Entity),
) -> Vec<String> {
    // Add common part
    let kdl = format!(
        r#"simac

workspace {{
    root main
}}

import std builtin:std

{kdl}"#
    );
    tracing::trace!(kdl);

    let mut ws = Workspace::load_str(&kdl);

    let mut operator_stack = OperatorStack::default();
    simac_session::init_stashable_components(&mut operator_stack, &mut ws.world);
    ws.world.insert_resource(operator_stack);

    ws.world
        .run_system_cached(simac_session::layout::position_terminal_nodes)
        .expect("SysIdPositionTerminalNodes");
    ws.world
        .run_system_cached(pick::update_symbols)
        .expect("update_symbols");

    let operator_stack: &OperatorStack = ws.world.resource();

    // Create an initial selection
    let comp_selection = SelectedCircuitItems {
        ent_circuit: ws.ent_main_circuit,
        ent_items: Arc::new(
            init_selection
                .iter()
                .map(|id| {
                    let ent = ws
                        .main_circuit_item_by_id(id)
                        .unwrap_or_else(|| panic!("item with id {id:?} not found"));
                    operator_stack.entity_to_virt(ent)
                })
                .collect(),
        ),
    };
    let ent_selection = ws.world.spawn(comp_selection).id();

    // Call `op`
    op(&mut ws, ent_selection);

    // Get the final selection
    let comp_selection: &SelectedCircuitItems = ws.world.get(ent_selection).unwrap();
    let operator_stack: &OperatorStack = ws.world.resource();
    let mut final_selection: Vec<String> = comp_selection
        .ent_items
        .iter()
        .filter_map(|virt_item| {
            let ent_item = operator_stack.try_entity_to_phys(*virt_item).ok()?;
            if ws.world.get_entity(ent_item).is_err() {
                return None;
            }
            let meta::Id(id) = ws.world.get(ent_item).unwrap_or_else(|| {
                panic!("selected item {ent_item} ({virt_item}) does not have `Id`")
            });
            Some(id.clone())
        })
        .collect();
    final_selection.sort_unstable();
    final_selection
}

/// Load a circuit, create a [`Mode`] by calling `make_mode`, apply its mode
/// operator, and return the resulting selection.
#[tracing::instrument(skip_all)]
pub(crate) fn apply_mode_on_main_circuit_selection(
    kdl: &str,
    init_selection: &[&str],
    make_mode: &mut dyn FnMut(&mut dyn ModeCx, &Workspace, Entity) -> Box<dyn Mode>,
) -> Vec<String> {
    modify_selection(kdl, init_selection, &mut |workspace, ent_selection| {
        // Create a `Mode`
        let mut cx = ModeCxImpl {
            world: &workspace.world,
        };
        let mut mode = make_mode(&mut cx, workspace, ent_selection);
        tracing::debug!(?mode);

        // Apply the mode operator
        let operator = mode.operator(&mut cx);
        tracing::debug!(?operator, "Applying mode operator");
        workspace
            .world
            .resource_scope(|world, mut operator_stack: Mut<OperatorStack>| {
                operator_stack
                    .execute(world, operator)
                    .expect("mode operator failed");
            });
    })
}

#[test]
fn select_hot_spot_device() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            std.resistor r x=2 y=0
        }" },
        &[],
        &mut |workspace, ent_selection| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            workspace
                .world
                .run_system_cached_with(
                    SysSelectHotSpot::run,
                    SysSelectHotSpot {
                        ent_selection,
                        hot_spot: Some(pick::HotSpot::Device { ent_device }),
                        additive: false,
                    },
                )
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "r",
    ]
    "#);
}

#[test]
fn select_hot_spot_wire_terminal_node_device() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            std.resistor r x=2 y=0
            node n x=0 y=0
            - w n r.+
        }" },
        &[],
        &mut |workspace, ent_selection| {
            let ent_device = workspace.main_circuit_item_by_id("w").unwrap();

            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let terminal_i = true;
            let ent_node = comp_device.terminals[terminal_i as usize];

            workspace
                .world
                .run_system_cached_with(
                    SysSelectHotSpot::run,
                    SysSelectHotSpot {
                        ent_selection,
                        hot_spot: Some(pick::HotSpot::WireTerminalNode {
                            ent_device,
                            terminal_i,
                            ent_node,
                        }),
                        additive: false,
                    },
                )
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "r",
    ]
    "#);
}

#[test]
fn select_hot_spot_wire_terminal_node_open() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            node n1 x=0 y=0
            node n2 x=2 y=0
            - w n1 n2
        }" },
        &[],
        &mut |workspace, ent_selection| {
            let ent_device = workspace.main_circuit_item_by_id("w").unwrap();

            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let terminal_i = false;
            let ent_node = comp_device.terminals[terminal_i as usize];

            workspace
                .world
                .run_system_cached_with(
                    SysSelectHotSpot::run,
                    SysSelectHotSpot {
                        ent_selection,
                        hot_spot: Some(pick::HotSpot::WireTerminalNode {
                            ent_device,
                            terminal_i,
                            ent_node,
                        }),
                        additive: false,
                    },
                )
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "n1",
    ]
    "#);
}

#[test]
fn select_hot_spot_wire_terminal_node_wire() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            node n1 x=0 y=0
            node n2 x=2 y=0
            node n3 x=4 y=0
            - w n1 n2
            - n2 n3
        }" },
        &[],
        &mut |workspace, ent_selection| {
            let ent_device = workspace.main_circuit_item_by_id("w").unwrap();

            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let terminal_i = true;
            let ent_node = comp_device.terminals[terminal_i as usize];

            workspace
                .world
                .run_system_cached_with(
                    SysSelectHotSpot::run,
                    SysSelectHotSpot {
                        ent_selection,
                        hot_spot: Some(pick::HotSpot::WireTerminalNode {
                            ent_device,
                            terminal_i,
                            ent_node,
                        }),
                        additive: false,
                    },
                )
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "n2",
    ]
    "#);
}

#[test]
fn select_hot_spot_open_terminal_node() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            std.resistor r x=2 y=0
        }" },
        &[],
        &mut |workspace, ent_selection| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();

            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let terminal_i = 1;
            let ent_node = comp_device.terminals[terminal_i];

            workspace
                .world
                .run_system_cached_with(
                    SysSelectHotSpot::run,
                    SysSelectHotSpot {
                        ent_selection,
                        hot_spot: Some(pick::HotSpot::OpenTerminalNode {
                            ent_device,
                            terminal_i,
                            ent_node,
                        }),
                        additive: false,
                    },
                )
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "r",
    ]
    "#);
}

#[test]
fn select_hot_spot_orphan_node() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            node n
            node n2
        }" },
        &[],
        &mut |workspace, ent_selection| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            workspace
                .world
                .run_system_cached_with(
                    SysSelectHotSpot::run,
                    SysSelectHotSpot {
                        ent_selection,
                        hot_spot: Some(pick::HotSpot::OrphanNode { ent_node }),
                        additive: false,
                    },
                )
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "n",
    ]
    "#);
}

#[test]
fn select_hot_spot_orphan_node_replace_add_subtract() {
    crate::tests::init();
    let [out_replace, out_add, out_subtract] = [
        (false, &["n2"][..]),
        (true, &["n2"][..]),
        (true, &["n", "n2"][..]),
    ]
    .map(|(additive, init_selection)| {
        modify_selection(
            indoc! { "circuit main {
                node n
                node n2
            }" },
            init_selection,
            &mut |workspace, ent_selection| {
                let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
                workspace
                    .world
                    .run_system_cached_with(
                        SysSelectHotSpot::run,
                        SysSelectHotSpot {
                            ent_selection,
                            hot_spot: Some(pick::HotSpot::OrphanNode { ent_node }),
                            additive,
                        },
                    )
                    .unwrap();
            },
        )
    });
    assert_ron_snapshot!(out_replace, @r#"
    [
      "n",
    ]
    "#);
    assert_ron_snapshot!(out_add, @r#"
    [
      "n",
      "n2",
    ]
    "#);
    assert_ron_snapshot!(out_subtract, @r#"
    [
      "n2",
    ]
    "#);
}

#[test]
fn select_all() {
    crate::tests::init();
    let out = modify_selection(
        indoc! { "circuit main {
            std.resistor r x=2 y=0
            node n x=0 y=0
            - w n r.+
        }" },
        &[],
        &mut |workspace, ent_selection| {
            workspace
                .world
                .run_system_cached_with(SysSelectAllInput::run, SysSelectAllInput { ent_selection })
                .unwrap();
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "n",
      "r",
      "w",
    ]
    "#);
}

/// An example circuit.
///
/// ```text
///  -3                n3
///            r       │
///   0  ●──🮦🮧🮦🮧🮦🮧──●──n1────n4
///                    │
///   3                n2
///
///      0          5  6     9
/// ```
const EXAMPLE1_KDL: &str = indoc! { "circuit main {
    std.resistor r x=0 y=0

    - w_r_n1 r.- n1

    node n1 x=6 y=0
    - w_n1_n2 n1 n2
    node n2 x=6 y=3
    - w_n1_n3 n1 n3
    node n3 x=6 y=-3

    - w_n1_n4 n1 n4

    node n4 x=9 y=0
}" };

#[test]
fn select_rect_none() {
    crate::tests::init();
    let [p1, p2] = [[0., 3.], [4., 4.]];
    let out = apply_mode_on_main_circuit_selection(
        EXAMPLE1_KDL,
        &[],
        &mut |_cx, workspace, ent_selected| {
            let mut mode =
                SelectRectByMouseMode::new(workspace.ent_main_circuit, ent_selected, p1.into());
            mode.mouse_move(p2.into());
            Box::new(mode)
        },
    );
    assert_ron_snapshot!(out, @r#"
    []
    "#);
}

#[test]
fn select_rect_r_n1() {
    crate::tests::init();
    let [p1, p2] = [[0., -1.], [8., 1.]];
    let out = apply_mode_on_main_circuit_selection(
        EXAMPLE1_KDL,
        &[],
        &mut |_cx, workspace, ent_selected| {
            let mut mode =
                SelectRectByMouseMode::new(workspace.ent_main_circuit, ent_selected, p1.into());
            mode.mouse_move(p2.into());
            Box::new(mode)
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "n1",
      "r",
      "w_n1_n2",
      "w_n1_n3",
      "w_n1_n4",
      "w_r_n1",
    ]
    "#);
}

#[test]
fn select_rect_w_n1_n2() {
    crate::tests::init();
    let [p1, p2] = [[5., 1.], [7., 2.]];
    let out = apply_mode_on_main_circuit_selection(
        EXAMPLE1_KDL,
        &[],
        &mut |_cx, workspace, ent_selected| {
            let mut mode =
                SelectRectByMouseMode::new(workspace.ent_main_circuit, ent_selected, p1.into());
            mode.mouse_move(p2.into());
            Box::new(mode)
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "w_n1_n2",
    ]
    "#);
}

#[test]
fn select_rect_w_n1_n2_n2() {
    crate::tests::init();
    let [p1, p2] = [[5., 1.], [7., 4.]];
    let out = apply_mode_on_main_circuit_selection(
        EXAMPLE1_KDL,
        &[],
        &mut |_cx, workspace, ent_selected| {
            let mut mode =
                SelectRectByMouseMode::new(workspace.ent_main_circuit, ent_selected, p1.into());
            mode.mouse_move(p2.into());
            Box::new(mode)
        },
    );
    assert_ron_snapshot!(out, @r#"
    [
      "n2",
      "w_n1_n2",
    ]
    "#);
}

#[test]
fn select_rect_modifiers() {
    crate::tests::init();
    let [p1, p2] = [[0., -1.], [5.5, 1.]];
    let [out_replace, out_add, out_remove, out_toggle] = [
        Modifiers::empty(),
        Modifiers::CONTROL,
        Modifiers::SHIFT,
        Modifiers::CONTROL | Modifiers::SHIFT,
    ]
    .map(|modifiers| {
        apply_mode_on_main_circuit_selection(
            EXAMPLE1_KDL,
            &["n1", "r"],
            &mut |_cx, workspace, ent_selected| {
                let mut mode =
                    SelectRectByMouseMode::new(workspace.ent_main_circuit, ent_selected, p1.into());
                mode.mouse_move(p2.into());
                mode.set_modifiers(modifiers);
                Box::new(mode)
            },
        )
    });
    assert_ron_snapshot!(out_replace, @r#"
    [
      "r",
      "w_r_n1",
    ]
    "#);
    assert_ron_snapshot!(out_add, @r#"
    [
      "n1",
      "r",
      "w_r_n1",
    ]
    "#);
    assert_ron_snapshot!(out_remove, @r#"
    [
      "n1",
    ]
    "#);
    assert_ron_snapshot!(out_toggle, @r#"
    [
      "n1",
      "w_r_n1",
    ]
    "#);
}
