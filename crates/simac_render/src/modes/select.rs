use bevy_ecs::{entity::EntityHashSet, prelude::*};
use bevy_math::{Rect, Vec2};
use simac_operator::{
    VirtEntity, operator,
    stack::{OperatorStack, VirtEntityHashSet},
};
use simac_session::{circuit, meta};
use std::{mem::replace, sync::Arc};

use crate::{draw::ModeVisual, pick};

#[cfg(test)]
mod tests;

/// A set of selected devices and nodes.
///
/// We store selection in an entity so that [`super::Mode`] can modify it
/// through an operator returned by [`super::Mode::operator`].
///
/// It may contain non-existent entities.
///
/// The following entities can be included in `self.0`:
///
/// - Devices
/// - Non-terminal nodes
#[derive(Debug, Clone, Component)]
pub struct SelectedCircuitItems {
    /// The circuit entity containing [`Self::ent_items`].
    pub ent_circuit: Entity,
    /// The selected circuit items.
    pub ent_items: Arc<VirtEntityHashSet>,
}

impl SelectedCircuitItems {
    pub fn new(ent_circuit: Entity) -> Self {
        Self {
            ent_circuit,
            ent_items: <_>::default(),
        }
    }
}

/// The physical entity counterpart of [`SelectedCircuitItems`].
///
/// In addition to the entities included in the original
/// [`SelectedCircuitItems`], `SelectedCircuitItemsPhys` include the following
/// entities:
///
/// - The terminal nodes of the non-wire devices included in the original
///   [`SelectedCircuitItems`]
///
/// > Rationale: Including terminal nodes is mostly for aesthetical
/// > reasons. An intended purpose of `SelectedCircuitItemsPhys` is to store a
/// > cached set of entities to highlight in an editor.
///
/// Use one of the following functions to update `Self` based on
/// [`SelectedCircuitItems`]:
///
/// - [`Self::update_from`]
/// - [`Self::system_update`]
#[derive(Default, Debug, Clone, Component)]
pub struct SelectedCircuitItemsPhys(pub Arc<EntityHashSet>);

impl SelectedCircuitItemsPhys {
    /// Convert [`SelectedCircuitItems`] to physical entities and store the
    /// result into `self`.
    pub fn update_from(
        &mut self,
        selected_virt: &SelectedCircuitItems,
        circuit: &circuit::Circuit,
        operator_stack: &OperatorStack,
        q_device_nonwire: &Query<Ref<circuit::Device>, Without<circuit::WireDevice>>,
    ) -> bool {
        let mut new_selected_phys = EntityHashSet::with_capacity_and_hasher(
            selected_virt.ent_items.len(),
            Default::default(),
        );

        for &virt_item in selected_virt.ent_items.iter() {
            let Ok(ent_item) = operator_stack.try_entity_to_phys(virt_item) else {
                tracing::trace!(%virt_item, "The virtual entity is not materialized");
                continue;
            };
            if circuit.nodes.contains(&ent_item) {
                tracing::trace!(%virt_item, %ent_item, "Found a node entity");
                new_selected_phys.insert(ent_item);
            } else if circuit.devices.contains(&ent_item) {
                tracing::trace!(%virt_item, %ent_item, "Found a device entity");
                new_selected_phys.insert(ent_item);

                let Ok(comp_device) = q_device_nonwire.get(ent_item) else {
                    tracing::trace!(
                        %virt_item,
                        %ent_item,
                        "The device either is a wire or lacks `Device` component",
                    );
                    continue;
                };

                tracing::trace!(?comp_device.terminals, "Appending device terminals as well");
                new_selected_phys.extend(comp_device.terminals.iter().copied());
            } else {
                tracing::trace!(
                    %virt_item,
                    %ent_item,
                    "Unable to locate the entity in `Circuit`; excluding from output",
                );
            }
        }

        if new_selected_phys != *self.0 {
            tracing::debug!(?new_selected_phys, "`SelectedCircuitItemsPhys` changed");
            self.0 = Arc::new(new_selected_phys);
            true
        } else {
            tracing::trace!("No changes");
            false
        }
    }

    /// The Bevy system to convert [`SelectedCircuitItems`] to `Self`.
    ///
    /// The selection entities must have both [`SelectedCircuitItems`] and
    /// [`SelectedCircuitItemsPhys`].
    #[tracing::instrument(name = "SelectedCircuitItemsPhys::system_update", skip_all)]
    pub fn system_update(
        mut q_selection: Query<(
            Entity,
            Ref<SelectedCircuitItems>,
            Mut<SelectedCircuitItemsPhys>,
        )>,
        q_circuit: Query<Ref<circuit::Circuit>>,
        q_device_nonwire: Query<Ref<circuit::Device>, Without<circuit::WireDevice>>,
        operator_stack: Res<OperatorStack>,
    ) {
        for (ent_selection, selected_virt, mut selected_phys) in q_selection.iter_mut() {
            let Ok(comp_circuit) = q_circuit.get(selected_virt.ent_circuit) else {
                tracing::warn!(
                    %ent_selection,
                    ent_circuit = %selected_virt.ent_circuit,
                    "The circuit entity referenced by this selection entity \
                    could not be found",
                );
                continue;
            };

            if operator_stack.is_changed()
                || selected_virt.is_changed()
                || comp_circuit.is_changed()
            {
                let _span =
                    tracing::info_span!("Updating a selection entity", %ent_selection).entered();
                if selected_phys.bypass_change_detection().update_from(
                    &selected_virt,
                    &comp_circuit,
                    &operator_stack,
                    &q_device_nonwire,
                ) {
                    selected_phys.set_changed();
                }
            }
        }
    }
}

/// A Bevy system to update [`SelectedCircuitItems`] based on a given
/// [`pick::HotSpot`].
#[derive(Debug)]
pub struct SysSelectHotSpot {
    /// An entity containing [`SelectedCircuitItems`].
    pub ent_selection: Entity,
    pub hot_spot: Option<pick::HotSpot>,
    pub additive: bool,
}

impl SysSelectHotSpot {
    /// The system entrypoint.
    pub fn run(
        In(Self {
            ent_selection,
            hot_spot,
            additive,
        }): In<Self>,
        mut q_selection: Query<Mut<SelectedCircuitItems>>,
        q_terminal_node: Query<&meta::TerminalNode>,
        operator_stack: Res<OperatorStack>,
    ) {
        let mut comp_selection = q_selection
            .get_mut(ent_selection)
            .expect("`ent_selection` does not have `SelectedCircuitItems`");

        // Decide the entity to add to `ent_selection`
        let ent = match hot_spot {
            Some(
                pick::HotSpot::Device { ent_device }
                | pick::HotSpot::OpenTerminalNode { ent_device, .. },
            ) => Some(ent_device),
            Some(pick::HotSpot::OrphanNode { ent_node }) => Some(ent_node),
            Some(pick::HotSpot::WireTerminalNode { ent_node, .. }) => {
                if let Ok(&meta::TerminalNode { device }) = q_terminal_node.get(ent_node) {
                    // This is a terminal node; selecte the owning device instead
                    Some(device)
                } else {
                    Some(ent_node)
                }
            }
            None => None,
        };

        let ent = ent.map(|ent| operator_stack.entity_to_virt(ent));

        if let Some(ent) = ent {
            if additive {
                // Add or remove `ent` to `comp_selection.ent_items`
                let ent_items = Arc::make_mut(&mut comp_selection.ent_items);
                if ent_items.take(&ent).is_none() {
                    ent_items.insert(ent);
                }
            } else if comp_selection.ent_items.iter().ne(&[ent]) {
                comp_selection.ent_items = Arc::new([ent].into_iter().collect());
            }
        } else if !additive && !comp_selection.ent_items.is_empty() {
            comp_selection.ent_items = <_>::default();
        }
    }
}

/// A Bevy system to add all circuit items to [`SelectedCircuitItems`].
#[derive(Debug)]
pub struct SysSelectAllInput {
    /// An entity containing [`SelectedCircuitItems`].
    pub ent_selection: Entity,
}

impl SysSelectAllInput {
    /// The system entrypoint.
    pub fn run(
        In(Self { ent_selection }): In<Self>,
        mut q_selection: Query<Mut<SelectedCircuitItems>>,
        q_circuit: Query<&circuit::Circuit>,
        q_terminal_nodes: Query<(), With<meta::TerminalNode>>,
        operator_stack: Res<OperatorStack>,
    ) {
        let mut comp_selection = q_selection
            .get_mut(ent_selection)
            .expect("`ent_selection` does not have `SelectedCircuitItems`");
        let comp_circuit = q_circuit
            .get(comp_selection.ent_circuit)
            .expect("`ent_circuit` does not have `Circuit`");

        let mut ent_new_items = VirtEntityHashSet::with_capacity_and_hasher(
            comp_circuit.devices.len()
                + comp_circuit
                    .nodes
                    .len()
                    .saturating_sub(comp_circuit.devices.len() * 2),
            Default::default(),
        );
        ent_new_items.extend(
            comp_circuit
                .devices
                .iter()
                .map(|&e| operator_stack.entity_to_virt(e)),
        );
        ent_new_items.extend(
            comp_circuit
                .nodes
                .iter()
                .filter(|&&ent_node| !q_terminal_nodes.contains(ent_node))
                .map(|&e| operator_stack.entity_to_virt(e)),
        );

        comp_selection.ent_items = Arc::new(ent_new_items);
    }
}

/// A mode for rubberband selection.
#[derive(Debug)]
pub struct SelectRectByMouseMode {
    mouse_initial_position: Vec2,
    mouse_position: Vec2,
    op: SelectOp,
    /// An entity containing [`SelectedCircuitItems`].
    ent_selected: Entity,
    ent_circuit: Entity,
}

impl SelectRectByMouseMode {
    pub fn new(ent_circuit: Entity, ent_selected: Entity, mouse_initial_position: Vec2) -> Self {
        Self {
            mouse_initial_position,
            mouse_position: mouse_initial_position,
            op: SelectOp::Replace,
            ent_selected,
            ent_circuit,
        }
    }

    fn rect(&self) -> Rect {
        Rect::from_corners(self.mouse_position, self.mouse_initial_position)
    }
}

impl super::Mode for SelectRectByMouseMode {
    // TODO: Exclude selection changes, including this operator, from undo stack
    fn operator(&mut self, _cx: &mut dyn super::ModeCx) -> Box<dyn operator::Operator> {
        Box::new(SelectRectByMouseOp {
            rect: self.rect(),
            op: self.op,
            ent_selected: self.ent_selected,
            ent_circuit: self.ent_circuit,
        })
    }

    fn visual(
        &self,
        _cx: &mut dyn super::ModeCx,
        _undo: &dyn operator::OperatorUndo,
    ) -> ModeVisual {
        ModeVisual {
            rubberband_rect: Some(self.rect()),
            ..<_>::default()
        }
    }

    fn mouse_move(&mut self, position: Vec2) {
        self.mouse_position = position;
    }

    fn set_modifiers(&mut self, flags: super::Modifiers) {
        self.op = match (
            flags.contains(super::Modifiers::CONTROL),
            flags.contains(super::Modifiers::SHIFT),
        ) {
            (false, false) => SelectOp::Replace,
            (false, true) => SelectOp::Remove,
            (true, false) => SelectOp::Add,
            (true, true) => SelectOp::Toggle,
        }
    }
}

/// Specifies how to modify [`SelectedCircuitItems::ent_items`].
#[derive(Debug, Clone, Copy)]
enum SelectOp {
    /// Discard an existing selection and select new items.
    Replace,
    /// Select items.
    Add,
    /// Deselect items.
    Remove,
    /// Invert the selection state of specified entities.
    Toggle,
}

#[derive(Debug)]
pub struct SelectRectByMouseOp {
    rect: Rect,
    op: SelectOp,
    ent_selected: Entity,
    ent_circuit: Entity,
}

#[derive(Debug)]
pub struct SelectRectByMouseOpRedo {
    rect: Rect,
    op: SelectOp,
    ent_selected: VirtEntity,
    ent_circuit: VirtEntity,
}

#[derive(Debug)]
pub struct SelectRectByMouseOpUndo {
    rect: Rect,
    op: SelectOp,
    ent_selected: VirtEntity,
    ent_circuit: VirtEntity,
    /// The old value that was stored in [`Self::ent_selected`].
    selected_old: Arc<VirtEntityHashSet>,
}

impl operator::Operator for SelectRectByMouseOp {
    fn execute(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorExecuteCx,
        _prev_operator: Option<&mut dyn operator::OperatorUndo>,
    ) -> Result<Box<dyn operator::OperatorUndo>, operator::OperatorExecuteError> {
        let ent_selected = cx.entity_to_virt(self.ent_selected);
        let ent_circuit = cx.entity_to_virt(self.ent_circuit);
        Ok(operator::OperatorRedo::redo(
            Box::new(SelectRectByMouseOpRedo {
                rect: self.rect,
                op: self.op,
                ent_selected,
                ent_circuit,
            }),
            cx.as_forward_cx_mut(),
        ))
    }
}

impl operator::OperatorRedo for SelectRectByMouseOpRedo {
    fn redo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorForwardCx,
    ) -> Box<dyn operator::OperatorUndo> {
        let ent_selected = cx.entity_to_phys(self.ent_selected);
        let ent_circuit = cx.entity_to_phys(self.ent_circuit);

        // Find items in `self.rect`
        let world = cx.world_mut();

        let selected_new = world
            .run_system_cached_with(
                SysFindItemsInRect::run,
                SysFindItemsInRect {
                    ent_circuit,
                    rect: self.rect,
                },
            )
            .expect("system `SysFindItemsInRect::run`");
        tracing::debug!(
            ?selected_new,
            "Physical entities overlapping with selection rectangle"
        );

        let mut selected_new: VirtEntityHashSet = selected_new
            .into_iter()
            .map(|ent| cx.entity_to_virt(ent))
            .collect();
        tracing::debug!(
            ?selected_new,
            "Virtual entities overlapping with selection rectangle"
        );

        // Update `self.ent_selected`
        let mut comp_selected: Mut<SelectedCircuitItems> = cx
            .world_mut()
            .get_mut(ent_selected)
            .expect("`ent_selected` does not contain `SelectedCircuitItems`");

        let selected_old = 'a: {
            match self.op {
                SelectOp::Replace => {}
                SelectOp::Add => {
                    if selected_new.is_subset(&comp_selected.ent_items) {
                        // No change - the new items are already included in
                        // `comp_selected`
                        break 'a Arc::clone(&comp_selected.ent_items);
                    }
                    // selected_new = (&*comp_selected.ent_items) + &selected_new
                    selected_new.extend(comp_selected.ent_items.iter().copied());
                }
                SelectOp::Remove => {
                    if selected_new.is_disjoint(&comp_selected.ent_items) {
                        // No change - the new items has no overlaps with
                        // `comp_selected`
                        break 'a Arc::clone(&comp_selected.ent_items);
                    }
                    selected_new = (&*comp_selected.ent_items) - &selected_new;
                }
                SelectOp::Toggle => {
                    if selected_new.is_empty() {
                        // No change
                        break 'a Arc::clone(&comp_selected.ent_items);
                    }
                    // selected_new = (&*comp_selected.ent_items) ^ &selected_new
                    for &ent in comp_selected.ent_items.iter() {
                        if !selected_new.remove(&ent) {
                            selected_new.insert(ent);
                        }
                    }
                }
            }

            replace(&mut comp_selected.ent_items, Arc::new(selected_new))
        };

        Box::new(SelectRectByMouseOpUndo {
            rect: self.rect,
            op: self.op,
            ent_selected: self.ent_selected,
            ent_circuit: self.ent_circuit,
            selected_old,
        })
    }
}

impl operator::OperatorUndo for SelectRectByMouseOpUndo {
    fn undo(
        self: Box<Self>,
        cx: &mut dyn operator::OperatorBackwardCx,
    ) -> Box<dyn operator::OperatorRedo> {
        let ent_selected = cx.entity_to_phys(self.ent_selected);
        let ent_circuit = cx.entity_to_phys(self.ent_circuit);

        cx.world_mut()
            .get_entity_mut(ent_selected)
            .expect("`ent_selected` not found")
            .insert(SelectedCircuitItems {
                ent_circuit,
                ent_items: self.selected_old,
            });

        Box::new(SelectRectByMouseOpRedo {
            rect: self.rect,
            op: self.op,
            ent_selected: self.ent_selected,
            ent_circuit: self.ent_circuit,
        })
    }
}

struct SysFindItemsInRect {
    ent_circuit: Entity,
    rect: Rect,
}

impl SysFindItemsInRect {
    fn run(
        In(Self { ent_circuit, rect }): In<Self>,
        find_items_in_rect_param: pick::FindItemsInRectParam,
    ) -> Vec<Entity> {
        pick::find_items_in_rect(&find_items_in_rect_param, ent_circuit, rect)
    }
}
