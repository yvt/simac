use bevy_math::Vec2;
use indoc::indoc;
use insta::assert_snapshot;
use simac_session::circuit;

use super::MoveByMouseMode;
use crate::{
    modes::{Mode, tests::apply_mode_on_main_circuit},
    pick,
};

/// Move a subcircuit device so that one of the terminals overlaps with another
/// node.
#[test]
fn move_hot_spot_subcircuit_device_overlap() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=2 y=0
            node n1 x=0 y=0
            node n2 x=1 y=0
            - n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(-1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=1 y=0
        node n1 x=0 y=0
        node n2 x=1 y=0
        - n1 n2
    }
    "#);
}

/// Move a wire device.
#[test]
fn move_hot_spot_wire_device() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - wire n2 r.+
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 n2
        node n2 x=3 y=1
        - wire n2 r.+
        std.resistor r x=6 y=1
    }
    "#);
}

/// Move an open wire terminal over a subcircuit device terminal.
#[test]
fn move_hot_spot_wire_terminal_overlap() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=2 y=0
            node n1 x=0 y=0
            node n2 x=1 y=0
            - wire n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let ent_node = workspace.main_circuit_item_by_id("n2").unwrap();

            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=2 y=0
        node n1 x=0 y=0
        node n2 x=2 y=0
        - wire n1 n2
    }
    "#);
}

/// Move a wire terminal connected to a subcircuit device.
#[test]
fn move_hot_spot_wire_terminal_terminal_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=2 y=0
            node n1 x=0 y=0
            - wire n1 r.+
        }"# },
        &mut |cx, workspace| {
            // The second endpoint of `wire`
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let terminal_i = true;
            let ent_node = comp_device.terminals[terminal_i as usize];

            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=2 y=1
        node n1 x=0 y=0
        - wire n1 r.+
    }
    "#);
}

/// Move a wire terminal connected to another wire.
#[test]
fn move_hot_spot_wire_terminal_connected() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            node n2 x=1 y=0
            node n3 x=2 y=0
            - wire n1 n2
            - n2 n3
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let ent_node = workspace.main_circuit_item_by_id("n2").unwrap();

            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        node n2 x=1 y=1
        node n3 x=2 y=0
        - wire n1 n2
        - n2 n3
    }
    "#);
}

/// Move an open subcircuit device terminal.
#[test]
fn move_hot_spot_subcircuit_open_terminal() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=0 y=0
        }"# },
        &mut |cx, workspace| {
            // `r.-`
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let ent_node = comp_device.terminals[1];
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OpenTerminalNode {
                    ent_device,
                    terminal_i: 1,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=1 y=0
    }
    "#);
}

/// Move an orphan node.
#[test]
fn move_hot_spot_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
    }
    "#);
}

/// Move an orphan node over another orphan node.
#[test]
fn move_hot_spot_orphan_node_overlap_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
            node n1 x=1 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
        node n1 x=1 y=0
    }
    "#);
}

/// Move an orphan node over a wire endpoint.
#[test]
fn move_hot_spot_orphan_node_overlap_wire_endpoint() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
            node n1 x=1 y=0
            node n2 x=2 y=0
            - n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
        node n1 x=1 y=0
        node n2 x=2 y=0
        - n1 n2
    }
    "#);
}

/// Move an orphan node over a subcircuit device terminal.
#[test]
fn move_hot_spot_orphan_node_overlap_terminal_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
            std.resistor r x=1 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
        std.resistor r x=1 y=0
    }
    "#);
}

/// Displace a subcircuit device to attach it to a wire.
#[test]
fn displace_hot_spot_subcircuit_device_attach_wire() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=2 y=0
            node n1 x=0 y=0
            node n2 x=1 y=0
            - n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(-1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=1 y=0
        node n1 x=0 y=0
        - n1 r.+
    }
    "#);
}

/// Displace a subcircuit device to detach it from a wire.
#[test]
fn displace_hot_spot_subcircuit_device_detach_wire() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=1 y=0
            node n1 x=0 y=0
            - n1 r.+
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=2 y=0
        node n1 x=0 y=0
        - n1 node1
    node node1 x=1
    }
    "#);
}

/// Displace a wire device to attach it to a wire and a subcircuit device.
#[test]
fn displace_hot_spot_wire_device_attach_wire_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2a
            node n2a x=3
            node n2 x=3 y=1
            - wire n2 n3
            node n3 x=6 y=1
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., -1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 n2
        node n2 x=3 y=0
        - wire n2 r.+
        std.resistor r x=6 y=0
    }
    "#);
}

/// Displace a wire device to detach it from a wire and a subcircuit device.
#[test]
fn displace_hot_spot_wire_device_detach_wire_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - wire n2 r.+
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 node2
        node n2 x=3 y=1
        - wire n2 node1
        std.resistor r x=6 y=0
    node node1 x=6 y=1
    node node2 x=3
    }
    "#);
}

/// Displace a wire device to attach it to an orphan node.
#[test]
fn displace_hot_spot_wire_device_attach_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - wire n1 n2
            node n2 x=3 y=0
            node n3 x=3 y=1
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::Device { ent_device },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=1
        - wire n1 n2
        node n2 x=3 y=1
    }
    "#);
}

/// Displace a wire endpoint to attach it to an orphan node.
#[test]
fn displace_hot_spot_wire_terminal_attach_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            node n2 x=1 y=0
            node n3 x=2 y=0
            - wire n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let ent_node = workspace.main_circuit_item_by_id("n2").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        node n2 x=2 y=0
        - wire n1 n2
    }
    "#);
}

/// Displace a wire endpoint to attach it to a subcircuit device.
#[test]
fn displace_hot_spot_wire_terminal_attach_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=2 y=0
            node n1 x=0 y=0
            node n2 x=1 y=0
            - wire n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let ent_node = workspace.main_circuit_item_by_id("n2").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=2 y=0
        node n1 x=0 y=0
        - wire n1 r.+
    }
    "#);
}

/// Displace a wire endpoint to detach it from a subcircuit device.
#[test]
fn displace_hot_spot_wire_terminal_detach_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=2 y=0
            node n1 x=0 y=0
            - wire n1 r.+
        }"# },
        &mut |cx, workspace| {
            // The second endpoint of `wire`
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let terminal_i = true;
            let ent_node = comp_device.terminals[terminal_i as usize];

            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(-1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=2 y=0
        node n1 x=0 y=0
        - wire n1 node1
    node node1 x=1
    }
    "#);
}

/// Displace a wire endpoint to attach it to another wire.
#[test]
fn displace_hot_spot_wire_terminal_attach_wire() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            node n2 x=1 y=0
            node n3 x=2 y=0
            node n4 x=4 y=0
            - wire n1 n2
            - n3 n4
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let ent_node = workspace.main_circuit_item_by_id("n2").unwrap();

            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        node n2 x=2 y=0
        node n4 x=4 y=0
        - wire n1 n2
        - n2 n4
    }
    "#);
}

/// Displace a wire endpoint to detach it from another wire.
#[test]
fn displace_hot_spot_wire_terminal_detach_wire() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            node n2 x=2 y=0
            node n3 x=4 y=0
            - wire n1 n2
            - n2 n3
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("wire").unwrap();
            let ent_node = workspace.main_circuit_item_by_id("n2").unwrap();

            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::WireTerminalNode {
                    ent_device,
                    terminal_i: true,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(-1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        node n2 x=1 y=0
        node n3 x=4 y=0
        - wire n1 n2
        - node1 n3
    node node1 x=2
    }
    "#);
}

/// Displace an open terminal of a subcircuit device.
#[test]
fn displace_hot_spot_subcircuit_open_terminal() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=0 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let ent_node = comp_device.terminals[1];
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OpenTerminalNode {
                    ent_device,
                    terminal_i: 1,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=1 y=0
    }
    "#);
}

/// Displace an open terminal of a subcircuit device to attach it to an orphan
/// node.
#[test]
fn displace_hot_spot_subcircuit_open_terminal_overlap_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=0 y=0
            node n x=0 y=1
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let ent_node = comp_device.terminals[1];
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OpenTerminalNode {
                    ent_device,
                    terminal_i: 1,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=0 y=1
    }
    "#);
}

/// Displace an open terminal of a subcircuit device to attach it to a wire.
#[test]
fn displace_hot_spot_subcircuit_open_terminal_overlap_wire() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=0 y=0
            node n1 x=0 y=1
            node n2 x=1 y=1
            - n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let ent_node = comp_device.terminals[1];
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OpenTerminalNode {
                    ent_device,
                    terminal_i: 1,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=0 y=1
        node n2 x=1 y=1
        - r.+ n2
    }
    "#);
}

/// Displace an open terminal of a subcircuit device to unsuccessfully attempt
/// to attach it to another subcircuit device.
///
/// Attachment is not possible in this case because one node can not become a
/// `TerminalNode` of two non-wire devices simultaneously.
#[test]
fn displace_hot_spot_subcircuit_open_terminal_overlap_subcircuit_open_termnal() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r x=0 y=0
            std.resistor r2 x=0 y=1 angle=90
        }"# },
        &mut |cx, workspace| {
            let ent_device = workspace.main_circuit_item_by_id("r").unwrap();
            let comp_device: &circuit::Device = workspace.world.get(ent_device).unwrap();
            let ent_node = comp_device.terminals[1];
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OpenTerminalNode {
                    ent_device,
                    terminal_i: 1,
                    ent_node,
                },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r x=0 y=1
        std.resistor r2 x=0 y=1 angle=90
    }
    "#);
}

/// Displace an orphan node.
#[test]
fn displace_hot_spot_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
    }
    "#);
}

/// Displace an orphan node to merge it with another orphan node.
#[test]
fn displace_hot_spot_orphan_node_merge_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
            node n1 x=1 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
    }
    "#);
}

/// Displace an orphan node to merge it with a wire endpoint.
#[test]
fn displace_hot_spot_orphan_node_overlap_wire_endpoint() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
            node n1 x=1 y=0
            node n2 x=2 y=0
            - n1 n2
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n x=1 y=0
        node n2 x=2 y=0
        - n n2
    }
    "#);
}

/// Displace an orphan node to merge it with a subcircuit device terminal.
#[test]
fn displace_hot_spot_orphan_node_overlap_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n x=0 y=0
            std.resistor r x=1 y=0
        }"# },
        &mut |cx, workspace| {
            let ent_node = workspace.main_circuit_item_by_id("n").unwrap();
            let mut mode = MoveByMouseMode::from_hot_spot(
                pick::HotSpot::OrphanNode { ent_node },
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    // FIXME: Improve formatting
    assert_snapshot!(out, @r#"
    circuit main {
            std.resistor r x=1 y=0
    }
    "#);
}

/// Select and move subcircuit devices.
#[test]
fn move_selection_subcircuit_devices() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r1 x=0 y=0
            - r1.- n
            node n x=6 y=0
            - n r2.+
            std.resistor r2 x=7 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_r1 = cx.entity_to_virt(workspace.main_circuit_item_by_id("r1").unwrap());
            let virt_r2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("r2").unwrap());

            let mut mode = MoveByMouseMode::from_selection(
                &[virt_r1, virt_r2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r1 x=0 y=1
        - r1.- n
        node n x=6 y=0
        - n r2.+
        std.resistor r2 x=7 y=1
    }
    "#);
}

/// Select and move a wire device.
#[test]
fn move_selection_wire_device() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - wire n2 r.+
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_wire = cx.entity_to_virt(workspace.main_circuit_item_by_id("wire").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_wire].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 n2
        node n2 x=3 y=1
        - wire n2 r.+
        std.resistor r x=6 y=1
    }
    "#);
}

/// Select and move non-terminal nodes.
#[test]
fn move_selection_nodes() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - n2 r.+
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_n1 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n1").unwrap());
            let virt_n2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n2").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_n1, virt_n2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                false,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=1
        - n1 n2
        node n2 x=3 y=1
        - n2 r.+
        std.resistor r x=6 y=0
    }
    "#);
}

/// Select and displace subcircuit devices to detach them from wires.
#[test]
fn displace_selection_subcircuit_devices_detach() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            std.resistor r1 x=0 y=0
            - r1.- n
            node n x=6 y=0
            - n r2.+
            std.resistor r2 x=7 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_r1 = cx.entity_to_virt(workspace.main_circuit_item_by_id("r1").unwrap());
            let virt_r2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("r2").unwrap());

            let mut mode = MoveByMouseMode::from_selection(
                &[virt_r1, virt_r2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        std.resistor r1 x=0 y=1
        - node2 n
        node n x=6 y=0
        - n node1
        std.resistor r2 x=7 y=1
    node node1 x=7
    node node2 x=5
    }
    "#);
}

/// Select and displace a wire device to detach it from another wire and a
/// subcircuit device.
#[test]
fn displace_selection_wire_device_detach() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - wire n2 r.+
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_wire = cx.entity_to_virt(workspace.main_circuit_item_by_id("wire").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_wire].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 node2
        node n2 x=3 y=1
        - wire n2 node1
        std.resistor r x=6 y=0
    node node1 x=6 y=1
    node node2 x=3
    }
    "#);
}

/// Select and displace non-terminal nodes.
#[test]
fn displace_selection_nodes_move() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - n2 r.+
            std.resistor r x=6 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_n1 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n1").unwrap());
            let virt_n2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n2").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_n1, virt_n2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=1
        - n1 n2
        node n2 x=3 y=1
        - n2 r.+
        std.resistor r x=6 y=0
    }
    "#);
}

/// Select and displace non-terminal nodes to attach one of them to a wire
/// device.
#[test]
fn displace_selection_node_move_attach_wire() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            node n3 x=4 y=0
            - n3 n4
            node n4 x=7 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_n1 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n1").unwrap());
            let virt_n2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n2").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_n1, virt_n2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=1 y=0
        - n1 n2
        node n2 x=4 y=0
        - n2 n4
        node n4 x=7 y=0
    }
    "#);
}

/// Select and displace non-terminal nodes to attach one of them to a subcircuit
/// device.
#[test]
fn displace_selection_node_move_attach_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            std.resistor r x=4 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_n1 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n1").unwrap());
            let virt_n2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n2").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_n1, virt_n2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=1 y=0
        - n1 r.+
        std.resistor r x=4 y=0
    }
    "#);
}

/// Select and displace a non-terminal node to merge it with an orphan node.
#[test]
fn displace_selection_node_move_merge_orphan_node() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            node n3 x=4 y=0
        }"# },
        &mut |cx, workspace| {
            let virt_n2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n2").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_n2].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(1., 0.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 n2
        node n2 x=4 y=0
    }
    "#);
}

/// Select and displace a wire device and one of its endpoints to attach it to
/// a wire and detach the other endpoint from a subcircuit device.
#[test]
fn displace_selection_wire_node_attach_wire_detach_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=3 y=0
            - wire n2 r.+
            std.resistor r x=6 y=0

            node n3 x=3 y=1
            - n3 n4
            node n4 x=3 y=3
        }"# },
        &mut |cx, workspace| {
            let virt_n2 = cx.entity_to_virt(workspace.main_circuit_item_by_id("n2").unwrap());
            let virt_wire = cx.entity_to_virt(workspace.main_circuit_item_by_id("wire").unwrap());
            let mut mode = MoveByMouseMode::from_selection(
                &[virt_n2, virt_wire].into_iter().collect(),
                workspace.ent_main_circuit,
                Vec2::ZERO,
                true,
                cx,
            );
            mode.mouse_move(Vec2::new(0., 1.));
            Box::new(mode)
        },
    );
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 n2
        node n2 x=3 y=1
        - wire n2 node1
        std.resistor r x=6 y=0
        - n2 n4
        node n4 x=3 y=3
    node node1 x=6 y=1
    }
    "#);
}
