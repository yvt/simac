use bevy_math::Vec2;
use indoc::indoc;
use insta::assert_snapshot;

use super::DrawWire;
use crate::modes::{Mode, tests::apply_mode_on_main_circuit};

/// Start drawing a wire but do not move the mouse pointer from the starting
/// point.
#[test]
fn draw_wire_empty() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
        }"# },
        &mut |_, workspace| Box::new(DrawWire::new(workspace.ent_main_circuit, Vec2::ZERO, None)),
    );
    assert_snapshot!(out, @r#"
    circuit main {
    }
    "#);
}

/// Draw a wire, attaching it to another wire and a subcircuit device.
#[test]
fn draw_wire_attach_wire_subcircuit() {
    crate::tests::init();
    let out = apply_mode_on_main_circuit(
        indoc! { r#"circuit main {
            node n1 x=0 y=0
            - n1 n2
            node n2 x=2 y=0
            std.resistor r x=4 y=0
        }"# },
        &mut |_, workspace| {
            let mut mode = DrawWire::new(workspace.ent_main_circuit, Vec2::new(2., 0.), None);
            mode.mouse_move(Vec2::new(4., 0.));
            Box::new(mode)
        },
    );
    // FIXME: Keep `n2`
    assert_snapshot!(out, @r#"
    circuit main {
        node n1 x=0 y=0
        - n1 node1
        std.resistor r x=4 y=0
    node node1 x=2
    - node1 r.+
    }
    "#);
}
