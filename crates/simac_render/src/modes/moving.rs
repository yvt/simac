use bevy_ecs::{entity::EntityHashSet, prelude::*};
use bevy_math::Vec2;
use simac_operator::{VirtEntity, stack::VirtEntityHashSet};
use simac_session::{circuit, meta};

use crate::{draw::ModeVisual, pick::HotSpot};

#[cfg(test)]
mod tests;

/// Calculate [`ItemMove::entities`][1] based on selection.
///
/// [1]: simac_session::ops::item_move::ItemMove::entities
pub fn move_target_from_selection(
    ent_circuit: Entity,
    world: &World,
    selected_items: &VirtEntityHashSet,
    try_entity_to_phys: impl Fn(VirtEntity) -> Option<Entity>,
) -> Vec<Entity> {
    let comp_circuit: &circuit::Circuit = world
        .get(ent_circuit)
        .expect("`ent_circuit` does not contain `Circuit`");

    let mut entities = Vec::with_capacity(selected_items.len());
    for &virt_item in selected_items {
        let Some(ent_item) = try_entity_to_phys(virt_item) else { continue };

        if comp_circuit.nodes.contains(&ent_item) {
            // Nothing to do
        } else if comp_circuit.devices.contains(&ent_item) {
            if world.get::<circuit::WireDevice>(ent_item).is_some() {
                // Move the wire terminals instead because the wire itself
                // does not have a `Position` component
                let comp_device = world.get::<circuit::Device>(ent_item).unwrap();
                for &ent_node in comp_device.terminals.iter() {
                    if let Some(comp_terminal_node) = world.get::<meta::TerminalNode>(ent_node) {
                        entities.push(comp_terminal_node.device);
                    } else {
                        entities.push(ent_node);
                    }
                }
                continue;
            }
        } else {
            // This entity is (no longer) a part of `ent_circuit`
            continue;
        }

        entities.push(ent_item);
    }
    entities
}

/// Calculate [`item_displace::Target::Items`][1]`::`{[`ent_devices`][2]<!--
/// -->,[`ent_nodes`][3]} based on selection.
///
/// [1]: simac_session::ops::item_displace::Target::Items
/// [2]: simac_session::ops::item_displace::Target::Items::ent_devices
/// [3]: simac_session::ops::item_displace::Target::Items::ent_nodes
pub fn displace_target_from_selection(
    ent_circuit: Entity,
    world: &World,
    selected_items: &VirtEntityHashSet,
    try_entity_to_phys: impl Fn(VirtEntity) -> Option<Entity>,
) -> (Vec<Entity>, Vec<Entity>) {
    let comp_circuit: &circuit::Circuit = world
        .get(ent_circuit)
        .expect("`ent_circuit` does not contain `Circuit`");

    let mut ent_nodes = Vec::new();
    let mut ent_devices = Vec::new();

    for &virt_item in selected_items {
        let Some(ent_item) = try_entity_to_phys(virt_item) else { continue };

        if comp_circuit.nodes.contains(&ent_item) {
            // Move and merge this node
            ent_nodes.push(ent_item);
        } else if comp_circuit.devices.contains(&ent_item) {
            ent_devices.push(ent_item);
        } else {
            // This entity is (no longer) a part of `ent_circuit`
        }
    }

    (ent_devices, ent_nodes)
}

#[derive(Debug)]
pub struct MoveByMouseMode {
    mouse_initial_position: Vec2,
    mouse_position: Vec2,
    circuit: Entity,
    move_or_displace: MoveOrDisplace,
    highlight_entities: EntityHashSet,
}

#[derive(Debug, Clone)]
enum MoveOrDisplace {
    Move(Vec<Entity>),
    DisplaceItems {
        ent_devices: Vec<Entity>,
        // The references to nodes must be stored as `VirtEntity` because
        // `ItemAttach` might merge them into other nodes and despawn them
        ent_nodes: Vec<VirtEntity>,
    },
    DisplaceWireTerminal {
        ent_device: Entity,
        terminal_i: bool,
    },
}

impl MoveByMouseMode {
    fn new(
        move_or_displace: MoveOrDisplace,
        circuit: Entity,
        mouse_initial_position: Vec2,
        cx: &mut dyn super::ModeCx,
    ) -> Self {
        let world = cx.world();

        let mut highlight_entities: EntityHashSet = <_>::default();

        match &move_or_displace {
            MoveOrDisplace::Move(ent_items) => {
                highlight_entities.extend(ent_items.iter().copied());

                // When moving a device, highlight its terminals
                for &ent in ent_items {
                    if let Some(comp_device) = world.get::<circuit::Device>(ent) {
                        highlight_entities.extend(comp_device.terminals.iter().copied());
                    }
                }

                // When moving wire terminals, highlight the wire
                if let Some(comp_circuit) = world.get::<circuit::Circuit>(circuit) {
                    for &ent_device in &comp_circuit.devices {
                        if world.get::<circuit::WireDevice>(ent_device).is_none() {
                            continue;
                        }
                        let Some(comp_device) = world.get::<circuit::Device>(ent_device) else {
                            continue;
                        };
                        if comp_device
                            .terminals
                            .iter()
                            .all(|x| highlight_entities.contains(x))
                        {
                            highlight_entities.insert(ent_device);
                        }
                    }
                }
            }
            MoveOrDisplace::DisplaceItems { .. } | MoveOrDisplace::DisplaceWireTerminal { .. } => {
                // Build `highlight_entities` in `Mode::visual` in this case.
                //
                // We want to highlight device terminals, but `ItemDisplace`
                // might replace them with different node entities, so we can
                // not build `highlight_entities` yet.
            }
        }

        Self {
            mouse_initial_position,
            mouse_position: mouse_initial_position,
            circuit,
            move_or_displace,
            highlight_entities,
        }
    }

    /// Construct this mode based on a given [`HotSpot`].
    pub fn from_hot_spot(
        hot: HotSpot,
        ent_circuit: Entity,
        mouse_initial_position: Vec2,
        displace: bool,
        cx: &mut dyn super::ModeCx,
    ) -> Self {
        let world = cx.world();

        // Decide the entities to move
        let move_or_displace = match hot {
            HotSpot::Device { ent_device } => {
                let mut entities = Vec::new();
                if displace {
                    MoveOrDisplace::DisplaceItems {
                        ent_devices: vec![ent_device],
                        ent_nodes: Vec::new(),
                    }
                } else if world.get::<circuit::WireDevice>(ent_device).is_some() {
                    // Move the wire terminals instead because the wire itself
                    // does not have a `Position` component
                    let comp_device = world.get::<circuit::Device>(ent_device).unwrap();
                    for &ent_node in comp_device.terminals.iter() {
                        if let Some(comp_terminal_node) = world.get::<meta::TerminalNode>(ent_node)
                        {
                            entities.push(comp_terminal_node.device);
                        } else {
                            entities.push(ent_node);
                        }
                    }
                    MoveOrDisplace::Move(entities)
                } else {
                    MoveOrDisplace::Move(vec![ent_device])
                }
            }
            HotSpot::WireTerminalNode {
                ent_node,
                ent_device,
                terminal_i,
            } => {
                if displace {
                    // Detach the wire
                    MoveOrDisplace::DisplaceWireTerminal {
                        ent_device,
                        terminal_i,
                    }
                } else if let Some(comp_terminal_node) = world.get::<meta::TerminalNode>(ent_node) {
                    // Move the device owning this terminal node
                    MoveOrDisplace::Move(vec![comp_terminal_node.device])
                } else {
                    // Move the node
                    MoveOrDisplace::Move(vec![ent_node])
                }
            }
            HotSpot::OpenTerminalNode { ent_device, .. } => {
                if displace {
                    MoveOrDisplace::DisplaceItems {
                        ent_devices: vec![ent_device],
                        ent_nodes: Vec::new(),
                    }
                } else {
                    MoveOrDisplace::Move(vec![ent_device])
                }
            }
            HotSpot::OrphanNode { ent_node } => {
                if displace {
                    MoveOrDisplace::DisplaceItems {
                        ent_devices: Vec::new(),
                        ent_nodes: vec![cx.entity_to_virt(ent_node)],
                    }
                } else {
                    MoveOrDisplace::Move(vec![ent_node])
                }
            }
        };

        // Create a `Mode`
        Self::new(move_or_displace, ent_circuit, mouse_initial_position, cx)
    }

    pub fn from_selection(
        selected_items: &VirtEntityHashSet,
        ent_circuit: Entity,
        mouse_initial_position: Vec2,
        displace: bool,
        cx: &mut dyn super::ModeCx,
    ) -> Self {
        let move_or_displace = if displace {
            let (ent_devices, ent_nodes) =
                displace_target_from_selection(ent_circuit, cx.world(), selected_items, |e| {
                    cx.try_entity_to_phys(e).ok()
                });

            let ent_nodes = ent_nodes
                .into_iter()
                .map(|e| cx.entity_to_virt(e))
                .collect();

            MoveOrDisplace::DisplaceItems {
                ent_devices,
                ent_nodes,
            }
        } else {
            let entities =
                move_target_from_selection(ent_circuit, cx.world(), selected_items, |e| {
                    cx.try_entity_to_phys(e).ok()
                });

            MoveOrDisplace::Move(entities)
        };

        // Create a `Mode`
        Self::new(move_or_displace, ent_circuit, mouse_initial_position, cx)
    }
}

impl super::Mode for MoveByMouseMode {
    fn operator(
        &mut self,
        cx: &mut dyn super::ModeCx,
    ) -> Box<dyn simac_operator::operator::Operator> {
        let delta = (self.mouse_position - self.mouse_initial_position)
            .round()
            .as_i64vec2();
        match self.move_or_displace.clone() {
            MoveOrDisplace::Move(entities) => Box::new(simac_session::ops::item_move::ItemMove {
                entities,
                delta,
                delta_rotation: <_>::default(),
            }),
            MoveOrDisplace::DisplaceItems {
                ent_devices,
                ent_nodes,
            } => Box::new(simac_session::ops::item_displace::ItemDisplace {
                ent_circuit: self.circuit,
                target: simac_session::ops::item_displace::Target::Items {
                    ent_devices,
                    ent_nodes: ent_nodes.iter().map(|&e| cx.entity_to_phys(e)).collect(),
                },
                delta,
                delta_rotation: <_>::default(),
            }),
            MoveOrDisplace::DisplaceWireTerminal {
                ent_device,
                terminal_i,
            } => Box::new(simac_session::ops::item_displace::ItemDisplace {
                ent_circuit: self.circuit,
                target: simac_session::ops::item_displace::Target::WireTerminal {
                    ent_device,
                    terminal_i,
                },
                delta,
                delta_rotation: <_>::default(),
            }),
        }
    }

    fn visual(
        &self,
        cx: &mut dyn super::ModeCx,
        undo: &dyn simac_operator::operator::OperatorUndo,
    ) -> ModeVisual {
        let mut entities = self.highlight_entities.clone();
        let world = cx.world();

        match &self.move_or_displace {
            MoveOrDisplace::Move(_) => {}
            MoveOrDisplace::DisplaceItems {
                ent_devices,
                ent_nodes: _,
            } => {
                let op_displace: &simac_session::ops::item_displace::ItemDisplaceUndo = undo
                    .as_any()
                    .downcast_ref()
                    .expect("`ItemDisplace` should produce `ItemDisplaceUndo`");

                entities.extend(ent_devices.iter().copied());
                entities.extend(op_displace.merged_nodes(|e| cx.entity_to_phys(e)));

                // Highlight device terminals
                for &ent_device in ent_devices {
                    let comp_device = world.get::<circuit::Device>(ent_device).unwrap();
                    entities.extend(comp_device.terminals.iter().copied());
                }
            }
            &MoveOrDisplace::DisplaceWireTerminal {
                ent_device,
                terminal_i,
            } => {
                entities.insert(ent_device);

                let comp_device = world.get::<circuit::Device>(ent_device).unwrap();
                entities.insert(comp_device.terminals[terminal_i as usize]);
            }
        }

        let attach_points = match &self.move_or_displace {
            MoveOrDisplace::Move(_) => Vec::new(),
            MoveOrDisplace::DisplaceItems { .. } | MoveOrDisplace::DisplaceWireTerminal { .. } => {
                let op_displace: &simac_session::ops::item_displace::ItemDisplaceUndo = undo
                    .as_any()
                    .downcast_ref()
                    .expect("`ItemDisplace` should produce `ItemDisplaceUndo`");
                op_displace.attach_points().to_owned()
            }
        };

        ModeVisual {
            entities,
            attach_points,
            ..<_>::default()
        }
    }

    fn mouse_move(&mut self, position: Vec2) {
        self.mouse_position = position;
    }

    // TODO: fn set_modifiers
}
