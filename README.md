# Simac

<img src="docs/resistors.gif" align="right" width="117" height="262">

*Simac (Simulator for Analog Circuits)* is a program to create and simulate analog circuits for casual/educational uses.

**Work in progress**

## Remaining Tasks

- Schematic editing
    - Variable zoom
    - Adding devices
    - Dividing wires
    - Clipboard operations
    - Duplicating circuit items
    - Editing subcircuits
    - Grouping a set of items into a new subcircuit
    - Expanding subcircuits?
    - Auto-rearranging/auto-routing

- Symbol editor

- UI
    - Scopes
    - Device toolbox
    - Device parameter editor
    - Root circuit parameter editor
    - Translation support

- I/O
    - Scopes
    - Device models
    - Interactive devices
    - Symbol writer

- Simulation
    - Gmin stepping
    - Run simulation on background thread
    - Diagnose common error cases (e.g., zero resistance loop)
    - Eliminate wires internally to speed up simulation
    - Rshunt
    - Minimize fill-ins

- Standard library ([`std.simac`](crates/simac_session/std.simac))
    - Linear elements
        - Controlled sources
        - Coupled inductors
    - Active components
        - More diode models
        - MOSFETs
        - JFETs
        - DIACs
        - TRIACs
        - Varicap
    - Integrated circuits
        - Opamps
        - Schmitt trigger
        - Logic ICs
        - 555 timer
        - DACs/ADCs
        - VCOs
    - I/O
        - Lamps
        - Switches

## Design Goals

- **Modern UI.** The application's user interface should meet the high standards expected with modern applications.

    - [The GTK shell](crates/simac_gtk) uses [libadwaita][1] and tries to adhere to [the GNOME Human Interface Guidelines][2].
      Touch and stylus inputs are supported.

- **Portability.** Low-level components should avoid depending on specific runtime environments.

    - [Device models](crates/simac_elements) are implemented by no-std generic code so that the simulation can run anywhere where Rust can, even where the full standard library or double-precision FP datatype is not supported.

- **Interactivity.** Place emphasis on responsiveness over qualitative accuracy.

    - Default error limits can be relaxed to provide a reasonable simulation speed as long as this does not preclude important phenomenons from being observed.

- **Intuitiveness.** Imitate breadboard experiments.
  The simulator should try to produce reasonable results anytime barring the most extreme or contradictory situations.

- **Hackable.** The source code should be well-organized and documented to make it understandable, educative, and extensible while providing reasonable performance.

    - This project is mostly written in the [Rust][5] programming language, whose strong typing and [ownership-oriented][6] programming paradigm guide programmers toward writing correct, well-organized code, and whose tooling allows them to [document][7] the code easily and regularly.

    - New compact device models can be easily [added](ARCHITECTURE.md#add-a-new-device).
      They can be implemented in [Rust][5] or [Verilog-A][4].


## Developing

On NixOS:

```console
$ nix develop
$ cargo run -p simac_shell_gtk
```

[1]: https://gnome.pages.gitlab.gnome.org/libadwaita/
[2]: https://developer.gnome.org/hig/
[3]: https://crates.io/crates/tagref
[4]: https://en.wikipedia.org/wiki/Verilog-A
[5]: https://www.rust-lang.org/
[6]: https://doc.rust-lang.org/book/ch04-00-understanding-ownership.html
[7]: https://doc.rust-lang.org/book/ch14-02-publishing-to-crates-io.html#making-useful-documentation-comments
